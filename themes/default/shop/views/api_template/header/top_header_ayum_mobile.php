<?php
    if ($this->api_shop_setting[0]['api_lang_key'] == 'en') {
        $temp_1 = '';
        $temp_2 = 'api_language_a_link_selected';
    }
    if ($this->api_shop_setting[0]['api_lang_key'] == 'ja') {
        $temp_1 = 'api_language_a_link_selected';
        $temp_2 = '';
    }   
    $temp_display_language_selector = '
        <div>
            <a class="api_big api_language_a_link '.$temp_1.'" href="'.base_url().'/main/language/japanese">
                JP
            </a>
            |
            <a class="api_big api_language_a_link '.$temp_2.'" href="'.base_url().'/main/language/english">
                EN
            </a>                
        </div>
    ';

    $temp_display = '
    <div id="api_header" style="width:100%; background-color:#efefef">
        <div id="api_header_sticky">
            <div class="api_height_5"></div>
            <table width="100%" border="0" height="35">
            <tr>
            <td valign="middle" align="center" width="50">
                <a id="api_menu_mobile_button" class="api_pointer" href="#">
                    <span class="fa fa-bars fa-lg"></span>
                </a>
            </td>
            <td valign="middle" align="left">
                <a class="api_color_white api_margin_left_5" href="'.site_url().'">
                    <img alt="'.$this->api_web[0]['title_'.$l].'" src="'.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/'.$this->api_web[0]['logo'].'" height="25px;" />
                </a>
            </td>   
            <td valign="middle"  align="center" width="80">
                '.$temp_display_language_selector.'
            </td>               
            </tr>
            </table>
        </div>
    </div>
    ';
    $config_data = array(
        'custom_html' => '',
        'display' => $temp_display,
        'type' => 'full',
    );
    $temp = $this->api_display->template_display($config_data);
    echo $temp['display'];    
 
?>
<script>
    window.onscroll = function() {
        api_header_sticky()
    };
    
    var header = document.getElementById("api_header");
    var header_sticky = document.getElementById("api_header_sticky");
    
    var sticky = header.offsetTop;
    
    function api_header_sticky() {
        if (window.pageYOffset >= sticky) {
            header.classList.add("sticky");
            header_sticky.classList.add("api_header_sticky");
        } 
        else 
        {
            header.classList.remove("sticky");
            header_sticky.classList.remove("api_header_sticky");
        }
    }
</script>