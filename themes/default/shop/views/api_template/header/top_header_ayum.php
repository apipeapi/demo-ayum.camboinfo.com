
<?php
$field_data = array();
$field_data[0] = array(
    'type' => 'file',
    'col_class' => 'col-md-6',
    'field_name' => 'logo',    
    'field_label_name' => 'Web Logo',
    'field_value' => $this->api_web[0]['logo'],
    'recommended_width' => 207,
    'recommended_height' => 60,
);
$field_data[1] = array(
    'type' => 'file',
    'col_class' => 'col-md-6',
    'field_name' => 'apple_logo',    
    'field_label_name' => 'Web Apple Logo',
    'field_value' => $this->api_web[0]['logo'],
    'recommended_width' => 100,
    'recommended_height' => 100,
);
$field_data[2] = array(
    'type' => 'file',
    'col_class' => 'col-md-6',
    'field_name' => 'icon',    
    'field_label_name' => 'Web Icon',
    'field_value' => $this->api_web[0]['logo'],
    'recommended_width' => 16,
    'recommended_height' => 16,
);
$config_data = array(
    'l' => $l,
    'wrapper_class' => 'api_logo_wrapper',
    'modal_class' => '',
    'title' => 'Edit Logo', 
    'field_data' => $field_data,
    'selected_id' => $this->api_web[0]['id'],
    'table_name' => 'sma_web',
    'table_id' => 'id',
    'upload_path' => 'assets/uploads/web/'.$this->api_web[0]['id'],
    'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
);
$temp_admin = $this->api_admin->front_end_edit($config_data);
$this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

$temp_display = '';

if ($this->session->userdata('api_mobile') != 1) {
    $temp_display .= '
    <div class="col-md-12">
        <table width="100%" border="0">
        <tr>
        <td valign="middle" class="api_temp_td_1">  
            <div class="api_admin api_logo_wrapper">
                <a href="'.site_url().'">
                    <img class="api_logo" alt="'.$this->api_web[0]['title_'.$l].'" src="'.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/'.$this->api_web[0]['logo'].'" />
                </a>           
                '.$temp_admin['display'].'  
            </div>
        </td>
    ';
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    $config_data = array(
        'table_name' => 'sma_menu',
        'select_table' => 'sma_menu',
        'translate' => 'yes',
        'translate_2' => 'yes',
        'description' => '',
        'select_condition' => "id > 0 order by ordering asc",
    );
    $api_header_menu = $this->api_helper->api_select_data_v2($config_data);    
    for ($i=0;$i<count($api_header_menu);$i++) {
        $temp_selected = '';
        if (is_int(strpos($actual_link,$api_header_menu[$i]['slug']))) {
            $temp_selected = 'api_menu_box_selected';
        }        

        $temp_display .= '
            <td valign="middle" align="center" class="api_menu_wrapper_'.$api_header_menu[$i]['id'].' api_temp_menu_td">
                <div class="api_admin api_menu_wrapper">
                    <a class="api_menu_box '.$temp_selected.'" href="'.$api_header_menu[$i]['link'].'" >
                        <div class="api_display_table_cell api_vertical_align_middle">
                            <div class="api_big">
                                '.$api_header_menu[$i]['title_'.$l].'
                            </div>
                            <div class="api_normal">
                                '.$api_header_menu[$i]['title_2_'.$l].'
                            </div>
                        </div>
                    </a>
        ';
                    $field_data[0] = array(
                        'type' => 'translate',
                        'col_class' => 'col-md-12',
                        'field_name' => 'translate',
                        'translate' => 'yes',
                        'field_label_name' => 'Title Top',                      
                        'field_value' => $api_header_menu[$i]['title_'.$l],
                    );
                    $field_data[1] = array(
                        'type' => 'translate',
                        'col_class' => 'col-md-12',
                        'field_name' => 'translate_2',
                        'translate_2' => 'yes',
                        'field_label_name' => 'Title Bottom',                   
                        'field_value' => $api_header_menu[$i]['title_2_'.$l],
                    );     
                    $field_data[2] = array(
                        'type' => 'text',
                        'col_class' => 'col-md-12',
                        'field_name' => 'link',
                        'field_label_name' => 'Menu Link',                   
                        'field_value' => $api_header_menu[$i]['link'],
                    );                            
                    $config_data = array(
                        'l' => $l,
                        'wrapper_class' => 'api_menu_wrapper_'.$api_header_menu[$i]['id'],
                        'modal_class' => '',
                        'title' => 'Edit Menu', 
                        'field_data' => $field_data,
                        'selected_id' => $api_header_menu[$i]['id'],
                        'table_name' => 'sma_menu',
                        'table_id' => 'id',
                        'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
                    );
                    $temp_admin = $this->api_admin->front_end_edit($config_data);  
                    $temp_display .=  $temp_admin['display'];
                    $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
                    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

        $temp_display .= '
                    <div class="api_clear_both"></div>
                </div>
            </td>
        ';
    }

    if ($this->api_shop_setting[0]['api_lang_key'] == 'en') {
        $temp_1 = '';
        $temp_2 = 'api_language_a_link_selected';
    }
    if ($this->api_shop_setting[0]['api_lang_key'] == 'ja') {
        $temp_1 = 'api_language_a_link_selected';
        $temp_2 = '';
    }    
    $temp_display .= '
        <td valign="middle" align="center" width="65">
            <div>
                <a class="api_big api_language_a_link '.$temp_1.'" href="'.base_url().'/main/language/japanese">
                    JP
                </a>
                |
                <a class="api_big api_language_a_link '.$temp_2.'" href="'.base_url().'/main/language/english">
                    EN
                </a>                
            </div>
        </td>
    ';

    $temp_display .= '
            </tr>
        </table>
    </div>
    ';





    echo '
        <div class="api_admin_wrapper">
            <div class="api_header_wrapper">
                <div class="api_header">
                    '.$temp_display.'                   
                    <div class="api_clear_both"></div>
                </div>
                <div class="api_clear_both"></div>  
            </div>               
        </div>                    
    ';




} 
else {
    include('themes/default/shop/views/api_template/header/top_header_ayum_mobile.php');
}

?>


