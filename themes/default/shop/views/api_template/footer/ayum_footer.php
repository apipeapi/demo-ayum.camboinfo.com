<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php

$config_data_function = array(
    'parent_id' => 123,
);
include 'themes/default/shop/views/api/sub_page/api_footer_menu.php';
$temp_display_1 = $temp_display;

$config_data_function = array(
    'parent_id' => 124,
);
include 'themes/default/shop/views/api/sub_page/api_footer_menu.php';
$temp_display_2 = $temp_display;

$config_data_function = array(
    'parent_id' => 125,
);
include 'themes/default/shop/views/api/sub_page/api_footer_menu.php';
$temp_display_3 = $temp_display;

$config_data = array(
    'table_name' => 'sma_footer_menu',
    'select_table' => 'sma_footer_menu',
    'translate' => 'yes',
    'description' => '',
    'select_condition' => "id = 143 order by ordering asc",
);
$temp = $this->api_helper->api_select_data_v2($config_data);
$temp_display_4 = '
    <div class="api_padding_left_15_pc api_admin api_temp_admin_wrapper_footer_'.$temp[0]['id'].'">
        <a class="api_big api_footer_link" href="'.$temp[0]['link'].'" >
            <table width="100%" border="0">
            <tr class="">
            <td valign="top" align="center" width="25">
                '.$temp[0]['icon'].'
            </td>            
            <td valign="top" align="left">
                <div class="api_text_transform_uppercase ">
                    <b>'.$temp[0]['title_'.$l].'</b>
                </div>
            </td>
            </tr>
            </table>        
        </a>
';
            $field_data = array();
            $field_data[0] = array(
                'type' => 'translate',
                'col_class' => 'col-md-12',
                'field_class' => '',
                'field_name' => 'translate',
                'translate' => 'yes',
                'field_label_name' => 'Title',                      
                'field_value' => $temp[0]['title_'.$l],
            );
            $field_data[1] = array(
                'type' => 'text',
                'col_class' => 'col-md-12',
                'field_class' => '',
                'field_name' => 'link',
                'field_label_name' => 'Link',                      
                'field_value' => $temp[0]['link'],
            );            
            $config_data = array(
                'l' => $l,
                'wrapper_class' => 'api_temp_admin_wrapper_footer_'.$temp[0]['id'],
                'modal_class' => '',
                'title' => 'Edit', 
                'field_data' => $field_data,
                'selected_id' => $temp[0]['id'],
                'table_name' => 'sma_footer_menu',
                'table_id' => 'id',
                'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
            );
            $temp_admin = $this->api_admin->front_end_edit($config_data);
            $temp_display_4 .= $temp_admin['display'];

            $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
            $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

$temp_display_4 .= '
    </div> 
';

$config_data = array(
    'table_name' => 'sma_footer_menu',
    'select_table' => 'sma_footer_menu',
    'translate' => 'yes',
    'description' => '',
    'select_condition' => "parent_id = 141 order by ordering asc",
);
$temp = $this->api_helper->api_select_data_v2($config_data);
for ($i=0;$i<count($temp);$i++) {
    $temp_display_4 .= '
        <div class="api_height_10"></div>
        <div class="api_admin api_temp_admin_wrapper_footer_'.$temp[$i]['id'].' api_padding_left_15_pc">
            <a class="api_footer_link" href="'.$temp[$i]['link'].'">
                <table width="100%" border="0">
                <tr>
                <td valign="top" align="center" width="25">
                    '.$temp[$i]['icon'].'
                </td>               
                <td valign="top" align="left">
                    '.$temp[$i]['title_'.$l].'
                </td>
                </tr>
                </table>           
            </a>
    ';
            $field_data = array();
            $field_data[0] = array(
                'type' => 'translate',
                'col_class' => 'col-md-12',
                'field_class' => '',
                'field_name' => 'translate',
                'translate' => 'yes',
                'field_label_name' => 'Title',                      
                'field_value' => $temp[$i]['title_'.$l],
            );
            $field_data[1] = array(
                'type' => 'text',
                'col_class' => 'col-md-12',
                'field_class' => '',
                'field_name' => 'link',
                'field_label_name' => 'Link',                      
                'field_value' => $temp[$i]['link'],
            );            
            $config_data = array(
                'l' => $l,
                'wrapper_class' => 'api_temp_admin_wrapper_footer_'.$temp[$i]['id'],
                'modal_class' => '',
                'title' => 'Edit', 
                'field_data' => $field_data,
                'selected_id' => $temp[$i]['id'],
                'table_name' => 'sma_footer_menu',
                'table_id' => 'id',
                'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
            );
            $temp_admin = $this->api_admin->front_end_edit($config_data);
            $temp_display_4 .= $temp_admin['display'];

            $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
            $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display_4 .= '
        </div>
    ';
}

$config_data = array(
    'table_name' => 'sma_footer_menu',
    'select_table' => 'sma_footer_menu',
    'translate' => 'yes',
    'description' => '',
    'select_condition' => "id = 144 order by ordering asc",
);
$temp = $this->api_helper->api_select_data_v2($config_data);
$temp_display_4 .= '
    <div class="api_height_10"></div>
    <div class="api_padding_left_15_pc api_admin api_temp_admin_wrapper_footer_'.$temp[0]['id'].'">
        <a class="api_footer_link" href="javascript:void(0);" onclick="$(\'body,html\').animate({scrollTop:0},500); return false">
            <table width="100%" border="0">
            <tr>
            <td valign="top" align="center" width="25">
                '.$temp[0]['icon'].'
            </td>                 
            <td valign="top" align="left">
                '.$temp[0]['title_'.$l].'
            </td>
            </tr>
            </table>                
        </a>
';
            $field_data = array();
            $field_data[0] = array(
                'type' => 'translate',
                'col_class' => 'col-md-12',
                'field_class' => '',
                'field_name' => 'translate',
                'translate' => 'yes',
                'field_label_name' => 'Title',                      
                'field_value' => $temp[0]['title_'.$l],
            );
            $config_data = array(
                'l' => $l,
                'wrapper_class' => 'api_temp_admin_wrapper_footer_'.$temp[0]['id'],
                'modal_class' => '',
                'title' => 'Edit', 
                'field_data' => $field_data,
                'selected_id' => $temp[0]['id'],
                'table_name' => 'sma_footer_menu',
                'table_id' => 'id',
                'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
            );
            $temp_admin = $this->api_admin->front_end_edit($config_data);
            $temp_display_4 .= $temp_admin['display'];

            $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
            $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

$temp_display_4 .= '
    </div>    
';


$temp_display_5 = '
    <div class="api_admin api_footer_logo_wrapper">
        <a href="'.site_url().'">
            <img class="" alt="'.$this->api_web[0]['title_'.$l].'" src="'.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/'.$this->api_web[0]['logo'].'" width="150" />
        </a>
';
    $field_data[0] = array(
        'type' => 'file',
        'col_class' => 'col-md-6',
        'field_name' => 'logo',    
        'field_label_name' => 'Logo',
        'field_value' => $this->api_web[0]['logo'],
        'recommended_width' => 207,
        'recommended_height' => 60,
    );
    $config_data = array(
        'l' => $l,
        'wrapper_class' => 'api_footer_logo_wrapper',
        'modal_class' => '',
        'title' => 'Edit Logo', 
        'field_data' => $field_data,
        'selected_id' => $this->api_web[0]['id'],
        'table_name' => 'sma_web',
        'table_id' => 'id',
        'upload_path' => 'assets/uploads/web/'.$this->api_web[0]['id'],
        'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
    );
    $temp_admin = $this->api_admin->front_end_edit($config_data);
    $temp_display_5 .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

$temp_display_5 .= '
    </div>
';

$temp_display_5 .= '
    <div class="api_height_10"></div>
    <div class="api_normal">
';
$temp_admin = $this->api_helper->api_lang_v2('Social Media');
$temp_display_5 .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

$temp_display_5 .= '
    </div>    
    <div class="api_height_10"></div>
';

if ($l == 'en') {
    $temp_1 = 'facebook_link';
    $temp_2 = 'Facebook Link English';
}
if ($l == 'ja') {
    $temp_1 = 'facebook_link_2';
    $temp_2 = 'Facebook Link Japanese';
}

$temp_display_5 .= '
    <div class="api_display_inline_block">
        <div class="api_admin api_admin_web_facebook_link">
            <a class="api_link_box_none" target="_blank" href="'.$this->api_web[0][$temp_1].'">
                <img class="" alt="Facebook" src="'.base_url('assets/images/facebook_icon.png').'" />
            </a>
        
';
            $field_data = array();
            $field_data[0] = array(
                'type' => 'text',
                'col_class' => 'col-md-12',
                'field_class' => '',
                'field_name' => $temp_1,
                'field_label_name' => $temp_2,                   
                'field_value' => $this->api_web[0][$temp_1],
            );    
            $config_data = array(
                'l' => $l,
                'wrapper_class' => 'api_admin_web_facebook_link',
                'modal_class' => '',
                'title' => 'Edit', 
                'field_data' => $field_data,
                'selected_id' => $this->api_web[0]['id'],
                'table_name' => 'sma_web',
                'table_id' => 'id',
                'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
            );
            $temp_admin = $this->api_admin->front_end_edit($config_data);
            $temp_display_5 .= $temp_admin['display'];
            $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
            $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script']; 
$temp_display_5 .= '
        </div>
    </div>
';

$temp_display_5 .= '
    <div class="api_display_inline_block api_padding_left_10">
        <div class="api_admin api_admin_web_linkedin_link">
            <a class="api_link_box_none" target="_blank" href="'.$this->api_web[0]['linkedin_link'].'">
                <img class="" alt="linkedIn" src="'.base_url('assets/images/linkedIn.png').'" />
            </a>
        
';
            $field_data = array();
            $field_data[0] = array(
                'type' => 'text',
                'col_class' => 'col-md-12',
                'field_class' => '',
                'field_name' => 'linkedin_link',
                'field_label_name' => 'LinkedIn Link',                   
                'field_value' => $this->api_web[0]['linkedin_link'],
            );    
            $config_data = array(
                'l' => $l,
                'wrapper_class' => 'api_admin_web_linkedin_link',
                'modal_class' => '',
                'title' => 'Edit', 
                'field_data' => $field_data,
                'selected_id' => $this->api_web[0]['id'],
                'table_name' => 'sma_web',
                'table_id' => 'id',
                'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
            );
            $temp_admin = $this->api_admin->front_end_edit($config_data);
            $temp_display_5 .= $temp_admin['display'];
            $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
            $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script']; 
$temp_display_5 .= '
        </div>
    </div>
';

$temp_display_6 .= '
    <div class="api_big api_admin api_admin_wrapper_web_title">
        '.$this->api_web[0]['title_'.$l].'
';
        $field_data = array();
        $field_data[0] = array(
            'type' => 'translate',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'translate',
            'translate' => 'yes',
            'field_label_name' => 'Website Title',
            'field_value' => $this->api_web[0]['title_'.$l],
        );
        $config_data = array(
            'l' => $l,
            'wrapper_class' => 'api_admin_wrapper_web_title',
            'modal_class' => '',
            'title' => 'Edit', 
            'field_data' => $field_data,
            'selected_id' => $this->api_web[0]['id'],
            'table_name' => 'sma_web',
            'table_id' => 'id',
            'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
        );
        $temp_admin = $this->api_admin->front_end_edit($config_data);
        $temp_display_6 .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display_6 .= '
    </div>
    <div class="api_clear_both"></div>
    <div class="api_height_10"></div>
    <div class="api_admin api_admin_wrapper_web_address">
        '.$this->api_web[0]['address'].'
';
$field_data = array();
$field_data[0] = array(
    'type' => 'textarea',
    'col_class' => 'col-md-12',
    'field_class' => '',
    'field_name' => 'address',
    'field_label_name' => 'Website Address',                   
);    
$config_data = array(
    'l' => $l,
    'wrapper_class' => 'api_admin_wrapper_web_address',
    'modal_class' => 'modal-lg',
    'title' => 'Edit', 
    'field_data' => $field_data,
    'selected_id' => $this->api_web[0]['id'],
    'table_name' => 'sma_web',
    'table_id' => 'id',
    'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
);
$temp_admin = $this->api_admin->front_end_edit($config_data);
$temp_display_6 .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display_6 .= '
    </div>
    <div class="api_clear_both"></div>
';

$temp_display_7 .= '
    <div class="api_height_25"></div>
    <table width="100%">
        <tr>
            <td valign="top"><li class="fa fa-phone"></li></td>
            <td valign="top" class="api_padding_left_5">
                <div class="api_admin api_admin_wrapper_web_phone">
                    '.$this->api_web[0]['phone'].'
';
                    $field_data = array();
                    $field_data[0] = array(
                        'type' => 'text',
                        'col_class' => 'col-md-12',
                        'field_class' => '',
                        'field_name' => 'phone',
                        'field_label_name' => 'Website Phone',                   
                        'field_value' => $this->api_web[0]['phone'],
                    );    
                    $config_data = array(
                        'l' => $l,
                        'wrapper_class' => 'api_admin_wrapper_web_phone',
                        'modal_class' => '',
                        'title' => 'Edit', 
                        'field_data' => $field_data,
                        'selected_id' => $this->api_web[0]['id'],
                        'table_name' => 'sma_web',
                        'table_id' => 'id',
                        'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
                    );
                    $temp_admin = $this->api_admin->front_end_edit($config_data);
                    $temp_display_7 .= $temp_admin['display'];
                    $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
                    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display_7 .= '
                </div>
            </td>
        </tr>
        <tr>
            <td valign="top"><i class="fa fa-envelope-o"></i></td>
            <td valign="top" class="api_padding_left_5">
                <div class="api_admin api_admin_wrapper_web_email">
                    '.$this->api_web[0]['email'].'
';
                    $field_data = array();
                    $field_data[0] = array(
                        'type' => 'text',
                        'col_class' => 'col-md-12',
                        'field_class' => '',
                        'field_name' => 'email',
                        'field_label_name' => 'Website Email',                   
                        'field_value' => $this->api_web[0]['email'],
                    );    
                    $config_data = array(
                        'l' => $l,
                        'wrapper_class' => 'api_admin_wrapper_web_email',
                        'modal_class' => '',
                        'title' => 'Edit', 
                        'field_data' => $field_data,
                        'selected_id' => $this->api_web[0]['id'],
                        'table_name' => 'sma_web',
                        'table_id' => 'id',
                        'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
                    );
                    $temp_admin = $this->api_admin->front_end_edit($config_data);
                    $temp_display_7 .= $temp_admin['display'];
                    $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
                    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display_7 .= '
                </div>
            </td>
        </tr>
    </table>   
    <div class="api_height_10"></div>
    <div class="api_admin api_admin_wrapper_web_map_link">
        <a href="'.$this->api_web[0]['map_link'].'" target="_blank">
            <div class="api_button_view_map">
                <i class="fa fa-map-marker"></i>&nbsp; '.lang('view_map').'
            </div>      
        </a>
';

        $field_data = array();
        $field_data[0] = array(
            'type' => 'text',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'map_link',
            'field_label_name' => 'Map Link',                   
            'field_value' => $this->api_web[0]['map_link'],
        );
        $config_data = array(
            'l' => $l,
            'wrapper_class' => 'api_admin_wrapper_web_map_link',
            'modal_class' => '',
            'title' => 'Edit', 
            'field_data' => $field_data,
            'selected_id' => $this->api_web[0]['id'],
            'table_name' => 'sma_web',
            'table_id' => 'id',
            'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
        );
        $temp_admin = $this->api_admin->front_end_edit($config_data);
        $temp_display_7 .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

$temp_display_7 .= '
    </div>
    <div class="api_clear_both"></div>
';





$temp_display_7_mobile .= '
    <div class="api_height_25"></div>
    <table>
        <tr>
            <td valign="top"><li class="fa fa-phone"></li></td>
            <td valign="top" class="api_padding_left_5">
                <div class="">
                    '.$this->api_web[0]['phone'].'
                </div>
            </td>
        </tr>
        <tr>
            <td valign="top"><i class="fa fa-envelope-o"></i></td>
            <td valign="top" class="api_padding_left_5">
                <div class="api_admin api_admin_wrapper_web_email">
                    '.$this->api_web[0]['email'].'
                </div>
            </td>
        </tr>
    </table>   
    <div class="api_height_10"></div>
    <div class="">
        <a href="'.$this->api_web[0]['map_link'].'" target="_blank">
            <div class="api_button_view_map">
                <i class="fa fa-map-marker"></i>&nbsp; '.lang('view_map').'
            </div>      
        </a>
    </div>
    <div class="api_clear_both"></div>
';


if ($l == 'ja') {
    $temp_display_8 .= '
        <div class="api_admin api_admin_wrapper_web_line_link">
            <a href="'.$this->api_web[0]['line_link'].'" target="_blank">
                <img class="api_link_box_none" src="'.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/'.$this->api_web[0]['line_image'].'" width="187"/>
            </a>
    ';
    $field_data = array();
    $field_data[0] = array(
        'type' => 'text',
        'col_class' => 'col-md-12',
        'field_class' => '',
        'field_name' => 'line_link',
        'field_label_name' => 'LINE Link',                   
        'field_value' => $this->api_web[0]['line_link'],
    );
    $field_data[1] = array(
        'type' => 'file',
        'col_class' => 'col-md-6',
        'field_class' => '',
        'field_name' => 'line_image',    
        'label_name' => 'LINE QR Code',
        'field_value' => $this->api_web[0]['line_image'],
        'recommended_width' => 187,
        'recommended_height' => 55,
    );    
    $config_data = array(
        'l' => $l,
        'wrapper_class' => 'api_admin_wrapper_web_line_link',
        'modal_class' => '',
        'title' => 'Edit', 
        'field_data' => $field_data,
        'selected_id' => $this->api_web[0]['id'],
        'table_name' => 'sma_web',
        'table_id' => 'id',
        'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
        'upload_path' => 'assets/uploads/web/'.$this->api_web[0]['id'],
    );
    $temp_admin = $this->api_admin->front_end_edit($config_data);
    $temp_display_8 .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display_8 .= '
        </div>
    ';
}
if ($l == 'en') {
    $temp_display_8 .= '
        <div class="en">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Contact us through via Telegram');
    $temp_display_8 .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display_8 .= '
        </div>
    ';

    $temp_display_8 .= '
        <div class="api_height_5"></div>
        <div class="en api_admin api_admin_wrapper_web_telegram_link">
            <a class="api_footer_link" href="'.$this->api_web[0]['telegram_link'].'" target="_blank" title="Telegram">
                <table border="0" >
                <tr>
                    <td>
                        <img  class="api_link_box_none" src="'.base_url('assets/images/Logo_telegram.png').'" style="width:30px" />
                    </td>
                    <td valign="middle" class="api_padding_left_5">                        
                        '.$this->api_web[0]['telegram_phone'].'
                    </td>
                </tr>     
                </table>
            </a>
    ';
            $field_data = array();
            $field_data[0] = array(
                'type' => 'text',
                'col_class' => 'col-md-12',
                'field_class' => '',
                'field_name' => 'telegram_phone',
                'field_label_name' => 'Telegram Phone Number',                   
                'field_value' => $this->api_web[0]['telegram_phone'],
            );            
            $field_data[1] = array(
                'type' => 'text',
                'col_class' => 'col-md-12',
                'field_class' => '',
                'field_name' => 'telegram_link',
                'field_label_name' => 'Telegram Link',                   
                'field_value' => $this->api_web[0]['telegram_link'],
            );
            $config_data = array(
                'l' => $l,
                'wrapper_class' => 'api_admin_wrapper_web_telegram_link',
                'modal_class' => '',
                'title' => 'Edit', 
                'field_data' => $field_data,
                'selected_id' => $this->api_web[0]['id'],
                'table_name' => 'sma_web',
                'table_id' => 'id',
                'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
            );
            $temp_admin = $this->api_admin->front_end_edit($config_data);
            $temp_display_8 .= $temp_admin['display'];
            $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
            $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display_8 .= '
            </div>   
        '; 
}





if ($l == 'ja') {
    $temp_display_8_mobile .= '
        <table width="100%" border="0">
        <tr>
        <td valign="middle" align="center">
            <a href="'.$this->api_web[0]['line_link'].'" target="_blank">
            <img class="api_link_box_none" src="'.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/'.$this->api_web[0]['line_image'].'" width="187"/>
            </a>
        </td>
        </tr>
        </table>
    ';
}
if ($l == 'en') {
    $temp_display_8_mobile .= '
        <table width="100%" border="0">
        <tr>
        <td valign="middle" align="center">
            <div class="en">
                '.$this->api_helper->api_lang('Contact us through via Telegram').'
            </div>
        </td>
        </tr>
        </table>
    ';

    $temp_display_8_mobile .= '
        <div class="api_height_5"></div>
        <table width="100%" border="0">
        <tr>
        <td valign="middle" align="center">
            <div class="en">
                <a class="api_footer_link" href="'.$this->api_web[0]['telegram_link'].'" target="_blank" title="Telegram">
                    <table border="0" >
                    <tr>
                        <td>
                            <img  class="api_link_box_none" src="'.base_url('assets/images/Logo_telegram.png').'" style="width:30px" />
                        </td>
                        <td valign="middle" class="api_padding_left_5">                        
                            '.$this->api_web[0]['telegram_phone'].'
                        </td>
                    </tr>     
                    </table>
                </a>
            </div>   
        </td>
        </tr>
        </table>        
    '; 
}



$temp_display_border .= '
    <div class="col-md-12">
        <div class="api_height_10"></div>
        <div class="api_border_bottom"></div>
        <div class="api_height_15"></div>
    </div>
';


$temp_display = '';
$temp_display .= '
    <div class="col-md-12 api_footer_font_size api_padding_0 hidden-sm hidden-xs">
        <div class="col-md-3 col-sm-12">
            <div class="col-md-8 col-sm-8 api_padding_0 api_text_align_center api_temp_td_1">
            '.$temp_display_5.'
            </div>
            <div class="col-md-4 col-sm-4 api_padding_0 api_text_align_center api_temp_td_1">
                <div style="width:1px; height:130px; border-right:1px solid #d3d3d3">
                &nbsp;
                </div>
            </div>
        </div>
        <div class="col-md-9 col-sm-12 api_padding_0">
            <div class="col-md-3 col-sm-3">
                '.$temp_display_1.'
            </div>
            <div class="col-md-3 col-sm-3">
                '.$temp_display_2.'
            </div>
            <div class="col-md-3 col-sm-3">
                '.$temp_display_3.'
            </div> 
            <div class="col-md-3 col-sm-3 api_temp_td_1">
                '.$temp_display_4.'
            </div>    
        </div>
        '.$temp_display_border.'
        <div class="col-md-4">
            '.$temp_display_6.'
        </div>
        <div class="col-md-3">
            '.$temp_display_7.'
        </div>
        <div class="col-md-3">
            <div class="api_height_40 api_clear_both"></div>
            '.$temp_display_8.'
        </div>
    </div>
';

$temp_display .= '
    <div class="col-md-12 api_padding_0 visible-sm hidden-xs">
        <div class="col-md-12 api_padding_0">
            <div class="col-md-4 col-sm-4">
                '.$temp_display_1.'
            </div>
            <div class="col-md-4 col-sm-4">
                '.$temp_display_2.'
            </div>
            <div class="col-md-4 col-sm-4">
                '.$temp_display_3.'
            </div>         
        </div>
        <div class="api_height_10 api_clear_both"></div>
        '.$temp_display_border.'

            <div class="col-md-4 col-sm-2">
                
            </div>
            <div class="col-md-4 col-sm-4">
                '.$temp_display_5.'
            </div>
            <div class="col-md-4 col-sm-6">
                '.$temp_display_6.'
            </div>     
            <div class="api_clear_both"></div>

            <div class="col-md-4 col-sm-2">
                
            </div>
            <div class="col-md-4 col-sm-4"> 
                <div class="api_clear_both api_height_20"></div>
                '.$temp_display_8.'
            </div>      
            <div class="col-md-4 col-sm-6"> 
                '.$temp_display_7.'
            </div>      
        <div class="api_clear_both"></div>
    </div>
';

$temp_display .= '
    <div class="col-md-12 api_padding_0 visible-xs hidden-sm">
        <div class="col-md-12 api_padding_0">
            <div class="col-md-4 col-sm-4">
                '.$temp_display_1.'
            </div>
            <div class="col-md-4 col-sm-4">
                '.$temp_display_2.'
            </div>
            <div class="col-md-4 col-sm-4">
                '.$temp_display_3.'
            </div>         
        </div>
        <div class="api_height_10 api_clear_both"></div>
        '.$temp_display_border.'

        <div class="col-md-12 api_padding_0">
            <div class="col-md-4 col-sm-4">
                '.$temp_display_4.'
            </div>
            <div class="api_clear_both"></div>
            <div class="col-md-4 col-sm-4"> 
                <div class="api_height_15 api_clear_both"></div>
                '.$temp_display_6.'
            </div>      
            <div class="col-md-4 col-sm-4"> 
              <table width="100%">
                <tr>
                    <td valign="middle" align="center">
                    '.$temp_display_7_mobile.'
                    </td>
                </tr>
              </table>  
            </div>
            <div class="col-md-4 text-center">
                <div class="api_height_30 api_clear_both"></div>
                '.$temp_display_5.'
            </div>
            <div class="col-md-4 text-center">
                <div class="api_height_30 api_clear_both"></div>
                '.$temp_display_8_mobile.'
            </div>                       
        </div>
        <div class="api_clear_both"></div>
    </div>
';

echo '
    <div class="api_footer_wrapper">
        <div class="api_footer">
            '.$temp_display.'
            <div class="api_clear_both"></div>
        </div>
        <div class="api_clear_both"></div>
    </div>
';

echo '
    <a href="#" class="back-to-top text-center btn-scrolltop" onclick="$(\'body,html\').animate({scrollTop:0},500); return false">
        <i class="fa fa-angle-double-up"></i>
    </a>
';


?>




