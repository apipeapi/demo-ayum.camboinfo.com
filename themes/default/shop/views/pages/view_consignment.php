<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="page-contents api_padding_bottom_0_im api_padding_top_5_mobile">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                <div class="row">
                    <div class="col-sm-9 col-md-10">

                        <div class="api_height_15 hidden-sm hidden-xs"></div>                        
                        <div class="panel panel-default">
                            <div class="panel-heading text-bold">
<table width="100%" border="0">
<tr>
<td valign="top" width="60%">
    <?php
        echo '<i class="fa fa-list-alt margin-right-sm"></i>'.lang('Consignment_Detail').($inv ? ' ('.$inv->id.')' : ''); 
    ?>
</td>
<td valign="top"  width="40%">
    <?php 
        if ($this->loggedIn) 
            echo '<a href="'.shop_url('Consignment').'" class="pull-right"><i class="fa fa-share"></i> '.lang('Consignment').'</a>';
    ?>
</td>
</tr>
</table>
                            
                            </div>
                            <div class="col-md-12 text-bold api_padding_bottom_0" style="font-size: 18px; text-align: center; padding-top:10px; color:#f44336; text-transform:capitalize;">
                                <?= lang('Consignment_Received'); ?>
                            </div>
                            <div style="clear: both;"></div>
                            <div class="panel-body mprint">
                                <div class="text-center biller-header print" style="margin-bottom:20px;">
                                </div>

                                <div class="well well-sm">
                                    <div class="row bold">
                                        <div class="col-md-7 col-sm-7 col-xs-7">
                                            <p style="margin-bottom:0;">
                                                <?php
                                                    $temp = date_create($inv->date);
                                                    $temp = date_format($temp, 'd F Y');                                                        
                                                    echo '<b>'.lang("date").'</b>: '.$temp.'<br>';
                                                ?>                                                 
                                                <?php
                                                    if ($inv->reference_no != '')
                                                        echo '<b>'.lang("ref").'</b>: '.$inv->reference_no.'<br>';
                                                ?>
                                                <?php
                                                    echo '<b>'.lang("Status").'</b>: ';
                                                    if ($inv->sale_status != 'completed') 
                                                        echo lang('pending').'<br>';
                                                    else
                                                        echo lang('completed').'<br>';
                                                ?>
                                                <?php
                                                    echo '<b>'.lang("Delivery_Status").'</b>: ';
                                                    if ($inv->delivery_status != 'completed') 
                                                        echo lang('pending').'<br>';
                                                    else
                                                        echo lang('completed').'<br>';
                                                ?>                                                

                                                <?php
                                                    $temp = '';
                                                    if ($inv->delivery_date != '') {
                                                        $temp = date_create($inv->delivery_date);
                                                        $temp_2 = date_format($temp, 'd F Y');
                                                        $temp_3 = date_format($temp, 'H');
                                                        if ($temp_3 < 12) 
                                                            $temp_3 = '9:00 PM to 12:00 AM';
                                                        else
                                                            $temp_3 = '12:00 PM to 7:00 PM';
                                                        $temp = $temp_2.' ('.$temp_3.')';
                                                        echo '<br><b>'.lang("delivery_date").'</b>: '.$temp;
                                                    }                            
                                                ?>                                                
                                            </p>
                                        </div>
                                        
                                        <div class="col-md-5 col-sm-5 col-xs-5 visible-sm visible-xs text-center api_padding_top_0_im order_barcodes page_view_order_3">
                                            <?php
                                                if ($inv->reference_no != '')
                                                echo '
                                                    <img src="'.admin_url('misc/barcode/'.$this->sma->base64url_encode($inv->reference_no).'/code128/74/0/1').'" alt="'.$inv->reference_no.'" class="bcimg img-responsive" />
                                                    <div class="api_height_5"></div>
                                                ';
                                                echo $this->sma->qrcode('link', urlencode(shop_url('orders/' . $inv->id)), 2);
                                            ?>
                                        </div>

                                        <div class="col-md-5 col-sm-5 col-xs-5 hidden-sm hidden-xs text-right order_barcodes page_view_order_3">
                                            <?php
                                                if ($inv->reference_no != '')
                                                echo '
                                                    <img src="'.admin_url('misc/barcode/'.$this->sma->base64url_encode($inv->reference_no).'/code128/74/0/1').'" alt="'.$inv->reference_no.'" class="bcimg" />
                                                ';
                                                echo $this->sma->qrcode('link', urlencode(shop_url('orders/' . $inv->id)), 2);
                                            ?>
                                        </div>
                                                                                
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="row" style="margin-bottom:15px;">

                                    <div class="col-xs-6">

                                        <?php echo $this->lang->line("billing"); ?>:<br/>
                                        <h2 class="page_view_order_4" style="margin-top:10px;">
                                        <?php
                                                if ($customer->company != '' && $customer->company != '-' )
                                                    echo $customer->company;
                                                else
                                                    echo $customer->name;
                                        ?>                                            
                                        </h2>
                                        <?= $customer->company ? "" : "Attn: " . $customer->name ?>

                        <?php
                            if ($inv->parent_name != '')
                                echo '
                                    '.lang('Parent_Company').': '.$inv->parent_name.'<br>
                                ';
                        ?>

                                        <?php
                                        if ($customer->address)
                                            echo $customer->address . "<br>";
                                        if ($customer->city)
                                            echo $customer->city. " " . $customer->postal_code . " " . $customer->state . "<br>";
                                        if ($customer->country)
                                            $customer->country;

                                        if ($customer->vat_no != "-" && $customer->vat_no != "") {
                                            echo "<br>" . lang("vat_no") . ": " . $customer->vat_no;
                                        }
                                        if ($customer->cf1 != "-" && $customer->cf1 != "") {
                                            echo "<br>" . lang("ccf1") . ": " . $customer->cf1;
                                        }
                                        if ($customer->cf2 != "-" && $customer->cf2 != "") {
                                            echo "<br>" . lang("ccf2") . ": " . $customer->cf2;
                                        }
                                        if ($customer->cf3 != "-" && $customer->cf3 != "") {
                                            echo "<br>" . lang("ccf3") . ": " . $customer->cf3;
                                        }
                                        if ($customer->cf4 != "-" && $customer->cf4 != "") {
                                            echo "<br>" . lang("ccf4") . ": " . $customer->cf4;
                                        }
                                        if ($customer->cf5 != "-" && $customer->cf5 != "") {
                                            echo "<br>" . lang("ccf5") . ": " . $customer->cf5;
                                        }
                                        if ($customer->cf6 != "-" && $customer->cf6 != "") {
                                            echo "<br>" . lang("ccf6") . ": " . $customer->cf6;
                                        }

                                        echo "<br>".lang("tel") . ": " . $customer->phone . "<br>" . lang("email") . ": " . $customer->email;
                                        ?>
                                    </div>
                                    <?php if ($address) { ?>
                                    <div class="col-xs-6">
                                        <?php echo $this->lang->line("shipping"); ?>:
                                        <h2 class="page_view_order_4" style="margin-top:10px;"><?= $customer->company != '-' ? $customer->company : $customer->name; ?></h2>
                                        <?= $customer->company ? "" : "Attn: " . $customer->name ?>
                                        <p>
                                            <?= $address->line1; ?><br>
                                            <?= $address->line2; ?><br>
                                            <?= $address->city; ?> <?= $address->state; ?><br>
                                            <?= $address->postal_code; ?> <?= $address->country; ?><br>
                                            <?= lang('phone').': '.$address->phone; ?>
                                        </p>
                                    </div>
                                    <?php } ?>

                                </div>


<?php
echo '
<table class="visible-sm visible-xs table table-bordered table-hover table-striped print-table order-table api_margin_bottom_0" width="100%">
    <tr>
        <th>'.lang("no").'</th>
        <th>'.lang("description").'</th>
    </tr>
';
    $k = 1;
    foreach ($rows as $row) {
        if ($Settings->product_discount && $inv->product_discount != 0) {
            $temp_discount = '';
            if ($row->discount != 0 )
                $temp_discount = '<small>(' . $row->discount . ')</small> ';
            $temp_discount .= $this->sma->formatMoney($row->item_discount);            
        }
        echo '
        <tr>
            <td style="text-align:center; width:40px; vertical-align:top;">'.$k.'</td>
            <td style="vertical-align:middle;">
                '.$row->product_name.'
                <div class="api_margin_top_5">
                    <strong>'.lang("quantity").'</strong>: '.$this->sma->formatQuantity($row->unit_quantity).'
                </div>
                <div class="api_margin_top_5">
                    <strong>'.lang("unit_price").'</strong>: '.$this->sma->formatMoney($row->unit_price).'
                </div>
        ';
        if ($row->item_discount > 0)
        echo '
                <div class="api_margin_top_5">
                    <strong>'.lang("discount").'</strong>: '.$temp_discount.'
                </div>
        ';
        echo '
                <div class="api_margin_top_5">
                    <strong>'.lang("subtotal").'</strong>: '.$this->sma->formatMoney($row->subtotal).'
                </div>
            </td>
        </tr>
        ';
        $k++;
    }
    if ($return_sale)
        $temp2 = $inv->grand_total + $return_sale->grand_total;
    else
        $temp2 = $inv->grand_total;
        
    if ($inv->grand_total != $inv->total) {
        $temp3 .= '<span class="api_padding_right_20">'.lang("total").' ('.$default_currency->code.'):</span>';
        $temp4 = $inv->total + $inv->product_tax;        
        $temp3 = $temp3.$this->sma->formatMoney($temp4);
    }            

    if ($Settings->tax2 && $inv->order_tax != 0) {
        $temp_vat = '<span class="api_padding_right_20">'.lang("order_tax").' (' . $default_currency->code . '):</span>'.$this->sma->formatMoney($inv->order_tax);
    }
    if ($inv->shipping != 0) {
        $temp_shipping = '<span class="api_padding_right_20">'.lang("shipping").' (' . $default_currency->code . '):</span>'.$this->sma->formatMoney($inv->shipping);
    }

echo '
    <tr>
        <td colspan="2" style="text-align:right;">        
            '.$temp3.'
            <div class="api_height_10"></div>  
            '.$temp_vat.'
            <div class="api_height_5"></div>  
            '.$temp_shipping.'
            <div class="api_height_5"></div>  
            <strong>'.lang("total_amount").'</strong>
            <strong><span class="api_padding_right_20">('.$default_currency->code.'):</span>'.$this->sma->formatMoney($temp2).'</strong>
        </td>
    </tr>
</table>
';
?>
                                <div class="table-responsive hidden-sm hidden-xs">
                                    <table class="table table-bordered table-hover table-striped print-table order-table api_margin_bottom_0">

                                        <thead>

                                            <tr>
                                                <th><?= lang("no"); ?></th>
                                                <th><?= lang("description"); ?></th>
                                                <?php if ($Settings->indian_gst) { ?>
                                                    <th><?= lang("hsn_code"); ?></th>
                                                <?php } ?>
                                                <th><?= lang("quantity"); ?></th>
                                                <th style="text-align: right !important;"><?= lang("unit_price"); ?></th>
                                                <?php
                                                if ($Settings->tax1 && $inv->product_tax > 0) {
                                                    echo '<th>' . lang("tax") . '</th>';
                                                }
                                                if ($Settings->product_discount && $inv->product_discount != 0) {
                                                    echo '<th style="text-align: right !important;">' . lang("discount") . '</th>';
                                                }
                                                ?>
                                                <th style="text-align: right !important;"><?= lang("subtotal"); ?></th>
                                            </tr>

                                        </thead>

                                            <?php $r = 1;
                                            $tax_summary = array();
                                            foreach ($rows as $row):
                                                ?>
                                            <tr>
                                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                                <td style="vertical-align:middle;">
                                                    <?= $row->product_name ?>
                                                </td>
                                                <?php if ($Settings->indian_gst) { ?>
                                                <td style="width: 85px; text-align:center; vertical-align:middle;"><?= $row->hsn_code; ?></td>
                                                <?php } ?>
                                                <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->unit_quantity); ?></td>
                                                <td style="text-align:right; width:100px;"><?= $this->sma->formatMoney($row->unit_price); ?></td>
                                                <?php
                                                if ($Settings->tax1 && $inv->product_tax > 0) {
                                                    echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 && $row->tax_code ? '<small>('.$row->tax_code.')</small>' : '') . ' ' . $this->sma->formatMoney($row->item_tax) . '</td>';
                                                }
                                                if ($Settings->product_discount && $inv->product_discount != 0) {
                                                    echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($row->item_discount) . '</td>';
                                                }
                                                ?>
                                                <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($row->subtotal); ?></td>
                                            </tr>
                                            <?php
                                            $r++;
                                            endforeach;
                                            if ($return_rows) {
                                                echo '<tr class="warning"><td colspan="100%" class="no-border"><strong>'.lang('returned_items').'</strong></td></tr>';
                                                foreach ($return_rows as $row):
                                                    ?>
                                                <tr class="warning">
                                                    <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                                    <td style="vertical-align:middle;">
                                                        <?= $row->product_code.' - '.$row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                                        <?= $row->details ? '<br>' . $row->details : ''; ?>
                                                        <?= $row->serial_no ? '<br>' . $row->serial_no : ''; ?>
                                                    </td>
                                                    <?php if ($Settings->indian_gst) { ?>
                                                    <td style="width: 85px; text-align:center; vertical-align:middle;"><?= $row->hsn_code; ?></td>
                                                    <?php } ?>
                                                    <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity).' '.$row->product_unit_code; ?></td>
                                                    <td style="text-align:right; width:100px;"><?= $this->sma->formatMoney($row->real_unit_price); ?></td>
                                                    <?php
                                                    if ($Settings->tax1 && $inv->product_tax > 0) {
                                                        echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 && $row->tax_code ? '<small>('.$row->tax_code.')</small>' : '') . ' ' . $this->sma->formatMoney($row->item_tax) . '</td>';
                                                    }
                                                    if ($Settings->product_discount && $inv->product_discount != 0) {
                                                        echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($row->item_discount) . '</td>';
                                                    }
                                                    ?>
                                                    <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($row->subtotal); ?></td>
                                                </tr>
                                                <?php
                                                $r++;
                                                endforeach;
                                            }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <?php
                                            $col = $Settings->indian_gst ? 5 : 4;
                                            if ($Settings->product_discount && $inv->product_discount != 0) {
                                                $col++;
                                            }
                                            if ($Settings->tax1 && $inv->product_tax > 0) {
                                                $col++;
                                            }
                                            if ($Settings->product_discount && $inv->product_discount != 0 && $Settings->tax1 && $inv->product_tax > 0) {
                                                $tcol = $col - 2;
                                            } elseif ($Settings->product_discount && $inv->product_discount != 0) {
                                                $tcol = $col - 1;
                                            } elseif ($Settings->tax1 && $inv->product_tax > 0) {
                                                $tcol = $col - 1;
                                            } else {
                                                $tcol = $col;
                                            }
                                            ?>
                                            <?php if ($inv->grand_total != $inv->total) { ?>
                                            <tr>
                                                <td colspan="<?= $tcol; ?>"
                                                    style="text-align:right; padding-right:10px;"><?= lang("total"); ?>
                                                    (<?= $default_currency->code; ?>)
                                                </td>
                                                <?php
                                                if ($Settings->tax1 && $inv->product_tax > 0) {
                                                    echo '<td style="text-align:right;">' . $this->sma->formatMoney($return_sale ? ($inv->product_tax+$return_sale->product_tax) : $inv->product_tax) . '</td>';
                                                }
                                                if ($Settings->product_discount && $inv->product_discount != 0) {
                                                    echo '<td style="text-align:right;">' . $this->sma->formatMoney($return_sale ? ($inv->product_discount+$return_sale->product_discount) : $inv->product_discount) . '</td>';
                                                }
                                                ?>
                                                <td style="text-align:right; padding-right:10px;"><?= $this->sma->formatMoney($return_sale ? (($inv->total + $inv->product_tax)+($return_sale->total + $return_sale->product_tax)) : ($inv->total + $inv->product_tax)); ?></td>
                                            </tr>
                                            <?php } ?>
                                            <?php if ($Settings->indian_gst) {
                                                if ($inv->cgst > 0) {
                                                    $cgst = $return_sale ? $inv->cgst + $return_sale->cgst : $inv->cgst;
                                                    echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px; font-weight:bold;">' . lang('cgst') . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px; font-weight:bold;">' . ( $Settings->format_gst ? $this->sma->formatMoney($cgst) : $cgst) . '</td></tr>';
                                                }
                                                if ($inv->sgst > 0) {
                                                    $sgst = $return_sale ? $inv->sgst + $return_sale->sgst : $inv->sgst;
                                                    echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px; font-weight:bold;">' . lang('sgst') . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px; font-weight:bold;">' . ( $Settings->format_gst ? $this->sma->formatMoney($sgst) : $sgst) . '</td></tr>';
                                                }
                                                if ($inv->igst > 0) {
                                                    $igst = $return_sale ? $inv->igst + $return_sale->igst : $inv->igst;
                                                    echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px; font-weight:bold;">' . lang('igst') . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px; font-weight:bold;">' . ( $Settings->format_gst ? $this->sma->formatMoney($igst) : $igst) . '</td></tr>';
                                                }
                                            } ?>
                                            <?php
                                            if ($return_sale) {
                                                echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("return_total") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_sale->grand_total) . '</td></tr>';
                                            }
                                            if ($inv->surcharge != 0) {
                                                echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("return_surcharge") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($inv->surcharge) . '</td></tr>';
                                            }
                                            ?>
                                            <?php if ($inv->order_discount != 0) {
                                                echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("order_discount") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">'.($inv->order_discount_id ? '<small>('.$inv->order_discount_id.')</small> ' : '') . $this->sma->formatMoney($return_sale ? ($inv->order_discount+$return_sale->order_discount) : $inv->order_discount) . '</td></tr>';
                                            }
                                            ?>
                                            <?php if ($Settings->tax2 && $inv->order_tax != 0) {
                                                echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;">' . lang("order_tax") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_sale ? ($inv->order_tax+$return_sale->order_tax) : $inv->order_tax) . '</td></tr>';
                                            }
                                            ?>
                                            <?php if ($inv->shipping != 0) {
                                                echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("shipping") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($inv->shipping) . '</td></tr>';
                                            }
                                            ?>
                                            <tr>
                                                <td colspan="<?= $col; ?>"
                                                    style="text-align:right; font-weight:bold;"><?= lang("total_amount"); ?>
                                                    (<?= $default_currency->code; ?>)
                                                </td>
                                                <td style="text-align:right; padding-right:10px; font-weight:bold;">
<?= $this->sma->formatMoney($inv->grand_total); ?></td>
                                            </tr>
<!--                                            
                                            <tr>
                                                <td colspan="<?//= $col; ?>"
                                                    style="text-align:right; font-weight:bold;"><?//= lang("paid"); ?>
                                                    (<?//= $default_currency->code; ?>)
                                                </td>
                                                <td style="text-align:right; font-weight:bold;"><?//= $this->sma->formatMoney($return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid); ?></td>
                                            </tr>
                                            <tr>
                                                <td colspan="<?//= $col; ?>"
                                                    style="text-align:right; font-weight:bold;"><?//= lang("balance"); ?>
                                                    (<?//= $default_currency->code; ?>)
                                                </td>
                                                <td style="text-align:right; font-weight:bold;"><?//= $this->sma->formatMoney(($return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total) - ($return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid)); ?></td>
                                            </tr>
-->
                                        </tfoot>
                                    </table>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <?php
                                        if ($inv->note || $inv->note != "") { ?>

                                        <br>
                                        <div class="well well-sm" style="margin-bottom:0;">
                                            <p class="bold"><?= lang("note"); ?>:</p>
                                            <div>
                                                <?php
                                                    $temp_note = str_replace('\r', '', $inv->note);              
                                                    $temp_note = str_replace('\n', '<br>', $temp_note); 
                                                    echo $this->sma->decode_html($temp_note); 
                                                ?>                                                    
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>

                                    <?php if ($customer->award_points != 0 && $Settings->each_spent > 0) { ?>
                                    <div class="col-xs-5 pull-left">
                                        <div class="well well-sm" style="margin-bottom:0;">
                                            <?=
                                            '<p>'.lang('this_sale').': '.floor(($inv->grand_total/$Settings->each_spent)*$Settings->ca_point)
                                            .'<br>'.
                                            lang('total').' '.lang('award_points').': '. $customer->award_points . '</p>';?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php
                                if ($inv->grand_total > $inv->paid && !$inv->attachment) {
                                    /*
                                    echo '<div class="no-print well well-sm" style="margin:20px 0 0 0;">';
                                    if (!empty($shop_settings->bank_details)) {
                                        echo '<div class="text-center">';
                                        echo $shop_settings->bank_details;
                                        echo shop_form_open_multipart('manual_payment/'.$inv->id);
                                        echo '<input type="file" name="payment_receipt" id="file" class="file" />';
                                        echo '<label for="file" class="btn btn-default"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>'.lang('select_file').'&hellip;</span></label>';
                                        echo '<span id="submit-container">'.form_submit('upload', lang('upload'), 'id="upload-file" class="btn btn-theme"').'</span>';
                                        echo form_close();
                                        echo '</div><hr class="divider or">';
                                    }
                                    $btn_code = '<div id="payment_buttons" class="text-center margin010">';
                                    if ($paypal->active == "1" && $inv->grand_total != "0.00") {
                                        $btn_code .= '<a href="'.site_url('pay/paypal/'.$inv->id).'">=<img src="' . base_url('assets/images/btn-paypal.png') . '" alt="Pay by PayPal"></a> ';
                                    }
                                    if ($skrill->active == "1" && $inv->grand_total != "0.00") {
                                        $btn_code .= ' <a href="'.site_url('pay/skrill/'.$inv->id).'"><img src="' . base_url('assets/images/btn-skrill.png') . '" alt="Pay by Skrill"></a>';
                                    }
                                    $btn_code .= '<div class="clearfix"></div></div>';
                                    echo $btn_code;
                                    echo '</div>';
                                    */
                                }
                                if ($inv->payment_status != 'paid' && $inv->attachment) {
                                    echo '<div class="alert alert-info" style="margin-top:15px;">'.lang('payment_under_review').'</div>';
                                }
                                ?>


<?php
//$payments = array();

$payments = array();
if (count($payments) > 0) { 
$temp_padding = 'api_padding_left_10 api_padding_right_10';



$api_bank_in_form .= '
    <div class="api_height_20"></div>
    <fieldset class="scheduler-border">
        <legend class="scheduler-border">'.lang('payments','payments').'</legend>
';


$api_bank_in_form .= '
<div class="table-responsive">
        <table width="100%" class="table table-striped table-hover table-va-middle" border="0">
        <thead>
        <tr>
        <th>'.lang('date').'</th>
        <th>'.lang('payment_method').'</th>
        <th style="text-align:right !important;">'.lang('amount').'</th>
        <th style="text-align:right !important;">'.lang('reference_number').'</th>
        <th style="text-align:center !important; width:150px;">'.lang('payment_status').'</th>
        </tr>
        </thead>

            <tbody>
';

for ($i=0;$i<count($payments);$i++) {
    $temp = date_create($payments[$i]['date']);
    $temp = date_format($temp, 'd F Y');
    if ($payments[$i]['amount'] > 0) {
        $temp1 = 'paid';
        $temp2 = 'label-success';
    }
    else {
        $temp1 = 'pending';
        $temp2 = 'label-warning';        
    }
    if ($payments[$i]['paid_by'] == 'cash')
        $temp3 = lang('cod');
    else
        $temp3 = lang($payments[$i]['paid_by']);

    if ($payments[$i]['paid_by'] == 'aba' || $payments[$i]['paid_by'] == 'acleda') {
        $temp4 = $payments[$i]['transfer_amount'];
        $temp5 = $payments[$i]['transfer_reference_number'];
    }
    else {
        $temp4 = $payments[$i]['amount'];
        if ($payments[$i]['paid_by'] != 'paypal')
            $temp5 = $payments[$i]['reference_no'];
        else
            $temp5 = $payments[$i]['transaction_id'];
    } 
        
    $api_bank_in_form .= '
        <tr class="product">
            <td>'.$temp.'</td>                
            <td>'.$temp3.'</td>                
            <td align="right">'.$this->sma->formatMoney($temp4).'</td>                
            <td align="right">'.$temp5.'</td>
            <td align="center">
                <span class="label '.$temp2.'">'.$temp1.'</span>
            </td>            
        </tr>
    ';
}

$api_bank_in_form .= '
    </tbody></table>
</div>
';

echo $api_bank_in_form;
} 

?>                                                


                            </div>


                            
                        </div>
                        
                        
                    </div>

                    <div class="col-sm-3 col-md-2">
                        <?php include('sidebar2.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
    include('wing_message.php');
?>

<style>
.page_view_order_1, .page_view_order_2{
    padding-left:0px;
    padding-right:0px;
}
@media screen and (min-width: 100px) and (max-width:600px) {
.page_view_order_2{
    padding-top:10px;
    float:left;
}
.page_view_order_3{
    padding-top:10px;
    float:left;
}
.page_view_order_4{
    font-size:20px !important;
}
}

fieldset.scheduler-border {
    border: 1px groove #ddd !important;
    padding: 10px !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
}

fieldset.scheduler-border {
    border: 1px groove #ddd !important;
    padding: 10px !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow: 0px 0px 0px 0px #000;
    box-shadow: 0px 0px 0px 0px #000;
}
legend.scheduler-border {
    font-size: 1.1em !important;
    font-weight: bold !important;
    text-align: left !important;
    width: auto;
    color: #f44336;
    padding: 5px 15px 0px 15px;
    border: 1px groove #ddd !important;
    margin: 0;
    /* background: #DBDEE0; */
}
.api_checkout_button_fix{
    display:none !important;
}
</style>



<?php


?>



