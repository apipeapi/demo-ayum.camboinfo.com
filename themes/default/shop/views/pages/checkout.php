<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php
$shipping = $this->sma->convertMoney($this->cart->shipping(), false, false);
$order_tax = $this->sma->convertMoney($this->cart->order_tax(), false, false);

$total = $this->cart->total();
$total = $this->sma->convertMoney($total, false, false);

if ($customer->vat_no) {
    if ($customer->vat_invoice_type != 'do') {
        $order_tax = ($total * 10) / 100;
    } else {
        $order_tax = 0;
    }
    $order_tax = $this->sma->convertMoney($order_tax, false, false);
}

$grand_total_label = $this->sma->formatMoney(($this->sma->formatDecimal($total)+$this->sma->formatDecimal($order_tax)+$this->sma->formatDecimal($shipping)), $selected_currency->symbol);


if ($this->session->userdata('sale_consignment_auto_insert') != 'yes') {
    echo shop_form_open('order', 'class="validate" name="frm_order" id="frm_order"');
} else {
    echo shop_form_open('order_consignment', 'class="validate" name="frm_order" id="frm_order"');
}
?>

<section class="page-contents api_padding_bottom_0_im api_padding_top_0_im">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">

                    <div class="col-sm-8">
                        <div class="panel panel-default margin-top-lg"> 
                            <div class="panel-heading text-bold">
                                <i class="fa fa-shopping-cart margin-right-sm" style="margin-left:-8px"></i> <?= lang('checkout'); ?>
                                <a href="<?= site_url('cart'); ?>" class="pull-right"style="margin-left:3px">
                                    <i class="fa fa-share"></i>
                                    <?= lang('back_to_cart'); ?>
                                </a>
                            </div>
                            <div class="panel-body">

                                <div>
                                <?php
                                if (!$this->loggedIn) {
                                    ?>
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#user" aria-controls="user" role="tab" data-toggle="tab"><?= lang('returning_user'); ?></a></li>
                                        <li role="presentation api_display_none"><a href="#guest" aria-controls="guest" role="tab" data-toggle="tab"><?= lang('guest_checkout'); ?></a></li>
                                    </ul>
                                    <?php
                                }
                                ?>

                                    <div class="tab-content padding-lg api_padding_0_im">
                                        <div role="tabpanel" class="tab-pane fade in active" id="user">
                                            <?php
                                            if ($this->loggedIn) {

if ($this->api_shop_setting[0]['product_import_date'] == 1 && base_url() == 'https://air.phsarjapan.com/') {
    if ($this->session->userdata('api_selected_import_date') != '') {
        $temp2 = explode('<api>',$this->session->userdata('api_selected_import_date'));

        $date1 = date_create($temp2[0]);
        $date2 = date_create($temp2[1]);    
        echo '
            <input type="hidden" name="add_ons_product_import_date" id="add_ons_product_import_date" value="'.date_format($date1, 'jS, F Y').' - '.date_format($date2, 'jS, F Y').'" />
            <label class="control-label" for="delivery_date">
                <strong style="color: #f44336 !important;">'.lang('Import_Date').'</strong>
            </label>
            <div>
            '.date_format($date1, 'jS, F Y').' - '.date_format($date2, 'jS, F Y').'
            </div>
            <br>
        ';
    }
}
else
    echo '
        <input type="hidden" name="add_ons_product_import_date" id="add_ons_product_import_date" value="" />
    ';

                                                $temp_address = $customer->address;
                                                if ($customer->city != '') {
                                                    $temp_address .= ', '.$customer->city;
                                                }
                                                if ($customer->state != '') {
                                                    $temp_address .= ', '.$customer->state;
                                                }
                                                if ($customer->country != '') {
                                                    $temp_address .= ', '.$customer->country;
                                                }
                                                if ($customer->postal_code != '') {
                                                    $temp_address .= '<br>'.lang('postal_code').': '.$customer->postal_code;
                                                }
                                                $temp_script = '
    var temp_array = [0];
';
                                                if ($this->api_shop_setting[0]['display_company'] != 1) {
                                                    for ($i=0;$i<count($select_company);$i++) {
                                                        $temp_name = $select_company[$i]['company'];
                                                        if ($select_company[$i]['company'] == '' || $select_company[$i]['company'] == '-') {
                                                            $temp_name = $select_company[$i]['name'];
                                                        }
                                                        break;
                                                    }
                                                    echo '
        <label class="control-label" for="delivery_date">
            <strong style="color: #f44336 !important;">'.lang('name').'</strong>
        </label>
        <div>
            '.$temp_name.'
        </div>
        <br>    
    ';
                                                    $temp_display_2 = 'api_display_none';
                                                } else {
                                                    $temp_display_2 = '';
                                                }
                                                echo '
        <label class="control-label '.$temp_display_2.'" for="delivery_date">
            <strong style="color: #f44336 !important;">'.lang('Company').'</strong>
        </label>
        <div class="form-group '.$temp_display_2.'">
            <div class="controls"> 
    ';
                                                $temp_branch_name = '';
                                                $temp_address = $select_company[0]['address'];
                                                for ($i=0;$i<count($select_company);$i++) {
                                                    if ($select_company[$i]['company'] == '' || $select_company[$i]['company'] == '-') {
                                                        $select_company[$i]['company'] = $select_company[$i]['name'];
                                                    }
                                                    $tr2[$select_company[$i]['id']] = $temp.' '.$select_company[$i]['company'];

                                                    $select_company[$i]['address'] = nl2br($select_company[$i]['address']);
                                                    $select_company[$i]['address'] = preg_replace("/\r|\n/", "", $select_company[$i]['address']);

                                                    $select_company[$i]['address'] = str_replace('<br>', ' ', $select_company[$i]['address']);
                                                    $select_company[$i]['address'] = str_replace('<br/>', ' ', $select_company[$i]['address']);
                                                    $select_company[$i]['address'] = str_replace('<br />', ' ', $select_company[$i]['address']);

                                                    $temp_script .= 'temp_array['.$select_company[$i]['id'].'] = "'.$select_company[$i]['address'].'";';
                                                    if ($select_company[$i]['id'] == $user[0]['company_id']) {
                                                        $temp_branch_name = $select_company[$i]['company'];
                                                        $temp_address = $select_company[$i]['address'];
                                                    }
                                                }
                                                echo form_dropdown('customer_id', $tr2, $user[0]['company_id'], 'data-placeholder="'.lang("Please_select_a_customer").'" id="customer_id" class="form-control" onchange="api_set_checkout_address(this.value);" style="width:280px;"');
                                                echo '
            </div>
        </div>    
    ';


                                                if (is_array($select_company_branch)) {
                                                    if (count($select_company_branch) > 0) {
                                                        echo '
            <label class="control-label" for="delivery_date">
                <strong style="color: #f44336 !important;">'.lang('Branch').'</strong>
            </label>
            <div class="form-group">
                <div class="controls"> 
        ';
                                                        $temp_branch_name = '';
                                                        $temp_address = $select_company_branch[0]['address'];
                                                        for ($i=0;$i<count($select_company_branch);$i++) {
                                                            if ($select_company_branch[$i]['id'] != '') {
                                                                $tr2[$select_company_branch[$i]['id']] = $select_company_branch[$i]['title'];
                                                                $temp_script .= 'temp_array['.$select_company_branch[$i]['id'].'] = "'.$select_company_branch[$i]['address'].'";';
                                                                if ($select_company_branch[$i]['id'] == $user[0]['company_branch']) {
                                                                    $temp_branch_name = $select_company_branch[$i]['title'];
                                                                    $temp_address = $select_company_branch[$i]['address'];
                                                                }
                                                            }
                                                        }

                                                        if ($user[0]['company_branch_type'] != 'purchaser' || $user[0]['company_branch'] == '') {
                                                            echo form_dropdown('company_branch', $tr2, $user[0]['company_branch'], 'data-placeholder="'.lang("Please_select_a_customer").'" id="company_branch" class="form-control" onchange="api_set_checkout_address(this.value);" style="width:280px;"');
                                                        } else {
                                                            echo $temp_branch_name;
                                                            echo '<input type="hidden" name="company_branch" value="'.$user[0]['company_branch'].'">';
                                                        }
                                                        echo '
                </div>
            </div>    
        ';
                                                    }
                                                }


echo '
<div class="api_height_15"></div>
'; 


?>

<?php
if ($this->loggedIn) {
    $config_data = array(
        'id' => $customer->city_id,
        'table_name' => 'sma_city',
        'field_name' => 'title_en',
        'translate' => 'yes',
        'seperate' => ', ',
        'reverse' => '',
    );
    $temp_10 = $this->site->api_display_category($config_data);    
    $config_data = array(
        'table_name' => 'sma_city',
        'select_table' => 'sma_city',
        'translate' => 'yes',
        'select_condition' => "id = ".$customer->city_id,
    );
    $select_city = $this->site->api_select_data_v2($config_data);
    $temp_default_address = '
        <div class="col-sm-12">
            <div class="checkbox bg">
                <label>
                    <input type="radio" name="address" value="" checked>
                    <span class="api_border_red" id="default_delivery_address" onclick="var postData = {
                            \'field_id\' : \''.$customer->city_id.'\',
                            };
                            ajax_getDeliveryAddress(postData);">
                        <span class="api_font_size_18">'.lang('default_delivery_address').'</span><br>
                        '.lang('Address').' : '.$customer->address.$temp_10['display'].'<br>
                        '.lang('phone').' : '.$customer->phone.'<br>
                        '.lang('delivery_fee').' : '.$this->sma->formatMoney($select_city[0]['delivery_fee']).'<br>
                        <span class="api_display_none">'.lang('discount').' : '.$select_city[0]['address_discount'].'%</span>
                    </span>
                </label>
            </div>
        </div>    
    ';
    
    if (!empty($addresses)) {
        echo shop_form_open('order', 'class="validate" name="frm_order" id="frm_order"');
        echo '<div class="row">';
        echo '<div class="col-sm-12 text-bold" style="color: #f44336 !important;">'.lang('select_address').'</div>';
        echo $temp_default_address;

        $r = 1;
        foreach ($addresses as $address) {
            ?>
            <div class="col-sm-12">
                <div class="checkbox bg">
                    <label>
                        <input type="radio" name="address" value="<?= $address->id; ?>">
                        <span class="api_border_red" onclick="var postData = {
                                    'field_id' : '<?= $address->city_id;?>',
                                    };
                                    ajax_getDeliveryAddress(postData);">
                            <?php
                                $config_data = array(
                                    'id' => $address->city_id,
                                    'table_name' => 'sma_city',
                                    'field_name' => 'title_en',
                                    'translate' => 'yes',
                                    'seperate' => ', ',
                                    'reverse' => '',
                                );
                                $temp_10 = $this->site->api_display_category($config_data);
                                
                                $config_data = array(
                                    'table_name' => 'sma_addresses',
                                    'select_table' => 'sma_addresses',
                                    'translate' => '',
                                    'select_condition' => "id = ".$address->id." and company_id = ".$this->session->userdata('company_id'),
                                );
                                $address_v2 = $this->site->api_select_data_v2($config_data);
                                for($i=0;$i<count($address_v2);$i++) {
                                    $a = $address_v2[$i]['delivery_address'];
                                }
                                
                                echo '
                                    <span class="api_font_size_18">
                                        '.lang('Delivery_Address').' '.$r.'
                                    </span>
                                    <br>
                                    '.lang('Address').' : '.$a.$address->line1.$temp_10['display'].'
                                    <br>
                                ';

                                echo lang('phone').': '.$address->phone.'<br>';
                                $config_data = array(
                                    'table_name' => 'sma_city',
                                    'select_table' => 'sma_city',
                                    'translate' => 'yes',
                                    'select_condition' => "id = ".$address->city_id,
                                );
                                $select_city2 = $this->site->api_select_data_v2($config_data);
                                for($j=0;$j<count($select_city2);$j++) {
                                    echo lang('delivery_fee').' : '.$this->sma->formatMoney($select_city2[$j]['delivery_fee']).'<br>';
                                    echo '<span class="api_display_none">'.lang('discount').' : '.$select_city2[$j]['address_discount'].'%</span>';
                                }
                            ?>

                        </span>
                    </label>
                </div>
            </div>
            <?php
            $r++;
        }
        echo '</div>';
    }
    if (count($addresses) < 6 && !$this->Staff) {
        echo '<div class="row margin-bottom-lg">';
        // echo '<div class="col-sm-12"><a href="#" id="add-address" class="btn btn-primary btn-sm">'.lang('add_new_address').'</a></div>';
        echo '<div class="col-sm-12"><button class="btn btn-primary btn-sm" onclick="add_delivery_address();">'.lang('add_address').'</button></div>';
        echo '</div>';
    }
    if (isset($istates))
        echo '
            <script>
                var istates = '.json_encode($istates).'
            </script>
        ';
    else
        echo '
            <script>
                var istates = false; 
            </script>
        ';
}

echo '
    <div class="api_height_15"></div>
';
?>

                    <div class="form-group api_display_none">
                        <label class="control-label" for="delivery_date">
            			<strong style="color: #f44336 !important;"><?php echo lang("delivery_date"); ?></strong>
                        </label>
                        <div class="controls"> 
            				<?php
                                

                                if (date('w') != 0) {
                                    if (date('w') == 6) {
                                        $temp = lang('Next_Monday');
                                    } else {
                                        $temp = lang('Tomorrow');
                                    }
                                    
                                    if (date('H') < 18) {
                                        $tr['1'] = lang('Today').' 12:00 PM to 7:00 PM';
                                        $tr['2'] = $temp.' 9:00 AM to 12:00 PM';
                                        $tr['3'] = $temp.' 12:00 PM to 7:00 PM';
                                        $temp_selected = 1;
                                    } else {
                                        $tr['2'] = $temp.' 9:00 AM to 12:00 PM';
                                        $tr['3'] = $temp.' 12:00 PM to 7:00 PM';
                                        $temp_selected = 2;
                                    }
                                } else {
                                    $temp = lang('Tomorrow');
                                    $tr['2'] = $temp.' 9:00 AM to 12:00 PM';
                                    $tr['3'] = $temp.' 12:00 PM to 7:00 PM';
                                    $temp_selected = 2;
                                }
                                    
                                                echo form_dropdown('delivery_date', $tr, $temp_selected, 'data-placeholder="'.lang("Available_delivery_date").'" id="delivery_date" class="form-control" style="width:280px;"'); ?>
                        </div>
                    </div>
                    
                                                <?php

$temp_padding = 'api_padding_left_10 api_padding_right_10';

 

?>                                                

                                                <div><strong><?= lang('payment_method'); ?></strong></div>
                                                <input type="hidden" name="payment_method" id="payment_method" value="" id="" required="required">
                                                
                                                <?php

echo '
<div class="api_height_10"></div>
';

echo '
<table width="100%">
<tr>
<td align="center">   
';

if ($customer->payment_category != '' && $customer->payment_category != 'cash on delivery' && $this->api_shop_setting[0]['air_base_url'] != base_url())
    $temp = '';
else
    $temp = 'api_display_none';
echo '
<div class="api_payment_box '.$temp.'" title="Account Payable">
<table class="api_payment_box_table" id="api_payment_box_table_account_receivable" onclick="api_select_payment(\'account_receivable\');" width="100" height="88" border="0">
<tr>
<td valign="middle" align="center">
    <div class="api_padding_10">
        <strong style="color:#555555">Account<br />Payable<br /><span class="api_text_transform_capitalize">'.$customer->payment_category.'</span></strong>
    </div>
</td>
</tr>
</table>
</div>
';

if ($this->api_shop_setting[0]['air_base_url'] != base_url())
    $temp = '';
else
    $temp = 'api_display_none';
echo '
<div class="api_payment_box '.$temp.'" title="Cash on delivery">
<table class="api_payment_box_table" id="api_payment_box_table_cod" onclick="api_bank_in_form_action(\'hide\'); api_select_payment(\'cod\');" width="100" height="88" border="0">
<tr>
<td valign="middle" align="center">
    <img class="img-responsive" src="'.base_url().'assets/api/image/cod.png"/>
    </div>
</td>
</tr>
</table>
</div>
';

echo '
<div class="api_payment_box " title="ABA Bank Transfer">
<table class="api_payment_box_table" id="api_payment_box_table_aba" onclick="api_bank_in_form_action(\'show\');  api_select_payment(\'aba\');" width="100" height="88" border="0">
<tr>
<td valign="middle" align="center">
    <div class="api_padding_10">
    <img  src="'.base_url().'assets/api/image/aba.jpg" height="66"/>
    </div>
</td>
</tr>
</table>
</div>
';

if ((is_int(strpos($_SERVER["HTTP_HOST"],"localhost")) || base_url() == "https://phsarjapan.com/" || base_url() == "https://air.phsarjapan.com/") && $customer->sale_consignment_auto_insert != 'yes')
    $temp = '';
else
    $temp = 'api_display_none';
echo '
<div class="api_payment_box '.$temp.'" title="Pipay">
<table class="api_payment_box_table" id="api_payment_box_table_pipay" onclick="api_select_payment(\'pipay\');" width="100" height="88" border="0">
<tr>
<td valign="middle" align="center">
    <div class="api_padding_10">
    <img class="img-responsive" src="'.base_url().'assets/api/image/pipay_gateway_logo.png"/>
    </div>
</td>
</tr>
</table>
</div>
';

if ((is_int(strpos($_SERVER["HTTP_HOST"],"localhost")) || base_url() == "https://phsarjapan.com/" || base_url() == "https://air.phsarjapan.com/"))
    $temp = '';
else
    $temp = 'api_display_none';    
echo '
<div class="api_payment_box '.$temp.'" title="Wing">
<table class="api_payment_box_table" id="api_payment_box_table_wing" onclick="api_select_payment(\'wing\');" width="100" height="88" border="0">
<tr>
<td valign="middle" align="center">
    <div class="api_padding_10">
    <img class="img-responsive" src="'.base_url().'assets/api/image/wing_gateway_logo.png"/>
    </div>
</td>
</tr>
</table>
</div>
';

echo '
    <div class="api_clear_both"></div>
';


if ($customer->sale_consignment_auto_insert != 'yes')
    $temp = '';
else
    $temp = 'api_display_none';
    
if ($this->api_shop_setting[0]['air_base_url'] == base_url()){
    //if ($this->session->userdata('user_id') == 37 && $customer->sale_consignment_auto_insert != 'yes')
    if ($customer->sale_consignment_auto_insert != 'yes')
        $temp = '';
    else
        $temp = 'api_display_none';
}

$temp = 'api_display_none';
echo '
<div class="api_payment_box '.$temp.'" title="Credit/Debit Card" style="width:auto;">
<table class="api_payment_box_table" id="api_payment_box_table_payway_credit" onclick="api_select_payment(\'payway_credit\');"  height="88" width="280" border="0">
<tr>
<td class="api_padding_10" valign="middle" align="center">
    <img class="credit_card_image_1" src="'.base_url().'assets/images/payway_generic.png"  />
</td>
<td class="api_padding_right_10" valign="middle" align="left">
    <div class="payment_credit_card">Credit/Debit Card</div>
    <div class="payment_credit_card_2">VISA, Mastercard, UnionPay</div>
</td>
</tr>
</table>
</div>
';
echo '
<div class="api_payment_box '.$temp.'" title="ABA PAY" style="width:auto;">
<table class="api_payment_box_table" id="api_payment_box_table_payway_aba_pay" onclick="api_select_payment(\'payway_aba_pay\');" width="290" height="88" border="0">
<tr>
<td class="api_padding_10" valign="middle" align="center">
    <img class="credit_card_image_1" src="'.base_url().'assets/images/payway_pay.png"  />
</td>
<td class="api_padding_right_10" valign="middle" align="left">
    <div class="payment_credit_card">ABA PAY</div>
    <div class="payment_credit_card_2">Scan to pay with ABA Mobile</div>
</td>
</tr>
</table>
</div>
';




echo '
<div class="api_height_5 api_clear_both"></div>
</td>
</tr>
</table>
';

?>
<?php 
$api_bank_in_form = '
<div id="api_bank_in_form">
';

$api_bank_in_form .= '
    <div class="api_height_20"></div>
    <fieldset class="scheduler-border">
        <legend class="scheduler-border">'.lang('ABA_Payment').'</legend>
        <div class="col-md-12 '.$temp_padding.'">
            <div class="api_height_10"></div>
'.lang('Only_ABA_bank_is_available').'. '.lang('first_you_have_to_transfer').' <span id="grand_total_label" style="font-weight: bold;"><b>'.$grand_total_label.'</b></span>
'.lang('to_our_bank_account').'.
<div class="api_height_10"></div> 
    <div class="col-md-6 api_display_none">
        <ul>
            <li>                
                ABA account number : <b>000924240</b>
            </li>                
            <li>                
                ABA account name : <b>DONMARU by S.SENG</b><br>
            </li>  
        </ul>
    </div>
    <div class="col-md-12" align="center"><img src="'.base_url().'assets/api/image/aba_qr_code.png" style="width: 250px;"></div><br>
    <div class="col-md-12" style="margin-bottom: 10px;padding-top: 10px;">
        '.lang('then_fill_form').'
    </div>
            <div class="api_height_30"></div>
</div>
';


$api_bank_in_form .= '        
    <div class="col-md-6 api_display_none '.$temp_padding.'">                                 
    <div class="form-group">
        '.lang("your_bank", "your_bank").'
';
        $tr22['aba'] = 'ABA Bank';
        $api_bank_in_form .= form_dropdown('your_bank', $tr22, '', 'class="form-control select" id="your_bank" onchange="api_your_bank_change(this.value);" style="width:100%;"');                
$api_bank_in_form .= '
    </div>
    </div>
';

$api_bank_in_form .= '   
    <div class="col-md-6 api_display_none '.$temp_padding.'">                                 
    <div class="form-group">
        '.lang('transfer_amount', 'transfer_amount').'
        '.form_input('transfer_amount', $grand_total_label, 'class="form-control" id="transfer_amount" readonly="readonly"').'
    </div>
    </div>
';

$api_bank_in_form .= '   
    <div class="col-md-6 '.$temp_padding.'">                                 
    <div class="form-group">
        '.lang('Your_ABA_bank_account_number', 'Your_ABA_bank_account_number').'
        '.form_input('your_bank_account_number', '', 'class="form-control" id="your_bank_account_number"').'
    </div>
    </div>
';

$api_bank_in_form .= '   
    <div class="col-md-6 api_display_none '.$temp_padding.'">                                 
    <div class="form-group">
        '.lang('our_company_bank_account_number', 'our_company_bank_account_number').'
        '.form_input('our_company_bank_account_number', '000358355', 'class="form-control" id="our_company_bank_account_number" readonly="readonly"').'
    </div>
    </div>
';

$api_bank_in_form .= '                                    
    <div class="col-md-6 '.$temp_padding.'">                                 
    <div class="form-group">
        '.lang('ABA_Transfer_Reference_#', 'ABA_Transfer_Reference_#').'
        '.form_input('transfer_reference_number', '', 'class="form-control" id="transfer_reference_number"').'
    </div>
    </div>
';

$api_bank_in_form .= '
    <div class="api_height_15 api_clear_both"></div>
</div>
';

echo $api_bank_in_form;
?>
                                                <div class="form-group">
                                                    <?= lang('comment_any', 'comment'); ?>
                                                    <?= form_textarea('comment', set_value('comment'), 'class="form-control" id="comment" style="height:100px;"'); ?>
                                                </div>
                                                <?php
if (!$this->Staff) {
    echo '
        <div class="col-md-12 api_padding_0">
            <label class=" label-warning  api_padding_5 api_color_white">'.lang('Please click Submit Order button to complete your order').'</label>
        </div>
        <div class="api_height_30 api_clear_both"></div>
    ';
    echo '
        <div class="btn btn-primary btn-group-payment btn-group-payment-o btn-checkout" onclick="api_checkout();" style="width:100%">
            <i class="fa fa-money" aria-hidden="true" style="margin-right:2rem; font-weight: 700;" ></i>'.lang('submit_order').'
        </div>
    ';
    //echo form_submit('add_order',lang('submit_order'),'class="btn btn-theme api_display_none" id="frm_order_submit"');                                          
} elseif ($this->Staff) {
    echo '<div class="alert alert-warning margin-bottom-no">'.lang('staff_not_allowed').'</div>';
}
echo form_close();
                                            }
                                            ?>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                    <!--cart_total-->
                    <div class="col-sm-4">
                        <div id="sticky-con" class="margin-top-lg">
                            <div class="panel panel-default">
                                <div class="panel-heading text-bold">
                                    <i class="fa fa-shopping-cart margin-right-sm"></i> <?= lang('totals'); ?>
                                </div>
                                <div class="panel-body">                     
                                    <table class="table table-striped table-borderless cart-totals margin-bottom-no">
                                        <tr>
                                            <td><?= lang('total'); ?></td>
                                            <td class="text-right" id="tts"><?= $this->sma->formatMoney($total, $selected_currency->symbol); ?></td>
                                        </tr>
                                        <tr class="api_display_none">
                                            <td><?= lang('discount'); ?></td>
                                            <td class="text-right" id="discount"></td>
                                        </tr>
                                        <tr class="">
                                            <td><?= lang('service_charge'); ?></td>
                                            <td class="text-right">5%</td>
                                            <td class="text-right api_display_none" id="service_charge">5</td>
                                        </tr>
                                        <?php if ($Settings->tax2 !== false && $order_tax > 0) {
                                                echo '<tr><td>'.lang('order_tax').'</td><td class="text-right">'.$this->sma->formatMoney($order_tax, $selected_currency->symbol).'</td></tr>';
                                            } ?>
                                        <tr>
                                            <td><?= lang('shipping'); ?> *</td>
                                            <td class="text-right" id="shipping_fee"><?= $this->sma->formatMoney($shipping, $selected_currency->symbol); ?></td>
                                        </tr>
                                        <tr class="">
                                            <td><?= lang('vat'); ?></td>
                                            <td class="text-right" >10%</td>
                                            <td class="text-right api_display_none" id="vat">10</td>
                                        </tr>
                                        <tr class="active text-bold">
                                            <td><?= lang('grand_total'); ?></td>
                                            <td class="text-right api_display_none"><?= $grand_total_label; ?></td>
                                            <td class="text-right" id="grand_total"><?= $grand_total_label; ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--cart_total-->

                </div>
                <?php /* <code class="text-muted">* <?= lang('shipping_rate_info'); ?></code>*/?>
            </div>
        </div>
    </div>
</section>

<?php
    include 'themes/default/shop/views/sub_page/api_gateway_payway.php';
?>

<script type="text/javascript">

function api_select_payment(id){
    var postData = [];
    
    postData[0] = 'cod';
    postData[1] = 'pipay';
    postData[2] = 'wing';
    postData[3] = 'account_receivable';
    postData[4] = 'payway_credit';    
    postData[5] = 'payway_aba_pay';    
    postData[6] = 'aba';    

    for (var i=0; i<=5; i++) {
        document.getElementById("api_payment_box_table_" + postData[i]).classList.remove("api_payment_box_table_selected");
        if (id == postData[i])
            $("#payment_method").val(id);    
    }
    $("#payment_method").val(id);  
    document.getElementById("api_payment_box_table_" + id).classList.add("api_payment_box_table_selected");    
}

<?php
if ($customer->payment_category != '' && $customer->payment_category != 'cash on delivery') {
                                                echo '
document.getElementById("payment_method").value = "account_receivable";
api_select_payment("account_receivable");

    ';
                                            } else {
                                                echo '
document.getElementById("payment_method").value = "cod";    
api_select_payment("cod");
    ';
                                            }

                                            $config_data = array(
                                                'none_label' => lang("Select_a_city"),
                                                'table_name' => 'sma_city',
                                                'space' => ' &rarr; ',
                                                'strip_id' => '',        
                                                'field_name' => 'title_'.$this->api_shop_setting[0]['api_lang_key'],
                                                'condition' => 'order by title_'.$this->api_shop_setting[0]['api_lang_key'].' asc',
                                                'condition_parent' => ' and parent_id = 268',
                                                'translate' => 'yes',
                                                'no_space' => 1,
                                            );                        
                                            $this->site->api_get_option_category($config_data);
                                            $temp_option = $_SESSION['api_temp'];
                                            for ($i=0;$i<count($temp_option);$i++) {                        
                                                $temp = explode(':{api}:',$temp_option[$i]);
                                                $temp_10 = '';
                                                if ($temp[0] != '') {
                                                    $config_data_2 = array(
                                                        'id' => $temp[0],
                                                        'table_name' => 'sma_city',
                                                        'field_name' => 'title_'.$this->api_shop_setting[0]['api_lang_key'],
                                                        'parent_id' => '268',
                                                        'translate' => 'yes',
                                                    );
                                                    $_SESSION['api_temp'] = array();
                                                    $this->site->api_get_category_arrow($config_data_2);          
                                                    for ($i2 = count($_SESSION['api_temp']) - 1; ; $i2--) {
                                                        if ($i2 == 0) {
                                                            break;
                                                        }
                                                        $temp_arrow = '';
                                                        if ($i2 > 1)
                                                            $temp_arrow = ' &rarr; ';
                                                        $temp_10 .= $_SESSION['api_temp'][$i2].$temp_arrow;
                                                    }   
                                                }
                                                $tr_city[$temp[0]] = $temp_10.$temp[1];
                                            }
?>

function api_set_checkout_address(id){
    <?php
    //echo $temp_script;
    ?>
    // if (id == '') id = 0;
    // document.getElementById('api_address').innerHTML = temp_array[id];
}
function api_checkout(){
    if (document.getElementById('payment_method').value == 'payway_credit' || document.getElementById('payment_method').value == 'payway_aba_pay') {
        
        var postData = {
            'payment_method' : document.getElementById('payment_method').value,
            'comment' : document.getElementById('comment').value,
            'customer_id' : document.getElementById('customer_id').value,
            'payment_method' : document.getElementById('payment_method').value,
            'delivery_date' : document.getElementById('delivery_date').value,
            'add_ons_product_import_date' : document.getElementById('add_ons_product_import_date').value,
        };

        var result = $.ajax
        (
            {
                url: '<?php echo base_url(); ?>shop/api_ajax_order_insert',
                type: 'GET',
                secureuri:false,
                dataType: 'html',
                data:postData, 
                async: false,
                error: function (response, status, e)
                {
                    alert(e);
                }
            }
        ).responseText;

        var array_data = String(result).split("api-ajax-request-multiple-result-split");
        var result_text = array_data[1];
// var myWindow = window.open("", "MsgWindow", "width=700, height=400");
// myWindow.document.write(result);           
        if (result_text == 'product_not_found') {
            alert('A product is not found.');
            window.location = '<?php echo base_url().'cart'; ?>';
        }
        else {
            document.api_form_payway.hash.value = array_data[2];
            document.api_form_payway.tran_id.value = array_data[3];
            document.api_form_payway.amount.value = array_data[4];
            document.api_form_payway.payment_option.value = array_data[5];
            AbaPayway.checkout();
        }
    }
    else
        document.frm_order.submit();
}
function api_bank_in_form_action(action){        
    if (action == 'show') {
        $('#api_bank_in_form').show('toggle');
    }
    if (action == 'hide') {
        $('#api_bank_in_form').hide('toggle');
    }
}
api_bank_in_form_action('hide');

var select_city = "<?php foreach (array_keys($tr_city) as $key) { echo "<option value='$key' >".$tr_city[$key]."</option>"; } ?>";

</script>

<style>
.api_payment_box{
    margin-left:5px;
    margin-right:5px;
    margin-bottom:15px;
    width:105px;
    height:88px;
    border:0px solid red;
    display:inline-block;
}
.api_payment_box_table{
    border:1px solid #c4c4c4;    
    cursor:pointer;
}
.api_payment_box_table_selected{
    border:2px solid #f30000;    
    cursor:pointer;
}
.api_payment_box_table:hover{
    border:1px solid #f30000 !important;    
}
.api_button{
    padding: 16px 42px;
    width:100%;
    box-shadow: 0px 0px 6px -2px rgba(0,0,0,0.5);
    line-height: 1.25;
    background: #f8acaf;
    text-decoration: none;
    color: white;
    font-size: 16px;
    text-transform: uppercase;
    position: relative;
    overflow: hidden;
    border-radius:10px;
    text-align:center;
    border-color:#f8acaf !important;
    cursor:pointer;
}
.api_button:hover{
    background: #ef238b;
}
.api_button_air{
    padding: 16px 42px;
    width:100%;
    box-shadow: 0px 0px 6px -2px rgba(0,0,0,0.5);
    line-height: 1.25;
    background: #992824;
    text-decoration: none;
    color: white;
    font-size: 16px;
    text-transform: uppercase;
    position: relative;
    overflow: hidden;
    border-radius:10px;
    text-align:center;
    border-color:#f8acaf !important;
    cursor:pointer;
}
.api_button_air:hover{
    background: #ee7571;
}
</style>

<script>
function ajax_getDeliveryAddress(postData) {
    var result = $.ajax({
        url: '<?php echo base_url();?>shop/ajax_getDeliveryAddress',
        type: 'GET',
        secureuri:false,
        dataType: 'html',
        data:postData,
        async: false,
        error: function (response, status, e)
        {
        alert(e);
        }
    }).responseText;
    // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
    // myWindow.document.write(result);     
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    var result_text = array_data[1];
    $('#discount').text(array_data[1]+'%');
    var shipping = '';
    if (array_data[2] != '') {
        shipping = array_data[2];
        // $("#shipping_fee").text('$' + parseFloat(shipping, 10).toFixed(2));
        $("#shipping_fee").text('$' + parseFloat(shipping).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
    } else {
        shipping = '0.00'
        $("#shipping_fee").text('$'+ shipping);
    }

    var total = document.getElementById('tts').innerHTML;
    var sc = document.getElementById('service_charge').innerHTML;
    var vt = document.getElementById('vat').innerHTML;
    total = total.replace('$',"");
    total = total.replace(",",""); // replace , to 1111.00
    
    // dicount (array_data[1])
    // var total2 = parseFloat((total[1] * array_data[1]) / 100).toFixed(2);
    //var total2 = (total[1] * array_data[1]) / 100;
    // var sum = parseFloat(total[1] - total2) + parseFloat(shipping);

    var service_charge = (total * sc) / 100;
    var grand_total = parseFloat(total) + parseFloat(shipping) + parseFloat(service_charge);
    var vat = (grand_total * vt) / 100; // find vat 10% of (total + shipping + service_charge)
    var final_grand_total = parseFloat(vat) + parseFloat(grand_total);
    
    console.log('vat='+vat+'==='+final_grand_total+'=='+grand_total);
    $('#grand_total').text('$' + parseFloat(final_grand_total).toFixed(2));
    $('#grand_total_label').text('$' + parseFloat(final_grand_total).toFixed(2));
}

$( document ).ready(function() {
   $( "#default_delivery_address" ).trigger( "click" );
});

function add_delivery_address() {
    var body = '<form action="<?php echo base_url();?>shop/add_delivery_address" name="api_fmr_add_delivery_address" class="validate" id="api_fmr_add_delivery_address"><div class="row"><div class="form-group col-sm-6"><input name="add_ons_first_name" value=""  class="form-control" placeholder="<?= lang('first_name');?>"></div><div class="form-group col-sm-6"><input name="add_ons_last_name" value="" class="form-control" placeholder="<?= lang('last_name');?>"></div></div><div class="row"><div class="form-group col-sm-6"><input name="phone" value="" class="form-control" placeholder="<?= lang('phone_number');?> *" required="required"></div><div class="form-group col-sm-6"><input name="add_ons_email" value="" class="form-control" placeholder="<?= lang('email');?>"></div></div><div class="row"><div class="form-group col-sm-12"><input name="add_ons_delivery_address"  class="form-control" value="" placeholder="<?= lang('Delivery_Address');?> *"></div></div><div class="row"><div class="col-sm-12 form-group"><select name="city_id" class="form-control" onchange="ajax_getCityId(this.value);">'+ select_city +'</select></div></div>  <div class="row"><div class="form-group col-sm-4"><input name="postal_code" value="" class="form-control" placeholder="<?= lang('postal_code');?>"></div><div class="form-group col-sm-4"><label style="font-size: 15px;padding-top: 8px;"><?= lang('delivery_fee');?> : <span id="address-delivery-fee"></span> </label></div><div class="form-group col-sm-4"><label style="font-size: 15px;padding-top: 8px;"><?= lang('discount');?> : <span id="address-discount"></span> </label></div></div><input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>"></form>';

    $('#api_modal_title').html('<?= lang('add_address');?>');
    $('#api_modal_body').html(body);
    $('#api_modal_footer').html('<button type="button" class="btn btn-info swal2-confirm swal2-styled" data-dismiss="modal" onclick="document.api_fmr_add_delivery_address.submit();"><?= lang('Add');?></button> <button type="button" class="btn btn-danger" data-dismiss="modal"><?= lang('Close');?></button>');
    $('#api_modal_trigger').click();

    if(document.getElementById('address-delivery-fee').innerHTML == '')
        document.getElementById('address-delivery-fee').innerHTML = '$0.00';
    if(document.getElementById('address-discount').innerHTML == '')
        document.getElementById('address-discount').innerHTML = '0%';
}
</script>
