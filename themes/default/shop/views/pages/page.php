<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
$l = $this->api_shop_setting[0]['api_lang_key'];
$temp = str_replace(' ', '_', strtolower($page->title));
$temp_title = lang($temp);
$config_data = array(
    'table_name' => 'sma_pages',
    'select_table' => 'sma_pages',
    'translate' => 'yes',
    'select_condition' => "id = ".$page->id,
);
$select_data = $this->site->api_select_data_v2($config_data);                 

$temp_display = '';
$temp_display .= '
    <div class="col-md-12">
        <div class="panel panel-default margin-top-lg">
            <div class="panel-heading text-bold">
                '.$temp_title.'
            </div>
            <div class="panel-body" style="min-height:500px;">
                '.$this->sma->decode_html($select_data[0]['title_'.$l]).'
            </div>
        </div>
    </div>
';

$config_data = array(
    'wrapper_class' => 'api_ayum_blog_detail',
    'custom_html' => '<div class="api_height_30 visible-xs"></div>',
    'page_header' => '',
    'display' => $temp_display,
    'display_class' => 'col-md-12 api_padding_0',
    'panel' => '',
    'panel_class' => 'col-md-3 api_padding_0',
    'type' => '',
);
$temp = $this->api_display->template_display($config_data);
echo $temp['display'];

?>