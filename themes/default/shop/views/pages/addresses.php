<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style>
    .api_address_detail{
        color:red;
        background-color: #333;
    }
    .are_you_sure{
        color: #595959;
        font-size: 30px;
        text-align: center;
        font-weight: 600;
        text-transform: none;
        position: relative;
        margin: 0 0 .4em;
        padding: 0;
        display: block;
    }
    .delivery_adderss{
        font-size: 18px;
        text-align: center;
        font-weight: 300;
        position: relative;
        float: none;
        margin: 0;
        padding: 0;
        line-height: normal;
        color: #545454;
        word-wrap: break-word;
    }
    .btn_cancel_ok{
        border: 0;
        border-radius: 3px;
        box-shadow: none;
        color: #fff;
        cursor: pointer;
        font-size: 17px;
        font-weight: 500;
        margin: 0 5px;
        padding: 10px 32px;
    }
</style>
<div id="myModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-confirm" >
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="border-bottom: none;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="icon-box">
                    <div class="swal3-icon swal3-question" id="swal3-question" style="">?</div>
                </div>              
                <h2 class="are_you_sure" id="conf_title" style="text-align:center"><?= lang('are_you_sure');?></h2>
                <p class="delivery_adderss" id="delivery_address"><?= lang('delete_delivery_address'); ?></p>
            </div><br>
            <div class="modal-footer" id="delete_data" style="border-top:none;text-align: center;">
                <button type="button" role="button"  tabindex="0" class="submit_delete btn_cancel_ok" style="background-color: rgb(48, 133, 214);"><?= lang('Okay'); ?></button>
                <button id="" type="button" role="button" tabindex="0" class="cancel_delete btn_cancel_ok" data-dismiss="modal" style="background-color: rgb(170, 170, 170);"><?= lang('Cancel'); ?></button>
            </div>
        </div>
    </div>
</div>

<section class="page-contents">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-9 col-md-10 api_padding_right_0_pc">
                        
                        <div class="col-md-12 api_padding_0_mobile">
                            <h4 class="margin-top-md title text-bold">
                                <span><i class="fa fa-building margin-right-sm"></i> <?= lang('my_addresses'); ?></span>            
                                <div id="api_customer_menu_panel_btn" class="api_float_right api_pointer api_screen_show_768" onclick="$('#api_customer_menu_panel').toggle('slideToggle?');" style="top: -10px; margin-top: -15px;">
                                    <button class="btn btn-danger navbar-btn"><div class="fa fa-bars fa-lg"></div></button>
                                </div>
                            </h4>
                        </div>

                        <div id="api_customer_menu_panel" class="col-md-2 api_padding_right_0_pc api_padding_0_mobile api_screen_hide_768">
                            <div class="api_height_10 api_screen_hide_768"></div>
                            <?php include('themes/default/shop/views/pages/customer_menu_panel.php'); ?>                            
                        </div>
                        <div class="col-md-10 api_padding_0_mobile">
                            <div class="api_height_13"></div>

    <?php
    if ($this->Settings->indian_gst) { $istates = $this->gst->getIndianStates(); }
        $user_info = $this->site->getUser();
        $config_data = array(
            'table_name' => 'sma_companies',
            'select_table' => 'sma_companies',
            'translate' => '',
            'select_condition' => "id = ".$user_info->company_id,
        );
        $select_data = $this->site->api_select_data_v2($config_data);
        $config_data = array(
            'id' => $select_data[0]['city_id'],
            'table_name' => 'sma_city',
            'field_name' => 'title_en',
            'translate' => 'yes',
            'seperate' => ', ',
            'reverse' => '',
        );
        $select_city = $this->site->api_display_category($config_data);         
        // $display_city = explode(',',$select_city['display']);
        // if($display_city[4] != '') 
        //     $a = ', ';
        // if($display_city[3] != '')
        //     $b = ', ';

        $config_data = array(
            'table_name' => 'sma_city',
            'select_table' => 'sma_city',
            'translate' => 'yes',
            'select_condition' => "id = ".$select_data[0]['city_id'],
        );
        $select_city2 = $this->site->api_select_data_v2($config_data);
        
        echo '<div class="row">
                <div class="col-sm-12"><span class="text-bold">'.lang('select_address_to_edit').'</span>
                <div class="col-sm-12 link-address api_address_detail">
                    <p style="font-size: 20px;">'.lang('default_delivery_address').'</p>
                    <div>'.lang('name').' : <span>'.$user_info->first_name.' '.$user_info->last_name.'</span></div>
                    <div>'.lang('phone_number').' : <span>'.$user_info->phone.'</span></div>
                    <div>'.lang('email').' : <span>'.$user_info->email.'</span></div>
                    <div>'.lang('Address').' : <span>'.$select_data[0]['address'].' '.$select_city['display'].' '.$select_data[0]['postal_code'].'</div>
                    <div>'.lang('delivery_fee').' : <span>'.$this->sma->formatMoney($select_city2[0]['delivery_fee']).'</span></div>';
                    if ($select_city2[0]['address_discount'] != '')
                        echo '<div>'.lang('discount').' : <span>'.$select_city2[0]['address_discount'].'%</span></div>';
                    else 
                        echo '<div>'.lang('discount').' : <span>0%</span></div>';
                echo '</div>
                </div>
            </div>';
            
    // $this->sma->print_arrays($select_data);
    
    echo '<div class="row">';
        $r = 1;
        
        for($i=0;$i<count($addresses_v2);$i++) {
            if ($i != 0)
                $and = ' or ';
            $eid .= $and. ' id = '.$addresses_v2[$i]['id'].'';
            $config_data = array(
                'id' => $addresses_v2[$i]['city_id'],
                'table_name' => 'sma_city',
                'field_name' => 'title_en',
                'translate' => 'yes',
                'seperate' => ', ',
                'reverse' => '',
            );
           
            echo '<div class="col-sm-12" data-id="'.$addresses_v2[$i]['id'].'" id="element_id_address_'.$addresses_v2[$i]['id'].'">';
            $city = $this->site->api_display_category($config_data);
                echo '<div class="link-address api_address_detail">
                            <p class="api_display_none" style="font-size: 20px;">'.lang('Delivery_Address').'<span style="margin-left: 5px;">'.$r.'</span></p>';
                            if ($addresses_v2[$i]['first_name'] != '' || $addresses_v2[$i]['last_name'] != '') {
                                echo '<div>'.lang('name').' : '.$addresses_v2[$i]['first_name'].' '.$addresses_v2[$i]['last_name'].'</div>';
                            } 
                            echo '<div>'.lang('phone_number').' : <span>'.$addresses_v2[$i]['phone'].'</span></div>';
                            if ($addresses_v2[$i]['email'] != '') {
                                echo '<div>'.lang('email').' : <span>'.$addresses_v2[$i]['email'].'</span></div>';
                            }
                            echo '<div>'.lang('Address').' : <span>'.$addresses_v2[$i]['delivery_address'].' '.$city['display'].' '.$addresses_v2[$i]['postal_code'].'</span></div>    
                            <span id="address_id_'.$addresses_v2[$i]['id'].'" class="edit api_display_none"><i class="fa fa-edit" ></i></span>';
                            $config_data = array(
                                'table_name' => 'sma_city',
                                'select_table' => 'sma_city',
                                'translate' => 'yes',
                                'select_condition' => "id = ".$addresses_v2[$i]['city_id'],
                            );
                            $select_city3 = $this->site->api_select_data_v2($config_data);
                            for($j=0;$j<count($select_city3);$j++) {
                                echo '    <div>'.lang('delivery_fee').' : <span>'.$this->sma->formatMoney($select_city3[$j]['delivery_fee']).'</span></div>';
                                if ($select_city3[$j]['address_discount'] != '')
                                    echo '<div>'.lang('discount').' : <span>'.$select_city3[$j]['address_discount'].'%</span></div>';
                                else 
                                    echo '<div>'.lang('discount').' : <span>0%</span></div>';
                            }
                        // echo  '   <div class="edit-address" data-id="'.$addresses_v2[$i]['id'].'" style="cursor: pointer;">
                        // onclick="$(\'#address_id_'.$addresses_v2[$i]['id'].'\').click();"
                        echo  '   <div class="" data-id="'.$addresses_v2[$i]['id'].'" style="cursor: pointer;">

                                <div style="position: absolute; right: 5px; bottom: 4px;">
                                    <i onclick="update_delivery_address('.$addresses_v2[$i]['id'].');" title="Edit" class="fa fa-edit fa-2x" style="color:red; width:35px !important; height:33px;"></i>
                                </div>   
                            </div>
                            <div style="position: absolute; right: 50px; bottom: 8px; cursor: pointer;">
                                <div class="delete" onclick="
                                            var postData = {
                                                \'address_id\' : '.$addresses_v2[$i]['id'].',
                                                \'element_id\' : \'element_id_address_'.$addresses_v2[$i]['id'].'\'
                                            };
                                            delete_address(postData);">
                                    <i title="Delete" class="fa fa-trash fa-2x" style="color:red; width:25px !important; height:30px;"></i>
                                </div>
                            </div>
                    </div>';
            echo '</div>';
            $r++;
        }
    echo '</div>';

    if (count($addresses) < 6) {
        echo '<div class="row margin-top-lg">';
        // echo '<div class="col-sm-12"><a href="#" id="add-address" class="btn btn-primary btn-sm">'.lang('add_address').'</a></div>';
        echo '<div class="col-sm-12"><button class="btn btn-primary btn-sm" onclick="add_delivery_address();">'.lang('add_address').'</button></div>';
        echo '</div>';
    }
    if ($this->Settings->indian_gst) {
    ?>
    <script>
        var istates = <?= json_encode($istates); ?>
    </script>
    <?php
    } else {
        echo '<script>var istates = false; </script>';
    }
    ?>

                        </div>
                    </div>


                    <div class="col-sm-3 col-md-2">
                        <?php include(b'themes/default/shop/views/pages/sidebar2.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
                        <?php

                            $config_data = array(
                                'none_label' => lang("Select_a_city"),
                                'table_name' => 'sma_city',
                                'space' => ' &rarr; ',
                                'strip_id' => '',        
                                'field_name' => 'title_'.$this->api_shop_setting[0]['api_lang_key'],
                                'condition' => 'order by ordering',
                                'condition_parent' => ' and parent_id = 268',
                                'translate' => 'yes',
                                'no_space' => 1,
                            );                        
                            $this->site->api_get_option_category($config_data);
                            $temp_option = $_SESSION['api_temp'];
                            for ($i=0;$i<count($temp_option);$i++) {                        
                                $temp = explode(':{api}:',$temp_option[$i]);
                                $temp_10 = '';
                                if ($temp[0] != '') {
                                    $config_data_2 = array(
                                        'id' => $temp[0],
                                        'table_name' => 'sma_city',
                                        'field_name' => 'title_'.$this->api_shop_setting[0]['api_lang_key'],
                                        'parent_id' => '268',
                                        'translate' => 'yes',
                                    );
                                    $_SESSION['api_temp'] = array();
                                    $this->site->api_get_category_arrow($config_data_2);          
                                    for ($i2 = count($_SESSION['api_temp']) - 1; ; $i2--) {
                                        if ($i2 == 0) {
                                            break;
                                        }
                                        $temp_arrow = '';
                                        if ($i2 > 1)
                                            $temp_arrow = ' &rarr; ';
                                        $temp_10 .= $_SESSION['api_temp'][$i2].$temp_arrow;
                                    }   
                                }
                                $tr_city[$temp[0]] = $temp_10.$temp[1];
                            }

                               
                        ?>             

<script type="text/javascript">
var addresses = <?= !empty($addresses_v2) ? json_encode($addresses_v2) : 'false'; ?>;

function delete_address(postData){
    $('#myModal').modal('show');    $(".submit_delete").show();
    $('#delete_data').on('click','.submit_delete',function(){
        var result = $.ajax
        (
        {
        url: '<?php echo base_url(); ?>shop/delete_address',
        type: 'GET',
        secureuri:false,
        dataType: 'html',
        data:postData,
        async: false,
        success: function(data)
        {
            if('#'+result_text)
            {
                $('#myModal').modal('hide');
                location.reload();
            }
            else
            {
                alert('Fail');
            }

        },
        error: function (response, status, e)
        {
        alert(e);
        }
        }
        ).responseText;
        var array_data = String(result).split("api-ajax-request-multiple-result-split");
        var result_text = array_data[1];
    });
}
var select_city = "<?php foreach (array_keys($tr_city) as $key) { echo "<option value='$key'>".$tr_city[$key]."</option>"; } ?>";


function update_delivery_address(id) {
    var postData = {'address_id' : id };
    var result = $.ajax({
        url: '<?php echo site_url();?>shop/addresses_v2',
        type: 'GET',
        secureuri:false,
        dataType: 'html',
        data:postData,
        async: false,
        error: function (response, status, e)
        {
        alert(e);
        }
    }).responseText;
    // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
    // myWindow.document.write(result);     
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    var delivery_fee = '';
    var dicount = '';
    var city = '';
    if (array_data[8] != '')
        delivery_fee = array_data[8];
    else
        delivery_fee = 0;

    // if (array_data[9] != '')
    //     dicount = array_data[9];
    // else 
    //     dicount = 0;
    

    var body = '<form action="<?php echo base_url();?>shop/address_v2/'+ id +'" name="api_fmr_update_delivery_address" class="validate" id="api_fmr_update_delivery_address" data-toggle="validator" role="form" novalidate="novalidate"><div class="row"><div class="form-group col-sm-6"><input name="add_ons_first_name" id="address-first-name" value="'+ array_data[1] +'"  class="form-control" placeholder="<?= lang('first_name');?>"></div><div class="form-group col-sm-6"><input name="add_ons_last_name" id="address-last-name" value="'+ array_data[2] +'" class="form-control" placeholder="<?= lang('last_name');?>"></div></div><div class="row"><div class="form-group col-sm-6"><input name="phone" value="'+ array_data[3] +'" id="address-phone" class="form-control" placeholder="<?= lang('phone_number');?> *" required="required"></div><div class="form-group col-sm-6"><input name="add_ons_email" value="'+ array_data[4] +'" id="address-email" class="form-control" placeholder="<?= lang('email');?>"></div></div><div class="row"><div class="form-group col-sm-12"><input name="add_ons_delivery_address" id="address-delivery-address" class="form-control" value="'+ array_data[5] +'" placeholder="<?= lang('Delivery_Address');?> *" required="required"></div></div><div class="row"><div class="col-sm-12 form-group"><select name="city_id" class="form-control" id="address-city-id" onchange="ajax_getCityId(this.value);">'+ select_city.replace("value='" + array_data[6] + "'","value='" + array_data[6] + "' selected") +'</select></div></div>  <div class="row"><div class="form-group col-sm-4"><input name="postal_code" value="'+ array_data[7] +'" id="address-postal-code" class="form-control" placeholder="<?= lang('postal_code');?>"></div><div class="form-group col-sm-4"><label style="font-size: 15px;padding-top: 8px;"><?= lang('delivery_fee');?> : <span id="address-delivery-fee"> $'+ parseFloat(delivery_fee).toFixed(2) +'</span> </label></div><div class="form-group col-sm-4"><label style="font-size: 15px;padding-top: 8px;"><?= lang('discount');?> : <span id="address-discount">'+ array_data[9] +'%</span> </label></div></div><input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>"></form>';

    $('#api_modal_title').html('<?= lang('update_address');?>');
    $('#api_modal_body').html(body);
    $('#api_modal_footer').html('<button type="button" class="btn btn-info swal2-confirm swal2-styled" data-dismiss="modal" onclick="document.api_fmr_update_delivery_address.submit();"><?= lang('update');?></button> <button type="button" class="btn btn-danger" data-dismiss="modal"><?= lang('Close');?></button>');
    $('#api_modal_trigger').click();

    if(document.getElementById('address-discount').innerHTML == '%')
        document.getElementById('address-discount').innerHTML = '0%';
    
}
function add_delivery_address() {
    var body = '<form action="<?php echo base_url();?>shop/add_delivery_address" name="api_fmr_add_delivery_address" class="validate" id="api_fmr_add_delivery_address"><div class="row"><div class="form-group col-sm-6"><input name="add_ons_first_name" value=""  class="form-control" placeholder="<?= lang('first_name');?>"></div><div class="form-group col-sm-6"><input name="add_ons_last_name" value="" class="form-control" placeholder="<?= lang('last_name');?>"></div></div><div class="row"><div class="form-group col-sm-6"><input name="phone" value="" class="form-control" placeholder="<?= lang('phone_number');?> *" required="required"></div><div class="form-group col-sm-6"><input name="add_ons_email" value="" class="form-control" placeholder="<?= lang('email');?>"></div></div><div class="row"><div class="form-group col-sm-12"><input name="add_ons_delivery_address"  class="form-control" value="" placeholder="<?= lang('Delivery_Address');?> *"></div></div><div class="row"><div class="col-sm-12 form-group"><select name="city_id" class="form-control" onchange="ajax_getCityId(this.value);">'+ select_city +'</select></div></div>  <div class="row"><div class="form-group col-sm-4"><input name="postal_code" value="" class="form-control" placeholder="<?= lang('postal_code');?>"></div><div class="form-group col-sm-4"><label style="font-size: 15px;padding-top: 8px;"><?= lang('delivery_fee');?> : <span id="address-delivery-fee"></span> </label></div><div class="form-group col-sm-4"><label style="font-size: 15px;padding-top: 8px;"><?= lang('discount');?> : <span id="address-discount"></span> </label></div></div><input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>"></form>';

    $('#api_modal_title').html('<?= lang('add_address');?>');
    $('#api_modal_body').html(body);
    $('#api_modal_footer').html('<button type="button" class="btn btn-info swal2-confirm swal2-styled" data-dismiss="modal" onclick="document.api_fmr_add_delivery_address.submit();"><?= lang('Add');?></button> <button type="button" class="btn btn-danger" data-dismiss="modal"><?= lang('Close');?></button>');
    $('#api_modal_trigger').click();
    
    if(document.getElementById('address-delivery-fee').innerHTML == '')
        document.getElementById('address-delivery-fee').innerHTML = '$0.00';
    if(document.getElementById('address-discount').innerHTML == '')
        document.getElementById('address-discount').innerHTML = '0%';
}
</script>


