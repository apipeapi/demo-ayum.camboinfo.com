<?php

$temp = array();
if (is_int(strpos($_SERVER['REQUEST_URI'],"profile"))) $temp[1] = 'selected';
if (is_int(strpos($_SERVER['REQUEST_URI'],"shop/orders"))) $temp[2] = 'selected';
if (is_int(strpos($_SERVER['REQUEST_URI'],"shop/wishlist"))) $temp[3] = 'selected';
if (is_int(strpos($_SERVER['REQUEST_URI'],"shop/quotes"))) $temp[4] = 'selected';
if (is_int(strpos($_SERVER['REQUEST_URI'],"shop/downloads"))) $temp[5] = 'selected';
if (is_int(strpos($_SERVER['REQUEST_URI'],"shop/addresses"))) $temp[6] = 'selected';
if (is_int(strpos($_SERVER['REQUEST_URI'],"shop/order_history"))) $temp[7] = 'selected';
if (is_int(strpos($_SERVER['REQUEST_URI'],"shop/consignment"))) $temp[8] = 'selected';
echo '                            
    <ul class="api_customer_menu_panel_ul">
        <a href="'.base_url().'profile">
            <li class="api_customer_menu_panel_li"><span class="'.$temp[1].'">'.lang('profile').'</span> <i class="mi fa fa-user api_float_right api_margin_top_3 '.$temp[1].'"></i></li>
        </a>        
        <a href="'.base_url().'shop/orders">
            <li class="api_customer_menu_panel_li"><span class="'.$temp[2].'">'.lang('orders').'</span> <i class="mi fa fa-heart api_float_right api_margin_top_3 '.$temp[2].'"></i></li>
        </a>
';

if ($this->session->userdata('sale_consignment_auto_insert') == 'yes')
    echo '
        <a href="'.base_url().'shop/consignment">
            <li class="api_customer_menu_panel_li"><span class="'.$temp[8].'">'.lang('Consignment').'</span> <i class="mi fa fa-truck api_float_right api_margin_top_3 '.$temp[8].'"></i></li>
        </a>
    ';


echo '
        <a href="'.base_url().'shop/wishlist">
            <li class="api_customer_menu_panel_li"><span class="'.$temp[3].'">'.lang('wishlist').'</span> <i class="mi fa fa-heart-o api_float_right api_margin_top_3 '.$temp[3].'"></i></li>
        </a>
';
/*
echo '
        <a href="'.base_url().'shop/order_history">
            <li class="api_customer_menu_panel_li"><span class="'.$temp[7].'">'.lang('Order_History').'</span> <i class="mi fa fa-list-alt api_float_right api_margin_top_3 '.$temp[7].'"></i></li>
        </a>
        <a href="'.base_url().'shop/quotes">
            <li class="api_customer_menu_panel_li"><span class="'.$temp[4].'">'.lang('quotes').'</span> <i class="mi fa fa-heart-o api_float_right api_margin_top_3 '.$temp[4].'"></i></li>
        </a>
        <a href="'.base_url().'shop/downloads">
            <li class="api_customer_menu_panel_li"><span class="'.$temp[5].'">'.lang('downloads').'</span> <i class="mi fa fa-download api_float_right api_margin_top_3 '.$temp[5].'"></i></li>
        </a>
        <a href="'.base_url().'shop/addresses">
            <li class="api_customer_menu_panel_li"><span class="'.$temp[6].'">'.lang('addresses').'</span> <i class="mi fa fa-building api_float_right api_margin_top_3 '.$temp[6].'"></i></li>
        </a>
';
*/

echo '
    <a href="'.base_url().'shop/addresses">
        <li class="api_customer_menu_panel_li"><span class="'.$temp[6].'">'.lang('addresses').'</span> <i class="mi fa fa-building api_float_right api_margin_top_3 '.$temp[6].'"></i></li>
    </a>
';

echo '
        <a href="'.base_url().'logout">
            <li class="api_customer_menu_panel_li"><span class="">'.lang('logout').'</span><i class="mi fa fa-sign-out api_float_right api_margin_top_3 "></i></li>
        </a>
    </ul>
    <div class="api_height_20"></div>
';
?>
<style>
.api_customer_menu_panel_ul{
    list-style: none; padding:0px; margin:0px;
}
.api_customer_menu_panel_li{
    padding-top: 5px; padding-bottom:5px; font-size: 16px;
    color:#ff5e52;
    font-weight:bold;
}
.api_customer_menu_panel_li:hover, .api_customer_menu_panel_li .selected, .api_table_view:hover{
    color:#ff1100;
}
</style>