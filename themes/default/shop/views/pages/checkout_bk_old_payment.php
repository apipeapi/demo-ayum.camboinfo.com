<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php

$shipping = $this->sma->convertMoney($this->cart->shipping(), FALSE, FALSE);
$order_tax = $this->sma->convertMoney($this->cart->order_tax(), FALSE, FALSE);

$total = $this->cart->total();
$total = $this->sma->convertMoney($total, FALSE, FALSE);

if ($customer->vat_no) {
    $order_tax = ($total * 10) / 100;
    $order_tax = $this->sma->convertMoney($order_tax, FALSE, FALSE);
}                                

$grand_total_label = $this->sma->formatMoney(($this->sma->formatDecimal($total)+$this->sma->formatDecimal($order_tax)+$this->sma->formatDecimal($shipping)), $selected_currency->symbol);                                    

?>

<section class="page-contents api_padding_bottom_0_im api_padding_top_0_im">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">

                    <div class="col-sm-8">
                        <div class="panel panel-default margin-top-lg"> 
                            <div class="panel-heading text-bold">
                                <i class="fa fa-shopping-cart margin-right-sm"></i> <?= lang('checkout'); ?>
                                <a href="<?= site_url('cart'); ?>" class="pull-right">
                                    <i class="fa fa-share"></i>
                                    <?= lang('back_to_cart'); ?>
                                </a>
                            </div>
                            <div class="panel-body">

                                <div>
                                <?php
                                if (!$this->loggedIn) {
                                    ?>
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#user" aria-controls="user" role="tab" data-toggle="tab"><?= lang('returning_user'); ?></a></li>
                                        <li role="presentation"><a href="#guest" aria-controls="guest" role="tab" data-toggle="tab"><?= lang('guest_checkout'); ?></a></li>
                                    </ul>
                                    <?php
                                }
                                ?>

                                    <div class="tab-content padding-lg api_padding_0_im">
                                        <div role="tabpanel" class="tab-pane fade in active" id="user">
                                            <?php
                                            if ($this->loggedIn) {
                                                if ($this->Settings->indian_gst) { $istates = $this->gst->getIndianStates(); }
                                                if (!empty($addresses)) {
                                                    echo shop_form_open('order', 'class="validate" name="frm_order" id="frm_order"');
                                                    echo '<div class="row">';
                                                    echo '<div class="col-sm-12 text-bold" style="color: #f44336 !important;">'.lang('select_address').'</div>';
                                                    $r = 1;
                                                    foreach ($addresses as $address) {
                                                        ?>
                                                        <div class="col-sm-6">
                                                            <div class="checkbox bg">
                                                                <label>
                                                                    <input type="radio" name="address" value="<?= $address->id; ?>" <?= $r == 1 ? 'checked' : ''; ?>>
                                                                    <span>
                                                                        <?= $address->line1; ?><br>
                                                                        <?= $address->line2; ?><br>
                                                                        <?= $address->city; ?>
                                                                        <?= $this->Settings->indian_gst && isset($istates[$address->state]) ? $istates[$address->state].' - '.$address->state : $address->state; ?><br>
                                                                        <?= $address->postal_code; ?> <?= $address->country; ?><br>
                                                                        <?= lang('phone').': '.$address->phone; ?>
                                                                    </span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        $r++;
                                                    }
                                                    echo '</div>';
                                                }
                                                if (count($addresses) < 6 && !$this->Staff) {
                                                    echo '<div class="row margin-bottom-lg">';
                                                    echo '<div class="col-sm-12"><a href="#" id="add-address" class="btn btn-primary btn-sm">'.lang('add_new_address').'</a></div>';
                                                    echo '</div>';
                                                }
                                                if ($this->Settings->indian_gst && (isset($istates))) {
                                                ?>
                                                <script>
                                                    var istates = <?= json_encode($istates); ?>
                                                </script>
                                                <?php
                                                } else {
                                                    echo '<script>var istates = false; </script>';
                                                }
                                                ?>

                                                            
                                    			<div class="form-group api_margin_bottom_0_im">
                                                    <span style="color: #f44336 !important;"><?= lang("Requested_Delivery_Date", "sldate"); ?></span>
                                                    <div class="input-group date delivery_date col-md-4 col-sm-9 col-xs-12" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                                        <?php echo form_input('delivery_date', '', 'class="form-control input-tip datetime" size="10" id="delivery_date" '); ?>                    
                                                        <span class="input-group-addon pointer"><li class="fa fa-times"></li></span>
                                    					<span class="input-group-addon pointer"><li class="fa fa-calendar"></li></span>
                                                    </div>
                                    				<input type="hidden" id="dtp_input2" value="" /><br/>
                                                </div>
                                                                        
                                                <strong style="color: #f44336 !important;"><?= lang('payment_method'); ?></strong>
                                                <div class="checkbox bg">
                                                    <?php if ($paypal->active) { ?>
                                                    <label style="display: inline-block; width: auto;" onclick="api_bank_in_form_action('hide');">
                                                        <input type="radio" name="payment_method" value="paypal" id="paypal" required="required">
                                                        <span>
                                                            <i class="fa fa-paypal margin-right-md"></i> <?= lang('paypal') ?>
                                                        </span>
                                                    </label>
                                                    <?php } ?>
                                                    <?php if ($skrill->active) { ?>
                                                    <label style="display: inline-block; width: auto;" onclick="api_bank_in_form_action('hide');">
                                                        <input type="radio" name="payment_method" value="skrill" id="skrill" required="required">
                                                        <span>
                                                            <i class="fa fa-credit-card-alt margin-right-md"></i> <?= lang('skrill') ?>
                                                        </span>
                                                    </label>
                                                    <?php } ?>
                                                    <label style="display: inline-block; width: auto;" onclick="api_bank_in_form_action('show');">
                                                        <input type="radio" name="payment_method" value="bank" id="bank" required="required">
                                                        <span>
                                                            <i class="fa fa-bank margin-right-md"></i> <?= lang('bank_in') ?>
                                                        </span>
                                                    </label>

                                                    <label style="display: inline-block; width: auto;" onclick="api_bank_in_form_action('hide');">
                                                        <input type="radio" name="payment_method" value="cod" id="cod" required="required">
                                                        <span>
                                                            <i class="fa fa-money margin-right-md"></i> <?= lang('cod') ?>
                                                        </span>
                                                    </label>
                                                </div>
<?php

$temp_padding = 'api_padding_left_10 api_padding_right_10';

$api_bank_in_form = '
<div id="api_bank_in_form">
';

$api_bank_in_form .= '
    <div class="api_height_20"></div>
    <fieldset class="scheduler-border">
        <legend class="scheduler-border">'.lang('bank_in','bank_in').' '.lang('payment','payment').'</legend>
        <div class="col-md-12 '.$temp_padding.'">
            <div class="api_height_10"></div>
Only ABA bank and ACLEDA bank are available. First you have to transfer <b>'.$grand_total_label.'</b>
to our bank account.
<div class="api_height_10"></div> 
            <ul>
            <li>
                For ABA accounts please transfer money to ABA account number: <b>'.$settings->aba_account_number.'</b>
            </li>    
            <li>
                For ACLEDA accounts please trasfer money to ACLEDA account number: <b>'.$settings->acleda_account_number.'</b>
            </li>
            </ul>
Then fill form below with the bank transfer reference number for us to verify your payment.   
            <div class="api_height_30"></div>
        </div>
';

$api_bank_in_form .= '        
    <div class="col-md-6 '.$temp_padding.'">                                 
    <div class="form-group">
        '.lang("your_bank", "your_bank").'
';
        $tr[''] = 'Please select your bank';
        $tr['aba'] = 'ABA Bank';
        $tr['acleda'] = 'ACLEDA Bank';
        $api_bank_in_form .= form_dropdown('your_bank', $tr, '', 'class="form-control select" id="your_bank" onchange="api_your_bank_change(this.value);" style="width:100%;"');                
$api_bank_in_form .= '
    </div>
    </div>
';

$api_bank_in_form .= '   
    <div class="col-md-6 '.$temp_padding.'">                                 
    <div class="form-group">
        '.lang('transfer_amount', 'transfer_amount').'
        '.form_input('transfer_amount', $grand_total_label, 'class="form-control" id="transfer_amount" readonly="readonly"').'
    </div>
    </div>
';

$api_bank_in_form .= '   
    <div class="col-md-6 '.$temp_padding.'">                                 
    <div class="form-group">
        '.lang('your_bank_account_number', 'your_bank_account_number').'
        '.form_input('your_bank_account_number', '', 'class="form-control" id="your_bank_account_number"').'
    </div>
    </div>
';

$api_bank_in_form .= '   
    <div class="col-md-6 '.$temp_padding.'">                                 
    <div class="form-group">
        '.lang('our_company_bank_account_number', 'our_company_bank_account_number').'
        '.form_input('our_company_bank_account_number', '', 'class="form-control" id="our_company_bank_account_number" readonly="readonly"').'
    </div>
    </div>
';

$api_bank_in_form .= '                                    
    <div class="col-md-6 '.$temp_padding.'">                                 
    <div class="form-group">
        '.lang('transfer_reference_number', 'transfer_reference_number').'
        '.form_input('transfer_reference_number', '', 'class="form-control" id="transfer_reference_number"').'
    </div>
    </div>
';

$api_bank_in_form .= '
</div>
';

echo $api_bank_in_form; 

?>                                                
                                                <div class="form-group">
                                                    <?= lang('comment_any', 'comment'); ?>
                                                    <?= form_textarea('comment', set_value('comment'), 'class="form-control" id="comment" style="height:100px;"'); ?>
                                                </div>
                                                <?php
                                                if (!empty($addresses) && !$this->Staff) {
                                                    echo form_submit('add_order', lang('submit_order'), 'class="btn btn-theme" id="frm_order_submit"');                                                    
                                                } elseif ($this->Staff) {
                                                    echo '<div class="alert alert-warning margin-bottom-no">'.lang('staff_not_allowed').'</div>';
                                                } else {
                                                    echo '<div class="alert alert-warning margin-bottom-no">'.lang('please_add_address_first').'</div>';
                                                }
                                                echo form_close();
                                            } else {
                                                ?>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="well margin-bottom-no">
                                                            <?php  include FCPATH.'themes'.DIRECTORY_SEPARATOR.$Settings->theme.DIRECTORY_SEPARATOR.'shop'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'user'.DIRECTORY_SEPARATOR.'login_form.php';  ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h4 class="title"><span><?= lang('register_new_account'); ?></span></h4>
                                                        <p>
                                                            <?= lang('register_account_info'); ?>
                                                        </p>
                                                        <a href="<?= site_url('login#register'); ?>" class="btn btn-theme"><?= lang('register'); ?></a>
                                                        <a href="#" class="btn btn-default pull-right guest-checkout"><?= lang('guest_checkout'); ?></a>
                                                    </div>
                                                </div>

                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="guest">
                                            <?= shop_form_open('order', 'class="validate" id="guest-checkout"'); ?>
                                            
                                            <input type="hidden" value="1" name="guest_checkout">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <?= lang('name', 'name'); ?> *
                                                                <?= form_input('name', set_value('name'), 'class="form-control" id="name" required="required"'); ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <?= lang('company', 'company'); ?>
                                                                <?= form_input('company', set_value('company'), 'class="form-control" id="company"'); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <?= lang('email', 'email'); ?> *
                                                        <?= form_input('email', set_value('email'), 'class="form-control" id="email" required="required"'); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <?= lang('phone', 'phone'); ?> *
                                                        <?= form_input('phone', set_value('phone'), 'class="form-control" id="phone" required="required"'); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <h5><strong><?= lang('billing_address'); ?></strong></h5>
                                                    <input type="hidden" value="new" name="address">
                                                    <hr>
                                                    <div class="form-group">
                                                        <?= lang('line1', 'billing_line1'); ?> *
                                                        <?= form_input('billing_line1', set_value('billing_line1'), 'class="form-control" id="billing_line1" required="required"'); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <?= lang('line2', 'billing_line2'); ?>
                                                        <?= form_input('billing_line2', set_value('billing_line2'), 'class="form-control" id="billing_line2" required="required"'); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <?= lang('city', 'billing_city'); ?> *
                                                                <?= form_input('billing_city', set_value('billing_city'), 'class="form-control" id="billing_city" required="required"'); ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <?= lang('postal_code', 'billing_postal_code'); ?>
                                                                <?= form_input('billing_postal_code', set_value('billing_postal_code'), 'class="form-control" id="billing_postal_code"'); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <?= lang('state', 'billing_state'); ?>
                                                        <?php
                                                        if ($Settings->indian_gst) {
                                                            $states = $this->gst->getIndianStates();
                                                            echo form_dropdown('billing_state', $states, '', 'class="form-control selectpicker mobile-device" id="billing_state" title="Select" required="required"');
                                                        } else {
                                                            echo form_input('billing_state', '', 'class="form-control" id="billing_state"');
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <?= lang('country', 'billing_country'); ?> *
                                                        <?= form_input('billing_country', set_value('billing_country'), 'class="form-control" id="billing_country" required="required"'); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="checkbox bg pull-right" style="margin-top: 0; margin-bottom: 0;">
                                                        <label>
                                                            <input type="checkbox" name="same" value="1" id="same_as_billing">
                                                            <span>
                                                                <?= lang('same_as_billing') ?>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <h5><strong><?= lang('shipping_address'); ?></strong></h5>
                                                    <input type="hidden" value="new" name="address">
                                                    <hr>
                                                    <div class="form-group">
                                                        <?= lang('line1', 'shipping_line1'); ?> *
                                                        <?= form_input('shipping_line1', set_value('shipping_line1'), 'class="form-control" id="shipping_line1" required="required"'); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <?= lang('line2', 'shipping_line2'); ?>
                                                        <?= form_input('shipping_line2', set_value('shipping_line2'), 'class="form-control" id="shipping_line2" required="required"'); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <?= lang('city', 'shipping_city'); ?> *
                                                                <?= form_input('shipping_city', set_value('shipping_city'), 'class="form-control" id="shipping_city" required="required"'); ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <?= lang('postal_code', 'shipping_postal_code'); ?>
                                                                <?= form_input('shipping_postal_code', set_value('shipping_postal_code'), 'class="form-control" id="shipping_postal_code"'); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <?= lang('state', 'shipping_state'); ?>
                                                        <?php
                                                        if ($Settings->indian_gst) {
                                                            $states = $this->gst->getIndianStates();
                                                            echo form_dropdown('shipping_state', $states, '', 'class="form-control selectpicker mobile-device" id="shipping_state" title="Select" required="required"');
                                                        } else {
                                                            echo form_input('shipping_state', '', 'class="form-control" id="shipping_state"');
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <?= lang('country', 'shipping_country'); ?> *
                                                        <?= form_input('shipping_country', set_value('shipping_country'), 'class="form-control" id="shipping_country" required="required"'); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <?= lang('phone', 'shipping_phone'); ?> *
                                                        <?= form_input('shipping_phone', set_value('shipping_phone'), 'class="form-control" id="shipping_phone" required="required"'); ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <h5><strong><?= lang('payment_method'); ?></strong></h5>
                                                    <hr>
                                                    <div class="checkbox bg">
                                                        <?php if ($paypal->active) { ?>
                                                        <label style="display: inline-block; width: auto;">
                                                            <input type="radio" name="payment_method" value="paypal" id="paypal" required="required">
                                                            <span>
                                                                <i class="fa fa-paypal margin-right-md"></i> <?= lang('paypal') ?>
                                                            </span>
                                                        </label>
                                                        <?php } ?>
                                                        <?php if ($skrill->active) { ?>
                                                        <label style="display: inline-block; width: auto;">
                                                            <input type="radio" name="payment_method" value="skrill" id="skrill" required="required">
                                                            <span>
                                                                <i class="fa fa-credit-card-alt margin-right-md"></i> <?= lang('skrill') ?>
                                                            </span>
                                                        </label>
                                                        <?php } ?>

                                                        <label style="display: inline-block; width: auto;">
                                                            <input type="radio" name="payment_method" value="bank" id="bank" required="required">
                                                            <span>
                                                                <i class="fa fa-bank margin-right-md"></i> <?= lang('bank_in') ?>
                                                            </span>
                                                        </label>

                                                        <label style="display: inline-block; width: auto;">
                                                            <input type="radio" name="payment_method" value="cod" id="cod" required="required">
                                                            <span>
                                                                <i class="fa fa-money margin-right-md"></i> <?= lang('cod') ?>
                                                            </span>
                                                        </label>
                                                    </div>
                                                </div>


                                            </div>
                                            <?= form_submit('guest_order', lang('submit'), 'class="btn btn-lg btn-primary"'); ?>
                                            <?= form_close(); ?>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div id="sticky-con" class="margin-top-lg">
                            <div class="panel panel-default">
                                <div class="panel-heading text-bold">
                                    <i class="fa fa-shopping-cart margin-right-sm"></i> <?= lang('totals'); ?>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-striped table-borderless cart-totals margin-bottom-no">
                                        <tr>
                                            <td><?= lang('total'); ?></td>
                                            <td class="text-right"><?= $this->sma->formatMoney($total, $selected_currency->symbol); ?></td>
                                        </tr>
                                        <?php if ($Settings->tax2 !== false) {
                                            echo '<tr><td>'.lang('order_tax').'</td><td class="text-right">'.$this->sma->formatMoney($order_tax, $selected_currency->symbol).'</td></tr>';
                                        } ?>
                                        <tr>
                                            <td><?= lang('shipping'); ?> *</td>
                                            <td class="text-right"><?= $this->sma->formatMoney($shipping, $selected_currency->symbol); ?></td>
                                        </tr>
                                        <tr><td colspan="2"></td></tr>
                                        <tr class="active text-bold">
                                            <td><?= lang('grand_total'); ?></td>
                                            <td class="text-right"><?= $grand_total_label; ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php /* <code class="text-muted">* <?= lang('shipping_rate_info'); ?></code>*/?>
            </div>
        </div>
    </div>
</section>

<link href="<?php echo base_url(); ?>assets/datetimepicker_master/bootstrap/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
            
<script type="text/javascript" src="<?php echo base_url(); ?>assets/datetimepicker_master/bootstrap/js/jquery-1.8.3.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/datetimepicker_master/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/datetimepicker_master/bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>



<script type="text/javascript">
	$('.delivery_date').datetimepicker({
	    format: 'yyyy-mm-dd hh:ii',
        language:  'en',
    });
function api_bank_in_form_action(action){        
    if (action == 'show') {
        $('#api_bank_in_form').show('toggle');
    }
    if (action == 'hide') {
        $('#api_bank_in_form').hide('toggle');
    }
}
api_bank_in_form_action('hide');

function api_your_bank_change(bank){
    if (bank == 'aba') {
        $('#our_company_bank_account_number').val('<?php echo $settings->aba_account_number; ?>');
    }
    if (bank == 'acleda') {
        $('#our_company_bank_account_number').val('<?php echo $settings->acleda_account_number; ?>');
    }
    if (bank == '') {
        $('#our_company_bank_account_number').val('');
    }
}


</script>

<style>
fieldset.scheduler-border {
    border: 1px groove #ddd !important;
    padding: 10px !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
}

fieldset.scheduler-border {
    border: 1px groove #ddd !important;
    padding: 10px !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow: 0px 0px 0px 0px #000;
    box-shadow: 0px 0px 0px 0px #000;
}
legend.scheduler-border {
    font-size: 1.1em !important;
    font-weight: bold !important;
    text-align: left !important;
    width: auto;
    color: #f44336;
    padding: 5px 15px 0px 15px;
    border: 1px groove #ddd !important;
    margin: 0;
    /* background: #DBDEE0; */
}
.api_checkout_button_fix{
    display:none !important;
}
</style>

