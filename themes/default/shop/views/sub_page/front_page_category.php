<?php
    // $this->sma->print_arrays($list_categories);
    
for ($i=0;$i<count($list_categories);$i++) {
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/'.$list_categories[$i]['image'],
        'max_width' => 500,
        'max_height' => 500,
        'product_link' => 'true',
        'image_class' => 'img-responsive',
        'image_id' => '',   
        'resize_type' => 'full',
    );
    $temp = $this->site->api_get_image($data_view);
    $temp_img = $temp['image_table'];
   
        $temp_category .= '
            <a class="api_link_box_none " href="category/'.$list_categories[$i]['slug'].'">
                <div class="col-md-4 col-sm-4 col-xs-6 api_padding_bottom_15 api_padding_top_15">
                    <div class="col-md-12  api_padding_0">
                        <div class="bg_front" >
                        '.$list_categories[$i]['title_'.$this->api_shop_setting[0]['api_lang_key']].'
                        
                        </div>
                    </div>
                    '.$temp_img.'
                </div>
            </a> 
             
    ';
        
}
// echo $list_categories[1]['slug'];
    echo '
        <section class="page-contents api_padding_bottom_0_im">
            <div class="container api_padding_0">
                '.$temp_category.' 
            </div> 
        </section>
    ';

?>

<style>
.bg_front{
    background-image: url(assets/images/opacity.png);
    background-repeat: repeat;
    padding: 10px; 
    position: absolute;
    top:0; 
    width: 100%; 
    font-size: 24px;
    text-align: center;
    color: #fff;
    text-transform: uppercase;
    font-weight: bold;
}
.api_link_box_none:hover{
    background-color: #979799;
}
@media screen and (min-width: 375px) and (max-width:812px) {
    .bg_front{
        font-size: 15px;
    }
    .col-xs-6{
        padding-right: 5px;
        padding-left: 6px;
    }
}
</style>