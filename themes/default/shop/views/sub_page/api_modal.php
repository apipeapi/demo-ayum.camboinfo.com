<?php
echo '
<button type="button" id="api_modal_trigger" class="api_display_none" data-toggle="modal" data-target="#api_modal">Open Modal</button>
<!-- Modal -->
<div id="api_modal" class="modal fade" role="dialog" style="margin-top:100px;">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="api_modal_title">
        </h4>
      </div>
      <div class="modal-body" id="api_modal_body">
      </div>
      <div class="modal-footer" id="api_modal_footer">
      </div>
    </div>
  </div>
</div>
';
?>