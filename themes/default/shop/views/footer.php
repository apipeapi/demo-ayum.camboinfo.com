<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<?php
    $l = $this->api_shop_setting[0]['api_lang_key'];
    include('themes/default/shop/views/api_template/footer/'.$this->api_shop_setting[0]['footer'].'.php');
?>


<script type="text/javascript">
<?php
    echo 'var api_vat = "'.$customer->vat_no.'";';
    if ($customer->vat_no) {
        if ($customer->vat_invoice_type != 'do') {
            echo '
                api_vat = "'.$customer->vat_no.'";
            ';
        } else {
            echo '
                api_vat = "";
            ';
        }
    }
?>    
    if (api_vat != '') api_vat = 10; else api_vat = 0;
    var m = '<?= $m; ?>', v = '<?= $v; ?>', products = {}, filters = <?= isset($filters) && !empty($filters) ? json_encode($filters) : '{}'; ?>, shop_color, shop_grid, sorting;
    var cart = <?= isset($cart) && !empty($cart) ? json_encode($cart) : '{}' ?>;
    // console.log(cart);
    var site = {base_url: '<?= base_url(); ?>', site_url: '<?= site_url('/'); ?>', shop_url: '<?= shop_url(); ?>', csrf_token: '<?= $this->security->get_csrf_token_name() ?>', csrf_token_value: '<?= $this->security->get_csrf_hash() ?>', settings: {display_symbol: '<?= $Settings->display_symbol; ?>', symbol: '<?= $Settings->symbol; ?>', decimals: <?= $Settings->decimals; ?>, thousands_sep: '<?= $Settings->thousands_sep; ?>', decimals_sep: '<?= $Settings->decimals_sep; ?>', order_tax_rate: false, products_page: <?= $shop_settings->products_page ? 1 : 0; ?>}, shop_settings: {private: <?= $shop_settings->private ? 1 : 0; ?>, hide_price: <?= $shop_settings->hide_price ? 1 : 0; ?>}}

    var lang = {};
    lang.page_info = '<?= lang('page_info'); ?>';
    lang.cart_empty = '<?= lang('empty_cart'); ?>';
    lang.item = '<?= lang('item'); ?>';
    lang.items = '<?= lang('items'); ?>';
    lang.unique = '<?= lang('unique'); ?>';
    lang.total_items = '<?= lang('total_items'); ?>';
    lang.total_unique_items = '<?= lang('total_unique_items'); ?>';
    lang.tax = '<?= lang('tax'); ?>';
    lang.shipping = '<?= lang('shipping'); ?>';
    lang.total_w_o_tax = '<?= lang('total_w_o_tax'); ?>';
    lang.product_tax = '<?= lang('product_tax'); ?>';
    lang.order_tax = '<?= lang('order_tax'); ?>';
    lang.total = '<?= lang('total'); ?>';
    lang.grand_total = '<?= lang('grand_total'); ?>';
    lang.reset_pw = '<?= lang('forgot_password?'); ?>';
    lang.type_email = '<?= lang('type_email_to_reset'); ?>';
    lang.submit = '<?= lang('submit'); ?>';
    lang.error = '<?= lang('error'); ?>';
    lang.add_address = '<?= lang('add_address'); ?>';
    lang.update_address = '<?= lang('update_address'); ?>';
    lang.fill_form = '<?= lang('fill_form'); ?>';
    lang.already_have_max_addresses = '<?= lang('already_have_max_addresses'); ?>';
    lang.send_email_title = '<?= lang('send_email_title'); ?>';
    lang.message_sent = '<?= lang('message_sent'); ?>';
    lang.add_to_cart = '<?= lang('add_to_cart'); ?>';
    lang.out_of_stock = '<?= lang('out_of_stock'); ?>';
    lang.x_product = '<?= lang('x_product'); ?>';
    lang.are_you_sure = '<?= lang('are_you_sure');?>';
    lang.you_will_order_item_first_time = '<?= lang('first_time_order');?>';
    lang.remove_from_favorite_list = '<?= lang('remove_from_favorite_list');?>';
    lang.validation_form = '<?= lang('validation_form'); ?>';
   // lang.reset_pw = '<?=lang('resert_pw');?>';
    lang.type_email = '<?= lang('type_email'); ?>';
    
</script>
<?php if ($m == 'shop' && $v == 'product') { ?>
<script type="text/javascript">
$(document).ready(function ($) {
  $('.rrssb-buttons').rrssb({
    title: '<?= $product->code.' - '.$product->name; ?>',
    url: '<?= site_url('product/'.$product->slug); ?>',
    image: '<?= base_url('assets/uploads/'.$product->image); ?>',
    description: '<?= $page_desc; ?>',
    // emailSubject: '',
    // emailBody: '',
  });
});
</script>
<?php } ?>
<script type="text/javascript">
<?php if ($message || $warning || $error || $reminder) { ?>
$(document).ready(function() {
    
    <?php if ($message) { ?>
        sa_alert('<?=lang('success');?>', '<?= trim(str_replace(array("\r","\n","\r\n"), '', addslashes($message))); ?>');
    <?php } if ($warning) { ?>
        sa_alert('<?=lang('warning');?>', '<?= trim(str_replace(array("\r","\n","\r\n"), '', addslashes($warning))); ?>', 'warning');
    <?php } if ($error) { ?>
        sa_alert('<?=lang('error');?>', '<?= trim(str_replace(array("\r","\n","\r\n"), '', addslashes($error))); ?>', 'error', 1);
    <?php } if ($reminder) { ?>
        
        sa_alert('<?=lang('reminder');?>', '<?= trim(str_replace(array("\r","\n","\r\n"), '', addslashes($reminder))); ?>', 'info');
        
    <?php } ?>
});
<?php } ?>
    let o_url = '<?=site_url();?>';
    function a_btn(href, class_name, name,style=""){
        return '<a href="'+href+'" class="'+class_name+'" style="'+style+'">'+name+'</a>';
    }
    function alert_auth(){
        swal({
            title: '<?php echo lang('login_first');?>',
            type:'warning',
            html: '<p style="font-size:15px"><?php echo lang('You can register with us or with socials'); ?></p> <br/><div style="margin-top:10px">'
            +a_btn(o_url+'social_auth/login/Facebook','btn','<i class="fa fa-facebook"></i> facebook','background-color:rgb(76, 103, 161);border-color:rgb(76, 103, 161);margin:4px 4px;border-radius:20px;color:white;width:120px')
            +a_btn(o_url+'social_auth/login/Google','btn btn-danger','<i class="fa fa-google"></i> Google','margin:4px 4px;background-color:rgb(223, 76, 57);border-color:rgb(223, 76, 57);border-radius:20px;color:white;width:120px')+'</div>'
            +'<div style="padding-top:50px">'+a_btn(o_url+'login','btn btn-danger','<?php echo lang('login');?>','margin:4px 4px;background-color:rgb(76, 175, 80);border-color:rgb(76, 175, 80);border-radius:4px;color:white;width:120px')
            +a_btn(o_url+'login?register=1','btn btn-danger','<?php echo lang('registers');?>','margin:4px 4px;background-color:rgb(0, 188, 212);border-color:rgb(0, 188, 212);border-radius:4px;color:white;width:120px')+'</div>',
            showCancelButton: false,
            showConfirmButton: false
        }).catch(swal.noop);
    }
    function alert_auth_no_social(){
        swal({
            title: '<?php echo lang('login_first');?>',
            type:'warning',
            html:'<div style="padding-top:50px">'+a_btn(o_url+'login','btn btn-danger','<?php echo lang('login');?>','margin:4px 4px;background-color:rgb(76, 175, 80);border-color:rgb(76, 175, 80);border-radius:4px;color:white;width:120px')
            +a_btn(o_url+'login?register=1','btn btn-danger','<?php echo lang('registers');?>','margin:4px 4px;background-color:rgb(0, 188, 212);border-color:rgb(0, 188, 212);border-radius:4px;color:white;width:120px')+'</div>',
            showCancelButton: false,
            showConfirmButton: false
        }).catch(swal.noop);
    }    
    var login_first = function () {
        var tmp = null;
        $.ajax({
            async: false,
            global: false,
            url: "<?=site_url('/is_login');?>",
            success: function (data) {
                tmp = data;
            }
        });
        return tmp == 1?true:false;
    }();
    $('button.add-to-cart').click(function(){
        if(login_first===false){
            alert_auth_no_social();
            return false;
        }else{
            return true;
        }
    })
</script>
<?php
    
    $temp_display = '';
    include 'themes/default/shop/views/api/sub_page/api_ayum_for_companies_contact_form.php';
    echo $temp_display;

    $temp_display = '';
    include 'themes/default/shop/views/api/sub_page/api_ayum_for_jobseeker_contact_form.php';
    echo $temp_display;

    include 'assets/api/page/admin/api_admin_modal.php';

    echo $this->api_shop_setting[0]['api_temp_admin_modal'];
echo '
<script>
    '.$this->api_shop_setting[0]['api_temp_admin_script'].'
</script>
';
    
    echo '    
        <script src="'.base_url().'/assets/api/page/default/js/script.js?v='.$this->api_shop_setting[0]['version'].'"></script>
    ';   

    include 'themes/default/shop/views/sub_page/api_style_mobile.php';
    echo '<script src="'.base_url().'assets/api/js/public.js?v='.$this->api_shop_setting[0]['version'].'"></script>';
    echo '<script src="'.$assets.'js/scripts.min.js?v='.$this->api_shop_setting[0]['version'].'"></script>';
    echo '<script src="'.base_url().'assets/api/page/admin/js/script.js?v='.$this->api_shop_setting[0]['version'].'"></script>';

?>

<script type="text/javascript">
    $(window).bind("load", function () {
        $('#loader-wrapper').fadeOut(1000);
    });
</script>

</body>
</html>





