<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>



<?php 
    $l = $this->api_shop_setting[0]['api_lang_key'];;
    $temp_page_name = 'front';
    echo '
<link href="'.base_url().'/assets/api/page/front/css/front.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/front/css/front_600.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/front/css/front_768.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/front/css/front_1024.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
    ';
    $config_data = array(
        'table_name' => 'sma_blog',
        'type' => 'blog',
        'count' => 10,
    );
    //$this->api_helper->api_sample_data($config_data);

    include 'themes/default/shop/views/api/sub_page/api_front_ayum_banner.php';

    echo '<div class="api_height_30 api_clear_both" id="Makara-Video"></div>';

    include 'themes/default/shop/views/api/sub_page/api_front_ayum_movie.php';

    echo '<div class="api_height_50 api_clear_both" id="Service"></div>';

    include 'themes/default/shop/views/api/sub_page/api_front_ayum_service.php';
    
    echo '<div class="api_height_50 api_clear_both" id="Service-List"></div>';

    include 'themes/default/shop/views/api/sub_page/api_front_ayum_service_list.php';
        
    echo '<div class="api_height_50 api_clear_both" id="Our-Member"></div>';

    include 'themes/default/shop/views/api/sub_page/api_front_ayum_member.php';

    echo '<div class="api_height_50 api_clear_both"></div>';

    include 'themes/default/shop/views/api/sub_page/api_front_ayum_blog.php';

    echo '<div class="api_height_30 api_clear_both"></div>';

echo '    
    <script  src="'.base_url().'/assets/api/page/'.$temp_page_name.'/js/script.js?v='.$this->api_shop_setting[0]['version'].'"></script>
';   

?>

