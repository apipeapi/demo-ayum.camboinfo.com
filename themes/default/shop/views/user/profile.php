<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="page-contents api_padding_0_mobile">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-9 col-md-10 api_padding_right_0_pc">
                        
                        <div class="col-md-12 api_padding_0_mobile">
                            <h4 class="margin-top-md title text-bold">
                                <span><i class="fa fa-user margin-right-sm"></i> <?= lang('profile'); ?></span>            
                                <div id="api_customer_menu_panel_btn" class="api_float_right api_pointer api_screen_show_768" onclick="$('#api_customer_menu_panel').toggle('slideToggle?');" style="top: -10px; margin-top: -15px;">
                                    <button class="btn btn-danger navbar-btn"><div class="fa fa-bars fa-lg"></div></button>
                                </div>
                            </h4>
                        </div>

                        <div id="api_customer_menu_panel" class="col-md-2 api_padding_right_0_pc api_padding_0_mobile api_screen_hide_768">
                            <div class="api_height_10 api_screen_hide_768"></div>
                            <?php include('themes/default/shop/views/pages/customer_menu_panel.php'); ?>                            
                        </div>
                        <div class="col-md-10 api_padding_0_mobile">
                            <div class="api_height_13"></div>
                            
<?php
    if ($this->session->userdata('user_id') > 0) {
        $temp = $this->site->api_select_some_fields_with_where("
            activation_code, active
            "
            ,"sma_users"
            ,"id = ".$this->session->userdata('user_id')
            ,"arr"
        );        
    }

    if ($this->api_shop_setting[0]['require_login'] != 1) {
        $temp_display_1 = 'api_display_none';
        $temp_display_2 = '<label class="label label-danger api_float_right">'.lang('We_can_delivery_in_Phnom_Penh_Only').'</label>';
    }

?>                            
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#user" aria-controls="user" role="tab" data-toggle="tab" ><?= lang('details'); ?></a></li>
        <li role="presentation"><a href="#password" aria-controls="password" role="tab" data-toggle="tab" ><?= lang('change_password'); ?></a></li>
        <?php if ($temp[0]['active'] != 1 && $this->api_shop_setting[0]['require_login'] != 1) { ?>
        <li role="presentation"><a href="#verify_email" aria-controls="verify_email" role="tab" data-toggle="tab" ><?= lang('Verify_Email_Address'); ?></a></li>
        <?php } ?>
    </ul>
    <div class="tab-content padding-lg white bordered-light" style="margin-top:-1px;">
        <div role="tabpanel" class="tab-pane fade in active" id="user">
            <p><?= lang('fill_form'); ?></p>
            <?= form_open("profile/user", 'class="validate"'); ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('first_name', 'first_name'); ?>
                        <?= form_input('first_name', set_value('first_name', $user->first_name), 'class="form-control tip" id="first_name" required="required"'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('last_name', 'last_name'); ?>
                        <?= form_input('last_name', set_value('last_name', $user->last_name), 'class="form-control tip" id="last_name" required="required"'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('phone', 'phone'); ?>
                        <?= form_input('phone', set_value('phone', $customer->phone), 'class="form-control tip" id="phone" required="required"'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('email', 'email'); ?>
                        <?= form_input('email', set_value('email', $customer->email), 'class="form-control tip"  id="email" readonly="readonly"'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group <?= $temp_display_1; ?>">
                        <?= lang('company', 'company'); ?>
                        <?= form_input('company', set_value('company', $customer->company), 'class="form-control tip" id="company"'); ?>
                    </div>                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 api_display_none">
                    <div class="form-group">
                        <?= lang("city", "city"); ?>
                        <?= form_input('city', set_value('city', $customer->city), 'class="form-control tip" id="city"'); ?>
                    </div>
                </div>
                <div class="col-md-12 color_red_change">
                    <div class="form-group">
                        <?= lang("city", "city"); ?>
                        <?php
                            $config_data = array(
                                'none_label' => lang("Select_a_city"),
                                'table_name' => 'sma_city',
                                'space' => ' &rarr; ',
                                'strip_id' => '',        
                                'field_name' => 'title_'.$this->api_shop_setting[0]['api_lang_key'],
                                'condition' => 'order by title_'.$this->api_shop_setting[0]['api_lang_key'].' asc',
                                'condition_parent' => ' and parent_id = 268',
                                'translate' => 'yes',
                                'no_space' => 1,
                            );                        
                            $this->site->api_get_option_category($config_data);
                            $temp_option = $_SESSION['api_temp'];
                            for ($i=0;$i<count($temp_option);$i++) {                        
                                $temp = explode(':{api}:',$temp_option[$i]);
                                $temp_10 = '';
                                if ($temp[0] != '') {
                                    $config_data_2 = array(
                                        'id' => $temp[0],
                                        'table_name' => 'sma_city',
                                        'field_name' => 'title_'.$this->api_shop_setting[0]['api_lang_key'],
                                        'parent_id' => '268',
                                        'translate' => 'yes',
                                    );
                                    $_SESSION['api_temp'] = array();
                                    $this->site->api_get_category_arrow($config_data_2);          
                                    for ($i2 = count($_SESSION['api_temp']) - 1; ; $i2--) {
                                        if ($i2 == 0) {
                                            break;
                                        }
                                        $temp_arrow = '';
                                        if ($i2 > 1)
                                            $temp_arrow = ' &rarr; ';
                                        $temp_10 .= $_SESSION['api_temp'][$i2].$temp_arrow;
                                    }   
                                }
                                $tr_city[$temp[0]] = $temp_10.$temp[1];
                            }
                            echo form_dropdown('city_id', $tr_city, $customer->city_id, 'class="form-control"');
                        ?>
                    </div>
                </div>
                <div class="col-md-6 api_display_none ">
                    <div class="form-group">
                        <?= lang("state", "state"); ?>
                        <?php
                        if ($Settings->indian_gst) {
                            $states = $this->gst->getIndianStates(true);
                            echo form_dropdown('state', $states, set_value('state', $customer->state), 'class="form-control selectpicker mobile-device" id="state" ');
                        } else {
                            echo form_input('state', set_value('state', $customer->state), 'class="form-control" id="state"');
                        }
                        ?>
                    </div>
                </div>                
            </div>

            <div class="row">
                <div class="col-md-6 api_display_none">
                    <div class="form-group">
                        <?= lang('country', 'country'); ?>
                        <?= form_input('country', set_value('country', $customer->country), 'class="form-control tip" id="country" '); ?>
                    </div>
                </div>
                
                
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group color_red_change">
                        <?= lang('Delivery_Address', 'address'); ?>
                        <?= form_textarea('address', $customer->address, 'class="form-control  tip" id="address" required="required" style="height:100px;"'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('postal_code', 'postal_code'); ?>
                        <?= form_input('postal_code', set_value('postal_code', $customer->postal_code), 'class="form-control tip" id="postal_code" '); ?>
                    </div>
                </div>                
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('google_map_ping_location', 'google_map_ping_location'); ?>
                        <?= form_input('add_ons_map_coordinate', $customer_v2[0]['map_coordinate'], 'class="form-control tip" '); ?>
                    </div>
                </div>   

                <?php
                    if ($this->api_shop_setting[0]['map_coordinate'] != '' && $customer_v2[0]['map_coordinate'] != '') {
                        $config_data = array(
                            'map_css' => 'width:100%; height:400px;',
                            'origin_coordinate' => $this->api_shop_setting[0]['map_coordinate'],
                            'destination_coordinate' => $customer_v2[0]['map_coordinate'],
                        );
                        $temp_display = '';
                        include 'themes/default/shop/views/sub_page/api_google_map_distance.php';
                        echo '
                            <div class="col-sm-12"> 
                                '.$temp_display.'
                            </div>
                        ';
                    }
                ?>

                <?php
                    $config_data = array(
                        'table_name' => 'sma_companies',
                        'select_table' => 'sma_companies',
                        'translate' => '',
                        'select_condition' => "id = ".$customer->id,
                    );
                    $select_data = $this->site->api_select_data_v2($config_data);
                ?>
                <?php
                echo '
                    <div class="form-group col-sm-4">
                        <div class="form-group">
                            '.lang(' ',' ').'
                            <div class="api_padding_top_15">
                                '.lang('distance_from_our_restaurant').' : 
                                <span id="distance_from_our_restaurant">
                                </span>
                            </div>
                        </div>
                    </div>
                ';                
                echo '
                    <div class="form-group col-sm-4">
                        <div class="form-group">
                            '.lang(' ',' ').'
                            <div class="api_padding_top_15">
                                '.lang('delivery_fee').' : 
                                <span id="address_delivery_fee">
                                </span>
                            </div>
                        </div>
                    </div>
                ';
                echo '
                    <div class="form-group col-sm-4">
                        <div class="form-group">
                            '.lang(' ',' ').'
                            <div class="api_padding_top_15">
                                '.lang('discount').' : 
                                <span id="address_discount">
                                </span>
                            </div>
                        </div>
                    </div>
                ';
                ?>
                    
            </div>
            <?= $temp_display_2; ?> 

            <?= form_submit('billing', lang('update'), 'class="btn btn-primary"'); ?>
            <?php echo form_close(); ?>        
        </div>

        <div role="tabpanel" class="tab-pane fade" id="password">
            <p><?= lang('fill_form'); ?></p>
            <?= form_open("profile/password", 'class="validate"'); ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('current_password', 'old_password'); ?>
                        <?= form_password('old_password', set_value('old_password'), 'class="form-control tip" id="old_password" required="required"'); ?>
                    </div>

                    <div class="form-group">
                        <?= lang('user_new_password', 'user_new_password'); ?>
                        <?= form_password('new_password', set_value('new_password'), 'class="form-control tip" id="new_password" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-fv-regexp-message="'.lang('pasword_hint').'"'); ?>
                    </div>

                    <div class="form-group">
                        <?= lang('confirm_password', 'new_password_confirm'); ?>
                        <?= form_password('new_password_confirm', set_value('new_password_confirm'), 'class="form-control tip" id="new_password_confirm" required="required" data-fv-identical="true" data-fv-identical-field="new_password" data-fv-identical-message="'.lang('pw_not_same').'"'); ?>
                    </div>

<?php
    echo '
        <button type="button" class="btn btn-default" onclick="
        var postData = {
            \'website_address\' : \''.base_url().'\',
            \'close\' : \''.lang('Close').'\',
            \'use_this_password\' : \''.lang('use_this_password').'\',
            \'generated_password\' : \''.lang('generated_password').'\',
        };        
        api_generate_password(postData);
        ">
            '.lang('Generate_Password').'
        </button>
        <div class="api_height_30"></div>
    ';
?>
                    <?= form_submit('change_password', lang('change_password'), 'class="btn btn-primary"'); ?>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>

        <?php if ($temp[0]['active'] != 1 && $this->api_shop_setting[0]['require_login'] != 1) { ?>
        <div role="tabpanel" class="tab-pane fade" id="verify_email">
            <div class="row">
                <div class="col-md-12">
                    <?php
                        echo '
                            <b>'.lang('Email_Activation_Link').'</b>
                            <br><br>
                            '.lang('text_activation') .' <b>'. $this->session->userdata('email').'</b> '.lang('text_activation_2').'
                            <br><br>
                            '.lang('text_activation_3').'
                            <br><br>
                            <a href="'.base_url().'shop/api_resend_email_activation_link">
                                <button class="btn btn-primary">
                                    '.lang('Resend_Email_Activation_Link').'
                                </button>
                            </a>
                        ';
                    ?>

                </div>
            </div>
        </div>
        <?php } ?>
        
    </div>


                        </div>
                    </div>

                    <div class="col-sm-3 col-md-2">
                        <?php include(b'themes/default/shop/views/pages/sidebar2.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script>

function api_use_this_password(){
    document.getElementById('new_password').value = document.getElementById('api_modal_body').innerHTML;
    document.getElementById('new_password_confirm').value = document.getElementById('api_modal_body').innerHTML;
}
</script>