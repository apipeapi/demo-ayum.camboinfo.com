<?php
$temp_display = '';
$l = $this->api_shop_setting[0]['api_lang_key'];
if($l == 'ja') 
    $lang_font_size = 'style="font-size:20px;line-height: 1.3;"';
else $lang_font_size = 'style="font-size:15px;line-height: 1.3em;"';
$temp_display = '
    <div class="col-md-14.28">
        <div class="api_text_align_center">
            <div class="api_large api_title_1 api_template_title_1">
';
$temp_admin = $this->api_helper->api_lang_v2('SERVICE LIST FRONT');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
            </div>    
            <div class="api_height_10"></div>
            <div class="api_text_transform_capitalize api_template_title_2">
';
$temp_admin = $this->api_helper->api_lang_v2('Service list');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
            </div>  
            <div class="api_height_30"></div>    
        </div>
    </div>

    <div class="col-md-14.28 api_padding_10">
        <div class="hidden-sm hidden-xs">
            <table class="api_temp_table_1 api_margin_auto" width="100%" cell border="0" >
                <tr>
                    <td colspan="4" align="center">
                        <div class="api_temp_cell_padding_1">
                            <div style="background-color:#efefef; padding:5px;">
';
$temp_admin = $this->api_helper->api_lang_v2('Adoption support areas');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                            </div>
                        </div>
                    </td>
                    <td colspan="2" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="background-color:#efefef; padding:5px;">
';
$temp_admin = $this->api_helper->api_lang_v2('Education / retention support area');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '   
                            </div>
                        </div>      
                    </td>
                    <td align="center">
                        <div class="api_temp_cell_padding_3">
                            <div style="background-color:#efefef; padding:5px;">
';
$temp_admin = $this->api_helper->api_lang_v2('Area for Japan');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                            </div>
                        </div>    
                    </td>
                </tr>

                <tr>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_1">
                            <div style="width:100%; height:70px;display:table;background-color:#6399f0; padding:5px;">                
                                <div class="api_display_table_cell api_vertical_align_middle">                       
                                    <div class="api_padding_top_15">
';
$temp_admin = $this->api_helper->api_lang_v2('Think');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                    </div>
                                    <div class="api_temp_border_bottom_1"></div>
                                    <div class="api_height_40" '.$lang_font_size.'>
';
$temp_admin = $this->api_helper->api_lang_v2('Strategic rules');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                    </div>
                                </div>
                            </div>
                        </div>                  
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:70px;display:table;background-color:#f89196; padding:5px;">                
                                <div class="api_display_table_cell api_vertical_align_middle">                      
                                    <div class="api_padding_top_15">
';
$temp_admin = $this->api_helper->api_lang_v2('Fascinate');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                    </div>
                                    <div class="api_temp_border_bottom_1"></div>
                                    <div class="api_height_40" '.$lang_font_size.'>
';
$temp_admin = $this->api_helper->api_lang_v2('Recruitment public relations v2');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                    </div>
                                </div>
                            </div>
                        </div>                   
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:70px;display:table;background-color:#b8a2d9; padding:5px;">                
                                <div class="api_display_table_cell api_vertical_align_middle">                      
                                    <div class="api_padding_top_15">
';
$temp_admin = $this->api_helper->api_lang_v2('Gather');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                    </div>
                                    <div class="api_temp_border_bottom_1"></div>
                                    <div class="api_height_40" '.$lang_font_size.'>
';
$temp_admin = $this->api_helper->api_lang_v2('Population formation');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                    </div>
                                </div>
                            </div>
                        </div>              
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:70px;display:table;background-color:#acdca5; padding:5px;">                
                                <div class="api_display_table_cell api_vertical_align_middle">                      
                                    <div class="api_padding_top_15">
';
$temp_admin = $this->api_helper->api_lang_v2('Identify');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                    </div>
                                    <div class="api_temp_border_bottom_1"></div>
                                    <div class="api_height_40" '.$lang_font_size.'>
';
$temp_admin = $this->api_helper->api_lang_v2('Selection');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                    </div>
                                </div>
                            </div>
                        </div>             
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:70px;display:table;background-color:#ffca95; padding:5px;">                
                                <div class="api_display_table_cell api_vertical_align_middle">                      
                                    <div class="api_padding_top_15">
';
$temp_admin = $this->api_helper->api_lang_v2('Grow');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                    </div>
                                    <div class="api_temp_border_bottom_1"></div>
                                    <div class="api_height_40" '.$lang_font_size.'>
';
$temp_admin = $this->api_helper->api_lang_v2('training');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                    </div>
                                </div>
                            </div>
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:70px;display:table;background-color:#68a7b1; padding:5px;">                
                                <div class="api_display_table_cell api_vertical_align_middle">                      
                                    <div class="api_padding_top_15">
';
$temp_admin = $this->api_helper->api_lang_v2('Arrange');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                    </div>
                                    <div class="api_temp_border_bottom_1"></div>
                                    <div class="api_height_40" '.$lang_font_size.'>
';
$temp_admin = $this->api_helper->api_lang_v2('Organizational improvement');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                    </div>
                                </div>
                            </div>
                        </div>              
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_3">
                            <div style="width:100%; height:70px;display:table;background-color:#ffe78f; padding:5px;">                
                                <div class="api_display_table_cell api_vertical_align_middle">                      
                                    <div class="api_padding_top_15">
';
$temp_admin = $this->api_helper->api_lang_v2('connect');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                    </div>
                                    <div class="api_temp_border_bottom_1"></div>
                                    <div class="api_height_40" '.$lang_font_size.'>
';
$temp_admin = $this->api_helper->api_lang_v2('Internationalization');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                    </div>
                                </div>
                            </div>
                        </div>                 
                    </td>
                </tr>
                <tr>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_1">
                            <div style="width:100%; height:60px;display:table;border:2px solid #6399f0;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
';
$temp_admin = $this->api_helper->api_lang_v2('Adopt design support');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                </div>
                            </div>
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:60px;display:table;border:2px solid #f89196; padding:5px;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
';
$temp_admin = $this->api_helper->api_lang_v2('Recruitment branding');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                </div>
                            </div>                
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:60px;display:table;border:2px solid #b8a2d9;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
';
$temp_admin = $this->api_helper->api_lang_v2('Introduction of human resources');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                </div>
                            </div>                
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:60px;display:table;border:2px solid #acdca5;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
';
$temp_admin = $this->api_helper->api_lang_v2('Selection agency');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                </div>
                            </div>                
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:60px;display:table;border:2px solid #ffca95;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
';
$temp_admin = $this->api_helper->api_lang_v2('Staff training');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                </div>
                            </div>                
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:60px;display:table;border:2px solid #68a7b1;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
';
$temp_admin = $this->api_helper->api_lang_v2('Business improvement design');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                </div>
                            </div>                
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_3">
                            <div style="width:100%; height:60px;display:table;border:2px solid #ffe78f;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
';
$temp_admin = $this->api_helper->api_lang_v2('Interpretation and translation');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                </div>
                            </div>                
                        </div>                 
                    </td>                                                                        
                </tr>
                <tr>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_1">
                            <div style="width:100%; height:60px;display:table;border:2px solid #6399f0;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
';
$temp_admin = $this->api_helper->api_lang_v2('Recruitment strategy support');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                </div>
                            </div>
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:60px;display:table;border:2px solid #f89196; padding:5px;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
';
$temp_admin = $this->api_helper->api_lang_v2('Recruitment public relations');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                </div>
                            </div>                
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:60px;display:table;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
                                    
                                </div>
                            </div>                
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:60px;display:table;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
                                    
                                </div>
                            </div>                
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:60px;display:table;border:2px solid #ffca95;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
';
$temp_admin = $this->api_helper->api_lang_v2('follow up');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                </div>
                            </div>                
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:60px;display:table;border:2px solid #68a7b1;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
';
$temp_admin = $this->api_helper->api_lang_v2('Business survey');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                </div>
                            </div>                
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_3">
                            <div style="width:100%; height:60px;display:table;border:2px solid #ffe78f;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
';
$temp_admin = $this->api_helper->api_lang_v2('Global training');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                </div>
                            </div>                
                        </div>                 
                    </td>                                                                        
                </tr>
                <tr>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_1">
                            <div style="width:100%; height:60px;display:table;border:2px solid #6399f0;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
';
$temp_admin = $this->api_helper->api_lang_v2('Requirement definition support');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                </div>
                            </div>
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:60px;display:table;border:2px solid #f89196; padding:5px;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
';
$temp_admin = $this->api_helper->api_lang_v2('Recruitment document creation');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                </div>
                            </div>                
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:60px;display:table;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
                                    
                                </div>
                            </div>                
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:60px;display:table;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
                                    
                                </div>
                            </div>                
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:60px;display:table;border:2px solid #ffca95;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
';
$temp_admin = $this->api_helper->api_lang_v2('Customized training');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                </div>
                            </div>                
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:60px;display:table;border:2px solid #68a7b1;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
';
$temp_admin = $this->api_helper->api_lang_v2('Labor support');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                </div>
                            </div>                
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_3">
                            <div style="width:100%; height:60px;display:table;border:2px solid #ffe78f;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
';
$temp_admin = $this->api_helper->api_lang_v2('Employment support in Japan');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                </div>
                            </div>                
                        </div>                 
                    </td>                                                                        
                </tr>
                <tr>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_1">
                            <div style="width:100%; height:60px;display:table;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
                                    
                                </div>
                            </div>
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:60px;display:table;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
                                    
                                </div>
                            </div>                
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:60px;display:table;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
                                    
                                </div>
                            </div>                
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:60px;display:table;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
                                    
                                </div>
                            </div>                
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:60px;display:table;border:2px solid #ffca95;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
';
$temp_admin = $this->api_helper->api_lang_v2('Philosophy penetration workshop');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                </div>
                            </div>                
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_2">
                            <div style="width:100%; height:60px;display:table;border:2px solid #68a7b1;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
';
$temp_admin = $this->api_helper->api_lang_v2('Personnel system construction');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                </div>
                            </div>                
                        </div>                 
                    </td>
                    <td width="14.28%" align="center">
                        <div class="api_temp_cell_padding_3">
                            <div style="width:100%; height:60px;display:table;border:2px solid #ffe78f;">
                                <div class="api_display_table_cell api_vertical_align_middle">                
';
$temp_admin = $this->api_helper->api_lang_v2('Field survey support');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                                </div>
                            </div>                
                        </div>                 
                    </td>                                                                        
                </tr>
            </table>
        </div>
        <div class="visible-sm visible-xs">
            <table class="api_temp_table_1" width="100%" cell border="0">
            <tr>
                <td colspan="4" align="center">
                    <div class="api_temp_cell_padding_1">
                        <div style="background-color:#efefef; padding:5px;">
';
$temp_admin = $this->api_helper->api_lang_v2('Adoption support areas');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                        </div>
                    </div>
                </td>
            </tr>   

            <tr>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_1">
                        <div style="width:100%; height:85px;display:table;background-color:#6399f0; padding:5px;">                
                            <div class="api_display_table_cell api_vertical_align_middle">                       
                                <div class="api_padding_top_15">'.$this->api_helper->api_lang('Think').'</div>
                                <div class="api_temp_border_bottom_1"></div>
                                <div class="api_height_40">'.$this->api_helper->api_lang('Strategic rules').'</div>
                            </div>
                        </div>
                    </div>                  
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:85px;display:table;background-color:#f89196; padding:5px;">                
                            <div class="api_display_table_cell api_vertical_align_middle">                      
                                <div class="api_padding_top_15">'.$this->api_helper->api_lang('Fascinate').'</div>
                                <div class="api_temp_border_bottom_1"></div>
                                <div class="api_height_40">'.$this->api_helper->api_lang('Recruitment public relations').'</div>
                            </div>
                        </div>
                    </div>                   
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:85px;display:table;background-color:#b8a2d9; padding:5px;">                
                            <div class="api_display_table_cell api_vertical_align_middle">                      
                                <div class="api_padding_top_15">'.$this->api_helper->api_lang('Gather').'</div>
                                <div class="api_temp_border_bottom_1"></div>
                                <div class="api_height_40">'.$this->api_helper->api_lang('Population formation').'</div>
                            </div>
                        </div>
                    </div>              
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:85px;display:table;background-color:#acdca5; padding:5px;">                
                            <div class="api_display_table_cell api_vertical_align_middle">                      
                                <div class="api_padding_top_15">'.$this->api_helper->api_lang('Identify').'</div>
                                <div class="api_temp_border_bottom_1"></div>
                                <div class="api_height_40">'.$this->api_helper->api_lang('Selection').'</div>
                            </div>
                        </div>
                    </div>             
                </td>
            </tr>    

            <tr>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_1">
                        <div style="width:100%; height:60px;display:table;border:2px solid #6399f0;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                '.$this->api_helper->api_lang('Adopt design support').'
                            </div>
                        </div>
                    </div>                 
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:60px;display:table;border:2px solid #f89196; padding:5px;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                '.$this->api_helper->api_lang('Recruitment branding').'
                            </div>
                        </div>                
                    </div>                 
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:60px;display:table;border:2px solid #b8a2d9;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                '.$this->api_helper->api_lang('Introduction of human resources').'
                            </div>
                        </div>                
                    </div>                 
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:60px;display:table;border:2px solid #acdca5;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                '.$this->api_helper->api_lang('Selection agency').'
                            </div>
                        </div>                
                    </div>                 
                </td>
            </tr>            
            
            <tr>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_1">
                        <div style="width:100%; height:60px;display:table;border:2px solid #6399f0;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                '.$this->api_helper->api_lang('Recruitment strategy support').'
                            </div>
                        </div>
                    </div>                 
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:60px;display:table;border:2px solid #f89196; padding:5px;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                '.$this->api_helper->api_lang('Recruitment public relations').'
                            </div>
                        </div>                
                    </div>                 
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:60px;display:table;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                
                            </div>
                        </div>                
                    </div>                 
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:60px;display:table;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                
                            </div>
                        </div>                
                    </div>                 
                </td>
            </tr>            

            <tr>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_1">
                        <div style="width:100%; height:60px;display:table;border:2px solid #6399f0;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                '.$this->api_helper->api_lang('Requirement definition support').'
                            </div>
                        </div>
                    </div>                 
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:60px;display:table;border:2px solid #f89196; padding:5px;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                '.$this->api_helper->api_lang('Recruitment document creation').'
                            </div>
                        </div>                
                    </div>                 
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:60px;display:table;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                
                            </div>
                        </div>                
                    </div>                 
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:60px;display:table;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                
                            </div>
                        </div>                
                    </div>                 
                </td>
            </tr>            

            <tr>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_1">
                        <div style="width:100%; height:60px;display:table;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                
                            </div>
                        </div>
                    </div>                 
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:60px;display:table;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                
                            </div>
                        </div>                
                    </div>                 
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:60px;display:table;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                
                            </div>
                        </div>                
                    </div>                 
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:60px;display:table;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                
                            </div>
                        </div>                
                    </div>                 
                </td>
            </tr>            
            </table>
            
            <table class="api_temp_table_1" width="100%" cell border="0">
            <tr>
                <td colspan="2" align="center">
                    <div class="api_padding_2">
                    <div class="" style="background-color:#efefef;">
                        <div class="api_display_table_cell api_vertical_align_middle api_padding_5" style="background-color:#efefef; width:100%;  height:45px;">
                            '.$this->api_helper->api_lang('Education / retention support area').'
                        </div>
                    </div>    
                    </div>  
                </td>
                <td class="" align="center">
                    <div class="api_padding_2">
                    <div class="" style="background-color:#efefef;">
                        <div class="api_display_table_cell api_vertical_align_middle api_padding_5" style="background-color:#efefef; width:100%; height:45px;">
                            '.$this->api_helper->api_lang('Area for Japan').'
                        </div>
                    </div>    
                    </div>
                </td>
            </tr>

            <tr>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:70px;display:table;background-color:#ffca95; padding:5px;">                
                            <div class="api_display_table_cell api_vertical_align_middle">                      
                                <div class="api_padding_top_15">'.$this->api_helper->api_lang('Grow').'</div>
                                <div class="api_temp_border_bottom_1"></div>
                                <div class="api_height_40">'.$this->api_helper->api_lang('training').'</div>
                            </div>
                        </div>
                    </div>                 
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:70px;display:table;background-color:#68a7b1; padding:5px;">                
                            <div class="api_display_table_cell api_vertical_align_middle">                      
                                <div class="api_padding_top_15">'.$this->api_helper->api_lang('Arrange').'</div>
                                <div class="api_temp_border_bottom_1"></div>
                                <div class="api_height_40">'.$this->api_helper->api_lang('Organizational improvement').'</div>
                            </div>
                        </div>
                    </div>              
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_3">
                        <div style="width:100%; height:70px;display:table;background-color:#ffe78f; padding:5px;">                
                            <div class="api_display_table_cell api_vertical_align_middle">                      
                                <div class="api_padding_top_15">'.$this->api_helper->api_lang('connect').'</div>
                                <div class="api_temp_border_bottom_1"></div>
                                <div class="api_height_40">'.$this->api_helper->api_lang('Internationalization').'</div>
                            </div>
                        </div>
                    </div>                 
                </td>
            </tr>

            <tr>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:60px;display:table;border:2px solid #ffca95;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                '.$this->api_helper->api_lang('Staff training').'
                            </div>
                        </div>                
                    </div>                 
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:60px;display:table;border:2px solid #68a7b1;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                '.$this->api_helper->api_lang('Business improvement design').'
                            </div>
                        </div>                
                    </div>                 
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_3">
                        <div style="width:100%; height:60px;display:table;border:2px solid #ffe78f;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                '.$this->api_helper->api_lang('Interpretation and translation').'
                            </div>
                        </div>                
                    </div>                 
                </td>                                                                        
            </tr>

            <tr>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:60px;display:table;border:2px solid #ffca95;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                '.$this->api_helper->api_lang('follow up').'
                            </div>
                        </div>                
                    </div>                 
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:60px;display:table;border:2px solid #68a7b1;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                '.$this->api_helper->api_lang('Business survey').'
                            </div>
                        </div>                
                    </div>                 
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_3">
                        <div style="width:100%; height:60px;display:table;border:2px solid #ffe78f;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                '.$this->api_helper->api_lang('Global training').'
                            </div>
                        </div>                
                    </div>                 
                </td>                                                                        
            </tr>

            <tr>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:60px;display:table;border:2px solid #ffca95;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                '.$this->api_helper->api_lang('Customized training').'
                            </div>
                        </div>                
                    </div>                 
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:60px;display:table;border:2px solid #68a7b1;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                '.$this->api_helper->api_lang('Labor support').'
                            </div>
                        </div>                
                    </div>                 
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_3">
                        <div style="width:100%; height:60px;display:table;border:2px solid #ffe78f;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                '.$this->api_helper->api_lang('Employment support in Japan').'
                            </div>
                        </div>                
                    </div>                 
                </td>                                                                        
            </tr>

            <tr>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:60px;display:table;border:2px solid #ffca95;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                '.$this->api_helper->api_lang('Philosophy penetration workshop').'
                            </div>
                        </div>                
                    </div>                 
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_2">
                        <div style="width:100%; height:60px;display:table;border:2px solid #68a7b1;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                '.$this->api_helper->api_lang('Personnel system construction').'
                            </div>
                        </div>                
                    </div>                 
                </td>
                <td width="14.28%" align="center">
                    <div class="api_temp_cell_padding_3">
                        <div style="width:100%; height:60px;display:table;border:2px solid #ffe78f;">
                            <div class="api_display_table_cell api_vertical_align_middle">                
                                '.$this->api_helper->api_lang('Field survey support').'
                            </div>
                        </div>                
                    </div>                 
                </td>                                                                        
            </tr>

            </table>

        </div>
    </div>
';

$config_data = array(
    'wrapper_class' => 'api_front_ayum_service_list',
    'custom_html' => '',
    'display' => $temp_display,
    'type' => '',
);
$temp = $this->api_display->template_display($config_data);
echo $temp['display'];

?>                       

