<?php
$l = $this->api_shop_setting[0]['api_lang_key'];
$temp_display = '';
$config_data = array(
    'table_name' => 'sma_staff',
    'select_table' => 'sma_staff',
    'translate' => 'yes',
    'translate_2' => 'yes',
    'translate_3' => 'yes',
    'description' => 'yes',
    'select_condition' => "id > 0 order by ordering asc",
);
$select_data = $this->api_helper->api_select_data_v2($config_data);

$language_array = unserialize(multi_language);
$select_view = array();
for ($i=0;$i<count($select_data);$i++) {    
    for ($i2=0;$i2<count($language_array);$i2++) {
        $select_view[$i]['title_'.$language_array[$i2][0]] = $select_data[$i]['title_'.$language_array[$i2][0]];
        $select_view[$i]['intro_'.$language_array[$i2][0]] = $this->sma->decode_html($select_data[$i]['description_'.$language_array[$i2][0]]);             
    }    
    $select_view[$i]['id'] = $select_data[$i]['id'];
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/staff/thumb/'.$select_data[$i]['image'],
        'max_width' => 150,
        'max_height' => 150,
        'product_link' => '',
        'image_class' => 'api_border_r50 api_img_staff',
        'image_id' => '',   
        'resize_type' => 'fixed',
    );
    $temp_image = $this->api_helper->api_get_image($data_view);
    //$select_view[$i]['image'] = $temp_image['image_table'];

    $select_view[$i]['image'] = '
    <div class="api_box_150_150">
        <img class="api_border_r50" src="'.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/staff/thumb/'.$select_data[$i]['image'].'" />
    </div>
    ';

    $select_view[$i]['facebook'] = $select_data[$i]['facebook'];
    $select_view[$i]['twitter'] = $select_data[$i]['twitter'];
    $select_view[$i]['linkedin'] = $select_data[$i]['linkedin'];
    $select_view[$i]['position'] = $select_data[$i]['title_2_'.$l];
    $select_view[$i]['type'] = $select_data[$i]['title_3_'.$l];
    $select_view[$i]['link'] = '';
}

$config_data = array();
$temp_config = $this->api_display->large_icon_config($config_data);
$config_data = $temp_config['config_data'];
$config_data['lang_key'] = $this->api_shop_setting[0]['api_lang_key'];
$config_data['select_data'] = $select_view;

$config_data['image_class'] = 'img-responsive api_link_box_none api_border_r50';
$config_data['view_all'] = '';
$config_data['col_class'] = 'col-md-6 col-sm-12 col-xs-12 api_padding_50';
$config_data['file_path'] = 'assets/uploads/web/'.$this->api_web[0]['id'].'/staff';
$config_data['recommended_width'] = 150;
$config_data['recommended_height'] = 150;
$temp_blog = $this->api_display->large_icon_staff($config_data);
$temp_blog_xs = $this->api_display->large_icon_staff_mobile($config_data);

$temp_display = '
    <div class="col-md-12">
        <div class="api_text_align_center">
            <div class="api_large api_title_1 api_template_title_1">
';
$temp_admin = $this->api_helper->api_lang_v2('OUR MEMBER');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
            </div>    
            <div class="api_height_10"></div>
            <div class="api_text_transform_capitalize api_template_title_2">
';
$temp_admin = $this->api_helper->api_lang_v2('Members Introduction');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
            </div>  
            <div class="api_height_30"></div>    
        </div>
        <div class="api_height_30"></div> 
    </div>
    <div class="hidden-xs">
        '.$temp_blog['display'].'
    </div>
    <div class="visible-xs">
        '.$temp_blog_xs['display'].'
    </div>
';

$config_data = array(
    'wrapper_class' => 'api_front_ayum_member',
    'custom_html' => '',
    'display' => $temp_display,
    'type' => '',
);
$temp = $this->api_display->template_display($config_data);
echo $temp['display'];

?>                       

