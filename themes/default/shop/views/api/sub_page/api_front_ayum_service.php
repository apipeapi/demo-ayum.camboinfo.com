<?php
$temp_display = '';
$config_data = array(
    'table_name' => 'sma_service',
    'select_table' => 'sma_service',
    'translate' => 'yes',
    'description' => 'yes',
    'select_condition' => "id > 0 order by ordering asc limit 3",
);
$select_data = $this->api_helper->api_select_data_v2($config_data);


$language_array = unserialize(multi_language);
$select_view = array();
for ($i=0;$i<count($select_data);$i++) {    
    for ($i2=0;$i2<count($language_array);$i2++) {
        $select_view[$i]['title_'.$language_array[$i2][0]] = $select_data[$i]['title_'.$language_array[$i2][0]];
        $select_view[$i]['intro_'.$language_array[$i2][0]] = $this->sma->decode_html($select_data[$i]['description_'.$language_array[$i2][0]]);             
    }    
    $select_view[$i]['id'] = $select_data[$i]['id'];
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/service/thumb/'.$select_data[$i]['image'],
        'max_width' => 0,
        'max_height' => 0,
        'product_link' => '',
        'image_class' => 'api_link_box_none api_img_service_medium',
        'image_id' => '',   
        'resize_type' => 'fixed',
    );
    $temp_image = $this->api_helper->api_get_image($data_view);
    $select_view[$i]['image'] = $temp_image['image_table'];



    $select_view[$i]['type'] = $select_data[$i]['type'];    
    $select_view[$i]['link'] = base_url().$select_data[$i]['slug'];
    $select_view[$i]['date'] = $select_data[$i]['created_date'];
}


$config_data = array(
    'border_bottom' => '',
    'col_class' => 'col-md-4 col-sm-4 col-xs-12',
    'date_format' => 'Y/m/d',
    'display_border_bottom' => 0,
    'display_date' => 0,
    'lang_key' => $this->api_shop_setting[0]['api_lang_key'],
    'read_more' => '
        <div class="api_button_readmore">
            MORE <li class="fa fa-arrow-right"></li>
        </div>  
    ',
    'select_data' => $select_view,
    'title_class' => '
        api_big api_title api_title_'.$this->api_shop_setting[0]['api_lang_key'].' api_text_align_center api_font_size_20
    ',
    'title_a_class' => 'api_front_ayum_service_title',
    'view_all' => '',
    'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/service',
    'recommended_width' => 600,
    'recommended_height' => 375,
);
$temp_large_icon = $this->api_display->large_icon($config_data);



$temp_display = '
    <div class="col-md-12">
        <div class="api_text_align_center">
            <div class="api_large api_title_1 api_template_title_1">
';
$temp_admin = $this->api_helper->api_lang_v2('SERVICE_v2');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
            </div>    
            <div class="api_height_10"></div>
            <div class="api_text_transform_capitalize api_template_title_2">
';
$temp_admin = $this->api_helper->api_lang_v2('Ayum service');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '              
            </div>  
            <div class="api_height_30"></div>    
        </div>
    </div>
    <div class="">
        '.$temp_large_icon['display'].'
    </div>       
';

$config_data = array(
    'wrapper_class' => '',
    'custom_html' => '',
    'display' => $temp_display,
    'type' => '',
);
$temp = $this->api_display->template_display($config_data);
echo $temp['display'];

?>                       

