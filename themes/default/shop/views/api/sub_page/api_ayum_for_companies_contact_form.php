<?php
$temp_name = 'form_for_companies';
$temp_display .= '
<button type="button" id="api_modal_trigger_'.$temp_name.'" class="api_display_none" data-toggle="modal" data-target="#api_modal_'.$temp_name.'">Open Modal</button>
<!-- Modal -->
<div id="api_modal_'.$temp_name.'" class="modal fade api_'.$temp_name.'" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header api_display_none">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="api_modal_title">
        </h4>
      </div>
      <div class="modal-body" id="api_modal_body">
        <div class="api_height_30 visible-xs"></div>
        <div class="api_big api_temp_1"> 
            '.lang('Recruitment request, Inquiry form').'
        </div>
        <div class="api_height_15 api_clear_both"></div>

        <div class="api_temp_2"> 
        <form action="'.base_url().'main/ayum_for_companies_send_contact" name="api_'.$temp_name.'" method="post">
        <input type="hidden" name="'.$this->security->get_csrf_token_name().'" value="'.$this->security->get_csrf_hash().'">
';

          if ($this->session->userdata('api_mobile') != 1) {
            $temp_display .= '
              <table width="100%" border="0" >
              <tr>
              <td valign="middle" class="api_big api_temp_td_3 api_td_width_auto">
                '.lang('company_name:').'
              </td>
              <td valign="middle" align="left" width="10" class="api_big api_temp_td_1 api_td_width_auto">
                :
              </td>
              <td valign="middle">
                <input class="api_temp_input" name="company_name" type="text" value="" />
              </td>            
              </tr>   

              <tr>
              <td valign="middle" colspan="3" height="15"></td>
              </tr>

              <tr>
              <td valign="middle" class="api_big api_temp_td_3 api_td_width_auto">
                '.lang('Name:').'
              </td>
              <td valign="middle" align="left" width="10" class="api_big api_temp_td_1 api_td_width_auto">
                :
              </td>
              <td valign="middle">
                <input class="api_temp_input" name="name" type="text" value="" />
              </td>            
              </tr>   

              <tr>
              <td valign="middle" colspan="3" height="15">
                <div class="api_error api_email_error api_color_red api_padding_bottom_3 api_padding_top_10" style="display:none;">
                  '.lang('validation_form').'
                </div>            
              </td>
              </tr>

              <tr>
              <td valign="middle" class="api_big api_temp_td_3 api_td_width_auto">
                '.lang('Mail Address:').'
              </td>
              <td valign="middle" align="left" width="10" class="api_big api_temp_td_1 api_td_width_auto">
                :
              </td>
              <td valign="middle">              
                <input class="api_temp_input" name="email" type="text" required="required" value="" />  
              </td>            
              </tr>

              </table>
            ';
          }
          else {
            $temp_display .= '
              <table width="100%" border="0">

              <tr>
              <td valign="middle">
                <span class="api_big api_font_size_14">'.lang('company_name:').'</span>
                <input class="api_temp_input" name="name" type="text" value="" />
              </td>            
              </tr>

              <tr>
              <td valign="middle">
                <div class="api_height_10"></div>
                <div class="api_error api_email_error api_color_red api_padding_bottom_3 " style="display:none;">
                  '.lang('validation_form').'
                </div>              
                <span class="api_big api_font_size_14">'.lang('Mail Address:').'</span>
                <input class="api_temp_input" name="email" type="text" required="required" value="" />  
              </td>            
              </tr>
              </table>
            ';
          }

            $temp_display .= '
              <div class="api_height_20 api_clear_both"></div>
              '.lang('Inquiry item：Please choose from the below:').'
              <div class="api_height_10 api_clear_both"></div>          
            ';

            $config_data = array(
                'table_name' => 'sma_post',
                'select_table' => 'sma_post',
                'translate' => 'yes',
                'description' => '',
                'select_condition' => "c_id = 108 order by ordering asc",
            );
            $temp = $this->api_helper->api_select_data_v2($config_data);            

            $temp_display_2 = '';
            for ($i=0;$i<count($temp);$i++) {
                $temp_display_2 .= '
                <div class="api_admin api_admin_wrapper_ayum_inquiry_item_'.$temp[$i]['id'].'">
                    <table width="100%" border="0">
                    <tr>
                    <td valign="middle" width="25">
                        <input name="inquiry_item_'.($i + 1).'" type="checkbox" class="checkbox" value="'.$temp[$i]['title_'.$l].'"/>
                    </td>
                    <td class="api_padding_left_5" valign="middle">
                        '.$temp[$i]['title_'.$l].'
                    </td>
                    </tr>
                    </table>    
                    <div class="api_height_15 visible-xs"></div>
                ';
                $field_data = array();
                $field_data[0] = array(
                    'type' => 'translate',
                    'col_class' => 'col-md-9',
                    'field_class' => '',
                    'field_name' => 'translate',
                    'translate' => 'yes',
                    'field_label_name' => 'Title',                      
                );
                $field_data[1] = array(
                  'type' => 'text',
                  'col_class' => 'col-md-3',
                  'field_class' => 'api_numberic_input',
                  'field_name' => 'ordering',
                  'field_label_name' => 'Ordering',                      
                  'field_value' => '',
                );                 
                $field_data_add[0] = array(
                  'type' => 'hidden',
                  'col_class' => 'col-md-3',
                  'field_class' => '',
                  'field_name' => 'c_id',
                  'field_label_name' => '',                      
                  'field_value' => 108,
                );                
                $config_data = array(
                    'l' => $l,
                    'wrapper_class' => 'api_admin_wrapper_ayum_inquiry_item_'.$temp[$i]['id'],
                    'modal_class' => '',
                    'title' => 'Edit', 
                    'field_data' => $field_data,
                    'selected_id' => $temp[$i]['id'],
                    'table_name' => 'sma_post',
                    'table_id' => 'id',
                    'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
                    'field_data_add' => $field_data_add,
                    'add' => 'yes',
                    'title_add' => 'Add new inquiry item',
                    'delete' => 'yes',
                    'title_delete' => '<div>Your will remove</div>'.$temp[$i]['title_'.$l],
                    'condition_ordering' => " and c_id = 108",   
                );
                $temp_admin = $this->api_admin->front_end_edit($config_data);
                $temp_display_2 .= $temp_admin['display'];
                $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];   
              $temp_display_2 .= '
                  </div>
              ';                             
            }
            $temp_display .= '
                <div class="api_temp_3">
                    '.$temp_display_2.'
                </div>
            ';

            $temp_display .= '
                <div class="api_height_20 api_clear_both"></div>
                '.lang('Inquiry Contents').'
                <div class="api_height_10 api_clear_both"></div>
                <div class="api_message_error api_color_red api_padding_bottom_3" style="display:none;">'.lang('validation_form').'</div>
                <textarea class="api_temp_textarea" name="message" required="required"></textarea>
            ';

            $temp_display .= '
                <div class="api_height_10 api_clear_both"></div>
            ';

            $temp_display .= '
              <div class="col-md-6 col-xs-6 api_padding_right_5 api_padding_left_0">
                  <a href="javascript:void(0);" id="api_temp_button_1" onclick="api_'.$temp_name.'_submit();">
                    <div class="api_link_box_none api_temp_button_1" >
                    '.lang('Send').'
                    </div>
                  </a>
              </div>
              <div class="col-md-6 col-xs-6 api_padding_right_0 api_padding_left_5">
                  <a class="api_temp_button_2" href="javascript:void(0);" data-dismiss="modal" >
                    <div class="api_link_box_none api_temp_button_1" >
                    '.lang('Cancel').'
                    </div>
                  </a>                
              </div>
              <div class=" api_clear_both"></div>        
            ';            
        $temp_display .= '
        </form>
        </div>

      </div>
      <div class="modal-footer api_display_none" id="api_modal_footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">
            '.lang('Cancel').'
        </button>
      </div>
    </div>
  </div>
</div>
';
?>
<script>
  var temp_lang_3 = "<?php echo lang('Success'); ?>";
  var temp_lang_4 = "<?php echo lang('Successfully send recruitment request.'); ?>";
</script>