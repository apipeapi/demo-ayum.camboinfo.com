<?php
$temp_display = '';

$temp_display .= '
    <div class="hidden-xs">
        <table width="100%" border="0">
        <tr>
        <td class="api_temp_td_1">
            <div class="api_temp_title_1">
                <div class="api_text_align_center api_big api_front_title_1_'.$l.'">            
';
$temp_admin = $this->api_helper->api_lang_v2('Creating a society where you can work in your own way');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                </div>
                <div class="api_temp_1"></div>
            </div>            
        </td>
        <td rowspan="2" valign="top" align="center" class="api_admin api_admin_wrapper_web_image_front_image_1">
            
';
$data_view = array (
    'wrapper_class' => 'api_temp_image_pc',
    'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/'.$this->api_web_image['front_image_1'],
    'max_width' => 1000,
    'max_height' => 650,
    'image_class' => 'img-responsive',
    'image_id' => '',   
    'resize_type' => '',       
);
$temp_image = $this->api_helper->api_get_image($data_view);
$temp_display .= $temp_image['image_table'];

$data_view = array (
    'wrapper_class' => 'api_temp_image_1024',
    'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/'.$this->api_web_image['front_image_1_image_2'],
    'max_width' => 700,
    'max_height' => 620,
    'image_class' => 'img-responsive',
    'image_id' => '',   
    'resize_type' => '',       
);
$temp_image = $this->api_helper->api_get_image($data_view);
$temp_display .= $temp_image['image_table'];

$data_view = array (
    'wrapper_class' => 'visible-sm',
    'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/'.$this->api_web_image['front_image_1_image_3'],
    'max_width' => 500,
    'max_height' => 620,
    'image_class' => 'img-responsive',
    'image_id' => '',   
    'resize_type' => '',       
);
$temp_image = $this->api_helper->api_get_image($data_view);
$temp_display .= $temp_image['image_table'];

    $field_data = array();
    $field_data[0] = array(
        'type' => 'file',
        'col_class' => 'col-md-6',
        'field_class' => '',
        'field_name' => 'image',    
        'field_label_name' => 'Image',
        'recommended_width' => 1000,
        'recommended_height' => 650,
    );
    $field_data[1] = array(
        'type' => 'file',
        'col_class' => 'col-md-6',
        'field_class' => '',
        'field_name' => 'image_2',    
        'field_label_name' => 'Image',
        'recommended_width' => 700,
        'recommended_height' => 620,
    );    
    $field_data[2] = array(
        'type' => 'file',
        'col_class' => 'col-md-6',
        'field_class' => '',
        'field_name' => 'image_3',    
        'field_label_name' => 'Image',
        'recommended_width' => 500,
        'recommended_height' => 620,
    );        
    $config_data = array(
        'l' => $l,
        'wrapper_class' => 'api_admin_wrapper_web_image_front_image_1',
        'modal_class' => '',
        'title' => 'Edit', 
        'field_data' => $field_data,
        'selected_id' => $this->api_web_image['front_image_1_id'],
        'table_name' => 'sma_web_image',
        'table_id' => 'id',
        'upload_path' => 'assets/uploads/web/'.$this->api_web[0]['id'],
        'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
    );
    $temp_admin = $this->api_admin->front_end_edit($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];


$temp_display .= '
        </td>
        </tr>

        <tr>
        <td class="api_temp_td_2" valign="top">
            <div class="api_padding_20" style="display:inline-block; margin:auto; text-align:left;">
                <div class="api_front_title_2">
';
$temp_admin = $this->api_helper->api_lang_v2('The Changing  into a Borderless world');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                </div>
                <div class="api_height_15"></div>
            </div>

            <div class="text-center api_temp_hover_1 hidden-sm hidden-xs" style="min-height:62.4px">
                <div class="api_front_button_1 api_temp_button_1" onmouseover="$(\'.api_temp_button_2\').show(); $(\'.api_temp_button_1\').hide();" onmouseout="$(\'.api_temp_button_2\').hide(); $(\'.api_temp_button_1\').show();">
                    <table width="100%" border="0">
                    <tr>
                        <td valign="middle" class="api_temp_td_5">
                            '.$this->api_helper->api_lang('contact_us').'
                        </td>
                        <td valign="middle" width="30">
                            &nbsp;<li class="fa fa-arrow-right"></li>
                        </td>
                    </tr>
                    </table>                
                </div>

                <div class="api_temp_button_2" style="display:none;" onmouseover="$(\'.api_temp_button_2\').show(); $(\'.api_temp_button_1\').hide();" onmouseout="$(\'.api_temp_button_2\').hide(); $(\'.api_temp_button_1\').show();">
                    <div style="display:inline-block">
                        <table border="0">
                        <tr>
                        <td valign="middle" >
                        <div class="api_admin api_admin_wrapper_translation_Click_here_for_employer" >
                            <a href="'.base_url().'#Service-List">
                                <div class="api_front_button_1">
                                    <table width="100%" border="0">
                                        <tr>
                                            <td valign="middle" class="api_temp_td_5">
                                                '.$this->api_helper->api_lang('Click here for employer').'
                                            </td>
                                            <td valign="middle" width="30">
                                                &nbsp;<li class="fa fa-arrow-right"></li>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="api_clear_both"></div>
                                </div>       
                                <div class="api_clear_both"></div>
                            </a>
';
$temp = $this->site->api_select_some_fields_with_where("
    id
    "
    ,"sma_translation"
    ,"keyword = 'Click here for employer'"
    ,"arr"
);
$field_data = array();
$field_data[0] = array(
    'type' => 'translate_textarea',
    'col_class' => 'col-md-12',
    'field_class' => '',
    'field_name' => 'translate',
    'translate' => 'yes',
    'field_label_name' => 'Title',                      
);
$config_data = array(
    'l' => $l,
    'wrapper_class' => 'api_admin_wrapper_translation_Click_here_for_employer',
    'modal_class' => '',
    'title' => 'Edit', 
    'field_data' => $field_data,
    'selected_id' => $temp[0]['id'],
    'table_name' => 'sma_translation',
    'table_id' => 'id',
    'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
);
$temp_admin = $this->api_admin->front_end_edit($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                            <div class="api_clear_both"></div>
                        </div>
                        
                        </td>
                        <td width="30" valign="center" >
                            &nbsp;
                        </td>
                        <td valign="middle">
                        <div class="api_admin api_admin_wrapper_translation_Click_here_for_job_seekers" >
                            <a href="'.base_url().'For-Jobseeker">
                                <div class="api_front_button_1">
                                    <table width="100%" border="0">
                                        <tr>
                                            <td valign="middle" class="api_temp_td_5">
                                                '.$this->api_helper->api_lang('Click here for job seekers').'
                                            </td>
                                            <td valign="middle" width="30">
                                                &nbsp;<li class="fa fa-arrow-right"></li>
                                            </td>
                                        </tr>
                                    </table>
                                </div>               
                            </a>
';

$temp = $this->site->api_select_some_fields_with_where("
    id
    "
    ,"sma_translation"
    ,"keyword = 'Click here for job seekers'"
    ,"arr"
);
$field_data = array();
$field_data[0] = array(
    'type' => 'translate_textarea',
    'col_class' => 'col-md-12',
    'field_class' => '',
    'field_name' => 'translate',
    'translate' => 'yes',
    'field_label_name' => 'Title',                      
);
$config_data = array(
    'l' => $l,
    'wrapper_class' => 'api_admin_wrapper_translation_Click_here_for_job_seekers',
    'modal_class' => '',
    'title' => 'Edit', 
    'field_data' => $field_data,
    'selected_id' => $temp[0]['id'],
    'table_name' => 'sma_translation',
    'table_id' => 'id',
    'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
);
$temp_admin = $this->api_admin->front_end_edit($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                            <div class=" api_clear_both"></div>
                        </div>

                        </td>
                        </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="text-center api_temp_hover_2 visible-sm visible-xs">
                <div class="api_temp_button_2">
                    <div style="display:inline-block">
                        <table border="0">
                        <tr>
                        <td valign="middle" >
                            <a href="'.base_url().'For-Companies">
                                <div class="api_front_button_1">
                                    <table width="100%" border="0">
                                        <tr>
                                            <td valign="middle" class="api_temp_td_5">
                                            '.$this->api_helper->api_lang('Click here for employer').'
                                            </td>
                                            <td valign="middle" width="30">
                                                &nbsp;<li class="fa fa-arrow-right"></li>
                                            </td>
                                        </tr>
                                    </table>
                                </div>       
                            </a>     
                        </td>
                        <td width="30" valign="center" >
                            &nbsp;
                        </td>
                        <td valign="middle">
                            <a href="'.base_url().'For-Jobseeker">
                                <div class="api_front_button_1">
                                    <table width="100%" border="0">
                                        <tr>
                                            <td valign="middle" class="api_temp_td_5">
                                            '.$this->api_helper->api_lang('Click here for job seekers').'
                                            </td>
                                            <td valign="middle" width="30">
                                                &nbsp;<li class="fa fa-arrow-right"></li>
                                            </td>
                                        </tr>
                                    </table>
                                </div>               
                            </a>
                        </td>
                        </tr>
                        </table>
                    </div>
                </div>
            </div>

        </td>
        </tr>
        </table>
    </div>
';

$temp_display .= '
    <div class="visible-xs">
        <table width="100%" border="0">
        <tr>
        <td valign="top" align="center" class="">
';

$data_view = array (
    'wrapper_class' => '',
    'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/'.$this->api_web_image['front_image_1'],
    'max_width' => 1000,
    'max_height' => 650,
    'image_class' => 'img-responsive',
    'image_id' => '',   
    'resize_type' => '',       
);
$temp_image = $this->api_helper->api_get_image($data_view);
$temp_display .= $temp_image['image_table'];

$temp_display .= '
        </td>
        </tr>
        </table>

        <div class="api_temp_title_1">
            <div class="api_height_15 api_clear_both"></div>
            <div class="api_text_align_center api_big api_front_title_1_'.$l.'">            
            '.$this->api_helper->api_lang('Creating a society where you can work in your own way').'
            </div>
            <div class="api_height_15 api_clear_both"></div>
        </div>            
        <div class="col-md-12">
            <div class="api_front_title_2">
                '.$this->api_helper->api_lang('The Changing  into a Borderless world').'
            </div>
            <div class="api_height_15"></div>
        </div>
        <div class="col-md-6 col-xs-6">
            <a href="'.base_url().'For-Companies">
                <div class="api_front_button_1">
                    <table width="100%" border="0">
                        <tr>
                            <td valign="middle" class="api_temp_td_5">
                            '.$this->api_helper->api_lang('Click here for employer').'
                            </td>
                            <td valign="middle" width="30">
                                &nbsp;<li class="fa fa-arrow-right"></li>
                            </td>
                        </tr>
                    </table>
                </div>       
            </a>     
        </div>
        <div class="col-md-6 col-xs-6">
            <a href="'.base_url().'For-Jobseeker">
                <div class="api_front_button_1">
                    <table width="100%" border="0">
                        <tr>
                            <td valign="middle" class="api_temp_td_5">
                            '.$this->api_helper->api_lang('Click here for job seekers').'
                            </td>
                            <td valign="middle" width="30">
                                &nbsp;<li class="fa fa-arrow-right"></li>
                            </td>
                        </tr>
                    </table>
                </div>               
            </a>
        </div>


    </div>
';



$config_data = array(
    'wrapper_class' => 'api_front_ayum_banner',
    'custom_html' => '<div class="api_height_30 api_clear_both visible-xs"></div>',
    'display' => $temp_display,
    'type' => 'full',
);
$temp = $this->api_display->template_display($config_data);
echo $temp['display'];

?>                       

