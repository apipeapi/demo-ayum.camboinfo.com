<?php
$data_view = array (
    'wrapper_class' => '',
    'file_path' => $include_data['image_path'],
    'max_width' => $include_data['image_max_width'],
    'max_height' => $include_data['image_max_height'],
    'product_link' => 'true',
    'image_class' => 'api_img_welcome_table',
    'image_id' => '',   
    'image_title' => '',
    'table_height' => '',
    'resize_type' => 'fixed',
);
$temp_image = $this->api_helper->api_get_image($data_view);

$temp_display .= '
    <div class="col-md-12 hidden-xs">
        <div class="api_temp_2">
            <table class="api_temp_table_1" border="0">
            <tr>
            <td class="api_temp_td_1" valign="top" align="left">                
                <div class="api_admin api_admin_wrapper_web_image_'.$include_data['image_name'].'">
                    '.$temp_image['image_table'].'
                
';
$config_data = array(
    'wrapper' => 'api_admin_wrapper_web_image_'.$include_data['image_name'],
    'name' => $include_data['image_name'],
    'recommended_width' => $include_data['image_max_width'],
    'recommended_height' => $include_data['image_max_height'],
);
$temp_admin = $this->api_helper->api_admin_web_image($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

$temp_display .= '
                </div>
            </td>
            <td class="api_temp_td_2" valign="top" >     
                <div class="api_temp_1">   
                    <div class="api_big api_temp_title_1">
';
$temp_admin = $this->api_helper->api_lang_v2($include_data['title_key']);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

$temp_display .= '
                    </div>
                    <div class="api_height_15"></div>
                    <div class="api_temp_title_2">
';
                      
$temp_admin = $this->api_helper->api_lang_v2($include_data['description_key']);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                    </div>         
                </div>       
            </td>
            </tr>
            </table>
        </div>
    </div>
    <div class="col-md-12 visible-xs">
        <div class="api_temp_2">
            <table width="100%" border="0">
            <tr>
            <td class="api_temp_td_1" valign="top" align="left">
                '.$temp_image['image_table'].'
            </td>
            </tr>
            <tr>
            <td class="api_temp_td_2" valign="top" >     
                <div class="api_temp_1">   
                    <div class="api_big api_temp_title_1">
                        '.$include_data['title'].'
                    </div>
                    <div class="api_height_15"></div>
                    <div class="api_temp_title_2">
                        '.$this->api_helper->api_lang($include_data['description_key']).'
                    </div>         
                </div>       
            </td>
            </tr>
            </table>
        </div>    
    </div>
';

$config_data = array(
    'wrapper_class' => 'api_welcome_table_wrapper',
    'custom_html' => '',
    'display' => $temp_display,
    'display_class' => '',
    'panel' => '',
    'panel_class' => '',
    'type' => '',
);
$temp = $this->api_display->template_display($config_data);
echo $temp['display'];


?>