<?php
$temp_display = '';
$config_data = array(
    'table_name' => 'sma_blog',
    'select_table' => 'sma_blog',
    'translate' => 'yes',
    'description' => 'yes',
    'select_condition' => "add_ons like '%:featured:{yes}:%' order by created_date desc limit 3",
);
//'select_condition' => "id = 151 or id = 152 or id = 150",
$select_data = $this->api_helper->api_select_data_v2($config_data);

$language_array = unserialize(multi_language);
$select_view = array();
for ($i=0;$i<count($select_data);$i++) {    
    for ($i2=0;$i2<count($language_array);$i2++) {
        $select_view[$i]['name_'.$language_array[$i2][0]] = $select_data[$i]['title_'.$language_array[$i2][0]];
        $select_view[$i]['intro_'.$language_array[$i2][0]] = $this->sma->decode_html($select_data[$i]['description_'.$language_array[$i2][0]]);             
    }    
    $config_data_2 = array(
        'table_name' => 'sma_blog_category',
        'select_table' => 'sma_blog_category',
        'translate' => 'yes',
        'description' => '',
        'select_condition' => "id = ".$select_data[$i]['c_id'],
    );
    $temp_category = $this->api_helper->api_select_data_v2($config_data_2);    

    $select_view[$i]['id'] = $select_data[$i]['id'];
    $select_view[$i]['link'] = base_url().'Blog/'.$temp_category[0]['slug'].'/'.$select_data[$i]['id'];
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/blog/thumb/'.$select_data[$i]['image'],
        'max_width' => 0,
        'max_height' => 0,
        'image_class' => 'api_link_box_none api_img_blog_small',
        'image_id' => '',   
        'resize_type' => 'fixed',       
    );
    $temp_image = $this->api_helper->api_get_image($data_view); 
    $select_view[$i]['image'] = '
        <a class="api_link_box_none" href="'.$select_view[$i]['link'].'">
            '.$temp_image['image'].'
        </a>
    ';

    $select_view[$i]['date'] = $select_data[$i]['created_date'];
}

$config_data = array();
$temp_config = $this->api_display->blog_config($config_data);
$config_data = $temp_config['config_data'];
$config_data['lang_key'] = $this->api_shop_setting[0]['api_lang_key'];
$config_data['select_data'] = $select_view;
$config_data['border_bottom'] = '
    <div class="api_height_10"></div>
    <div class="api_border_bottom"></div>
    <div class="api_height_15"></div>
';
$config_data['view_all'] = '
    <div class="api_height_15"></div>
    <a href="'.base_url().'Blog/News">
        <div class="api_button_readmore" style="display:inline-block">
            MORE <li class="fa fa-arrow-right"></li>
        </div>
    </a>
    <div class="api_height_15"></div>   
';
$config_data['title_class'] = 'api_display_none';
$config_data['file_path'] = 'assets/uploads/web/'.$this->api_web[0]['id'].'/blog';
$temp_blog = $this->api_display->blog($config_data);

$temp_display .= '<div class="api_height_30"></div>';

$temp_display .= '
<div class="col-md-6">
    <div class="api_height_15"></div>
    <table width="100%" border="0">
    <tr>
    <td valign="middle" align="center">
        <div class="api_large api_title_1 api_template_title_1">
';
$temp_admin = $this->api_helper->api_lang_v2('News & Topics');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
        </div>    
        <div class="api_height_10"></div>
        <div class="api_text_transform_capitalize api_template_title_2">
';
$temp_admin = $this->api_helper->api_lang_v2('Peep into Ayum');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
        </div>        
    </td>
    </tr>
    </table>

    <div class="api_height_30"></div>
    
    '.$temp_blog['display'].'
</div>
<div class="col-md-6">
    <div class="api_height_15"></div>
    <div class="api_height_30 visible-sm visible-xs "></div>
    <table width="100%" >
        <tr>
            <td class="api_admin api_admin_wrapper_translation_facebook_video" valign="middle" colspan="3">
                <p class="fb-wrapper"><iframe class="api_page_youtube_iframe" src="'.$this->api_web[0]['facebook_video_link'].'" width="500" height="354" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share">
                </iframe></p>
';
$field_data = array();
$field_data[0] = array(
    'type' => 'add_ons',
    'col_class' => 'col-md-12',
    'field_class' => '',
    'field_name' => 'facebook_video_link',
    'field_label_name' => 'Facebook Video Link Src',                      
);  
$config_data = array(
    'l' => $l,
    'wrapper_class' => 'api_admin_wrapper_web_facebook_video_link',
    'modal_class' => '',
    'title' => 'Edit', 
    'field_data' => $field_data,
    'selected_id' => $this->api_web[0]['id'],
    'table_name' => 'sma_web',
    'table_id' => 'id',
    'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
);
$temp_admin = $this->api_admin->front_end_edit($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];



$temp_display .= '
            </td>
        </tr>
        </table>

        <div class="api_height_5"></div>
';


$temp_display .= '        
        <table width="100%" class="hidden-xs">
        <tr>
';

//-Post-1-------------------
$temp = $this->api_select_data->get_post(159);
$data_view = array (
    'wrapper_class' => '',
    'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/post/'.$temp[0]['image_2'],
    'max_width' => 180,
    'max_height' => 50,
    'image_class' => 'api_link_box_none',
    'image_id' => '',   
    'resize_type' => 'full',       
);
$temp_image = $this->api_helper->api_get_image($data_view);   
$temp_display .= '
            <td width="49%" class="api_link_box_none api_admin api_admin_wrapper_post_'.$temp[0]['id'].'" valign="middle" align="center">
                <a class="api_link_box_none" href="'.$temp[0]['link'].'">
                    <table width="100%">
                        <tr>
                            <td class="api_big api_temp_image_2" align="center" valign="middle" style="background-image:url('.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/post/thumb/'.$temp[0]['image'].');">
                            '.$temp_image['image'].'
                            <div class="api_height_5"></div>
                            '.$temp[0]['title_'.$l].'
                            </td>
                        </tr>
                    </table>
                </a>
';
$field_data = array();
$field_data[0] = array(
    'type' => 'translate_textarea',
    'col_class' => 'col-md-12',
    'field_class' => '',
    'field_name' => 'translate',
    'translate' => 'yes',
    'field_label_name' => 'Title',                      
);     
$field_data[1] = array(
    'type' => 'text',
    'col_class' => 'col-md-12',
    'field_class' => '',
    'field_name' => 'link',
    'field_label_name' => 'Link',                   
);    
$field_data[2] = array(
    'type' => 'file',
    'col_class' => 'col-md-3',
    'field_class' => '',
    'field_name' => 'image_2',    
    'field_label_name' => 'Logo Image',
    'recommended_width' => 250,
    'recommended_height' => 130,
);
$field_data[3] = array(
    'type' => 'file',
    'col_class' => 'col-md-3',
    'field_class' => '',
    'field_name' => 'image',    
    'field_label_name' => 'Background Image',
    'recommended_width' => 600,
    'recommended_height' => 350,
);
$config_data = array(
    'l' => $l,
    'wrapper_class' => 'api_admin_wrapper_post_'.$temp[0]['id'],
    'modal_class' => '',
    'title' => 'Edit', 
    'field_data' => $field_data,
    'selected_id' => $temp[0]['id'],
    'table_name' => 'sma_post',
    'table_id' => 'id',
    'upload_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/post',
    'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
);
$temp_admin = $this->api_admin->front_end_edit($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

$temp_display .= '
            </td>
';

$temp_display .= '
            <td width="2%"></td>
';
//-Post-1-------------------

//-Post-2-en-------------------
if ($l == 'en')  {
    $temp = $this->api_select_data->get_post(158);
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/post/'.$temp[0]['image_2'],
        'max_width' => 180,
        'max_height' => 50,
        'image_class' => 'api_link_box_none',
        'image_id' => '',   
        'resize_type' => 'full',       
    );
    $temp_image = $this->api_helper->api_get_image($data_view);    
    $temp_display .= '
        <td width="49%" class="api_link_box_none api_admin api_admin_wrapper_post_'.$temp[0]['id'].'" valign="middle" align="center">
            <a class="api_link_box_none" href="'.$temp[0]['link'].'">
                <table width="100%">
                    <tr>
                        <td class="api_big api_temp_image_2" align="center" valign="middle" style="background-image:url('.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/post/thumb/'.$temp[0]['image'].');">
                            '.$temp_image['image'].'
                            <div class="api_height_5"></div>
                            '.$temp[0]['title_'.$l].'
                        </td>
                    </tr>
                </table>
            </a>
    ';
    $field_data = array();
    $field_data[0] = array(
        'type' => 'translate_textarea',
        'col_class' => 'col-md-12',
        'field_class' => '',
        'field_name' => 'translate',
        'translate' => 'yes',
        'field_label_name' => 'Title',                      
    );     
    $field_data[1] = array(
        'type' => 'text',
        'col_class' => 'col-md-12',
        'field_class' => '',
        'field_name' => 'link',
        'field_label_name' => 'Link',                   
    );    
    $field_data[2] = array(
        'type' => 'file',
        'col_class' => 'col-md-3',
        'field_class' => '',
        'field_name' => 'image_2',    
        'field_label_name' => 'Logo Image',
        'recommended_width' => 250,
        'recommended_height' => 130,
    );
    $field_data[3] = array(
        'type' => 'file',
        'col_class' => 'col-md-3',
        'field_class' => '',
        'field_name' => 'image',    
        'field_label_name' => 'Background Image',
        'recommended_width' => 600,
        'recommended_height' => 350,
    );    
    $config_data = array(
        'l' => $l,
        'wrapper_class' => 'api_admin_wrapper_post_'.$temp[0]['id'],
        'modal_class' => '',
        'title' => 'Edit', 
        'field_data' => $field_data,
        'selected_id' => $temp[0]['id'],
        'table_name' => 'sma_post',
        'table_id' => 'id',
        'upload_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/post',
        'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
    );
    $temp_admin = $this->api_admin->front_end_edit($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
        </td>
    ';
}
//-Post-2-en------------------
else {
//-Post-2-ja------------------
    $temp = $this->api_select_data->get_post(161);
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/post/'.$temp[0]['image_2'],
        'max_width' => 180,
        'max_height' => 50,
        'image_class' => 'api_link_box_none',
        'image_id' => '',   
        'resize_type' => 'full',       
    );
    $temp_image = $this->api_helper->api_get_image($data_view);
    
    $temp_display .= '
        <td width="49%" class="api_link_box_none api_admin api_admin_wrapper_post_'.$temp[0]['id'].'" valign="middle" align="center">
            <a class="api_link_box_none" href="'.$temp[0]['link'].'">
                <table width="100%">
                    <tr>
                        <td class="api_big api_temp_image_2" align="center" valign="middle" style="background-image:url('.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/post/thumb/'.$temp[0]['image'].');">
                            '.$temp_image['image'].'
                            <div class="api_height_5"></div>
                            '.$temp[0]['title_'.$l].'
                        </td>
                    </tr>
                </table>
            </a>
    ';
    $field_data = array();
    $field_data[0] = array(
        'type' => 'translate_textarea',
        'col_class' => 'col-md-12',
        'field_class' => '',
        'field_name' => 'translate',
        'translate' => 'yes',
        'field_label_name' => 'Title',                      
    );     
    $field_data[1] = array(
        'type' => 'text',
        'col_class' => 'col-md-12',
        'field_class' => '',
        'field_name' => 'link',
        'field_label_name' => 'Link',                   
    );    
    $field_data[2] = array(
        'type' => 'file',
        'col_class' => 'col-md-3',
        'field_class' => '',
        'field_name' => 'image_2',    
        'field_label_name' => 'Logo Image',
        'recommended_width' => 250,
        'recommended_height' => 130,
    );
    $field_data[3] = array(
        'type' => 'file',
        'col_class' => 'col-md-3',
        'field_class' => '',
        'field_name' => 'image',    
        'field_label_name' => 'Background Image',
        'recommended_width' => 600,
        'recommended_height' => 350,
    );    
    $config_data = array(
        'l' => $l,
        'wrapper_class' => 'api_admin_wrapper_post_'.$temp[0]['id'],
        'modal_class' => '',
        'title' => 'Edit', 
        'field_data' => $field_data,
        'selected_id' => $temp[0]['id'],
        'table_name' => 'sma_post',
        'table_id' => 'id',
        'upload_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/post',
        'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
    );
    $temp_admin = $this->api_admin->front_end_edit($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
        </td>
    ';
//-Post-2-ja------------------
}


$temp_display .= '
        </tr>
        <tr>
            <td class="api_height_10"></td>
        </tr>
        <tr>
';


//-Post-3-------------------
$temp = $this->api_select_data->get_post(160);
$data_view = array (
    'wrapper_class' => '',
    'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/post/'.$temp[0]['image_2'],
    'max_width' => 180,
    'max_height' => 50,
    'image_class' => 'api_link_box_none',
    'image_id' => '',   
    'resize_type' => 'full',       
);
$temp_image = $this->api_helper->api_get_image($data_view);
$temp_display .= '
            <td width="49%" class="api_link_box_none api_admin api_admin_wrapper_post_'.$temp[0]['id'].'" valign="middle" align="center">
                <a class="api_link_box_none" href="'.$temp[0]['link'].'">
                    <table width="100%">
                        <tr>
                            <td class="api_big api_temp_image_2" align="center" valign="middle" style="background-image:url('.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/post/thumb/'.$temp[0]['image'].');">
                            '.$temp_image['image'].'
                            <div class="api_height_5"></div>
                            '.$temp[0]['title_'.$l].'
                            </td>
                        </tr>
                    </table>
                </a>            
';
$field_data = array();
$field_data[0] = array(
    'type' => 'translate_textarea',
    'col_class' => 'col-md-12',
    'field_class' => '',
    'field_name' => 'translate',
    'translate' => 'yes',
    'field_label_name' => 'Title',                      
);
$field_data[1] = array(
    'type' => 'text',
    'col_class' => 'col-md-12',
    'field_class' => '',
    'field_name' => 'link',
    'field_label_name' => 'Link',                   
);    
$field_data[2] = array(
    'type' => 'file',
    'col_class' => 'col-md-3',
    'field_class' => '',
    'field_name' => 'image_2',    
    'field_label_name' => 'Logo Image',
    'recommended_width' => 250,
    'recommended_height' => 130,
);
$field_data[3] = array(
    'type' => 'file',
    'col_class' => 'col-md-3',
    'field_class' => '',
    'field_name' => 'image',    
    'field_label_name' => 'Background Image',
    'recommended_width' => 600,
    'recommended_height' => 350,
);
$config_data = array(
    'l' => $l,
    'wrapper_class' => 'api_admin_wrapper_post_'.$temp[0]['id'],
    'modal_class' => '',
    'title' => 'Edit', 
    'field_data' => $field_data,
    'selected_id' => $temp[0]['id'],
    'table_name' => 'sma_post',
    'table_id' => 'id',
    'upload_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/post',
    'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
);
$temp_admin = $this->api_admin->front_end_edit($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];


$temp_display .= '
            </td>
            <td width="2%"></td>
';
//-Post-3-------------------


//-Post-4-------------------
$temp = $this->api_select_data->get_post(162);
$data_view = array (
    'wrapper_class' => '',
    'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/post/'.$temp[0]['image_2'],
    'max_width' => 150,
    'max_height' => 70,
    'image_class' => 'img-responsive api_link_box_none',
    'image_id' => '',   
    'resize_type' => 'full',       
);
$temp_image = $this->api_helper->api_get_image($data_view);
$temp_display .= '
            <td width="49%" class="api_link_box_none api_admin api_admin_wrapper_post_'.$temp[0]['id'].'" valign="middle" align="center">
                <a class="api_link_box_none" href="'.$temp[0]['link'].'">
                    <table width="100%">
                        <tr>
                            <td class="api_big api_temp_image_2" align="center" valign="middle" style="background-image:url('.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/post/thumb/'.$temp[0]['image'].');">
                            '.$temp_image['image'].'
                            <div class="api_height_5"></div>
                            '.$temp[0]['title_'.$l].'                            
                            </td>
                        </tr>
                    </table>
                </a>               
';
$field_data = array();
$field_data[0] = array(
    'type' => 'translate_textarea',
    'col_class' => 'col-md-12',
    'field_class' => '',
    'field_name' => 'translate',
    'translate' => 'yes',
    'field_label_name' => 'Title',                      
);
$field_data[1] = array(
    'type' => 'text',
    'col_class' => 'col-md-12',
    'field_class' => '',
    'field_name' => 'link',
    'field_label_name' => 'Link',                   
);    
$field_data[2] = array(
    'type' => 'file',
    'col_class' => 'col-md-3',
    'field_class' => '',
    'field_name' => 'image_2',    
    'field_label_name' => 'Logo Image',
    'recommended_width' => 250,
    'recommended_height' => 130,
);
$field_data[3] = array(
    'type' => 'file',
    'col_class' => 'col-md-3',
    'field_class' => '',
    'field_name' => 'image',    
    'field_label_name' => 'Background Image',
    'recommended_width' => 600,
    'recommended_height' => 350,
);
$config_data = array(
    'l' => $l,
    'wrapper_class' => 'api_admin_wrapper_post_'.$temp[0]['id'],
    'modal_class' => '',
    'title' => 'Edit', 
    'field_data' => $field_data,
    'selected_id' => $temp[0]['id'],
    'table_name' => 'sma_post',
    'table_id' => 'id',
    'upload_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/post',
    'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
);
$temp_admin = $this->api_admin->front_end_edit($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

$temp_display .= '
            </td>
        </tr>
    </table>
';
//-Post-4-------------------














//-Mobile-------------------
$temp_display .= '        
        <table width="100%" class="visible-xs">
';
$temp_display .= '
        <tr>
            <td class="api_height_5"></td>
        </tr>
';
//-Post-1-------------------
$temp = $this->api_select_data->get_post(159);
$data_view = array (
    'wrapper_class' => '',
    'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/post/'.$temp[0]['image_2'],
    'max_width' => 150,
    'max_height' => 70,
    'image_class' => 'img-responsive api_link_box_none',
    'image_id' => '',   
    'resize_type' => 'full',       
);
$temp_image = $this->api_helper->api_get_image($data_view);   
$temp_display .= '
            <tr>
            <td class="api_link_box_none" valign="middle" align="center">
                <a class="api_link_box_none" href="'.$temp[0]['link'].'">
                    <table width="100%">
                        <tr>
                            <td class="api_big api_temp_image_2" align="center" valign="middle" style="background-image:url('.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/post/thumb/'.$temp[0]['image'].');">
                            '.$temp_image['image'].'
                            <div class="api_height_5"></div>
                            '.$temp[0]['title_'.$l].'
                            </td>
                        </tr>
                    </table>
                </a>
            </td>
            </tr>
';
//-Post-1-------------------

$temp_display .= '
        <tr>
            <td class="api_height_10"></td>
        </tr>
';

//-Post-2-en-------------------
if ($l == 'en')  {
    $temp = $this->api_select_data->get_post(158);
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/post/'.$temp[0]['image_2'],
        'max_width' => 150,
        'max_height' => 70,
        'image_class' => 'img-responsive api_link_box_none',
        'image_id' => '',   
        'resize_type' => 'full',       
    );
    $temp_image = $this->api_helper->api_get_image($data_view);    
    $temp_display .= '
        <tr>
        <td class="api_link_box_none" valign="middle" align="center">
            <a class="api_link_box_none" href="'.$temp[0]['link'].'">
                <table width="100%">
                    <tr>
                        <td class="api_big api_temp_image_2" align="center" valign="middle" style="background-image:url('.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/post/thumb/'.$temp[0]['image'].');">
                            '.$temp_image['image'].'
                            '.$temp[0]['title_'.$l].'
                        </td>
                    </tr>
                </table>
            </a>
        </td>
        </tr>
    ';
}
//-Post-2-en------------------
else {
//-Post-2-ja------------------
    $temp = $this->api_select_data->get_post(161);
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/post/'.$temp[0]['image_2'],
        'max_width' => 150,
        'max_height' => 70,
        'image_class' => 'img-responsive api_link_box_none',
        'image_id' => '',   
        'resize_type' => 'full',       
    );
    $temp_image = $this->api_helper->api_get_image($data_view);
    
    $temp_display .= '
        <tr>
        <td class="api_link_box_none" valign="middle" align="center">
            <a class="api_link_box_none" href="'.$temp[0]['link'].'">
                <table width="100%">
                    <tr>
                        <td class="api_big api_temp_image_2" align="center" valign="middle" style="background-image:url('.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/post/thumb/'.$temp[0]['image'].');">
                            '.$temp_image['image'].'
                            <div class="api_height_5"></div>
                            '.$temp[0]['title_'.$l].'
                        </td>
                    </tr>
                </table>
            </a>
        </td>
        </tr>
    ';
//-Post-2-ja------------------
}


$temp_display .= '
        <tr>
            <td class="api_height_10"></td>
        </tr>
';


//-Post-3-------------------
$temp = $this->api_select_data->get_post(160);
$data_view = array (
    'wrapper_class' => '',
    'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/post/'.$temp[0]['image_2'],
    'max_width' => 150,
    'max_height' => 70,
    'image_class' => 'img-responsive api_link_box_none',
    'image_id' => '',   
    'resize_type' => 'full',       
);
$temp_image = $this->api_helper->api_get_image($data_view);
$temp_display .= '
            <tr>
            <td class="api_link_box_none" valign="middle" align="center">
                <a class="api_link_box_none" href="'.$temp[0]['link'].'">
                    <table width="100%">
                        <tr>
                            <td class="api_big api_temp_image_2" align="center" valign="middle" style="background-image:url('.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/post/thumb/'.$temp[0]['image'].');">
                            '.$temp_image['image'].'
                            <div class="api_height_5"></div>
                            '.$temp[0]['title_'.$l].'
                            </td>
                        </tr>
                    </table>
                </a>            
            </td>
            </tr>
';
//-Post-3-------------------

$temp_display .= '
        <tr>
            <td class="api_height_10"></td>
        </tr>
';

//-Post-4-------------------
$temp = $this->api_select_data->get_post(162);
$data_view = array (
    'wrapper_class' => '',
    'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/post/'.$temp[0]['image_2'],
    'max_width' => 150,
    'max_height' => 70,
    'image_class' => 'img-responsive api_link_box_none',
    'image_id' => '',   
    'resize_type' => 'full',       
);
$temp_image = $this->api_helper->api_get_image($data_view);
$temp_display .= '
            <tr>
            <td class="api_link_box_none" valign="middle" align="center">
                <a class="api_link_box_none" href="'.$temp[0]['link'].'">
                    <table width="100%">
                        <tr>
                            <td class="api_big api_temp_image_2" align="center" valign="middle" style="background-image:url('.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/post/thumb/'.$temp[0]['image'].');">
                            '.$temp_image['image'].'
                            <div class="api_height_5"></div>
                            '.$temp[0]['title_'.$l].'                            
                            </td>
                        </tr>
                    </table>
                </a>               
            </td>
            </tr>
    </table>
';
//-Post-4-------------------
//-mobile-------------------





$temp_display .= '
</div>
';


$config_data = array(
    'wrapper_class' => 'api_page_front_movie',
    'custom_html' => '',
    'display' => $temp_display,
    'type' => '',
);
$temp = $this->api_display->template_display($config_data);
echo $temp['display'];

?>                       

