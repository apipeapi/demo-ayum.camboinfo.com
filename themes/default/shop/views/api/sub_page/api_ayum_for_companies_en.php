<?php

//-banner-    
    $data_view = array (
        'wrapper_class' => 'hidden-xs',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/'.$this->api_web_image['for_companies_banner_en'],
        'max_width' => 2000,
        'max_height' => 450,
        'product_link' => '',
        'image_class' => 'img-responsive',
        'image_id' => '',   
        'resize_type' => 'full',       
    );
    $temp_image = $this->api_helper->api_get_image($data_view);

    $data_view = array (
        'wrapper_class' => 'visible-xs',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/'.$this->api_web_image['for_companies_banner_en_image_2'],
        'max_width' => 700,
        'max_height' => 450,
        'product_link' => '',
        'image_class' => 'img-responsive',
        'image_id' => '',   
        'resize_type' => 'full',       
    );
    $temp_image_mobile = $this->api_helper->api_get_image($data_view);

    $temp_display = '
        <div class="api_admin api_admin_wrapper_web_image_for_companies_banner_en">
            '.$temp_image['image_table'].'
            '.$temp_image_mobile['image_table'].'
    ';

    $field_data = array();
    $field_data[0] = array(
        'type' => 'file',
        'col_class' => 'col-md-6',
        'field_class' => '',
        'field_name' => 'image',    
        'field_label_name' => 'Image PC',
        'recommended_width' => 2000,
        'recommended_height' => 450,
    );
    $field_data[1] = array(
        'type' => 'file',
        'col_class' => 'col-md-6',
        'field_class' => '',
        'field_name' => 'image_2',    
        'field_label_name' => 'Image Mobile',
        'recommended_width' => 700,
        'recommended_height' => 450,
    );    
    $config_data = array(
        'l' => $l,
        'wrapper_class' => 'api_admin_wrapper_web_image_for_companies_banner_en',
        'modal_class' => '',
        'title' => 'Edit', 
        'field_data' => $field_data,
        'selected_id' => $this->api_web_image['for_companies_banner_en_id'],
        'table_name' => 'sma_web_image',
        'table_id' => 'id',
        'upload_path' => 'assets/uploads/web/'.$this->api_web[0]['id'],
        'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
    );
    $temp_admin = $this->api_admin->front_end_edit($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
        </div>    
    ';
    $config_data = array(
        'wrapper_class' => 'api_temp_banner',
        'custom_html' => '<div class="api_height_30 visible-xs"></div>',
        'display' => $temp_display,
        'type' => 'full',
    );
    $temp = $this->api_display->template_display($config_data);
    echo $temp['display'];

    $temp_display = '';
//-banner-

//-Welcome-    
    $include_data = array(
        'image_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['for_companies_welcome_en'],
        'image_name' => 'for_companies_welcome_en',
        'image_max_width' => 600,
        'image_max_height' => 400,
        'title_key' => 'what_METI',
        'description_key' => 'METI_Japan_Internship_Program_v2',        
    );
    include 'themes/default/shop/views/api/sub_page/api_ayum_welcome_table.php';
    $temp_display = '';
//-Welcome-    

$temp_display = '';
$temp_display .= '<div class="api_height_30 api_temp_8 hidden-sm hidden-xs"></div>';

$temp_display .= '
    <div class="api_height_30 api_clear_both"></div>
    <div class="col-md-12">            
        <div class="api_text_align_center">
            <div class="api_large api_title_1">
';
$temp_admin = $this->api_helper->api_lang_v2('កម្មវិធីហាត់ការរបស់ជប៉ុន ដែលហៅកាត់ថា “ METI ”');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
            </div>    
            <div class="api_large api_temp_border_1"></div>
        </div>
    </div>   
    <div class="api_height_30 api_clear_both hidden-xs"></div> 
    <div class="api_height_20 api_clear_both"></div> 
';

//-service-flow-
    $config_data = array(
        'table_name' => 'sma_blog_step',
        'select_table' => 'sma_blog_step',
        'translate' => 'yes',
        'translate_2' => 'yes',
        'description' => 'yes',
        'select_condition' => "c_id = 107 order by ordering asc",
    );
    $temp = $this->api_helper->api_select_data_v2($config_data);      
    $config_data = array(
        'l' => $l,
        'select_data' => $temp,
    );
    $temp_initial_data = $this->api_initial_data->blog($config_data);
    $config_data = array(
        'l' => $l,
        'select_data' => $temp_initial_data,
        'image_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/blog_step/thumb',
        'image_class' => 'api_img_blog_medium_v3',
        'step_class' => 'api_display_none',
        'seperator' => '
            <table width="100%" border="0">
            <tr>
            <td class="api_large" valign="middle" align="center">
                <div  width="100%" class="api_temp_arrow_down"> 
                <img src="'.base_url().'assets/api/page/ayum_for_companies/image/image_10.png" />
                </div>
            </tr>
            </table>
        ',  
        'category_id' => 107,      
    );
    $temp_view_as = $this->api_view_as->blog_step_v2($config_data);    
    $temp_display .= $temp_view_as['display'];

//-service-flow-

$temp_display .= '
    <div class="api_height_50 api_clear_both"></div>
    <div class="api_height_30 api_clear_both"></div>
    <div class="col-md-12">            
        <div class="api_text_align_center">
            <div class="api_large api_title_1">
';
$temp_admin = $this->api_helper->api_lang_v2('ករណីគំរូមួយរបស់អ្នកចូលរួមកម្មវិធីហាត់ការ');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

$temp_display .= '
            </div>    
            <div class="api_large api_temp_border_1"></div>
            <div class="api_height_20 api_clear_both"></div>
            <div class="api_title_2">
';

$temp_admin = $this->api_helper->api_lang_v2('Nam Cao Hoang Nguyen');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

$temp_display .= '
            </div>
        </div>
    </div>   
    <div class="api_height_30 api_clear_both"></div> 
    <div class="api_height_20 api_clear_both hidden-xs"></div> 
';


//-blog-step---
    $config_data = array(
        'table_name' => 'sma_blog_step',
        'select_table' => 'sma_blog_step',
        'translate' => 'yes',
        'translate_2' => 'yes',
        'description' => 'yes',
        'select_condition' => "c_id = 106 order by ordering asc",
    );
    $temp = $this->api_helper->api_select_data_v2($config_data);      
    $config_data = array(
        'l' => $l,
        'select_data' => $temp,
    );
    $temp_initial_data = $this->api_initial_data->blog_step($config_data);
    $config_data = array(
        'l' => $l,
        'select_data' => $temp_initial_data,
        'image_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/blog_step/thumb',
        'image_class' => 'api_img_blog_medium_v3',        
        'seperator' => '<div class="api_height_50 api_clear_both"></div>',        
        'category_id' => 106, 
    );
    $temp_view_as = $this->api_view_as->blog_step_v3($config_data);    
    $temp_display .= $temp_view_as['display'];
//-blog-step---

    $temp_display .= '
        <div class="api_height_20 api_clear_both"></div>
        <div class="api_height_30 api_clear_both hidden-xs"></div>
    ';

    $temp_display .= '
        <div class="col-md-12 text-center">
            <a href="javascript:void(0);" onclick="api_ayum_for_companies_contact_click();">
                <div class="api_button_1">
                    Contact
                    &nbsp; <li class="fa fa-arrow-right"></li>
                </div>       
            </a>     
        </div>
    ';

    $temp_display .= '
        <div class="api_height_30 api_clear_both"></div>
    ';

$config_data = array(
    'wrapper_class' => 'api_page_ayum_for_companies',
    'custom_html' => '',
    'display' => $temp_display,
    'display_class' => '',
    'panel' => '',
    'panel_class' => '',
    'type' => '',
);
$temp = $this->api_display->template_display($config_data);
echo $temp['display'];



?>