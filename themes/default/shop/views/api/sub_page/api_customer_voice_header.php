<?php
$return = array();
$return['display'] .= '
    <div class="api_customer_voice_header">
        <div class="api_temp_div_3 ">
            <div class="api_temp_div_1">
                <div class="api_big api_temp_title_1">
';
$temp_admin = $this->api_helper->api_lang_v2($config_data_function['title_1_key']);
$return['display'] .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

$return['display'] .= '
                </div>        
                <div class="api_big api_temp_title_2">
';

    $temp_admin = $this->api_helper->api_lang_v2($config_data_function['title_2_key']);
    $return['display'] .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

$return['display'] .= '
                </div>                            
            </div>
            <div class="api_temp_div_2">
                <div class="api_big api_temp_voice">
                    <img class="api_temp_image" src="'.base_url().'assets/api/page/default/image/image_2.png" />
';
$temp_admin = $this->api_helper->api_lang_v3($config_data_function['title_3_key']);
$return['display'] .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$return['display'] .= '
                </div>
                <div class="api_clear_both"></div>                  
            </div>
            <div class="api_big api_temp_title_2">
';
if ($l == 'en') {
    $temp_admin = $this->api_helper->api_lang_v2($config_data_function['title_4_key']);
    $return['display'] .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
}
$return['display'] .= '
            </div>
        </div>
';



$return['display'] .= '
    </div>
';  

?>