<?php
//-banner-    
    $temp_display = '
        <div class="api_temp_banner api_admin api_admin_wrapper_web_image_google_map">
            <table width="100%" border="0">
            <tr>
            <td valign="top" align="center" class="api_temp_td_1">
                <div class="api_big api_temp_title_3" style="position:relative; z-index:1">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Last_frontier_in_Cambodia');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
                </div>
                <div class="api_height_10"></div>
                <div class="api_admin_block api_admin_wrapper_translation_contact_us_v2" style="position:relative; z-index:1">
                    <a href="javascript:void(0);" onclick="api_ayum_for_companies_contact_click();">
                        <div class="api_button_1">
                            '.$this->api_helper->clear_all_tags($this->api_helper->api_lang('contact_us_v2')).'
                            &nbsp; <li class="fa fa-arrow-right"></li>
                        </div>       
                    </a>
    ';
    $temp = $this->site->api_select_some_fields_with_where("
        id
        "
        ,"sma_translation"
        ,"keyword = 'contact_us_v2'"
        ,"arr"
    );
    $field_data = array();
    $field_data[0] = array(
        'type' => 'translate_textarea',
        'col_class' => 'col-md-12',
        'field_class' => '',
        'field_name' => 'translate',
        'translate' => 'yes',
        'field_label_name' => 'Title',                      
    );
    $config_data = array(
        'l' => $l,
        'wrapper_class' => 'api_admin_wrapper_translation_contact_us_v2',
        'modal_class' => '',
        'title' => 'Edit', 
        'field_data' => $field_data,
        'selected_id' => $temp[0]['id'],
        'table_name' => 'sma_translation',
        'table_id' => 'id',
        'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
    );
    $temp_admin = $this->api_admin->front_end_edit($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];



    $temp_display .= '
                </div>

            </td>
            </tr>
            </table>
    ';
    
    $field_data = array();
    $field_data[0] = array(
        'type' => 'file',
        'col_class' => 'col-md-6',
        'field_class' => '',
        'field_name' => 'image',    
        'field_label_name' => 'Image PC',
        'recommended_width' => 2000,
        'recommended_height' => 500,
    );
    $field_data[1] = array(
        'type' => 'file',
        'col_class' => 'col-md-6',
        'field_class' => '',
        'field_name' => 'image_2',    
        'field_label_name' => 'Image Mobile',
        'recommended_width' => 700,
        'recommended_height' => 450,
    );    
    $config_data = array(
        'l' => $l,
        'wrapper_class' => 'api_admin_wrapper_web_image_for_companies_bg',
        'modal_class' => '',
        'title' => 'Edit', 
        'field_data' => $field_data,
        'selected_id' => $this->api_web_image['for_companies_bg_id'],
        'table_name' => 'sma_web_image',
        'table_id' => 'id',
        'upload_path' => 'assets/uploads/web/'.$this->api_web[0]['id'],
        'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
    );
    $temp_admin = $this->api_admin->front_end_edit($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];


    $temp_display .= '
        </div>
    ';

    $config_data = array(
        'wrapper_class' => 'api_temp_banner',
        'custom_html' => '<div class="api_height_30 visible-xs"></div>',
        'display' => $temp_display,
        'type' => 'full',
    );
    $temp = $this->api_display->template_display($config_data);
    echo $temp['display'];
    $temp_display = '';

echo '
<style>
.api_ja .api_temp_banner {
    background-image:url('.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/'.$this->api_web_image['for_companies_bg'].');
}
@media screen and (min-width: 100px) and (max-width:767px) {
.api_ja .api_temp_banner {
    background-image:url('.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/'.$this->api_web_image['for_companies_bg_image_2'].');
}    
}
</style>
';    
//-banner-

//-Welcome-    
    $include_data = array(
        'image_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['for_companies_welcome_ja'],
        'image_name' => 'for_companies_welcome_ja',
        'image_max_width' => 600,
        'image_max_height' => 400,
        'title_key' => 'ayum_for_companies_1',
        'description_key' => 'ayum_for_companies_2',
      
    );
    include 'themes/default/shop/views/api/sub_page/api_ayum_welcome_table.php';
//-Welcome-    

$temp_display = '';
$temp_display .= '<div class="api_height_30 api_temp_8 hidden-sm hidden-xs"></div>';

$temp_display .= '
    <div class="api_height_30 api_clear_both"></div>
    <div class="col-md-12">            
        <div class="api_text_align_center">
            <div class="api_large api_title_1 api_template_title_1">
';
$temp_admin = $this->api_helper->api_lang_v2('For Japan Client');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='    
            </div>    
            <div class="api_height_10"></div>
            <div class="api_template_title_2">
';
$temp_admin = $this->api_helper->api_lang_v2('Service for Japan');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='   
            </div>  
            <div class="api_height_30 hidden-xs"></div>    
        </div>
    </div>    
';

$data_view = array (
    'wrapper_class' => '',
    'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['for_companies_jp_image_2'],
    'max_width' => 0,
    'max_height' => 0,
    'product_link' => '',
    'image_class' => 'api_img_500_300',
    'image_id' => '',   
    'resize_type' => 'fixed',       
);
$temp_image = $this->api_helper->api_get_image($data_view);

$data_view = array (
    'wrapper_class' => '',
    'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['for_companies_jp_image_1'],
    'max_width' => 0,
    'max_height' => 0,
    'product_link' => '',
    'image_class' => 'api_img_500_300',
    'image_id' => '',   
    'resize_type' => 'fixed',       
);
$temp_image_2 = $this->api_helper->api_get_image($data_view);

$data_view = array (
    'wrapper_class' => '',
    'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['for_companies_jp_image_3'],
    'max_width' => 0,
    'max_height' => 0,
    'product_link' => '',
    'image_class' => 'api_img_500_300',
    'image_id' => '',   
    'resize_type' => 'fixed',       
);
$temp_image_3 = $this->api_helper->api_get_image($data_view);


$data_view = array (
    'wrapper_class' => '',
    'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['for_companies_jp_image_5'],
    'max_width' => 0,
    'max_height' => 0,
    'product_link' => '',
    'image_class' => 'api_img_500_300',
    'image_id' => '',   
    'resize_type' => 'fixed',       
);
$temp_image_4 = $this->api_helper->api_get_image($data_view);

//-1111--------------
    $temp_display .= '
        <div class="col-md-12 api_temp_wrapper_1 api_padding_0">
            <div class="col-md-6 col-sm-6">
                <div class="api_big api_temp_2">
';
$temp_admin = $this->api_helper->api_lang_v2('Interpretation and translation');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .=' 
                </div>
                <div class="api_temp_4 api_admin api_admin_wrapper_web_image_for_companies_jp_image_2">
                    '.$temp_image['image_table'].'
';
$config_data = array(
    'wrapper' => 'api_admin_wrapper_web_image_for_companies_jp_image_2',
    'name' => 'for_companies_jp_image_2',
    'recommended_width' => 700,
    'recommended_height' => 400,
);
$temp_admin = $this->api_helper->api_admin_web_image($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                </div>
                <div class="api_height_20"></div>

                <div class="api_temp_3" id="api_ayum_for_companies_api_temp_div_3">
                    <div class="api_big">
';
$temp_admin = $this->api_helper->api_lang_v2('Service details');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .=' 
                    </div>        
                    <div class="api_height_10"></div>    
                    <div class="">
';
$temp_admin = $this->api_helper->api_lang_v2('ayum_for_companies_3');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .=' 
                    </div>      
   
                    <div class="api_height_10"></div>        
                    <div class="api_big">
';
$temp_admin = $this->api_helper->api_lang_v2('Past achievements');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                    </div>      
                    <div class="api_height_10"></div>   
                    <div class="">
';
$temp_admin = $this->api_helper->api_lang_v2('ayum_for_companies_4');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                    </div>                       
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="api_big api_temp_2">
';
$temp_admin = $this->api_helper->api_lang_v2('Site inspection support');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                </div>
                <div class="api_temp_4 api_admin api_admin_wrapper_web_image_for_companies_jp_image_1">
                    '.$temp_image_2['image_table'].'
';
$config_data = array(
    'wrapper' => 'api_admin_wrapper_web_image_for_companies_jp_image_1',
    'name' => 'for_companies_jp_image_1',
    'recommended_width' => 700,
    'recommended_height' => 400,
);
$temp_admin = $this->api_helper->api_admin_web_image($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                </div>
                <div class="api_height_20"></div>

                <div class="api_temp_3" id="api_ayum_for_companies_api_temp_div_4">
                    <div class="api_big">
';
$temp_admin = $this->api_helper->api_lang_v2('Service details');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                    </div>        
                    <div class="api_height_10"></div>    
                    <div class="">
';
$temp_admin = $this->api_helper->api_lang_v2('ayum_for_companies_5');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                    </div>      
                    <div class="api_height_10"></div>
                    <div class="api_big">
';
$temp_admin = $this->api_helper->api_lang_v2('Do you have such a problem?');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                    </div>      
                    <div class="api_height_10"></div>   
                    <div class="">
';
$temp_admin = $this->api_helper->api_lang_v2('ayum_for_companies_6');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                    </div>                       
                </div>
            </div>
        </div>
    ';
//-1111--------------

$temp_display .= '
    <div class="api_height_30 api_clear_both"></div>
';

//-2222--------------
    $temp_display .= '
        <div class="col-md-12 api_temp_wrapper_1 api_padding_0">
            <div class="col-md-6 col-sm-6">
                <div class="api_big api_temp_2">
';
$temp_admin = $this->api_helper->api_lang_v2('(For individuals) Global fieldwork');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                </div>
                <div class="api_temp_4 api_admin api_admin_wrapper_web_image_for_companies_jp_image_3">
                    '.$temp_image_3['image_table'].'
';
$config_data = array(
    'wrapper' => 'api_admin_wrapper_web_image_for_companies_jp_image_3',
    'name' => 'for_companies_jp_image_3',
    'recommended_width' => 700,
    'recommended_height' => 400,
);
$temp_admin = $this->api_helper->api_admin_web_image($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                </div>
                <div class="api_height_20"></div>

                <div class="api_temp_3" id="api_ayum_for_companies_api_temp_div_5">
                    <div class="api_big">
';
$temp_admin = $this->api_helper->api_lang_v2('Service details');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                    </div>        
                    <div class="api_height_10"></div>    
                    <div class="">
';
$temp_admin = $this->api_helper->api_lang_v2('ayum_for_companies_6_v2');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                    </div>      
                    <div class="api_height_10"></div>
                    <div class="api_big">
';
$temp_admin = $this->api_helper->api_lang_v2('Check it out if you like this');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                    </div>      
                    <div class="api_height_10"></div>   
                    <div class="">
';
$temp_admin = $this->api_helper->api_lang_v2('ayum_for_companies_7');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                    </div> 
                    <div class="api_height_10"></div> 
                    <div class="api_temp_9 api_padding_right_20">
                        <div class="api_admin api_admin_wrapper_post_link_151">
                            <a href="'.$this->api_post_link['for_japan_link'].'" target="_blank">
                                <div class="api_button_readmore">
                                    MORE <li class="fa fa-arrow-right"></li>
                                </div>
                            </a>
';
$field_data = array();
$field_data[0] = array(
    'type' => 'text',
    'col_class' => 'col-md-12',
    'field_class' => '',
    'field_name' => 'link',
    'field_label_name' => 'More Link',                   
);
$config_data = array(
    'l' => $l,
    'wrapper_class' => 'api_admin_wrapper_post_link_151',
    'modal_class' => '',
    'title' => 'Edit', 
    'field_data' => $field_data,
    'selected_id' => 151,
    'table_name' => 'sma_post',
    'table_id' => 'id',
    'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
    'condition_ordering' => "",
);
$temp_admin = $this->api_admin->front_end_edit($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

$temp_display .= '
                        </div>
                    </div>
                    <div class="api_height_10 api_clear_both"></div>                  
                </div>
            </div>

            <div class="col-md-6 col-sm-6">
                <div class="api_big api_temp_2">
';
$temp_admin = $this->api_helper->api_lang_v2('(For corporations) <br> Global training');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                </div>
                <div class="api_temp_4 api_admin api_admin_wrapper_web_image_for_companies_jp_image_5">
                    '.$temp_image_4['image_table'].'
';
$config_data = array(
    'wrapper' => 'api_admin_wrapper_web_image_for_companies_jp_image_5',
    'name' => 'for_companies_jp_image_5',
    'recommended_width' => 700,
    'recommended_height' => 400,
);
$temp_admin = $this->api_helper->api_admin_web_image($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                </div>
                <div class="api_height_20"></div>

                <div class="api_temp_3" id="api_ayum_for_companies_api_temp_div_6">
                    <div class="api_big">
';
$temp_admin = $this->api_helper->api_lang_v2('Service details');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                    </div>        
                    <div class="api_height_10"></div>    
                    <div class="">
';
$temp_admin = $this->api_helper->api_lang_v2('ayum_for_companies_8');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                    </div>      
                    <div class="api_height_10"></div>
                    <div class="api_big">
';                        
$temp_admin = $this->api_helper->api_lang_v2('Do you have such a problem?');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                    </div>      
                    <div class="api_height_10"></div>   
                    <div class="">
';                        
$temp_admin = $this->api_helper->api_lang_v2('ayum_for_companies_9');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                    </div>                       
                </div>
            </div>
        </div>
    ';
//-2222--------------

$temp_display .= '
    <div class="api_height_50 api_clear_both hidden-xs"></div>
    <div class="api_height_20 api_clear_both visible-xs"></div>
';

//-3333--------------
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['for_companies_jp_image_4'],
        'max_width' => 0,
        'max_height' => 0,
        'product_link' => '',
        'image_class' => 'api_img_500_400',
        'image_id' => '',   
        'resize_type' => 'fixed',       
    );
    $temp_image = $this->api_helper->api_get_image($data_view);


    $temp_display .= '
        <div class="col-md-12">
            <div class="api_big api_temp_2">
';                        
$temp_admin = $this->api_helper->api_lang_v2('Recruitment and retention consulting for Cambodians (Japan)');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
            </div>
        </div>

        <div class="col-md-12 api_temp_wrapper_1 api_padding_0 hidden-xs">
            <table width="100%" border="0">
            <tr>
            <td class="api_padding_15 api_admin api_admin_wrapper_web_image_for_companies_jp_image_4" width="50%" valign="top">
                <div id="api_ayum_for_companies_api_temp_div_1">
                    '.$temp_image['image_table'].'
';
$config_data = array(
    'wrapper' => 'api_admin_wrapper_web_image_for_companies_jp_image_4',
    'name' => 'for_companies_jp_image_4',
    'recommended_width' => 800,
    'recommended_height' => 500,
);
$temp_admin = $this->api_helper->api_admin_web_image($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                </div>
            </td>
            <td class="api_padding_15" width="50%" valign="top">
                <div class="api_temp_3" id="api_ayum_for_companies_api_temp_div_2">
                    <div class="api_big">
';                        
$temp_admin = $this->api_helper->api_lang_v2('Service details');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                    </div>        
                    <div class="api_height_10"></div>    
                    <div class="">
';                        
$temp_admin = $this->api_helper->api_lang_v2('ayum_for_companies_10');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                    </div>      
                    <div class="api_height_10"></div>
                    <div class="api_big">
';                        
$temp_admin = $this->api_helper->api_lang_v2('Do you have such a problem?');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                    </div>      
                    <div class="api_height_10"></div>   
                    <div class="">
';                        
$temp_admin = $this->api_helper->api_lang_v2('ayum_for_companies_11');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                    </div>                       
                </div>            
            </td>
            </tr>
            </table>        
        </div>
        
        <div class="col-md-12 api_temp_wrapper_1 api_padding_0 visible-xs">
            <table width="100%" border="0">
            <tr>
            <td class="api_padding_15" width="50%" valign="top">
                <div id="api_ayum_for_companies_api_temp_div_1">
                    '.$temp_image['image_table'].'
                </div>
            </td>
            <tr>
            <tr>
            <td class="api_padding_15" width="50%" valign="top">
                <div class="api_temp_3" id="api_ayum_for_companies_api_temp_div_2">
                    <div class="api_big">
                        '.$this->api_helper->api_lang('Service details').'
                    </div>        
                    <div class="api_height_10"></div>    
                    <div class="">
                        '.$this->api_helper->api_lang('ayum_for_companies_10').'
                    </div>      
                    <div class="api_height_10"></div>
                    <div class="api_big">
                        '.$this->api_helper->api_lang('Do you have such a problem?').'
                    </div>      
                    <div class="api_height_10"></div>   
                    <div class="">
                        '.$this->api_helper->api_lang('ayum_for_companies_11').'
                    </div>                       
                </div>            
            </td>
            </tr>
            </table>        
        </div>
      
    ';
//-3333--------------

$temp_display .= '
    <div class="api_height_50 api_clear_both hidden-xs"></div>
    <div class="api_height_20 api_clear_both visible-xs"></div>

    <div class="col-md-12">            
        <div class="api_text_align_center">
            <div class="api_large api_title_1 api_template_title_1">
';                        
$temp_admin = $this->api_helper->api_lang_v2('PROJECT');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
            </div>    
            <div class="api_height_10"></div>
            <div class="api_template_title_2">
';                        
$temp_admin = $this->api_helper->api_lang_v2('Past projects');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
            </div>   
        </div>
    </div>    
';    

//-4444--------------
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['for_companies_jp_image_6'],
        'max_width' => 0,
        'max_height' => 0,
        'product_link' => '',
        'image_class' => 'api_img_500_300',
        'image_id' => '',   
        'resize_type' => 'fixed',       
    );
    $temp_image = $this->api_helper->api_get_image($data_view);

    $temp_display .= '    
    <div class="col-md-12 api_temp_wrapper_1 api_padding_0">
        <div class="col-md-6 col-sm-6">
            <div class="api_big api_temp_2">
                <div class="api_height_10"></div>
';                        
$temp_admin = $this->api_helper->api_lang_v2('Ministry_v3');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='     
            </div>
            <div class="api_temp_4">
                <div class="api_admin api_admin_wrapper_web_image_for_companies_jp_image_6" >
                    '.$temp_image['image_table'].'
';
$config_data = array(
    'wrapper' => 'api_admin_wrapper_web_image_for_companies_jp_image_6',
    'name' => 'for_companies_jp_image_6',
    'recommended_width' => 700,
    'recommended_height' => 400,
);
$temp_admin = $this->api_helper->api_admin_web_image($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                </div>
                <table width="100%" border="0">
                <tr>
                <td valign="middle" align="center">
                    <div class="api_height_10"></div>
                    <div class="api_height_100 api_display_table_cell api_vertical_align_middle">
                    <a class="api_link_box_none" href="'.$this->api_post_link['for_japan_link_2'].'">
                    <div class="api_admin api_admin_wrapper_web_image_for_companies_jp_logo_1">
';
$data_view = array (
    'wrapper_class' => '',
    'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['for_companies_jp_logo_1'],
    'max_width' => 250,
    'max_height' => 120,
    'product_link' => '',
    'image_class' => 'img-responsive',
    'image_id' => '',   
    'resize_type' => 'full',
    'table_height' => '',   
);
$temp_image = $this->api_helper->api_get_image($data_view);

$temp_display .= '
                        '.$temp_image['image_table'].' 
                    </a>  
';
$config_data = array(
    'wrapper' => 'api_admin_wrapper_web_image_for_companies_jp_logo_1',
    'name' => 'for_companies_jp_logo_1',
    'recommended_width' => 320,
    'recommended_height' => 120,
);
$temp_admin = $this->api_helper->api_admin_web_image($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                        </div> 
                    </div>   
                </td>
                </tr>
                </table>                
                            
            </div>

            <div class="api_height_10"></div>
            <div class="api_temp_7">
                <div class="api_temp_5">
';                        
$temp_admin = $this->api_helper->api_lang_v2('ayum_for_companies_13');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                </div>      
                <div class="api_height_30"></div>
                    <div class="api_admin_block api_admin_wrapper_post_link_for_japan_link_2 api_float_right">
                        <a class="api_temp_9" href="'.$this->api_post_link['for_japan_link_2'].'">
                            <div class="api_button_readmore">
                                MORE <li class="fa fa-arrow-right"></li>
                            </div>
                        </a>
';
$field_data = array();
$field_data[0] = array(
    'type' => 'text',
    'col_class' => 'col-md-12',
    'field_class' => '',
    'field_name' => 'link',
    'field_label_name' => 'More Link',                   
);
$config_data = array(
    'l' => $l,
    'wrapper_class' => 'api_admin_wrapper_post_link_for_japan_link_2',
    'modal_class' => '',
    'title' => 'Edit', 
    'field_data' => $field_data,
    'selected_id' => $this->api_post_link['for_japan_link_2_id'],
    'table_name' => 'sma_post',
    'table_id' => 'id',
    'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
);
$temp_admin = $this->api_admin->front_end_edit($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

$temp_display .= '
                    </div>
                <div class="api_height_15 api_clear_both"></div>                                 
            </div>
        </div>
    ';
    
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['for_companies_jp_image'],
        'max_width' => 0,
        'max_height' => 0,
        'product_link' => '',
        'image_class' => 'api_img_500_300',
        'image_id' => '',   
        'resize_type' => 'fixed',       
    );
    $temp_image = $this->api_helper->api_get_image($data_view);   

    $temp_display .= '
        <div class="col-md-6 col-sm-6">
            <div class="api_big api_temp_2">
                <div class="api_height_10"></div>
';                        
$temp_admin = $this->api_helper->api_lang_v2('ayum_for_companies_12');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='  
            </div>
            <div class="api_temp_4">
            <div class="api_admin api_admin_wrapper_web_image_for_companies_jp_image">
                '.$temp_image['image_table'].'
';
$config_data = array(
    'wrapper' => 'api_admin_wrapper_web_image_for_companies_jp_image',
    'name' => 'for_companies_jp_image',
    'recommended_width' => 700,
    'recommended_height' => 400,
);
$temp_admin = $this->api_helper->api_admin_web_image($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
            </div>    
                <table width="100%" border="0">
                <tr>
                <td valign="middle" align="center">
                    <div class="api_height_10"></div>
                    <div class="api_height_100 api_display_table_cell api_vertical_align_middle">
                    <a class="api_link_box_none" href="'.$this->api_post_link['for_japan_link_3'].'" target="_blank">
                    
                    <div class="api_admin api_admin_wrapper_web_image_for_companies_jp_logo_2">
                    <div class="api_height_20"></div>
';
$data_view = array (
    'wrapper_class' => '',
    'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['for_companies_jp_logo_2'],
    'max_width' => 250,
    'max_height' => 120,
    'product_link' => '',
    'image_class' => 'img-responsive',
    'image_id' => '',   
    'resize_type' => 'full',       
    'table_height' => '',
);
$temp_image = $this->api_helper->api_get_image($data_view);

$temp_display .= '
                        '.$temp_image['image_table'].'
                    </a>
                    ';
$config_data = array(
    'wrapper' => 'api_admin_wrapper_web_image_for_companies_jp_logo_2',
    'name' => 'for_companies_jp_logo_2',
    'recommended_width' => 320,
    'recommended_height' => 120,
);
$temp_admin = $this->api_helper->api_admin_web_image($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .='
                    </div>
                    </div>
                </td>
                </tr>
                </table>                            
            </div>
            <div class="api_height_10"></div>

            <div class="api_temp_7">
                <div class="api_temp_5">
';                        
$temp_admin = $this->api_helper->api_lang_v2('ayum_for_companies_14');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                </div>      
                <div class="api_height_30"></div>
                <div class="api_admin_block api_admin_wrapper_post_link_for_japan_link_3 api_float_right">
                    <a class="api_temp_9" href="'.$this->api_post_link['for_japan_link_3'].'" target="_blank">
                        <div class="api_button_readmore">
                            MORE <li class="fa fa-arrow-right"></li>
                        </div>
                    </a>
';
$field_data = array();
$field_data[0] = array(
    'type' => 'text',
    'col_class' => 'col-md-12',
    'field_class' => '',
    'field_name' => 'link',
    'field_label_name' => 'More Link',                   
);
$config_data = array(
    'l' => $l,
    'wrapper_class' => 'api_admin_wrapper_post_link_for_japan_link_3',
    'modal_class' => '',
    'title' => 'Edit', 
    'field_data' => $field_data,
    'selected_id' => $this->api_post_link['for_japan_link_3_id'],
    'table_name' => 'sma_post',
    'table_id' => 'id',
    'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
);
$temp_admin = $this->api_admin->front_end_edit($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

$temp_display .= '
                </div>

                <div class="api_height_15 api_clear_both"></div>                                 
            </div>
        </div>
    </div>
        
    ';
//-4444--------------

    $temp_display .= '
        <div class="api_height_50 api_clear_both"></div>
    ';

    $temp_display .= '
        <div class="col-md-12 text-center">
            <a href="javascript:void(0);" onclick="api_ayum_for_companies_contact_click();" style="width:250px;">
                <div class="api_button_1">
                <table width="100%" border="0">
                                    <tr>
                                        <td valign="middle" class="api_temp_td_5">
                                            '.$this->api_helper->clear_all_tags(lang('contact_us_v2')).' 
                                    
                                        </td>
                                        <td valign="middle" width="30">
                                            &nbsp;<li class="fa fa-arrow-right"></li>
                                        </td>
                                    </tr>
                                </table>
                   
                </div>       
            </a>     
        </div>
    ';

    $temp_display .= '
        <div class="api_height_30 api_clear_both"></div>
    ';

$config_data = array(
    'wrapper_class' => 'api_page_ayum_for_companies',
    'custom_html' => '',
    'display' => $temp_display,
    'display_class' => '',
    'panel' => '',
    'panel_class' => '',
    'type' => '',
);
$temp = $this->api_display->template_display($config_data);
echo $temp['display'];



?>