<?php
$config_data = array(
    'table_name' => 'sma_footer_menu',
    'select_table' => 'sma_footer_menu',
    'translate' => 'yes',
    'description' => '',
    'select_condition' => "id = ".$config_data_function['parent_id'],
);
$temp_parent = $this->api_helper->api_select_data_v2($config_data);

$temp_display = '';
$temp_display .= '
    <div class="api_temp_title_footer api_border_bottom api_text_transform_capitalize api_padding_top_20">
        <div class="api_admin api_temp_wrapper_footer_menu_'.$temp_parent[0]['id'].'">
            '.$temp_parent[0]['title_'.$l].'
';
            $field_data = array();
            $field_data_add = array();
            $field_data[0] = array(
                'type' => 'translate',
                'col_class' => 'col-md-12',
                'field_name' => 'translate',
                'translate' => 'yes',
                'field_label_name' => 'Title',                      
                'field_value' => $temp_parent[0]['title_'.$l],
            );                              
            $field_data_add[0] = array(
                'type' => 'hidden',
                'col_class' => 'col-md-12',
                'field_name' => 'parent_id',
                'field_label_name' => 'parent_id',                      
                'field_value' => $temp_parent[0]['id'],
            );
            $field_data_add[1] = array(
                'type' => 'text',
                'col_class' => 'col-md-12',
                'field_class' => '',
                'field_name' => 'link',
                'field_label_name' => 'Link',                   
                'field_value' => '',
            );             
            $field_data_add[2] = array(
                'type' => 'text',
                'col_class' => 'col-md-3',
                'field_class' => 'api_numberic_input',
                'field_name' => 'ordering',
                'field_label_name' => 'Ordering',                      
                'field_value' => '',
            );            
            $config_data = array(
                'l' => $l,
                'wrapper_class' => 'api_temp_wrapper_footer_menu_'.$temp_parent[0]['id'],
                'modal_class' => '',
                'title' => 'Edition', 
                'field_data' => $field_data,
                'selected_id' => $temp_parent[0]['id'],
                'table_name' => 'sma_footer_menu',
                'table_id' => 'id',
                'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
                'field_data_add' => $field_data_add,
                'add' => 'yes',
                'title_add' => 'Add new menu for <b>'.$temp_parent[0]['title_'.$l].'</b>', 
                'condition_ordering' => " and parent_id = ".$config_data_function['parent_id'],
            );

            $temp_admin = $this->api_admin->front_end_edit($config_data);
            $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
            $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

            $temp_display .= $temp_admin['display']; 
$temp_display .= '
        </div>
    </div>
';

$temp_display .= '
    <div class="api_height_10"></div>
';


$config_data = array(
    'table_name' => 'sma_footer_menu',
    'select_table' => 'sma_footer_menu',
    'translate' => 'yes',
    'description' => '',
    'select_condition' => "parent_id = ".$config_data_function['parent_id']." order by ordering asc",
);
$temp_menu = $this->api_helper->api_select_data_v2($config_data);

for ($i=0;$i<count($temp_menu);$i++) {
    $field_data = array();
    $field_data_add = array();
    $field_data[0] = array(
        'type' => 'translate',
        'col_class' => 'col-md-12',
        'field_name' => 'translate',
        'translate' => 'yes',
        'field_label_name' => 'Title',                      
        'field_value' => $temp_menu[$i]['title_'.$l],
    );
    $temp_type = 'text';
    if ($temp_menu[$i]['link'] == 'api_ayum_for_companies_contact_click();')
        $temp_type = 'hidden';
    if ($temp_menu[$i]['link'] == 'api_ayum_for_jobseeker_contact_click();')
        $temp_type = 'hidden';
    $field_data[1] = array(
        'type' => $temp_type,
        'col_class' => 'col-md-12',
        'field_name' => 'link',
        'field_label_name' => 'Link',                      
        'field_value' => $temp_menu[$i]['link'],
    );
    $field_data[2] = array(
        'type' => 'text',
        'col_class' => 'col-md-3',
        'field_class' => 'api_numberic_input',
        'field_name' => 'ordering',
        'field_label_name' => 'Ordering',                      
        'field_value' => $temp_menu[$i]['ordering'],
    );

    $field_data_add[0] = array(
        'type' => 'hidden',
        'col_class' => 'col-md-12',
        'field_name' => 'parent_id',
        'field_label_name' => 'parent_id',                      
        'field_value' => $config_data_function['parent_id'],
    );     
    $config_data = array(
        'l' => $l,
        'wrapper_class' => 'api_temp_wrapper_footer_menu_'.$temp_menu[$i]['id'],
        'modal_class' => '',
        'title' => 'Edit', 
        'field_data' => $field_data,
        'selected_id' => $temp_menu[$i]['id'],
        'table_name' => 'sma_footer_menu',
        'table_id' => 'id',
        'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
        'field_data_add' => '',
        'add' => '',
        'title_add' => '', 
        'condition_ordering' => " and parent_id = ".$config_data_function['parent_id'],
        'delete' => 'yes',
        'title_delete' => '<div>Your will remove</div>'.$temp_menu[$i]['title_'.$l],        
    );
    
    $temp_admin = $this->api_admin->front_end_edit($config_data);
    $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_link = 'href="'.$temp_menu[$i]['link'].'"';
    if ($temp_menu[$i]['link'] == 'api_ayum_for_companies_contact_click();')
        $temp_link = 'href="javascript:void(0);" onclick="api_ayum_for_companies_contact_click();"';
    if ($temp_menu[$i]['link'] == 'api_ayum_for_jobseeker_contact_click();')
        $temp_link = 'href="javascript:void(0);" onclick="api_ayum_for_jobseeker_contact_click();"';
    $temp_display .= '
        <div class="api_height_5"></div>
        <div class="api_admin api_temp_wrapper_footer_menu_'.$temp_menu[$i]['id'].'">
            <a class="api_footer_link" '.$temp_link.'>
                <table width="100%" border="0">
                <tr>
                <td valign="top" width="15">
                    >
                </td>
                <td valign="top">
                    '.$temp_menu[$i]['title_'.$l].'
                </td>
                </tr>
                </table>           
            </a>
            '.$temp_admin['display'].'
        </div>
    ';
}



?>

