<?php

$temp_name = 'form_for_jobseeker';
$temp_display .= '
<link rel="stylesheet" href="'.base_url().'assets/api/plugin/phone_number/css/intlTelInput.css?1613236686837">

<button type="button" id="api_modal_trigger_'.$temp_name.'" class="api_display_none" data-toggle="modal" data-target="#api_modal_'.$temp_name.'">Open Modal</button>
<!-- Modal -->
<div id="api_modal_'.$temp_name.'" class="modal fade api_'.$temp_name.'" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header api_display_none">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="api_modal_title">
        </h4>
      </div>
      <div class="modal-body" id="api_modal_body">
        <div class="api_height_30 visible-xs"></div>
        <div class="col-md-12 text-center">
            <div class="api_big api_temp_1"> 
            '.lang('Changing_Job_Support_Service_Application').'     
            </div>
        </div>
        <div class="api_height_15 api_clear_both"></div>

        <div class="api_temp_2">     
            <form action="'.base_url().'main/ayum_for_jobseeker_send_contact" name="api_'.$temp_name.'" method="post" enctype="multipart/form-data">      
            <input type="hidden" name="'.$this->security->get_csrf_token_name().'" value="'.$this->security->get_csrf_hash().'">
';
      if ($this->session->userdata('api_mobile') != 1) {
        //-form-------------
          $temp_display .= '            
            <table width="100%" border="0" >

            <tr>
            <td valign="middle" class="api_big api_temp_td_3 api_td_width_auto">
              <div class="api_height_30">'.lang('Name:').'</div> 
            </td>
          
            <td valign="middle" align="left" width="10" class="api_big api_temp_td_1 api_td_width_auto">
             : 
            <div class="api_height_30 api_requied api_error api_email_error api_color_red api_padding_bottom_3 api_padding_top_10">
                &nbsp;
              </div> 
            </td>
            <td valign="middle">
              <input class="api_temp_input" name="name" type="text"  value="" />
              <div class="api_height_30 api_requied api_error api_email_error api_color_red api_padding_bottom_3 api_padding_top_10">
                &nbsp;
              </div> 
            </td>            
            </tr>
            <tr>
            <td valign="middle" colspan="3" height="25"></td>
            </tr>

            <tr>
            <td valign="middle" class="api_big api_temp_td_3 api_td_width_auto">
              <div class="api_height_30">'.lang('Date of birth:').'</div>
            </td>
            <td valign="middle" align="left" width="10" class="api_big api_temp_td_1 api_td_width_auto">
              :
              <div class="api_height_30 api_requied api_error api_email_error api_color_red api_padding_bottom_3 api_padding_top_10">
                &nbsp;
              </div> 
            </td>
            <td valign="middle">
              <input class="api_temp_input" name="dob" type="text" placeholder="MM/DD/YYYY" value="" />
              <div class="api_height_30 api_requied api_error api_email_error api_color_red api_padding_bottom_3 api_padding_top_10">
                &nbsp;
              </div> 
            </td>            
            </tr>
            <tr>
              <td valign="middle" colspan="3" height="25"></td>
            </tr>
            <tr>
            <td valign="middle" class="api_big api_temp_td_3 api_td_width_auto">
              <div class="api_height_30">'.lang('Mail Address:').'</div>
            </td>
            <td valign="middle" align="left" width="10" class="api_big api_temp_td_1 api_td_width_auto">
              :
              <div class="api_height_30 api_requied api_error api_email_error api_color_red api_padding_bottom_3 api_padding_top_10">
                &nbsp;
              </div> 
            </td>
            <td valign="middle">              
              <input class="api_temp_input" name="email" type="text" required="required" value="" />  
              <div class="api_height_30 api_requied api_error api_email_error api_color_red api_padding_bottom_3 api_padding_top_10">
                &nbsp;
              </div> 
            </td>            
            </tr>
            <tr>
              <td valign="middle" colspan="3" height="25"></td>
            </tr>

            <tr>
            <td valign="middle" class="api_big api_temp_td_3 api_td_width_auto">
              <div class="api_height_30">'.lang('Phone number:').'</div>
            </td>
            <td valign="middle" align="left" width="10" class="api_big api_temp_td_1 api_td_width_auto">
              :
              <div class="api_height_30 api_requied api_error api_phone_error api_color_red api_padding_bottom_3 api_padding_top_10">
                &nbsp;
              </div> 
            </td>
            <td valign="middle">     
          ';

              if ($l == 'ja')
                $temp = '+81';
              else
                $temp = '+855';
              $temp_display .= '
                <input class="api_temp_input" name="phone" id="phone" type="tel" required="required" value="'.$temp.'" />   
                <div class="api_height_30 api_requied api_error api_phone_number_error api_color_red api_padding_bottom_3 api_padding_top_10">
                &nbsp;
                </div> 
                <script src="'.base_url().'assets/api/plugin/phone_number/js/intlTelInput.js?1613236686837"></script>
                <script>
                var input = document.querySelector("#phone");
                window.intlTelInput(input, {
                  utilsScript: "'.base_url().'assets/api/plugin/phone_number/js/utils.js?1613236686837"
                });
                </script>
              ';

          $temp_display .= '
            </td>            
            </tr>

            <tr>
              <td valign="middle" colspan="3" height="25">
              <div class="api_error api_file_error api_color_red api_padding_bottom_3 api_padding_top_10" style="display:none;">
                '.lang('validation_form').'
                </div> 
              </td>
            </tr>

            <tr>
            <td valign="middle" class="api_big api_temp_td_3 api_td_width_auto">
              <div class="api_height_10">'.lang('Attacted CV:').'</div>
              <div class="api_height_30  api_file_error api_color_red api_padding_bottom_3 api_padding_top_10">
                '.lang('required_v2').'
              </div>
            </td>
            <td valign="middle" align="left" width="10" class="api_big api_temp_td_1 api_td_width_auto">
              :
              <div class="api_height_30 api_requied api_error  api_color_red api_padding_bottom_3 api_padding_top_10">
                &nbsp;
              </div> 
            </td>
            <td valign="middle">              
              <input class="api_temp_input api_temp_input_file" name="api_temp_file_2" type="text"  id="api_temp_file_2" readonly="readonly" value="" onclick="$(\'#api_temp_file_1\').click();" />   
              <input class="api_display_none" id="api_temp_file_1" name="userfile" type="file" id="userfile" value="" onchange="$(\'#api_temp_file_2\').val(this.value);"/>     
              <div class="api_height_30 api_requied api_error  api_color_red api_padding_bottom_3 api_padding_top_10">
                &nbsp;
              </div>           
            </td>            
            </tr>
            <tr>
              <td valign="middle" colspan="3" height="25"></td>
            </tr>
            </table>          
            </form>          
          ';
        //-form-------------
      }
      else {
        //-form_mobile-------------
          $temp_display .= '
            <table width="100%" border="0">

            <tr>
            <td valign="middle">
              <span class="api_big api_font_size_14">
                '.lang('Name:').'
              </span>
              <input class="api_temp_input" name="name" type="text"  value="" />
            </td>            
            </tr>

            <tr>
            <td valign="middle">
              <div class="api_height_10"></div>
              <span class="api_big api_font_size_14">
                '.lang('Date of birth:').'
              </span>
              <input class="api_temp_input" name="dob" type="text" placeholder="MM/DD/YYYY" value="" />
            </td>            
            </tr>

            <tr>
            <td valign="middle">  
              <div class="api_height_10"></div>            
              <span class="api_big api_font_size_14">
                '.lang('Mail Address:').'
              </span>
              <input class="api_temp_input" name="email" type="text" required="required" value="" />  
            </td>            
            </tr>

            <tr>
            <td valign="middle">
              <div class="api_height_10"></div>                
              <span class="api_big api_font_size_14">
                '.lang('Phone number:').'
              </span>
          ';

              if ($l == 'ja')
                $temp = '+81';
              else
                $temp = '+855';
              $temp_display .= '
                <input class="api_temp_input" name="phone" id="phone" type="tel" required="required" value="'.$temp.'" />   
                
                <script src="'.base_url().'assets/api/plugin/phone_number/js/intlTelInput.js?1613236686837"></script>
                <script>
                var input = document.querySelector("#phone");
                window.intlTelInput(input, {
                  utilsScript: "'.base_url().'assets/api/plugin/phone_number/js/utils.js?1613236686837"
                });
                </script>
              ';

          $temp_display .= '
            </td>            
            </tr>

            <tr>
            <td valign="middle">       
              <div class="api_height_10"></div>            
              <span class="api_big api_font_size_14">
                '.lang('Attacted CV:').'
                <div class="api_requied api_error  api_color_red api_padding_bottom_3 api_padding_top_10">
                '.lang('required_v2').'
                </div>
              </span>
              <input class="api_temp_input api_temp_input_file" name="file_2" type="text"  id="api_temp_file_2" readonly="readonly" value="" onclick="$(\'#api_temp_file_1\').click();" />   

              <input class="api_display_none" id="api_temp_file_1" name="file" type="file" value="" onchange="$(\'#api_temp_file_2\').val(this.value);"/>               
            </td>            
            </tr>
            </table>                
          ';
        //-form_mobile-------------        
      }
$temp_display .= '
            <div class="api_height_20 api_clear_both"></div>
            <div class="col-md-6 col-xs-6 api_padding_right_5 api_padding_left_0">
                <a href="javascript:void(0);" id="api_temp_button_1" onclick="api_'.$temp_name.'_submit();">
                  <div class="api_link_box_none api_temp_button_1" >
                  '.lang('Send').'
                  </div>
                </a>
            </div>
            <div class="col-md-6 col-xs-6 api_padding_right_0 api_padding_left_5">
                <a class="api_temp_button_2" href="javascript:void(0);" data-dismiss="modal" >
                  <div class="api_link_box_none api_temp_button_1" >
                  '.lang('Cancel').'
                  </div>
                </a>                
            </div>
            <div class="api_clear_both"></div>
            </form>
';            
        $temp_display .= '        
        </div>

      </div>
      <div class="modal-footer api_display_none" id="api_modal_footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">
            '.lang('Cancel').'
        </button>
      </div>
    </div>
  </div>
</div>
';

echo '

';
?>

<script>
  var temp_lang_1 = "<?php echo lang('Success'); ?>";
  var temp_lang_2 = "<?php echo lang('Successfully send service application.'); ?>";


</script>



