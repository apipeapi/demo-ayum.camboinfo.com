<?php
$temp_display = '';
$config_data = array(
    'table_name' => 'sma_blog',
    'select_table' => 'sma_blog',
    'translate' => 'yes',
    'description' => 'yes',
    'select_condition' => "add_ons like '%:featured:{yes}:%' order by created_date desc limit 5",    
);
//'select_condition' => "id = 151 or id = 152",
$select_data = $this->api_helper->api_select_data_v2($config_data);


$language_array = unserialize(multi_language);
$select_view = array();
for ($i=0;$i<count($select_data);$i++) {    
    for ($i2=0;$i2<count($language_array);$i2++) {
        $select_view[$i]['title_'.$language_array[$i2][0]] = $select_data[$i]['title_'.$language_array[$i2][0]];
        $select_view[$i]['intro_'.$language_array[$i2][0]] = $this->sma->decode_html($select_data[$i]['description_'.$language_array[$i2][0]]);             
    }    
    $select_view[$i]['id'] = $select_data[$i]['id'];
    $temp = $this->api_select_data->blog_category_slug($select_data[$i]['c_id']);
    $select_view[$i]['link'] = base_url().'Blog/'.$temp.'/'.$select_data[$i]['id'];
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/blog/thumb/'.$select_data[$i]['image'],
        'max_width' => 500,
        'max_height' => 300,
        'product_link' => '',
        'image_class' => 'api_img_blog_medium api_link_box_none',
        'image_id' => '',   
        'resize_type' => 'fixed',       
    );
    $temp_image = array();
    $temp_image = $this->api_helper->api_get_image($data_view);

    $select_view[$i]['image'] = '
        <a href="'.$select_view[$i]['link'].'">
            '.$temp_image['image_table'].'
        </a>    
    ';
    

    $select_view[$i]['date'] = $select_data[$i]['date'];
    $select_view[$i]['image_title_center'] = $select_data[$i]['image_title_center'];
    $select_view[$i]['image_title_bg'] = $this->api_helper->get_category_title($l,$select_data[$i]['c_id'],'sma_blog_category');
}

$select_view_1 = array();
$select_view_2 = array();
for ($i=0;$i<2;$i++) {    
    $select_view_1[$i] = $select_view[$i];
}
$k = 0;
for ($i=2;$i<count($select_view);$i++) {    
    $select_view_2[$k] = $select_view[$i];
    $k++;
}

$config_data = array();
$temp_config = $this->api_display->large_icon_config($config_data);
$config_data = $temp_config['config_data'];
$config_data['lang_key'] = $this->api_shop_setting[0]['api_lang_key'];
$config_data['select_data'] = $select_view_1;

$config_data['image_class'] = 'img-responsive api_link_box_none';
$config_data['view_all'] = '';
$config_data['col_class'] = 'col-md-12 col-sm-12 col-xs-12';
$config_data['title_class'] = 'api_big';
$config_data['intro_class'] = 'api_display_none';
$config_data['readmore_class'] = 'api_display_none';
$config_data['date_class'] = 'api_color_gray api_text_align_left';
$config_data['view_all'] = '
    <div class="api_height_30"></div>
    <a href="'.base_url().'Blog">
        <div class="api_button_readmore">
            MORE <li class="fa fa-arrow-right"></li>
        </div>
    </a>
    <div class="api_height_15"></div>    
';
$config_data['image_class'] = 'api_big api_font_size_14';
$config_data['file_path'] = 'assets/uploads/web/'.$this->api_web[0]['id'].'/blog';
if (count($select_view_1) > 0)
    $temp_blog = $this->api_display->large_icon_news($config_data);
$config_data['select_data'] = $select_view_2;
$config_data['col_class'] = 'col-md-4 col-sm-4 col-xs-12';
if (count($select_view_2) > 0)
    $temp_blog_2 = $this->api_display->large_icon_news($config_data);


if ($this->session->userdata('user_id') != '') {
    $temp_display = '

        <div class="col-md-12">
                <div class="api_text_align_center">      
                <div class="api_large api_title_1 api_template_title_1">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('AYUM BLOG');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
                </div>    
                <div class="api_height_10"></div>
                <div class="api_text_transform_capitalize api_template_title_2">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Various things in Cambodia');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
                </div>  
                <div class="api_height_30"></div>    
            </div>
            <div class="api_height_30"></div> 
        </div>
        <div class="api_temp_front_1 hidden-xs">
            <div class="col-md-6 col-sm-6">
    ';
            if ($temp_blog['array'][0] != '')
                $temp_display .= $temp_blog['array'][0];
        $temp_display .= '
            </div>
            <div class="col-md-6 col-sm-6">
        ';
            if ($temp_blog['array'][1] != '')
                $temp_display .= $temp_blog['array'][1];
        $temp_display .= '
            </div>
        </div>
        <div class="api_temp_front_1 visible-xs">
            <div class="col-md-12 api_padding_0">
        ';
            if ($temp_blog['array'][0] != '')
                $temp_display .= $temp_blog['array'][0];
        $temp_display .= '
            </div>
            <div class="col-md-12 api_padding_0">
        ';
            if ($temp_blog['array'][1] != '')
                $temp_display .= $temp_blog['array'][1];
        $temp_display .= '
            </div>
        </div>    
        <div class="api_height_15"></div>    
        '.$temp_blog_2['display'].'
    ';

    $config_data = array(
        'wrapper_class' => 'api_front_ayum_blog',
        'custom_html' => '',
        'display' => $temp_display,
        'type' => '',
    );
    $temp = $this->api_display->template_display($config_data);
    echo $temp['display'];
}

?>                       

