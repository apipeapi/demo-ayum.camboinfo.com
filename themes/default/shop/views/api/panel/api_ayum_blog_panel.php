<?php
$l = $this->api_shop_setting[0]['api_lang_key'];
$temp_panel_name = 'api_ayum_blog_panel';

$temp_display_panel = '';
$temp_display_panel .= '
<link href="'.base_url().'/assets/api/panel/api_ayum_blog_panel/css/style.css?v=100" rel="stylesheet">
<link href="'.base_url().'/assets/api/panel/api_ayum_blog_panel/css/style_600.css" rel="stylesheet">
<link href="'.base_url().'/assets/api/panel/api_ayum_blog_panel/css/style_768.css" rel="stylesheet">
<link href="'.base_url().'/assets/api/panel/api_ayum_blog_panel/css/style_1024.css" 
rel="stylesheet">
';



$config_data = array(
    'table_name' => 'sma_blog',
    'select_table' => 'sma_blog',
    'translate' => 'yes',
    'description' => 'yes',
    'select_condition' => "id > 0 order by view_count desc limit 5",
);
$temp_select_data = $this->api_helper->api_select_data_v2($config_data);

$language_array = unserialize(multi_language);
$select_view = array();
for ($i=0;$i<count($temp_select_data);$i++) {    
    for ($i2=0;$i2<count($language_array);$i2++) {
        $select_view[$i]['title_'.$language_array[$i2][0]] = $temp_select_data[$i]['title_'.$language_array[$i2][0]];
        $select_view[$i]['intro_'.$language_array[$i2][0]] = $this->sma->decode_html($temp_select_data[$i]['description_'.$language_array[$i2][0]]);             
    }    
    $config_data_2 = array(
        'table_name' => 'sma_blog_category',
        'select_table' => 'sma_blog_category',
        'translate' => 'yes',
        'description' => '',
        'select_condition' => "id = ".$temp_select_data[$i]['c_id'],
    );
    $temp_category = $this->api_helper->api_select_data_v2($config_data_2);  

    $select_view[$i]['id'] = $temp_select_data[$i]['id'];
    $select_view[$i]['link'] = base_url().'Blog/'.$temp_category[0]['slug'].'/'.$temp_select_data[$i]['id'];
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/blog/thumb/'.$temp_select_data[$i]['image'],
        'max_width' => 0,
        'max_height' => 0,
        'product_link' => '',
        'image_class' => 'api_link_box_none api_img_blog_small_v2',
        'image_id' => '',   
        'resize_type' => 'fixed',
    );
    $temp_image = $this->api_helper->api_get_image($data_view);
    $select_view[$i]['image'] = '
        <a class="api_link_box_none" href="'.$select_view[$i]['link'].'">
            '.$temp_image['image_table'].'
        </a>
    ';
    $select_view[$i]['date'] = $temp_select_data[$i]['created_date'];
}

$config_data = array();
$temp_config = $this->api_display->blog_config($config_data);
$config_data = $temp_config['config_data'];
$config_data['lang_key'] = $this->api_shop_setting[0]['api_lang_key'];
$config_data['select_data'] = $select_view;
$config_data['border_bottom'] = '
    <div class="api_height_10"></div>
    <div class="api_border_bottom"></div>
    <div class="api_height_15"></div>
';
$config_data['view_all'] = '';
$config_data['date_class'] = 'api_display_none';
$config_data['intro_class'] = 'api_display_none';
$config_data['wrapper_class'] = 'ayum_blog_latest';
$config_data['display_index_number'] = 'yes';
$config_data['file_path'] = 'assets/uploads/web/'.$this->api_web[0]['id'].'/blog';
$temp_latest = $this->api_display->blog($config_data);

if ($l == 'ja')
    $temp_facebook = '
        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v10.0&appId=165659780599699&autoLogAppEvents=1" nonce="fbPl3O7B"></script>    
        
        <div class="fb-page" data-href="https://www.facebook.com/ayumjapan" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/ayumjapan" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/ayumjapan">Ayum Japan Consulting</a></blockquote></div>
    ';

if ($l == 'en')
    $temp_facebook = '
        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v10.0&appId=165659780599699&autoLogAppEvents=1" nonce="fbPl3O7B"></script>    

        <div class="fb-page" data-href="https://www.facebook.com/joburgentPP" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/joburgentPP" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/joburgentPP">ការងារ Job board by Ayum</a></blockquote></div>
    ';
//Tags_blog    


$temp_display_panel .= '
    <div class="col-md-12 hidden-sm hidden-xs '.$temp_panel_name.'">
        <form action="'.base_url().'Blog" name="api_form_blog" method="GET">
            <input class="api_temp_search" name="search" placeholder="'.lang('Search').'" value="'.$_GET['search'].'" onkeydown="if (event.keyCode == 13) document.api_form_blog.submit();"/>
        </form>
        <div class="api_temp_search_icon" onclick="document.api_form_blog.submit();">
            <li class="fa fa-search"></li>
        </div>

        <div class="api_height_30 api_clear_both"></div>
        <table width="100%" border="0">
        <tr>
        <td valign="top" align="center">
            <div class="api_large api_temp_title_1 api_template_title_1">
';
$temp_admin = $this->api_helper->api_lang_v2('RANKING');
$temp_display_panel .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

$temp_display_panel .= '
            </div>
            <div class="api_height_5 api_clear_both"></div>    
            <div class="api_template_title_2">
';
$temp_admin = $this->api_helper->api_lang_v2('Popular articles');
$temp_display_panel .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

$temp_display_panel .= '
            </div>        
        </td>
        </tr>
        </table>
        <div class="api_height_15"></div>
        '.$temp_latest['display'].'

        <div class="api_height_30 api_clear_both"></div>
        <table width="100%" border="0">
        <tr>
        <td valign="top" align="center">
            <div class="api_large api_temp_title_1">
';
$temp_admin = $this->api_helper->api_lang_v2('FACEBOOK');
$temp_display_panel .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display_panel .= '
            </div>    
            <div class="api_font_size_18 api_text_transform_capitalize">
';
$temp_admin = $this->api_helper->api_lang_v2('Ayum_facebook');
$temp_display_panel .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display_panel .= '
            </div>        
        </td>
        </tr>
        </table>
        <div class="api_height_15"></div>
        '.$temp_facebook.'
        <div class="api_height_30 api_clear_both"></div>

        <table width="100%" border="0">
        <tr>
        <td valign="top" align="center">
            <div class="api_large api_temp_title_1">
';
$temp_admin = $this->api_helper->api_lang_v2('TAGS');
$temp_display_panel .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display_panel .= '
            </div>    
            <div class="api_font_size_18 api_text_transform_capitalize">
';
$temp_admin = $this->api_helper->api_lang_v2('tags_v2');
$temp_display_panel .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display_panel .= '
            </div>        
        </td>
        </tr>
        </table>
        <div class="api_height_15"></div> 
        ';
            $config_data_3 = array(
                'table_name' => 'sma_blog_tags',
                'select_table' => 'sma_blog_tags',
                'translate' => 'yes',
                'select_condition' => "id > 0 order by 'title_en' asc ",
            );
            $temp_select_data_2 = $this->api_helper->api_select_data_v2($config_data_3);        
            for ($k=0;$k<count($temp_select_data_2);$k++) {
                $temp_display_panel .= '
                    <div class="api_admin_block api_admin_wrapper_blog_tag_'.$temp_select_data_2[$k]['id'].'">
                ';
                if($_GET['tag_id'] == $temp_select_data_2[$k]['id']){
                    $temp_display_panel .=  ' 
                    <a href="'.base_url().'Blog?tag_id='.$temp_select_data_2[$k]['id'].'">
                        <div class="api_tag_box_selected">
                            '.$temp_select_data_2[$k]['title_'.$l].'
                        </div> 
                    </a>
                    ';   
                } else {
                    $temp_display_panel .= ' 
                    <a href="'.base_url().'Blog?tag_id='.$temp_select_data_2[$k]['id'].'">
                        <div class="api_tag_box_1">
                            '.$temp_select_data_2[$k]['title_'.$l].'
                        </div> 
                    </a>
                    ';   
                }     
                $field_data = array();
                $field_data_add = array();
                $field_data[0] = array(
                    'type' => 'translate',
                    'col_class' => 'col-md-12',
                    'field_class' => '',
                    'field_name' => 'translate',
                    'translate' => 'yes',
                    'field_label_name' => 'Tag',
                    'required' => 'yes',
                );
                $config_data = array(
                    'l' => $l,
                    'wrapper_class' => 'api_menu_wrapper_'.$temp_select_data_2[$k]['id'],
                    'modal_class' => '',
                    'title' => 'Edit', 
                    'field_data' => $field_data,
                    'selected_id' => $temp_select_data_2[$k]['id'],
                    'table_name' => 'sma_blog_tags',
                    'table_id' => 'id',
                    'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
                    'condition_ordering' => "",
                    'field_data_add' => $field_data_add,
                    'add' => 'yes',
                    'title_add' => 'Add New Tag',
                    'delete' => 'yes',
                    'title_delete' => '<div>Your will remove</div>'.$temp_select_data_2[$k]['title_'.$l],
                );
                $temp_admin = $this->api_admin->front_end_edit($config_data);
                $temp_display_panel .= $temp_admin['display'];
                $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

                $temp_display_panel .= '
                    </div>
                ';                
            }         
    $temp_display_panel .= '     
    </div>
    <div class="col-md-12 visible-sm visible-xs api_padding_0 '.$temp_panel_name.'">
        <div class="col-md-6 col-sm-6">

            <div class="api_height_30 api_clear_both"></div>
            <table width="100%" border="0">
            <tr>
            <td valign="top" align="center">
                <div class="api_large api_temp_title_1">
                    '.lang('RANKING').'
                </div> 
                <div class="api_height_5 api_clear_both"></div>     
                <div class="api_font_size_18 api_text_transform_capitalize">
                    '.lang('Popular articles').'
                </div>        
            </td>
            </tr>
            </table>
            <div class="api_height_15"></div>
            '.$temp_latest['display'].'
        </div>

        <div class="col-md-6 col-sm-6">

            <div class="api_height_30 api_clear_both"></div>
            <table width="100%" border="0">
            <tr>
            <td valign="top" align="center">
                <div class="api_large api_temp_title_1">
                    '.lang('FACEBOOK').'
                </div>    
                <div class="api_font_size_18 api_text_transform_capitalize">
                    '.lang('Ayum_facebook').'
                </div>        
            </td>
            </tr>
            </table>
            <div class="api_height_15"></div>
            '.$temp_facebook.'
            <div class="api_height_30 api_clear_both"></div>
            <table width="100%" border="0">
                <tr>
                <td valign="top" align="center">
                    <div class="api_large api_temp_title_1">
                        '.lang('TAGS').'
                    </div>
                    <div class="api_font_size_18 api_text_transform_capitalize">
                        '.lang('tags_v2').'
                    </div>       
                </td>
                </tr>
            </table>
            <div class="api_height_15"></div>
';
for ($k=0;$k<count($temp_select_data_2);$k++) {
    $temp_display_panel .= '
        <div class="api_admin_block api_admin_wrapper_blog_tag_'.$temp_select_data_2[$k]['id'].'">
    ';
    if($_GET['tag_id'] == $temp_select_data_2[$k]['id']){
        $temp_display_panel .=  ' 
        <a href="'.base_url().'Blog?tag_id='.$temp_select_data_2[$k]['id'].'">
            <div class="api_tag_box_selected">
                '.$temp_select_data_2[$k]['title_'.$l].'
            </div> 
        </a>
        ';   
    } else {
        $temp_display_panel .= ' 
        <a href="'.base_url().'Blog?tag_id='.$temp_select_data_2[$k]['id'].'">
            <div class="api_tag_box_1">
                '.$temp_select_data_2[$k]['title_'.$l].'
            </div> 
        </a>
        ';   
    }     
    
    $temp_display_panel .= '
        </div>
    ';                
}         
$temp_display_panel .= '
            <div class="api_height_30 api_clear_both"></div>  
        </div>
    </div>
';

?>