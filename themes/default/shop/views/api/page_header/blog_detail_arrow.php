<?php
$temp_panel_name = 'api_page_header_blog_detail_arrow';

$temp_display_page_header = '';
// $temp_display_page_header .= '
// <link href="'.base_url().'/assets/api/panel/api_ayum_blog_panel/css/style.css" rel="stylesheet">
// <link href="'.base_url().'/assets/api/panel/api_ayum_blog_panel/css/style_600.css" rel="stylesheet">
// <link href="'.base_url().'/assets/api/panel/api_ayum_blog_panel/css/style_768.css" rel="stylesheet">
// <link href="'.base_url().'/assets/api/panel/api_ayum_blog_panel/css/style_1024.css" 
// rel="stylesheet">
// ';
$config_data_2 = array(
    'table_name' => 'sma_blog_category',
    'select_table' => 'sma_blog_category',
    'translate' => 'yes',
    'description' => '',
    'select_condition' => "id = ".$select_data[0]['c_id'],
);
$temp_category = $this->api_helper->api_select_data_v2($config_data_2);  

$temp_display_page_header .= '
    <div class="col-md-12 '.$temp_panel_name.'">
        <div class="api_temp_page_header">
            <a href="'.base_url().'Blog">
                Blog
            </a>

            <span class="fa fa-long-arrow-right api_padding_left_5 api_padding_right_5"></span>

            <a href="'.base_url().'Blog/'.$temp_category[0]['slug'].'">
                '.$temp_category[0]['title_'.$l].'
            </a>
        </div>
    </div>
';

?>