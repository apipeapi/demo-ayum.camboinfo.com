<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php 
    $l = $this->api_shop_setting[0]['api_lang_key'];
    $temp_page_name = 'ayum_company';
    echo '
<link href="'.base_url().'/assets/api/page/ayum_company/css/style.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/ayum_company/css/style_600.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/ayum_company/css/style_768.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/ayum_company/css/style_1024.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
    ';

    $temp_display = '
        <div class="api_temp_banner hidden-xs">
            <table width="100%" border="0">
            <tr>
            <td valign="middle" align="center" width="50%">
                <div class="api_admin api_admin_wrapper_web_image_company_banner">
                    <img class="img-responsive" src="'.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/'.$this->api_web_image['company_banner'].'" />
    ';
    $field_data = array();
    $field_data[0] = array(
        'type' => 'file',
        'col_class' => 'col-md-6',
        'field_class' => '',
        'field_name' => 'image',    
        'label_name' => 'Image',
        'field_value' => $this->api_web_image['company_banner'],
        'recommended_width' => 1000,
        'recommended_height' => 667,
    );
    $config_data = array(
        'l' => $l,
        'wrapper_class' => 'api_admin_wrapper_web_image_company_banner',
        'modal_class' => '',
        'title' => 'Edit', 
        'field_data' => $field_data,
        'selected_id' => $this->api_web_image['company_banner_id'],
        'table_name' => 'sma_web_image',
        'table_id' => 'id',
        'upload_path' => 'assets/uploads/web/'.$this->api_web[0]['id'],
        'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
    );
    $temp_admin = $this->api_admin->front_end_edit($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
                </div>
            </td>
            <td valign="middle" align="center" width="55%">
                <div class="api_big api_temp_title_3">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('SMILE×Walk Together_v2');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
                
    $temp_display .= '
                </div>
                <div class="api_height_5"></div>
                <div class="api_temp_title_4 ">

    ';
                    $temp_admin = $this->api_helper->api_lang_v2('We express the feeling');
                    $temp_display .= $temp_admin['display'];
                    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
                </div>
            </td>
            </tr>
            </table>            
        </div>
        <div class="api_temp_banner visible-xs">
            <table width="100%" border="0">
            <tr>
            <td valign="middle" align="center" width="50%" id="api_temp_id_1">
                <img class="img-responsive" src="'.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/'.$this->api_web_image['company_banner'].'" />
            </td>
            </tr>
            <tr>
            <td valign="middle" align="center" width="50%" id="api_temp_id_2">
                <div class="api_big api_temp_title_3">
                    '.$this->api_helper->api_lang('SMILE×Walk Together_v2').'
                </div>
                <div class="api_height_5"></div>
                <div class="api_big api_temp_title_4">
                    '.$this->api_helper->clear_all_tags($this->api_helper->api_lang('We express the feeling')).'
                </div>
            </td>
            </tr>
            </table>            
        </div>        
    ';
    $config_data = array(
        'wrapper_class' => 'api_page_ayum_company',
        'custom_html' => '',
        'display' => $temp_display,
        'type' => 'full',
    );
    $temp = $this->api_display->template_display($config_data);
    echo $temp['display'];

    echo '<div class="api_height_30 hidden-xs"></div>';
    $temp_display = '
        <div class="col-md-12">
            <div class="api_text_align_center">
                <div class="api_large api_title_1">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Top Message');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
                </div>    
                <div class="api_height_10"></div>
                <div class="api_text_transform_capitalize api_template_title_2">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Message from the President');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
                </div>  
                <div class="api_height_30 hidden-xs"></div>    
            </div>
            <div class="api_height_30 hidden-sm hidden-xs"></div> 
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="api_admin api_admin_wrapper_web_image_company_welcome">
    ';
            $data_view = array (
                'wrapper_class' => '',
                'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['company_welcome'],
                'max_width' => 0,
                'max_height' => 0,
                'product_link' => '',
                'image_class' => 'api_padding_right_15_pc api_img_web_medium_v2',
                'image_id' => '',   
                'resize_type' => 'fixed',
                'table_height' => '',
            );
            $temp_image = $this->api_helper->api_get_image($data_view);

            $field_data = array();
            $field_data[0] = array(
                'type' => 'file',
                'col_class' => 'col-md-6',
                'field_class' => '',
                'field_name' => 'image',    
                'label_name' => 'Image',
                'field_value' => $this->api_web_image['company_welcome'],
                'recommended_width' => 700,
                'recommended_height' => 350,
            );
            $config_data = array(
                'l' => $l,
                'wrapper_class' => 'api_admin_wrapper_web_image_company_welcome',
                'modal_class' => '',
                'title' => 'Edit', 
                'field_data' => $field_data,
                'selected_id' => $this->api_web_image['company_welcome_id'],
                'table_name' => 'sma_web_image',
                'table_id' => 'id',
                'upload_path' => 'assets/uploads/web/'.$this->api_web[0]['id'],
                'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
            );
            $temp_admin = $this->api_admin->front_end_edit($config_data);
            $temp_display .= $temp_admin['display'];
            $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
            $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];            
    $temp_display .= '
            
            '.$temp_image['image_table'].'
            <div class="visible-sm visible-xs api_height_30"></div>
            </div>
        </div>
        <div class="col-md-8  col-sm-12 col-xs-12">
            <div class="api_height_15 visible-xs api_clear_both"></div> 
    ';
    $temp_admin = $this->api_helper->api_lang_v2('about_us_v2');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];


    $temp_display .= '
            <div class="api_height_30"></div>
            <table width="100%" border="0">
            <tr>
            <td align="center">
                <table width="300" border="0" class="api_temp_table_1">
                <tr>
                <td valign="middle" width="100px">
                    <div class="api_big">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('CEO');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
                    </div>
                </td>
                <td width="15">&nbsp;</td>
                <td valign="middle">
                    <div class="api_big">                    
    ';
    $temp_admin = $this->api_helper->api_lang_v2('SHIOI HARUKA');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
                    </div>
                </td>
                </tr>
                <tr>
                <td valign="middle">
                    <div class="api_big">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Director');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
                    </div>
                </td>
                <td width="15">&nbsp;</td>
                <td valign="middle">
                    <div class="api_big">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('HAN MAKARA');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
                    </div>
                </td>
                </tr>
                </table>          
            </td>
            </tr>
            </table>       
        </div>
    ';
    $temp_display .= '<div class="api_height_50 api_clear_both"></div>';   
    $temp_display .= '<div class="api_height_50 api_clear_both hidden-xs"></div>';
    $temp_display .= '
        <div class="col-md-12">
            <div class="api_text_align_center">
                <div class="api_large api_title_1">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('VISION_2');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
                </div>    
                <div class="api_height_10"></div>
                <div class="api_big api_temp_title_2">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('creating_a_society_where_you_can_work_in_your_own_way_v2');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
                </div>
            </div>
            <div class="api_height_15"></div> 

            <div class="api_text_align_center api_temp_title_5">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('vision');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
            </div>    
        </div>
    ';

    $temp_display .= '<div class="api_height_30 api_clear_both"></div>';
    $temp_display .= '<div class="api_height_30 api_clear_both hidden-xs"></div>';
    $temp_display .= '
        <div class="col-md-12">
            <div class="api_text_align_center">
                <div class="api_large api_title_1">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('MISSION_v2');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    
    $temp_display .= '
                </div>    
                <div class="api_height_10"></div>
                <div class="api_big api_temp_title_2 ">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Unlock the potential of people and walk together');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
                </div>  
            </div>
            <div class="api_height_15"></div> 

            <div class="api_text_align_center api_temp_title_5">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('mission');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
            </div>    
        </div>
    ';

    $temp_display .= '<div class="api_height_30 api_clear_both"></div>';
    $temp_display .= '<div class="api_height_30 api_clear_both hidden-xs"></div>';
    $temp_display .= '
        <div class="col-md-12">
            <div class="api_text_align_center">
                <div class="api_large api_title_1">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('COMPANY_NAME');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
                </div>    
                <div class="api_height_10"></div>
                <div class="api_big api_temp_title_2 ">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('SMILE×Walk Together');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
                </div>
            </div>            
            <div class="api_height_15"></div> 

            <div class="api_text_align_center api_temp_title_5">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('company_name_description');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
            </div>    
        </div>
    ';

    $temp_display .= '<div class="api_height_30 api_clear_both"></div>';
    $temp_display .= '<div class="api_height_30 api_clear_both hidden-xs"></div>';
    $temp_display .= '
        <div class="col-md-12">
            <div class="api_text_align_center">
                <div class="api_large api_title_1">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('OUR OFFICE');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
                </div>    
                <div class="api_height_10"></div>
                <div class="api_big api_temp_title_2 api_temp_title_v2 ">
    ';

    $temp_admin = $this->api_helper->api_lang_v2('Office introduction');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
                </div>  
            </div>
            <div class="api_height_15"></div> 

            <div class="api_text_align_center api_temp_title_5">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('our_office_description');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
            </div>    
        </div>
    ';

    $temp_display .= '<div class="api_height_50 api_clear_both hidden-sm hidden-xs"></div>';

    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['company_image_1'],
        'max_width' => 0,
        'max_height' => 0,
        'product_link' => '',
        'image_class' => 'api_link_box_none api_img_web_medium',
        'image_id' => '',   
        'resize_type' => 'fixed',
    );
    //$temp_image_1 = $this->api_helper->api_get_image($data_view);
    $temp_image_1['image_table'] = '
        <img class="api_image_400_250" src="'.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['company_image_1'].'" />
    ';

    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['company_image_2'],
        'max_width' => 0,
        'max_height' => 0,
        'product_link' => '',
        'image_class' => 'api_link_box_none api_img_web_medium',
        'image_id' => '',   
        'resize_type' => 'fixed',
        'table_height' => '',
    );
    //$temp_image_2 = $this->api_helper->api_get_image($data_view);
    $temp_image_2['image_table'] = '
        <img class="api_image_400_250" src="'.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['company_image_2'].'" />
    ';

    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['company_image_3'],
        'max_width' => 0,
        'max_height' => 0,
        'product_link' => '',
        'image_class' => 'api_link_box_none api_img_web_medium',
        'image_id' => '',   
        'resize_type' => 'fixed',
        'table_height' => '',
    );
    //$temp_image_3 = $this->api_helper->api_get_image($data_view);
    $temp_image_3['image_table'] = '
        <img class="api_image_400_250" src="'.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['company_image_3'].'" />
    ';


    $temp_display .= '
        <div class="col-md-4 col-sm-4 col-xs-12 api_padding_top_30 api_temp_2">
            <div class="api_admin api_admin_wrapper_web_image_company_image_1">
                '.$temp_image_1['image_table'].'
    ';
    $config_data = array(
        'wrapper' => 'api_admin_wrapper_web_image_company_image_1',
        'name' => 'company_image_1',
        'recommended_width' => 600,
        'recommended_height' => 300,
    );
    $temp_admin = $this->api_helper->api_admin_web_image($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
            </div>
            '.$temp_image_1_mobile['image_table'].'
            <div class="api_big api_text_align_center api_text_transform_capitalize api_padding_10">
    ';
        $temp_admin = $this->api_helper->api_lang_v2('Counseling booth');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
            </div>
            <div class="api_text_align_center">                
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Counseling booth text');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12 api_padding_top_30  api_temp_2">        
            <div class="api_admin api_admin_wrapper_web_image_company_image_2">
                '.$temp_image_2['image_table'].'
    ';
    $config_data = array(
        'wrapper' => 'api_admin_wrapper_web_image_company_image_2',
        'name' => 'company_image_2',
        'recommended_width' => 600,
        'recommended_height' => 300,
    );
    $temp_admin = $this->api_helper->api_admin_web_image($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
            </div>
            '.$temp_image_2_mobile['image_table'].'
            <div class="api_big api_text_align_center api_text_transform_capitalize api_padding_10">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Training room and meeting room');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
            </div>
            <div class="api_text_align_center">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Training room and meeting room text');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];    
    $temp_display .= '
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12 api_padding_top_30 api_temp_2">
            <div class="api_admin api_admin_wrapper_web_image_company_image_3">
                '.$temp_image_3['image_table'].'
    ';
    $config_data = array(
        'wrapper' => 'api_admin_wrapper_web_image_company_image_3',
        'name' => 'company_image_3',
        'recommended_width' => 600,
        'recommended_height' => 300,
    );
    $temp_admin = $this->api_helper->api_admin_web_image($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
            </div>
            '.$temp_image_3_mobile['image_table'].'
            <div class="api_big api_text_align_center api_text_transform_capitalize api_padding_10">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Nap space');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];    

    $temp_display .= '
            </div>            
            <div class="api_text_align_center">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Nap space text');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_modal'] .= $temp_admin['modal'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];      

    $temp_display .= '
            </div>            
        </div>
    ';


    $temp_display .= '<div class="api_height_50 api_clear_both"></div>';

    $temp_display .= '
        <div class="col-md-6 api_temp_col_1 api_padding_right_0">
            <div id="api_contact_1" class="api_temp_1">           
                <table width="100%" border="0">
                <tr>
                <td valign="middle" class="api_big api_temp_3 api_temp_5">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Company Info');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];      
    $temp_display .= '
                </td>
                <td valign="top" width="10" class="api_temp_3 api_padding_right_10"></td>
                <td valign="top" class="api_temp_3">                    
    ';
    $temp_admin = $this->api_helper->api_lang_v2('company_info_text');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];      

    $temp_display .= '
                </td>
                </tr>
                <tr>
                <td valign="top" class="api_temp_3 api_temp_4">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('company name');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];      
    $temp_display .= '
                </td>
                <td valign="top" width="10" class="api_temp_3 api_padding_right_10"></td>
                <td valign="top" class="api_temp_3">
                    <div class="api_admin api_admin_wrapper_web_translate">
                        '.$this->api_web[0]['title_'.$l].'
    ';
    $field_data = array();
    $field_data[0] = array(
        'type' => 'translate',
        'col_class' => 'col-md-12',
        'field_class' => '',
        'field_name' => 'translate',
        'translate' => 'yes',
        'field_label_name' => 'Web Title',                      
        'field_value' => $this->api_web[0]['title_'.$l],
    );
    $config_data = array(
        'l' => $l,
        'wrapper_class' => 'api_admin_wrapper_web_translate',
        'modal_class' => '',
        'title' => 'Edit', 
        'field_data' => $field_data,
        'selected_id' => $this->api_web[0]['id'],
        'table_name' => 'sma_web',
        'table_id' => 'id',
        'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
    );
    $temp_admin = $this->api_admin->front_end_edit($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
                    </div>
                </td>
                </tr>
                <tr>
                <td valign="top" class="api_temp_3 api_temp_4">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('location');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];      
    $temp_display .= '
                </td>
                <td valign="top" width="10" class="api_temp_3 api_padding_right_10"></td>
                <td valign="top" class="api_temp_3">
                    <div class="api_admin api_admin_wrapper_web_address">
                        '.$this->api_web[0]['address'].'
    ';
    $field_data = array();
    $field_data[0] = array(
        'type' => 'textarea',
        'col_class' => 'col-md-12',
        'field_class' => '',
        'field_name' => 'address',
        'translate' => '',
        'field_label_name' => 'Address',                      
        'field_value' => $this->api_web[0]['address'],
    );
    $config_data = array(
        'l' => $l,
        'wrapper_class' => 'api_admin_wrapper_web_address',
        'modal_class' => '',
        'title' => 'Edit', 
        'field_data' => $field_data,
        'selected_id' => $this->api_web[0]['id'],
        'table_name' => 'sma_web',
        'table_id' => 'id',
        'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
    );
    $temp_admin = $this->api_admin->front_end_edit($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    
    $temp_display .= '
                    </div>
                </td>
                </tr>           
                <tr>
                <td valign="top" class="api_temp_3 api_temp_4">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Establishment date');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];      
    $temp_display .= '
                </td>
                <td valign="top" width="10" class="api_temp_3 api_padding_right_10"></td>
                <td valign="top" class="api_temp_3">
    ';    
    $temp_admin = $this->api_helper->api_lang_v2('2018 July');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];      
    $temp_display .= '
                </td>
                </tr>      
                <tr>
                <td valign="top" class="api_temp_3 api_temp_4">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Representative Director');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];      
    $temp_display .= '
                </td>
                <td valign="top" width="10" class="api_temp_3 api_padding_right_10"></td>
                <td valign="top" class="api_temp_3">
    ';                    
    $temp_admin = $this->api_helper->api_lang_v2('Haruka_Saii_Ito');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];      
    $temp_display .= '              
                </td>
                </tr>     
                <tr>
                <td valign="top" class="api_temp_3 api_temp_4">
    ';    
    $temp_admin = $this->api_helper->api_lang_v2('Director');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];      
    $temp_display .= '
                </td>                
                <td valign="top" width="10" class="api_temp_3 api_padding_right_10"></td>
                <td valign="top" class="api_temp_3">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('HAN MAKARA');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];      
    $temp_display .= '
                </td>
                </tr> 
                <tr>
                <td valign="top" class="api_temp_3 api_temp_4">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Telephone');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];      
    $temp_display .= '
                </td>
                <td valign="top" width="10" class="api_temp_3 api_padding_right_10"></td>
                <td valign="top" class="api_temp_3">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Telephone_num');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];      
    $temp_display .= '
                </td>
                </tr>                                   
                <tr>
                <td valign="top" class="api_temp_3 api_temp_4">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('email');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];      
    $temp_display .= '
                </td>
                <td valign="top" width="10" class="api_temp_3 api_padding_right_10"></td>
                <td valign="top" class="api_temp_3">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('email_contact_us');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];      
    $temp_display .= '
                </td>
                </tr>                                   
                </table>                     
            </div>
        </div>
        <div class="col-md-6 api_temp_col_1 api_temp_col_2 api_padding_left_0">
            <div class="api_admin api_admin_wrapper_web_google_map">
                <iframe id="api_contact_map" src="'.$this->api_web[0]['map_google'].'" width="100%" height="500" style="border:0;" allowfullscreen="" loading="lazy"></iframe>       
    ';        
    $field_data = array();
    $field_data[0] = array(
        'type' => 'text',
        'col_class' => 'col-md-12',
        'field_class' => '',
        'field_name' => 'map_google',
        'translate' => '',
        'field_label_name' => 'Google Map Src',
    );
    $config_data = array(
        'l' => $l,
        'wrapper_class' => 'api_admin_wrapper_web_google_map',
        'modal_class' => '',
        'title' => 'Edit', 
        'field_data' => $field_data,
        'selected_id' => $this->api_web[0]['id'],
        'table_name' => 'sma_web',
        'table_id' => 'id',
        'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
    );
    $temp_admin = $this->api_admin->front_end_edit($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
            </div>
        </div>
    ';

    $temp_display .= '<div class="api_height_30 api_clear_both hidden-xs"></div>';


    $temp_display .= '
        <table class="hidden-xs" width="100%" border="0">
        <tr>
        <td valign="middle" align="right">
            <div class="api_admin_block api_wrapper_translation_Click_here_for_employer">
                <a href="'.base_url().'For-Companies">
                    <div class="api_button_1">
                        '.$this->api_helper->clear_all_tags($this->api_helper->api_lang('Click here for employer')).'
                        &nbsp; <li class="fa fa-arrow-right"></li>
                    </div>       
                </a>     
    ';
$temp = $this->site->api_select_some_fields_with_where("
    id
    "
    ,"sma_translation"
    ,"keyword = 'Click here for employer'"
    ,"arr"
);
$field_data = array();
$field_data[0] = array(
    'type' => 'translate',
    'col_class' => 'col-md-12',
    'field_class' => '',
    'field_name' => 'translate',
    'translate' => 'yes',
    'field_label_name' => 'Title',                      
);
$config_data = array(
    'l' => $l,
    'wrapper_class' => 'api_wrapper_translation_Click_here_for_employer',
    'modal_class' => '',
    'title' => 'Edit', 
    'field_data' => $field_data,
    'selected_id' => $temp[0]['id'],
    'table_name' => 'sma_translation',
    'table_id' => 'id',
    'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
    'generate_translate' => 'yes',
);
$temp_admin = $this->api_admin->front_end_edit($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
            </div>
        </td>
        <td valign="middle" width="50">
            &nbsp;
        </td>
        <td valign="middle" align="left">
            <div class="api_admin_block api_wrapper_translation_Click_here_for_job_seekers">
                <a href="'.base_url().'For-Jobseeker">
                    <div class="api_button_1">
                        '.$this->api_helper->clear_all_tags($this->api_helper->api_lang('Click here for job seekers')).'
                        &nbsp; <li class="fa fa-arrow-right"></li>
                    </div>       
                </a>      
    ';
$temp = $this->site->api_select_some_fields_with_where("
    id
    "
    ,"sma_translation"
    ,"keyword = 'Click here for job seekers'"
    ,"arr"
);
$field_data = array();
$field_data[0] = array(
    'type' => 'translate',
    'col_class' => 'col-md-12',
    'field_class' => '',
    'field_name' => 'translate',
    'translate' => 'yes',
    'field_label_name' => 'Title',                      
);
$config_data = array(
    'l' => $l,
    'wrapper_class' => 'api_wrapper_translation_Click_here_for_job_seekers',
    'modal_class' => '',
    'title' => 'Edit', 
    'field_data' => $field_data,
    'selected_id' => $temp[0]['id'],
    'table_name' => 'sma_translation',
    'table_id' => 'id',
    'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
);
$temp_admin = $this->api_admin->front_end_edit($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
            </div>
        </td>
        </tr>
        </table>    

        <table class="visible-xs" width="100%" border="0">
        <tr>
        <td valign="middle" align="center">
            <div class="api_height_20"></div>
            <a href="'.base_url().'For-Companies">
                <div class="api_button_1">
                    '.$this->api_helper->clear_all_tags(lang('Click here for employer')).'
                    &nbsp; <li class="fa fa-arrow-right"></li>
                </div>       
            </a>     
        </td>
        </tr>
        <tr>
        <td valign="middle" align="center">
            <div class="api_height_20"></div>
            <a href="'.base_url().'For-Jobseeker">
                <div class="api_button_1">
                    '.$this->api_helper->clear_all_tags(lang('Click here for job seekers')).'
                    &nbsp; <li class="fa fa-arrow-right"></li>
                </div>       
            </a>                 
        </td>
        </tr>
        </table>
    ';

    $temp_display .= '<div class="api_height_30 api_clear_both"></div>';


    $config_data = array(
        'wrapper_class' => 'api_page_ayum_company',
        'custom_html' => '<div class="api_height_30 api_clear_both visible-xs"></div>',
        'display' => $temp_display,
        'type' => '',
    );
    $temp = $this->api_display->template_display($config_data);
    echo $temp['display'];

    echo '<div class="api_height_10 api_clear_both"></div>';

echo '    
<script  src="'.base_url().'/assets/api/page/'.$temp_page_name.'/js/script.js?v='.$this->api_shop_setting[0]['version'].'"></script>
';
?>

