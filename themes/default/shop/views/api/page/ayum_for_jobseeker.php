<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php 
    $l = $this->api_shop_setting[0]['api_lang_key'];
    $temp_page_name = 'ayum_for_jobseeker';
    echo '
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style_600.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style_768.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style_1024.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
    ';

//-Banner-    
    $temp_display = '
        <div class="api_admin api_admin_wrapper_web_image_jobseeker_bg ">
            <table class="api_temp_table_1" border="0">
            <tr>
            <td valign="bottom" class="api_temp_td_1">
                <a href="javascript:void(0);" onclick="api_ayum_for_jobseeker_contact_click();">
                    <div class="api_button_1">  
                        <table  border="0">
                        <tbody>
                        <tr>
                            <td valign="middle" class="api_temp_td_5">
                                '.$this->api_helper->api_lang('contact_us_v2').'
                            </td>
                            <td valign="middle" width="30">
                                &nbsp;<li class="fa fa-arrow-right"></li>
                            </td>
                        </tr>
                        </tbody>
                        </table>
                    </div>       
                </a>
            </td>
            </tr>
            </table>
    ';
    $field_data = array();
    $field_data[0] = array(
        'type' => 'file',
        'col_class' => 'col-md-6',
        'field_class' => '',
        'field_name' => 'image',    
        'field_label_name' => 'Image PC',
        'recommended_width' => 2000,
        'recommended_height' => 500,
    );
    $field_data[1] = array(
        'type' => 'file',
        'col_class' => 'col-md-6',
        'field_class' => '',
        'field_name' => 'image_2',    
        'field_label_name' => 'Image Mobile',
        'recommended_width' => 700,
        'recommended_height' => 450,
    );    
    $config_data = array(
        'l' => $l,
        'wrapper_class' => 'api_admin_wrapper_web_image_jobseeker_bg',
        'modal_class' => '',
        'title' => 'Edit', 
        'field_data' => $field_data,
        'selected_id' => $this->api_web_image['jobseeker_bg_id'],
        'table_name' => 'sma_web_image',
        'table_id' => 'id',
        'upload_path' => 'assets/uploads/web/'.$this->api_web[0]['id'],
        'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
    );
    $temp_admin = $this->api_admin->front_end_edit($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
        </div>
    ';

    $config_data = array(
        'wrapper_class' => 'api_temp_banner',
        'custom_html' => '',
        'display' => $temp_display,
        'type' => 'full',
    );
    $temp = $this->api_display->template_display($config_data);
    echo '<div class="api_height_30 api_clear_both visible-xs"></div>';
    echo $temp['display'];
echo '
<style>
.api_temp_banner .api_section_full {
    background-image:url('.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/'.$this->api_web_image['jobseeker_bg'].');
}
@media screen and (min-width: 100px) and (max-width:767px) {
.api_temp_banner .api_section_full {
    background-image:url('.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/'.$this->api_web_image['jobseeker_bg_image_2'].');
}    
}
</style>
';

    $temp_display = '';
//-Banner-    
// //-Welcome-    
$temp_display = '';
$include_data = array(
    'image_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['jobseeker_banner'],
    'image_name' => 'jobseeker_banner',
    'image_max_width' => 700,
    'image_max_height' => 400,
    'title_key' => 'We will support your career as a career change partner',
    'description_key' => 'ayum_for_jobseeker_1',
);
include 'themes/default/shop/views/api/sub_page/api_ayum_welcome_table.php';

$temp_display = '';
//-Welcome-
//-Feature-
    $temp_display .= '
        <div class="api_height_30 api_clear_both visible-sm visible-xs"></div>
        <div class="col-md-12 api_kh_padding">            
            <div class="api_text_align_center">
                <div class="api_large api_title_1 api_template_title_1">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('FEATURE');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
                </div>    
                <div class="api_height_10"></div>
                <div class="api_template_title_2">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Ayum employment support');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
                </div>   
            </div>          
        </div>    
    ';
    $temp_display .= '<div class="api_clear_both"></div>';
//-Feature-   

//-blog-step---
    $config_data = array(
        'table_name' => 'sma_blog_step',
        'select_table' => 'sma_blog_step',
        'translate' => 'yes',
        'translate_2' => 'yes',
        'description' => 'yes',
        'select_condition' => "c_id = 108 order by ordering asc",
    );
    $temp = $this->api_helper->api_select_data_v2($config_data);      
    $config_data = array(
        'l' => $l,
        'select_data' => $temp,
    );
    $temp_initial_data = $this->api_initial_data->blog_step($config_data);
    $config_data = array(
        'l' => $l,
        'select_data' => $temp_initial_data,
        'image_path' => base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/blog_step/thumb',
        'seperator' => '<div class="api_height_50 api_clear_both"></div>',        
        'category_id' => 108,        
    );
    $temp_view_as = $this->api_view_as->blog_step($config_data);    
    $temp_display .= $temp_view_as['display'];
//-blog-step---

    $temp_display .= '<div class="api_height_50 api_clear_both hidden-xs"></div>';

//-Service-    
    $temp_display .= '
        <div class="api_height_30 api_clear_both visible-xs"></div>
        <div class="col-md-12 api_padding_kh">            
            <div class="api_text_align_center">
                <div class="api_large api_title_1 api_template_title_1">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('SERVICE FLOW');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
                </div>    
                <div class="api_height_10"></div>
                <div class="api_template_title_2">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Flow from registration interview to follow-up after joining the company');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
                </div>   
            </div>          
        </div>   
        <div class="api_height_20 api_clear_both hidden-xs"></div> 
    ';
//-Service- 


//-service-flow-
    $config_data = array(
        'table_name' => 'sma_blog_step',
        'select_table' => 'sma_blog_step',
        'translate' => 'yes',
        'translate_2' => 'yes',
        'description' => 'yes',
        'select_condition' => "c_id = 123 order by ordering asc",
    );
    $temp = $this->api_helper->api_select_data_v2($config_data);      
    $config_data = array(
        'l' => $l,
        'select_data' => $temp,
    );
    $temp_initial_data = $this->api_initial_data->blog($config_data);
    $config_data = array(
        'l' => $l,
        'select_data' => $temp_initial_data,
        'image_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/blog_step/thumb',
        'image_class' => 'api_link_box_none api_img_blog_medium_v3',
        'seperator' => '
            <table width="100%" border="0">
            <tr>
            <td class="api_large" valign="middle" align="center">
                <div  width="100%" class="api_temp_arrow_down">
                <img src="'.base_url().'assets/api/page/ayum_for_jobseeker/image/image_10.PNG" />
                </div>
            </tr>
            </table>
        ',        
        'category_id' => 123,
    );
    $temp_view_as = $this->api_view_as->blog_step_v2($config_data);    
    $temp_display .= $temp_view_as['display'];

//-service-flow-


$temp_display .= '<div class="api_height_50 api_clear_both hidden-sm hidden-xs"></div>';
$temp_display .= '<div class="api_height_50 api_clear_both hidden-xs"></div>';

//-api_temp_wrapper_1- 
    $temp_display .= '
        <div class="col-md-6">
    ';
        $config_data_function = array(
            'title_1_key' => 'Have been using HR Service',
            'title_2_key' => 'From Company which',
            'title_3_key' => 'Voice',
            'title_4_key' => 'have been used HR Service',
        );
        include 'themes/default/shop/views/api/sub_page/api_customer_voice_header.php';
        $temp_display .= $return['display'];

        $config_data_function = array(
            'wrapper_class' => 'col_border_title_description_year',
            'col_class' => 'col-md-6',
            'title_key' => 'career_change',
            'description_key' => 'ayum_for_jobseeker_6',
            'year_text_key' => 'work_at_japanese',
        );
        $temp = $this->api_display->col_border_title_description_year($config_data_function);
        $temp_display .= $temp['display'];


    $temp_display .= '
        </div>   
    ';

    $temp_display .= '
        <div class="col-md-6">
    ';

            $config_data_function = array(
                'wrapper_class' => 'col_border_title_description_year',
                'col_class' => 'col-md-6',
                'title_key' => 'wide',
                'description_key' => 'ayum_for_jobseeker_7',
                'year_text_key' => 'work_at_japanese_30',
            );
            $temp = $this->api_display->col_border_title_description_year($config_data_function);
            $temp_display .= $temp['display'];

            $config_data_function = array(
                'wrapper_class' => 'col_border_title_description_year',
                'col_class' => 'col-md-6',
                'title_key' => 'compatibility',
                'description_key' => 'ayum_for_jobseeker_8',
                'year_text_key' => 'work_at_japanese_v2',
            );
            $temp = $this->api_display->col_border_title_description_year($config_data_function);
            $temp_display .= $temp['display'];
            
    $temp_display .= '
        </div> 
    ';
//-api_temp_wrapper_1-   
$temp_display .= '<div class="api_height_30 api_clear_both"></div>';  
$temp_display .= '
    <div class="col-md-12 text-center">
        <div class="api_admin_block api_admin_wrapper_translation_contact_us_v22">
            <a href="javascript:void(0);" onclick="api_ayum_for_jobseeker_contact_click();">
                <div class="api_button_2">
                    <table width="100%" border="0">
                        <tbody>
                        <tr>
                            <td valign="middle" class="api_temp_td_5">
                                '.$this->api_helper->api_lang('contact_us_v2').'
                            </td>
                            <td valign="middle" width="30">
                                &nbsp;<li class="fa fa-arrow-right"></li>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>       
            </a>
';

$temp = $this->site->api_select_some_fields_with_where("
    id
    "
    ,"sma_translation"
    ,"keyword = 'contact_us_v2'"
    ,"arr"
);
$field_data = array();
$field_data[0] = array(
    'type' => 'translate_textarea',
    'col_class' => 'col-md-12',
    'field_class' => '',
    'field_name' => 'translate',
    'translate' => 'yes',
    'field_label_name' => 'Title',                      
);
$config_data = array(
    'l' => $l,
    'wrapper_class' => 'api_admin_wrapper_translation_contact_us_v22',
    'modal_class' => '',
    'title' => 'Edit', 
    'field_data' => $field_data,
    'selected_id' => $temp[0]['id'],
    'table_name' => 'sma_translation',
    'table_id' => 'id',
    'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
);
$temp_admin = $this->api_admin->front_end_edit($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];


$temp_display .= '
        </div>
        <div class="api_clear_both"></div>
    </div>
';
    $temp_display .= '<div class="api_height_50 api_clear_both"></div>';

    $config_data = array(
        'wrapper_class' => 'api_page_ayum_for_jobseeker',
        'custom_html' => '',
        'display' => $temp_display,
        'display_class' => '',
        'panel' => '',
        'panel_class' => '',
        'type' => '',
    );
    $temp = $this->api_display->template_display($config_data);
    echo $temp['display'];


    echo '<div class="api_height_10 api_clear_both"></div>';


echo '    
    <script  src="'.base_url().'/assets/api/page/'.$temp_page_name.'/js/script.js?v='.$this->api_shop_setting[0]['version'].'"></script>
';    
?>
