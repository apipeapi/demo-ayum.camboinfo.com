<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php 
    $l = $this->api_shop_setting[0]['api_lang_key'];
    $temp_page_name = 'ayum_for_companies';
    echo '
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style_1024.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style_768.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style_600.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
    ';


    if ($l == 'ja')
        include 'themes/default/shop/views/api/sub_page/api_'.$temp_page_name.'_ja.php';
    else
        include 'themes/default/shop/views/api/sub_page/api_'.$temp_page_name.'_en.php';
    
    $temp_display = '';
    $temp_display .= '<div class="api_height_30 api_clear_both"></div>';

    $config_data = array(
        'wrapper_class' => 'api_page_'.$temp_page_name.'',
        'custom_html' => '',
        'display' => $temp_display,
        'display_class' => '',
        'panel' => '',
        'panel_class' => '',
        'type' => '',
    );
    $temp = $this->api_display->template_display($config_data);
    echo $temp['display'];


    echo '<div class="api_height_10 api_clear_both"></div>';

echo '    
    <script  src="'.base_url().'/assets/api/page/'.$temp_page_name.'/js/script.js?v='.$this->api_shop_setting[0]['version'].'"></script>
    <script  src="'.base_url().'/assets/api/page/'.$temp_page_name.'/js/script_'.$l.'.js?v='.$this->api_shop_setting[0]['version'].'"></script>
';
?>

