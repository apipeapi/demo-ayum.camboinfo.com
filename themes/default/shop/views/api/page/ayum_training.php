<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php 
    $l = $this->api_shop_setting[0]['api_lang_key'];
    $temp_page_name = 'ayum_training';
    echo '
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style_600.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style_768.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style_1024.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
    ';

//-banner-    
    $temp_display = '
        <div class="api_temp_banner">
            <table width="100%" border="0">
            <tr>
            <td valign="middle" align="center" class="api_temp_td_1">
                <div class="api_temp_div_1">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('would_you_like_to_leave_t');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
                    <div class="api_height_10"></div>
                    <div class="api_admin_block api_admin_wrapper_translation_contact_us_v2 api_float_right">
                        <a href="javascript:void(0);" onclick="api_ayum_for_companies_contact_click();">
                            <div class="api_button_1 api_link_box_none">
                                '.$this->api_helper->clear_all_tags($this->api_helper->api_lang('contact_us_v2')).'
                                &nbsp; <li class="fa fa-arrow-right"></li>
                            </div> 
                        </a>
    ';
    $temp = $this->site->api_select_some_fields_with_where("
        id
        "
        ,"sma_translation"
        ,"keyword = 'contact_us_v2'"
        ,"arr"
    );
    $field_data = array();
    $field_data[0] = array(
        'type' => 'translate_textarea',
        'col_class' => 'col-md-12',
        'field_class' => '',
        'field_name' => 'translate',
        'translate' => 'yes',
        'field_label_name' => 'Title',                      
    );
    $config_data = array(
        'l' => $l,
        'wrapper_class' => 'api_admin_wrapper_translation_contact_us_v2',
        'modal_class' => '',
        'title' => 'Edit', 
        'field_data' => $field_data,
        'selected_id' => $temp[0]['id'],
        'table_name' => 'sma_translation',
        'table_id' => 'id',
        'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
    );
    $temp_admin = $this->api_admin->front_end_edit($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
                    </div>
    ';
    $temp = $this->site->api_select_some_fields_with_where("
        id
        "
        ,"sma_translation"
        ,"keyword = 'contact_us_v2'"
        ,"arr"
    );
    $field_data = array();
    $field_data[0] = array(
        'type' => 'translate_textarea',
        'col_class' => 'col-md-12',
        'field_class' => '',
        'field_name' => 'translate',
        'translate' => 'yes',
        'field_label_name' => 'Title',                      
    );
    $config_data = array(
        'l' => $l,
        'wrapper_class' => 'api_admin_wrapper_translation_contact_us_v2',
        'modal_class' => '',
        'title' => 'Edit', 
        'field_data' => $field_data,
        'selected_id' => $temp[0]['id'],
        'table_name' => 'sma_translation',
        'table_id' => 'id',
        'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
    );
    $temp_admin = $this->api_admin->front_end_edit($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
                    </div>
                </div>
            </td>
            </tr>
            </table>            
        </div> 
    ';
    $config_data = array(
        'wrapper_class' => '',
        'custom_html' => '',
        'display' => $temp_display,
        'type' => 'full',
    );
    $temp = $this->api_display->template_display($config_data);
    echo '<div class="api_height_30 api_clear_both visible-xs"></div>';
    echo $temp['display'];
    $temp_display = '';
//-banner-

$temp_display .= '
    <div class="api_height_30 api_clear_both"></div>
    <div class="col-md-12">            
        <div class="api_text_align_center">
            <div class="api_title_1 api_template_title_1">
';
$temp_admin = $this->api_helper->api_lang_v2('TRAINING ORGANIZATIONAL IMPROVEMENT');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
            </div>    
            <div class="api_height_10"></div>
            <div class="api_text_transform_capitalize api_template_title_2">
';
$temp_admin = $this->api_helper->api_lang_v2('Training/organization improvement support');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '                
            </div>    
        </div>
    </div>    
';

//-welcome--------------
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['training_image_1'],
        'max_width' => 600,
        'max_height' => 400,
        'product_link' => '',
        'image_class' => 'img-responsive',
        'image_id' => '',   
        'resize_type' => 'fixed',       
    );
    $temp_image = $this->api_helper->api_get_image($data_view);

    $temp_display .= '
        <div class="col-md-12">
            <div class="api_big api_temp_2 text-center">
    '; 
    $temp_admin = $this->api_helper->api_lang_v2('training');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '           
            </div>
        </div>
        <div class="api_height_15 api_clear_both hidden-xs"></div>
        <div class="col-md-12 hidden-xs">
            <div class="api_temp_border_1">
                <table width="100%" border="0">
                <tr>
                <td class="api_padding_15" width="45%" valign="top">
                    <div class="api_admin api_admin_wrapper_web_image_training_image_1">
                        '.$temp_image['image_table'].'
    '; 
    $config_data = array(
        'wrapper' => 'api_admin_wrapper_web_image_training_image_1',
        'name' => 'training_image_1',
        'recommended_width' => 600,
        'recommended_height' => 400,
    );
    $temp_admin = $this->api_helper->api_admin_web_image($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '                    
                    </div>
                </td>
                <td class="api_padding_15" width="55%" valign="middle">
                    <div class="api_temp_3a" id="api_ayum_for_companies_api_temp_div_2">
                        <div class="api_big api_title_2">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('training_title_1');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];                        
    $temp_display .= '                                      
                        </div>        
                        <div class="api_height_10"></div>    
                        <div class="">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('training_description_1');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script']; 
    $temp_display .= ' 
                        </div>                        
                    </div>            
                </td>
                </tr>
                <tr>
                <td colspan="2" valign="middle" align="center">
                    <div class="api_height_30 api_clear_both"></div>
                    <a href="javascript:void(0);" onclick="$(\'#feather_training_scroll\').scrollView();">
                        <div class="api_big api_temp_bottom_1">
                            MORE
                            <div>
                                <li class="fa fa-arrow-down"></li>
                            </div>
                        </div>
                    </a>
                </tr>
                </table>      
            </div>  
        </div>

        <div class="col-md-12 visible-xs">
            <div class="api_temp_border_1">
                <table width="100%" border="0">
                <tr>
                <td class="api_padding_15" width="45%" valign="top">
                    '.$temp_image['image_table'].'
                </td>
                </tr>
                <tr>
                <td class="api_padding_15" width="55%" valign="middle">
                    <div class="api_temp_3a" id="api_ayum_for_companies_api_temp_div_2">
                        <div class="api_big api_title_2">
                            '.$this->api_helper->api_lang('training_title_1').'                 
                        </div>        
                        <div class="api_height_10"></div>    
                        <div class="">
                            '.$this->api_helper->api_lang('training_description_1').'
                        </div>                        
                    </div>            
                </td>
                </tr>
                <tr>
                <td valign="middle" align="center">
                    <div class="api_height_30 api_clear_both"></div>
                    <a href="javascript:void(0);" onclick="$(\'#feather_training_scroll\').scrollView();">
                        <div class="api_big api_temp_bottom_1">
                            MORE
                            <div>
                                <li class="fa fa-arrow-down"></li>
                            </div>
                        </div>
                    </a>
                </tr>
                </table>      
            </div>    
        </div>
    
    ';
//-welcome--------------

//-2222--------------
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['training_image_2'],
        'max_width' => 0,
        'max_height' => 0,
        'product_link' => '',
        'image_class' => 'api_img_600_350',
        'image_id' => '',   
        'resize_type' => 'fixed',       
    );
    $temp_image = $this->api_helper->api_get_image($data_view);

    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['training_image_3'],
        'max_width' => 0,
        'max_height' => 0,
        'product_link' => '',
        'image_class' => 'api_img_600_350',
        'image_id' => '',   
        'resize_type' => 'fixed',       
    );
    $temp_image_2 = $this->api_helper->api_get_image($data_view);

    $temp_display .= '
    <div class="col-md-12 api_temp_wrapper_1">
        <div class="col-md-6 col-sm-6 api_kh_padding_v2">
            <div class="api_big api_temp_2">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Labor Management Support');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
            </div>
            <div class="api_temp_4 api_admin api_admin_wrapper_web_image_training_image_3">
                '.$temp_image_2['image_table'].'
    ';
    $config_data = array(
        'wrapper' => 'api_admin_wrapper_web_image_training_image_3',
        'name' => 'training_image_3',
        'recommended_width' => 700,
        'recommended_height' => 400,
    );
    $temp_admin = $this->api_helper->api_admin_web_image($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='            
            </div>
            <div class="api_height_20"></div>

            <div class="api_temp_3" id="api_ayum_training_api_temp_div_1">
                <div class="api_big text-center api_kh_font_size_20">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Do_you_have_these_problems?');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='   
                </div>          
                <div class="api_height_20"></div>   
                <div class="">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('training_description_2');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '                
                </div>                       
            </div>
        </div>
        <div class="col-md-6 col-sm-6">
            <div class="api_big api_temp_2">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Improvement_Of_Operations_Support');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
            </div>
            <div class="api_temp_4 api_admin api_admin_wrapper_web_image_training_image_2">
                '.$temp_image['image_table'].'
    ';
    $config_data = array(
        'wrapper' => 'api_admin_wrapper_web_image_training_image_2',
        'name' => 'training_image_2',
        'recommended_width' => 700,
        'recommended_height' => 400,
    );
    $temp_admin = $this->api_helper->api_admin_web_image($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
            </div>
            <div class="api_height_20"></div>

            <div class="api_temp_3" id="api_ayum_training_api_temp_div_2">
                <div class="api_big text-center api_kh_font_size_20">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Do_you_have_these_problems?');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
                </div>          
                <div class="api_height_20"></div>   
                <div class="">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('training_description_3');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
                </div>                  
            </div>
        </div>
    </div>
    ';
//-2222--------------

//-3333--------------
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['training_image_4'],
        'max_width' => 0,
        'max_height' => 0,
        'product_link' => '',
        'image_class' => 'api_img_600_350',
        'image_id' => '',   
        'resize_type' => 'fixed',       
    );
    $temp_image = $this->api_helper->api_get_image($data_view);

    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['training_image_5'],
        'max_width' => 0,
        'max_height' => 0,
        'product_link' => '',
        'image_class' => 'api_img_600_350',
        'image_id' => '',   
        'resize_type' => 'fixed',       
    );
    $temp_image_2 = $this->api_helper->api_get_image($data_view);

    $temp_display .= '
    <div class=" api_clear_both"></div>
    <div class="col-md-12 api_temp_wrapper_1">
        <div class="col-md-6 col-sm-6 api_kh_padding_v2">
            <div class="api_big api_temp_2">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Business Survey');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
            </div>
            <div class="api_temp_4 api_admin api_admin_wrapper_web_image_training_image_4">
                '.$temp_image['image_table'].'
                '.$temp_image_mobile['image_table'].'
    ';
    $config_data = array(
        'wrapper' => 'api_admin_wrapper_web_image_training_image_4',
        'name' => 'training_image_4',
        'recommended_width' => 700,
        'recommended_height' => 400,
    );
    $temp_admin = $this->api_helper->api_admin_web_image($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
            </div>
            <div class="api_height_20"></div>

            <div class="api_temp_3" id="api_ayum_training_api_temp_div_3">
                <div class="api_big text-center api_kh_font_size_20">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Do_you_have_these_problems?');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
                </div>          
                <div class="api_height_20"></div>   
                <div class="">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('training_description_4');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
                </div>                       
            </div>
        </div>
        <div class="col-md-6 col-sm-6">
            <div class="api_big api_temp_2">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Concept_Penetration_Workshop');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
            </div>
            <div class="api_temp_4 api_admin api_admin_wrapper_web_image_training_image_5">
                '.$temp_image_2['image_table'].'
                '.$temp_image_mobile_2['image_table'].'
    ';
    $config_data = array(
        'wrapper' => 'api_admin_wrapper_web_image_training_image_5',
        'name' => 'training_image_5',
        'recommended_width' => 700,
        'recommended_height' => 400,
    );
    $temp_admin = $this->api_helper->api_admin_web_image($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
            </div>
            <div class="api_height_20"></div>

            <div class="api_temp_3" id="api_ayum_training_api_temp_div_4">
                <div class="api_big text-center api_kh_font_size_20">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Do_you_have_these_problems?');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
                </div>          
                <div class="api_height_20"></div>   
                <div class="">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('training_description_5');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
                </div>                  
            </div>
        </div>
    </div>
    ';
//-3333--------------


//-Feature-
    $temp_display .= '
        <div class="api_height_30 api_clear_both "></div>
        <div class="col-md-12 api_kh_padding">            
            <div class="api_text_align_center">
                <div class="api_large api_title_1 api_template_title_1" id="feather_training_scroll">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('FEATURE');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
                </div>    
                <div class="api_height_10"></div>
                <div class="api_text_transform_capitalize api_template_title_2">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Ayum training');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
                </div>   
            </div>          
        </div>    
    ';
    $temp_display .= '<div class="api_height_20 api_clear_both"></div>';
//-Feature-   

//-blog-step---
    $config_data = array(
        'table_name' => 'sma_blog_step',
        'select_table' => 'sma_blog_step',
        'translate' => 'yes',
        'translate_2' => 'yes',
        'description' => 'yes',
        'select_condition' => "c_id = 124 order by ordering asc",
    );
    $temp = $this->api_helper->api_select_data_v2($config_data);     
    $config_data = array(
        'l' => $l,
        'select_data' => $temp,
    );
    $temp_initial_data = $this->api_initial_data->blog_step($config_data);
    $config_data = array(
        'l' => $l,
        'select_data' => $temp_initial_data,
        'image_path' => base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/blog_step/thumb',
        'seperator' => '<div class="api_height_50 api_clear_both"></div>',        
        'category_id' => 124,
    );
    $temp_view_as = $this->api_view_as->blog_step($config_data);    
    $temp_display .= $temp_view_as['display'];
//-blog-step---

//-content-
$temp_display .= '
<div class="api_height_20 api_clear_both "></div>
<div class="api_height_30 api_clear_both"></div>
<div class="col-md-12 api_kh_padding">            
    <div class="api_text_align_center">
        <div class="api_large api_title_1 api_template_title_1" >
';
$temp_admin = $this->api_helper->api_lang_v2('CONTENT');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
        </div> 
        <div class="api_padding_5 api_text_transform_capitalize api_template_title_2">
';
$temp_admin = $this->api_helper->api_lang_v2('training_area');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .=' 
        </div>  
    </div>          
</div>    
';


$temp_display .= '
    <div class="api_height_20 api_clear_both hidden-xs"></div>
';
//-content-  

//-2222--------------
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['training_image_6'],
        'max_width' => 300,
        'max_height' => 200,
        'product_link' => '',
        'image_class' => 'api_img_300_200',
        'image_id' => '',   
        'resize_type' => 'fixed',       
    );
    $temp_image_3 = $this->api_helper->api_get_image($data_view);
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['training_image_7'],
        'max_width' => 300,
        'max_height' => 200,
        'product_link' => '',
        'image_class' => 'api_img_300_200',
        'image_id' => '',   
        'resize_type' => 'fixed',       
    );
    $temp_image_4 = $this->api_helper->api_get_image($data_view);

    $temp_display .= '
    <div class="col-md-12 api_temp_wrapper_1">
        <div class="col-md-6 col-sm-6 api_temp_col_1">

            <table width="100%" border="0">
            <tr>
            <td valign="middle" width="30%" class="api_temp_td_1 api_admin api_admin_wrapper_web_image_training_image_6">
                '.$temp_image_3['image_table'].'
    ';
    $config_data = array(
        'wrapper' => 'api_admin_wrapper_web_image_training_image_6',
        'name' => 'training_image_6',
        'recommended_width' => 300,
        'recommended_height' => 200,
    );
    $temp_admin = $this->api_helper->api_admin_web_image($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
            </td>
            <td valign="middle" class="api_big api_temp_td_2">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Improvement of working skills of Newcomers and young people');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
            </td>
            </tr>
            </table>

            <div class="api_height_10"></div>

            <div class="api_temp_3" id="api_ayum_training_api_temp_div_5">
                <div class="api_big  api_kh_font_size_18">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Do_you_have_this_problems?');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
                </div>        
                <div class="api_height_10"></div>    
    ';
    $temp_admin = $this->api_helper->api_lang_v2('training_content_description_1');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
            </div>
        </div>

        <div class="col-md-6 col-sm-6 api_temp_col_1">

            <table width="100%" border="0">
            <tr>
            <td valign="middle" width="30%" class="api_temp_td_1 api_admin api_admin_wrapper_web_image_training_image_7">
                '.$temp_image_4['image_table'].'
    ';
    $config_data = array(
        'wrapper' => 'api_admin_wrapper_web_image_training_image_7',
        'name' => 'training_image_7',
        'recommended_width' => 300,
        'recommended_height' => 200,
    );
    $temp_admin = $this->api_helper->api_admin_web_image($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
            </td>
            <td valign="middle" class="api_big api_temp_td_2">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Business skill Strengthen the management skill');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
            </td>
            </tr>
            </table>

            <div class="api_height_10"></div>

            <div class="api_temp_3" id="api_ayum_training_api_temp_div_6">
                <div class="api_big  api_kh_font_size_18">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Do_you_have_this_problems?');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
                </div>        
                <div class="api_height_10"></div>    
    ';
    $temp_admin = $this->api_helper->api_lang_v2('training_content_description_2');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
                 </div>
        </div>
    </div>
    ';    
//-2222--------------

$temp_display .= '<div class="api_clear_both"></div>';

//-333--------------
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['training_image_8'],
        'max_width' => 300,
        'max_height' => 200,
        'product_link' => '',
        'image_class' => 'api_img_300_200',
        'image_id' => '',   
        'resize_type' => 'fixed',       
    );
    $temp_image_3 = $this->api_helper->api_get_image($data_view);
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['training_image_9'],
        'max_width' => 300,
        'max_height' => 200,
        'product_link' => '',
        'image_class' => 'api_img_300_200',
        'image_id' => '',   
        'resize_type' => 'fixed',       
    );
    $temp_image_4 = $this->api_helper->api_get_image($data_view);


    $temp_display .= '
    <div class="col-md-12 api_temp_wrapper_1">
        <div class="col-md-6 col-sm-6 api_temp_col_1">

            <table width="100%" border="0">
            <tr>
            <td valign="middle" width="30%" class="api_temp_td_1 api_admin api_admin_wrapper_web_image_training_image_8">
                '.$temp_image_3['image_table'].'
    ';
    $config_data = array(
        'wrapper' => 'api_admin_wrapper_web_image_training_image_8',
        'name' => 'training_image_8',
        'recommended_width' => 300,
        'recommended_height' => 200,
    );
    $temp_admin = $this->api_helper->api_admin_web_image($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .=' 
            </td>
            <td valign="middle" class="api_big api_temp_td_2">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Whole Company’s improvement of organizational strength');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='            
            </td>
            </tr>
            </table>

            <div class="api_height_10"></div>

            <div class="api_temp_3" id="api_ayum_training_api_temp_div_7">
                <div class="api_big  api_kh_font_size_18" >
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Do_you_have_this_problems?');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
                </div>        
                <div class="api_height_10"></div>    
    ';
    $temp_admin = $this->api_helper->api_lang_v2('training_content_description_3');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
            </div>
        </div>

        <div class="col-md-6 col-sm-6 api_temp_col_1">

            <table width="100%" border="0">
            <tr>
            <td valign="middle" width="30%" class="api_temp_td_1 api_admin api_admin_wrapper_web_image_training_image_9">
                '.$temp_image_4['image_table'].'
    ';
    $config_data = array(
        'wrapper' => 'api_admin_wrapper_web_image_training_image_9',
        'name' => 'training_image_9',
        'recommended_width' => 300,
        'recommended_height' => 200,
    );
    $temp_admin = $this->api_helper->api_admin_web_image($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .=' 
            </td>
            <td valign="middle" class="api_big api_temp_td_2">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Japanese style training');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
            </td>
            </tr>
            </table>

            <div class="api_height_10"></div>

            <div class="api_temp_3" id="api_ayum_training_api_temp_div_8">
                <div class="api_big  api_kh_font_size_18">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Do_you_have_this_problems?');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
                </div>        
                <div class="api_height_10"></div>    
    ';
    $temp_admin = $this->api_helper->api_lang_v2('training_content_description_4');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .='
            </div>

        </div>
    </div>
    ';    
//-3333--------------



$temp_display .= '<div class="api_height_50 api_clear_both"></div>';
$temp_display .= '<div class="api_height_50 api_clear_both hidden-sm hidden-xs"></div>';

//voice
$temp_display .= '
        <div class="col-md-6">
    ';
        $config_data_function = array(
            'title_1_key' => 'Have been using Training Service',
            'title_2_key' => 'From Company which',
            'title_3_key' => 'Voice',
            'title_4_key' => 'have been used Training Service',
        );
        include 'themes/default/shop/views/api/sub_page/api_customer_voice_header.php';
        $temp_display .= $return['display'];        

        $config_data_function = array(
            'wrapper_class' => 'col_border_title_description_year',
            'col_class' => 'col-md-6',
            'title_key' => 'I_realized_that_v2',
            'title_2_key' => 'Teamwork Training lecture',
            'description_key' => 'ayum_training_1',
            'year_text_key' => 'japanese company 1',
        );
        $temp = $this->api_display->col_border_title_description_year($config_data_function);
        $temp_display .= $temp['display'];
    
    $temp_display .= '
        </div>   
    ';

    $temp_display .= '
        <div class="col-md-6">
    ';

            $config_data_function = array(
                'wrapper_class' => 'col_border_title_description_year',
                'col_class' => 'col-md-6',
                'title_key' => 'Ayum_Has_you_carry_out_v2',
                'title_2_key' => 'Customized_Training_Lecture_v2',
                'description_key' => 'ayum_training_2',
                'year_text_key' => 'japanese company 2',
            );
            $temp = $this->api_display->col_border_title_description_year($config_data_function);
            $temp_display .= $temp['display'];

            $config_data_function = array(
                'wrapper_class' => 'col_border_title_description_year',
                'col_class' => 'col-md-6',
                'title_key' => 'I_realized tha_ Attitude_v2',
                'title_2_key' => 'Semi-annual_training',
                'description_key' => 'ayum_training_3',
                'year_text_key' => 'japanese company 3',
            );
            $temp = $this->api_display->col_border_title_description_year($config_data_function);
            $temp_display .= $temp['display'];
            
    $temp_display .= '
        </div> 
    ';
//end_voice
$temp_display .= '<div class="col-md-12 text-center">
            <a href="javascript:void(0);" onclick="api_ayum_for_companies_contact_click();">
                <div class="api_button_1">
                <table width="100%" border="0">
                    <tbody>
                    <tr>
                        <td valign="middle" class="api_temp_td_5">
                            '.$this->api_helper->clear_all_tags(lang('contact_us_v2')).'
                        </td>
                        <td valign="middle" width="30">
                            &nbsp;<li class="fa fa-arrow-right"></li>
                        </td>
                    </tr>
                    </tbody>
                </table>
                   
                </div>       
            </a>     
        </div>
';
$temp_display .= '<div class="api_height_30 api_clear_both"></div>';



    $temp_display .= '<div class="api_height_30 api_clear_both"></div>';

    $config_data = array(
        'wrapper_class' => 'api_page_'.$temp_page_name.'',
        'custom_html' => '',
        'display' => $temp_display,
        'display_class' => '',
        'panel' => '',
        'panel_class' => '',
        'type' => '',
    );
    $temp = $this->api_display->template_display($config_data);
    echo $temp['display'];


    echo '<div class="api_height_10 api_clear_both"></div>';

    
echo '    
    <script  src="'.base_url().'/assets/api/page/'.$temp_page_name.'/js/script.js?v='.$this->api_shop_setting[0]['version'].'"></script>
';



?>



