<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php 
    $l = $this->api_shop_setting[0]['api_lang_key'];
    $temp_page_name = 'ayum_hr';
    echo '
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style.css?v='.$this->api_shop_setting[0]['api_lang_key'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style_600.css?v='.$this->api_shop_setting[0]['api_lang_key'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style_768.css?v='.$this->api_shop_setting[0]['api_lang_key'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style_1024.css?v='.$this->api_shop_setting[0]['api_lang_key'].'" rel="stylesheet">
';

    if($l == 'en') {
        $margin_mobile_banner = '90px';
    } else {
        $margin_mobile_banner = '90px';
    }
        
    // HR banner
    $temp_display = '   
            <div class="api_page_hr_banner hidden-xs">
                <div class="hidden-sm hidden-xs">
                    <div class="api_height_100"></div>
                    <table width="100%">
                        <tr>
                            <td width="10%" class="api_height_30"></td>
                        </tr>
                        <tr>
                            <td width="10%"></td>
                            <td width="12%"></td>
                            <td class="api_text_header_banner">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('are_you_suffering_about');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '            
                            </td>
                        </tr>
                        <tr>
                            <td width="10%" class="api_height_30"></td>
                        </tr>
                        <tr>
                            <td width="10%"></td>
                            <td width="12%"></td>
                            <td class="api_text_banner_padding">
                            <a href="javascript:void(0);" onclick="api_ayum_for_companies_contact_click();">
                            <div class="api_button_2">  
                                <table  border="0">
                                <tbody>
                                <tr>
                                    <td valign="middle" class="api_temp_td_5">
                                    '.$this->api_helper->api_lang('contact_us_v2').'
                                    </td>
                                    <td valign="middle" width="30">
                                        &nbsp;<li class="fa fa-arrow-right"></li>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                            </div>       
                            </div>       
                            </a>         
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="visible-sm">
                    <div class="" style="height:50px"></div>
                    <table width="100%">
                        <tr>
                            <td width="10%" class="api_height_10"></td>
                        </tr>
                        <tr>
                            <td width="10%"></td>
                            <td width="12%"></td>
                            <td class="api_text_header_banner">
                                '.$this->api_helper->api_lang('are_you_suffering_about').'
                            </td>
                        </tr>
                        <tr>
                            <td width="10%" class="api_height_20"></td>
                        </tr>
                        <tr>
                            <td width="10%"></td>
                            <td width="12%"></td>
                            <td valign="bottom" class="api_temp_td_1">
                            <div class="">
                                <a href="javascript:void(0);" onclick="api_ayum_for_companies_contact_click();">
                                <div class="api_button_1">  
                                <table  border="0">
                                <tbody>
                                    <tr>
                                        <td valign="middle" class="api_temp_td_5">
                                        '.$this->api_helper->api_lang('contact_us_v2').'
                                        </td>
                                        <td valign="middle" width="30">
                                            &nbsp;<li class="fa fa-arrow-right"></li>
                                        </td>
                                    </tr>  
                                </tbody>  
                                </table>
                            </div>    
                        </td>   
                        </tr>
                    </table>
                </div>
            </div>    
            <div class="api_height_30 visible-xs"></div>
            <div class="api_page_hr_banner visible-xs">
                <div class="api_banner_height_30 visible-xs"></div>
                <table width="100%">
                    <tr>                   
                        <td valign="middle" align="center">
                            <div class="api_big">
                                '.$this->api_helper->api_lang('are_you_suffering_about').'
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" class="api_temp_td_1">
                            <div class="">
                                <a href="javascript:void(0);" onclick="api_ayum_for_companies_contact_click();">
                                <div class="api_button_1">  
                                <table  border="0">
                                <tbody>
                                    <tr>
                                        <td valign="middle" class="api_temp_td_5">
                                        '.$this->api_helper->api_lang('contact_us_v2').'
                                        </td>
                                        <td valign="middle" width="30">
                                            &nbsp;<li class="fa fa-arrow-right"></li>
                                        </td>
                                    </tr>  
                                </tbody>  
                                </table>
                            </div>    
                        </td>       
                    </tr>
                </tbody>
                </table>
            </div>       
                </a> 
            </div>               
        </td>
                    </tr>
                </table>
            </div>
        
    ';
    
    
    $config_data = array(
        'wrapper_class' => 'api_page_ayum_for_jobseeker',
        'custom_html' => '',
        'display' => $temp_display,
        'display_class' => '',
        'panel' => '',
        'panel_class' => '',
        'type' => 'full',
    );
    $temp = $this->api_display->template_display($config_data);
    echo $temp['display'];
    $temp_display = '';
    echo '<div class="api_height_30 api_clear_both hidden-xs"></div>';

//-api_temp_section_1---    
    $temp_display .= '
        <div class="col-md-12">            
            <div class="api_text_align_center">
                <div class="api_large api_title_1">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('hr_consulting_v2');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
                </div>    
                <div class="api_height_10"></div>
                <div class="api_big  api_text_transform_capitalize">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('HR_consulting_recruitment support_v3');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
                </div>  
                <div class="api_height_30"></div>    
            </div>
        </div>    
    ';

    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['hr_image_1'],
        'max_width' => 0,
        'max_height' => 0,
        'product_link' => '',
        'image_class' => 'api_img_500_300',
        'image_id' => '',   
        'resize_type' => 'fixed',       
    );
    $temp_image = $this->api_helper->api_get_image($data_view);

    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['hr_image_2'],
        'max_width' => 0,
        'max_height' => 0,
        'product_link' => '',
        'image_class' => 'api_img_500_300',
        'image_id' => '',   
        'resize_type' => 'fixed',       
    );
    $temp_image_2 = $this->api_helper->api_get_image($data_view);

    $temp_display .= '
        <div class="col-md-12 api_padding_0 api_temp_section_1">
            <div class="col-md-6 col-sm-6 api_temp_col_1">
                <div class="api_big api_temp_2">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Introduce to Human Resource');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
                </div>
                <div class="api_temp_4 api_admin api_admin_wrapper_web_image_hr_image_1">        
                    '.$temp_image['image_table'].'
    ';
    $config_data = array(
        'wrapper' => 'api_admin_wrapper_web_image_hr_image_1',
        'name' => 'hr_image_1',
        'recommended_width' => 700,
        'recommended_height' => 400,
    );
    $temp_admin = $this->api_helper->api_admin_web_image($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
                </div>
                <div class="api_height_20"></div>

                <div class="api_temp_3" id="api_ayum_hr_api_temp_3a">
                    <div class="api_big">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Service overview');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '                    
                    </div>          
                    <div class="api_height_10"></div>   
                    <div class="">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('our_experienced_v4');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '  
                    </div>        
                </div>
                <div class="api_height_10"></div> 
                <div class="api_text_align_center">
                    <a  href="#api_temp_recruit_service">  
                        <div class="temp_hover api_big">
                            <div class="api_temp_4">
                                '.$this->api_helper->api_lang('More_v2').'
                            </div>
                            <div class="api_temp_5">
                                <li class="fa fa-arrow-down"></li>
                            </div>
                        </div> 
                    </a>                                  
                </div>
            </div>

            <div class="col-md-6 col-sm-6 api_temp_col_1">
                <div class="api_big api_temp_2">

';
$temp_admin = $this->api_helper->api_lang_v2('Recruitment Process Outsourcing');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '                     
                </div>
                <div class="api_temp_4 api_admin api_admin_wrapper_web_image_hr_image_2">                    
                    '.$temp_image_2['image_table'].'
';
$config_data = array(
    'wrapper' => 'api_admin_wrapper_web_image_hr_image_2',
    'name' => 'hr_image_2',
    'recommended_width' => 700,
    'recommended_height' => 400,
);
$temp_admin = $this->api_helper->api_admin_web_image($config_data);
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= '
                </div>
                <div class="api_height_20"></div>

                <div class="api_temp_3" id="api_ayum_hr_api_temp_3b">
                    <div class="api_big">
';
$temp_admin = $this->api_helper->api_lang_v2('Service overview');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= ' 
                    </div>          
                    <div class="api_height_10"></div>   
                    <div class="">
';
$temp_admin = $this->api_helper->api_lang_v2('We_aim to_eliminate_v4');
$temp_display .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
$temp_display .= ' 
                    </div>        
                </div>
                <div class="api_height_10"></div>  
                <div class="api_text_align_center">
                    <a  href="#api_temp_rpo_service">
                        <div class="temp_hover api_big">
                            <div class="api_temp_4">
                                '.$this->api_helper->api_lang('More_v2').'
                            </div>
                            <div class="api_temp_5">
                                <li class="fa fa-arrow-down"></li>
                            </div>
                        </div> 
                    </a> 
                </div>                                  
            </div>
        </div>
    ';    
//-api_temp_section_1---

//-api_temp_section_2---
    $temp_display .= '
        <div class="api_height_30 api_clear_both hidden-sm hidden-xs"></div>
        <div class="col-md-12 api_temp_section_2">            
            <div class="api_text_align_center">
                <div class="api_big api_temp_title_4">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Recruitment_v4');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
                </div>    
                <div class="api_height_30 hidden-xs hidden-sm"></div>    
                <div class="api_height_15 visible-xs"></div>  
            </div>
        </div>    
    ';

    $temp_display .= '
        <div class="col-md-12 api_padding_0 api_temp_section_2 hidden-sm hidden-xs">
            <table class="api_temp_table_1" border="0" style="margin:auto">
            <tr>
            <td valign="top" class="api_temp_td_1 api_admin api_admin_wrapper_web_image_ayum_hr_image_1" style="background-image:url('.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['ayum_hr_image_1'].');">
    ';
    $config_data = array(
        'wrapper' => 'api_admin_wrapper_web_image_ayum_hr_image_1',
        'name' => 'ayum_hr_image_1',
        'recommended_width' => 700,
        'recommended_height' => 450,
    );
    $temp_admin = $this->api_helper->api_admin_web_image($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
            </td>
                        
            <td valign="top" >
                <div class="api_height_20 visible-xs"></div>

                <div class="api_temp_3">
                    <div class="api_big">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Service overview');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
                    </div>        
                    <div class="api_height_10"></div>    
                    <div class="">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('We’re supporting Recruitment Public Relations');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
                    </div>      
                    <div class="api_height_30"></div>
                    <div class="api_big">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('“Have You Ever Suffered Abou This?”');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    
    $temp_display .= '
                    </div>      
                    <div class="api_height_10"></div>   
                    <div class="api_temp_4">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('ayum_hr_1');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
                    </div>                       
                </div>

            </td>
            </tr>
            </table>        
        </div>
    ';

    $temp_display .= '
        <div class="col-md-12 api_padding_0 api_temp_section_2 visible-sm visible-xs">
            <div class="col-md-12">
                <img class="img-responsive" src="'.base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/thumb/'.$this->api_web_image['ayum_hr_image_1'].'" >
            </div>
            <div class="col-md-12">
                <div class="api_temp_3">
                    <div class="api_big">
                        '.$this->api_helper->api_lang('Service overview').'
                    </div>        
                    <div class="api_height_10"></div>    
                    <div class="">
                        '.$this->api_helper->api_lang('We’re supporting Recruitment Public Relations').' 
                    </div>      
                    <div class="api_height_30"></div>
                    <div class="api_big">
                        '.$this->api_helper->api_lang('“Have You Ever Suffered Abou This?”').'
                    </div>      
                    <div class="api_height_10"></div>   
                    <div class="api_temp_4">
                        '.$this->api_helper->api_lang('ayum_hr_1').'
                    </div>                       
                </div>
            </div>
        </div>
    ';

//-api_temp_section_2---

    $temp_display .= '<div class="api_height_60 api_clear_both" id="api_temp_recruit_service"></div>';

    $temp_display .= '
        <div class="col-md-12">            
            <div class="api_text_align_center">
                <div class="api_large api_title_1 api_template_title_1">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('RECRUIT SERVICE');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
                </div>    
                <div class="api_height_10"></div>
                <div class="api_text_transform_capitalize api_template_title_2">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('Ayum HR Introduction');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
                </div>   
            </div>          
        </div>       
    ';

    $temp_display .= '<div class="api_height_30 api_clear_both"></div>';


//-blog-step1---
    $config_data = array(
        'table_name' => 'sma_blog_step',
        'select_table' => 'sma_blog_step',
        'translate' => 'yes',
        'translate_2' => 'yes',
        'description' => 'yes',
        'select_condition' => "c_id = 109 order by ordering asc",
    );
    $temp = $this->api_helper->api_select_data_v2($config_data);      
    $config_data = array(
        'l' => $l,
        'select_data' => $temp,
    );
    $temp_initial_data = $this->api_initial_data->blog_step($config_data);
    $config_data = array(
        'l' => $l,
        'select_data' => $temp_initial_data,
        'image_path' => base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/blog_step/thumb',
        'seperator' => '<div class="api_height_50 api_clear_both"></div>',
        'category_id' => 109,  
    );
    $temp_view_as = $this->api_view_as->blog_step($config_data);    
    $temp_display .= $temp_view_as['display'];
    
//-blog-step1---

    $temp_display .= '<div class="api_height_70 api_clear_both"></div>';


    $temp_display .= '<div class="api_height_30 api_clear_both hidden-sm hidden-xs"></div>';

//-SERVICE FLOW 1---
    $temp_display .= '
        <div class="col-md-12">            
            <div class="api_text_align_center">
                <div class="api_large api_title_1">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('SERVICE FLOW');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
                </div>    
                <div class="api_height_10"></div>
                <div class=" api_text_transform_capitalize" style="line-height: 1.5;">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('service_flow_document');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
                </div>   
            </div>          
        </div>    
    ';
    $temp_display .= '<div class="api_height_30 api_clear_both"></div>';


$temp_display .= '
    <div class="col-md-12">
        <div class="api_temp_section_wrapper_1">
';
    $config_data = array(
        'title_1' => 'STEP1',
        'title_2' => 'Preparation before selection',
        'title_3' => 'Carefully match the requirements and the personality',
        'icon_1' => '<img class="" src="'.base_url().'assets/api/page/ayum_hr/image/icon_1.png" />',
        'icon_1_title' => 'Recruitment Plan Requirements Hiring',
        'icon_2' => '<img class="" src="'.base_url().'assets/api/page/ayum_hr/image/icon_2.png" />',
        'icon_2_title' => '​Share in Ayum Inhouse study meeting',
        'icon_3' => '',
        'icon_3_title' =>'',
    );
    if ($this->api_helper->is_mobile() != 1)
        $temp = $this->api_display->api_temp_function_1_v2($config_data);
    else
        $temp = $this->api_display->api_temp_function_1($config_data);
    $temp_display .= $temp['display'];    
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp['script'];

    $temp_display .= '<div class="api_height_20"></div>';
    $config_data = array(
        'title_1' => 'STEP2',
        'title_2' => 'Selection Candidates',
        'title_3' => 'Introducing job seekers after selection',
        'icon_1' => '<img class="" src="'.base_url().'assets/api/page/ayum_hr/image/icon_3.png" />',
        'icon_1_title' => 'Selection in Ayum',
        'icon_2' => '<img class="" src="'.base_url().'assets/api/page/ayum_hr/image/icon_4.png" />',
        'icon_2_title' => 'Introduction_(Sending Document)_Ayum→Company',
        'icon_3' => '',
        'icon_3_title' =>'',
    );
    if ($this->api_helper->is_mobile() != 1)
        $temp = $this->api_display->api_temp_function_1_v2($config_data);
    else
        $temp = $this->api_display->api_temp_function_1($config_data);    
    $temp_display .= $temp['display']; 
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp['script'];
    $temp_display .= '<div class="api_height_20"></div>';

    $config_data = array(
        'title_1' => 'STEP3',
        'title_2' => 'Selection',
        'title_3' => 'Document screening and interview at Company',
        'icon_1' => '<img class="" src="'.base_url().'assets/api/page/ayum_hr/image/icon_5.png" />',
        'icon_1_title' => 'Document Screening (Company)',
        'icon_2' => '<img class="" src="'.base_url().'assets/api/page/ayum_hr/image/icon_6.png" />',
        'icon_2_title' => 'Adjust Interview date (Ayum)',
        'icon_3' => '<img class="" src="'.base_url().'assets/api/page/ayum_hr/image/icon_7.png" />',
        'icon_3_title' => 'Interview (Company)',
    );
    if ($this->api_helper->is_mobile() != 1)
        $temp = $this->api_display->api_temp_function_1_v2($config_data);
    else
        $temp = $this->api_display->api_temp_function_1($config_data);    
    $temp_display .= $temp['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp['script'];

    $temp_display .= '<div class="api_height_20"></div>';
    $config_data = array(
        'title_1' => 'STEP4',
        'title_2' => 'Job Offer',
        'title_3' => 'Ayum will tell  job seekers the results',
        'icon_1' => '<img class="" src="'.base_url().'assets/api/page/ayum_hr/image/icon_8.png" />',
        'icon_1_title' => 'Acceptance informin (Ayum)',
        'icon_2' => '<img class="" src="'.base_url().'assets/api/page/ayum_hr/image/icon_9.png" />',
        'icon_2_title' => 'Condition adjustment Confirmation of intention (Ayum)',
        'icon_3' => '<img class="" src="'.base_url().'assets/api/page/ayum_hr/image/icon_10.png" />',
        'icon_3_title' => 'Acceptance of offer',
    );
    if ($this->api_helper->is_mobile() != 1)
        $temp = $this->api_display->api_temp_function_1_v2($config_data);
    else
        $temp = $this->api_display->api_temp_function_1($config_data);
    
    $temp_display .= $temp['display']; 
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp['script'];

    $temp_display .= '<div class="api_height_20"></div>';

    $config_data = array(
        'title_1' => 'STEP5',
        'title_2' => 'Joining a company',
        'title_3' => 'Ayum will take responsibility and support until entry the Company',
        'icon_1' => '<img class="" src="'.base_url().'assets/api/page/ayum_hr/image/icon_11.png" />',
        'icon_1_title' => 'Joining a company Candidate→Company',
        'icon_2' => '<img class="" src="'.base_url().'assets/api/page/ayum_hr/image/icon_12.png" />',
        'icon_2_title' => 'Inviove Ayum→Company',
        'icon_3' => '',
        'icon_3_title' =>'',
    );
    if ($this->api_helper->is_mobile() != 1)
        $temp = $this->api_display->api_temp_function_1_v2($config_data);
    else
        $temp = $this->api_display->api_temp_function_1($config_data);
    
    $temp_display .= $temp['display']; 
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp['script'];
    $temp_display .= '<div class="api_height_20"></div>';

    $config_data = array(
        'title_1' => 'STEP6',
        'title_2' => 'Follow-up after entry the company',
        'title_3' => 'Follow-up from Ayum for 3 months after joining the company To make a long time employment',
        'icon_1' => '<img class="" src="'.base_url().'assets/api/page/ayum_hr/image/icon_8.png" />',
        'icon_1_title' => 'Follow-call',
        'icon_2' => '<img class="" src="'.base_url().'assets/api/page/ayum_hr/image/icon_13.png" />',
        'icon_2_title' => 'Free Training (2times)',
        'icon_3' => '<img class="" src="'.base_url().'assets/api/page/ayum_hr/image/icon_14.png" />',
        'icon_3_title' => 'Feedback to company',
    ); 
    if ($this->api_helper->is_mobile() != 1)
        $temp = $this->api_display->api_temp_function_1_v2($config_data);
    else
        $temp = $this->api_display->api_temp_function_1($config_data);
    
    $temp_display .= $temp['display']; 
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp['script'];

$temp_display .= '
        </div>
    </div>
';

$temp_display .= '
    <div class="api_height_30 api_clear_both" id="api_temp_rpo_service"></div>
';

  
//-SERVICE FLOW 1---


//-RPO SERVICE---
    $temp_display .= '
                <div class="col-md-12 hidden-xs">            
                    <div class="api_text_align_center">
                        <div class="api_large api_title_1">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('RPO SERVICE');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
                        </div>    
                        <div class="api_height_10"></div>
                        <div class=" api_text_transform_capitalize" style="line-height: 1.5;">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('rpo_service_document');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display .= '
                        </div>   
                    </div>          
                </div>    
    ';
    $temp_display .= '<div class="api_height_30 api_clear_both"></div>';
    
    // ----pc
        if($l == 'ja') {
            $class_rpo_service = 'api_height_30';
        } else {
            $class_rpo_service_2 = 'api_height_30';
            $class_rpo_service_3 = 'api_height_30';
        }
        $temp_display .= '
            <div class="col-md-12 api_temp_section_4 hidden-xs">  
                <div class="api_padding_15 api_rpo_service">
                    <span class="api_rpo_service_font_size">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('​If you have this kind of problem, please contact to Ayum recruitment agency! (RPO＝Recruitment Process Outsourcing)');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                        </span><br>
                        <div class="api_height_30 api_clear_both"></div>
                        <table with="100%" style="margin: auto;">
                        <tr>
                            <td valign="center" class="api_rpo_service_border">
                                <div style="padding-top:10px">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Where is the candidate?');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                </div>
                                <div class="api_height_30"></div>
                                <div class="api_font_size_25" style="font-weight:bold">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Small number of applying');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                </div>
                            </td>
                            <td width="6%"></td>
                            <td valign="center" class="api_rpo_service_border">
                                <div style="padding-top:10px">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Lack of information & Experiences');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                </div>
                                <div class="'.$class_rpo_service.'"></div>
                                <div class="api_font_size_25" style="font-weight:bold;padding: 0px 10px 0px 10px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Lack of Recruitment Know-How');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="api_height_30 api_clear_both hidden-xs"></div>
                    <table widht="100%" style="margin:auto">
                        <tr>
                            <td valign="center" class="api_rpo_service_border">
                                <div style="">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('A lot of detailed adjustment');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                </div>
                                <div class="api_height_30"></div>
                                <div class="api_font_size_25" style="font-weight:bold">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Lack of staff');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                </div>
                                <div class="'.$class_rpo_service_2.'"></div>
                            </td>
                            <td width="4%"></td>
                            <td valign="center" class="api_rpo_service_border">
                                <div style="">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Difficulty of recruitment in company');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                </div>
                                <div class="'.$class_rpo_service.'"></div>
                                <div class="api_font_size_25" style="font-weight:bold;padding: 0px 10px 0px 10px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Recruitment Cost UP');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                </div>
                            </td>
                            <td width="4%"></td>
                            <td valign="center" class="api_rpo_service_border">
                                <div style="">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('What is bad?');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                </div>
                                <div class="'.$class_rpo_service.$class_rpo_service_3.'"></div>
                                <div class="api_font_size_25" style="font-weight:bold;padding: 0px 10px 0px 10px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Doesn’t root');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                </div>
                                <div class="'.$class_rpo_service.$class_rpo_service_3.'"></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            ';
        $temp_display .= '<div class="api_height_20 api_clear_both  hidden-xs"></div>';
    // ----pc

//mobile
    $temp_display .= '
                <div class="col-md-12 visible-xs">            
                    <div class="api_text_align_center">
                        <div class="api_large api_title_1">
                            RPO SERVICE
                        </div>    
                        <div class="api_height_10"></div>
                        <div class=" api_text_transform_capitalize" style="line-height: 1.5;">
                            '.$this->api_helper->api_lang('rpo_service_document').'
                        </div>   
                    </div>          
                </div>    
    ';
    $temp_display .= '<div class="api_height_10 api_clear_both"></div>';
    
    // ----mobile
        if($l == 'ja') {
            $class_rpo_service = 'api_height_30';
        } else {
            $class_rpo_service_2 = 'api_height_30';
            $class_rpo_service_3 = 'api_height_30';
        }
        $temp_display .= '
            <div class="col-md-12 api_padding_0 api_temp_section_4 visible-xs">  
                <div class="api_rpo_service api_padding_0">
                    <span class="api_rpo_service_font_size">'.$this->api_helper->api_lang('​If you have this kind of problem, please contact to Ayum recruitment agency! (RPO＝Recruitment Process Outsourcing)').'</span><br>
                    <table with="100%" style="margin: auto;">
                        <tr>
                            <td valign="middle" class="api_rpo_service_border">
                                <div style="padding-top:10px">'.$this->api_helper->api_lang('Where is the candidate?').'</div>
                                <div class="api_font_size_25" style="font-weight:bold; padding-top:20px;">'.$this->api_helper->api_lang('Small number of applying').'</div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" width="50">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" class="api_rpo_service_border">
                                <div style="padding-top:10px">'.$this->api_helper->api_lang('Lack of information & Experiences').'</div>
                                <div class="'.$class_rpo_service.'"></div>
                                <div class="api_font_size_25" style="font-weight:bold;">'.$this->api_helper->api_lang('Lack of Recruitment Know-How').'</div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" width="50">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" class="api_rpo_service_border">
                            <div style="">'.$this->api_helper->api_lang('A lot of detailed adjustment').'</div>
                            <div class="api_font_size_25" style="font-weight:bold;padding-top:20px;">'.$this->api_helper->api_lang('Lack of staff').'</div>
                            <div class="'.$class_rpo_service_2.'"></div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" width="50">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td valign="center" class="api_rpo_service_border">
                                <div style="">'.$this->api_helper->api_lang('Difficulty of recruitment in company').'</div>
                                <div class="'.$class_rpo_service.'"></div>
                                <div class="api_font_size_25" style="font-weight:bold;padding-top:20px;">'.$this->api_helper->api_lang('Recruitment Cost UP').'</div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" width="50">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" class="api_rpo_service_border">
                                <div style="">'.$this->api_helper->api_lang('What is bad?').'</div>
                                <div class="'.$class_rpo_service.$class_rpo_service_3.'"></div>
                                <div class="api_font_size_25" style="font-weight:bold;padding: 0px 10px 0px 10px;">'.$this->api_helper->api_lang('Doesn’t root').'</div>
                                <div class="'.$class_rpo_service.$class_rpo_service_3.'"></div>
                            </td>
                        </tr>
                    </table>
                    <div class="api_height_30 api_clear_both hidden-xs"></div>
                </div>
            </div>
            ';
        $temp_display .= '<div class="api_height_30 api_clear_both visible-xs"></div>';
    // ----mobile

//-RPO SERVICE---


//-BLOG-step2----
    $config_data = array(
        'table_name' => 'sma_blog_step',
        'select_table' => 'sma_blog_step',
        'translate' => 'yes',
        'translate_2' => 'yes',
        'description' => 'yes',
        'select_condition' => "c_id = 125 order by ordering asc",
    );
    $temp = $this->api_helper->api_select_data_v2($config_data);      
    $config_data = array(
        'l' => $l,
        'select_data' => $temp,
    );
    $temp_initial_data = $this->api_initial_data->blog_step($config_data);
    $config_data = array(
        'l' => $l,
        'select_data' => $temp_initial_data,
        'image_path' => base_url().'assets/uploads/web/'.$this->api_web[0]['id'].'/blog_step/thumb',
        'seperator' => '<div class="api_height_30 api_clear_both"></div>',
        'category_id' => 125,
    );
    $temp_view_as = $this->api_view_as->blog_step($config_data);    
    $temp_display .= $temp_view_as['display'];

    $temp_display .= '<div class="api_height_30 api_clear_both"></div>';
//-BLOG-step2----


//-SERVICE FLOW 2----
    $temp_display .= '
                <div class="col-md-12">            
                    <div class="api_text_align_center">
                        <div class="api_large api_title_1">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('SERVICE FLOW');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display  .= '
                        </div>    
                        <div class="api_height_10"></div>
                        <div class=" api_text_transform_capitalize" style="line-height: 1.5;">
    ';
    $temp_admin = $this->api_helper->api_lang_v2('service_flow_2_document');
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    $temp_display  .= '
                        </div>   
                    </div>          
                </div>    
    ';
    $temp_display .= '<div class="api_height_30 api_clear_both"></div>';
    
    // ----pc
    if($l == 'en') {
        $pc_width = '15%';
        $pc_width_2 = '5%';
        $pc_width_colspan = '32%';
    } else  {
        $pc_width = '16%';
        $pc_width_2 = '8%';
        $pc_width_colspan = '30%';
    }

        $temp_display .= '
                    <div class="col-md-12 api_temp_section_5 hidden-sm hidden-xs">  
                        <div class="api_temp_5 api_padding_0">
                            <table width="100%" border="0" style="font-size: 13px;color: #000;">
                                <tr>
                                    <td width="'.$pc_width_2.'" align="center"></td>
                                    <td width="16%" align="center" class="service_flow_arrow_pointer_tr_1 api_hr_line_height api_service_flow_font_size_15">             
                                        <div class="api_display_table_cell api_vertical_align_middle api_padding_left_20">                       
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Recruit plan Recruit strategy');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                        </div>         
                                    </td>
                                    <td width="16%" align="center" class="service_flow_arrow_pointer_tr_2 api_hr_line_height api_service_flow_font_size_15">
                                        <div class="api_display_table_cell api_vertical_align_middle api_padding_left_20">                       
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Population Formation');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                        </div>           
                                    </td>
                                    <td width="16%" align="center" class="service_flow_arrow_pointer_tr_3 api_hr_line_height api_service_flow_font_size_15">
                                        <div class="api_display_table_cell api_vertical_align_middle api_padding_left_30">                       
                                            <span>
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Application reception Document screening');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                            </span>
                                        </div>
                                    </td>
                                    <td width="16%" align="center" class="service_flow_arrow_pointer_tr_4 api_hr_line_height api_service_flow_font_size_15">
                                        <div class="api_display_table_cell api_vertical_align_middle api_padding_left_25">                       
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Interview selection');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                        </div>              
                                    </td>
                                    <td width="16%" align="center" class="service_flow_arrow_pointer_tr_5 api_hr_line_height api_service_flow_font_size_15">               
                                        <div class="api_display_table_cell api_vertical_align_middle api_padding_left_20">                       
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Acceptance Support');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '       
                                        </div>             
                                    </td>
                                </tr>
                                <tr>
                                    <td width="5%" align="center" class="api_service_flow_font_size_15">
                                        <div style="width:100%; height:100px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle">                       
        ';
        $temp_admin = $this->api_helper->api_lang_v2('In-house Response');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '  
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="'.$pc_width.'" align="center">
                                        <div style="width:100%; height:100px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Recruitment planning');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 40px;
                                                ">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Secure recruitment budget');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div> 
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="'.$pc_width.'" align="center">
                                        <div style="width:100%; height:100px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_height_5"></div>
                                                <div class="api_height_35"></div> 
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="16%" align="center">
                                        <div style="width:100%; height:100px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Pass / Fail');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>
                                                <div class="api_height_5"></div>
                                                <div class="api_height_40"></div> 
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="'.$pc_width.'" align="center">
                                        <div style="width:100%; height:100px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Conduct an interview');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Pass / fail judgment');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="'.$pc_width.'" align="center">
                                        <div style="width:100%; height:100px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Recruitment judgment');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Confirmation of conditions');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>
                                            </div>
                                        </div>             
                                    </td>
                                </tr>
                                <tr>
                                    <td width="5%" align="center" class="api_service_flow_font_size_15">
                                        <div style="width:100%; height:185px;display:table;background-color:#f4cbcc; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle">                       
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Consulting');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="14.28%" align="center">
                                        <div style="width:100%; height:185px;display:table;background-color:#f4cbcc; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Human resources requirement definition');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Recruitment process design');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div> 
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Recruitment channel selection');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>
                                                <div class="api_height_45"></div>
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="14.28%" align="center">
                                        <div style="width:100%; height:185px;display:table;background-color:#f4cbcc; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">      
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">           
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Agent management');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 40px;
                                                ">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Advertising direction');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div> 
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Event Planing');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Direct recruitment planing');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="14.28%" align="center" colspan="3">
                                        <div style="width:100%; height:185px;display:table;background-color:#f4cbcc; padding:5px;line-height: 1.2em;">                
                                            <div class="" style="padding:5px">  
                                                <table width="100%">
                                                    <tr>
                                                        <td width="'.$pc_width_colspan.'"></td>
                                                        <td width="30%" align="center" style="padding: 0px 5px 0px 5px;">
                                                            <div class="api_display_text_middle" style="font-size:12px;border: 2px solid #eb9c9d;padding: 8px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Interviewer training');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                            </div>
                                                        </td>
                                                        <td width="30%" align="center" style="padding: 0px 0px 0px 5px;">
                                                            <div class="api_display_text_middle" style="font-size:12px;border: 2px solid #eb9c9d;padding: 8px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Closing support');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Candidate progress management');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>
                                            </div>
                                        </div>             
                                    </td>
                                </tr>
                                <tr>
                                    <td width="5%" align="center" class="api_service_flow_font_size_15">
                                        <div style="width:100%; height:145px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle">                       
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Agency');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="14.28%" align="center">
                                        <div style="width:100%; height:145px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Recruitment public relations');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>
                                                <div class="api_height_90"></div>
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="14.28%" align="center">
                                        <div style="width:100%; height:145px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">    
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Each channel cooperation');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Advertising operation');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>                    
                                                <div class="api_height_45"></div> 
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="14.28%" align="center">
                                        <div style="width:100%; height:145px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Application reception');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Document screening');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Pass/ Fail inform');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="14.28%" align="center">
                                        <div style="width:100%; height:145px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Interview schedule adjustment');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Pass/ Fail inform v2');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= ' 
                                                </div>
                                                <div class="api_height_45"></div>
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="14.28%" align="center">
                                        <div style="width:100%; height:145px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Sending job offer');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 40px;"> 
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Collection of tentative job offer documents');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 40px;">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('Follow-up');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
        $temp_display  .= '
                                                </div>
                                            </div>
                                        </div>             
                                    </td>
                                </tr> 
                            </table>
                        </div>
                    </div>
                ';
        $temp_display .= '<div class="api_height_30 api_clear_both hidden-sm hidden-xs"></div>';
    // pc----

    // ---ipad
        if($l == 'en') {
            $ipad_width = '5%';
            $mobile_width = '1%';
            $mobile_width2 = '30%';
            $mobile_padding = 'padding: 0px 10px 0px 0px;';
            $mobile_padding2 = 'padding: 0px 0px 0px 0px';
            $ipad_colspan_width = '30%';
            $ipad_colspan_padding = 'padding: 0px 10px 0px 10px;';
            $ipad_colspan_width2 = '35%';
            $ipad_colspan_padding2 = 'padding: 0px 0px 0px 0px;';
            $ipad_width2 = '1%';
            $ipad_width_3 = '18%';
        }
        else {  
            $ipad_width_2 = '10%';
            $mobile_width = '5%';
            $mobile_width2 = '30%';
            $mobile_padding = 'padding: 0px 5px 0px 0px;';
            $mobile_padding2 = 'padding: 0px 0px 0px 5px';
            $ipad_colspan_width = '35%';
            $ipad_colspan_padding = 'padding: 0px 0px 0px 0px;';
            $ipad_colspan_width2 = '31%';
            $ipad_colspan_padding2 = 'padding: 0px 0px 0px 15px;';
            $ipad_width_2 = '13%';
            $ipad_width_3 = '18%';
        }
        $temp_display .='<div class="api_display_none">';
        $temp_display .= '
                    <div class="col-md-12 api_padding_0 api_temp_section_5 visible-sm">  
                        <div class="api_temp_5 api_padding_0">
                            <table width="100%" border="0" style="font-size: 13px;color: #000;">
                                <tr>
                                    <td width="10%" align="center"></td>
                                    <td width="5%" align="center" class="service_flow_arrow_pointer_tr_1 api_hr_line_height api_service_flow_font_size_15">             
                                        <div class="api_display_table_cell api_vertical_align_middle api_padding_left_20">                       
                                            '.$this->api_helper->api_lang('Recruit plan Recruit strategy').'
                                        </div>         
                                    </td>
                                    <td width="5%" align="center" class="service_flow_arrow_pointer_tr_2 api_hr_line_height api_service_flow_font_size_15">             
                                        <div class="api_display_table_cell api_vertical_align_middle api_padding_left_20">                       
                                            '.$this->api_helper->api_lang('Population Formation').'
                                        </div>         
                                    </td>
                                    <td width="5%" align="center" class="service_flow_arrow_pointer_tr_3 api_hr_line_height api_service_flow_font_size_15">             
                                        <div class="api_display_table_cell api_vertical_align_middle api_padding_left_20">                       
                                            '.$this->api_helper->api_lang('Application reception Document screening').'
                                        </div>         
                                    </td>
                                    <td width="5%" align="center" class="service_flow_arrow_pointer_tr_4 api_hr_line_height api_service_flow_font_size_15">             
                                        <div class="api_display_table_cell api_vertical_align_middle api_padding_left_20">                       
                                            '.$this->api_helper->api_lang('Interview selection').'
                                        </div>         
                                    </td>
                                    <td width="5%" align="center" class="service_flow_arrow_pointer_tr_5 api_hr_line_height api_service_flow_font_size_15">             
                                        <div class="api_display_table_cell api_vertical_align_middle api_padding_left_20">                       
                                            '.$this->api_helper->api_lang('Acceptance Support').'
                                        </div>         
                                    </td>
                                </tr>
                                <tr>
                                    <td width="5%" align="center" class="api_service_flow_font_size_15">
                                        <div style="width:100%; height:125px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle">                       
                                                '.$this->api_helper->api_lang('In-house Response').'
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="15%" align="center">
                                        <div style="width:100%; height:125px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 55px;">'.$this->api_helper->api_lang('Recruitment planning').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 55px;
                                                ">'.$this->api_helper->api_lang('Secure recruitment budget').'</div> 
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="" align="center">
                                        <div style="width:100%; height:125px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_height_5"></div>
                                                <div class="api_height_35"></div> 
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="" align="center">
                                        <div style="width:100%; height:125px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 55px;">'.$this->api_helper->api_lang('Pass / Fail').'</div>
                                                <div class="api_height_20"></div>
                                                <div class="api_height_40"></div> 
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="16%" align="center">
                                        <div style="width:100%; height:125px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 55px;">'.$this->api_helper->api_lang('Conduct an interview').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 55px;">'.$this->api_helper->api_lang('Pass / fail judgment').'</div>
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="18%" align="center">
                                        <div style="width:100%; height:125px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 55px;">'.$this->api_helper->api_lang('Recruitment judgment').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 55px;">'.$this->api_helper->api_lang('Confirmation of conditions').'</div>
                                            </div>
                                        </div>             
                                    </td>
                                </tr>
                                <tr>
                                    <td width="5%" align="center" class="api_service_flow_font_size_15">
                                        <div style="width:100%; height:245px;display:table;background-color:#f4cbcc; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle">                       
                                                '.$this->api_helper->api_lang('Consulting').'
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="14.28%" align="center">
                                        <div style="width:100%; height:245px;display:table;background-color:#f4cbcc; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 55px;
                                                ">'.$this->api_helper->api_lang('Human resources requirement definition').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 55px;padding-top: 2px;
                                                "'.$this->api_helper->api_lang('Recruitment process design').'</div> 
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 55px;">'.$this->api_helper->api_lang('Recruitment channel selection').'</div>
                                                <div class="api_height_60"></div>
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="14.28%" align="center">
                                        <div style="width:100%; height:245px;display:table;background-color:#f4cbcc; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">      
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">           
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 55px;">'.$this->api_helper->api_lang('Agent management').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 55px;">'.$this->api_helper->api_lang('Advertising direction').'</div> 
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 55px;">'.$this->api_helper->api_lang('Event Planing').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 55px;">'.$this->api_helper->api_lang('Direct recruitment planing').'</div>
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="14.28%" align="center" colspan="3">
                                        <div style="width:100%; height:245px;display:table;background-color:#f4cbcc; padding:5px;line-height: 1.2em;">                
                                            <div class="" style="padding:5px">  
                                                <table width="100%">
                                                    <tr>
                                                        <td width="'.$ipad_colspan_width.'"></td>
                                                        <td width="'.$ipad_colspan_width2.'" align="center" style="'.$ipad_colspan_padding.'">
                                                            <div class="api_display_text_middle" style="height:55px;font-size:12px;border: 2px solid #eb9c9d;">
                                                                '.$this->api_helper->api_lang('Interviewer training').'
                                                            </div>
                                                        </td>
                                                        <td width="30%" align="center" style="'.$ipad_colspan_padding2.'">
                                                            <div class="api_display_text_middle" style="height:55px;font-size:12px;border: 2px solid #eb9c9d;">
                                                                '.$this->api_helper->api_lang('Closing support').'
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 55px;">'.$this->api_helper->api_lang('Candidate progress management').'</div>
                                            </div>
                                        </div>             
                                    </td>
                                </tr>
                                <tr>
                                    <td width="5%" align="center" class="api_service_flow_font_size_15">
                                        <div style="width:100%; height:185px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle">                       
                                                '.$this->api_helper->api_lang('Agency').'
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="" align="center">
                                        <div style="width:100%; height:185px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Recruitment public relations').'</div>
                                                <div class="api_height_100"></div>
                                                <div class="api_height_20"></div>
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="" align="center">
                                        <div style="width:100%; height:185px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Each channel cooperation').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Advertising operation').'</div>                    
                                                <div class="api_height_60"></div> 
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="" align="center">
                                        <div style="width:100%; height:185px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Application reception').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Document screening').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Pass/ Fail inform').'</div>
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="14.28%" align="center">
                                        <div style="width:100%; height:185px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Interview schedule adjustment').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Pass/ Fail inform v2').'</div>
                                                <div class="api_height_60"></div>
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="14.28%" align="center">
                                        <div style="width:100%; height:185px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Sending job offer').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Collection of tentative job offer documents').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Follow-up').'</div>
                                            </div>
                                        </div>             
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                ';
        $temp_display .= '<div class="api_height_30 api_clear_both visible-sm"></div>';
        $temp_display .= '</div>';
        
        $temp_display .= '
                    <div class="col-md-12 api_padding_0 api_temp_section_5 visible-sm">  
                        <div class="api_temp_5 api_padding_0">
                            <table width="100%" border="0" style="font-size: 13px;color: #000;">
                                <tr>
                                    <td width="'.$ipad_width_2.'" align="center"></td>
                                    <td width="5%" align="center" class="service_flow_arrow_pointer_tr_1 api_hr_line_height api_service_flow_font_size_15">             
                                        <div class="api_display_table_cell api_vertical_align_middle api_padding_left_20">                       
                                            '.$this->api_helper->api_lang('Recruit plan Recruit strategy').'
                                        </div>         
                                    </td>
                                    <td width="5%" align="center" class="service_flow_arrow_pointer_tr_2 api_hr_line_height api_service_flow_font_size_15">             
                                        <div class="api_display_table_cell api_vertical_align_middle api_padding_left_20">                       
                                            '.$this->api_helper->api_lang('Population Formation').'
                                        </div>         
                                    </td>
                                    <td width="5%" align="center" class="service_flow_arrow_pointer_tr_3 api_hr_line_height api_service_flow_font_size_15">             
                                        <div class="api_display_table_cell api_vertical_align_middle api_padding_left_20">                       
                                            '.$this->api_helper->api_lang('Application reception Document screening').'
                                        </div>         
                                    </td>
                                    <td width="5%" align="center" class="service_flow_arrow_pointer_tr_4 api_hr_line_height api_service_flow_font_size_15">             
                                        <div class="api_display_table_cell api_vertical_align_middle api_padding_left_20">                       
                                            '.$this->api_helper->api_lang('Interview selection').'
                                        </div>         
                                    </td>
                                    <td width="5%" align="center" class="service_flow_arrow_pointer_tr_5 api_hr_line_height api_service_flow_font_size_15">             
                                        <div class="api_display_table_cell api_vertical_align_middle api_padding_left_20">                       
                                            '.$this->api_helper->api_lang('Acceptance Support').'
                                        </div>         
                                    </td>
                                </tr>
                                <tr>
                                    <td width="5%" align="center" class="api_service_flow_font_size_15">
                                        <div style="width:100%; height:125px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle">                       
                                                '.$this->api_helper->api_lang('In-house Response').'
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="18%" align="center">
                                        <div style="width:100%; height:125px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 55px;">'.$this->api_helper->api_lang('Recruitment planning').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 55px;
                                                ">'.$this->api_helper->api_lang('Secure recruitment budget').'</div> 
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="18%" align="center">
                                        <div style="width:100%; height:125px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_height_5"></div>
                                                <div class="api_height_35"></div> 
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="'.$ipad_width_3.'" align="center">
                                        <div style="width:100%; height:125px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 55px;">'.$this->api_helper->api_lang('Pass / Fail').'</div>
                                                <div class="api_height_20"></div>
                                                <div class="api_height_40"></div> 
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="18%" align="center">
                                        <div style="width:100%; height:125px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 55px;">'.$this->api_helper->api_lang('Conduct an interview').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 55px;">'.$this->api_helper->api_lang('Pass / fail judgment').'</div>
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="18%" align="center">
                                        <div style="width:100%; height:125px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 55px;">'.$this->api_helper->api_lang('Recruitment judgment').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 55px;">'.$this->api_helper->api_lang('Confirmation of conditions').'</div>
                                            </div>
                                        </div>             
                                    </td>
                                </tr>
                                <tr>
                                    <td width="5%" align="center" class="api_service_flow_font_size_15">
                                        <div style="width:100%; height:245px;display:table;background-color:#f4cbcc; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle">                       
                                                '.$this->api_helper->api_lang('Consulting').'
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="14.28%" align="center">
                                        <div style="width:100%; height:245px;display:table;background-color:#f4cbcc; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 55px;
                                                ">'.$this->api_helper->api_lang('Human resources requirement definition').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 55px;padding-top: 2px;
                                                "'.$this->api_helper->api_lang('Recruitment process design').'</div> 
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 55px;">'.$this->api_helper->api_lang('Recruitment channel selection').'</div>
                                                <div class="api_height_60"></div>
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="14.28%" align="center">
                                        <div style="width:100%; height:245px;display:table;background-color:#f4cbcc; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">      
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">           
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 55px;">'.$this->api_helper->api_lang('Agent management').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 55px;">'.$this->api_helper->api_lang('Advertising direction').'</div> 
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 55px;">'.$this->api_helper->api_lang('Event Planing').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 55px;">'.$this->api_helper->api_lang('Direct recruitment planing').'</div>
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="14.28%" align="center" colspan="3">
                                        <div style="width:100%; height:245px;display:table;background-color:#f4cbcc; padding:5px;line-height: 1.2em;">                
                                            <div class="" style="padding:5px">  
                                                <table width="100%">
                                                    <tr>
                                                        <td width="'.$ipad_colspan_width.'"></td>
                                                        <td width="'.$ipad_colspan_width2.'" align="center" style="'.$ipad_colspan_padding.'">
                                                            <div class="api_display_text_middle" style="height:55px;font-size:12px;border: 2px solid #eb9c9d;">
                                                                '.$this->api_helper->api_lang('Interviewer training').'
                                                            </div>
                                                        </td>
                                                        <td width="30%" align="center" style="'.$ipad_colspan_padding2.'">
                                                            <div class="api_display_text_middle" style="height:55px;font-size:12px;border: 2px solid #eb9c9d;">
                                                                '.$this->api_helper->api_lang('Closing support').'
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #eb9c9d;height: 55px;">'.$this->api_helper->api_lang('Candidate progress management').'</div>
                                            </div>
                                        </div>             
                                    </td>
                                </tr>
                                <tr>
                                    <td width="5%" align="center" class="api_service_flow_font_size_15">
                                        <div style="width:100%; height:185px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle">                       
                                                '.$this->api_helper->api_lang('Agency').'
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="" align="center">
                                        <div style="width:100%; height:185px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Recruitment public relations').'</div>
                                                <div class="api_height_100"></div>
                                                <div class="api_height_20"></div>
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="" align="center">
                                        <div style="width:100%; height:185px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Each channel cooperation').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Advertising operation').'</div>                    
                                                <div class="api_height_60"></div> 
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="" align="center">
                                        <div style="width:100%; height:185px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Application reception').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Document screening').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Pass/ Fail inform').'</div>
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="14.28%" align="center">
                                        <div style="width:100%; height:185px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Interview schedule adjustment').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Pass/ Fail inform v2').'</div>
                                                <div class="api_height_60"></div>
                                            </div>
                                        </div>             
                                    </td>
                                    <td width="14.28%" align="center">
                                        <div style="width:100%; height:185px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;">                
                                            <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Sending job offer').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Collection of tentative job offer documents').'</div>
                                                <div class="api_height_5"></div>
                                                <div class="api_display_text_middle" style="border: 2px solid #e79840;height: 55px;">'.$this->api_helper->api_lang('Follow-up').'</div>
                                            </div>
                                        </div>             
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
            ';
    // ipad---

    // ---mobile
        if($l == 'en') {
            $mb_tr_1 = '10%';
            $mb_tr = '1%';
            $mb_tr_2 = '5%';
            $mb_tr_3 = '5%';
            $mb_tr_4 = '30%';
            $mb_tr_5 = '35%';
        } else {
            $mb_tr_1 = '10%';
            $mb_tr = '3.5%';
            $mb_tr_2 = '10%';
            $mb_tr_3 = '5%';
            $mb_tr_4 = '30%';
            $mb_tr_5 = '30%';
        }
        $temp_display .= '
                        <div class="col-md-12 api_padding_0 api_temp_section_5 visible-xs">  
                            <div class="api_temp_5 api_padding_0">
                                <table width="100%" border="0" style="font-size: 10px;color: #000;">
                                    <tr>
                                        <td width="'.$mb_tr.'" align="center"></td>
                                        <td width="'.$mb_tr_2.'" align="center" class="service_flow_arrow_pointer_tr_1 api_service_flow_font_size_15">             
                                            <div class="api_display_table_cell api_vertical_align_middle api_padding_left_20 api_hr_line_height">                       
                                                '.$this->api_helper->api_lang('Recruit plan Recruit strategy').'
                                            </div>         
                                        </td>
                                        <td width="10%" align="center" class="service_flow_arrow_pointer_tr_2 api_hr_line_height api_service_flow_font_size_15">             
                                            <div class="api_display_table_cell api_vertical_align_middle api_padding_left_20">                       
                                                '.$this->api_helper->api_lang('Recruit plan Recruit strategy').'
                                            </div>         
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="1%" align="center" class="api_service_flow_font_size_15">
                                            <div class="api_mb_screen_320_height_2" style="width:100%; height:100px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                                <div class="api_display_table_cell api_vertical_align_middle">                       
                                                    '.$this->api_helper->api_lang('In-house Response').'
                                                </div>
                                            </div>             
                                        </td>
                                        <td width="'.$mb_tr_2.'" align="center">
                                            <div class="api_mb_screen_320_height_2" style="width:100%; height:100px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                                <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #6c98e9;height: 40px;">'.$this->api_helper->api_lang('Recruitment planning').'</div>
                                                    <div class="api_height_5"></div>
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #6c98e9;height: 40px;
                                                    ">'.$this->api_helper->api_lang('Secure recruitment budget').'</div> 
                                                </div>
                                            </div>             
                                        </td>
                                        <td width="'.$mb_tr_1.'" align="center">
                                            <div class="api_mb_screen_320_height_2" style="width:100%; height:100px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;">                
                                                <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                    <div class="api_height_5"></div>
                                                    <div class="api_height_35"></div> 
                                                </div>
                                            </div>             
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="1%" align="center" class="api_service_flow_font_size_15">
                                            <div class="api_mb_screen_320_height_4" style="width:100%; height:185px;display:table;background-color:#f4cbcc; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                                <div class="api_display_table_cell api_vertical_align_middle">                       
                                                    '.$this->api_helper->api_lang('Consulting').'
                                                </div>
                                            </div>             
                                        </td>
                                        <td width="5%" align="center">
                                            <div class="api_mb_screen_320_height_4" style="width:100%; height:185px;display:table;background-color:#f4cbcc; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                                <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #eb9c9d;height: 40px;">'.$this->api_helper->api_lang('Human resources requirement definition').'</div>
                                                    <div class="api_height_5"></div>
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #eb9c9d;height: 40px;">'.$this->api_helper->api_lang('Recruitment process design').'</div> 
                                                    <div class="api_height_5"></div>
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #eb9c9d;height: 40px;">'.$this->api_helper->api_lang('Recruitment channel selection').'</div>
                                                    <div class="api_height_45"></div>
                                                    <div class="api_mb_screen_320_height_15"></div>
                                                </div>
                                            </div>             
                                        </td>
                                        <td width="'.$mb_tr_1.'" align="center">
                                            <div class="api_mb_screen_320_height_4" style="width:100%; height:185px;display:table;background-color:#f4cbcc; padding:5px;line-height: 1.2em;">      
                                                <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">           
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #eb9c9d;height: 40px;">'.$this->api_helper->api_lang('Agent management').'</div>
                                                    <div class="api_height_5"></div>
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #eb9c9d;height: 40px;
                                                    ">'.$this->api_helper->api_lang('Advertising direction').'</div> 
                                                    <div class="api_height_5"></div>
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #eb9c9d;height: 40px;">'.$this->api_helper->api_lang('Event Planing').'</div>
                                                    <div class="api_height_5"></div>
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #eb9c9d;height: 40px;">'.$this->api_helper->api_lang('Direct recruitment planing').'</div>
                                                </div>
                                            </div>             
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="1%" align="center" class="api_service_flow_font_size_15">
                                            <div class="api_mb_screen_320_height_3" style="width:100%; height:145px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                                <div class="api_display_table_cell api_vertical_align_middle">                       
                                                    '.$this->api_helper->api_lang('Agency').'
                                                </div>
                                            </div>             
                                        </td>
                                        <td width="5%" align="center">
                                            <div class="api_mb_screen_320_height_3" style="width:100%; height:145px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                                <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #e79840;height: 40px;">'.$this->api_helper->api_lang('Recruitment public relations').'</div>
                                                    <div class="api_height_90"></div>
                                                    <div class="api_mb_screen_320_height_15"></div>
                                                </div>
                                            </div>             
                                        </td>
                                        <td width="'.$mb_tr_1.'" align="center">
                                            <div class="api_mb_screen_320_height_3" style="width:100%; height:145px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;">                
                                                <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">    
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #e79840;height: 40px;">'.$this->api_helper->api_lang('Each channel cooperation').'</div>
                                                    <div class="api_height_5"></div>
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #e79840;height: 40px;">'.$this->api_helper->api_lang('Advertising operation').'</div>                    
                                                    <div class="api_height_45"></div> 
                                                </div>
                                            </div>             
                                        </td>
                                    </tr>
                                </table>
                                <div class="api_height_30"></div>
                                <table width="100%" border="0" style="font-size: 10px;color: #000;">
                                    <tr>
                                        <td width="'.$mb_tr.'" align="center"></td>
                                        <td width="'.$mb_tr_3.'" align="center" class="service_flow_arrow_pointer_tr_3 api_hr_line_height api_service_flow_font_size_15">             
                                            <div class="api_display_table_cell api_vertical_align_middle ">                       
                                                '.$this->api_helper->api_lang('Application reception Document screening').'
                                            </div>         
                                        </td>
                                        <td width="10%" align="center" class="service_flow_arrow_pointer_tr_4 api_hr_line_height api_service_flow_font_size_15">             
                                            <div class="api_display_table_cell api_vertical_align_middle api_padding_left_20">                       
                                                '.$this->api_helper->api_lang('Interview selection').'
                                            </div>         
                                        </td>
                                        <td width="10%" align="center" class="service_flow_arrow_pointer_tr_5 api_hr_line_height api_service_flow_font_size_15">             
                                            <div class="api_display_table_cell api_vertical_align_middle api_padding_left_20">                       
                                            '.$this->api_helper->api_lang('Acceptance Support').'
                                            </div>         
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="1%" align="center" class="api_service_flow_font_size_15">
                                            <div class="api_mb_screen_320_height_2" style="width:100%; height:100px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                                <div class="api_display_table_cell api_vertical_align_middle">                       
                                                    '.$this->api_helper->api_lang('In-house Response').'
                                                </div>
                                            </div>             
                                        </td>
                                        <td width="'.$mb_tr_3.'" align="center">
                                            <div class="api_mb_screen_320_height_2" style="width:100%; height:100px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                                <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                    <div class="api_display_text_middle" style="border: 2px solid #6c98e9;height: 40px;">'.$this->api_helper->api_lang('Pass / Fail').'</div>
                                                    <div class="api_height_45"></div>
                                                    <div class="api_mb_screen_320_height_30"></div>
                                                </div>
                                            </div>             
                                        </td>
                                        <td width="5%" align="center">
                                            <div style="width:100%; height:100px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                                <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #6c98e9;height: 40px;">'.$this->api_helper->api_lang('Conduct an interview').'</div>
                                                    <div class="api_height_5"></div>
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #6c98e9;height: 40px;
                                                    ">'.$this->api_helper->api_lang('Pass / fail judgment').'</div> 
                                                </div>
                                            </div>             
                                        </td>
                                        <td width="5%" align="center">
                                            <div style="width:100%; height:100px;display:table;background-color:#c9d9f8; padding:5px;line-height: 1.2em;">                
                                                <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #6c98e9;height: 40px;">'.$this->api_helper->api_lang('Recruitment judgment').'</div>
                                                    <div class="api_height_5"></div>
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #6c98e9;height: 40px;
                                                    ">'.$this->api_helper->api_lang('Confirmation of conditions').'</div> 
                                                </div>
                                            </div>             
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="1%" align="center" class="api_service_flow_font_size_15">
                                            <div style="width:100%; height:185px;display:table;background-color:#f4cbcc; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                                <div class="api_display_table_cell api_vertical_align_middle">                       
                                                    '.$this->api_helper->api_lang('Consulting').'
                                                </div>
                                            </div>             
                                        </td>
                                        <td width="'.$mb_tr_3.'" align="center" colspan="3">
                                            <div style="width:100%; height:185px;display:table;background-color:#f4cbcc; padding:5px;line-height: 1.2em;">                
                                                <div class="" style="padding:5px">  
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="'.$mb_tr_4.'" align="center" style="'.$mobile_padding.'">
                                                                <div class="api_display_text_middle api_mb_screen_320" style="font-size:10px;border: 2px solid #eb9c9d;padding: 8px;height: 40px;">
                                                                    '.$this->api_helper->api_lang('Interviewer training').'
                                                                </div>
                                                            </td>
                                                            <td width="30%" align="center" style="'.$mobile_padding2.'">
                                                                <div class="api_display_text_middle api_mb_screen_320" style="font-size:10px;border: 2px solid #eb9c9d;padding: 8px;height: 40px;">
                                                                    '.$this->api_helper->api_lang('Closing support').'
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="api_height_5"></div>
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #eb9c9d;height: 40px;">'.$this->api_helper->api_lang('Candidate progress management').'</div>
                                                </div>
                                            </div>             
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="1%" align="center" class="api_service_flow_font_size_15">
                                            <div class="api_mb_screen_320_height" style="width:100%; height:145px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                                <div class="api_display_table_cell api_vertical_align_middle">                       
                                                    '.$this->api_helper->api_lang('Agency').'
                                                </div>
                                            </div>             
                                        </td>
                                        <td width="'.$mb_tr_3.'" align="center">
                                            <div class="api_mb_screen_320_height" style="width:100%; height:145px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                                <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">                       
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #e79840;height: 40px;">'.$this->api_helper->api_lang('Application reception').'</div>
                                                    <div class="api_height_5"></div>
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #e79840;height: 40px;">'.$this->api_helper->api_lang('Document screening').'</div>
                                                    <div class="api_height_5"></div>
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #e79840;height: 40px;">'.$this->api_helper->api_lang('Pass/ Fail inform').'</div>
                                                </div>
                                            </div>             
                                        </td>
                                        <td width="" align="center">
                                            <div class="api_mb_screen_320_height" style="width:100%; height:145px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;border-right: 1px solid #fff;">                
                                                <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">    
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #e79840;height: 40px;">'.$this->api_helper->api_lang('Interview schedule adjustment').'</div>
                                                    <div class="api_height_5"></div>
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #e79840;height: 40px;">'.$this->api_helper->api_lang('Pass/ Fail inform v2').'</div>                    
                                                    <div class="api_height_45"></div> 
                                                    <div class="api_mb_screen_320_height_15"></div>
                                                </div>
                                            </div>             
                                        </td>
                                        <td width="2%" align="center">
                                            <div style="width:100%; height:145px;display:table;background-color:#f7cb9c; padding:5px;line-height: 1.2em;">                
                                                <div class="api_display_table_cell api_vertical_align_middle" style="padding:5px">    
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #e79840;height: 40px;">'.$this->api_helper->api_lang('Sending job offer').'</div>
                                                    <div class="api_height_5"></div>
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #e79840;height: 40px;">'.$this->api_helper->api_lang('Collection of tentative job offer documents').'</div>                    
                                                    <div class="api_height_5"></div>
                                                    <div class="api_display_text_middle api_mb_screen_320" style="border: 2px solid #e79840;height: 40px;">'.$this->api_helper->api_lang('Follow-up').'</div>
                                                </div>
                                            </div>             
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
            ';
        $temp_display .= '<div class="api_height_30 api_clear_both visible-xs"></div>';
    // mobile---

//-SERVICE FLOW 2----


//-section voice- 
    $temp_display .= '
            <div class="col-md-6">
    ';
        $config_data_function = array(
            'title_1_key' => 'Have been using HR Service',
            'title_2_key' => 'From Company which',
            'title_3_key' => 'Voice',
            'title_4_key' => 'have been used HR Service',
        );
        include 'themes/default/shop/views/api/sub_page/api_customer_voice_header.php';
        $temp_display .= $return['display']; 

        $config_data_function = array(
            'wrapper_class' => 'col_border_title_description_year',
            'col_class' => 'col-md-6',
            'title_key' => 'we_are_resecued_by_hight_quality',
            'description_key' => 'ayum_for_hr_document_1',
            'year_text_key' => 'japanese_company_v2',
        );
        $temp = $this->api_display->col_border_title_description_year($config_data_function);
        $temp_display .= $temp['display'];

    $temp_display .= '
             </div>   
    ';

    $temp_display .= '
            <div class="col-md-6">
    ';

        $config_data_function = array(
            'wrapper_class' => 'col_border_title_description_year',
            'col_class' => 'col-md-6',
            'title_key' => 'perfect_follow_up_lead',
            'description_key' => 'ayum_for_hr_document_2',
            'year_text_key' => 'japanese_company',
        );
        $temp = $this->api_display->col_border_title_description_year($config_data_function);
        $temp_display .= $temp['display'];

        $config_data_function = array(
            'wrapper_class' => 'col_border_title_description_year',
            'col_class' => 'col-md-6',
            'title_key' => 'be_able_consult_from_the_strategy',
            'description_key' => 'ayum_for_hr_document_3',
            'year_text_key' => 'hong_kong_company',
        );
        $temp = $this->api_display->col_border_title_description_year($config_data_function);
        $temp_display .= $temp['display'];
            
    $temp_display .= '
            </div> 
    ';
//-section voice-     


$temp_display .= '<div class="col-md-12 text-center">
            <a href="javascript:void(0);" onclick="api_ayum_for_companies_contact_click();">
                <div class="api_button_2">
                <table width="100%" border="0">
                    <tbody>
                    <tr>
                        <td valign="middle" class="api_temp_td_5">
                            '.$this->api_helper->api_lang('contact_us_v2').'
                        </td>
                        <td valign="middle" width="30">
                            &nbsp;<li class="fa fa-arrow-right"></li>
                        </td>
                    </tr>
                    </tbody>
                </table>
                   
                </div>       
            </a>     
        </div>
';
    

    // footer
    $config_data = array(
        'wrapper_class' => 'api_page_ayum_hr',
        'custom_html' => '',
        'display' => $temp_display,
        'display_class' => '',
        'panel' => '',
        'panel_class' => '',
        'type' => '',
    );
    $temp = $this->api_display->template_display($config_data);
    echo $temp['display'];

    echo '<div class="api_height_10 api_clear_both"></div>';


echo '    
    <script  src="'.base_url().'/assets/api/page/'.$temp_page_name.'/js/script.js?v='.$this->api_shop_setting[0]['version'].'"></script>
';    
?>