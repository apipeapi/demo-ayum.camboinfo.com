<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php 
    $l = $this->api_shop_setting[0]['api_lang_key'];

    echo '
<link href="'.base_url().'/assets/api/page/ayum_blog/css/style.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/ayum_blog/css/style_600.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/ayum_blog/css/style_768.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/ayum_blog/css/style_1024.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
    ';
    
    echo '<div class="api_height_30"></div>';
        
    $temp_display = '';

    $temp_under = '';
    if ($this->session->userdata('user_id') == '') {
        $temp_display .= '
        <table width="100%" border="0">
        <tr>
        <td valign="top" align="center" height="300">
            <img class="img-responsive api_margin_auto api_display_none" src="'.base_url().'assets/api/image/under_construction.gif" />
            <div class="api_big api_padding_bottom_15" id="api_color_red_3">
                '.$this->api_helper->api_lang('We are developing this page').'
            </div>           
        </td>
        </tr>
        </table>     
        ';
        $temp_under = 'api_display_none';
    }
    $temp_display .= '
<div class="'.$temp_under.'">
        <div class="api_height_30 visible-xs"></div>
        <div class="col-md-12 visible-sm visible-xs">
                   <form action="'.base_url().'Blog" name="api_form_blog" method="GET">
            <input class="api_temp_search" name="search" placeholder="'.lang('Search').'" value="'.$_GET['search'].'" onkeydown="if (event.keyCode == 13) document.api_form_blog.submit();" autocomplete="off"/>
        </form>
            <div class="api_temp_search_icon" onclick="document.api_form_blog_sm.submit();">
                <li class="fa fa-search"></li>
            </div>
            <div class="api_height_15"></div>
            <div class="api_height_15 hidden-xs"></div>
        </div>
    ';

    $temp_display_2 = '';
    $temp_display_3 = '';

    $temp_selected = '';
    if ($data_category_slug == '')
        $temp_selected = '_selected api_link_box_none';       
    $temp_display_2 .= '
        <td valign="middle" class="api_temp_1'.$temp_selected.' api_admin api_admin_wrapper_translation_Hot_News_Article">  
            <a href="'.base_url().'Blog/'.$temp[$i]['slug'].'" >
                <div class="col-md-12 api_padding_0">        
                    <div class="api_height_5 visible-xs"></div>
                    '.$this->api_helper->api_lang('Hot News Article').'
                    <div class="api_height_5 visible-xs"></div>
                </div>
            </a>
    ';

$temp = $this->site->api_select_some_fields_with_where("
    id
    "
    ,"sma_translation"
    ,"keyword = 'Hot News Article'"
    ,"arr"
);
$field_data = array();
$field_data[0] = array(
    'type' => 'translate',
    'col_class' => 'col-md-12',
    'field_class' => '',
    'field_name' => 'translate',
    'translate' => 'yes',
    'field_label_name' => 'Title',                      
);
$config_data = array(
    'l' => $l,
    'wrapper_class' => 'api_admin_wrapper_translation_Hot_News_Article',
    'modal_class' => '',
    'title' => 'Edit', 
    'field_data' => $field_data,
    'selected_id' => $temp[0]['id'],
    'table_name' => 'sma_translation',
    'table_id' => 'id',
    'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
);
$temp_admin = $this->api_admin->front_end_edit($config_data);
$temp_display_2 .= $temp_admin['display'];
$this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display_2 .= '
        </td>
        <td valign="middle" width="30">
        </td>
    ';

    $config_data = array(
        'table_name' => 'sma_blog_category',
        'select_table' => 'sma_blog_category',
        'translate' => 'yes',
        'description' => '',
        'select_condition' => "id > 0 order by ordering asc",
    );
    $temp = $this->api_helper->api_select_data_v2($config_data);    
    for ($i=0;$i<count($temp);$i++) {
        $temp_selected = '';
        if ($data_category_slug == $temp[$i]['slug'])
            $temp_selected = '_selected api_link_box_none';  
        if ($i < count($temp) - 1)      
            $temp_2 = '<td valign="middle" width="30"></td> ';
        else
            $temp_2 = '';
        $temp_display_2 .= '
            <td valign="middle" class="api_temp_1'.$temp_selected.' api_admin api_admin_wrapper_blog_category_'.$temp[$i]['id'].'">
                <a href="'.base_url().'Blog/'.$temp[$i]['slug'].'">
                    <div class="col-md-12 api_padding_0">
                        <div class="api_height_5 visible-xs"></div>
                        '.$temp[$i]['title_'.$l].'
                        <div class="api_height_5 visible-xs"></div>
                    </div>
                </a>
        ';
        $field_data = array();
        $field_data_add = array();
        $field_data[0] = array(
            'type' => 'translate',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'translate',
            'translate' => 'yes',
            'field_label_name' => 'Title',                      
        );     
        $field_data[1] = array(
            'type' => 'text',
            'col_class' => 'col-md-3',
            'field_class' => 'api_numberic_input',
            'field_name' => 'ordering',
            'field_label_name' => 'Ordering',                      
            'field_value' => '',
        );        
        $config_data = array(
            'l' => $l,
            'wrapper_class' => 'api_admin_wrapper_blog_category_'.$temp[$i]['id'],
            'modal_class' => '',
            'title' => 'Edit', 
            'field_data' => $field_data,
            'selected_id' => $temp[$i]['id'],
            'table_name' => 'sma_blog_category',
            'table_id' => 'id',
            'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
            'condition_ordering' => "",
            'field_data_add' => $field_data_add,
            'add' => 'yes',
            'title_add' => 'Add New Blog Category',
            'delete' => 'yes',
            'title_delete' => '<div>Your will remove</div>'.$temp[$i]['title_'.$l],
            'generate_slug' => 'yes',
        );
        $temp_admin = $this->api_admin->front_end_edit($config_data);
        $temp_display_2 .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

        $temp_display_2 .= '
            </td>
            '.$temp_2.'       
        ';
    }
    
    $temp_selected = '';
    if ($data_category_slug == '')
        $temp_selected = '_selected api_link_box_none';       
    $temp_display_3 .= '
        <td valign="middle" class="api_temp_1'.$temp_selected.' api_admin api_admin_wrapper_translation_Hot_News_Article">  
            <div class="api_height_5 visible-xs"></div>
            <a href="'.base_url().'Blog/'.$temp[$i]['slug'].'">     
                '.lang('Hot News Article').'
            </a>
            <div class="api_height_5 visible-xs"></div>
        </td>
        <td valign="middle" width="30">
        </td>
    ';

    $config_data = array(
        'table_name' => 'sma_blog_category',
        'select_table' => 'sma_blog_category',
        'translate' => 'yes',
        'description' => '',
        'select_condition' => "id > 0 order by ordering asc limit 2",
    );
    $temp = $this->api_helper->api_select_data_v2($config_data);    
    for ($i=0;$i<count($temp);$i++) {
        $temp_selected = '';
        if ($data_category_slug == $temp[$i]['slug'])
            $temp_selected = '_selected api_link_box_none';  
        if ($i < count($temp) - 1)      
            $temp_2 = '<td valign="middle" width="30"></td> ';
        else
            $temp_2 = '';
        $temp_display_3 .= '
            <td valign="middle" class="api_temp_1'.$temp_selected.'">
                <div class="api_height_5 visible-xs"></div>
                <a href="'.base_url().'Blog/'.$temp[$i]['slug'].'">
                    <div class="">        
                        '.$temp[$i]['title_'.$l].'
                    </div>
                </a>
                <div class="api_height_5 visible-xs"></div>
            </td>
            '.$temp_2.'       
        ';
    }

    $temp_display_4 = '';
    $config_data = array(
        'table_name' => 'sma_blog_category',
        'select_table' => 'sma_blog_category',
        'translate' => 'yes',
        'description' => '',
        'select_condition' => "id > 0 order by ordering asc limit 2,3",
    );
    $temp = $this->api_helper->api_select_data_v2($config_data);    
    for ($i=0;$i<count($temp);$i++) {
        $temp_selected = '';
        if ($data_category_slug == $temp[$i]['slug'])
            $temp_selected = '_selected api_link_box_none';  
        if ($i < count($temp) - 1)      
            $temp_2 = '<td valign="middle" width="30"></td> ';
        else
            $temp_2 = '';
        $temp_display_4 .= '
            <td valign="middle" class="api_temp_1'.$temp_selected.'">
                <div class="api_height_5 visible-xs"></div>
                <a href="'.base_url().'Blog/'.$temp[$i]['slug'].'">
                    <div class="">        
                        '.$temp[$i]['title_'.$l].'
                    </div>
                </a>
                <div class="api_height_5 visible-xs"></div>
            </td>
            '.$temp_2.'       
        ';
    }

    $temp_display .= '
        <div class="col-md-12 hidden-xs">
            <table width="100%" border="0">
            <tr>    
            '.$temp_display_2.'
            </tr>
            </table>        
        </div>
        <div class="col-md-12 visible-xs">
            <table width="100%" border="0">
            <tr>    
            '.$temp_display_3.'
            </tr>
            <tr> 
            <td colspan="3" height="15">
            </td>
            </tr>
            <tr>    
            '.$temp_display_4.'
            </tr>            
            </table>        
        </div>
    ';

    $language_array = unserialize(multi_language);
    $select_view = array();
    for ($i=0;$i<count($select_data);$i++) {    
        for ($i2=0;$i2<count($language_array);$i2++) {
            $select_view[$i]['title_'.$language_array[$i2][0]] = $select_data[$i]['title_'.$language_array[$i2][0]];
            $select_view[$i]['intro_'.$language_array[$i2][0]] = $this->sma->decode_html($select_data[$i]['description_'.$language_array[$i2][0]]); 
        }    
        $config_data_2 = array(
            'table_name' => 'sma_blog_category',
            'select_table' => 'sma_blog_category',
            'translate' => 'yes',
            'description' => '',
            'select_condition' => "id = ".$select_data[$i]['c_id'],
        );
        $temp_category = $this->api_helper->api_select_data_v2($config_data_2);    

        $select_view[$i]['id'] = $select_data[$i]['id'];
        $select_view[$i]['link'] = base_url().'Blog/'.$temp_category[0]['slug'].'/'.$select_data[$i]['id'];
        $data_view = array (
            'wrapper_class' => '',
            'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/blog/thumb/'.$select_data[$i]['image'],
            'max_width' => 0,
            'max_height' => 0,
            'product_link' => '',
            'image_class' => 'api_link_box_none api_img_blog_medium_v2',
            'image_id' => '',   
            'resize_type' => 'fixed',       
        );
        $temp_image = array();
        $temp_image = $this->api_helper->api_get_image($data_view);
        $select_view[$i]['image'] = '
            <a href="'.$select_view[$i]['link'].'">
                '.$temp_image['image_table'].'
            </a>    
        ';        
        $select_view[$i]['date'] = $select_data[$i]['created_date'];
        $select_view[$i]['image_title_center'] = $select_data[$i]['title_2_'.$l];
        $select_view[$i]['image_title_bg'] = $this->api_helper->get_category_title($l,$select_data[$i]['c_id'],'sma_blog_category');
    }    
    $config_data = array();
    $temp_config = $this->api_display->large_icon_config($config_data);
    $config_data = $temp_config['config_data'];
    $config_data['lang_key'] = $this->api_shop_setting[0]['api_lang_key'];
    $config_data['select_data'] = $select_view;
    
    $config_data['image_class'] = 'img-responsive api_link_box_none';
    $config_data['view_all'] = '';
    $config_data['col_class'] = 'col-md-3 col-sm-3 col-xs-3';
    $config_data['title_class'] = 'api_big';
    $config_data['intro_class'] = '';
    $config_data['readmore_class'] = 'api_display_none';
    $config_data['date_class'] = 'api_color_gray api_text_align_left';
    $config_data['date_format'] = 'd/m/Y';
    $config_data['view_all'] = '';
    $config_data['image_class'] = 'api_big api_font_size_14';
    $config_data['wrapper_class'] = 'api_temp_blog_news';
    $config_data['file_path'] = 'assets/uploads/web/'.$this->api_web[0]['id'].'/blog';

    $temp_blog = $this->api_display->blog_news($config_data);
    if (count($select_view) <= 0)
        $temp_blog['display'] = lang('no_data_to_display');
    $temp_display .= '
        <div class="api_height_15 api_clear_both"></div>
        <div class="api_height_15 api_clear_both hidden-xs"></div>
        <div class="col-md-12">
            '.$temp_blog['display'].'
        </div>
    ';    
    if (count($select_view) > 0) {
        $config_data = array(
            'pagination_info' => $pagination_info,
            'number_class' => 'api_display_none',
            'box_class' => 'col-md-12 text-center',
        );
        $temp = $this->api_display->pagination($config_data);
        $temp_display .= $temp['display'];
    }

    
    $temp_display .= '<div class="api_height_30 api_clear_both"></div>';

    include 'themes/default/shop/views/api/panel/api_ayum_blog_panel.php';
    
$temp_display .= '
</div>
';
if ($this->session->userdata('user_id') != '') {
    $config_data = array(
        'wrapper_class' => 'api_page_ayum_blog',
        'custom_html' => '',
        'display' => $temp_display,
        'display_class' => 'col-md-9 api_padding_0',
        'panel' => $temp_display_panel,
        'panel_class' => 'col-md-3 api_padding_0',
        'type' => '',
    );
}
else {
    $config_data = array(
        'wrapper_class' => 'api_page_ayum_blog',
        'custom_html' => '',
        'display' => $temp_display,
        'display_class' => 'col-md-12 api_padding_0',
        'panel' => '',
        'panel_class' => 'col-md-3 api_padding_0',
        'type' => '',
    );    
}
    $temp = $this->api_display->template_display($config_data);
    echo $temp['display'];


    echo '<div class="api_height_10 api_clear_both"></div>';


?>

