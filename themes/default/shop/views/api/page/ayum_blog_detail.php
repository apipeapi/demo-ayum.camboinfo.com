<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php 
    $l = $this->api_shop_setting[0]['api_lang_key'];
    $temp_display = '';
    echo '
<link href="'.base_url().'/assets/api/page/ayum_blog/css/style.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/ayum_blog/css/style_600.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/ayum_blog/css/style_768.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/ayum_blog/css/style_1024.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
    ';
    
    
    echo '<div class="api_height_30"></div>';


    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/blog/'.$select_data[0]['image'],
        'max_width' => 1000,
        'max_height' => 500,
        'product_link' => '',
        'image_class' => 'img-responsive api_link_box_none',
        'image_id' => '',   
        'resize_type' => 'full',       
    );
    $temp_image = array();
    $temp_image = $this->api_helper->api_get_image($data_view); 

    $config_data_3 = array(
        'table_name' => 'sma_blog_tags',
        'select_table' => 'sma_blog_tags',
        'translate' => 'yes',
        'select_condition' => "id > 0 order by 'title_en' asc ",
    );
    $temp_select_data_2 = $this->api_helper->api_select_data_v2($config_data_3);

    $temp_display .= '
        <div class="col-md-12 api_admin api_admin_wrapper_blog_'.$select_data[0]['id'].'">
            <div class="api_big">
                '.$select_data[0]['title_'.$l].'
            </div>
            <div class="api_height_15"></div>
            <div class="api_padding_right_15 api_padding_bottom_15 api_temp_detail_image">
                '.$temp_image['image_link'].'
            </div>
            <div class="api_temp_detail_description">
                '.$select_data[0]['description_'.$l].'
            </div>
    ';
    $field_data = array();
    $field_data[0] = array(
        'type' => 'translate',
        'col_class' => 'col-md-6',
        'field_class' => '',
        'field_name' => 'translate',
        'translate' => 'yes',
        'field_label_name' => 'Title',                      
        'required' => 'yes',
    );    
    $field_data[1] = array(
        'type' => 'translate',
        'col_class' => 'col-md-6',
        'field_class' => '',
        'field_name' => 'translate_2',
        'translate_2' => 'yes',
        'field_label_name' => 'Title Over Image',                      
    );             
    $field_data[2] = array(
        'type' => 'date',
        'col_class' => 'col-md-4',
        'field_class' => '',
        'field_name' => 'created_date',
        'translate' => '',
        'field_label_name' => 'Post Date',                      
    );    
    $field_data[3] = array(
        'type' => 'select',
        'col_class' => 'col-md-4',
        'field_class' => '',
        'field_name' => 'c_id',
        'field_label_name' => 'Category',
        'field_value' => $select_data[0]['c_id'],
        'select_field' => 'blog_category',
        'select_field_condition' => "order by title_en asc",
        'required' => 'yes',
    );              
    $field_data[4] = array(
        'type' => 'add_ons_checkbox_yes_no',
        'col_class' => 'col-md-4',
        'field_class' => '',
        'field_name' => 'featured',
        'field_label_name' => 'Display in front page',  
        'field_value' => $select_data[0]['featured'],                   
    );          
    $field_data[5] = array(
        'type' => 'translate_textarea',
        'col_class' => 'col-md-12',
        'field_class' => '',
        'field_name' => 'description',
        'description' => 'yes',
        'field_label_name' => 'Intro',                      
    );             
    $field_data[6] = array(
        'type' => 'checkbox_list',
        'col_class' => 'col-md-6',
        'field_class' => '',
        'field_name' => 'tag_id',
        'field_label_name' => 'Tags',
        'field_value' => $select_data[0]['tag_id'],
        'select_field' => 'blog_tag',
        'select_field_condition' => "order by title_en asc",            
    );         
    $field_data[7] = array(
        'type' => 'file',
        'col_class' => 'col-md-3',
        'field_class' => '',
        'field_name' => 'image',    
        'field_label_name' => 'Image',
        'recommended_width' => 700,
        'recommended_height' => 400,
    );
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    
    $config_data = array(
        'l' => $l,
        'wrapper_class' => 'api_admin_wrapper_blog_'.$select_data[0]['id'],
        'modal_class' => 'modal-lg',
        'title' => 'Edit', 
        'field_data' => $field_data,
        'selected_id' => $select_data[0]['id'],
        'table_name' => 'sma_blog',
        'table_id' => 'id',
        'upload_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/blog',
        'redirect' => base_url().'Blog',
        'datepicker' => 'yes',
        'delete' => 'yes',
        'title_delete' => '<div>Your will remove</div>'.$select_data[$i]['title_'.$l],
        'add' => 'yes',
        'title_add' => 'Add New Blog',        
    );
    $temp_admin = $this->api_admin->front_end_edit($config_data);
    $temp_display .= $temp_admin['display'];
    $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];

    $temp_display .= '
        </div>  
    ';
    $temp_display .=  '
        <div class="col-md-12">
    ';
    
    $temp_display_2 = '';
    $k2 = 0;
    for ($k=0;$k<count($temp_select_data_2);$k++) {
        if (is_int(strpos($select_data[0]['tag_id'],"".$temp_select_data_2[$k]['id'].""))) {
            if ($_GET['tag_id'] == $temp_select_data_2[$k]['id']) {
                $temp_display_2 .=  '
                    <div class="api_float_left">
                        <a href="'.base_url().'Blog?tag_id='.$temp_select_data_2[$k]['id'].'">
                            <div class="api_tag_box_selected">
                                '.$temp_select_data_2[$k]['title_'.$l].'
                            </div> 
                        </a>
                    </div>
                ';   
            } 
            else {
                $temp_display_2 .= '
                    <div class="api_float_left">
                        <a href="'.base_url().'Blog?tag_id='.$temp_select_data_2[$k]['id'].'">
                            <div class="api_tag_box_1">
                                '.$temp_select_data_2[$k]['title_'.$l].'
                            </div> 
                        </a>
                    </div>
                ';  
            }       
            $k2++;
        }
    }

    if ($k2 > 0) {
        $temp_display .= '
            <div class="api_float_left">
                <table width="100%" border="0">
                <tr>
                <td valign="middle" height="40">
        ';
        $temp_admin = $this->api_helper->api_lang_v2('TAGS');
        $temp_display .= $temp_admin['display'];
        $this->api_shop_setting[0]['api_temp_admin_script'] .= $temp_admin['script'];
    
        $temp_display .= '
                </td>
                </tr>
                </table>            
            </div>
        ';
    }
    $temp_display .= $temp_display_2;

    $temp_display .=  '
        </div>
    ';

    $temp_display .= '<div class="api_height_30 api_clear_both"></div>';

    include 'themes/default/shop/views/api/panel/api_ayum_blog_panel.php';
    include 'themes/default/shop/views/api/page_header/blog_detail_arrow.php';
    
    if ($this->session->userdata('user_id') != '') {
        $config_data = array(
            'wrapper_class' => 'api_ayum_blog_detail',
            'custom_html' => '<div class="api_height_30 visible-xs"></div>',
            'page_header' => $temp_display_page_header,
            'display' => $temp_display,
            'display_class' => 'col-md-9 api_padding_0',
            'panel' => $temp_display_panel,
            'panel_class' => 'col-md-3 api_padding_0',
            'type' => '',
        );
    }
    else {
        $config_data = array(
            'wrapper_class' => 'api_ayum_blog_detail',
            'custom_html' => '<div class="api_height_30 visible-xs"></div>',
            'page_header' => $temp_display_page_header,
            'display' => $temp_display,
            'display_class' => 'col-md-12 api_padding_0',
            'panel' => '',
            'panel_class' => 'col-md-3 api_padding_0',
            'type' => '',
        );
    }

    $temp = $this->api_display->template_display($config_data);
    echo $temp['display'];


    echo '<div class="api_height_10 api_clear_both"></div>';


?>
