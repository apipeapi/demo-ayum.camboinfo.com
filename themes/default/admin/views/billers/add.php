<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_biller'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open_multipart("billers/add", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("logo", "biller_logo"); ?>
                        <?php
                        $biller_logos[''] = '';
                        foreach ($logos as $key => $value) {
                            $biller_logos[$value] = $value;
                        }
                        echo form_dropdown('logo', $biller_logos, '', 'class="form-control select" id="biller_logo" required="required" '); ?>
                    </div>
                </div>

                <div class="col-md-6">
                    <div id="logo-con" class="text-center"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group company">
                        <?= lang("company", "company"); ?>
                        <?php echo form_input('company', '', 'class="form-control tip" id="company" data-bv-notempty="true"'); ?>
                    </div>
					<div class="form-group company_kh">
                        <?= lang("company_kh", "company_kh"); ?>
                        <?php echo form_input('company_kh', '', 'class="form-control tip" id="company_kh" '); ?>
                    </div>
                    <div class="form-group person">
                        <?= lang("name", "name"); ?>
                        <?php echo form_input('name', '', 'class="form-control tip" id="name" data-bv-notempty="true"'); ?>
                    </div>
					<div class="form-group person">
                        <?= lang("name_kh", "name_kh"); ?>
                        <?php echo form_input('name_kh', '', 'class="form-control tip" id="name_kh" '); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("vat_no", "vat_no"); ?>
                        <?php echo form_input('vat_no', '', 'class="form-control" id="vat_no"'); ?>
                    </div>
                    <!--<div class="form-group company">
                    <?= lang("contact_person", "contact_person"); ?>
                    <?php echo form_input('contact_person', '', 'class="form-control" id="contact_person" data-bv-notempty="true"'); ?>
                </div>-->
                    <div class="form-group">
                        <?= lang("email_address", "email_address"); ?>
                        <input type="email" name="email" class="form-control" required="required" id="email_address"/>
                    </div>
                    <div class="form-group">
                        <?= lang("phone", "phone"); ?>
                        <input type="tel" name="phone" class="form-control" required="required" id="phone"/>
                    </div>
                    <div class="form-group">
                        <?= lang("address", "address"); ?>
                        <?php echo form_input('address', '', 'class="form-control" id="address" required="required"'); ?>
                    </div>  
					<div class="form-group">
                        <?= lang("address_kh", "address_kh"); ?>
                        <?php echo form_input('address_kh', '', 'class="form-control" id="address_kh" '); ?>
                    </div>
                    <div class="form-group api_display_none">
                        <?= lang("City", "city"); ?>
                        <?php 
                            if($customer->city == '')
                                echo form_input('city', '-', 'class="form-control" id="city" required="required"'); 
                            else 
                                echo form_input('city', $biller->city, 'class="form-control" id="city" required="required"'); 
                        ?>
                    </div>
                    
					<div class="form-group api_display_none">
                        <?= lang("city_kh", "city_kh"); ?>
                        <?php 
                            if($customer->city_kh == '')   
                                echo form_input('city_kh', '-', 'class="form-control" id="city_kh"'); 
                            else 
                                echo form_input('city_kh', $biller->city_kh, 'class="form-control" id="city_kh"'); 
                        ?>
                    </div>
                    <div class="form-group api_display_none">
                        <?= lang("state", "state"); ?>
                        <?php
                        if ($Settings->indian_gst) {
                            $states = $this->gst->getIndianStates();
                            echo form_dropdown('state', $states, '', 'class="form-control select" id="state" required="required"');
                        } else {
                            echo form_input('state', '', 'class="form-control" id="state"');
                        }
                        ?>
                    </div>
					<div class="form-group api_display_none">
                        <?= lang("state_kh", "state_kh"); ?>
                        <?php echo form_input('state_kh', '', 'class="form-control" id="state_kh"'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("postal_code", "postal_code"); ?>
                        <?php echo form_input('postal_code', '', 'class="form-control" id="postal_code"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("country", "country"); ?>
                        <?php echo form_input('country', '', 'class="form-control" id="country"'); ?>
                    </div>
					<div class="form-group">
                        <?= lang("country_kh", "country_kh"); ?>
                        <?php echo form_input('country_kh', '', 'class="form-control" id="country_kh"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("bcf1", "cf1"); ?>
                        <?php echo form_input('cf1', '', 'class="form-control" id="cf1"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("bcf2", "cf2"); ?>
                        <?php echo form_input('cf2', '', 'class="form-control" id="cf2"'); ?>

                    </div>
                    <div class="form-group">
                        <?= lang("bcf3", "cf3"); ?>
                        <?php echo form_input('cf3', '', 'class="form-control" id="cf3"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("bcf4", "cf4"); ?>
                        <?php echo form_input('cf4', '', 'class="form-control" id="cf4"'); ?>

                    </div>
                    <div class="form-group">
                        <?= lang("bcf5", "cf5"); ?>
                        <?php echo form_input('cf5', '', 'class="form-control" id="cf5"'); ?>

                    </div>
                    <div class="form-group">
                        <?= lang("bcf6", "cf6"); ?>
                        <?php echo form_input('cf6', '', 'class="form-control" id="cf6"'); ?>
                    </div>
                </div>
                <div class="col-md-12">
                <div class="form-group">
                        <?= lang("City", "city"); ?>
                        <?php 
                            $config_data = array(
                                'none_label' => lang("Select a City"),
                                'table_name' => 'sma_city',
                                'space' => ' &rarr; ',
                                'strip_id' => '',        
                                'field_name' => 'title_en',
                                'condition' => 'order by title_en asc',
                                'translate' => 'yes',
                                'no_space' => 1,
                            );                        
                            $this->site->api_get_option_category($config_data);
                            $temp_option = $_SESSION['api_temp'];
                            for ($i=0;$i<count($temp_option);$i++) {                        
                                $temp = explode(':{api}:',$temp_option[$i]);
                                $temp_10 = '';
                                if ($temp[0] != '') {
                                    $config_data_2 = array(
                                        'id' => $temp[0],
                                        'table_name' => 'sma_city',
                                        'field_name' => 'title_en',
                                        'translate' => 'yes',
                                    );
                                    $_SESSION['api_temp'] = array();
                                    $this->site->api_get_category_arrow($config_data_2);          
                                    for ($i2 = count($_SESSION['api_temp']) - 1; ; $i2--) {
                                        if ($i2 == 0) {
                                            break;
                                        }
                                        $temp_arrow = '';
                                        if ($i2 > 1)
                                            $temp_arrow = ' &rarr; ';
                                        $temp_10 .= $_SESSION['api_temp'][$i2].$temp_arrow;
                                    }   
                                }
                                $tr_city[$temp[0]] = $temp_10.$temp[1];
                            }
                            echo form_dropdown('city_id', $tr_city, $biller->city_id, 'class="form-control"  required="required" ');
                        ?>
                    </div>
                    <div class="form-group">
                        <?= lang("invoice_footer", "invoice_footer"); ?>
                        <?php echo form_textarea('invoice_footer', '', 'class="form-control skip" id="invoice_footer" style="height:100px;"'); ?>
                    </div>
                </div>
            </div>


        </div>
        <div class="modal-footer">
            <?php echo form_submit('add_biller', lang('add_biller'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#biller_logo').change(function (event) {
            var biller_logo = $(this).val();
            $('#logo-con').html('<img src="<?=base_url('assets/uploads/logos')?>/' + biller_logo + '" alt="">');
        });
    });
</script>
<?= $modal_js ?>
