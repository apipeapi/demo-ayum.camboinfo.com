<?php
    $form_name = 'frm_list';
    if ($this->input->post('start_date')) {
        $v .= "&start_date=" . $this->input->post('start_date');
    }
    if ($this->input->post('end_date')) {
        $v .= "&end_date=" . $this->input->post('end_date');
    }
    if ($this->input->post('customer')) {
        $v .= "&customer=" . $this->input->post('customer');
    }
    if ($this->input->post('sale_person')) {
        $v .= "&sale_person=" . $this->input->post('sale_person');
    }
    if ($this->input->post('city')) {
        $v .= "&city=" . $this->input->post('city');
    }
    if ($this->input->post('payment_status')) {
        $v .= "&payment_status=" . $this->input->post('payment_status');
    }
?>  
<div class="box">
    <div class="box-header" style="">
        <h2 class="blue"><i class="fa-fw fa fa-heart"></i><?= lang('sales_report_for_each_customer'); ?> <?php
            if ($this->input->get('start_day')) {
                echo "From " . $this->input->get('start_day') . " to " . $this->input->get('end_day');
            }
            ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown"><a href="#" id="csv" class="tip" title="<?= lang('download_csv') ?>"><i
                            class="icon fa fa-file-o"></i></a></li>
            </ul>
        </div>
    </div>


    <div class="box-content">
        <div class="row">
            <div class="col-lg-12 api_padding_0">

                <div id="form">




<?php 
    echo form_open("admin/reports/sales_report_for_each_customer",'name="'.$form_name.'" id="'.$form_name.'"');
    echo '
        <input type="hidden" name="sort_by" id="sort_by" value="'.$_POST['sort_by'].'">
        <input type="hidden" name="order_by" id="order_by" value="'.$_POST['order_by'].'">
    '; 
?>

    <div class="col-md-3 " style="width:250px;">
        <div class="form-group">
            <?= lang("start_date", "start_date"); ?>
            <?php echo form_input('start_date', isset($_POST['start_date']) ? $_POST['start_date'] : 'January '.date('Y'), 'class="form-control" id="start_date"'); ?>
        </div>
    </div>
    <div class="col-md-3 " style="width:250px;" >
        <div class="form-group">
            <?= lang("end_date", "end_date"); ?>
            <?php
                if (isset($_POST['end_date'])) {
                    $temp = strtotime($_POST['start_date']);
                    $temp2 = strtotime($_POST['end_date']);        
                    if ($temp > $temp2) $_POST['end_date'] = $_POST['start_date'];                                
                }
            ?>
            <?php echo form_input('end_date', isset($_POST['end_date']) ? $_POST['end_date'] : 'December '.date('Y'), 'class="form-control" id="end_date"'); ?>
        </div>
    </div>

<?php
    echo '
    <div class="col-md-3 " style="width:350px;" >
        <div class="form-group">
            <label class="control-label" for="parent_id">
                '.lang("Customer").'
            </label>
            <div class="controls"> 
    ';
                $tr_2[''] = lang("All_Customers");
                for ($i=0;$i<count($customer);$i++) {
                    $tr_2[$customer[$i]['id']] = $customer[$i]['name'].' [Company: '.$customer[$i]['company'].']';
                }
                echo form_dropdown('customer', $tr_2, $_POST['customer'], 'id="" class="form-control"');
    echo '
            </div>
        </div>
    </div>
    ';
?>    
<?php
    echo '
    <div class="col-md-3 " style="width:350px;" >
        <div class="form-group">
            <label class="control-label" for="parent_id">
                '.lang("Customer_Group").'
            </label>
            <div class="controls"> 
    ';
                $tr_3[''] = lang("All_Customer_Groups");
                for ($i=0;$i<count($customer_group);$i++) {
                    $tr_3[$customer_group[$i]['id']] = $customer_group[$i]['name'];
                }
                echo form_dropdown('customer_group_id', $tr_3, $_POST['customer_group_id'], 'id="" class="form-control"');
    echo '
            </div>
        </div>
    </div>
    ';
?>    
<div class="api_clear_both"></div>
<?php
    echo '
        <div class="col-md-3 " style="width:250px;" >
            <div class="form-group">
                '.lang("sale_person", "sale_person").'
    ';
        $sp[0] = lang("all_sale_person");
        foreach ($sales_person as $sale_person) {
            $sp[$sale_person->id] = $sale_person->first_name.' '.$sale_person->last_name;
        }
        echo form_dropdown('sale_person', $sp, (isset($_POST['sale_person']) ? $_POST['sale_person'] : ''), 'class="form-control tip select" id="" style="width:100%;" ');

    
    echo '
            </div>
        </div>
    ';

    
    echo '
        <div class="col-md-3 " style="width:250px;" >
            <div class="form-group">
                '.lang("city", "city").'
    ';
    $tr_city[0] = lang("all_cities");
    foreach ($city as $temp) {
        if ($temp->city != '')
            $tr_city[$temp->city] = $temp->city;
    }
    echo form_dropdown('city', $tr_city, (isset($_POST['city']) ? $_POST['city'] : ''), 'data-placeholder="'.lang("all_cities").'" class="form-control"');
    echo '
            </div>
        </div>
    ';                        

    echo '
        <div class="col-md-3 " style="width:250px;">
            <div class="form-group">
                '.lang("payment_status", "payment_status").'
    ';
    $tr_p[0] = lang("all_payment_status");
    $tr_p[1] = 'Paid';
    $tr_p[2] = 'Due/Partial';
    $tr_p[3] = 'Pending';
    echo form_dropdown('payment_status', $tr_p, (isset($_POST['payment_status']) ? $_POST['payment_status'] : ''), 'data-placeholder="'.lang("all_payment_status").'" class="form-control"');
    echo '
            </div>
        </div>
    ';                        
?>
                                              
<div class="col-md-3 " style="width:250px;" >
    <div class="form-group">
        <div class="controls" style="margin-top: 30px"> 
            <?php echo form_submit('submit_report', $this->lang->line("submit"), 'class="btn btn-primary"'); ?> 
            <button class="btn btn-warning api_margin_left_15" id="csv_2">Export</button>
        </div>
    </div>
</div>   
<div class="api_clear_both"></div>
<?php echo form_close(); ?>




                </div>
</div>
                <!-- /form search -->
<div class="clearfix"></div>
<div class=" api_height_20"></div>
<div class="col-lg-12">

<?php
$temp_row_1 = '';
$temp_row_2 = '';
$temp_row_3 = '';
$temp_row_4 = '';
for ($y=$select_start_year;$y<=$select_end_year;$y++) {
    if (($y%2) != 0) { 
        $temp_bg = "
            background-color:#428BCA; border-color: #428BCA; border-top: 1px solid #428BCA;
        ";
        $temp_bg_footer = "
            background-color:#8fcbff; border-color: #8fcbff; border-top: 1px solid #8fcbff;
        ";
    }
    else { 
        $temp_bg = "
            background-color:#d9534f; border-color: #d9534f; border-top: 1px solid #d9534f;
        ";
        $temp_bg_footer = "
            background-color:#ffafad; border-color: #ffafad; border-top: 1px solid #ffafad;
        ";
    }
    
    if ($y == $select_start_year) $temp_colspan = 15; else $temp_colspan = 13;
    $temp_row_1 .= '
        <td colspan="'.$temp_colspan.'" style="'.$temp_bg.'">
            '.${'select_start_date_'.$y}.' &nbsp;&nbsp;<span class="fa fa-long-arrow-right"></span>&nbsp;&nbsp; '.${'select_end_date_'.$y}.'
        </td>    
    ';

    if ($y == $select_start_year) {
        $temp_sort_arrow = '<span class="fa fa-sort"></span>';
        if ($_POST['sort_by'] == 'name' && $_POST['order_by'] == 'asc') $temp_sort_arrow = '<span class="fa fa-sort-down"></span>'; 
        if ($_POST['sort_by'] == 'name' && $_POST['order_by'] == 'desc') $temp_sort_arrow = '<span class="fa fa-sort-up"></span>'; 
        $temp_row_2 .= '
            <th class="pointer" onclick="api_sort_by(\''.$form_name.'\',\'name\',\''.$_POST['order_by'].'\');" style="min-width:200px; max-width:200px;">'.lang("cal_customer").' '.$temp_sort_arrow.'</th>
            <th class="pointer" onclick="api_sort_by(\''.$form_name.'\',\'name\',\''.$_POST['order_by'].'\');" style="min-width:120px; max-width:120px;">'.lang("Branch").' '.$temp_sort_arrow.'</th>
        ';
        
        $temp_sort_arrow = '<span class="fa fa-sort"></span>';
        if ($_POST['sort_by'] == 'sale_person_fullname' && $_POST['order_by'] == 'asc') $temp_sort_arrow = '<span class="fa fa-sort-down"></span>'; 
        if ($_POST['sort_by'] == 'sale_person_fullname' && $_POST['order_by'] == 'desc') $temp_sort_arrow = '<span class="fa fa-sort-up"></span>'; 
        $temp_row_2 .= '
            <th class="pointer" onclick="api_sort_by(\''.$form_name.'\',\'sale_person_fullname\',\''.$_POST['order_by'].'\');" style="min-width:120px; max-width:120px;">'.lang("cal_person").' '.$temp_sort_arrow.'</th>
        ';
    }
    
    for ($j=1;$j<=12;$j++) {
        $dateObj = DateTime::createFromFormat('!m', $j);
        $monthName = $dateObj->format('F');        
        $temp_sort_arrow = '<span class="fa fa-sort"></span>';
        if ($_POST['sort_by'] == 'all_total_each_month_'.$y.'_'.$j && $_POST['order_by'] == 'asc') $temp_sort_arrow = '<span class="fa fa-sort-down"></span>'; 
        if ($_POST['sort_by'] == 'all_total_each_month_'.$y.'_'.$j && $_POST['order_by'] == 'desc') $temp_sort_arrow = '<span class="fa fa-sort-up"></span>'; 
        $temp_row_2 .= '
            <th class="pointer" onclick="api_sort_by(\''.$form_name.'\',\'all_total_each_month_'.$y.'_'.$j.'\',\''.$_POST['order_by'].'\');" style="white-space:nowrap;">'.$monthName.' '.$temp_sort_arrow.'</th>
        ';
    }
    

    for ($i=0;$i<count(${'select_data_'.$y});$i++) {
        for ($j=1;$j<=12;$j++) {
            ${$y.'_grand_total_paid_'.$j} += ${'select_data_'.$y}[$i]['total_paid_'.$j];
            ${$y.'_grand_total_due_'.$j} += ${'select_data_'.$y}[$i]['total_due_'.$j];
            ${$y.'_grand_total_pending_'.$j} += ${'select_data_'.$y}[$i]['total_pending_'.$j];
    
            ${$y.'_grand_paid_count_'.$j} += ${'select_data_'.$y}[$i]['total_paid_count_'.$j];
            ${$y.'_grand_due_count_'.$j} += ${'select_data_'.$y}[$i]['total_due_count_'.$j];
            ${$y.'_grand_pending_count_'.$j} += ${'select_data_'.$y}[$i]['total_pending_count_'.$j];
        }
        }
}


    $k = 1;    
    for ($i=0;$i<count(${'select_data_'.$select_start_year});$i++) {
        if (($k%2) != 0) $class_record = "odd"; else $class_record = "even";


        $temp_count_company_branch = 1;
        $temp_label = '';
        $temp_sale_person = 1;
        if (${'select_data_'.$select_start_year}[$i]['company_branch_'.${'select_data_'.$select_start_year}[$i]['id']][0]['id'] > 0) {
            $temp_count_company_branch = count(${'select_data_'.$select_start_year}[$i]['company_branch_'.${'select_data_'.$select_start_year}[$i]['id']]) + 2;
            $temp_label = lang('All_Branch_Total');
            $temp_sale_person = '';
        }

        $temp_row_3 .= '
            <tr class="'.$class_record.'">
        ';
        for ($y=$select_start_year;$y<=$select_end_year;$y++) {        
            if ($y == $select_start_year) {
                if ($temp_sale_person == 1) $temp_sale_person = ${'select_data_'.$y}[$i]['sale_person_fullname'];
                $temp_row_3 .= '
                    <td rowspan="'.$temp_count_company_branch.'" class="" style="min-width:200px; max-width:200px;">
                        '.${'select_data_'.$y}[$i]['name'].'
                    </td>
                    <td class="" style="min-width:120px; max-width:120px; font-weight:bold;">
                        '.$temp_label.'                         
                    </td>                        
                    <td class="" style="min-width:120px; max-width:120px;">
                        '.$temp_sale_person.'
                    </td>
                ';

            }
            for ($j=1;$j<=12;$j++) {
                $temp_row_3 .= '
                    <td align="right" style="width:1%; white-space:nowrap; vertical-align: top !important;">
                ';


                $temp_all_total_each_month = ${'select_data_'.$y}[$i]['all_total_each_month_'.$y.'_'.$j];

                $temp_all_total_each_month_count = ${'select_data_'.$y}[$i]['total_paid_count_'.$j] + ${'select_data_'.$y}[$i]['total_due_count_'.$j] + ${'select_data_'.$y}[$i]['total_pending_count_'.$j];
        
                if ($_POST['payment_status'] == 0)                
                if ($temp_all_total_each_month > 0)
                $temp_row_3 .= '
                    <div class="text-right" style="display:inline-block; font-size:100%; font-weight:bold; font-size:14px; color:#000;">
                        '.$this->sma->formatMoney($temp_all_total_each_month).' ('.$temp_all_total_each_month_count.')
                    </div>
                ';
                
                if ($_POST['payment_status'] == 1 || $_POST['payment_status'] == 0)                
                if (${'select_data_'.$y}[$i]['total_paid_'.$j] > 0)        
                    $temp_row_3 .= '
                        <div class="text-right" style="display:inline-block; font-size:100%; font-weight:normal; font-size:13px; color:#00a200;">
                            '.$this->sma->formatMoney(${'select_data_'.$y}[$i]['total_paid_'.$j]).' ('.${'select_data_'.$y}[$i]['total_paid_count_'.$j].')
                        </div>
                    ';
        
                $temp_row_3 .= '
                    <div style="height:3px;clear:both;"></div>
                ';
                            
                if ($_POST['payment_status'] == 2 || $_POST['payment_status'] == 0)                
                if (${'select_data_'.$y}[$i]['total_due_'.$j] > 0)
                    $temp_row_3 .= '
                        <div class="text-right" style="display:inline-block; font-size:100%; font-weight:normal; font-size:13px; color:#ff0700;">
                            '.$this->sma->formatMoney(${'select_data_'.$y}[$i]['total_due_'.$j]).' ('.${'select_data_'.$y}[$i]['total_due_count_'.$j].')
                        </div>
                    ';
                    
                if ($_POST['payment_status'] == 3 || $_POST['payment_status'] == 0)                
                if (${'select_data_'.$y}[$i]['total_pending_'.$j] > 0)                        
                    $temp_row_3 .= '
                        <div class="text-right" style="display:inline-block; font-size:100%; font-weight:normal; font-size:13px; color:#ff9600;">
                            '.$this->sma->formatMoney(${'select_data_'.$y}[$i]['total_pending_'.$j]).' ('.${'select_data_'.$y}[$i]['total_pending_count_'.$j].')
                        </div>
                    ';
        
                $temp_row_3 .= '
                    </td>
                ';
            }
        }
        $temp_row_3 .= '
            </tr>
        ';

        if (${'select_data_'.$select_start_year}[$i]['company_branch_'.${'select_data_'.$select_start_year}[$i]['id']][0]['id'] > 0) {
            $temp_row_3 .= '
                <tr  class="'.$class_record.'">
            ';
            for ($y=$select_start_year;$y<=$select_end_year;$y++) {
                $i_temp_company_branch = 0;
                if ($y == $select_start_year) {
                    $temp_row_3 .= '
                        <td class="" style="min-width:120px; max-width:120px;">
                            '.${'select_data_'.$y}[$i]['company_branch_0_'.${'select_data_'.$y}[$i]['id']][0]['title'].'
                        </td>                        
                        <td class="" style="min-width:120px; max-width:120px;">
                            '.${'select_data_'.$y}[$i]['company_branch_0_'.${'select_data_'.$y}[$i]['id']][0]['sale_person_fullname'].'
                        </td>
                    ';

                }          
                
                for ($j=1;$j<=12;$j++) {
                    $temp_row_3 .= '
                        <td align="right" style="width:1%; white-space:nowrap; vertical-align: top !important;">
                    ';

                    $temp_all_total_each_month = ${'select_data_'.$y}[$i]['company_branch_0_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_due_'.$j] + ${'select_data_'.$y}[$i]['company_branch_0_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_pending_'.$j] + ${'select_data_'.$y}[$i]['company_branch_0_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_paid_'.$j];

                    $temp_all_total_each_month_count = ${'select_data_'.$y}[$i]['company_branch_0_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_due_count_'.$j] + ${'select_data_'.$y}[$i]['company_branch_0_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_pending_count_'.$j] + ${'select_data_'.$y}[$i]['company_branch_0_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_paid_count_'.$j];
            
                    if ($_POST['payment_status'] == 0)                
                    if ($temp_all_total_each_month > 0)
                    $temp_row_3 .= '
                        <div class="text-right" style="display:inline-block; font-size:100%; font-weight:bold; font-size:14px; color:#000;">
                            '.$this->sma->formatMoney($temp_all_total_each_month).' ('.$temp_all_total_each_month_count.')
                        </div>
                    ';
                    


                    if ($_POST['payment_status'] == 1 || $_POST['payment_status'] == 0)                
                    if (${'select_data_'.$y}[$i]['company_branch_0_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_paid_'.$j] > 0)        
                        $temp_row_3 .= '
                            <div class="text-right" style="display:inline-block; font-size:100%; font-weight:normal; font-size:13px; color:#00a200;">
                                '.$this->sma->formatMoney(${'select_data_'.$y}[$i]['company_branch_0_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_paid_'.$j]).' ('.${'select_data_'.$y}[$i]['company_branch_0_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_paid_count_'.$j].')
                            </div>
                        ';
            
                    $temp_row_3 .= '
                        <div style="height:3px;clear:both;"></div>
                    ';
                                
                    if ($_POST['payment_status'] == 2 || $_POST['payment_status'] == 0)                
                    if (${'select_data_'.$y}[$i]['company_branch_0_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_due_'.$j] > 0)
                        $temp_row_3 .= '
                            <div class="text-right" style="display:inline-block; font-size:100%; font-weight:normal; font-size:13px; color:#ff0700;">
                                '.$this->sma->formatMoney(${'select_data_'.$y}[$i]['company_branch_0_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_due_'.$j]).' ('.${'select_data_'.$y}[$i]['company_branch_0_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_due_count_'.$j].')
                            </div>
                        ';
                        
                    if ($_POST['payment_status'] == 3 || $_POST['payment_status'] == 0)                
                    if (${'select_data_'.$y}[$i]['company_branch_0_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_pending_'.$j] > 0)                        
                        $temp_row_3 .= '
                            <div class="text-right" style="display:inline-block; font-size:100%; font-weight:normal; font-size:13px; color:#ff9600;">
                                '.$this->sma->formatMoney(${'select_data_'.$y}[$i]['company_branch_0_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_pending_'.$j]).' ('.${'select_data_'.$y}[$i]['company_branch_0_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_pending_count_'.$j].')
                            </div>
                        ';
            
                    $temp_row_3 .= '
                        </td>
                    ';
                }
            }                
            $temp_row_3 .= '
                </tr>
            ';
        }


        for ($i_temp_company_branch=0;$i_temp_company_branch<count(${'select_data_'.$select_start_year}[$i]['company_branch_'.${'select_data_'.$select_start_year}[$i]['id']]);$i_temp_company_branch++) {

            $temp_row_3 .= '
                <tr  class="'.$class_record.'">
            ';
            for ($y=$select_start_year;$y<=$select_end_year;$y++) {
            
                if ($y == $select_start_year) {
                    $temp_row_3 .= '
                        <td class="" style="min-width:120px; max-width:120px;">
                            '.${'select_data_'.$y}[$i]['company_branch_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['title'].'
                        </td>                        
                        <td class="" style="min-width:120px; max-width:120px;">
                            '.${'select_data_'.$y}[$i]['company_branch_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['sale_person_fullname'].'
                        </td>
                    ';

                }          
                
                for ($j=1;$j<=12;$j++) {
                    $temp_row_3 .= '
                        <td align="right" style="width:1%; white-space:nowrap; vertical-align: top !important;">
                    ';

                    $temp_all_total_each_month = ${'select_data_'.$y}[$i]['company_branch_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_due_'.$j] + ${'select_data_'.$y}[$i]['company_branch_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_pending_'.$j] + ${'select_data_'.$y}[$i]['company_branch_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_paid_'.$j];

                    $temp_all_total_each_month_count = ${'select_data_'.$y}[$i]['company_branch_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_due_count_'.$j] + ${'select_data_'.$y}[$i]['company_branch_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_pending_count_'.$j] + ${'select_data_'.$y}[$i]['company_branch_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_paid_count_'.$j];
            
                    if ($_POST['payment_status'] == 0)                
                    if ($temp_all_total_each_month > 0)
                    $temp_row_3 .= '
                        <div class="text-right" style="display:inline-block; font-size:100%; font-weight:bold; font-size:14px; color:#000;">
                            '.$this->sma->formatMoney($temp_all_total_each_month).' ('.$temp_all_total_each_month_count.')
                        </div>
                    ';
                    


                    if ($_POST['payment_status'] == 1 || $_POST['payment_status'] == 0)                
                    if (${'select_data_'.$y}[$i]['company_branch_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_paid_'.$j] > 0)        
                        $temp_row_3 .= '
                            <div class="text-right" style="display:inline-block; font-size:100%; font-weight:normal; font-size:13px; color:#00a200;">
                                '.$this->sma->formatMoney(${'select_data_'.$y}[$i]['company_branch_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_paid_'.$j]).' ('.${'select_data_'.$y}[$i]['company_branch_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_paid_count_'.$j].')
                            </div>
                        ';
            
                    $temp_row_3 .= '
                        <div style="height:3px;clear:both;"></div>
                    ';
                                
                    if ($_POST['payment_status'] == 2 || $_POST['payment_status'] == 0)                
                    if (${'select_data_'.$y}[$i]['company_branch_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_due_'.$j] > 0)
                        $temp_row_3 .= '
                            <div class="text-right" style="display:inline-block; font-size:100%; font-weight:normal; font-size:13px; color:#ff0700;">
                                '.$this->sma->formatMoney(${'select_data_'.$y}[$i]['company_branch_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_due_'.$j]).' ('.${'select_data_'.$y}[$i]['company_branch_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_due_count_'.$j].')
                            </div>
                        ';
                        
                    if ($_POST['payment_status'] == 3 || $_POST['payment_status'] == 0)                
                    if (${'select_data_'.$y}[$i]['company_branch_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_pending_'.$j] > 0)                        
                        $temp_row_3 .= '
                            <div class="text-right" style="display:inline-block; font-size:100%; font-weight:normal; font-size:13px; color:#ff9600;">
                                '.$this->sma->formatMoney(${'select_data_'.$y}[$i]['company_branch_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_pending_'.$j]).' ('.${'select_data_'.$y}[$i]['company_branch_'.${'select_data_'.$y}[$i]['id']][$i_temp_company_branch]['total_pending_count_'.$j].')
                            </div>
                        ';
            
                    $temp_row_3 .= '
                        </td>
                    ';
                }
            }                
            $temp_row_3 .= '
                </tr>
            ';
        }        
        
        $k++;
    }

for ($y=$select_start_year;$y<=$select_end_year;$y++) {
    if (($y%2) != 0) { 
        $temp_bg_footer = "
            background-color:#8fcbff; border-color: #8fcbff; border-top: 1px solid #8fcbff;
        ";
    }
    else { 
        $temp_bg_footer = "
            background-color:#ffafad; border-color: #ffafad; border-top: 1px solid #ffafad;
        ";
    }    
    if ($y == $select_start_year) {   
        $temp_row_4 .= '
            <th style="'.$temp_bg_footer.'"></th>
            <th style="'.$temp_bg_footer.'"></th>
            <th style="'.$temp_bg_footer.'"></th>
        ';
    }
    for ($j=1;$j<=12;$j++) {    
        $temp4 = ${$y.'_grand_total_paid_'.$j} + ${$y.'_grand_total_due_'.$j} + ${$y.'_grand_total_pending_'.$j};
        $temp5 = ${$y.'_grand_paid_count_'.$j} + ${$y.'_grand_due_count_'.$j} + ${$y.'_grand_pending_count_'.$j};
        $temp_row_4 .= '
                                <td style="width:1%; white-space:nowrap; text-align:right; vertical-align: top !important; '.$temp_bg_footer.'">
        ';
        if ($temp4 > 0) {
            if ($_POST['payment_status'] == 0)
            $temp_row_4 .= '                                        
                                    <div class="text-right label label-default" style="font-size:100%; font-weight:bold; font-size:14px;">
                                        <span style="color:#fff !important;">'.$this->sma->formatMoney($temp4).' ('.$temp5.')</span>
                                    </div>
                                    <div style="clear:both; height:5px;"></div>
            ';
            if ($_POST['payment_status'] == 1 || $_POST['payment_status'] == 0)
            if (${$y.'_grand_paid_count_'.$j} > 0)
            $temp_row_4 .= '                                        
                                    <div class="text-right label label-success" style="font-size:100%; font-weight:normal; font-size:13px;">
                                        <span style="color:#fff !important;">'.$this->sma->formatMoney(${$y.'_grand_total_paid_'.$j}).' ('.${$y.'_grand_paid_count_'.$j}.')</span>
                                    </div>
                                    <div style="clear:both; height:5px;"></div>
            ';
            if ($_POST['payment_status'] == 2 || $_POST['payment_status'] == 0)
            if (${$y.'_grand_due_count_'.$j} > 0)
            $temp_row_4 .= '
                                    <div class="text-right label label-danger" style="font-size:100%; font-weight:normal; font-size:13px;">
                                        <span style="color:#fff !important;">'.$this->sma->formatMoney(${$y.'_grand_total_due_'.$j}).' ('.${$y.'_grand_due_count_'.$j}.')</span>
                                    </div>
                                    <div style="clear:both; height:5px;"></div>
            ';
    
            if ($_POST['payment_status'] == 3 || $_POST['payment_status'] == 0)
            if (${$y.'_grand_pending_count_'.$j} > 0)
            $temp_row_4 .= '    
                                    <div class="text-right label label-warning" style="font-size:100%; font-weight:normal; font-size:13px;">
                                        <span style="color:#fff !important;">'.$this->sma->formatMoney(${$y.'_grand_total_pending_'.$j}).' ('.${$y.'_grand_pending_count_'.$j}.')</span>
                                    </div>
            ';
        }
        $temp_row_4 .= '    
                                </td>
        ';
    }
}

echo '
<div class="table-responsive">
    <table id="CusSlRData" class="table table-bordered table-hover table-striped table-condensed reports-table">
        <thead>
            <tr class="year_roller">
                '.$temp_row_1.'
            </tr>
            <tr>
                '.$temp_row_2.'
            </tr>
        </thead>
        <tbody>
            '.$temp_row_3.'
        </tbody>
        <tfoot class="dtFilter">
            <tr class="active year_roller">
                '.$temp_row_4.'
            </tr>        
        </tfoot>
    </table>
</div>
';




echo '
            </div>
        </div>
    </div>
</div>
';
?>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#csv').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('admin/reports/getSalesCustomerReport/csv/0/csv/?v=1'.$v)?>";
            return false;
        });
    });


    
</script>

<link href="<?php echo base_url(); ?>/assets/datepicker/bootstrap-datetimepicker.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>/assets/datepicker/moment-with-locales.js" type="text/javascript" ></script>
<script src="<?php echo base_url(); ?>/assets/datepicker/bootstrap-datetimepicker.js" type="text/javascript" ></script>
    
<script type="text/javascript">
    $(function() {
        $("#start_date").datetimepicker({
            format: "MMMM YYYY",
            viewMode: "years",
            toolbarPlacement: "top",
            allowInputToggle: true,
            icons: {
                time: "fa fa-time",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: "fa fa-chevron-left",
                next: "fa fa-chevron-right",
                today: "fa fa-screenshot",
                clear: "fa fa-trash",
                close: "fa fa-remove"
            }
        });
        $("#end_date").datetimepicker({
            format: "MMMM YYYY",
            viewMode: "years",
            toolbarPlacement: "top",
            allowInputToggle: true,
            icons: {
                time: "fa fa-time",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: "fa fa-chevron-left",
                next: "fa fa-chevron-right",
                today: "fa fa-screenshot",
                clear: "fa fa-trash",
                close: "fa fa-remove"
            }
        });
    });
    
    function api_sort_by(form_name, sort_by, order_by){
        var temp = order_by;
        document.getElementById('sort_by').value = sort_by; 
        if (temp == '' || temp == 'asc') temp = 'desc'; else temp = 'asc';
        document.getElementById('order_by').value = temp;
        document.forms[form_name].submit();    
    }
        
</script> 



