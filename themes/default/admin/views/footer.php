<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="clearfix"></div>
<?= '</div></div></div></td></tr></table></div></div>'; ?>
<div class="clearfix"></div>
<footer>
<a href="#" id="toTop" class="blue" style="position: fixed; bottom: 30px; right: 30px; font-size: 30px; display: none;">
    <i class="fa fa-chevron-circle-up"></i>
</a>

    <p style="text-align:center;">&copy; <?= date('Y') . " " . $Settings->site_name; ?> (<a href="<?= base_url('documentation.pdf'); ?>" target="_blank">v<?= $Settings->version; ?></a>
        ) <?php if ($_SERVER["REMOTE_ADDR"] == '127.0.0.1') {
    echo ' - Page rendered in <strong>{elapsed_time}</strong> seconds';
} ?></p>
</footer>
<?= '</div>'; ?>
<div class="modal fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
<div class="modal fade in" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true
"></div>
<div class="modal fade in" id="myModal_static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_static" aria-hidden="true" data-keyboard="false" data-backdrop="static"></div>
<div id="modal-loading" style="display: none;">
    <div class="blackbg"></div>
    <div class="loader"></div>
</div>
<div id="ajaxCall"><i class="fa fa-spinner fa-pulse"></i></div>
<?php unset($Settings->setting_id, $Settings->smtp_user, $Settings->smtp_pass, $Settings->smtp_port, $Settings->update, $Settings->reg_ver, $Settings->allow_reg, $Settings->default_email, $Settings->mmode, $Settings->timezone, $Settings->restrict_calendar, $Settings->restrict_user, $Settings->auto_reg, $Settings->reg_notification, $Settings->protocol, $Settings->mailpath, $Settings->smtp_crypto, $Settings->corn, $Settings->customer_group, $Settings->envato_username, $Settings->purchase_code); ?>
<script type="text/javascript">
var dt_lang = <?=$dt_lang?>, dp_lang = <?=$dp_lang?>, site = <?=json_encode(array('url' => base_url(), 'base_url' => admin_url(), 'assets' => $assets, 'settings' => $Settings, 'dateFormats' => $dateFormats))?>;
var lang = {paid: '<?=lang('paid');?>', pending: '<?=lang('pending');?>', completed: '<?=lang('completed');?>', ordered: '<?=lang('ordered');?>', received: '<?=lang('received');?>', partial: '<?=lang('partial');?>', sent: '<?=lang('sent');?>', r_u_sure: '<?=lang('r_u_sure');?>', due: '<?=lang('due');?>', returned: '<?=lang('returned');?>', transferring: '<?=lang('transferring');?>', active: '<?=lang('active');?>', inactive: '<?=lang('inactive');?>', unexpected_value: '<?=lang('unexpected_value');?>', select_above: '<?=lang('select_above');?>', download: '<?=lang('download');?>'};
</script>
<?php
$s2_lang_file = read_file('./assets/config_dumps/s2_lang.js');
foreach (lang('select2_lang') as $s2_key => $s2_line) {
    $s2_data[$s2_key] = str_replace(array('{', '}'), array('"+', '+"'), $s2_line);
}
$s2_file_date = $this->parser->parse_string($s2_lang_file, $s2_data, true);
?>

<?php
    echo '<script src="'.base_url().'assets/api/js/public.js"></script>';
?>
<script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.dtFilter.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/select2.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/bootstrapValidator.min.js"></script>

<?php
if (is_int(strpos($_SERVER['REQUEST_URI'], "sales_report_for_each_customer"))) {
    echo '
<script type="text/javascript" src="'.$assets.'js/custom_no_datetime.js?v='.time().'"></script>
';
} else {
    echo '
<script type="text/javascript" src="'.$assets.'js/custom.js?v='.time().'"></script>
';
}
?>
<script type="text/javascript" src="<?= $assets ?>js/jquery.calculator.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/core.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/perfect-scrollbar.min.js"></script>
<?= ($m == 'purchases' && ($v == 'add' || $v == 'edit' || $v == 'purchase_by_csv')) ? '<script type="text/javascript" src="' . $assets . 'js/purchases.js"></script>' : ''; ?>
<?= ($m == 'transfers' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/transfers.js"></script>' : ''; ?>
<?= ($m == 'sales' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/sales.js"></script>' : ''; ?>
<?= ($m == 'returns' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/returns.js"></script>' : ''; ?>
<?= ($m == 'quotes' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/quotes.js"></script>' : ''; ?>
<?= ($m == 'products' && ($v == 'add_adjustment' || $v == 'edit_adjustment')) ? '<script type="text/javascript" src="' . $assets . 'js/adjustments.js"></script>' : ''; ?>
<?= ($m == 'consignment' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/consignment.js"></script>' : ''; ?>

<script type="text/javascript" charset="UTF-8">
    $('.mm_<?=$m?>').addClass('active');
    $('#<?= $m.'_'.$v ?>').addClass('active');
<?php
    if ($m == 'reports' && $v == 'sales_report_for_each_customer') {
        echo '$(".mm_'.$m.'").find("ul").first().slideToggle();';
    }
?>

    var oTable = '', r_u_sure = "<?=lang('r_u_sure')?>";
    <?=$s2_file_date?>
    $.extend(true, $.fn.dataTable.defaults, {"oLanguage":<?=$dt_lang?>});
<?php
    if (!is_int(strpos($_SERVER['REQUEST_URI'], "sales_report_for_each_customer")) && !is_int(strpos($_SERVER['REQUEST_URI'], "reports/sales"))) {
        echo '$.fn.datetimepicker.dates["sma"] = '.$dp_lang;
    }
?>

    $(window).load(function () {
        $('.mm_<?=$m?>').addClass('active');
        $('.mm_<?=$m?>').find("ul").first().slideToggle();
<?php
    if ($m == 'purchases' && $_GET['mode'] == 'consignment') {
        echo '$("#'.$m.'_'.$v.'").removeClass("active");';
        if ($v != 'add') {
            echo '$("#purchases_consignment").addClass("active");';
        } else {
            echo '$("#purchases_consignment_add").addClass("active");';
        }
    }

    if ($m == 'reports' && $_GET['mode'] == 'consignment') {
        echo '$("#'.$m.'_'.$v.'").removeClass("active");';
        echo '$("#reports_purchases_consignment").addClass("active");';
    }

    if ($m == 'sales' && $_GET['mode'] == 'sample') {
        echo '
            $("#'.$m.'_'.$v.'").removeClass("active");
            $("#sales_sample").addClass("active");
        ';
    }
    if ($m == 'auth') {
        if ($_GET['group'] == 'customer') {
            echo '$("#'.$m.'_'.$v.'").removeClass("active");';
            echo '$("#auth_users_customer").addClass("active");';
        }
    }
?>        
        $('.mm_<?=$m?> a .chevron').removeClass("closed").addClass("opened");
    });

    $('body').delegate("[trigger='update_addons']",'click',function(){
        const id = $(this).parents('tr').attr('id')
        const value = $(this).attr('status')
        const target = $(this).attr('data-target')
        var result = $.ajax({
            async: false,
            global: false,
            url: "admin/update/addons/"+target,
            data:{
                id:id,
                air:value
            },
            error: (response, status, e)=>alert(e)
        }).responseText
        console.log(result)
        $(this).replaceWith(result)
    })
</script>
<?= (DEMO) ? '<script src="'.$assets.'js/ppp_ad.min.js"></script>' : ''; ?>

<?php
    echo '
        <script src="'.base_url().'assets/api/js/public.js"></script>
    ';

    //include('themes/default/admin/views/sub_page/auto_update_kh_currencies.php');
    include('themes/default/admin/views/sub_page/api_reminder.php');
    include('themes/default/shop/views/sub_page/api_modal_2.php');

?>
</body>
</html>






