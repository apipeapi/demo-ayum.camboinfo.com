<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo $modal_title; ?></h4>
        </div>
        <?php 
        $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'name' => 'frm_add_sale_consignment', 'id' => 'frm_add_sale_consignment');
        $temp_2 = '';
        if ($_GET['return'] != '') $temp_2 = '?return='.$_GET['return'];
        echo admin_form_open_multipart("consignment/add_sale_consignment".$temp_2, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="row">
                <?php if ($Owner || $Admin) { ?>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <?= lang("Consignment_Reference_No", "Consignment_Reference_No").' *'; ?>
                            <?php
                                if ($select_data[0]['reference_no'] != '') { 
                                    $temp_disable = 'disabled="disabled"';
                                    echo '<input type="hidden" name="reference_no" id="reference_no" value="'.$select_data[0]['reference_no'].'" />'; 
                                }
                                else
                                    $temp_disable = '';
                                echo form_input('reference_no', $select_data[0]['reference_no'], 'class="form-control" id="consignment_reference_no" '.$temp_disable.' required="required" placeholder="' . lang("Consignment_Reference_No") . '"');
                                                                                   
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <?= lang("Create_Date","Create_Date"); ?>
                            <?php
                                $temp = date('d/m/Y H:i');
                                echo form_input('date', $temp, 'class="form-control datetime" id="date" required="required"'); 
                            ?>
                        </div>
                    </div>
                <?php } ?>

                                

            </div>
            <div class="clearfix"></div>
            <div class="table-responsive" id="api_get_table_sale_items">
                <?php
                    if ($select_data[0]['id'] != '')
                        echo $get_consignment_table;
                
                ?>
            </div>
        </div>
        <div class="modal-footer">
            <?php                
                echo form_submit('frm_add_sale_consignment', $modal_submit_label, 'class="btn btn-primary"'); 
            ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>


<?php
if ($select_data[0]['reference_no'] == '')
echo '
<script>
    $("#consignment_reference_no").autocomplete({
        source: function (request, response) {          
            $.ajax({
                type: "get",
                url: "'.admin_url('consignment/api_get_table_sale_items').'",
                dataType: "json",
                data: {
                    reference_no: $("#consignment_reference_no").val(),
                },
                success: function (data) {
                    $(this).removeClass("ui-autocomplete-loading");
                    response(data);
                }
            });
        },
        minLength: 1,
        autoFocus: false,
        delay: 250,
        response: function (event, ui) {
            $("#api_get_table_sale_items").html(ui.content[0].api_result);
        },
    });
</script>
';

?>
