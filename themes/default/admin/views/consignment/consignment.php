<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
    $(document).ready(function () {
        if (localStorage.getItem('remove_slls')) {
            if (localStorage.getItem('slitems')) {
                localStorage.removeItem('slitems');
            }
            if (localStorage.getItem('sldiscount')) {
                localStorage.removeItem('sldiscount');
            }
            if (localStorage.getItem('sltax2')) {
                localStorage.removeItem('sltax2');
            }
            if (localStorage.getItem('slref')) {
                localStorage.removeItem('slref');
            }
            if (localStorage.getItem('slshipping')) {
                localStorage.removeItem('slshipping');
            }
            if (localStorage.getItem('slwarehouse')) {
                localStorage.removeItem('slwarehouse');
            }
            if (localStorage.getItem('slnote')) {
                localStorage.removeItem('slnote');
            }
            if (localStorage.getItem('slinnote')) {
                localStorage.removeItem('slinnote');
            }
            if (localStorage.getItem('slcustomer')) {
                localStorage.removeItem('slcustomer');
            }
            if (localStorage.getItem('slbiller')) {
                localStorage.removeItem('slbiller');
            }
            if (localStorage.getItem('slcurrency')) {
                localStorage.removeItem('slcurrency');
            }
            if (localStorage.getItem('sldate')) {
                localStorage.removeItem('sldate');
            }
            if (localStorage.getItem('slsale_status')) {
                localStorage.removeItem('slsale_status');
            }
            if (localStorage.getItem('slpayment_status')) {
                localStorage.removeItem('slpayment_status');
            }
            if (localStorage.getItem('paid_by')) {
                localStorage.removeItem('paid_by');
            }
            if (localStorage.getItem('amount_1')) {
                localStorage.removeItem('amount_1');
            }
            if (localStorage.getItem('paid_by_1')) {
                localStorage.removeItem('paid_by_1');
            }
            if (localStorage.getItem('pcc_holder_1')) {
                localStorage.removeItem('pcc_holder_1');
            }
            if (localStorage.getItem('pcc_type_1')) {
                localStorage.removeItem('pcc_type_1');
            }
            if (localStorage.getItem('pcc_month_1')) {
                localStorage.removeItem('pcc_month_1');
            }
            if (localStorage.getItem('pcc_year_1')) {
                localStorage.removeItem('pcc_year_1');
            }
            if (localStorage.getItem('pcc_no_1')) {
                localStorage.removeItem('pcc_no_1');
            }
            if (localStorage.getItem('cheque_no_1')) {
                localStorage.removeItem('cheque_no_1');
            }
            if (localStorage.getItem('slpayment_term')) {
                localStorage.removeItem('slpayment_term');
            }
            localStorage.removeItem('remove_slls');
        }

        <?php if ($this->session->userdata('remove_slls')) {?>
        if (localStorage.getItem('slitems')) {
            localStorage.removeItem('slitems');
        }
        if (localStorage.getItem('sldiscount')) {
            localStorage.removeItem('sldiscount');
        }
        if (localStorage.getItem('sltax2')) {
            localStorage.removeItem('sltax2');
        }
        if (localStorage.getItem('slref')) {
            localStorage.removeItem('slref');
        }
        if (localStorage.getItem('slshipping')) {
            localStorage.removeItem('slshipping');
        }
        if (localStorage.getItem('slwarehouse')) {
            localStorage.removeItem('slwarehouse');
        }
        if (localStorage.getItem('slnote')) {
            localStorage.removeItem('slnote');
        }
        if (localStorage.getItem('slinnote')) {
            localStorage.removeItem('slinnote');
        }
        if (localStorage.getItem('slcustomer')) {
            localStorage.removeItem('slcustomer');
        }
        if (localStorage.getItem('slbiller')) {
            localStorage.removeItem('slbiller');
        }
        if (localStorage.getItem('slcurrency')) {
            localStorage.removeItem('slcurrency');
        }
        if (localStorage.getItem('sldate')) {
            localStorage.removeItem('sldate');
        }
        if (localStorage.getItem('slsale_status')) {
            localStorage.removeItem('slsale_status');
        }
        if (localStorage.getItem('slpayment_status')) {
            localStorage.removeItem('slpayment_status');
        }
        if (localStorage.getItem('paid_by')) {
            localStorage.removeItem('paid_by');
        }
        if (localStorage.getItem('amount_1')) {
            localStorage.removeItem('amount_1');
        }
        if (localStorage.getItem('paid_by_1')) {
            localStorage.removeItem('paid_by_1');
        }
        if (localStorage.getItem('pcc_holder_1')) {
            localStorage.removeItem('pcc_holder_1');
        }
        if (localStorage.getItem('pcc_type_1')) {
            localStorage.removeItem('pcc_type_1');
        }
        if (localStorage.getItem('pcc_month_1')) {
            localStorage.removeItem('pcc_month_1');
        }
        if (localStorage.getItem('pcc_year_1')) {
            localStorage.removeItem('pcc_year_1');
        }
        if (localStorage.getItem('pcc_no_1')) {
            localStorage.removeItem('pcc_no_1');
        }
        if (localStorage.getItem('cheque_no_1')) {
            localStorage.removeItem('cheque_no_1');
        }
        if (localStorage.getItem('slpayment_term')) {
            localStorage.removeItem('slpayment_term');
        }
        <?php $this->sma->unset_data('remove_slls');}
        ?>

        $(document).on('click', '.sledit', function (e) {
            if (localStorage.getItem('slitems')) {
                e.preventDefault();
                var href = $(this).attr('href');
                bootbox.confirm("<?=lang('you_will_loss_sale_data')?>", function (result) {
                    if (result) {
                        window.location.href = href;
                    }
                });
            }
        });
        $(document).on('click', '.slduplicate', function (e) {
            if (localStorage.getItem('slitems')) {
                e.preventDefault();
                var href = $(this).attr('href');
                bootbox.confirm("<?=lang('you_will_loss_sale_data')?>", function (result) {
                    if (result) {
                        window.location.href = href;
                    }
                });
            }
        });

    });

</script>

<?php 
    $wa = $warehouse_id != 0 ? '/'.$warehouse_id : '';
    
    $spage = $show_data != '' ? $show_data : 1;

    if ($Owner && $GP['bulk_actions'])
        echo admin_form_open('sales/consignment_actions', 'id="action-form"');
    else
        echo admin_form_open('sales/consignment'.$wa.'?page='.$spage, 'id="action-form"');
?>

<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-truck"></i>
            <?=lang('list_consignment') . ' (' . ($warehouse_id ? $warehouse->name : lang('all_warehouses')) . ')';?>
        </h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li id="sales_consignment_add">
                            <a class="submenu" href="<?= admin_url('sales/add_consignment'); ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus-circle"></i>
                                <span class="text"> <?= lang('add_consignment'); ?></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="combine" data-action="combine">
                                <i class="fa fa-file-pdf-o"></i> <?=lang('combine_to_pdf')?>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#" class="bpo" title="<b><?=lang("delete")?></b>" 
                            data-content="
                                <p><?=lang('r_u_sure')?></p>
                                <button type='button' class='btn btn-danger' onclick='bulk_actions_delete();' data-action='delete'><?=lang('i_m_sure')?>
                                    </a> <button class='btn bpo-close'><?=lang('no')?>
                                </button>
                            " data-html="true" data-placement="left">
                                <i class="fa fa-trash-o"></i> <?=lang('delete')?>
                            </a>
                        </li>                        
                    </ul>
                </li>
                <?php if (!empty($warehouses)) {
                    ?>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-building-o tip" data-placement="left" title="<?=lang("warehouses")?>"></i></a>
                        <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                            <li><a href="<?=admin_url('sales/consignment')?>"><i class="fa fa-building-o"></i> <?=lang('all_warehouses')?></a></li>
                            <li class="divider"></li>
                            <?php
                                foreach ($warehouses as $warehouse) {
                                        echo '<li><a href="' . admin_url('sales/consignment/' . $warehouse->id) . '"><i class="fa fa-building"></i>' . $warehouse->name . '</a></li>';
                                    }
                                ?>
                        </ul>
                    </li>
                <?php }
                ?>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?=lang('list_results');?></p>

                <div class="table-responsive">
                    <div id="SLData_wrapper" class="dataTables_wrapper form-inline" role="grid">
                        <div class="row">
                            <div class="sale-header">
                                <div class="col-md-6 text-left">
                                    <div class="short-result">
                                        <label>
                                            Show
                                        </label>
                                        <select id="sel_id" name="per_page" onchange="this.form.submit()">
                                            <?php
                                                $per_page_arr = array(
                                                    '10' => '10',
                                                    '25' => '25',
                                                    '50' => '50',
                                                    '100' => '100',
                                                    '200' => '200',
                                                    'All' => $total_rows_sale
                                                );
                                            
                                                foreach($per_page_arr as $key =>$value){
                                                    $select = $value == $per_page?'selected':'';
                                                    echo '<option value="'.$value.'" '.$select.'>';
                                                            echo $key;
                                                    echo '</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 text-right">
                                    <div class="short-result">
                                        <div id="SLData_filter" class="show-data-search dataTables_filter">
                                            <label>
                                                Search
                                                <input class="input-xs" value="<?php echo $this->input->post('search');?>" type="text" name="search" id="mysearch" placeholder="search"/>
                                                <input style="height: 34px;" class="btn btn-primary" type="submit" name="submit_search" value="Submit">
                                            </label>
                                            <div id="result"></div>
                                        </div>
                                    </div>    
                                </div>	
                                <div class="clearfix"></div>
                             </div>
                        </div>     
                    </div> 
                    
                    <?php
                    $row_headers = array(
                        'checkbox'      => [
                                            'label' => '<input class="checkbox checkft" type="checkbox" name="check"/>', 
                                            'sort_fn' => false,
                                            'style' => 'min-width:30px; width: 30px; text-align: center;'
                                        ],
                        'id'          => ['label' => lang("numbering"), 'style' => ''],
                        'date'          => ['label' => lang("date"), 'style' => ''],
                        'reference_no'  => ['label' => lang("reference_no"), 'style' => ''],
                        'biller'        => ['label' => lang("biller"), 'style' => ''],
                        'customer'      => ['label' => lang("customer"), 'style' => ''],
                        'sale_status'   => ['label' => lang("sale_status"), 'style' => ''],
                        'grand_total'   => ['label' => lang("total_amount"), 'style' => ''],
                        'payment_status' => ['label' => lang("payment_status"), 'style' => ''],
                        'chain'         => [
                                                'label'     => '<i class="fa fa-chain"></i>', 
                                                'style'     => 'min-width:30px; width: 30px; text-align: center;',
                                                'sort_fn'   => false
                                            ],
                        // 'null'          => ['label' => '', 'style' => '', 'sort_fn' => false],
                        'action'        => [
                                            'label' => lang("actions"), 
                                            'style' => 'width:80px; text-align:center;', 
                                            'sort_fn' => false
                                        ],
                    );
                    ?>
                    <table id="SLData" class="table table-bordered table-hover table-striped" cellpadding="0" cellspacing="0" border="0">
                        <thead>
                            <tr>
                                <?php foreach ($row_headers as $key => $row) :?>
                                    <?php
                                   
                                    $th_class="class='pointer'";
                                    $e_click = true;
                                    if(isset($row['sort_fn']) && $row['sort_fn'] == false) {
                                        $th_class="";
                                        $e_click = false;
                                    }
                                    $style = isset($row['style']) && $row['style'] != '' ? 'style="'.$row['style'].'"' : '';
                                    ?>
                                    <th <?=$th_class?> <?= $style ?> >
                                        <?php if($e_click ) :?>
                                            <?php if ($sort_by == $key && $sort_order == "ASC") : ?>
                                                    <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort"  aria-hidden="true">
                                                    <?php echo $row['label']; ?>
                                                        <span class="fa fa-sort-down"></span>
                                                    </label>
                                            <?php elseif ($sort_by == $key and $sort_order=="DESC") : ?>	
                                                <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'ASC')" id="fa_sort_up"  aria-hidden="true">
                                                    <?php echo $row['label']; ?>
                                                    <span class="fa fa-sort-up"></span>
                                                </label>
                                            <?php else : ?>	
                                                <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort_down" aria-hidden="true">
                                                    <?php echo $row['label']; ?>
                                                    <span class="fa fa-sort"></span>
                                                </label>
                                            <?php endif; ?>	
                                        <?php else : ?>
                                            <label class="font-normal" aria-hidden="true">
                                                <?php echo $row['label']; ?>
                                            </label>
                                        <?php endif; ?>	
                                    </th>
                                <?php endforeach; ?>
                               
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($sl_sale) : ?>
                            <?php
                            // echo '<pre>';
                            // print_r($sl_sale);exit();
                            $total_grand = 0;
                            $total_paid = 0;
                            $total_balance = 0;

                            ?>
                                <?php foreach($sl_sale as $key => $value) : ?>
                                    <?php
                                   
                                    $grand_total = $value->grand_total;
                                    $paid = $value->paid;
                                    
                                    $balance = (float) $grand_total - (float) $paid;

                                    $sale_status = $value->sale_status;
                                    $payment_status = $value->payment_status;

                                    $total_grand += $grand_total;
                                    $total_paid += $paid;
                                    $total_balance += $balance;
                                    
                                    ?>
                                    
                                    <tr id='<?php echo $value->id;?>' class="invoice_link_consignment">
                                        <td style="min-width:30px; width: 30px; text-align: center;">
                                            <div class="text-center">
                                                <input class="checkbox multi-select" type="checkbox" 
                                                    name="val[]" value="<?php echo $value->id;?>" />
                                            </div>
                                        </td>
                                        <td><?php echo 'CS'.str_pad($value->id, 5, '0', STR_PAD_LEFT); ?></td>
                                        <td><?php echo date('d/m/Y H:i:s', strtotime($value->date));?></td>
                                        <td><?php echo $value->reference_no; ?></td>
                                        <td><?php echo $value->biller;?></td>
                                        <td><?php echo htmlspecialchars($value->customer);?></td>
                                        <td>
                                            <div class="text-center">
                                            <?php 
                                                
                                                if($sale_status == 'pending') {
                                                    echo '<span class="label label-warning">'.lang('Consignment').'</span>';

                                                } else if($sale_status == 'completed' || $sale_status == 'paid' || 
                                                        $sale_status == 'sent' || $sale_status == 'received') {
                                                    echo '<span class="label label-success">'.$sale_status.'</span>';

                                                } else if($sale_status == 'partial' || $sale_status == 'transferring' || 
                                                        $sale_status == 'ordered') {
                                                    echo '<span class="label label-info">'.$sale_status.'</span>';

                                                } else if($sale_status == 'due' || $sale_status == 'returned') {
                                                    echo '<span class="label label-danger">'.$sale_status.'</span>';

                                                } else {
                                                    echo '<span class="label label-default">'.$sale_status.'</span>';
                                                }
                                            ?>
                                            </div>
                                        </td>
                                        <td><?php echo $this->sma->formatMoney($grand_total); ?></td>
                                        <td>
                                            <div class="text-center">
                                            <?php 

                                                if($payment_status == 'pending') {
                                                echo '<span class="label label-warning">'.$payment_status.'</span>';
                                                } else if($payment_status == 'completed' || $payment_status == 'paid' || $payment_status == 'sent' || $payment_status == 'received') {
                                                echo '<span class="label label-success">'.$payment_status.'</span>';
                                                } else if($payment_status == 'partial' || $payment_status == 'transferring' || $payment_status == 'ordered') {
                                                echo '<span class="label label-info">'.$payment_status.'</span>';
                                                } else if($payment_status == 'due' || $payment_status == 'returned') {
                                                echo '<span class="label label-danger">'.$payment_status.'</span>';
                                                } else {
                                                echo '<span class="label label-default">'.$payment_status.'</span>';
                                            }
                                            ?>
                                            </div>
                                        </td>
                                        <td></td>
                                        <td style="width:80px; text-align:center;">
                                            <?php
                                            $detail_link = anchor('admin/sales/view/'.$value->id, '<i class="fa fa-file-text-o"></i> ' . lang('sale_details'));
                                            $duplicate_link = anchor('admin/sales/add?sale_id='.$value->id, '<i class="fa fa-plus-circle"></i> ' . lang('duplicate_sale'));
                                            $payments_link = anchor('admin/sales/payments/'.$value->id, '<i class="fa fa-money"></i> ' . lang('view_payments'), 'data-toggle="modal" data-target="#myModal"');
                                            $add_payment_link = anchor('admin/sales/add_payment/'.$value->id, '<i class="fa fa-money"></i> ' . lang('add_payment'), 'data-toggle="modal" data-target="#myModal"');
                                            $packagink_link = anchor('admin/sales/packaging/'.$value->id, '<i class="fa fa-archive"></i> ' . lang('packaging'), 'data-toggle="modal" data-target="#myModal"');
                                            $add_delivery_link = anchor('admin/sales/add_delivery/'.$value->id, '<i class="fa fa-truck"></i> ' . lang('add_delivery'), 'data-toggle="modal" data-target="#myModal"');
                                            $email_link = anchor('admin/sales/email/'.$value->id, 
                                                        '<i class="fa fa-envelope"></i> ' . lang('email_sale'), 'data-toggle="modal" data-target="#myModal"');
                                            $edit_link = anchor('admin/sales/add_consignment/'.$value->id, '<i class="fa fa-edit"></i> ' . lang('edit'), 'class="sledit" data-toggle="modal" data-target="#myModal"');
                                            $pdf_link = anchor('admin/sales/pdf/'.$value->id, '<i class="fa fa-file-pdf-o"></i> ' . lang('download_pdf'));
                                            $return_link = anchor('admin/sales/return_sale/'.$value->id, 
                                                        '<i class="fa fa-angle-double-left"></i> ' . lang('return_sale'));
                                         
                                            ?>
                                            <div class="text-center">
                                                <div class="btn-group text-left">
                                                    <button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" 
                                                            data-toggle="dropdown">
                                                        <?=lang('actions')?>
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                        <li id="edit_<?php echo $value->id; ?>"><?= $edit_link ?></li>
                                                        <li><?= $pdf_link ?></li>
                                                        <li>
                                                            <a id="delete_<?php echo $value->id; ?>" href='#' class='po' 
                                                                title='<?= lang("delete")?>' 
                                                                data-content="
                                                                    <p><?=lang('r_u_sure')?></p>
                                                                    <a class='btn btn-danger' href='<?= admin_url('sales/delete_consignment/'.$value->id)?>'>
                                                                        <?=lang('i_m_sure')?>
                                                                    </a> 
                                                                    <button class='btn po-close'><?=lang('no')?></button>" 
                                                                    data-placement="left">
                                                                <i class='fa fa-trash-o'></i>
                                                                <?= lang('delete')?>
                                                            </a>
                                                       
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else :?>
                                <tr>
                                    <td colspan="<?php echo count($row_headers); ?>" class="dataTables_empty">
                                        <?= lang("loading_data"); ?>
                                    </td>
                                </tr>
                            <?php endif;?>
                        </tbody>
                        <tfoot class="dtFilter">
                            <tr class="active">
                                <th style="min-width:30px; width: 30px; text-align: center;">
                                    <input class="checkbox checkft" type="checkbox" name="check"/>
                                </th>
                                <th></th><th></th><th></th><th></th><th></th><th></th>
                                <th><?php echo $this->sma->formatMoney($total_grand); ?></th>
                                <th></th>
                                <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-chain"></i></th>
                                <th style="width:80px; text-align:center;"><?= lang("actions"); ?></th>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel-heading-left">
                                <p class="data-total">
                                    <?php echo $show;?>
                                </p>
                            </div>
                            <div class="panel-heading-right">
                                   <?php echo $this->pagination->create_links(); //pagination links ?>
                            </div>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--change payment status to due-->

<!-- Modal -->
<div id="myModalDue" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Payment Status To Due</h4>
            </div>
            <div class="modal-body">
                <div id="model_body_due" style="display:none">
                    <p>Some Customer Below cannot update the payment status To Due. Because these customers will payment by Monthly.</p>
                    <table class="table table-bordered" id="tbl-update_to_due">
                        <thead>
                          <tr>
                            <th>Reference No</th>
                            <th>Customer Name</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php //Display content reference_no and customer name ?>
                        </tbody>
                    </table>
                </div>    
                <h2>Are you sure you want to update?</h2>    
            </div>
            <div class="modal-footer">  
                <a href="#" id="status_payment" data-action="change_payment_status_to_due" >
					<button class='btn btn-danger' ><?=lang('i_m_sure')?></a>
				</a>
                <button class='btn bpop-due-close' data-dismiss="modal"><?=lang('no')?></button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="myModalPartial" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Payment Status To Partail</h4>
            </div>
            <div class="modal-body" id="tbl-update_to_partial">
                <div id="model_body_partial" style="display:none">
                    <p>Some Customer Below cannot update the payment status To Partial. Because these customers will payment by Weekly.</p>              
                    <table class="table table-bordered" id="tbl-update_to_partial">
                        <thead>
                          <tr>
                            <th>Reference No</th>
                            <th>Customer Name</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php //Display content reference_no and customer name ?>
                        </tbody>
                    </table>   
                </div>    
                <h2>Are you sure you want to update?</h2>    
            </div>
            <div class="modal-footer">               
                <a href="#" id='status_payment' data-action='change_payment_status_to_partial'><button  class='btn btn-danger'><?=lang('i_m_sure')?></button></a> <button class='btn bpop-partial-close' data-dismiss="modal"><?=lang('no')?></button>
            </div>
        </div>
    </div>
</div>

<!--change payment status to paid-->
<!-- Modal -->
<div id="myModalPaid" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Payment Status To Paid</h4>
            </div>
                <div class="modal-body" id="tbl-update_to_paid">
                    <div id="model_body_paid" style="display:none">
                        <p>Some Customer Below cannot update the payment status To Paid.</p>
                        <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th>Reference No</th>
                                <th>Customer Name</th>
                              </tr>
                            </thead>
                            <tbody>
                                <?php //Display content reference_no and customer name ?>
                            </tbody>
                        </table>  
                    </div>
                    <h2>Are you sure you want to update?</h2> 
                </div>
           
            <div class="modal-footer">                
               <a href="#" id='status_payment' data-action='change_payment_status_to_paid'>
			   <button class='btn btn-danger'><?=lang('i_m_sure')?></button></a> <button class='btn bpop-paid-close' data-dismiss="modal"><?=lang('no')?></button>
            </div>
        </div>
    </div>
</div>

<input type="hidden" name="warehouse_id" value="<?php echo $warehouse_id;?>" id="warehouse_id"/>
<input type="hidden" name="show_data" value="<?php echo $show_data;?>" id="show_data"/>
<input type="hidden" name="sort_by" value="<?php echo $sort_by;?>" id="sort_by"/>
<input type="hidden" name="sort_order" value="<?php echo $sort_order;?>" id="sort_order"/>

<?php if ($Owner || $GP['bulk_actions']) {?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?=form_submit('performAction', 'performAction', 'id="action-form-submit"')?>
    </div>
    <?=form_close()?>
<?php }
?>


<script>
    function formactionsubmit(sort_by, sort_order) {
            document.getElementById("sort_by").value = sort_by;
            document.getElementById("sort_order").value = sort_order;
            //document.form_sale.submit();
            document.forms[0].submit();

        }
    $('document').ready(function(){
        
        $('body').on('click', '.pagination a', function(e) {
            e.preventDefault();
            var url = $(this).attr('href'); 
            $('#action-form').attr('action',url);
            window.history.pushState("", "", url);
            $('#action-form').submit();
        });
		
    
        $("#update_to_due").click(function(){
            newhtml=''
            $('input[name="val[]"]:checked').each(function() {
                var id = this.value;
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"sales/getPaymentCate/" + $(this).val(),
                    dataType: "json",
                    success: function (data) {
                        var comp = data.results;
                        var pc = comp.payment_category;
                        $("#SLData tr#"+id).each (function( column, td) {
                            var td_grand = $(this).find("td").eq(6).html();   
                            var td_paid = $(this).find("td").eq(7).html();    
                            var reference_no = $(this).find("td").eq(2).html(); 
                            var customer = $(this).find("td").eq(4).html(); 
                            paid = parseFloat(td_paid.substr(1));
                            grand = parseFloat(td_grand.substr(1));
                            if(pc=="monthly" || pc=="2 month"){
                                document.getElementById("model_body_due").style.display = 'block';
                                newhtml += '<tr>';
                                newhtml += '<td>';
                                newhtml +=reference_no;
                                newhtml += '</td>';
                                newhtml += '<td>';
                                newhtml +=customer;
                                newhtml += '</td>';
                                newhtml += '</tr>';
                            }
						$("#tbl-update_to_due tbody").html(newhtml);
					 });  
					}
				});
			});
        });
        
        //script change payment status to partial
        $("#update_to_partial").click(function(){
            newhtml='';
            $('input[name="val[]"]:checked').each(function() {
                var id = this.value;
                $.ajax({
					type: "get", async: false,
					url: site.base_url+"sales/getPaymentCate/" + $(this).val(),
					dataType: "json",
					success: function (data) {
						var comp = data.results;
						var pc = comp.payment_category;
						$("#SLData tr#"+id).each (function( column, td) {
							var td_grand = $(this).find("td").eq(6).html();   
							var td_paid = $(this).find("td").eq(7).html();    
							var reference_no = $(this).find("td").eq(2).html(); 
							var customer = $(this).find("td").eq(4).html(); 
							paid = parseFloat(td_paid.substr(1));
							grand = parseFloat(td_grand.substr(1));
							if(pc=="weekly" || pc=="cash on delivery"){
								document.getElementById("model_body_partial").style.display = 'block';
								newhtml += '<tr>';
								newhtml += '<td>';
								newhtml +=reference_no;
								newhtml += '</td>';
								newhtml += '<td>';
								newhtml +=customer;
								newhtml += '</td>';
								newhtml += '</tr>';
							}
							$("#tbl-update_to_partial tbody").html(newhtml);
						});  
					}
				});
			});
        });
	
	});
function bulk_actions_delete(){
    $("#action-form").attr('action', 'admin/sales/consignment_actions');
    $('#form_action').val('delete');
    $("#action-form").submit();
}       
</script>
