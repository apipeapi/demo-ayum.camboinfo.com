<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $this->lang->line('sale') . ' ' . $inv->reference_no; ?></title>
    <link href="<?= $assets ?>styles/pdf/bootstrap.min.css" rel="stylesheet">
    <link href="<?= $assets ?>styles/pdf/pdf.css" rel="stylesheet">
	<link href="<?php echo $assets ?>styles/style.css" rel="stylesheet">
</head>
 <style type="text/css">
        html, body {
            height: 100%;
            background: #FFF;
            font-size: 11px;
        }
        body:before, body:after {
            display: none !important;
        }
        .table th {
            text-align: center;
            padding: 5px;
        }
        .table td {
            padding: 4px;
        }
        .p-style table tr td h2{
            font-size: 14px;
            z-index: 1;
            //padding-top:-15px;
        }
        .p-style p{
            font-size: 11px;
        }
        .t-style table th{
            font-size: 12px;
        }
        .t-style table  tr td{
            font-size: 11px;
        }
        .img_logo{
           max-width: 250px;
           margin-top:-20px;
        }
        
    </style>
<body>
<div id="wrap">
    <div class="row">
        <div class="col-lg-12">
          
			 <?php
                if (is_int(strpos($inv->reference_no,"SL")))
                    $temp = $biller->logo;
                else
                    $temp = 'consignment_logo.png';             
                $path = 'assets/uploads/logos/'.$temp;                
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($path);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                ?>
                <div class="text-center" style="margin-bottom:20px;">
                    <img src="<?= $base64; ?>" alt="<?= $biller->company != '-' ? $biller->company : $biller->name; ?>">
                </div>
            
            <div class="clearfix"></div>
            <div class="row padding10">
<?php 
    $temp = '<td  width="10%"><div style="">&nbsp;</div></td>';            
?>

<div class="col-md-12">
<table width="100%" >
    <tr>
        <td valign="top" width="45%">
            From:&nbsp;
            <span style="font-size: 12px; font-weight:bold;"><?= $biller->company ?></span>
            <div class="api_height_10">&nbsp;</div>
        </td>
        <?= $temp ?>
        <td valign="top" width="45%">
            <?php
                echo '<h3 style="font-size:13px;">'.$customer->company.'</h3><br>';                        
            ?>
            <div class="api_height_10">&nbsp;</div>
        </td>        
    </tr>
    <tr>
        <td valign="top" width="45%">
            <?php echo $biller->address; ?>
            <div class="api_height_20">&nbsp;</div>
        </td>
        <?= $temp ?>
        <td valign="top" width="45%">
            <?php echo $customer->address; ?>
            <div class="api_height_20">&nbsp;</div>
        </td>
    </tr>
    <tr>
        <td valign="top" width="45%">
            <?php echo lang('tel').': '.$biller->phone; ?>
        </td>
        <?= $temp ?>
        <td valign="top" width="45%">
            <?php echo lang('tel').': '.$customer->phone; ?>
        </td>
    </tr>  
    <tr>
        <td valign="top" width="45%">
            <?php echo lang('email').': '.$biller->email; ?>
        </td>
        <?= $temp ?>
        <td valign="top" width="45%">
            <?php echo lang('email').': '.$customer->email; ?>
        </td>
    </tr>
    <tr>
        <td valign="top" colspan="3" align="left">
            <div class="api_height_20">&nbsp;</div>
            <?php
                $date = date_create($inv->date);
                echo '<span style="font-weight:bold;">'.lang('date').':</span> '.date_format($date, 'd/m/Y');;
            ?>        
        </td>
    </tr>    
</table>   
</div>
   
            </div>
            <div class="clearfix"></div>
            <div class="clearfix"></div>
            <div class="table-responsive t-style">

<table class="table"  style="undefined;table-layout: fixed; width: 100%;">

<thead class="table-bordered table-hover table-striped">
  <tr>
    <th class="" rowspan="2"><?= lang('no'); ?></th>
    <th class="" rowspan="2" width="100">Code (Lucky)</th>
    <th class="" rowspan="2" width="100"><?= lang('Barcode'); ?></th>
    <th class="" rowspan="2"><?= lang('description'); ?></th>
    <th class="" rowspan="2" width="100">Price</th>
    <th class="" rowspan="2">QTY (Unit)</th>
    <th class="" width="100">Date</th>
    <th class="" width="100">Date</th>
    <th class="" rowspan="2" width="100">EXP Date</th>
  </tr>
  <tr>
    <th class="">Balance</th>
    <th class="">Balance</th>
  </tr>
</thead>
<tbody class="table-bordered table-hover table-striped">
    <?php
        $r = 1;
        foreach ($rows as $row):
            $temp = array();                                    
            if ($row->product_id > 0) {     
                $config_data = array(
                    'table_name' => 'sma_products',
                    'select_table' => 'sma_products',
                    'translate' => '',
                    'select_condition' => "id = ".$row->product_id,
                );
                $temp = $this->site->api_select_data_v2($config_data);
            }
            ?>
            <tr>
                <td rowspan="2" style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                <td rowspan="2">
                    <?= $temp[0]['lucky_code'] ?>
                </td>
                <td rowspan="2" align="center">
                    <?= $temp[0]['barcode'] ?>
                </td>
                <td rowspan="2">
                    <?= $row->product_name ?>
                </td>
                <td rowspan="2" align="right">
                    <?= $this->sma->formatMoney($row->unit_price); ?>
                </td>                
                <td rowspan="2" style="text-align:right; vertical-align:middle;">
                    <?= $this->sma->formatQuantity($row->quantity); ?>
                </td>
                <td></td>
                <td></td>
                <td rowspan="2"></td>
            </tr>
            <tr>
                <td class=""></td>
                <td class=""></td>
            </tr>            
            <?php
            $r++;
        endforeach;
    ?>

</tbody>
<tfoot>                   
</tfoot>                    
</table>
            </div>
<br>
<br>
            <div class="row">
                <div class="clearfix"></div>
                <div class="col-xs-5  pull-left">
                    <p style="height: 80px;">
			 <?= lang('seller'); ?>
                        : <?= $biller->company != '-' ? $biller->company : $biller->name;?>
			<br>
                    </p>
                    <hr>
                    <div style="text-align:center;"><?= lang('stamp_sign'); ?></div>
                </div>
                <div class="col-xs-5  pull-right">
                    <p style="height: 80px;">
                        <?= lang('customer'); ?> 
                        : <?= $customer->company ? $customer->company : $customer->name; ?>
			<br>
							
                    </p>
                    <hr>
                    <div style="text-align:center;"><?= lang('stamp_sign'); ?></div>
                </div>
            </div>

        </div>
    </div>
</div>
</body>
</html>
