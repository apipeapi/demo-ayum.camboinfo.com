<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo $modal_title; ?></h4>
        </div>

        <div class="modal-body">
            <?php
                echo '<p><strong>'.lang('reference_no').': </strong>'.$select_data[0]['reference_no'].'</p>';
            ?>            

            <div class="table-responsive" id="api_get_table_sale_items">
                <?php
echo '
    <table class="table table-bordered table-hover table-striped print-table order-table">
        <thead>
        <tr>
            <th>No.</th>
            <th>'.lang('Description').'</th>
            <th>'.lang('Date').'</th>
            <th>'.lang('Sale_Ref_No').'</th>
            <th>'.lang('Quantity').'</th>
        </tr>
        </thead>
        <tbody>
';

$temp_total = 0;
$temp_id = '';
if ($select_data[0]['product_id'] > 0) {
    for ($i=0;$i<count($select_data);$i++) {
        $temp_id .= '-'.$select_data[$i]['sale_id'];
        $temp = $this->site->api_select_some_fields_with_where("
            code, name
            "
            ,"sma_products"
            ,"id = ".$select_data[$i]['product_id']
            ,"arr"
        );    
        $temp2 = $this->site->api_select_some_fields_with_where("
            reference_no
            "
            ,"sma_sales"
            ,"id = ".$select_data[$i]['sale_id']
            ,"arr"
        );    
        echo '
            <tr>
            <td style="text-align:center; width:40px; vertical-align:middle;">
                '.($i + 1).'
            </td>
            <td style="vertical-align:middle;">
                '.$temp[0]['code'].' - '.$temp[0]['name'].'
            </td>
            <td style="text-align:center; width:1%; white-space:nowrap;">
                '.date('Y/m/d h:i:s', strtotime($select_data[$i]['date'])).'
            </td>
            <td style="text-align:center; width:1%; white-space:nowrap;">
                <a href="'.base_url().'admin/sales?search='.$temp2[0]['reference_no'].'" target="_blank">
                    '.$temp2[0]['reference_no'].'
                </a>
            </td>
            <td style="text-align:center; width:1%; white-space:nowrap;">
                '.$this->sma->formatQuantity($select_data[$i]['quantity']).'
            </td>
            </tr>
        ';
        $temp_total = $temp_total + $select_data[$i]['quantity'];
    }
}
else
    echo '
        <tr><td colspan="5">'.lang('No_record').'</td></tr>
    ';
echo '
        </tbody>
';

echo '
        <tfoot>
        <tr>
            <td colspan="4" style="text-align:right; font-weight:bold;">
                '.lang('Sold_Quantity').'
            </td>
            <td style="text-align:center; font-weight:bold;">
                '.$temp_total.'/'.$total_quantity.'
            </td>
        </tr>
        </tfoot>
';

echo '
    </table>
';
                ?>
<?php
    if ($select_data[0]['product_id'] > 0) {                
        echo '
            <div class="btn-group btn-group-justified">
                <div class="btn-group">
                    <a href="'.admin_url('sales?adjustment_sale_track=' . $temp_id).'" class="btn btn-info" target="_blank">
                        <i class="fa fa-eye"></i>
                        <span class="hidden-sm hidden-xs api_margin_left_5">'.lang('View_All_Sales').'</span>
                    </a>
                </div>
            </div>
        ';
    }
?>
            </div>
        </div>
        <div class="modal-footer">
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

