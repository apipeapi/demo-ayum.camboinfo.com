<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php 
    $temp_url = '';
    if ($_GET['page'] != '') 
        $temp_url .= '?page='.$_GET['page']; 
    else 
        $temp_url .= '?page=1'; 

    if ($per_page != '') $temp_url .= '&per_page='.$per_page;

    foreach ($_GET as $name => $value) {
        if ($name != 'page' && $name != 'per_page' && $name != 'mode')
            if ($value != '') $temp_url .= '&'.$name.'='.$value;
    }

if ($Owner) {
    $spage = $show_data != '' ? $show_data : 1;
    echo admin_form_open('purchases/expenses'.$temp_url, 'id="action-form" name="action-form" method="GET"');
}
?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-dollar"></i><?= lang('expenses'); ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?= admin_url('purchases/add_expense') ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus-circle"></i> <?= lang('add_expense') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="excel" onclick="api_bulk_actions('export_excel');" data-action="export_excel">
                                <i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="change_status_to_paid" onclick="api_bulk_actions('change_status_to_paid');" data-action="change_status_to_paid">
                                <i class="fa fa-edit"></i> <?= lang('change_status_to_paid') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="change_status_to_pending" onclick="api_bulk_actions('change_status_to_pending');" data-action="change_status_to_pending">
                                <i class="fa fa-edit"></i> <?= lang('change_status_to_pending') ?>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#" class="bpo" title="<b><?= $this->lang->line("delete_expenses") ?></b>"
                                data-content="<p><?= lang('r_u_sure') ?></p><button type='button' class='btn btn-danger' onclick='bulk_actions_delete();' data-action='delete'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button>"
                                data-html="true" data-placement="left">
                                <i class="fa fa-trash-o"></i> <?= lang('delete_expenses') ?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

            <?php
    //-search_fields-================================================
echo '
<div style="background: #fff; margin: -20px -20px 20px -20px; padding: 10px; border-bottom: 1px solid #DBDEE0;">
';

echo '
    <div class="col-md-6 " >
        <div class="form-group">
            '.lang("Category", "Category").'
';
        $temp = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_expense_categories"
            ,"id > 0 order by name asc"
            ,"arr"
        );
        $tr[''] = lang("Please_a_category");
        for ($i=0;$i<count($temp);$i++) {
            $tr[$temp[$i]['id']] = $temp[$i]['name'];
        }                            
        echo form_dropdown('category_id', $tr, $_GET['category_id'], ' class="form-control" ');

echo '
        </div>
    </div>
';


echo '
    <div class="api_clear_both"></div>
</div>
';
//-search_fields-================================================
?>

                <div class="table-responsive">
                        <div class="row">
                            <div class="sale-header">

                        <div class="col-sm-3">
                            <div class="form-group">
                                <?= lang("Start_date", "Start_date"); ?>
                                <?php echo form_input('start_date', isset($_GET['start_date']) ? $_GET['start_date'] : "", 'class="form-control datetime" id="start_date"'); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?= lang("End_date", "End_date"); ?>
                                <?php echo form_input('end_date', isset($_GET['end_date']) ? $_GET['end_date'] : "", 'class="form-control datetime" id="end_date"'); ?>
                            </div>
                        </div>
                                <div class="clearfix"></div>
                            
                                <div class="col-md-6 text-left">
                                    <div class="short-result">
                                        <label>
                                            Show
                                        </label>
                                        <select id="sel_id" name="per_page" onchange="document.action-form.submit()">
                                            <?php
                                                $per_page_arr = array(
                                                    '10' => '10',
                                                    '25' => '25',
                                                    '50' => '50',
                                                    '100' => '100',
                                                    '200' => '200',
                                                    'All' => $total_rows_sale
                                                );
                                            
                                                foreach($per_page_arr as $key =>$value){
                                                    $select = $value == $per_page?'selected':'';
                                                    echo '<option value="'.$value.'" '.$select.'>';
                                                            echo $key;
                                                    echo '</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 text-right">
                                    <div class="short-result">
                                        <div id="SLData_filter" class="show-data-search dataTables_filter">
                                            <label>
                                                Search
                                                <input class="input-xs" value="<?php echo $this->input->get('search');?>" type="text" name="search" id="mysearch" placeholder="search"/>
                                                <input style="height: 34px;" class="btn btn-primary" type="submit" name="submit_search" value="Submit">
                                            </label>
                                            <div id="result"></div>
                                        </div>
                                    </div>    
                                </div>	
                                <div class="clearfix"></div>
                             </div>
                        </div>     
                    </div> 
                
                    <table id=" cellpadding="0" cellspacing="0" border="0"
                           class="table table-bordered table-hover table-striped">
                        <thead>
<?php
    $row_headers = array(
        'checkbox'      => [
                            'label' => '<input class="checkbox checkft" type="checkbox" name="check"/>', 
                            'sort_fn' => false,
                            'style' => 'min-width:30px; width: 30px; text-align: center;'
                        ],
        'date'          => ['label' => lang("date"), 'style' => 'width:135px;'],
        'reference'  => ['label' => lang("reference"), 'style' => 'width:135px;'],
        'category'        => ['label' => lang("category"), 'style' => 'width:135px'],
        'amount'      => ['label' => lang("amount"), 'style' => ''],
        'note'   => ['label' => lang("note"), 'style' => ''],
        'created_by'   => ['label' => lang("created_by"), 'style' => 'width:135px;'],
        'status'          => ['label' => lang("status"), 'style' => 'width:80px;'],
        'chain'         => [
                                'label'     => '<i class="fa fa-chain"></i>', 
                                'style'     => 'min-width:30px; width: 30px; text-align: center;',
                                'sort_fn'   => false
                            ],
        'action'        => [
                            'label' => lang("actions"), 
                            'style' => 'width:80px; text-align:center;', 
                            'sort_fn' => false
                        ],
    );
?>

                        
                        <tr class="">
                                <?php foreach ($row_headers as $key => $row) :?>
                                    <?php
                                   
                                    $th_class="class='pointer'";
                                    $e_click = true;
                                    if(isset($row['sort_fn']) && $row['sort_fn'] == false) {
                                        $th_class="";
                                        $e_click = false;
                                    }
                                    $style = isset($row['style']) && $row['style'] != '' ? 'style="'.$row['style'].'"' : '';
                                    ?>
                                    <th <?= $style ?> >
                                        <?php if($e_click ) :?>
                                            <?php if ($sort_by == $key && $sort_order == "ASC") : ?>
                                                    <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort"  aria-hidden="true">
                                                    <?php echo '<span class="pointer">'.$row['label'].'</span>'; ?>
                                                        <span class="fa fa-sort-down"></span>
                                                    </label>
                                            <?php elseif ($sort_by == $key and $sort_order=="DESC") : ?>	
                                                <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'ASC')" id="fa_sort_up"  aria-hidden="true">
                                                    <?php echo '<span class="pointer">'.$row['label'].'</span>'; ?>
                                                    <span class="fa fa-sort-up"></span>
                                                </label>
                                            <?php else : ?>	
                                                <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort_down" aria-hidden="true">
                                                    <?php echo '<span class="pointer">'.$row['label'].'</span>'; ?>
                                                    <span class="fa fa-sort"></span>
                                                </label>
                                            <?php endif; ?>	
                                        <?php else : ?>
                                            <label class="font-normal" aria-hidden="true">
                                                <?php echo $row['label']; ?>
                                            </label>
                                        <?php endif; ?>	
                                    </th>
                                <?php endforeach; ?>

                        </tr>
                        </thead>
                        <tbody>
<?php
for ($i=0;$i<count($select_data);$i++) {

        $detail_link = anchor('admin/purchases/expense_note/'.$select_data[$i]['id'], '<i class="fa fa-file-text-o"></i> ' . lang('expense_note'), 'data-toggle="modal" data-target="#myModal2"');
        $edit_link = anchor('admin/purchases/edit_expense/'.$select_data[$i]['id'], '<i class="fa fa-edit"></i> ' . lang('edit_expense'), 'data-toggle="modal" data-target="#myModal"');
        $duplicate_link = anchor('admin/purchases/expense_duplicate/'.$select_data[$i]['id'], '<i class="fa fa-files-o"></i> ' . lang('duplicate_expense'), '');
        $delete_link = '
            <a href="#" class="po" title="<b>'.$this->lang->line('delete_expense').'</b>" data-content="
<p>'.lang('r_u_sure').'</p>
<a class=\'btn btn-danger\' href=\''.admin_url('purchases/delete_expense/'.$select_data[$i]['id']).'\'>
    '.lang('i_m_sure').'
</a>
<button class=\'btn po-close\'>'.lang('no').'</button>       
            ">
                <i class="fa fa-trash-o"></i> '.lang('delete_expense').'
            </a>
        ';

        if ($select_data[$i]['status'] == 'paid')
            $temp_status = '<div class="text-center"><span class="label label-success">'.$select_data[$i]['status'].'</span></div>';
        else 
            $temp_status = '<div class="text-center"><span class="label label-warning">'.$select_data[$i]['status'].'</span></div>';
        
        $action = '<div class="text-center"><div class="btn-group text-left">'
        . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
        . lang('actions') . ' <span class="caret"></span></button>
        <ul class="dropdown-menu pull-right" role="menu">
            <li>' . $detail_link . '</li>
            <li>' . $edit_link . '</li>
            <li>' . $duplicate_link . '</li>
            <li>' . $delete_link . '</li>
        </ul>
    </div></div>';
    $select_data[$i]['temp_action'] = $action;
    
    $temp = array();
    if ($select_data[$i]['created_by'] != '') {
        $config_data = array(
            'table_name' => 'sma_users',
            'select_table' => 'sma_users',
            'select_condition' => "id = ".$select_data[$i]['created_by'],
        );
        $temp = $this->site->api_select_data_v2($config_data);
    }
        
    echo '
        <tr id="'.$select_data[$i]['id'].'" class="invoice_link_expense">
        <td style="min-width:30px; width: 30px; text-align: center;">
            <div class="text-center">
                <input class="checkbox multi-select" type="checkbox" 
                    name="val[]" value="'.$select_data[$i]['id'].'" />
            </div>
        </td>
        <td>
            '.date('d/m/Y H:i:s', strtotime($select_data[$i]['date'])).'
        </td>
        <td>
            '.$select_data[$i]['reference'].'
        </td>
        <td>
            '.$select_data[$i]['category'].'
        </td>
        <td align="right">
            '.$this->sma->formatMoney($select_data[$i]['amount']).'
        </td>
        <td>
            '.$select_data[$i]['note'].'
        </td>
        <td>
            '.$temp[0]['first_name'].' '.$temp[0]['last_name'].'
        </td>
        <td>
            '.$temp_status.'
        </td>
        <td>
        </td>
        <td>
            '.$select_data[$i]['temp_action'].'
        </td>
        </tr>        
    ';
    
}
?>                            
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft" type="checkbox" name="check"/>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-chain"></i>
                            </th>
                            <th style="width:100px; text-align: center;"><?= lang("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <div class="dataTables_info" id="EXPData_info">
                                    <?php echo $show;?>
                            </div>
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="dataTables_paginate paging_bootstrap">
                               <?php echo $this->pagination->create_links(); //pagination links ?>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
if ($Owner) { 
?>
    <div style="display: none;">
        <input type="hidden" name="show_data" value="<?php echo $show_data;?>" id="show_data"/>
        <input type="hidden" name="sort_by" value="<?php echo $sort_by;?>" id="sort_by"/>
        <input type="hidden" name="sort_order" value="<?php echo $sort_order;?>" id="sort_order"/>    
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php 
} 
?>

<script>
var initAppendingDOM;
var inc = 0;
/*******************
 * Author: TEP Afril
 * 18th May, 2018
 * odadcambodia.com
********************/
$(document).ready(function () {
	var grand_total = Number(0);
	var paid = Number(0);
	var balance = Number(0);
	var arrIndex = [];

	var updateData = function(){
		$('#custom_g_t').html(numberWithCommas(grand_total));
		$('#custom_paid').html(numberWithCommas(paid));
		$('#custom_balance').html(numberWithCommas(balance));
	};

	numberWithCommas = (x) => {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	var sumData = function( gbl, vbl, sign ){
		gbl = Number(gbl);
		vbl = vbl.replace(/\$/g,'');
		vbl = vbl.replace(/\,/g,'');
		vbl = Number(vbl);

		switch(sign)
		{
			case "+":
				gbl += (Number(vbl));
			break;

			case "-":
				gbl -= (Number(vbl));
			break;
		}
		gbl = gbl.toFixed(2);
		return gbl;
	};

	var resetData = function(){
		grand_total = '0.00';
		paid = '0.00';
		balance = '0.00';
		updateData();
	};

	initAppendingDOM = function(gt,pd,bl,idx){
		domStr = '<div style="width:20vw;position:fixed;top:50px;right:280px;z-index:1000;opacity:100">'+
		'<a id="expand_sum_table" class="btn btn-success btn-sm" style="top:45px;right:30px;position:fixed;display:none"><i style="" class="fa fa-2x fa-arrows-alt"></i></a>'+
		'<div id="sum_table_container">'+
		'<a id="close_sum_table" class="btn btn-sm btn-danger" style="padding: 2px 5px;cursor:pointer;top:45px;right:270px;position:fixed;"><i class="fa fa-times"></i></a>'+
		'<table  class="table table-bordered table-hover table-striped dataTable">'+
		'<thead>'+
		'<tr>'+
		'<td style="background-color:#333;border-color:#000;">'+gt+'</td>'+
		'</tr>'+
		'</thead>'+
		'<tbody>'+
		'<tr style="font-weight:bold;">'+
		'<td id="custom_g_t" style="background-color:#d1d1d1;">0.00</td>'+
		'</tr>'+
		'</tbody>'+
		'</table>'+
		'</div>'+
		'</div>';
		$('.box').append(domStr);
		arrIndex = idx;
	};

	$('body').on('ifChecked','.icheckbox_square-blue',function(){
		inc++;

		var row = $(this).parent().parent().parent();
		var td_g_t = $(row[0]).children().eq(arrIndex[0]);
		var td_paid = $(row[0]).children().eq(arrIndex[1]);
		var td_balance = $(row[0]).children().eq(arrIndex[2]);


		var selected_g_t = $(td_g_t).text();
		var selected_paid = $(td_paid).text();
		var selected_balance = $(td_balance).text();

		var temp = grand_total;

		grand_total = sumData(grand_total,selected_g_t,'+');
		paid = sumData(paid,selected_paid,'+');
		balance = sumData(balance,selected_balance,'+');

		console.log( inc + " : " + grand_total + " = " + temp + " + " + selected_g_t );

		updateData();
	});

	$('body').on('ifUnchecked','.icheckbox_square-blue',function(){


		var row = $(this).parent().parent().parent();

		var td_g_t = $(row[0]).children().eq(arrIndex[0]);
		var td_paid = $(row[0]).children().eq(arrIndex[1]);
		var td_balance = $(row[0]).children().eq(arrIndex[2]);

		var selected_g_t = $(td_g_t).text();
		var selected_paid = $(td_paid).text();
		var selected_balance = $(td_balance).text();

		grand_total = sumData(grand_total,selected_g_t,'-');
		paid = sumData(paid,selected_paid,'-');
		balance = sumData(balance,selected_balance,'-');

		updateData();
	});

	$('body').on('click','#close_sum_table',function(){
		$('#sum_table_container').hide(300,function(){
			$('#expand_sum_table').show(300);
		});
		});
		$('body').on('click','#expand_sum_table',function(){
		$('#expand_sum_table').hide(300,function(){
			$('#sum_table_container').show(300);
		});
	});

	$('body').on('change', 'select[name="POData_length"]', function (e) {
		resetData();
	});
	$('body').on('click', '.pagination a', function (e) {
		resetData();
	});

    $(document).ready(function(){
        initAppendingDOM('<?= lang("total_amount"); ?>','','',[4]);
    });
});

function formactionsubmit(sort_by, sort_order) {
    document.getElementById("sort_by").value = sort_by;
    document.getElementById("sort_order").value = sort_order;
    document.forms[0].submit();
}

function bulk_actions_delete(){
    $("#action-form").attr('action', 'admin/purchases/expense_actions');
    $('#form_action').val('delete');
    $("#action-form").submit();
}       
function api_bulk_actions(action){
    $("#action-form").attr('action', 'admin/purchases/expense_actions');
    $('#form_action').val(action);
    $('body').on('click', '#' + action, function(e) {
        e.preventDefault();
        $('#form_action').val($(this).attr('data-action'));
        $('#action-form-submit').trigger('click');
    });
}
   
</script>

