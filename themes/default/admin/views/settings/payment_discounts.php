
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-cog"></i><?= lang('system_settings'); ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown"><a href="<?= admin_url('system_settings/paypal') ?>" class="toggle_up">
                    <i class="icon fa fa-paypal"></i> <span class="padding-right-10"><?= lang('paypal'); ?></span></a>
                </li>
                <li class="dropdown"><a href="<?= admin_url('system_settings/skrill') ?>" class="toggle_down">
                    <i class="icon fa fa-bank"></i> <span class="padding-right-10"><?= lang('skrill'); ?></span></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?= lang('update_info'); ?></p>
                <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
                    echo admin_form_open_multipart("system_settings", $attrib);
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border"><?= lang('Assign_Payment_Discount') ?></legend>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("site_name", "site_name"); ?>
                                    <?= form_input('site_name', $settings->site_name, 'class="form-control tip" id="site_name"  required="required"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select name="language" class="form-control tip" id="language" required="required" style="width: 100%; display: none;" tabindex="-1" title="" data-original-title="Language" data-bv-field="language">
                                        <option value="arabic">Arabic</option>
                                        <option value="english" selected="selected">English</option>
                                        <option value="german">German</option>
                                        <option value="indonesian">Indonesian</option>
                                        <option value="portuguese-brazilian">Portuguese (Brazil)</option>
                                        <option value="simplified-chinese">Simplified Chinese</option>
                                        <option value="spanish">Spanish</option>
                                        <option value="thai">Thai</option>
                                        <option value="traditional-chinese">Traditional Chinese</option>
                                        <option value="turkish">Turkish</option>
                                        <option value="vietnamese">Vietnamese</option>
                                    </select>
                                </div>
                            </div>
                    </fieldset>
                </div>
            </div>
            <div class="cleafix"></div>
        </div>
    </div>
    <div class="alert alert-info" role="alert">
    </div>