<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php
    $temp_url = '';
    if ($_GET['page'] != '') {
        $temp_url .= '?page='.$_GET['page'];
    } else {
        $temp_url .= '?page=1';
    }

    if ($per_page != '') {
        $temp_url .= '&per_page='.$per_page;
    }
    if ($_GET['mode'] == 'sample') {
        $temp_url .= '&mode='.$_GET['mode'];
    }

    foreach ($_GET as $name => $value) {
        if ($name != 'page' && $name != 'per_page' && $name != 'mode') {
            if ($value != '') {
                $temp_url .= '&'.$name.'='.$value;
            }
        }
    }
    

    echo admin_form_open('sales'.$temp_url, 'id="action-form" name="action-form" method="GET"');
    echo '<input type="hidden" name="mode" value="'.$_GET['mode'].'" />';
?>

<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-heart"></i>
            <?php
                $temp = '';
                if ($_GET['mode'] == 'sample') {
                    $temp = lang('Sample').' ';
                }
                if ($_GET['warehouse_id'] != '') {
                    for ($i=0;$i<count($api_warehouse);$i++) {
                        if ($_GET['warehouse_id'] == $api_warehouse[$i]['id']) {
                            $temp2 = $api_warehouse[$i]['name'];
                            break;
                        }
                    }
                } else {
                    $temp2 = lang('all_warehouses');
                }
                echo $temp.$page_title.' ('.$temp2.')';
            ?>
        </h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
<?php
                            $temp = '';
                            if ($_GET['mode'] == 'sample') {
                                $temp = '?mode=sample';
                            }
                            echo '
                                <li>
                                    <a href="'.admin_url('sales/add'.$temp).'">
                                        <i class="fa fa-plus-circle"></i> '.lang('add_sale').'
                                    </a>
                                </li>                               
                                <li>
                                    <a href="javascript:void(0)" onclick="api_bulk_actions(\'export_excel\');" data-action="export_excel">
                                        <i class="fa fa-file-excel-o"></i> '.lang('export_to_excel').'
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" onclick="api_bulk_actions(\'combine\');" data-action="combine">
                                        <i class="fa fa-file-pdf-o"></i> '.lang('combine_to_pdf').'
                                    </a>
                                </li>
                            ';
                            echo '
                                <li class="divider"></li>
                            ';
?>
                                <li>
                                    <a href="#" class="bpo" title="<b><?=lang("delete_sales")?></b>" data-content="<p><?=lang('r_u_sure')?></p>
                                    <button type='button' class='btn btn-danger' onclick='bulk_actions_delete();' data-action='delete'><?=lang('i_m_sure')?></a> <button class='btn bpo-close'><?=lang('no')?></button>" data-html="true" data-placement="left">
                                        <i class="fa fa-trash-o"></i> <?=lang('delete_sales')?>
                                    </a>
                                </li>
                    </ul>
                </li>
            </ul>
        </div>

    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
<?php
if ($_GET['mode'] != 'sample') {
    echo '
    <div style="background: #F9F9F9; margin: -20px -20px 20px -20px; padding: 10px; border-bottom: 1px solid #DBDEE0;">    
        <div class="api_float_right">
            <ul style="list-style: none; margin:0px;">
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">                    
                    <button type="button" class="btn btn-success" style="margin:5px 0px 5px 5px !important;">
                        <li class="fa fa-pencil-square-o"></li> '.lang('Change_Payment').'
                    </button>
                </a>

                <ul class="dropdown-menu tasks-menus" role="menu" aria-labelledby="dLabel" style="">
                    <li class="api_display_none">
                        <a class="" id="change_payment_status_to_due" href="javascript:void(0);" onclick="api_change_payment_status_to_due();">
                            <i class="fa fa-edit"></i>'.lang('to_due').'
                        </a>    
                    </li>

                    <li class="api_display_none">
                        <a id="api_change_payment_status_to_due" href="'.base_url().'admin/sales/change_payment_status_to_due" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-edit"></i>'.lang('to_due').'
                        </a>    
                    </li>                    
                    <li>
                        <a class="" id="change_payment_status_to_partial" href="javascript:void(0);" onclick="api_bulk_actions(\'change_payment_status_to_partial\');">
                            <i class="fa fa-edit"></i>'.lang('to_partial').'
                        </a>
                    </li>
                    <li>
                        <a class="" id="change_payment_status_to_ar" href="javascript:void(0);" onclick="api_bulk_actions(\'change_payment_status_to_ar\');">
                            <i class="fa fa-edit"></i>'.lang('to_AR').'
                        </a>
                    </li>

                    <li>
                        <a class="" id="change_payment_status_to_paid" href="javascript:void(0);" onclick="api_bulk_actions(\'change_payment_status_to_paid\');">
                            <i class="fa fa-edit"></i>'.lang('to_paid').'
                        </a>
                    </li>
                </ul>
            </li>
            </ul>
        </div>
    ';

    echo '
    <div class="api_float_right">
        <button type="button" class="btn btn-info api_margin_0 api_display_none" onclick="bulk_actions_generate_reference_no();">
            <li class="fa fa-pencil-square-o"></li> '.lang('Generate').'
        </button>
    ';
/*
    echo '
        <a href="javascript:void(0);" onclick="api_bulk_actions_print_pdf(\'print_combine_pdf\'); window.location = \''.base_url().'admin/sales\'">
        <button type="button" class="btn btn-info api_link_box_none" style="background-color:#8800a5 !important; border-color:#8800a5 !important; margin-left:5px;">
            <li class="fa fa-print"></li> '.lang('Print_PDF').'
        </button>
        </a>
    ';
*/
    echo '
        <a class="api_display_none" id="set_delivering" href="'.admin_url('sales/set_delivering_bulk').'" data-toggle="modal" data-target="#myModal">
            <i class="fa fa-truck"></i> '.lang('set_delivering').'
        </a>

        <a href="javascript:void(0);" onclick="api_bulk_actions_set_delivery(\'action-form\',\'val[]\',\'\');">    
        <button type="button" class="btn btn-info api_margin_5_im api_link_box_none" style="background-color:#004f93  !important; border-color:#004f93  !important" >
            <li class="fa fa-truck"></li> '.lang('set_delivering').'
        </button>
        </a>
    </div>
    ';

    echo '
        <div class="api_clear_both"></div>
    </div>
    ';
}
?>

                <div id="form">

                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <?= lang("start_date", "start_date"); ?>
                                <?php echo form_input('start_date', (isset($_GET['start_date']) ? $_GET['start_date'] : ''), 'class="form-control datetime" autocomplete="off" id="start_date"'); ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <?= lang("end_date", "end_date"); ?>
                                <?php echo form_input('end_date', (isset($_GET['end_date']) ? $_GET['end_date'] : ''), 'class="form-control datetime" autocomplete="off" id="end_date"'); ?>
                            </div>
                        </div>

<?php
                        echo '
                            <div class="col-md-2">
                                <div class="form-group">
                                    '.lang("sale_status", "sale_status").'
                        ';
                        $tr0a[''] = lang("all_sale_status");
                        for ($i=0;$i<count($sale_status);$i++) {
                            if ($sale_status[$i]['value'] != '') {
                                $tr0a[$sale_status[$i]['value']] = $sale_status[$i]['name'];
                            }
                        }
                        echo form_dropdown('sale_status', $tr0a, (isset($_GET['sale_status']) ? $_GET['sale_status'] : ''), 'data-placeholder="'.lang("sale_status").'" class="form-control"');
                        echo '
                                </div>
                            </div>
                        ';


                        echo '
                            <div class="col-md-2">
                                <div class="form-group">
                                    '.lang("VAT", "VAT").'
                        ';

                        $temp_tax_rate = $this->site->api_select_some_fields_with_where(
                            "
                            *     
                            ",
                            "sma_tax_rates",
                            "id > 0 order by id asc",
                            "arr"
                        );
                        $tr3 = array();
                        $tr3[''] = lang("All");
                        for ($i=0;$i<count($temp_tax_rate);$i++) {
                            $tr3[$temp_tax_rate[$i]['id']] = $temp_tax_rate[$i]['name'];
                        }
                        echo form_dropdown('order_tax_id', $tr3, (isset($_GET['order_tax_id']) ? $_GET['order_tax_id'] : ''), ' class="form-control"');
                        echo '
                                </div>
                            </div>
                        ';

                        echo '
                            <div class="col-md-2">
                                <div class="form-group">
                                    '.lang("warehouse", "warehouse").'
                        ';
                        $tr4[''] = lang("All_Warehouses");
                        for ($i=0;$i<count($api_warehouse);$i++) {
                            if ($api_warehouse[$i]['name'] != '') {
                                $tr4[$api_warehouse[$i]['id']] = $api_warehouse[$i]['name'];
                            }
                        }
                        echo form_dropdown('warehouse_id', $tr4, (isset($_GET['warehouse_id']) ? $_GET['warehouse_id'] : ''), ' class="form-control"');
                        echo '
                                </div>
                            </div>
                        ';

                        echo '
                            <div class="col-md-2">
                                <div class="form-group">
                                '.lang("Search", "Search").'
                                <input type="text" class="form-control" id="search" name="search" value="'.(isset($_GET['search']) ? $_GET['search'] : "").'" onkeydown="if (event.keyCode == 13) document.action-form.submit()" />
                        ';
                        echo '
                                </div>
                            </div>';?>
                        <!-- search trans -->
                        <div class="col-md-2">
                            <div class="input-group pull-left" id="transaction_search" style="width: 100%;">
                                <label for="search_input"><?=lang('Transaction Id')?></label>
                                <input type="text" class="form-control" placeholder="Payment Search" name="transaction_search" id="search_input">
                                <a class="input-group-prepend" id="search_btn" action="/" style="position:absolute;top:30px;right:0;z-index:99;padding-top:7px;padding-right:10px;cursor:pointer">
                                    <span class="input-group-text"><i class="fa fa-search"></i></span>
                                </a>
                            </div>  
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="paid_by"><?=lang('Payment Type')?></label>
                                <select name="paid_by" id="paid_by" class="form-control paid_by" onclick="api_payment_type();">
                                    <option value="all" selected="selected"><?=lang('All Payment Type');?></option>
                                    <?= $this->sma->paid_opts($_GET['paid_by']); ?>
                                </select>
                            </div>
                            
                        </div>
                    </div>

                    <div class="row">            
                        <div class="col-md-12">
                            <div class="form-group pull-right api_padding_left_10">
                                <a href="<?php echo base_url().'admin/sales'; ?>">
                                    <input type="button" name="reset" value="reset" class="btn btn-danger">
                                </a>
                            </div>
                            <div class="form-group pull-right">
                                <input type="button" name="submit_report" onclick="api_form_submit('<?php echo 'admin/sales'.$temp_url; ?>')" value="Submit" class="btn btn-primary">
                            </div>
                        </div>    
                    </div>
                        
                </div>
                <!-- /form search -->
                <div class="clearfix"></div>

                <div class="table-responsive">
                    <div id="SLData_wrapper" class="dataTables_wrapper form-inline" role="grid">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <div class="short-result">
                                     <label>Show</label>
                                         <select  id="sel_id" name="per_page" onchange="api_form_submit('<?php echo 'admin/sales'.$temp_url; ?>')" style="width:80px;">
                                             <?php
                                                 $per_page_arr = array(
                                                     '10'=>'10',
                                                     '25'=>'25',
                                                     '50'=> '50',
                                                     '100'=>'100',
                                                     '200'=>'200',
                                                     '500'=>'500',
                                                     '1000'=>'1000',
                                                     'All'=>$total_rows_sale
                                                 );
                                                
                                                 foreach ($per_page_arr as $key =>$value) {
                                                     $select = $value == $per_page?'selected':'';
                                                     echo '<option value="'.$value.'" '.$select.'>';
                                                     echo $key;
                                                     echo '</option>';
                                                 }

                                             ?>
                                         </select>
                                 </div>
                             </div>

                        </div>     
                    </div>   

                    <div class="clearfix api_height_15"></div>
<?php
    if ($_GET['mode'] == 'sample') {
        $temp = 'sale';
    } else {
        $temp = 'delivery';
    }

    $row_headers = array(
        'checkbox'      => [
                            'label' => '<input class="checkbox checkth skip" id="api_check_all" onclick="api_select_check_all(this,\'val[]\'); api_setValueCheckbox(\'val[]\',\'check_value\',\'action-form\',\'-\'); api_calculate_sum_all(\'api_check_all\');" type="checkbox" name="check"/>',
                            'sort_fn' => false,
                            'style' => 'min-width:30px; width: 30px; text-align: center;'
                        ],
                        'date'          => ['label' => lang("date"), 'style' => ''],
                        'reference_no'  => ['label' => lang("reference_no"), 'style' => ''],
                        'customer'      => ['label' => lang("customer"), 'style' => ''],
                        'parent_company'=> ['label' => lang("Parent_Company"), 'style' => '', 'sort_fn' => false],
                        'sale_status'   => ['label' => lang("sale_status"), 'style' => ''],
                        'grand_total'   => ['label' => lang("grand_total"), 'style' => ''],
                        'delivery_date' => ['label' => lang("Delivery_Date"), 'style' => ''],
                        'payment_status'=> ['label' => lang("payment_status"), 'style' => ''],
                        'paid_by'    => ['label' => lang("paid_by"), 'style' => '','sort_fn' => false],
                        // 'paid_by'    => ['label' => lang("paid_by"), 'style' => '','sort_fn' => false],
                        $temp.'_person' => ['label' => lang($temp), 'style' => ''],
                        'action'        => [
                        'label' => lang("actions"),
                        'style' => 'width:80px; text-align:center;',
                        'sort_fn' => false
                    ],
    );
?>

                    <table id="SLData" class="table table-bordered table-hover table-striped" cellpadding="0" cellspacing="0" border="0">
                        <thead>
                        <tr class="primary">

                            <?php foreach ($row_headers as $key => $row) :?>
                                <?php
                            
                                $th_class="class='pointer'";
                                $e_click = true;
                                if (isset($row['sort_fn']) && $row['sort_fn'] == false) {
                                    $th_class="";
                                    $e_click = false;
                                }
                                $style = isset($row['style']) && $row['style'] != '' ? 'style="'.$row['style'].'"' : '';
                                ?>
                                <th <?=$th_class?> <?= $style ?> >
                                    <?php if ($e_click) :?>
                                        <?php if ($sort_by == $key && $sort_order == "ASC") : ?>
                                                <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort"  aria-hidden="true">
                                                <?php echo $row['label']; ?>
                                                    <span class="fa fa-sort-down"></span>
                                                </label>
                                        <?php elseif ($sort_by == $key and $sort_order=="DESC") : ?>    
                                            <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'ASC')" id="fa_sort_up"  aria-hidden="true">
                                                <?php echo $row['label']; ?>
                                                <span class="fa fa-sort-up"></span>
                                            </label>
                                        <?php else : ?> 
                                            <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort_down" aria-hidden="true">
                                                <?php echo $row['label']; ?>
                                                <span class="fa fa-sort"></span>
                                            </label>
                                        <?php endif; ?> 
                                    <?php else : ?>
                                        <label class="font-normal" aria-hidden="true">
                                            <?php echo $row['label']; ?>
                                        </label>
                                    <?php endif; ?> 
                                </th>
                            <?php endforeach; ?>

                            </th>
                        </tr>
                        </thead>
                        <tbody role="alert" aria-live="polite" aria-relevant="all">

<?php
$temp_grand_total = 0;
$temp = (string)(date("Y-m-d"));
$now = date_create($temp);
$temp_now = date_format($now, 'Y-m-d');
for ($i=0;$i<count($select_data);$i++) {
    $config_data = array(
        'table_name' => 'sma_payments',
        'select_table' => 'sma_payments',
        'translate' => '',
        'select_condition' => "sale_id = ".$select_data[$i]["id"]." order by id asc",
    );
    $temp_payment2 = $this->site->api_select_data_v2($config_data);

    $temp_date = date_create($select_data[$i]['date']);
    $temp_date = date_format($temp_date, 'Y-m-d');

    if ($select_data[$i]['disabled'] == 1) {
        $temp_disabled_background = 'background-color:#dfdfdf !important';
        $temp_check = '';
        $temp3 = $this->sma->formatMoney(0);
        $temp4 = $this->sma->formatMoney(0);
    } else {
        if ($temp_date > $temp_now) {
            $temp_disabled_background = 'background-color:#ffd599 !important';
        } else {
            $temp_disabled_background = '';
        }

        $temp_check = '
            <div class="text-center">
                <input class="checkbox multi-select skip" type="checkbox" name="val[]" value="'.$select_data[$i]['id'].'" id="api_list_check_'.$select_data[$i]['id'].'" onclick="api_setValueCheckbox(\'val[]\',\'check_value\',\'action-form\',\'-\'); api_calculate_sum(\'api_list_check_'.$select_data[$i]['id'].'\')"/>
            </div>        
        ';
    }
    echo '
    <tr id="'.$select_data[$i]['id'].'" class="invoice_link">
        <td style="min-width:30px; width: 30px; text-align: center; '.$temp_disabled_background.'">
            '.$temp_check.'
        </td>
        <td align="center" class="api_td_width_auto" style="'.$temp_disabled_background.'">
            '.date('d/m/Y', strtotime($select_data[$i]['date'])).'
            </br>
            '.date('H:i:s', strtotime($select_data[$i]['date'])).'
        </td>
    ';
    
    $temp_display = '';
    if ($_GET['mode'] != 'sample') {
        if ($select_data[$i]['reference_no'] != '') {
            $temp = '';
            if ($select_data[$i]['do_sl_reference_no'] != '') {
                $temp = '
                    <div class="api_padding_top_5">
                        <span class="label label-default">
                        '.$select_data[$i]['do_sl_reference_no'].'
                        </span>
                    </div>                                                            
                    ';
            } elseif ($select_data[$i]['do_to_sl_reference_no'] != '') {
                $temp = '
                    <div class="api_padding_top_5">
                        <span class="label label-default">
                        '.$select_data[$i]['do_to_sl_reference_no'].'
                        </span>
                    </div>                                                            
                    ';
            }

            $temp2 = '';
            /*
            if ($select_data[$i]['reference_no'] != '') {
                if ($select_data[$i]['date'] > $select_data[$i]['generate_date'])
                    $temp2 = '<br>'.date('d/m/Y', strtotime($select_data[$i]['date']));
                else
                    $temp2 = '<br>'.date('d/m/Y', strtotime($select_data[$i]['generate_date']));
            }
            else
                $temp2 = '';
            */

            $temp_display = '
                    '.$select_data[$i]['reference_no'].'
                    '.$temp2.'
                    '.$temp.'
                ';
        } else {
            $temp_display = '
                    '.anchor('admin/sales/generate_ref_no/'.$select_data[$i]['id'], '<button type="button" class="btn btn-info"><li class="fa fa-pencil-square-o"></li> '.lang('generate').'</button>', 'data-toggle="modal" data-target="#myModal"').'
                ';
        }
        
    }
    
    if ($select_data[$i]['cashier'] > 0) {
        $config_data = array(
        'table_name' => 'sma_cashier',
        'select_table' => 'sma_cashier',
        'field_name' => 'translate',
        'translate' => 'yes',
        'select_condition' => "id =  ".$select_data[$i]['cashier'],
        );
        $select_cashier_id = $this->site->api_select_data_v2($config_data);
        for($k=0;$k<count($select_cashier_id);$k++) {
            $cashier_name = "<label class='label label-success' style='background-color:#8800a5 !important;'> Cashier : ".$select_cashier_id[$k]['title_en']."</lable>";
        }
    } else {
        $cashier_name = '';
    }
    
    echo '
        <td class="api_td_width_auto" align="center" style="'.$temp_disabled_background.'">
            '.$temp_display.' <br>
            '.$cashier_name.'
        </td>
    ';

    $temp = $this->site->api_select_some_fields_with_where(
        "
        parent_id,company
        ",
        "sma_companies",
        "id = ".$select_data[$i]['customer_id'],
        "arr"
    );
    echo '
        <td style="'.$temp_disabled_background.'">
            '.$select_data[$i]['customer'].'<br>
            <label class="label-warning api_color_white api_padding_left_3 api_padding_right_3">'.$temp[0]['company'].'</label>
        </td>
    ';

    $temp_2 = array();
    if (count($temp) > 0) {
        $temp_2 = $this->site->api_select_some_fields_with_where(
            "
            company
            ",
            "sma_companies",
            "id = ".$temp[0]['parent_id'],
            "arr"
        );
    }

    echo '
        <td style="'.$temp_disabled_background.'">
            '.$temp_2[0]['company'].'
        </td>
    ';
    $sale_status = $select_data[$i]['sale_status'];
    $temp_display = '';
    if ($sale_status == 'pending') {
        //$temp_display .= '<span class="label label-warning">'.$sale_status.'</span>';

    } elseif ($sale_status == 'completed' || $sale_status == 'paid' ||
        $sale_status == 'sent' || $sale_status == 'received') {
        $temp_display .= '<span class="label label-success">'.$sale_status.'</span>';
    } elseif ($sale_status == 'partial' || $sale_status == 'transferring' ||
        $sale_status == 'ordered') {
        $temp_display .= '<span class="label label-info">'.$sale_status.'</span>';
    } elseif ($sale_status == 'due' || $sale_status == 'returned') {
        $temp_display .= '<span class="label label-danger">'.$sale_status.'</span>';
    } elseif ($sale_status == 'print needed') {
        $temp_display .= '<span class="label label-info" style="background-color:#8800a5 !important;">'.$sale_status.'</span>';
    } elseif ($sale_status == 'preparing') {
        $temp_display .= '<span class="label label-info">'.$sale_status.'</span>';
    } elseif ($sale_status == 'delivering') {
        $temp_display .= '<span class="label label-info" style="background-color:#004f93 !important;">'.$sale_status.'</span>';
    } else {
        $temp_display .= '<span class="label label-default">'.$sale_status.'</span>';
    }

    $temp = $this->site->api_get_consignment_sale_items($select_data[$i]['id']);
    if (count($temp) > 0) {
        $temp2 = '';
        for ($i2=0;$i2<count($temp);$i2++) {
            $temp2 .= '-'.$temp[$i2]['consignment_id'];
        }
        $temp_display .= '
            <div style="padding-top:3px;">
                <a class="api_link_box_none" href="'.base_url().'admin/consignment?adjustment_consignment_track='.$temp2.'" target="_blank">
                    <span class="label label-default">'.lang('Consignment').'</span>
                </a>
            </div>
        ';
    }
      
    $temp = $this->site->api_select_some_fields_with_where(
        "*",
        "sma_returns",
        "add_ons like '%:sale_id:{".$select_data[$i]['id']."}:%'",
        "arr"
    );
    if (count($temp) > 0) {
        $temp_display .= '            
                <div class="api_padding_top_2">
                    <a class="api_link_box_none" href="'.base_url().'admin/returns?search='.$select_data[$i]['id'].'" target="_blank" title="'.lang('Returned_Items').'"> 
                        <span class="label label-danger">R</span>
                    </a>
                </div>
            ';
    }

    //if ($select_data[$i]['disabled'] == 1) $temp_display = '';
    echo '
        <td style="'.$temp_disabled_background.'">
            <div class="text-center">
                '.$temp_display.'
            </div>
        </td>
    ';
    echo '
        <td align="right" style="'.$temp_disabled_background.'">
            '.$this->sma->formatMoney($select_data[$i]['grand_total']).'
        </td>
    ';

    $temp = '';
    if ($select_data[$i]['delivery_date'] != '') {
        $temp = date('d/m/Y H:i:s', strtotime($select_data[$i]['delivery_date']));
    }
    echo '
        <td style="'.$temp_disabled_background.'">
            <div class="text-center">
                '.$temp.'
            </div>
        </td>
    ';

    if ($select_data[$i]['payment_status'] == 'pending') {
        $temp = '<span class="label label-warning">'.$select_data[$i]['payment_status'].'</span>';
    } elseif ($select_data[$i]['payment_status'] == 'completed' || $select_data[$i]['payment_status'] == 'paid' || $select_data[$i]['payment_status'] == 'sent' || $select_data[$i]['payment_status'] == 'received') {
        $temp = '<span class="label label-success">'.$select_data[$i]['payment_status'].'</span>';
    } elseif ($select_data[$i]['payment_status'] == 'partial' || $select_data[$i]['payment_status'] == 'transferring' || $select_data[$i]['payment_status'] == 'ordered') {
        $temp = '<span class="label label-info">'.$select_data[$i]['payment_status'].'</span>';
    } elseif ($select_data[$i]['payment_status'] == 'due' || $select_data[$i]['payment_status'] == 'returned') {
        $temp = '<span class="label label-danger">'.$select_data[$i]['payment_status'].'</span>';
    } elseif ($select_data[$i]['payment_status'] == 'AR') {
        $temp = '<span class="label label-primary">'.$select_data[$i]['payment_status'].'</span>';
    } else {
        $temp = '<span class="label label-default">'.$select_data[$i]['payment_status'].'</span>';
    }
    //if ($select_data[$i]['disabled'] == 1) $temp = '';

    echo '
        <td style="'.$temp_disabled_background.'">
            <div class="text-center">
                '.$temp.'
            </div>
        </td>
    ';

    $temp_display = '';
    if ($_GET['mode'] == 'sample') {
        if ($select_data[$i]['sample_sale_person'] != '') {
            $temp = $this->site->api_select_some_fields_with_where(
                "
            first_name
                ",
                "sma_users",
                "id = ".$select_data[$i]['sample_sale_person'],
                "arr"
            );
            $temp_display = $temp[0]['first_name'];
            
        }
    } else {
        if ($select_data[$i]['delivery_person'] != '') {
            $temp = $this->site->api_select_some_fields_with_where(
                "
            getTranslate(translate,'en','".f_separate."','".v_separate."') as title 
                ",
                "sma_delivery_person",
                "id = ".$select_data[$i]['delivery_person'],
                "arr"
            );
            $temp_display = $temp[0]['title'];
        }
        if ($select_data[$i]['shipping'] > 0)
            $temp_display .= ' 
                <div  title="Delivery Fee">
                <span class="label" style="background-color: #F0989B; font-size: 12px;">'.$this->sma->formatMoney($select_data[$i]['shipping']).'</span></div>
            ';     
    }
    
    $detail_link = anchor('admin/sales/view/'.$select_data[$i]['id'], '<i class="fa fa-file-text-o"></i> ' . lang('sale_details'));
    $duplicate_link = anchor('admin/sales/add?sale_id='.$select_data[$i]['id'], '<i class="fa fa-plus-circle"></i> ' . lang('duplicate_sale'));
    $payments_link = anchor('admin/sales/payments/'.$select_data[$i]['id'], '<i class="fa fa-money"></i> ' . lang('view_payments'), 'data-toggle="modal" data-target="#myModal"');
    $add_payment_link = anchor('admin/sales/add_payment/'.$select_data[$i]['id'], '<i class="fa fa-money"></i> ' . lang('add_payment'), 'data-toggle="modal" data-target="#myModal"');
    $packagink_link = anchor('admin/sales/packaging/'.$select_data[$i]['id'], '<i class="fa fa-archive"></i> ' . lang('packaging'), 'data-toggle="modal" data-target="#myModal"');
    $add_delivery_link = anchor('admin/sales/add_delivery/'.$select_data[$i]['id'], '<i class="fa fa-truck"></i> ' . lang('add_delivery'), 'data-toggle="modal" data-target="#myModal"');
    $print_pdf_link = anchor('admin/sales/pdf/'.$select_data[$i]['id'], '<i class="fa fa-print"></i> ' . lang('Print_PDF'), '');

    echo '
        <td style="'.$temp_disabled_background.'">
            <div class="text-center col-payment">';
    
                    for($j=0;$j<count($temp_payment2);$j++) {
                        if ($temp_payment2[$j]['paid_by'] != NULL || $temp_payment2[$j]['paid_by'] != "") {
                            if ($temp_payment2[$j]['paid_by'] == 'aba') {
                                echo '<span class="label label-primary">'.lang($temp_payment2[$j]['paid_by']).'</span>';
                            } elseif ($temp_payment2[$j]['paid_by'] == 'aba_daiki') {
                                echo '<div class="label label-primary" style="background-color:#ea1414 !important;color:white">'.lang('ABA_Daiki').'</div>';
                            } elseif ($temp_payment2[$j]['paid_by'] == 'aba_qr') {
                                echo '<span class="label label-primary" style="background-color:#005e7b !important;color:white">'.lang('ABA_QRcode').'</span>';
                            } elseif ($temp_payment2[$j]['paid_by'] == 'pipay') {
                                echo '<span class="label label-primary" style="background-color:#ef238b !important;color:white">'.lang('PiPay').'</span>';
                            } 
                            elseif ($temp_payment2[$j]['paid_by'] == 'wing') {
                                echo '<span class="label label-primary" style="background-color:#b1c11f !important;color:white">'.lang('Wing').'</span>';
                            } 
                            elseif ($temp_payment2[$j]['paid_by'] == 'AR' && count($temp_payment2) == 1) {
                                echo '<span class="label label-warning" style="background-color:#8800a5 !important;color:8800a5">'.lang('Pending').'</span>';
                                echo '<div class="display_amount api_padding_bottom_15">'.$this->sma->formatMoney($temp_payment2[$j]['amount']).'</div>';
                            } elseif ($temp_payment2[$j]['paid_by'] == 'cash') {
                                echo '<span class="label label-warning">'.lang('cash').'</span>';
                                echo '<div class="display_amount api_padding_bottom_15">'.$this->sma->formatMoney($temp_payment2[$j]['amount']).'</div>';
                            } elseif ($temp_payment2[$j]['paid_by'] == 'pending') {
                                echo '<span class="label label-warning" style="background-color:#8800a5 !important; border-color:#8800a5 !important;">'.lang('Pending').'</span>';
                            } elseif ($temp_payment2[$j]['paid_by'] == 'payway') {
                                echo '<span class="label label-primary" style="background-color:#005e7b !important;color:white">'.lang('Payway').'</span>';                             
                            } elseif ($temp_payment2[$j]['paid_by'] == 'CC') {
                                echo '<span class="label label-info">'.lang('Credit_Card').'</span>';                             
                                echo '<div class="display_amount api_padding_bottom_15">'.$this->sma->formatMoney($temp_payment2[$j]['amount']).'</div>';
                            } 
                            elseif ($temp_payment2[$j]['paid_by'] == 'wing_daiki') {
                                echo '<span class="label label-primary" style="background-color:#929f1b !important;color:white">'.lang('Wing_Daiki').'</span>';
                            }                
                            elseif ($temp_payment2[$j]['paid_by'] == 'acode_daiki') {
                                echo '<span class="label label-primary" style="background-color:#e20688 !important;color:white">'.lang('Acode_Daiki').'</span>';
                            }                                               
                            else {                                
                                if ($temp_payment2[$j]['paid_by'] != 'AR') {
                                    echo '<span class="label label-info">'.$temp_payment2[$j]['paid_by'].'</span>';
                                    echo '<div class="display_amount api_padding_bottom_15">'.$this->sma->formatMoney($temp_payment2[$j]['amount']).'</div>';
                                }
                            } 
                        } else {
                            echo '<span class="label label-danger">'.lang('Undefined').'</span>';
                        } 
                        if ($temp_payment2[$j]['paid_by'] == 'paypal' && $temp_payment2[$j]['transaction'] != '') {
                            echo '<br><span class="label display_amount" style="font-weight: 100;background-color:transparent;color:black; font-size:14px">TID: '.($temp_payment2[$j]['transaction_id']!=''?$temp_payment2[$j]['transaction_id']:"<span style='color:red'>NULL</span>").'</span>';
                            echo '<div class="display_amount_2 api_padding_bottom_15">'.$this->sma->formatMoney($temp_payment2[$j]['amount']).'</div>';
                        }
                        if ($temp_payment2[$j]['paid_by'] == 'aba' && $temp_payment2[$j]['paid_by'] == 'acleda') {
                            echo '<br/><span class="label display_amount" style="font-weight: 100;background-color:transparent;color:black; font-size:14px">TID: '.($temp_payment2[$j]['transfer_reference_number']!=''?$temp_payment2[$j]['transfer_reference_number']:"<span style='color:red'>NULL</span>").'</span>';
                            echo '<div class="display_amount_2 api_padding_bottom_15">'.$this->sma->formatMoney($temp_payment2[$j]['amount']).'</div>';
                        }
                        if ($temp_payment2[$j]['paid_by'] == 'aba_qr') {
                            echo '<br/><span class="label display_amount" style="font-weight: 100;background-color:transparent;color:black; font-size:14px">TID: '.($temp_payment2[$j]['aba_qr_transfer_reference_number']!=''?$temp_payment2[$j]['aba_qr_transfer_reference_number']:"<span style='color:red'>NULL</span>").'</span>';
                            echo '<div class="display_amount_2 api_padding_bottom_15">'.$this->sma->formatMoney($temp_payment2[$j]['amount']).'</div>';
                        }
                        if ($temp_payment2[$j]['paid_by'] == 'pipay') {
                            echo '<br/><span class="label display_amount" style="font-weight: 100;background-color:transparent;color:black; font-size:14px">TID: '.($temp_payment2[$j]['pipay_transaction_id']!=''?$temp_payment2[$j]['pipay_transaction_id']:"<span style='color:red'>NULL</span>").'</span>';
                            echo '<div class="display_amount_2 api_padding_bottom_15">'.$this->sma->formatMoney($temp_payment2[$j]['amount']).'</div>';
                        }
                        if ($temp_payment2[$j]['paid_by'] == 'wing') {
                            echo '<br/><span class="label display_amount" style="font-weight: 100;background-color:transparent;color:black; font-size:14px">TID: '.($temp_payment2[$j]['wing_transaction_id']!=''?$temp_payment2[$j]['wing_transaction_id']:"<span style='color:red'>NULL</span>").'</span>';
                            echo '<div class="display_amount_2 api_padding_bottom_15">'.$this->sma->formatMoney($temp_payment2[$j]['amount']).'</div>';
                        }
                        if ($temp_payment2[$j]['paid_by'] == 'payway') {
                            echo '<br/><span class="label display_amount" style="font-weight: 100;background-color:transparent;color:black; font-size:14px">TID: '.($temp_payment2[$j]['payway_transaction_id']!=''?$temp_payment2[$j]['payway_transaction_id']:"<span style='color:red'>NULL</span>").'</span>';
                            echo '<div class="display_amount_2 api_padding_bottom_15">'.$this->sma->formatMoney($temp_payment2[$j]['amount']).'</div>';
                        }
                        if ($temp_payment2[$j]['paid_by'] == 'aba_daiki') {
                            echo '<br/><div class="label display_amount" style="font-weight: 100;background-color:transparent;color:black; font-size:14px">TID: '.($temp_payment2[$j]['aba_daiki_transaction_id']!=''?$temp_payment2[$j]['aba_daiki_transaction_id']:"<span style='color:red'>NULL</span>").'</div>';
                            echo '<div class="display_amount_2 api_padding_bottom_15">'.$this->sma->formatMoney($temp_payment2[$j]['amount']).'</div>';
                        }
                        if ($temp_payment2[$j]['paid_by'] == 'aba') {
                            echo '<br/><div class="label display_amount" style="font-weight: 100;background-color:transparent;color:black; font-size:14px">TID: '.($temp_payment2[$j]['transfer_reference_number']!=''?$temp_payment2[$j]['transfer_reference_number']:"<span style='color:red'>NULL</span>").'</div>';
                            echo '<div class="display_amount_2 api_padding_bottom_15">'.$this->sma->formatMoney($temp_payment2[$j]['amount']).'</div>';
                        }
                        if ($temp_payment2[$j]['paid_by'] == 'wing_daiki') {
                            echo '<br/><div class="label display_amount" style="font-weight: 100;background-color:transparent;color:black; font-size:14px">TID: '.($temp_payment2[$j]['wing_daiki_transaction_id']!=''?$temp_payment2[$j]['wing_daiki_transaction_id']:"<span style='color:red'>NULL</span>").'</div>';
                            echo '<div class="display_amount_2 api_padding_bottom_15">'.$this->sma->formatMoney($temp_payment2[$j]['amount']).'</div>';
                        }
                        if ($temp_payment2[$j]['paid_by'] == 'acode_daiki') {
                            echo '<br/><div class="label display_amount" style="font-weight: 100;background-color:transparent;color:black; font-size:14px">TID: '.($temp_payment2[$j]['acode_daiki_transaction_id']!=''?$temp_payment2[$j]['acode_daiki_transaction_id']:"<span style='color:red'>NULL</span>").'</div>';
                            echo '<div class="display_amount_2 api_padding_bottom_15">'.$this->sma->formatMoney($temp_payment2[$j]['amount']).'</div>';
                        }                                                
                    } // end of loop $temp_payment2
         
                    

    echo '</div>
        </td>
    ';
    echo '
        <td style="'.$temp_disabled_background.'">
            '.$temp_display.'
        </td>
    ';
    
    $set_delivering = '
        <a href="'.base_url().'admin/sales/set_delivering/'.$select_data[$i]['id'].'" data-toggle="modal" data-target="#myModal">
            <i class="fa fa-truck"></i> '.lang('set_delivering').'
        </a>
    ';
    
    $email_link = anchor(
        'admin/sales/email/'.$select_data[$i]['id'],
        '<i class="fa fa-envelope"></i> ' . lang('email_sale'),
        'data-toggle="modal" data-target="#myModal"'
    );

    if ($select_data[$i]['sale_status'] == 'delivering' || $select_data[$i]['sale_status'] == 'completed') {
        $edit_link = '
            <a class="sledit" href="javascript:void(0);" onclick="api_confirm_report(\''.base_url().'admin/sales/edit/'.$select_data[$i]['id'].'\')">
                <i class="fa fa-edit"></i> '.lang('edit_sale').'
            </a>
        ';
        $edit_link_sample = '
            <a class="sledit" href="javascript:void(0);" onclick="api_confirm_report(\''.base_url().'admin/sales/edit/'.$select_data[$i]['id'].'?mode=sample\')">
                <i class="fa fa-edit"></i> '.lang('edit_sale').'
            </a>
        ';
    } else {
        $edit_link = anchor('admin/sales/edit/'.$select_data[$i]['id'], '<i class="fa fa-edit"></i> ' . lang('edit_sale'), 'class="sledit"');
        $edit_link_sample = anchor('admin/sales/edit/'.$select_data[$i]['id'].'?mode=sample', '<i class="fa fa-edit"></i> ' . lang('edit_sale'), 'class="sledit"');
    }

    $pdf_do_link = anchor('admin/sales/pdf/'.$select_data[$i]['id'].'?type=do', '<i class="fa fa-file-pdf-o"></i> ' . lang('Download_DO_Format_PDF'));
    $pdf_sl_link = anchor('admin/sales/pdf/'.$select_data[$i]['id'].'?type=sl', '<i class="fa fa-file-pdf-o"></i> ' . lang('Download_SL_Format_PDF'));

    $pos_print_link = anchor('admin/pos/view/'.$select_data[$i]['id'].'?print='.$select_data[$i]['id'], '<i class="fa fa-print"></i> ' . lang('Print_POS'));

    $view_return_link = anchor(
        'admin/returns?search='.$select_data[$i]['id'],
        '<i class="fa fa-eye"></i> ' . lang('view_return')
    );
    $return_link = anchor(
        'admin/sales/return_sale_v2/'.$select_data[$i]['id'],
        '<i class="fa fa-minus-circle"></i> ' . lang('return_sale')
    );
    $change_sl_to_do_link = anchor(
        'admin/sales/change_sl_to_do/'.$select_data[$i]['id'],
        '<i class="fa fa-plus-circle"></i> ' . lang('Change_SL_to_DO')
    );
    $change_do_to_sl_link = anchor(
        'admin/sales/change_do_to_sl/'.$select_data[$i]['id'],
        '<i class="fa fa-plus-circle"></i> ' . lang('Change_DO_to_SL')
    );

    $delete_link = "
        <a href='#' class='po' 
            title='<b>".lang("delete_sale")."</b>' 
            data-content=\"
                <p>".lang('r_u_sure')."</p>
                <a class='btn btn-danger' href='".admin_url('sales/delete/'.$select_data[$i]['id'])."'>
                    ".lang('i_m_sure')."
                </a> 
                <button class='btn po-close'>".lang('no')."</button>\" 
                data-html=\"true\" data-placement=\"left\">
            <i class='fa fa-trash-o'></i>
            ".lang('delete_sale')."
        </a>
    ";
    /*
                <li>
                    <a href="javascript:void(0);" onclick="window.open(\''.base_url().'admin/sales/pdf/'.$select_data[$i]['id'].'?print=1\', \'_blank\'); window.location = \''.base_url().'admin/sales\'">
                        <i class="fa fa-print"></i> '.lang('Print_PDF').'
                    </a>
                </li>
    */
    $temp_display = '';
    if ($_GET['mode'] != 'sample') {
        $temp_display .= '
            <li>'.$edit_link.'</li>
            <li>'.$duplicate_link.'</li>
            <li>'.$payments_link.'</li>
            <li>'.$add_payment_link.'</li>
            <li>'.$print_pdf_link.'</li>
            <li>'.$pos_print_link.'</li>
            <li>'.$delete_link.'</li>
        ';
    } else {
        $temp_display .= '
            <li>'.$edit_link_sample.'</li>
            <li>'.$pdf_link.'</li>
            <li>'.$delete_link.'</li>
        ';
    }
    
    echo '
        <td style="width:80px; text-align:center; '.$temp_disabled_background.'">
    ';
    if ($select_data[$i]['disabled'] != 1) {
        echo '
            <div class="text-center">
                <div class="btn-group text-left">
                    <button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" 
                            data-toggle="dropdown">
                        '.lang('actions').'
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        '.$temp_display.'
                    </ul>
                </div>
            </div>
        ';
    }
    echo '
        </td>
    ';

    echo '
    </tr>
    ';
    $temp_grand_total += $select_data[$i]['grand_total'];
}
if (count($select_data) <= 0) {
    echo '
    <tr>
    <td colspan="11">
        '.lang('No_record_found').'
    </td>
    </tr>
    ';
}
echo '
    </tbody>
';
?>                            
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft skip" type="checkbox" name="check" onclick="api_select_check_all(this,'val[]'); api_setValueCheckbox('val[]','check_value','action-form','-')"/>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel-heading-left">
                                <p class="data-total">
                                    <?php echo $show.' ';?>
                                </p>
                            </div>   
                            <div class="panel-heading-right">
                                <p>
                                   <?php echo $this->pagination->create_links(); //pagination links?>
                                </p>
                            </div>  
                            <div class="panel-heading-clear-fix"></div>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="show_data" value="<?php echo $show_data;?>" id="show_data"/>
<input type="hidden" name="sort_by" value="<?php echo $sort_by;?>" id="sort_by"/>
<input type="hidden" name="sort_order" value="<?php echo $sort_order;?>" id="sort_order"/>
<?= form_close() ?>

<?php

echo admin_form_open('sales/sale_actions', 'id="api_form_action" name="api_form_action" method="post"');
echo '
    <div class="api_display_none">
        <input type="text" name="check_value" value="" id="check_value"/>
        <input type="text" name="api_action" value="" id="api_action"/>
        <button type="button" onclick="document.api_form_action.submit();" value="ok">ok</button>
    </div>
';
echo form_close();

?>



<script>
    function api_bulk_actions(action){
        $("#api_action").val(action);
        document.api_form_action.submit();
    } 
    function bulk_actions_delete(){
        $("#api_action").val('delete');
        document.api_form_action.submit();
    }
    function bulk_actions_generate_reference_no(){
        $("#api_action").val('generate_reference_no');
        document.api_form_action.submit();
    }     
    function api_bulk_actions_print_pdf(action){    
        $("#api_form_action").attr('target', '_blank');
        $('#api_action').val(action);
        document.api_form_action.submit(); 
    }
    function api_bulk_actions_set_delivery(frm,chkname,url){
        var nocheck = true;
        var selected_id = 0;
        var temp = '';
        
        eles = document.forms[frm].elements;
        for (var i=0;i<eles.length;i++){
            if (eles[i].name == chkname){
                if(eles[i].checked==true){
                    selected_id = eles[i].value;
                    nocheck = false;
                    temp = temp + eles[i].value + '-';
                }
            } 
        }
        if (nocheck == true)
            window.location = '<?= base_url() ?>admin/sales/no_sale_seleted';
        else {
            $("#set_delivering").attr('href', '<?= base_url() ?>admin/sales/set_delivering_bulk/' + temp);
            $('#set_delivering').click();
        }    
    }

    function api_confirm_report(temp) {
        $('#api_swal3-question').show();
        $('#api_swal3-warning').hide();
        $('#api_swal3-info').hide();
        $('#api_swal3-success').hide();
        $('#api_modal_2_title').html('Have you reported ?');
        $('#api_modal_2_body').html('Have you already reported to accounting team ?');

        $('#api_modal_2_btn_ok').show();
        $('#api_modal_2_btn_ok').html('<button id="" type="button" onclick="window.location = \'' + temp + '\'" role="button" tabindex="0" class="api_swal3-cancel api_swal3-styled btn-primary" data-dismiss="modal" >Yes</button>');

        $('#api_modal_2_btn_close').show();
        $('#api_modal_2_btn_close').html('<button id="" type="button" onclick="api_confirm_report_no()" role="button" tabindex="0" class="api_swal3-cancel api_swal3-styled btn-danger" data-dismiss="modal" style="">No</button>');

        $('#api_modal_2_trigger').click();
    }
    function api_confirm_report_no() {
        $('#api_swal3-question').hide();
        $('#api_swal3-warning').show();
        $('#api_swal3-info').hide();
        $('#api_swal3-success').hide();
        $('#api_modal_2_title').html('Please Report');
        $('#api_modal_2_body').html('Please report to account team first.');

        $('#api_modal_2_btn_ok').hide();

        $('#api_modal_2_btn_close').show();
        $('#api_modal_2_btn_close').html('<button id="" type="button" role="button" tabindex="0" class="api_swal3-cancel api_swal3-styled btn-warning" data-dismiss="modal" style="">Close</button>');

        $('#api_modal_2_trigger').click();
    }

    function formactionsubmit(sort_by, sort_order) {
        document.getElementById("sort_by").value = sort_by;
        document.getElementById("sort_order").value = sort_order;
        document.forms[0].submit();
    }
    function api_form_submit(action){
        $("#action-form").attr('action', action);
        $('#form_action').val('');
        $("#action-form").submit();
    }         
</script>


<script type="text/javascript">
    
var initAppendingDOM;
var inc = 0;
/*******************
 * Author: TEP Afril
 * 18th May, 2018
 * odadcambodia.com
********************/

    var grand_total = Number(0);
    var paid = Number(0);
    var balance = Number(0);
    var arrIndex = [];

    var updateData = function(){
        var temp = numberWithCommas(grand_total);
        temp = temp.replace("-",""); 
        $('#custom_g_t').html(temp);
    };

    numberWithCommas = (x) => {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    var sumData = function( gbl, vbl, sign ){
        gbl = Number(gbl);
        vbl = vbl.replace(/\$/g,'');
        vbl = vbl.replace(/\,/g,'');
        vbl = Number(vbl);

        switch(sign)
        {
            case "+":
                gbl += (Number(vbl));
            break;

            case "-":
                gbl -= (Number(vbl));
            break;
        }
        gbl = gbl.toFixed(2);
        return gbl;
    };

    var resetData = function(){
        grand_total = '0.00';
        paid = '0.00';
        balance = '0.00';
        updateData();
    };

    initAppendingDOM = function(gt,pd,bl,idx){
        domStr = '<div style="width:20vw;position:fixed;top:50px;right:280px;z-index:1000;opacity:100">'+
        '<a id="expand_sum_table" class="btn btn-success btn-sm" style="top:45px;right:30px;position:fixed;"><i style="" class="fa fa-2x fa-arrows-alt"></i></a>'+
        '<div id="sum_table_container" style="display: none;">'+
        '<a id="close_sum_table" class="btn btn-sm btn-danger" style="padding: 2px 5px;cursor:pointer;top:45px;right:270px;position:fixed;"><i class="fa fa-times"></i></a>'+
        '<table  class="table table-bordered table-hover table-striped dataTable">'+
        '<thead>'+
        '<tr>'+
        '<td style="background-color:#333;border-color:#000;">'+gt+'</td>'+
        '</tr>'+
        '</thead>'+
        '<tbody>'+
        '<tr style="font-weight:bold;">'+
        '<td id="custom_g_t" style="background-color:#d1d1d1;">0.00</td>'+
        '</tr>'+
        '</tbody>'+
        '</table>'+
        '</div>'+
        '</div>';
        $('.box').append(domStr);
        arrIndex = idx;
    };

    $('body').on('ifChecked','.icheckbox_square-blue',function(){
        inc++;

        var row = $(this).parent().parent().parent();
        var td_g_t = $(row[0]).children().eq(arrIndex[0]);
        var td_paid = $(row[0]).children().eq(arrIndex[1]);
        var td_balance = $(row[0]).children().eq(arrIndex[2]);


        var selected_g_t = $(td_g_t).text();
        var selected_paid = $(td_paid).text();
        var selected_balance = $(td_balance).text();

        var temp = grand_total;

        grand_total = sumData(grand_total,selected_g_t,'+');
        paid = sumData(paid,selected_paid,'+');
        balance = sumData(balance,selected_balance,'+');

        console.log( inc + " : " + grand_total + " = " + temp + " + " + selected_g_t );

        updateData();
    });

    $('body').on('click','#close_sum_table',function(){
        $('#sum_table_container').hide(300,function(){
            $('#expand_sum_table').show(300);
        });
        });
        $('body').on('click','#expand_sum_table',function(){
        $('#expand_sum_table').hide(300,function(){
            $('#sum_table_container').show(300);
        });
    });

    $('body').on('change', 'select[name="POData_length"]', function (e) {
        resetData();
    });
    $('body').on('click', '.pagination a', function (e) {
        resetData();
    });

    $(document).ready(function(){
        initAppendingDOM('<?= lang("grand_total"); ?>','<?= lang("paid"); ?>','<?= lang("balance"); ?>',[6,7,8]);
    });

    function api_calculate_sum(id) {

        var row = $("#" + id).parent().parent().parent();

        var td_g_t = $(row[0]).children().eq(6);

        var selected_g_t = $(td_g_t).text();

        if ($("#" + id).is(":checked"))
            grand_total = sumData(grand_total,selected_g_t,'-');
        else
            grand_total = sumData(grand_total,selected_g_t,'+');

        updateData();
    }
    function api_calculate_sum_all(id) {
        var row = $("#" + id).parent().parent().parent();

        if ($("#" + id).is(":checked")) {            
            grand_total = <?php echo $temp_grand_total; ?>;
        }
        else {            
            grand_total = 0;
        }
        updateData();
    }

<?php
    $temp = $_GET['page'];
    if ($temp == '') {
        $temp = 1;
    }
    echo '
        $(".pagination .active").html(\'<a href="javascript:void(0);">'.$temp.'</a>\');
    ';
?>
    function api_change_payment_status_to_due() {
        var temp = "<?php echo base_url().'admin/sales/change_payment_status_to_due?check_value='; ?>" + document.getElementById('check_value').value;
        $("#api_change_payment_status_to_due").attr('href', temp);
        $('#api_change_payment_status_to_due').click();
    }
    $('table#CompTable tbody tr td a').on('click',function(){
        // $(this).parent('div.modal.fade.in').hide();
        console.log($(this));
    })


    // $('.col-payment a').each(function(){
    //     var url = $(this).attr('load-link')
    //     var target = $(this).attr('load-target')
        // $.ajax({
        //     url: url,
        //     success: function(data) {
        //         // for(var i=0;i<data.length;i++){
        //         //     console.log(data[i]);
        //         // }
        //         data!=''?$('a[load-target="'+target+'"]').html("<span class='label label-default'>"+data+"</span>"):$('a[load-target="'+target+'"]').html("<span class='label label-danger'>Undefined</span>")
        //     }
        // });
    // })

    $(function(){
        // let url = window.location.href
        // console.log(window.location.origin+window.location.pathname)
        // let url = '/admin/sales'
        let url = window.location.origin+window.location.pathname
        $('#transaction_search').on('click','#search_btn',function(){
            var param = $(this).parent().children('input')
            urls = url+'/get_tranc_sale?'+param.attr('name')+'='+param.val()
            $.get(urls,function(data){
                const new_href = data!=""? url+'?adjustment_sale_track='+data:url+'?adjustment_sale_track=-0'
                // console.log(new_href)
                location.assign(new_href)
            }).fail(function() {
                console.log("Error")
            })
        })
        $("#transaction_search").keypress(function(event) { 
            if (event.keyCode === 13) {
                $("#search_btn").click()
            }
        });

        // // add icon sort to th paid_by
        // console.log(paid_by.children('label'))
        // paid_by.children().attr('onclick','');
    })
    const getCellValue = (tr, idx) => tr.children[idx].innerText || tr.children[idx].textContent;
    const comparer = (idx, asc) => (a, b) => ((v1, v2) => v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2))(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));
    
    const paid_by = $('table#SLData thead tr.primary th:nth-child(10)');
    paid_by.attr('id','short_tras').append('<span class="fa fa-sort"></span>');
    paid_by.children('label').css('cursor', 'pointer');
    const table = paid_by.parents('table').children('tbody')[0];
    paid_by.click(function(){
        Array.from(table.querySelectorAll('tr:nth-child(n+1)'))
            .sort(comparer(9, this.children.asc = !this.children.asc))
            .forEach(tr => table.appendChild(tr));
    })

    // --> search tag by payment type
    function api_payment_type() {
        document.getElementById('action-form').submit();
    }
</script>

<style>
    .invoice_link td:nth-child(6){
        cursor: default;
    }
</style>

