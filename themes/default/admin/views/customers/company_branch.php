<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php 

if ($Owner) {
    $spage = $show_data != '' ? $show_data : 1;
    echo admin_form_open('customers/company_branch?page='.$spage, 'id="action-form" name="action-form"');
}
?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-building"></i><?= lang('company_branch'); ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?= admin_url('customers/company_branch_edit') ?>" data-toggle="modal" data-target="#myModal" id="add">
                                <i class="fa fa-plus-circle"></i> <?= lang('add_company_branch') ?>
                            </a>
                        </li>                    
                        <li class="divider"></li>
                        <li>
                            <a href="#" class="bpo" title="<b><?= $this->lang->line("delete") ?></b>"
                                data-content="<p><?= lang('r_u_sure') ?></p><button type='button' class='btn btn-danger' onclick='bulk_actions_delete();' data-action='delete'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button>"
                                data-html="true" data-placement="left">
                                <i class="fa fa-trash-o"></i> <?= lang('delete') ?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?= lang('list_results'); ?></p>

                <div class="table-responsive">
                        <div class="row">
                            <div class="sale-header">
                                <div class="col-md-12 api_padding_0">
                                    <div class="col-md-6">
                                		<div class="form-group">
                                            <label class="control-label" for="company">
                                			<?php echo lang("company"); ?></label>
                                            <div class="api_height_5"></div>
                                            <div class="controls"> 
                                				<?php
                                            $tr[''] = lang("All_companies");
                                            foreach ($company as $temp) {
                                                if ($temp->id != '')
                                                    $tr[$temp->id] = $temp->company;
                                            }
                                            echo form_dropdown('company', $tr, $_POST['company'], 'data-placeholder="'.lang("please_select_a_company").'" class="form-control" onchange="$(\'#action-form\').submit();"');                            
                                                ?>
                                            </div>
                                        </div>
                                	</div>
                                    <div class="api_clear_both api_height_15"></div>
                                </div>

                                <div class="col-md-6 text-left">
                                    <div class="short-result">
                                        <label>
                                            Show
                                        </label>
                                        <select id="sel_id" name="per_page" onchange="document.action-form.submit()">
                                            <?php
                                                $per_page_arr = array(
                                                    '10' => '10',
                                                    '25' => '25',
                                                    '50' => '50',
                                                    '100' => '100',
                                                    '200' => '200',
                                                    'All' => $total_rows_sale
                                                );
                                            
                                                foreach($per_page_arr as $key =>$value){
                                                    $select = $value == $per_page?'selected':'';
                                                    echo '<option value="'.$value.'" '.$select.'>';
                                                            echo $key;
                                                    echo '</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 text-right">
                                    <div class="short-result">
                                        <div id="SLData_filter" class="show-data-search dataTables_filter">
                                            <label>
                                                Search
                                                <input class="input-xs" value="<?php echo $this->input->post('search');?>" type="text" name="search" placeholder="search"/>
                                                <input style="height: 34px;" class="btn btn-primary" type="submit" name="submit_search" value="Submit">
                                            </label>
                                            <div id="result"></div>
                                        </div>
                                    </div>    
                                </div>	
                                <div class="clearfix"></div>
                             </div>
                        </div>     
                    </div> 
                
                    <table id="SLData cellpadding="0" cellspacing="0" border="0"
                           class="table table-bordered table-hover table-striped">
                        <thead>
<?php
    $row_headers = array(
        'checkbox'      => [
                            'label' => '<input class="checkbox checkft" type="checkbox" name="check"/>', 
                            'sort_fn' => false,
                            'style' => 'min-width:30px; width: 30px; text-align: center;'
                        ],
        'title'          => ['label' => lang("Branch_Name"), 'style' => 'text-align: left; width:auto;'],
        'parent_id'  => ['label' => lang("company"), 'style' => 'text-align: left;;width:auto;'],
        'branch_number'  => ['label' => lang("Branch_Number"), 'style' => 'text-align: left;;width:140px;'],
        'action'        => [
                            'label' => lang("actions"), 
                            'style' => 'width:80px; text-align:center;', 
                            'sort_fn' => false
                        ],
    );
?>

                        
                        <tr class="">
                                <?php foreach ($row_headers as $key => $row) :?>
                                    <?php
                                   
                                    $th_class="class='pointer'";
                                    $e_click = true;
                                    if(isset($row['sort_fn']) && $row['sort_fn'] == false) {
                                        $th_class="";
                                        $e_click = false;
                                    }
                                    $style = isset($row['style']) && $row['style'] != '' ? 'style="'.$row['style'].'"' : '';
                                    ?>
                                    <th <?= $style ?> >
                                        <?php if($e_click ) :?>
                                            <?php if ($sort_by == $key && $sort_order == "ASC") : ?>
                                                    <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort"  aria-hidden="true">
                                                    <?php echo '<span class="pointer">'.$row['label'].'</span>'; ?>
                                                        <span class="fa fa-sort-down"></span>
                                                    </label>
                                            <?php elseif ($sort_by == $key and $sort_order=="DESC") : ?>	
                                                <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'ASC')" id="fa_sort_up"  aria-hidden="true">
                                                    <?php echo '<span class="pointer">'.$row['label'].'</span>'; ?>
                                                    <span class="fa fa-sort-up"></span>
                                                </label>
                                            <?php else : ?>	
                                                <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort_down" aria-hidden="true">
                                                    <?php echo '<span class="pointer">'.$row['label'].'</span>'; ?>
                                                    <span class="fa fa-sort"></span>
                                                </label>
                                            <?php endif; ?>	
                                        <?php else : ?>
                                            <label class="font-normal" aria-hidden="true">
                                                <?php echo $row['label']; ?>
                                            </label>
                                        <?php endif; ?>	
                                    </th>
                                <?php endforeach; ?>

                        </tr>
                        </thead>
                        <tbody>
<?php
for ($i=0;$i<count($select_data);$i++) {
    echo '
        <tr id="'.$select_data[$i]['id'].'" class="api_table_link_company_branch">
        <td style="min-width:30px; width: 30px; text-align: center;">
            <div class="text-center">
                <input class="checkbox multi-select" type="checkbox" 
                    name="val[]" value="'.$select_data[$i]['id'].'" />
            </div>
        </td>
        <td>
            '.$select_data[$i]['title'].'
        </td>
        <td>
            '.$select_data[$i]['company'].'
        </td>
        <td>
            '.$select_data[$i]['branch_number'].'
        </td>
        <td align="center">
            <a class="tip" title="" href="'.admin_url('customers/company_branch_edit/'.$select_data[$i]['id']).'" data-toggle="modal" data-target="#myModal" data-original-title="Edit"><i class="fa fa-edit"></i>
            </a>
            <a href="#" class="tip po" title="<b>'.$this->lang->line("delete").'</b>"
                data-content="<p>'.lang('r_u_sure').'</p> <a href=\''.admin_url('customers/company_branch_delete/'.$select_data[$i]['id']).'\'><button type=\'button\' class=\'btn btn-danger\' data-action=\'delete\'>'.lang('i_m_sure').'</button></a> <button class=\'btn po-close\'>'.lang('no').'</button>"
                data-html="true" data-placement="left">
                <i class="fa fa-trash-o"></i>
            </a>
        </td>
        </tr>
    ';    
}
if (count($select_data) <= 0)
    echo '
    <tr>
    <td colspan="5">
        '.lang('no_record_found').'
    </td>
    </tr>
    ';
?>           



                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft" type="checkbox" name="check"/>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="width:100px; text-align: center;"><?= lang("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <div class="dataTables_info" id="EXPData_info">
                                    <?php echo $show;?>
                            </div>
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="dataTables_paginate paging_bootstrap">
                               <?php echo $this->pagination->create_links(); //pagination links ?>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
if ($Owner) { 
?>
    <div style="display: none;">
        <input type="hidden" name="show_data" value="<?php echo $show_data;?>" id="show_data"/>
        <input type="hidden" name="sort_by" value="<?php echo $sort_by;?>" id="sort_by"/>
        <input type="hidden" name="sort_order" value="<?php echo $sort_order;?>" id="sort_order"/>    
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php 
} 
?>

<script>
function formactionsubmit(sort_by, sort_order) {
    document.getElementById("sort_by").value = sort_by;
    document.getElementById("sort_order").value = sort_order;
    document.forms[0].submit();
}

function bulk_actions_delete(){
    $("#action-form").attr('action', 'admin/customers/company_branch_actions');
    $('#form_action').val('delete');
    $("#action-form").submit();
}       
<?php
    $temp = $_GET['page'];
    if ($temp == '') $temp = 1;
    echo '
        $(".pagination .active").html(\'<a href="javascript:void(0);">'.$temp.'</a>\');
    ';
?>
</script>

