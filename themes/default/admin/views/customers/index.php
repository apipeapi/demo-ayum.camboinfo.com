<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
    $temp_url = '';
    if ($_GET['page'] != '') 
        $temp_url .= '?page='.$_GET['page']; 
    else 
        $temp_url .= '?page=1'; 

    if ($per_page != '') $temp_url .= '&per_page='.$per_page;

    foreach ($_GET as $name => $value) {
        if ($name != 'page' && $name != 'per_page')
            if ($value != '') $temp_url .= '&'.$name.'='.$value;
    }

    echo admin_form_open('customers'.$temp_url,'id="action-form" name="action-form" method="GET"');
?>

<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-users"></i>
            <?php
                echo $page_title; 
            ?>
        </h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
<?php
                            echo '
                                <li>
                                    <a href="'.admin_url('customers/add').'" data-toggle="modal" data-target="#myModal">
                                        <i class="fa fa-plus-circle"></i> '.lang('add_customer').'
                                    </a>
                                </li>
                                <li>
                                    <a href="'.admin_url('customers/import_csv').'" data-toggle="modal" data-target="#myModal">
                                        <i class="fa fa-plus-circle"></i> '.lang('import_by_csv').'
                                    </a>
                                </li>     
                                <li>
                                    <a href="javascript:void(0)" onclick="api_bulk_actions(\'export_excel\');" data-action="export_excel">
                                        <i class="fa fa-file-excel-o"></i> '.lang('export_to_excel').'
                                    </a>
                                </li>
                            ';
                            echo '
                                <li class="divider"></li>
                            ';
?>
                                <li>
                                    <a href="#" class="bpo" title="<b><?=lang("delete_customers")?></b>" data-content="<p><?=lang('r_u_sure')?></p>
                                    <button type='button' class='btn btn-danger' onclick='bulk_actions_delete();' data-action='delete'><?=lang('i_m_sure')?></a> <button class='btn bpo-close'><?=lang('no')?></button>" data-html="true" data-placement="left">
                                        <i class="fa fa-trash-o"></i> <?=lang('delete_customers')?>
                                    </a>
                                </li>
                    </ul>
                </li>
            </ul>
        </div>

    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?= lang('list_results'); ?></p>
                <div class="row">
                    <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("Products", "products"); ?>
                                <?php 
                                    $config_data = array(
                                        'table_name' => 'sma_products',
                                        'select_table' => 'sma_products',
                                        'select_condition' => "id > 0 order by name asc"
                                    );
                                    $select_products = $this->site->api_select_data_v2($config_data);
                                    $api_products[''] = lang("All Products");
                                    for ($i=0;$i<count($select_products);$i++) {
                                        if ($select_products[$i]['id'] != '') {
                                            $api_products[$select_products[$i]['id']] = $select_products[$i]['name'];
                                        }
                                    }
                                    echo form_dropdown('products_name', $api_products, $_GET['products_name'], 'class="form-control"');
                                ?>
                            </div>
                        </div>
                </div>
                <div class="table-responsive">
                        <div class="row">
                            <div class="col-md-6 text-left api_padding_top_5">
                                <div class="short-result">
                                     <span><?= lang('Show') ?></span>
                                         <select  id="" name="per_page" onchange="api_form_submit('<?php echo 'admin/customers'.$temp_url; ?>')" style="width:80px;">
                                             <?php
                                                 $per_page_arr = array(
                                                     '10'=>'10',
                                                     '25'=>'25',
                                                     '50'=> '50',
                                                     '100'=>'100',
                                                     '200'=>'200',
                                                     '500'=>'500',
                                                     'All'=>$total_rows_sale
                                                 );
                                                
                                                 foreach($per_page_arr as $key =>$value){
                                                     $select = $value == $per_page?'selected':'';
                                                     echo '<option value="'.$value.'" '.$select.'>';
                                                             echo $key;
                                                     echo '</option>';
                                                 }

                                             ?>
                                         </select>
                                 </div>
                            </div>
                            <div class="col-md-6 text-right">
                                <div class="short-result">
                                    <div id="SLData_filter" class="show-data-search dataTables_filter">
                                        <label>
                                            Search
                                            <input class="input-xs" value="<?php echo $this->input->get('search');?>" type="text" name="search" placeholder="search"/>
                                            <input style="height: 34px;" class="btn btn-primary" type="submit" name="submit_search" value="<?= lang('Search') ?>">
                                        </label>                                        
                                    </div>
                                </div>    
                            </div>  

                        </div>     

<?php
    $row_headers = array(
        'checkbox'      => [
                            'label' => '<input class="checkbox checkth skip" onclick="api_select_check_all(this,\'val[]\'); api_setValueCheckbox(\'val[]\',\'check_value\',\'action-form\',\'-\')" type="checkbox" name="check"/>', 
                            'sort_fn' => false,
                            'style' => 'min-width:30px; width: 30px; text-align: center;'
                        ],
        'company'          => ['label' => lang("company"), 'style' => 'text-align:left;'],
        'name'  => ['label' => lang("Personal_Information"), 'style' => 'text-align:left;'],
        // 'parent_id'  => ['label' => lang("Branch"), 'style' => 'text-align:left;min-width:150px;'],
        'parent_id'  => ['label' => lang("Favorite Items"), 'style' => 'text-align:left;min-width:150px;'],
        'detail'      => ['label' => lang("Details"), 'style' => '', 'sort_fn' => false],
        'action'        => [
                            'label' => lang("actions"), 
                            'style' => 'width:80px; text-align:center;', 
                            'sort_fn' => false
                        ],
    );
?>

                    <table id="" class="table table-bordered table-hover table-striped" cellpadding="0" cellspacing="0" border="0">
                        <thead>
                        <tr class="primary">

<?php foreach ($row_headers as $key => $row) :?>
    <?php
   
    $th_class="pointer";
    $e_click = true;
    if(isset($row['sort_fn']) && $row['sort_fn'] == false) {
        $th_class="";
        $e_click = false;
    }
    $style = isset($row['style']) && $row['style'] != '' ? 'style="'.$row['style'].'"' : '';
    ?>
    <th <?= $style ?> >
        <?php if($e_click ) :?>
            <?php if ($sort_by == $key && $sort_order == "ASC") : ?>
                    <label class="font-normal <?=$th_class?>" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort"  aria-hidden="true">
                    <?php echo $row['label']; ?>
                        <span class="fa fa-sort-down"></span>
                    </label>
            <?php elseif ($sort_by == $key and $sort_order=="DESC") : ?>    
                <label class="font-normal <?=$th_class?>" onclick="formactionsubmit('<?= $key ?>', 'ASC')" id="fa_sort_up"  aria-hidden="true">
                    <?php echo $row['label']; ?>
                    <span class="fa fa-sort-up"></span>
                </label>
            <?php else : ?> 
                <label class="font-normal <?=$th_class?>" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort_down" aria-hidden="true">
                    <?php echo $row['label']; ?>
                    <span class="fa fa-sort"></span>
                </label>
            <?php endif; ?> 
        <?php else : ?>
            <label class="font-normal <?=$th_class?>" aria-hidden="true">
                <?php echo $row['label']; ?>
            </label>
        <?php endif; ?> 
    </th>
<?php endforeach; ?>

                            </th>
                        </tr>
                        </thead>
                        <tbody role="alert" aria-live="polite" aria-relevant="all">

<?php

for ($i=0;$i<count($select_data);$i++) {
    
    if ($select_data[$i]['closed'] == 'yes') {
        $temp_closed = '
            <br>
            <label class="label label-danger">
                '.lang('Closed').'
            </label>    
        ';
        $temp_disabled_background = 'background-color:#dfdfdf !important';
    }
    else {
        $temp_closed = '';
        $temp_disabled_background = '';
    }

    $temp_parent_name = '';
    if ($select_data[$i]['parent_name'] != '')
        $temp_parent_name = '
            '.lang('Parent').':
            <a href="'.base_url().'admin/customers?parent_id='.$select_data[$i]['parent_id'].'">
                <label class="label label-default api_link_box_none">
                    '.$select_data[$i]['parent_name'].'
                </label>
            </a>
        ';

    $temp_branch = '';
    if ($select_data[$i]['parent_id'] == 0)
    $temp_branch .= '
        <a href="'.base_url().'admin/customers/add_branch/'.$select_data[$i]['id'].'" data-toggle="modal" data-target="#myModal">
            <label class="label label-success api_link_box_none">
                '.lang('Add').'
            </label>
        </a>
    ';

    $display_products = '';
    //if ($select_data[$i]['products_list'] == 0 || $select_data[$i]['products_list'] != 0)
    $display_products .= '
        <a href="'.base_url().'admin/customers/favorite_items/'.$select_data[$i]['id'].'" data-toggle="modal" data-target="#myModal">
            <label class="label label-success api_link_box_none">
                '.lang('Add').'
            </label>
        </a>
    ';
    
    if ($select_data[$i]['parent_id'] == 0 && $select_data[$i]['products_list'] != '')
    $display_products .= '
        <a href="'.base_url().'admin/customers/remove_favorite_items/'.$select_data[$i]['id'].'" data-toggle="modal" data-target="#myModal">
            <label class="label label-danger api_link_box_none api_margin_left_5">
                '.lang('Remove').'
            </label>
        </a>
    ';

    if ($select_data[$i]['parent_id'] == 0 && $select_data[$i]['branch_list'] != '')
    $temp_branch .= '
        <a href="'.base_url().'admin/customers/remove_branch/'.$select_data[$i]['id'].'" data-toggle="modal" data-target="#myModal">
            <label class="label label-danger api_link_box_none api_margin_left_5">
                '.lang('Remove').'
            </label>
        </a>
    ';


    echo '
    <tr id="'.$select_data[$i]['id'].'" class="tr_customer_link">
        <td class="api_td_width_auto" style="text-align: center; '.$temp_disabled_background.'">
            <div class="text-center">
                <input class="checkbox multi-select skip" type="checkbox" name="val[]" value="'.$select_data[$i]['id'].'" onclick="api_setValueCheckbox(\'val[]\',\'check_value\',\'action-form\',\'-\')"/>
            </div>
        </td>
        <td align="left" style="'.$temp_disabled_background.'">
            <div style="width:130px; word-break:break-word;">
                '.$select_data[$i]['company'].'
                '.$temp_closed.'
            </div>
        </td>
    ';

    $temp = $this->site->api_select_some_fields_with_where("
        *     
        "
        ,"sma_users"
        ,"company_id = ".$select_data[$i]['id']." and group_id = 3"
        ,"arr"
    );
    if (count($temp) > 0) {
        $temp2 = '';
        for ($i2=0;$i2<count($temp);$i2++) {
            $temp2 .= '-'.$temp[$i2]['id'];
        }        
        $temp_user = '
            <a class="" href="'.base_url().'admin/users?group=customer&user_list='.$temp2.'" target="_blank">
                <label class="label label-success api_link_box_none">
                    '.count($temp).'
                </label>
            </a>
        ';
    }
    else
        $temp_user = '
            <label class="label label-danger">
                0
            </label>
        ';

    echo '
        <td valign="top" align="left" style="'.$temp_disabled_background.'">
            <strong>'.lang('Name').':</strong> '.$select_data[$i]['name'].'<br>
            <strong>'.lang('email').':</strong> '.$select_data[$i]['email'].'<br>
            <strong>'.lang('phone').':</strong> '.$select_data[$i]['phone'].'<br>
            <strong>'.lang('User_Registration').':</strong> '.$temp_user.'<br>
        </td>
    ';

    // echo '
    //     <td align="left" style="'.$temp_disabled_background.'">
    //         '.$temp_parent_name.'            
    //         '.$select_data[$i]['branch_list'].'
    //         '.$temp_branch.'
    //     </td>
    //     <td align="left" style="'.$temp_disabled_background.'">
    // ';
    
        
        echo '
                <td align="left" style="'.$temp_disabled_background.'">';
                    if ($select_data[$i]['products_list'] != 0) {   
                        $explode_products_id = explode("-",$select_data[$i]['products_list'],-1);
                        $number = 1;
                        for($j=0;$j<count($explode_products_id);$j++) {
                            if ($explode_products_id[$j] != 0) {
                                $config_data = array(
                                    'table_name' => 'sma_products',
                                    'select_table' => 'sma_products',
                                    'field_name' => 'translate',
                                    'translate' => 'yes',
                                    'select_condition' => "id = ".$explode_products_id[$j],
                                );
                                $select_products_name = $this->site->api_select_data_v2($config_data);
                                for($k=0;$k<count($select_products_name);$k++) {
                                    echo $number++;
                                    echo '.&nbsp;&nbsp;'.$select_products_name[$k]['name'].'<br>';
                                }
                            } 
                        }     
                    } 
                    echo  $display_products;
        echo ' </td>
            <td align="left" style="'.$temp_disabled_background.'">
        ';

        
        
    

    $temp = array();
    if ($select_data[$i]['group_id'] > 0) {
        $temp = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_customer_groups"
            ,"id = ".$select_data[$i]['customer_group_id']
            ,"arr"
        );
        if ($temp[0]['percent'] > 0)
            $temp2 = '<label class="label label-success">+ '.$temp[0]['percent'].'%</label>';
        if ($temp[0]['percent'] == 0)
            $temp2 = '<label class="label label-default">+ '.$temp[0]['percent'].'%</label>';
        if ($temp[0]['percent'] < 0)
            $temp2 = '<label class="label label-danger">'.$temp[0]['percent'].'%</label>';

    }

    $temp_vat = '';
    $temp_invoice_type = '';
    if ($select_data[$i]['vat_no'] != '') {
        $temp_vat = '
            <strong>'.lang('Vat').':</strong> 
            <label class="label label-success">
                '.lang('Yes').'
            </label>
            &nbsp;'.$select_data[$i]['vat_no'].'
            <br>
        ';

        if ($select_data[$i]['vat_invoice_type'] == 'do') {
            $temp = 'DO';
            $temp_2 = 'warning';
        }
        else {
            $temp = 'SL';
            $temp_2 = 'default';            
        }
        $temp_invoice_type = '
            <strong>'.lang('Vat_Invoice_Type').':</strong>  
            <label class="label label-'.$temp_2.'">
                '.$temp.'
            </label>
        ';
    }
    else
        $temp_vat = '
            <strong>'.lang('Vat').':</strong> 
            <label class="label label-danger">
                '.lang('No').'
            </label>
            <br>
        ';

    if ($select_data[$i]['payment_category'] == 'cash on delivery' || $select_data[$i]['payment_category'] == '')
        $temp_payment_category = '
            <label class="label label-danger">
            cash on delivery
            </label>
        ';
    else
        $temp_payment_category = '
            <label class="label label-info">
                '.$select_data[$i]['payment_category'].'
            </label>
        ';

    $temp_auto_add_sale_consignment = '';
    if ($select_data[$i]['sale_consignment_auto_insert'] == 'yes')
        $temp_auto_add_sale_consignment = '
            <br><strong>'.lang('Auto_Add_Sale_Consignment').':</strong> 
            <label class="label label-info">
                '.$select_data[$i]['sale_consignment_auto_insert'].'
            </label>        
        ';

    echo '
            <div style="word-break:break-word;">
                <strong>'.lang('Group').':</strong> '.$select_data[$i]['customer_group_name'].' '.$temp2.'<br>
                <strong>'.lang('Payment').':</strong> '.$temp_payment_category.'<br>
                '.$temp_vat.'
                '.$temp_invoice_type.'
                '.$temp_auto_add_sale_consignment.'
            </div>
    ';

    echo '
        </td>
    ';


    $delete_link = "
        <a href='#' class='po' 
            title='<b>".lang("Delete")."</b>' 
            data-content=\"
                <p>".lang('r_u_sure')."</p>
                <a class='btn btn-danger' href='".admin_url('customers/delete/'.$select_data[$i]['id'])."'>
                    ".lang('i_m_sure')."
                </a> 
                <button class='btn po-close'>".lang('no')."</button>\" 
                data-html=\"true\" data-placement=\"left\">
            <i class='fa fa-trash-o api_width_20'></i>
            ".lang('Delete')."
        </a>
    ";

    if ($select_data[$i]['parent_id'] != 0)
        $temp = '';

    $temp_display = '
        <li>
            <a href="'.base_url().'admin/customers/edit/'.$select_data[$i]['id'].'" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-edit api_width_20"></i> '.lang('Edit').'
            </a>
        </li> 
        <li>
            <a href="'.base_url().'admin/customers/deposits/'.$select_data[$i]['id'].'" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-file-text-o api_width_20"></i> '.lang('List_Deposits').'
            </a>
        </li>
        <li>
            <a href="'.base_url().'admin/customers/add_deposit/'.$select_data[$i]['id'].'" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-plus api_width_20"></i> '.lang('Add_Deposits').'
            </a>
        </li>    
        <li>
            <a href="'.base_url().'admin/customers/addresses/'.$select_data[$i]['id'].'" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-location-arrow api_width_20"></i> '.lang('List Addresses').'
            </a>
        </li>  
        <li>
            <a href="'.base_url().'admin/customers/users/'.$select_data[$i]['id'].'" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-users api_width_20"></i> '.lang('List_Users').'
            </a>
        </li>
        <li>
            <a href="'.base_url().'admin/customers/add_user/'.$select_data[$i]['id'].'" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-user-plus api_width_20"></i> '.lang('Add_User').'
            </a>
        </li>
        <li>'.$delete_link.'</li>
    ';
    echo '
        <td style="width:80px; text-align:center; '.$temp_disabled_background.'">
            <div class="text-center">
                <div class="btn-group text-left">
                    <button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" 
                            data-toggle="dropdown">
                        '.lang('actions').'
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        '.$temp_display.'
                    </ul>
                </div>
            </div>
        </td>
    ';

    echo '
    </tr>
    ';
}
if (count($select_data) <= 0) {
    echo '
    <tr>
    <td colspan="8">
        '.lang('No_record_found').'
    </td>
    </tr>
    ';
}
echo '
    </tbody>
';
?>                            
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft skip" type="checkbox" name="check" onclick="api_select_check_all(this,'val[]'); api_setValueCheckbox('val[]','check_value','action-form','-')"/>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>                            
                            <th style="text-align: center"><?php echo lang('Actions'); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel-heading-left">
                                <p class="data-total">
                                    <?php echo $show.' ';?>
                                </p>
                            </div>   
                            <div class="panel-heading-right">
                                <p>
                                   <?php echo $this->pagination->create_links(); //pagination links ?>
                                </p>
                            </div>  
                            <div class="panel-heading-clear-fix"></div>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="show_data" value="<?php echo $show_data;?>" id="show_data"/>
<input type="hidden" name="sort_by" value="<?php echo $sort_by;?>" id="sort_by"/>
<input type="hidden" name="sort_order" value="<?php echo $sort_order;?>" id="sort_order"/>
<?= form_close() ?>

<?php

echo admin_form_open('customers/customer_actions','id="api_form_action" name="api_form_action" method="post"');
echo '
    <div class="api_display_none">
            <input type="text" name="check_value" value="" id="check_value"/>
            <input type="text" name="api_action" value="" id="api_action"/>
            <button type="button" onclick="document.api_form_action.submit();" value="ok">ok</button>
    </div>
';
echo form_close();

?>


<script>
    function api_bulk_actions(action){
        $("#api_action").val(action);
        document.api_form_action.submit();
    } 
    function bulk_actions_delete(){
        $("#api_action").val('delete');
        document.api_form_action.submit();
    }
    function formactionsubmit(sort_by, sort_order) {
        document.getElementById("sort_by").value = sort_by;
        document.getElementById("sort_order").value = sort_order;
        document.forms[0].submit();
    }
    function api_form_submit(action){
        $("#action-form").attr('action', action);
        $('#form_action').val('');
        $("#action-form").submit();
    }         
<?php
    $temp = $_GET['page'];
    if ($temp == '') $temp = 1;
    echo '
        $(".pagination .active").html(\'<a href="javascript:void(0);">'.$temp.'</a>\');
    ';
?>    
</script>
