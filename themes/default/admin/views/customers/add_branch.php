<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('Add_branch') . " (" . $select_data[0]['company'] . ")"; ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open("customers/add_branch/" . $select_data[0]['id'], $attrib); ?>
        <div class="modal-body">            

            <div class="row">
                <div class="col-sm-12">

                    <?php
                    echo '
                        <div class="form-group">
                            <label class="control-label" for="company_id">
                                '.lang("Company").'
                            </label>
                            <div class="controls"> 
                    ';
                                $tr2[''] = lang("None");
                                for ($i=0;$i<count($api_company);$i++) {
                                    if ($api_company[$i]['temp_ignore'] != 1) {
                                        if ($api_company[$i]['company'] != '')
                                            $tr2[$api_company[$i]['id']] = $api_company[$i]['company'];
                                    }
                                }
                                echo form_dropdown('company_id', $tr2, '', 'data-placeholder="'.lang("None").'" id="company_id" class="form-control" required="required"');
                    echo '
                            </div>
                        </div>    
                    ';    
                    ?>


                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('add', lang('Add'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

