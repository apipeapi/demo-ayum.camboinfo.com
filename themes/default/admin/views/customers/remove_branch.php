<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('Remove_Branch') . " (" . $select_data[0]['company'] . ")"; ?></h4>
        </div>
        <?php $attrib = array('name' => 'frm_remove_branch', 'id' => 'frm_remove_branch', 'method' => 'post');
        echo admin_form_open("customers/remove_branch/" . $select_data[0]['id'], $attrib); ?>
        <div class="modal-body">            

            <div class="row">
                <div class="col-sm-12">

    

            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped print-table order-table">
                    <thead>
                    <tr>
                        <th>
<?php 
    echo '
        <input type="hidden" name="check_value_remove_branch" value="" id="check_value_remove_branch"  />

<input class="checkbox checkth skip" id="api_check_all" onclick="api_select_check_all(this,\'val[]\'); api_setValueCheckbox(\'val[]\',\'check_value_remove_branch\',\'frm_remove_branch\',\'-\');" type="checkbox" name="check"/>
    ';

?>
                            
                        </th>
                        <th><?= lang("Branch"); ?></th>
                    </tr>
                    </thead>
                    <tbody>
<?php
    for ($i=0;$i<count($api_company);$i++) {
        echo '
            <tr>
                <td style="min-width:30px; width: 30px; text-align: center;">
                    <div class="text-center">
                        <input class="checkbox multi-select skip" type="checkbox" name="val[]" value="'.$api_company[$i]['id'].'" id="api_list_check_'.$api_company[$i]['id'].'" onclick="api_setValueCheckbox(\'val[]\',\'check_value_remove_branch\',\'frm_remove_branch\',\'-\');"/>
                    </div>
                </td>
                <td>
                    '.$api_company[$i]['company'].'
                </td>
            </tr>
        ';
    }

?>
                    </tbody>
                </table>
            </div>                        


                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php 
                echo '
                <input type="button" name="" value="'.lang('Remove').'" class="btn btn-danger" onclick="api_frm_remove_branch_submit();">
                ';
            ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

<script type="text/javascript">
function api_setValueCheckbox(check_name,set_id,api_form_product,sep){
    var temp = '';
    eles = document.forms[api_form_product].elements;
    for (var i=0;i<eles.length;i++){        
        if (eles[i].name == check_name){
            if(eles[i].checked==true){                
                temp = temp + sep + eles[i].value;
            }
        } 
    }       
    document.getElementById(set_id).value = temp;
    //console.log(temp);
}
function api_frm_remove_branch_submit(){
    var temp = document.getElementById('check_value_remove_branch').value;
    if (temp == '')
        alert('Please select a branch.');
    else
        document.getElementById('frm_remove_branch').submit();
}

</script>