<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('Remove Products') . " (" . $select_data[0]['company'] . ")"; ?></h4>
        </div>
        <?php $attrib = array('name' => 'frm_remove_favorite_items', 'id' => 'frm_remove_favorite_items', 'method' => 'post');
        echo admin_form_open("customers/remove_favorite_items/" . $select_data[0]['id'], $attrib); ?>
        <div class="modal-body">            

            <div class="row">
                <div class="col-sm-12">

    

            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped print-table order-table">
                    <thead>
                    <tr>
                        <th>
<?php 
    echo '
        <input type="hidden" name="check_value_remove_favorite_items" value="" id="check_value_remove_favorite_items"  />

<input class="checkbox checkth skip" id="api_check_all" onclick="api_select_check_all(this,\'val[]\'); api_setValueCheckbox(\'val[]\',\'check_value_remove_favorite_items\',\'frm_remove_favorite_items\',\'-\');" type="checkbox" name="check"/>
    ';

?>
                            
                        </th>
                        <th><?= lang("Products"); ?></th>
                    </tr>
                    </thead>
                    <tbody>
<?php
    for($i=0;$i<count($explode_products_id);$i++) {
        if($explode_products_id[$i] > 0) {
            $config_data = array(
                'table_name' => 'sma_products',
                'select_table' => 'sma_products',
                'select_condition' => " id = ".$explode_products_id[$i]." order by id desc",
            );
            $select_products_id = $this->site->api_select_data_v2($config_data);
            echo '
                <tr>
                    <td style="min-width:30px; width: 30px; text-align: center;">
                        <div class="text-center">
                            <input class="checkbox multi-select skip" type="checkbox" name="val[]" value="'.$explode_products_id[$i].'" id="api_list_check_'.$explode_products_id[$i].'" onclick="api_setValueCheckbox(\'val[]\',\'check_value_remove_favorite_items\',\'frm_remove_favorite_items\',\'-\');"/>
                        </div>
                    </td>
                    <td> ';
                        for($j=0;$j<count($select_products_id);$j++) {
                            echo $select_products_id[$j]['name'];
                        }
                echo '</td>
                </tr>
            ';
        }
    }
?>
                    </tbody>
                </table>
            </div>                        


                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php 
                echo '
                <input type="button" name="" value="'.lang('Remove').'" class="btn btn-danger" onclick="api_frm_remove_favorite_items_submit();">
                ';
            ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

<script type="text/javascript">
function api_setValueCheckbox(check_name,set_id,api_form_product,sep){
    var temp = '';
    eles = document.forms[api_form_product].elements;
    for (var i=0;i<eles.length;i++){        
        if (eles[i].name == check_name){
            if(eles[i].checked==true){                
                temp = temp + sep + eles[i].value;
            }
        } 
    }       
    document.getElementById(set_id).value = temp;
    
}
function api_frm_remove_favorite_items_submit(){
    var temp = document.getElementById('check_value_remove_favorite_items').value;
    if (temp == '')
        alert('Please select a products.');
    else
        document.getElementById('frm_remove_favorite_items').submit();
        console.log(temp);
}

</script>