$(document).ready(function () {
    if (!localStorage.getItem('qaref')) {
        localStorage.setItem('qaref', '');
    }

    ItemnTotals();
    $('.bootbox').on('hidden.bs.modal', function (e) {
        $('#add_item').focus();
    });
    $('body a, body button').attr('tabindex', -1);
    check_add_item_val();
    if (site.settings.set_focus != 1) {
        $('#add_item').focus();
    }

    //localStorage.clear();
    // If there is any item in localStorage
    if (localStorage.getItem('qaitems')) {
        loadItems();
    }

    // clear localStorage and reload
    $('#reset').click(function (e) {
        bootbox.confirm(lang.r_u_sure, function (result) {
            if (result) {
                if (localStorage.getItem('qaitems')) {
                    localStorage.removeItem('qaitems');
                }
                if (localStorage.getItem('qaref')) {
                    localStorage.removeItem('qaref');
                }
                if (localStorage.getItem('qawarehouse')) {
                    localStorage.removeItem('qawarehouse');
                }
                if (localStorage.getItem('qanote')) {
                    localStorage.removeItem('qanote');
                }
                if (localStorage.getItem('qadate')) {
                    localStorage.removeItem('qadate');
                }

                $('#modal-loading').show();
                location.reload();
            }
        });
    });

    // save and load the fields in and/or from localStorage
    $('#qaref').change(function (e) {
        localStorage.setItem('qaref', $(this).val());
    });
    if (qaref = localStorage.getItem('qaref')) {
        $('#qaref').val(qaref);
    }
    $('#qawarehouse').change(function (e) {
        //localStorage.setItem('qawarehouse', $(this).val());
    });
    if (qawarehouse = localStorage.getItem('qawarehouse')) {
        //$('#qawarehouse').select2("val", qawarehouse);
    }

    //$(document).on('change', '#qanote', function (e) {
        $('#qanote').redactor('destroy');
        $('#qanote').redactor({
            buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
            formattingTags: ['p', 'pre', 'h3', 'h4'],
            minHeight: 100,
            changeCallback: function (e) {
                var v = this.get();
                localStorage.setItem('qanote', v);
            }
        });
        if (qanote = localStorage.getItem('qanote')) {
            $('#qanote').redactor('set', qanote);
        }

    // prevent default action upon enter
    $('body').bind('keypress', function (e) {
        if ($(e.target).hasClass('redactor_editor')) {
            return true;
        }
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });


    /* ----------------------
     * Delete Row Method
     * ---------------------- */

    $(document).on('click', '.qadel', function () {
        var row = $(this).closest('tr');
        var item_id = row.attr('data-item-id');
        delete qaitems[item_id];
        row.remove();
        if(qaitems.hasOwnProperty(item_id)) { } else {
            localStorage.setItem('qaitems', JSON.stringify(qaitems));
            loadItems();
            return;
        }
    });

    /* --------------------------
     * Edit Row Quantity Method
     -------------------------- */

    $(document).on("change", '.rquantity', function () {
/*        
        var row = $(this).closest('tr');
        if (!is_numeric($(this).val()) || parseFloat($(this).val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_qty = parseFloat($(this).val()),
        item_id = row.attr('data-item-id');
        qaitems[item_id].row.qty = new_qty;
        localStorage.setItem('qaitems', JSON.stringify(qaitems));
        loadItems();
*/
    });

    $(document).on("change", '.rtype', function () {
        var row = $(this).closest('tr');
        var new_type = $(this).val(),
        item_id = row.attr('data-item-id');
        qaitems[item_id].row.type = new_type;
        qaitems[item_id].row.qty = $('#quantity_' + qaitems[item_id].id).val();
        qaitems[item_id].row.actual_qty = $('#actual_qty_' + qaitems[item_id].id).val();
        if (parseFloat(qaitems[item_id].row.qty) < 0)
            qaitems[item_id].row.qty = Math.abs(parseFloat(qaitems[item_id].row.qty));
        else
            qaitems[item_id].row.qty = -Math.abs(parseFloat(qaitems[item_id].row.qty));

        console.log(qaitems[item_id]);
        localStorage.setItem('qaitems', JSON.stringify(qaitems));
        loadItems();
    });

    $(document).on("change", '.rvariant', function () {
        var row = $(this).closest('tr');
        var new_opt = $(this).val(),
        item_id = row.attr('data-item-id');
        qaitems[item_id].row.option = new_opt;
        localStorage.setItem('qaitems', JSON.stringify(qaitems));
    });


});


function add_adjustment_item(item) {
    if (count == 1) {
        qaitems = {};
    }
    if (localStorage.getItem('qaitems')) {
        qaitems = JSON.parse(localStorage.getItem('qaitems'));
        $.each(qaitems, function () {
            var item = this;    
            qaitems[item.item_id].row.qty = $('#quantity_' + item.id).val();
            qaitems[item.item_id].row.actual_qty = $('#actual_qty_' + item.id).val();
            qaitems[item.item_id].row.type = $('#type_' + item.id).val();
        });
    }

    if (item == null)
        return;

    var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;

    if (qaitems[item_id]) {        
        var new_qty = parseFloat(qaitems[item_id].row.qty) + 0;
        qaitems[item_id].row.base_quantity = new_qty;
        if(qaitems[item_id].row.unit != qaitems[item_id].row.base_unit) {
            $.each(qaitems[item_id].units, function(){
                if (this.id == qaitems[item_id].row.unit) {
                    qaitems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                }
            });
        }
        qaitems[item_id].row.qty = new_qty;
    } else {
        item.type = 'addition';        
        qaitems[item_id] = item;
        qaitems[item_id].order = new Date().getTime();
    }
    
    localStorage.setItem('qaitems', JSON.stringify(qaitems));
    loadItems();
    return true;
}

function api_actual_qty_update(id){
    document.getElementById('actual_qty_' + id).value = document.getElementById('actual_qty_' + id).value.replace(/[^0-9.]/g, '');
    var temp_warehouse_id = $('#qawarehouse').val();
    selected_warehouse_quantity = document.getElementById('warehouse_quantity_' + temp_warehouse_id + '_' + id).innerHTML;
    selected_warehouse_quantity = selected_warehouse_quantity.replace(",",""); 

    if (parseFloat(selected_warehouse_quantity) < parseFloat(document.getElementById('actual_qty_' + id).value)) {
        document.getElementById('quantity_' + id).value = parseFloat(document.getElementById('actual_qty_' + id).value) - parseFloat(selected_warehouse_quantity);
        document.getElementById('type_label_' + id).innerHTML = '<label class="label label-success api_pointer">+</label>';
        document.getElementById('type_' + id).value = 'addition';
    }
    else {
        document.getElementById('quantity_' + id).value = parseFloat(selected_warehouse_quantity) - parseFloat(document.getElementById('actual_qty_' + id).value);
        document.getElementById('type_label_' + id).innerHTML = '<label class="label label-danger api_pointer">-</label>';
        document.getElementById('type_' + id).value = 'subtraction';
    }

    document.getElementById('quantity_' + id).value = parseFloat(document.getElementById('quantity_' + id).value).toFixed(2);

}

function api_qty_update(id){
    api_change_adjustment_type(id);
    api_change_adjustment_type(id);
}
