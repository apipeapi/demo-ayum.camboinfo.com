
function api_change_admin_mode(postData) {
	
    var result = $.ajax
    (
    	{
    		url: site.base_url + 'admin/welcome/api_change_admin_mode',
    		type: 'GET',
    		secureuri:false,
    		dataType: 'html',
    		data:postData,
    		async: false,
    		error: function (response, status, e)
    		{
    			alert(e);
    		}
    	}
    ).responseText;
// var myWindow = window.open("", "MsgWindow", "width=700, height=400");
// myWindow.document.write(result);     
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    var result_text = array_data[1];

    if (result_text != 'yes')
        $(".api_admin_edit_wrapper").show();
    else
        $(".api_admin_edit_wrapper").hide();
}


function api_admin_delete(id,e,s,a,postData){s=s||"delete",e=e||postData['message'],(a=a||{})._method=s,a[site.csrf_token]=site.csrf_token_value,swal({title:lang.are_you_sure,html:e,type:"question",showCancelButton:!0,allowOutsideClick:!1,showLoaderOnConfirm:!0,preConfirm:function(){return new Promise(function(){
	api_ajax_admin_delete(postData);
    })}}).catch(swal.noop)
}


function api_ajax_admin_edit(postData) {
    var result = $.ajax
    (
    	{
    		url: site.base_url + 'admin/welcome/api_ajax_admin_edit',
    		type: 'GET',
    		secureuri:false,
    		dataType: 'html',
    		data:postData,
    		async: false,
    		error: function (response, status, e)
    		{
    			alert(e);
    		}
    	}
    ).responseText;
// var myWindow = window.open("", "MsgWindow", "width=700, height=400");
// myWindow.document.write(result);     
    var array_data = String(result).split("api-ajax-request-multiple-result-split");

	if (postData['modal_class'] != '')
		$('#api_modal_admin .modal-dialog').addClass('modal-lg');
	else
		$('#api_modal_admin .modal-dialog').removeClass('modal-lg');
	$('#api_modal_admin #api_modal_title').html(postData['title']);
	$('#api_modal_admin #api_modal_body').html(array_data[1]);
	$('#api_modal_admin #api_modal_footer').html(array_data[2]);
	$('#api_modal_trigger_admin').click();
}

function api_ajax_admin_delete(postData) {
    var result = $.ajax
    (
    	{
    		url: site.base_url + 'admin/welcome/api_ajax_admin_delete',
    		type: 'GET',
    		secureuri:false,
    		dataType: 'html',
    		data:postData,
    		async: false,
    		error: function (response, status, e)
    		{
    			alert(e);
    		}
    	}
    ).responseText;
// var myWindow = window.open("", "MsgWindow", "width=700, height=400");
// myWindow.document.write(result);     
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
	window.location = postData['redirect'];
}


