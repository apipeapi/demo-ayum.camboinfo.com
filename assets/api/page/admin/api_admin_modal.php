<?php
echo '            
    <button type="button" id="api_modal_trigger_admin" class="api_display_none" data-toggle="modal" data-target="#api_modal_admin">
        Open Modal
    </button>
    <div id="api_modal_admin" class="modal fade api_modal_admin" role="dialog">
        <div class="api_height_15 api_clear_both"></div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="api_modal_title">
                    </h4>
                </div>
                <div class="modal-body" id="api_modal_body">                
                </div>
                <div class="modal-footer" id="api_modal_footer">
                </div>
            </div>
        </div>
    </div>
';


?>