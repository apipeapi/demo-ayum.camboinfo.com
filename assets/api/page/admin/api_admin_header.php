<?php
    $l = $this->api_shop_setting[0]['api_lang_key'];

    $temp_display .= '
        <div class="api_height_30 api_clear_both hidden-xs"></div>
        <table class="api_temp_table_1" width="100%" border="0">
        <tr>
        <td class="api_td_width_auto api_padding_left_10 api_padding_right_10" valign="middle" align="left">
            <a href="'.base_url().'">
                <img src="'.base_url().'assets/uploads/logos/'.$this->api_shop_setting[0]['icon'].'" />
            </a>
        </td>
        <td class="api_td_width_auto api_padding_right_10 hidden-xs" valign="middle" align="left">
            <a href="'.base_url().'">
                '.$this->api_web[0]['title_'.$l].'
            </a>
        </td>
        <td class="api_td_width_auto api_padding_left_10 api_padding_right_10" valign="middle" align="left">
            <a href="'.admin_url().'">
                <i class="fa fa-dashboard api_font_size_16"></i><span class=" hidden-xs"> &nbsp;'.lang('admin_area').'</span>
            </a>
        </td> 
    ';

    if ($this->api_shop_setting[0]['admin_mode_hide'] != 'yes')
        $temp_checked = 'checked="checked"';
    else
        $temp_checked = '';
    $temp_display_3 .= '
        <div class="api_float_left api_padding_top_3">
            <input name="temp_admin_mode" type="checkbox" class="checkbox api_temp_checkbox_1" value="yes" '.$temp_checked.' onclick="
                postData = {
                    \'field_value\' : this.checked,
                }
                api_change_admin_mode(postData);
            " />
        </div>
        <div class="api_float_left api_padding_left_5">Admin mode</div>
        <div class="api_clear_both"></div>
    ';
    $temp_display .= '
        <td class="api_padding_left_10 api_padding_right_10" valign="middle" align="left">
            '.$temp_display_3.'
        </td>        
    ';

    $temp_display_2 = '';
    if ($loggedIn) {
        $temp_display_2 .= ' 
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-user"></i> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">          
                <li class=""><a href="'.base_url().'admin/auth/profile/'.$this->session->userdata('user_id').'"><i class="mi fa fa-user"></i> '.lang('profile').'</a></li>
                <li class=""><a href="'.site_url('logout').'"><i class="mi fa fa-sign-out"></i> '.lang('logout').'</a></li>
            </ul>
        ';
    }
    if ($this->api_shop_setting[0]['api_lang_key'] == 'en') {
        $temp_1 = '';
        $temp_2 = 'api_admin_language_a_link_selected';
    }
    if ($this->api_shop_setting[0]['api_lang_key'] == 'ja') {
        $temp_1 = 'api_admin_language_a_link_selected';
        $temp_2 = '';
    }        
    $temp_display .= '
        <td class="api_td_width_auto api_padding_left_10 api_padding_right_10" valign="middle" align="right">
            <div>
                <a class="api_big api_admin_language_a_link '.$temp_1.'" href="'.base_url().'/main/language/japanese">
                    JP
                </a>
                |
                <a class="api_big api_admin_language_a_link '.$temp_2.'" href="'.base_url().'/main/language/english">
                    EN
                </a>                
            </div>        
        </td>
        <td class="api_td_width_auto api_padding_left_10 api_padding_right_10" valign="middle" align="right">
            '.$temp_display_2.'
        </td>
        </tr>
        </table>    
    ';

    $config_data = array(
        'wrapper_class' => 'api_admin_header',
        'custom_html' => '<div class="api_height_30 api_clear_both visible-xs"></div>',
        'display' => $temp_display,
        'type' => 'full',
    );
    $temp = $this->api_display->template_display($config_data);
    echo $temp['display'];

?>

