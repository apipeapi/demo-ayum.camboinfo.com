/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100130
Source Host           : localhost:3306
Source Database       : stock_manager

Target Server Type    : MYSQL
Target Server Version : 100130
File Encoding         : 65001

Date: 2019-01-16 14:06:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sma_addresses
-- ----------------------------
DROP TABLE IF EXISTS `sma_addresses`;
CREATE TABLE `sma_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `line1` varchar(50) NOT NULL,
  `line2` varchar(50) DEFAULT NULL,
  `city` varchar(25) NOT NULL,
  `postal_code` varchar(20) DEFAULT NULL,
  `state` varchar(25) NOT NULL,
  `country` varchar(50) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_addresses
-- ----------------------------

-- ----------------------------
-- Table structure for sma_adjustments
-- ----------------------------
DROP TABLE IF EXISTS `sma_adjustments`;
CREATE TABLE `sma_adjustments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` text,
  `attachment` varchar(55) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `count_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `warehouse_id` (`warehouse_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_adjustments
-- ----------------------------

-- ----------------------------
-- Table structure for sma_adjustment_items
-- ----------------------------
DROP TABLE IF EXISTS `sma_adjustment_items`;
CREATE TABLE `sma_adjustment_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adjustment_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adjustment_id` (`adjustment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_adjustment_items
-- ----------------------------

-- ----------------------------
-- Table structure for sma_api_keys
-- ----------------------------
DROP TABLE IF EXISTS `sma_api_keys`;
CREATE TABLE `sma_api_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `reference` varchar(40) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_api_keys
-- ----------------------------

-- ----------------------------
-- Table structure for sma_api_limits
-- ----------------------------
DROP TABLE IF EXISTS `sma_api_limits`;
CREATE TABLE `sma_api_limits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) NOT NULL,
  `count` int(10) NOT NULL,
  `hour_started` int(11) NOT NULL,
  `api_key` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_api_limits
-- ----------------------------

-- ----------------------------
-- Table structure for sma_api_logs
-- ----------------------------
DROP TABLE IF EXISTS `sma_api_logs`;
CREATE TABLE `sma_api_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) NOT NULL,
  `method` varchar(6) NOT NULL,
  `params` text,
  `api_key` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `time` int(11) NOT NULL,
  `rtime` float DEFAULT NULL,
  `authorized` varchar(1) NOT NULL,
  `response_code` smallint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_api_logs
-- ----------------------------

-- ----------------------------
-- Table structure for sma_brands
-- ----------------------------
DROP TABLE IF EXISTS `sma_brands`;
CREATE TABLE `sma_brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `image` varchar(50) DEFAULT NULL,
  `slug` varchar(55) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_brands
-- ----------------------------

-- ----------------------------
-- Table structure for sma_calendar
-- ----------------------------
DROP TABLE IF EXISTS `sma_calendar`;
CREATE TABLE `sma_calendar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(55) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL,
  `color` varchar(7) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_calendar
-- ----------------------------

-- ----------------------------
-- Table structure for sma_captcha
-- ----------------------------
DROP TABLE IF EXISTS `sma_captcha`;
CREATE TABLE `sma_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `captcha_time` int(10) unsigned NOT NULL,
  `ip_address` varchar(16) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `word` varchar(20) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_captcha
-- ----------------------------

-- ----------------------------
-- Table structure for sma_cart
-- ----------------------------
DROP TABLE IF EXISTS `sma_cart`;
CREATE TABLE `sma_cart` (
  `id` varchar(40) NOT NULL,
  `time` varchar(30) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_cart
-- ----------------------------

-- ----------------------------
-- Table structure for sma_categories
-- ----------------------------
DROP TABLE IF EXISTS `sma_categories`;
CREATE TABLE `sma_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(55) NOT NULL,
  `name` varchar(55) NOT NULL,
  `image` varchar(55) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(55) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_categories
-- ----------------------------
INSERT INTO `sma_categories` VALUES ('1', 'C1', 'Category 1', null, null, 'category-1', null);

-- ----------------------------
-- Table structure for sma_combo_items
-- ----------------------------
DROP TABLE IF EXISTS `sma_combo_items`;
CREATE TABLE `sma_combo_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `item_code` varchar(20) NOT NULL,
  `quantity` decimal(12,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_combo_items
-- ----------------------------

-- ----------------------------
-- Table structure for sma_companies
-- ----------------------------
DROP TABLE IF EXISTS `sma_companies`;
CREATE TABLE `sma_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned DEFAULT NULL,
  `group_name` varchar(20) NOT NULL,
  `customer_group_id` int(11) DEFAULT NULL,
  `customer_group_name` varchar(100) DEFAULT NULL,
  `name` varchar(55) NOT NULL,
  `company` varchar(255) NOT NULL,
  `vat_no` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(55) DEFAULT NULL,
  `state` varchar(55) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `cf1` varchar(100) DEFAULT NULL,
  `cf2` varchar(100) DEFAULT NULL,
  `cf3` varchar(100) DEFAULT NULL,
  `cf4` varchar(100) DEFAULT NULL,
  `cf5` varchar(100) DEFAULT NULL,
  `cf6` varchar(100) DEFAULT NULL,
  `invoice_footer` text,
  `payment_term` int(11) DEFAULT '0',
  `logo` varchar(255) DEFAULT 'logo.png',
  `award_points` int(11) DEFAULT '0',
  `deposit_amount` decimal(25,4) DEFAULT NULL,
  `price_group_id` int(11) DEFAULT NULL,
  `price_group_name` varchar(50) DEFAULT NULL,
  `gst_no` varchar(100) DEFAULT NULL,
  `payment_category` varchar(100) DEFAULT NULL,
  `sale_person_id` int(11) DEFAULT NULL,
  `name_kh` varchar(100) DEFAULT NULL,
  `company_kh` varchar(255) DEFAULT NULL,
  `address_kh` varchar(255) DEFAULT NULL,
  `state_kh` varchar(255) DEFAULT NULL,
  `city_kh` varchar(255) DEFAULT NULL,
  `country_kh` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `group_id_2` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_companies
-- ----------------------------
INSERT INTO `sma_companies` VALUES ('1', '3', 'customer', '1', 'General', 'Walk-in Customer', 'Walk-in Customer', '', 'Customer Address', 'Petaling Jaya', 'Selangor', '46000', '', '0123456789', 'customer@tecdiary.com', '', '', '', '', '', '', null, '0', 'logo.png', '0', null, null, null, null, 'monthly', '2', '', '', '', '', '', null);
INSERT INTO `sma_companies` VALUES ('2', '4', 'supplier', null, null, 'Test Supplier', 'Supplier Company Name', null, 'Supplier Address', 'Petaling Jaya', 'Selangor', '46050', 'Malaysia', '0123456789', 'supplier@tecdiary.com', '-', '-', '-', '-', '-', '-', null, '0', 'logo.png', '0', null, null, null, null, 'monthly', null, null, null, null, null, null, null);
INSERT INTO `sma_companies` VALUES ('3', null, 'biller', null, null, 'Mian Saleem', 'Test Biller', '5555', 'Biller adddress', 'City', '', '', 'Country', '012345678', 'saleem@tecdiary.com', '', '', '', '', '', '', ' Thank you for shopping with us. Please come again', '0', 'logo1.png', '0', null, null, null, null, 'monthly', null, null, null, null, null, null, null);
INSERT INTO `sma_companies` VALUES ('5', '3', 'customer', '4', 'New Customer (+10)', 'New', 'Newcompany', '', 'St446, Toul tum poung', 'Phnom Penh', '', '', '', '0998887766', 'new@yahoo.com', '', '', '', '', '', '', null, '0', 'logo.png', '0', null, '1', 'Default', '', 'cash on delivery', '6', '', '', '', '', '', null);
INSERT INTO `sma_companies` VALUES ('6', '3', 'customer', '5', 'Siem reap', 'SR', 'SR', '', 'St454, pub streeet', 'Siem reap', '', '', '', '0966665544', 'sr@gmail.com', '', '', '', '', '', '', null, '0', 'logo.png', '0', null, '1', 'Default', '', 'weekly', '7', '', '', '', '', '', null);
INSERT INTO `sma_companies` VALUES ('7', '3', 'customer', '5', 'Siem reap', '2month', '2month', '', 'St488', 'Siem reap', '', '', '', '0965554433', 'twomonth@gmail.com', '', '', '', '', '', '', null, '0', 'logo.png', '0', null, '1', 'Default', '', '2 month', '3', '', '', '', '', '', null);

-- ----------------------------
-- Table structure for sma_costing
-- ----------------------------
DROP TABLE IF EXISTS `sma_costing`;
CREATE TABLE `sma_costing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `sale_item_id` int(11) NOT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `purchase_item_id` int(11) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `purchase_net_unit_cost` decimal(25,4) DEFAULT NULL,
  `purchase_unit_cost` decimal(25,4) DEFAULT NULL,
  `sale_net_unit_price` decimal(25,4) NOT NULL,
  `sale_unit_price` decimal(25,4) NOT NULL,
  `quantity_balance` decimal(15,4) DEFAULT NULL,
  `inventory` tinyint(1) DEFAULT '0',
  `overselling` tinyint(1) DEFAULT '0',
  `option_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_costing
-- ----------------------------
INSERT INTO `sma_costing` VALUES ('2', '2019-01-10', '1', '2', '2', null, '50.0000', '0.0000', '0.0000', '1.0000', '1.0000', null, '1', '1', null);
INSERT INTO `sma_costing` VALUES ('3', '2019-01-11', '5', '3', '3', null, '1.0000', '10.0000', '10.0000', '15.8000', '15.8000', null, '1', '1', null);
INSERT INTO `sma_costing` VALUES ('4', '2019-01-11', '6', '4', '3', null, '1.0000', '10.0000', '10.0000', '15.9000', '15.9000', null, '1', '1', null);
INSERT INTO `sma_costing` VALUES ('5', '2019-01-11', '7', '5', '3', null, '1.0000', '10.0000', '10.0000', '16.0000', '16.0000', null, '1', '1', null);
INSERT INTO `sma_costing` VALUES ('6', '2019-01-11', '2', '6', '4', null, '10.0000', '10.0000', '10.0000', '20.0000', '20.0000', null, '1', '1', null);
INSERT INTO `sma_costing` VALUES ('7', '2019-01-11', '5', '7', '4', null, '10.0000', '10.0000', '10.0000', '15.8000', '15.8000', null, '1', '1', null);
INSERT INTO `sma_costing` VALUES ('8', '2019-01-11', '7', '8', '4', null, '10.0000', '10.0000', '10.0000', '16.0000', '16.0000', null, '1', '1', null);
INSERT INTO `sma_costing` VALUES ('9', '2019-01-11', '7', '9', '4', null, '10.0000', '10.0000', '10.0000', '16.0000', '16.0000', null, '1', '1', null);
INSERT INTO `sma_costing` VALUES ('10', '2019-01-11', '6', '10', '4', null, '10.0000', '10.0000', '10.0000', '15.9000', '15.9000', null, '1', '1', null);
INSERT INTO `sma_costing` VALUES ('11', '2019-01-16', '1', '11', '5', null, '1.0000', '0.0000', '0.0000', '1.1000', '1.1000', null, '1', '1', null);

-- ----------------------------
-- Table structure for sma_currencies
-- ----------------------------
DROP TABLE IF EXISTS `sma_currencies`;
CREATE TABLE `sma_currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(5) NOT NULL,
  `name` varchar(55) NOT NULL,
  `rate` decimal(12,4) NOT NULL,
  `auto_update` tinyint(1) NOT NULL DEFAULT '0',
  `symbol` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_currencies
-- ----------------------------
INSERT INTO `sma_currencies` VALUES ('1', 'USD', 'US Dollar', '1.0000', '0', null);
INSERT INTO `sma_currencies` VALUES ('2', 'EUR', 'EURO', '0.7340', '0', null);

-- ----------------------------
-- Table structure for sma_customer_groups
-- ----------------------------
DROP TABLE IF EXISTS `sma_customer_groups`;
CREATE TABLE `sma_customer_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `percent` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_customer_groups
-- ----------------------------
INSERT INTO `sma_customer_groups` VALUES ('1', 'General', '0');
INSERT INTO `sma_customer_groups` VALUES ('2', 'Reseller', '-5');
INSERT INTO `sma_customer_groups` VALUES ('3', 'Distributor', '-15');
INSERT INTO `sma_customer_groups` VALUES ('4', 'New Customer (+10)', '10');
INSERT INTO `sma_customer_groups` VALUES ('5', 'Siem reap', '10');
INSERT INTO `sma_customer_groups` VALUES ('6', 'RESTAURANT (J)', '0');

-- ----------------------------
-- Table structure for sma_date_format
-- ----------------------------
DROP TABLE IF EXISTS `sma_date_format`;
CREATE TABLE `sma_date_format` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `js` varchar(20) NOT NULL,
  `php` varchar(20) NOT NULL,
  `sql` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_date_format
-- ----------------------------
INSERT INTO `sma_date_format` VALUES ('1', 'mm-dd-yyyy', 'm-d-Y', '%m-%d-%Y');
INSERT INTO `sma_date_format` VALUES ('2', 'mm/dd/yyyy', 'm/d/Y', '%m/%d/%Y');
INSERT INTO `sma_date_format` VALUES ('3', 'mm.dd.yyyy', 'm.d.Y', '%m.%d.%Y');
INSERT INTO `sma_date_format` VALUES ('4', 'dd-mm-yyyy', 'd-m-Y', '%d-%m-%Y');
INSERT INTO `sma_date_format` VALUES ('5', 'dd/mm/yyyy', 'd/m/Y', '%d/%m/%Y');
INSERT INTO `sma_date_format` VALUES ('6', 'dd.mm.yyyy', 'd.m.Y', '%d.%m.%Y');

-- ----------------------------
-- Table structure for sma_deliveries
-- ----------------------------
DROP TABLE IF EXISTS `sma_deliveries`;
CREATE TABLE `sma_deliveries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sale_id` int(11) NOT NULL,
  `do_reference_no` varchar(50) NOT NULL,
  `sale_reference_no` varchar(50) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  `attachment` varchar(50) DEFAULT NULL,
  `delivered_by` varchar(50) DEFAULT NULL,
  `received_by` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_deliveries
-- ----------------------------

-- ----------------------------
-- Table structure for sma_deposits
-- ----------------------------
DROP TABLE IF EXISTS `sma_deposits`;
CREATE TABLE `sma_deposits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `amount` decimal(25,4) NOT NULL,
  `paid_by` varchar(50) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_deposits
-- ----------------------------

-- ----------------------------
-- Table structure for sma_expenses
-- ----------------------------
DROP TABLE IF EXISTS `sma_expenses`;
CREATE TABLE `sma_expenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference` varchar(50) NOT NULL,
  `amount` decimal(25,4) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `created_by` varchar(55) NOT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_expenses
-- ----------------------------

-- ----------------------------
-- Table structure for sma_expense_categories
-- ----------------------------
DROP TABLE IF EXISTS `sma_expense_categories`;
CREATE TABLE `sma_expense_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(55) NOT NULL,
  `name` varchar(55) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_expense_categories
-- ----------------------------

-- ----------------------------
-- Table structure for sma_gift_cards
-- ----------------------------
DROP TABLE IF EXISTS `sma_gift_cards`;
CREATE TABLE `sma_gift_cards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `card_no` varchar(20) NOT NULL,
  `value` decimal(25,4) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `balance` decimal(25,4) NOT NULL,
  `expiry` date DEFAULT NULL,
  `created_by` varchar(55) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `card_no` (`card_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_gift_cards
-- ----------------------------

-- ----------------------------
-- Table structure for sma_gift_card_topups
-- ----------------------------
DROP TABLE IF EXISTS `sma_gift_card_topups`;
CREATE TABLE `sma_gift_card_topups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `card_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `card_id` (`card_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_gift_card_topups
-- ----------------------------

-- ----------------------------
-- Table structure for sma_groups
-- ----------------------------
DROP TABLE IF EXISTS `sma_groups`;
CREATE TABLE `sma_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_groups
-- ----------------------------
INSERT INTO `sma_groups` VALUES ('1', 'owner', 'Owner');
INSERT INTO `sma_groups` VALUES ('2', 'admin', 'Administrator');
INSERT INTO `sma_groups` VALUES ('3', 'customer', 'Customer');
INSERT INTO `sma_groups` VALUES ('4', 'supplier', 'Supplier');
INSERT INTO `sma_groups` VALUES ('5', 'sales', 'Sales Staff');

-- ----------------------------
-- Table structure for sma_login_attempts
-- ----------------------------
DROP TABLE IF EXISTS `sma_login_attempts`;
CREATE TABLE `sma_login_attempts` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_login_attempts
-- ----------------------------

-- ----------------------------
-- Table structure for sma_migrations
-- ----------------------------
DROP TABLE IF EXISTS `sma_migrations`;
CREATE TABLE `sma_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_migrations
-- ----------------------------
INSERT INTO `sma_migrations` VALUES ('315');

-- ----------------------------
-- Table structure for sma_notifications
-- ----------------------------
DROP TABLE IF EXISTS `sma_notifications`;
CREATE TABLE `sma_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `from_date` datetime DEFAULT NULL,
  `till_date` datetime DEFAULT NULL,
  `scope` tinyint(1) NOT NULL DEFAULT '3',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_notifications
-- ----------------------------
INSERT INTO `sma_notifications` VALUES ('1', '<p>Thank you for purchasing Stock Manager Advance. Please do not forget to check the documentation in help folder. If you find any error/bug, please email to support@tecdiary.com with details. You can send us your valued suggestions/feedback too.</p><p>Please rate Stock Manager Advance on your download page of codecanyon.net</p>', '2014-08-15 06:00:57', '2015-01-01 00:00:00', '2017-01-01 00:00:00', '3');

-- ----------------------------
-- Table structure for sma_order_ref
-- ----------------------------
DROP TABLE IF EXISTS `sma_order_ref`;
CREATE TABLE `sma_order_ref` (
  `ref_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `so` int(11) NOT NULL DEFAULT '1',
  `qu` int(11) NOT NULL DEFAULT '1',
  `po` int(11) NOT NULL DEFAULT '1',
  `to` int(11) NOT NULL DEFAULT '1',
  `pos` int(11) NOT NULL DEFAULT '1',
  `do` int(11) NOT NULL DEFAULT '1',
  `pay` int(11) NOT NULL DEFAULT '1',
  `re` int(11) NOT NULL DEFAULT '1',
  `rep` int(11) NOT NULL DEFAULT '1',
  `ex` int(11) NOT NULL DEFAULT '1',
  `ppay` int(11) NOT NULL DEFAULT '1',
  `qa` int(11) DEFAULT '1',
  PRIMARY KEY (`ref_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_order_ref
-- ----------------------------
INSERT INTO `sma_order_ref` VALUES ('1', '2015-03-01', '7', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for sma_pages
-- ----------------------------
DROP TABLE IF EXISTS `sma_pages`;
CREATE TABLE `sma_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL,
  `title` varchar(60) NOT NULL,
  `description` varchar(180) NOT NULL,
  `slug` varchar(55) DEFAULT NULL,
  `body` text NOT NULL,
  `active` tinyint(1) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_no` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_pages
-- ----------------------------

-- ----------------------------
-- Table structure for sma_payments
-- ----------------------------
DROP TABLE IF EXISTS `sma_payments`;
CREATE TABLE `sma_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sale_id` int(11) DEFAULT NULL,
  `return_id` int(11) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `reference_no` varchar(50) NOT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `paid_by` varchar(20) NOT NULL,
  `cheque_no` varchar(20) DEFAULT NULL,
  `cc_no` varchar(20) DEFAULT NULL,
  `cc_holder` varchar(25) DEFAULT NULL,
  `cc_month` varchar(2) DEFAULT NULL,
  `cc_year` varchar(4) DEFAULT NULL,
  `cc_type` varchar(20) DEFAULT NULL,
  `amount` decimal(25,4) NOT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `pos_paid` decimal(25,4) DEFAULT '0.0000',
  `pos_balance` decimal(25,4) DEFAULT '0.0000',
  `approval_code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_payments
-- ----------------------------

-- ----------------------------
-- Table structure for sma_paypal
-- ----------------------------
DROP TABLE IF EXISTS `sma_paypal`;
CREATE TABLE `sma_paypal` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `account_email` varchar(255) NOT NULL,
  `paypal_currency` varchar(3) NOT NULL DEFAULT 'USD',
  `fixed_charges` decimal(25,4) NOT NULL DEFAULT '2.0000',
  `extra_charges_my` decimal(25,4) NOT NULL DEFAULT '3.9000',
  `extra_charges_other` decimal(25,4) NOT NULL DEFAULT '4.4000',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_paypal
-- ----------------------------
INSERT INTO `sma_paypal` VALUES ('1', '1', 'mypaypal@paypal.com', 'USD', '0.0000', '0.0000', '0.0000');

-- ----------------------------
-- Table structure for sma_permissions
-- ----------------------------
DROP TABLE IF EXISTS `sma_permissions`;
CREATE TABLE `sma_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `products-index` tinyint(1) DEFAULT '0',
  `products-add` tinyint(1) DEFAULT '0',
  `products-edit` tinyint(1) DEFAULT '0',
  `products-delete` tinyint(1) DEFAULT '0',
  `products-cost` tinyint(1) DEFAULT '0',
  `products-price` tinyint(1) DEFAULT '0',
  `quotes-index` tinyint(1) DEFAULT '0',
  `quotes-add` tinyint(1) DEFAULT '0',
  `quotes-edit` tinyint(1) DEFAULT '0',
  `quotes-pdf` tinyint(1) DEFAULT '0',
  `quotes-email` tinyint(1) DEFAULT '0',
  `quotes-delete` tinyint(1) DEFAULT '0',
  `sales-index` tinyint(1) DEFAULT '0',
  `sales-add` tinyint(1) DEFAULT '0',
  `sales-edit` tinyint(1) DEFAULT '0',
  `sales-pdf` tinyint(1) DEFAULT '0',
  `sales-email` tinyint(1) DEFAULT '0',
  `sales-delete` tinyint(1) DEFAULT '0',
  `purchases-index` tinyint(1) DEFAULT '0',
  `purchases-add` tinyint(1) DEFAULT '0',
  `purchases-edit` tinyint(1) DEFAULT '0',
  `purchases-pdf` tinyint(1) DEFAULT '0',
  `purchases-email` tinyint(1) DEFAULT '0',
  `purchases-delete` tinyint(1) DEFAULT '0',
  `transfers-index` tinyint(1) DEFAULT '0',
  `transfers-add` tinyint(1) DEFAULT '0',
  `transfers-edit` tinyint(1) DEFAULT '0',
  `transfers-pdf` tinyint(1) DEFAULT '0',
  `transfers-email` tinyint(1) DEFAULT '0',
  `transfers-delete` tinyint(1) DEFAULT '0',
  `customers-index` tinyint(1) DEFAULT '0',
  `customers-add` tinyint(1) DEFAULT '0',
  `customers-edit` tinyint(1) DEFAULT '0',
  `customers-delete` tinyint(1) DEFAULT '0',
  `suppliers-index` tinyint(1) DEFAULT '0',
  `suppliers-add` tinyint(1) DEFAULT '0',
  `suppliers-edit` tinyint(1) DEFAULT '0',
  `suppliers-delete` tinyint(1) DEFAULT '0',
  `sales-deliveries` tinyint(1) DEFAULT '0',
  `sales-add_delivery` tinyint(1) DEFAULT '0',
  `sales-edit_delivery` tinyint(1) DEFAULT '0',
  `sales-delete_delivery` tinyint(1) DEFAULT '0',
  `sales-email_delivery` tinyint(1) DEFAULT '0',
  `sales-pdf_delivery` tinyint(1) DEFAULT '0',
  `sales-gift_cards` tinyint(1) DEFAULT '0',
  `sales-add_gift_card` tinyint(1) DEFAULT '0',
  `sales-edit_gift_card` tinyint(1) DEFAULT '0',
  `sales-delete_gift_card` tinyint(1) DEFAULT '0',
  `pos-index` tinyint(1) DEFAULT '0',
  `sales-return_sales` tinyint(1) DEFAULT '0',
  `reports-index` tinyint(1) DEFAULT '0',
  `reports-warehouse_stock` tinyint(1) DEFAULT '0',
  `reports-quantity_alerts` tinyint(1) DEFAULT '0',
  `reports-expiry_alerts` tinyint(1) DEFAULT '0',
  `reports-products` tinyint(1) DEFAULT '0',
  `reports-daily_sales` tinyint(1) DEFAULT '0',
  `reports-monthly_sales` tinyint(1) DEFAULT '0',
  `reports-sales` tinyint(1) DEFAULT '0',
  `reports-payments` tinyint(1) DEFAULT '0',
  `reports-purchases` tinyint(1) DEFAULT '0',
  `reports-profit_loss` tinyint(1) DEFAULT '0',
  `reports-customers` tinyint(1) DEFAULT '0',
  `reports-suppliers` tinyint(1) DEFAULT '0',
  `reports-staff` tinyint(1) DEFAULT '0',
  `reports-register` tinyint(1) DEFAULT '0',
  `sales-payments` tinyint(1) DEFAULT '0',
  `purchases-payments` tinyint(1) DEFAULT '0',
  `purchases-expenses` tinyint(1) DEFAULT '0',
  `products-adjustments` tinyint(1) NOT NULL DEFAULT '0',
  `bulk_actions` tinyint(1) NOT NULL DEFAULT '0',
  `customers-deposits` tinyint(1) NOT NULL DEFAULT '0',
  `customers-delete_deposit` tinyint(1) NOT NULL DEFAULT '0',
  `products-barcode` tinyint(1) NOT NULL DEFAULT '0',
  `purchases-return_purchases` tinyint(1) NOT NULL DEFAULT '0',
  `reports-expenses` tinyint(1) NOT NULL DEFAULT '0',
  `reports-daily_purchases` tinyint(1) DEFAULT '0',
  `reports-monthly_purchases` tinyint(1) DEFAULT '0',
  `products-stock_count` tinyint(1) DEFAULT '0',
  `edit_price` tinyint(1) DEFAULT '0',
  `returns-index` tinyint(1) DEFAULT '0',
  `returns-add` tinyint(1) DEFAULT '0',
  `returns-edit` tinyint(1) DEFAULT '0',
  `returns-delete` tinyint(1) DEFAULT '0',
  `returns-email` tinyint(1) DEFAULT '0',
  `returns-pdf` tinyint(1) DEFAULT '0',
  `reports-tax` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_permissions
-- ----------------------------
INSERT INTO `sma_permissions` VALUES ('1', '5', '1', '0', '0', '0', '0', '0', '1', '1', '1', '1', '1', '0', '1', '1', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0', '0', '1', '1', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for sma_pos_register
-- ----------------------------
DROP TABLE IF EXISTS `sma_pos_register`;
CREATE TABLE `sma_pos_register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `cash_in_hand` decimal(25,4) NOT NULL,
  `status` varchar(10) NOT NULL,
  `total_cash` decimal(25,4) DEFAULT NULL,
  `total_cheques` int(11) DEFAULT NULL,
  `total_cc_slips` int(11) DEFAULT NULL,
  `total_cash_submitted` decimal(25,4) DEFAULT NULL,
  `total_cheques_submitted` int(11) DEFAULT NULL,
  `total_cc_slips_submitted` int(11) DEFAULT NULL,
  `note` text,
  `closed_at` timestamp NULL DEFAULT NULL,
  `transfer_opened_bills` varchar(50) DEFAULT NULL,
  `closed_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_pos_register
-- ----------------------------
INSERT INTO `sma_pos_register` VALUES ('1', '2019-01-15 02:40:28', '1', '0.0000', 'open', null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for sma_pos_settings
-- ----------------------------
DROP TABLE IF EXISTS `sma_pos_settings`;
CREATE TABLE `sma_pos_settings` (
  `pos_id` int(1) NOT NULL,
  `cat_limit` int(11) NOT NULL,
  `pro_limit` int(11) NOT NULL,
  `default_category` int(11) NOT NULL,
  `default_customer` int(11) NOT NULL,
  `default_biller` int(11) NOT NULL,
  `display_time` varchar(3) NOT NULL DEFAULT 'yes',
  `cf_title1` varchar(255) DEFAULT NULL,
  `cf_title2` varchar(255) DEFAULT NULL,
  `cf_value1` varchar(255) DEFAULT NULL,
  `cf_value2` varchar(255) DEFAULT NULL,
  `receipt_printer` varchar(55) DEFAULT NULL,
  `cash_drawer_codes` varchar(55) DEFAULT NULL,
  `focus_add_item` varchar(55) DEFAULT NULL,
  `add_manual_product` varchar(55) DEFAULT NULL,
  `customer_selection` varchar(55) DEFAULT NULL,
  `add_customer` varchar(55) DEFAULT NULL,
  `toggle_category_slider` varchar(55) DEFAULT NULL,
  `toggle_subcategory_slider` varchar(55) DEFAULT NULL,
  `cancel_sale` varchar(55) DEFAULT NULL,
  `suspend_sale` varchar(55) DEFAULT NULL,
  `print_items_list` varchar(55) DEFAULT NULL,
  `finalize_sale` varchar(55) DEFAULT NULL,
  `today_sale` varchar(55) DEFAULT NULL,
  `open_hold_bills` varchar(55) DEFAULT NULL,
  `close_register` varchar(55) DEFAULT NULL,
  `keyboard` tinyint(1) NOT NULL,
  `pos_printers` varchar(255) DEFAULT NULL,
  `java_applet` tinyint(1) NOT NULL,
  `product_button_color` varchar(20) NOT NULL DEFAULT 'default',
  `tooltips` tinyint(1) DEFAULT '1',
  `paypal_pro` tinyint(1) DEFAULT '0',
  `stripe` tinyint(1) DEFAULT '0',
  `rounding` tinyint(1) DEFAULT '0',
  `char_per_line` tinyint(4) DEFAULT '42',
  `pin_code` varchar(20) DEFAULT NULL,
  `purchase_code` varchar(100) DEFAULT 'purchase_code',
  `envato_username` varchar(50) DEFAULT 'envato_username',
  `version` varchar(10) DEFAULT '3.4.11',
  `after_sale_page` tinyint(1) DEFAULT '0',
  `item_order` tinyint(1) DEFAULT '0',
  `authorize` tinyint(1) DEFAULT '0',
  `toggle_brands_slider` varchar(55) DEFAULT NULL,
  `remote_printing` tinyint(1) DEFAULT '1',
  `printer` int(11) DEFAULT NULL,
  `order_printers` varchar(55) DEFAULT NULL,
  `auto_print` tinyint(1) DEFAULT '0',
  `customer_details` tinyint(1) DEFAULT NULL,
  `local_printers` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`pos_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_pos_settings
-- ----------------------------
INSERT INTO `sma_pos_settings` VALUES ('1', '22', '20', '1', '1', '3', '1', 'GST Reg', 'VAT Reg', '123456789', '987654321', 'BIXOLON SRP-350II', 'x1C', 'Ctrl+F3', 'Ctrl+Shift+M', 'Ctrl+Shift+C', 'Ctrl+Shift+A', 'Ctrl+F11', 'Ctrl+F12', 'F4', 'F7', 'F9', 'F8', 'Ctrl+F1', 'Ctrl+F2', 'Ctrl+F10', '1', 'BIXOLON SRP-350II, BIXOLON SRP-350II', '0', 'default', '1', '0', '0', '0', '42', null, 'purchase_code', 'envato_username', '3.4.11', '0', '0', '0', null, '1', null, null, '0', null, null);

-- ----------------------------
-- Table structure for sma_price_groups
-- ----------------------------
DROP TABLE IF EXISTS `sma_price_groups`;
CREATE TABLE `sma_price_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_price_groups
-- ----------------------------
INSERT INTO `sma_price_groups` VALUES ('1', 'Default');

-- ----------------------------
-- Table structure for sma_printers
-- ----------------------------
DROP TABLE IF EXISTS `sma_printers`;
CREATE TABLE `sma_printers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(55) NOT NULL,
  `type` varchar(25) NOT NULL,
  `profile` varchar(25) NOT NULL,
  `char_per_line` tinyint(3) unsigned DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `ip_address` varbinary(45) DEFAULT NULL,
  `port` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_printers
-- ----------------------------

-- ----------------------------
-- Table structure for sma_products
-- ----------------------------
DROP TABLE IF EXISTS `sma_products`;
CREATE TABLE `sma_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `unit` int(11) DEFAULT NULL,
  `cost` decimal(25,4) DEFAULT NULL,
  `price` decimal(25,4) NOT NULL,
  `alert_quantity` decimal(15,4) DEFAULT '20.0000',
  `image` varchar(255) DEFAULT 'no_image.png',
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `cf1` varchar(255) DEFAULT NULL,
  `cf2` varchar(255) DEFAULT NULL,
  `cf3` varchar(255) DEFAULT NULL,
  `cf4` varchar(255) DEFAULT NULL,
  `cf5` varchar(255) DEFAULT NULL,
  `cf6` varchar(255) DEFAULT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `tax_rate` int(11) DEFAULT NULL,
  `track_quantity` tinyint(1) DEFAULT '1',
  `details` varchar(1000) DEFAULT NULL,
  `warehouse` int(11) DEFAULT NULL,
  `barcode_symbology` varchar(55) NOT NULL DEFAULT 'code128',
  `file` varchar(100) DEFAULT NULL,
  `product_details` text,
  `tax_method` tinyint(1) DEFAULT '0',
  `type` varchar(55) NOT NULL DEFAULT 'standard',
  `supplier1` int(11) DEFAULT NULL,
  `supplier1price` decimal(25,4) DEFAULT NULL,
  `supplier2` int(11) DEFAULT NULL,
  `supplier2price` decimal(25,4) DEFAULT NULL,
  `supplier3` int(11) DEFAULT NULL,
  `supplier3price` decimal(25,4) DEFAULT NULL,
  `supplier4` int(11) DEFAULT NULL,
  `supplier4price` decimal(25,4) DEFAULT NULL,
  `supplier5` int(11) DEFAULT NULL,
  `supplier5price` decimal(25,4) DEFAULT NULL,
  `promotion` tinyint(1) DEFAULT '0',
  `promo_price` decimal(25,4) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `supplier1_part_no` varchar(50) DEFAULT NULL,
  `supplier2_part_no` varchar(50) DEFAULT NULL,
  `supplier3_part_no` varchar(50) DEFAULT NULL,
  `supplier4_part_no` varchar(50) DEFAULT NULL,
  `supplier5_part_no` varchar(50) DEFAULT NULL,
  `sale_unit` int(11) DEFAULT NULL,
  `purchase_unit` int(11) DEFAULT NULL,
  `brand` int(11) DEFAULT NULL,
  `slug` varchar(55) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  `weight` decimal(10,4) DEFAULT NULL,
  `hsn_code` int(11) DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `hide` tinyint(1) NOT NULL DEFAULT '0',
  `second_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `category_id` (`category_id`),
  KEY `id` (`id`),
  KEY `id_2` (`id`),
  KEY `category_id_2` (`category_id`),
  KEY `unit` (`unit`),
  KEY `brand` (`brand`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_products
-- ----------------------------
INSERT INTO `sma_products` VALUES ('1', '98062731', 'Cocacola', '1', '0.0000', '1.0000', '0.0000', 'no_image.png', '1', null, '', '', '', '', '', '', '-51.0000', '1', '1', '', null, 'code128', '', '', '1', 'standard', '0', null, null, null, null, null, null, null, null, null, null, null, null, null, '', null, null, null, null, '1', '1', '0', 'cocacola', null, '0.0000', null, '0', '0', '');
INSERT INTO `sma_products` VALUES ('2', 'SK0001', 'Sake', '1', '10.0000', '20.0000', '0.0000', 'no_image.png', '1', null, '', '', '', '', '', '', '-10.0000', '1', '1', '', null, 'code128', '', '', '1', 'standard', '0', null, null, null, null, null, null, null, null, null, null, null, null, null, '', null, null, null, null, '1', '1', '0', 'sake', null, '0.0000', null, '0', '0', '');
INSERT INTO `sma_products` VALUES ('3', 'MI0001', 'Chicken', '1', '5.0000', '10.0000', '0.0000', 'no_image.png', '1', null, '', '', '', '', '', '', null, '1', '1', '', null, 'code128', '', '', '1', 'standard', '0', null, null, null, null, null, null, null, null, null, null, null, null, null, '', null, null, null, null, '1', '1', '0', 'chicken', null, '0.0000', null, '0', '0', '');
INSERT INTO `sma_products` VALUES ('4', 'WA0001', 'Water', '1', '50.0000', '100.0000', '0.0000', 'no_image.png', '1', null, '', '', '', '', '', '', null, '1', '1', '', null, 'code128', '', '', '1', 'standard', '0', null, null, null, null, null, null, null, null, null, null, null, null, null, '', null, null, null, null, '1', '1', '0', 'water', null, '0.0000', null, '0', '0', '');
INSERT INTO `sma_products` VALUES ('5', 'EK0001', 'Ekimiso', '1', '10.0000', '15.8000', '0.0000', 'no_image.png', '1', null, '', '', '', '', '', '', '-11.0000', '1', '1', '', null, 'code128', '', '', '1', 'standard', '0', null, null, null, null, null, null, null, null, null, null, null, null, null, '', null, null, null, null, '1', '1', '0', 'ekimiso', null, '0.0000', null, '0', '0', '');
INSERT INTO `sma_products` VALUES ('6', 'MI0002', 'MISO', '1', '10.0000', '15.9000', '0.0000', 'no_image.png', '1', null, '', '', '', '', '', '', '-11.0000', '1', '1', '', null, 'code128', '', '', '1', 'standard', '0', null, null, null, null, null, null, null, null, null, null, null, null, null, '', null, null, null, null, '0', '0', '0', 'miso', null, '0.0000', null, '0', '0', '');
INSERT INTO `sma_products` VALUES ('7', 'MI0003', 'AKAMISO', '1', '10.0000', '15.9500', '0.0000', 'no_image.png', '1', null, '', '', '', '', '', '', '-21.0000', '1', '1', '', null, 'code128', '', '', '1', 'standard', '0', null, null, null, null, null, null, null, null, null, null, null, null, null, '', null, null, null, null, '1', '1', '0', 'akamiso', null, '0.0000', null, '0', '0', '');

-- ----------------------------
-- Table structure for sma_product_discount
-- ----------------------------
DROP TABLE IF EXISTS `sma_product_discount`;
CREATE TABLE `sma_product_discount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `customer_name` varchar(200) DEFAULT NULL,
  `sale_person` varchar(200) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_name` varchar(200) DEFAULT NULL,
  `normal_price` decimal(25,4) DEFAULT NULL,
  `special_price` decimal(25,4) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `discount_price` int(11) DEFAULT NULL,
  `note` text,
  `status` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=770 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_product_discount
-- ----------------------------
INSERT INTO `sma_product_discount` VALUES ('765', '3', 'Mian Saleem', 'Owner', 'MI0001', '3', 'Chicken', '10.0000', '9.5000', '2019-01-14', '2019-01-19', '5', '<p>ffghgfh</p>', null);
INSERT INTO `sma_product_discount` VALUES ('766', '6', 'SR', 'Taiyo', 'MI0002', '6', 'MISO', '17.5000', '17.3300', '2019-01-14', '2019-01-26', '1', '', null);
INSERT INTO `sma_product_discount` VALUES ('767', '6', 'SR', 'Taiyo', 'SK0001', '2', 'Sake', '22.0000', '11.0000', '1899-12-31', '2019-06-07', '50', '', null);
INSERT INTO `sma_product_discount` VALUES ('768', '5', 'New', 'nanae', '98062731', '1', 'Cocacola', '1.1000', '0.9900', '2019-01-15', '2019-02-28', '10', '', null);
INSERT INTO `sma_product_discount` VALUES ('769', '7', '2month', 'kota', 'EK0001', '5', 'Ekimiso', '17.4000', '17.4000', '1899-12-31', '2019-02-07', '20', '', null);

-- ----------------------------
-- Table structure for sma_product_photos
-- ----------------------------
DROP TABLE IF EXISTS `sma_product_photos`;
CREATE TABLE `sma_product_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `photo` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_product_photos
-- ----------------------------

-- ----------------------------
-- Table structure for sma_product_prices
-- ----------------------------
DROP TABLE IF EXISTS `sma_product_prices`;
CREATE TABLE `sma_product_prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `price_group_id` int(11) NOT NULL,
  `price` decimal(25,4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `price_group_id` (`price_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_product_prices
-- ----------------------------

-- ----------------------------
-- Table structure for sma_product_variants
-- ----------------------------
DROP TABLE IF EXISTS `sma_product_variants`;
CREATE TABLE `sma_product_variants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `cost` decimal(25,4) DEFAULT NULL,
  `price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_product_variants
-- ----------------------------

-- ----------------------------
-- Table structure for sma_purchases
-- ----------------------------
DROP TABLE IF EXISTS `sma_purchases`;
CREATE TABLE `sma_purchases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference_no` varchar(55) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `supplier_id` int(11) NOT NULL,
  `supplier` varchar(55) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` varchar(1000) NOT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `product_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_discount` decimal(25,4) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT NULL,
  `product_tax` decimal(25,4) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `paid` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `status` varchar(55) DEFAULT '',
  `payment_status` varchar(20) DEFAULT 'pending',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `payment_term` tinyint(4) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `return_id` int(11) DEFAULT NULL,
  `surcharge` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `return_purchase_ref` varchar(55) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `return_purchase_total` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_purchases
-- ----------------------------

-- ----------------------------
-- Table structure for sma_purchase_items
-- ----------------------------
DROP TABLE IF EXISTS `sma_purchase_items`;
CREATE TABLE `sma_purchase_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) DEFAULT NULL,
  `transfer_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(20) DEFAULT NULL,
  `discount` varchar(20) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `quantity_balance` decimal(15,4) DEFAULT '0.0000',
  `date` date NOT NULL,
  `status` varchar(50) NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL,
  `quantity_received` decimal(15,4) DEFAULT NULL,
  `supplier_part_no` varchar(50) DEFAULT NULL,
  `purchase_item_id` int(11) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `gst` varchar(20) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_id` (`purchase_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_purchase_items
-- ----------------------------
INSERT INTO `sma_purchase_items` VALUES ('1', null, null, '1', '', '', null, '0.0000', '0.0000', '1', '0.0000', null, null, null, null, null, '0.0000', '-51.0000', '0000-00-00', 'received', null, null, null, null, null, null, null, '0.0000', null, null, null, null);
INSERT INTO `sma_purchase_items` VALUES ('2', null, null, '5', '', '', null, '0.0000', '0.0000', '1', '0.0000', null, null, null, null, null, '0.0000', '-11.0000', '0000-00-00', 'received', null, null, null, null, null, null, null, '0.0000', null, null, null, null);
INSERT INTO `sma_purchase_items` VALUES ('3', null, null, '6', '', '', null, '0.0000', '0.0000', '1', '0.0000', null, null, null, null, null, '0.0000', '-11.0000', '0000-00-00', 'received', null, null, null, null, null, null, null, '0.0000', null, null, null, null);
INSERT INTO `sma_purchase_items` VALUES ('4', null, null, '7', '', '', null, '0.0000', '0.0000', '1', '0.0000', null, null, null, null, null, '0.0000', '-21.0000', '0000-00-00', 'received', null, null, null, null, null, null, null, '0.0000', null, null, null, null);
INSERT INTO `sma_purchase_items` VALUES ('5', null, null, '2', '', '', null, '0.0000', '0.0000', '1', '0.0000', null, null, null, null, null, '0.0000', '-10.0000', '0000-00-00', 'received', null, null, null, null, null, null, null, '0.0000', null, null, null, null);

-- ----------------------------
-- Table structure for sma_quotes
-- ----------------------------
DROP TABLE IF EXISTS `sma_quotes`;
CREATE TABLE `sma_quotes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `internal_note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT NULL,
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `supplier` varchar(55) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_quotes
-- ----------------------------

-- ----------------------------
-- Table structure for sma_quote_items
-- ----------------------------
DROP TABLE IF EXISTS `sma_quote_items`;
CREATE TABLE `sma_quote_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quote_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `gst` varchar(20) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `quote_id` (`quote_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_quote_items
-- ----------------------------

-- ----------------------------
-- Table structure for sma_returns
-- ----------------------------
DROP TABLE IF EXISTS `sma_returns`;
CREATE TABLE `sma_returns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `staff_note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT '0.0000',
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_items` smallint(6) DEFAULT NULL,
  `paid` decimal(25,4) DEFAULT '0.0000',
  `surcharge` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `attachment` varchar(55) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_returns
-- ----------------------------

-- ----------------------------
-- Table structure for sma_return_items
-- ----------------------------
DROP TABLE IF EXISTS `sma_return_items`;
CREATE TABLE `sma_return_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `return_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `gst` varchar(20) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `return_id` (`return_id`),
  KEY `product_id` (`product_id`),
  KEY `product_id_2` (`product_id`,`return_id`),
  KEY `return_id_2` (`return_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_return_items
-- ----------------------------

-- ----------------------------
-- Table structure for sma_sales
-- ----------------------------
DROP TABLE IF EXISTS `sma_sales`;
CREATE TABLE `sma_sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `staff_note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT '0.0000',
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `sale_status` varchar(20) DEFAULT NULL,
  `payment_status` varchar(20) DEFAULT NULL,
  `payment_term` tinyint(4) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_items` smallint(6) DEFAULT NULL,
  `pos` tinyint(1) NOT NULL DEFAULT '0',
  `paid` decimal(25,4) DEFAULT '0.0000',
  `return_id` int(11) DEFAULT NULL,
  `surcharge` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `attachment` varchar(55) DEFAULT NULL,
  `return_sale_ref` varchar(55) DEFAULT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `return_sale_total` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `rounding` decimal(10,4) DEFAULT NULL,
  `suspend_note` varchar(255) DEFAULT NULL,
  `api` tinyint(1) DEFAULT '0',
  `shop` tinyint(1) DEFAULT '0',
  `address_id` int(11) DEFAULT NULL,
  `reserve_id` int(11) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `manual_payment` varchar(55) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  `payment_method` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_sales
-- ----------------------------
INSERT INTO `sma_sales` VALUES ('2', '2019-01-10 11:22:00', 'SALE2019/01/0002', '1', 'Walk-in Customer', '3', 'Test Biller', '1', '', '', '50.0000', '0.0000', '', '0.0000', '0.0000', '0.0000', '1', '0.0000', '0.0000', '0.0000', '50.0000', 'completed', 'pending', '0', null, '1', null, null, '50', '0', '0.0000', null, '0.0000', null, null, null, '0.0000', null, null, '0', '0', null, null, 'c5553073133738c0bd69180ed67805c92376ab21aab5c8fe07c860acae503a38', null, null, null, null, null);
INSERT INTO `sma_sales` VALUES ('3', '2019-01-11 15:18:00', 'SALE2019/01/0003', '1', 'Walk-in Customer', '3', 'Test Biller', '1', '', '', '47.7000', '0.0000', '', '0.0000', '0.0000', '0.0000', '1', '0.0000', '0.0000', '0.0000', '47.7000', 'completed', 'pending', '0', null, '1', null, null, '3', '0', '0.0000', null, '0.0000', null, null, null, '0.0000', null, null, '0', '0', null, null, 'a47c2753f2f0240adb51c01381deb8778622fe55b41cf45ff48326828e1006a7', null, null, null, null, null);
INSERT INTO `sma_sales` VALUES ('4', '2019-01-11 15:19:00', 'SALE2019/01/0004', '1', 'Walk-in Customer', '3', 'Test Biller', '1', '', '', '837.0000', '0.0000', '', '0.0000', '0.0000', '0.0000', '1', '0.0000', '0.0000', '0.0000', '837.0000', 'completed', 'pending', '0', null, '1', null, null, '50', '0', '0.0000', null, '0.0000', null, null, null, '0.0000', null, null, '0', '0', null, null, 'a549ed8cc376f1cddc2fdadbd0f6709297428511a9b1b600141fd5e1478c2114', null, null, null, null, null);
INSERT INTO `sma_sales` VALUES ('5', '2019-01-16 09:26:00', 'SALE2019/01/0006', '5', 'Newcompany', '3', 'Test Biller', '1', '', '', '1.1000', '0.0000', '', '0.0000', '0.0000', '0.0000', '1', '0.0000', '0.0000', '0.0000', '1.1000', 'completed', 'pending', '0', null, '1', null, null, '1', '0', '0.0000', null, '0.0000', null, null, null, '0.0000', null, null, '0', '0', null, null, '4156ab69aaa4074677f6b515d758232d01741b595645dd3933e4f798a7a8d07c', null, null, null, null, null);

-- ----------------------------
-- Table structure for sma_sale_items
-- ----------------------------
DROP TABLE IF EXISTS `sma_sale_items`;
CREATE TABLE `sma_sale_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `sale_item_id` int(11) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `gst` varchar(20) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sale_id` (`sale_id`),
  KEY `product_id` (`product_id`),
  KEY `product_id_2` (`product_id`,`sale_id`),
  KEY `sale_id_2` (`sale_id`,`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_sale_items
-- ----------------------------
INSERT INTO `sma_sale_items` VALUES ('2', '2', '1', '98062731', 'Cocacola', 'standard', null, '1.0000', '1.0000', '50.0000', '1', '0.0000', '1', '0', '0', '0.0000', '50.0000', '', '1.0000', null, '1', '001', '50.0000', null, null, null, null, null);
INSERT INTO `sma_sale_items` VALUES ('3', '3', '5', 'EK0001', 'Ekimiso', 'standard', null, '15.8000', '15.8000', '1.0000', '1', '0.0000', '1', '0', '0', '0.0000', '15.8000', '', '15.8000', null, '1', '001', '1.0000', null, null, null, null, null);
INSERT INTO `sma_sale_items` VALUES ('4', '3', '6', 'MI0002', 'MISO', 'standard', null, '15.9000', '15.9000', '1.0000', '1', '0.0000', '1', '0', '0', '0.0000', '15.9000', '', '15.9000', null, '1', '001', '1.0000', null, null, null, null, null);
INSERT INTO `sma_sale_items` VALUES ('5', '3', '7', 'MI0003', 'AKAMISO', 'standard', null, '16.0000', '16.0000', '1.0000', '1', '0.0000', '1', '0', '0', '0.0000', '16.0000', '', '16.0000', null, '1', '001', '1.0000', null, null, null, null, null);
INSERT INTO `sma_sale_items` VALUES ('6', '4', '2', 'SK0001', 'Sake', 'standard', null, '20.0000', '20.0000', '10.0000', '1', '0.0000', '1', '0', '0', '0.0000', '200.0000', '', '20.0000', null, '1', '001', '10.0000', null, null, null, null, null);
INSERT INTO `sma_sale_items` VALUES ('7', '4', '5', 'EK0001', 'Ekimiso', 'standard', null, '15.8000', '15.8000', '10.0000', '1', '0.0000', '1', '0', '0', '0.0000', '158.0000', '', '15.8000', null, '1', '001', '10.0000', null, null, null, null, null);
INSERT INTO `sma_sale_items` VALUES ('8', '4', '7', 'MI0003', 'AKAMISO', 'standard', null, '16.0000', '16.0000', '10.0000', '1', '0.0000', '1', '0', '0', '0.0000', '160.0000', '', '16.0000', null, '1', '001', '10.0000', null, null, null, null, null);
INSERT INTO `sma_sale_items` VALUES ('9', '4', '7', 'MI0003', 'AKAMISO', 'standard', null, '16.0000', '16.0000', '10.0000', '1', '0.0000', '1', '0', '0', '0.0000', '160.0000', '', '16.0000', null, '1', '001', '10.0000', null, null, null, null, null);
INSERT INTO `sma_sale_items` VALUES ('10', '4', '6', 'MI0002', 'MISO', 'standard', null, '15.9000', '15.9000', '10.0000', '1', '0.0000', '1', '0', '0', '0.0000', '159.0000', '', '15.9000', null, '1', '001', '10.0000', null, null, null, null, null);
INSERT INTO `sma_sale_items` VALUES ('11', '5', '1', '98062731', 'Cocacola', 'standard', null, '1.1000', '1.1000', '1.0000', '1', '0.0000', '1', '0', '0', '0.0000', '1.1000', '', '1.1000', null, '1', '001', '1.0000', null, null, null, null, null);

-- ----------------------------
-- Table structure for sma_sessions
-- ----------------------------
DROP TABLE IF EXISTS `sma_sessions`;
CREATE TABLE `sma_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_sessions
-- ----------------------------
INSERT INTO `sma_sessions` VALUES ('0gp3qambjkbf54oapukgd8oa7iqr1qeo', '::1', '1547538035', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373533373733393B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('0h701hdq2ffd114satpo7409b406pcf4', '::1', '1547537585', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373533373330373B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('0pq331se7d3su11641fv2hq732aa6eos', '::1', '1547534522', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373533343233393B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('20hjhjhgf5a7fnm2h9llrbp7qrbhncl0', '::1', '1547535700', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373533353431363B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('21e4n46iktbti0g4rg2hti8kpt5mv7hm', '::1', '1547536374', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373533363037383B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('2km2d25e0o5sbqmdc3ke62tamh6vk294', '::1', '1547538361', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373533383038353B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('2nh4vmnlihb5ej2djblp4jsdfa6c2g7k', '::1', '1547607077', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373630373037373B6572726F727C733A3139393A223C68343E343034204E6F7420466F756E64213C2F68343E3C703E546865207061676520796F7520617265206C6F6F6B696E6720666F722063616E206E6F7420626520666F756E642E3C2F703E687474703A2F2F6C6F63616C686F73743A383838382F43616D626F696E666F2F73746F636B2D6D616E616765722F7468656D65732F64656661756C742F61646D696E2F6173736574732F7374796C65732F7064662F666F6E74732F676C79706869636F6E732D68616C666C696E67732D726567756C61722E747466223B5F5F63695F766172737C613A313A7B733A353A226572726F72223B733A333A226E6577223B7D);
INSERT INTO `sma_sessions` VALUES ('53qosqqq25p767l4212vav05egfce0hj', '::1', '1547536143', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373533363134333B);
INSERT INTO `sma_sessions` VALUES ('5bsk0l8ukus7s4ieif4koh2lt80er08b', '::1', '1547607077', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373630373037373B);
INSERT INTO `sma_sessions` VALUES ('5g2dc2dvjro92g3bvb2c0u764jc8qr13', '::1', '1547604749', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373630343734353B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437353137393537223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6572726F727C4E3B);
INSERT INTO `sma_sessions` VALUES ('6jmeeuo3pbei85eor77eepqnjtncg09t', '::1', '1547534770', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373533343535343B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('76h25c8oof229nh5iqca10nvcvoros5s', '::1', '1547540913', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373534303931323B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('7gim2pp1h5ip90rra66k1be872sbemec', '::1', '1547539890', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373533393837363B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('8f2r1oj32jjlm7l4p8fe8h8pg0n9j5f1', '::1', '1547606286', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373630363139343B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437353137393537223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6572726F727C4E3B);
INSERT INTO `sma_sessions` VALUES ('90v3di6gdj462sj44bu7tmvf2cu9pq5p', '::1', '1547607082', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373630373037323B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437353137393537223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6572726F727C4E3B);
INSERT INTO `sma_sessions` VALUES ('9ffte4acejch5qfv8cq8rkr98gkgaq0d', '::1', '1547537217', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373533363936383B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('9jl7gr4qi9h4cplq7ufdcshf7j0e1u5f', '::1', '1547533281', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373533333234363B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('9m3i0uai6f8dm54onss9s2gn2cm7nf3t', '10.201.233.60', '1547606505', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373630363530353B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437363034303536223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B);
INSERT INTO `sma_sessions` VALUES ('b4kk3sae7pctvk1ki1mtbhkosh16s2bv', '::1', '1547605679', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373630353430303B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437353137393537223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6572726F727C4E3B);
INSERT INTO `sma_sessions` VALUES ('b6vnsglqml229o3mfnsn2h5l7aku3e15', '::1', '1547536031', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373533353736303B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('c5e00mnej29mm4n5i9e99mvvc2n7oipa', '10.201.233.60', '1547605762', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373630353539393B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437363034303536223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B);
INSERT INTO `sma_sessions` VALUES ('f5qvg9f19345cgjqad64eos82u3aodbk', '10.201.0.1', '1547605158', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373630343837383B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437363034303536223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B);
INSERT INTO `sma_sessions` VALUES ('f7k0upe3f9bmqqi1n2bbvfkpmf1p38qm', '::1', '1547605970', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373630353731383B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437353137393537223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6572726F727C4E3B);
INSERT INTO `sma_sessions` VALUES ('fhcmijobqju4b8ski6ijh6t5asj1nqv9', '::1', '1547607078', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373630373037383B);
INSERT INTO `sma_sessions` VALUES ('fj82854d972uml0bco7tuk21j7bbec33', '10.201.255.248', '1547604717', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373630343730353B7265717565737465645F706167657C733A31333A2261646D696E2F7265706F727473223B);
INSERT INTO `sma_sessions` VALUES ('gcurqmpeh5i0r8907a8p7vqh2rbdmkvh', '::1', '1547534191', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373533333930313B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('gkmj00hh421pv2c7ue1rhcn3es1vr2um', '::1', '1547540870', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373534303537373B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('hev1mv6v1s2cthj7oiafrbfvk0ht7l6q', '::1', '1547539556', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373533393236303B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('l2bghlb4s5u98mjinc7ged9efsr0lmcn', '::1', '1547527947', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373532373832363B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('mbjaqiam0tbtqvo80ukjasj9cj00gara', '::1', '1547539873', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373533393537333B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('nekhfm3eefir3dqrei41pf0fdr4r0dqv', '::1', '1547534990', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373533343838373B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('o5jevu14h97nu2hjbkh10ja2k51q8tjj', '::1', '1547538953', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373533383635353B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('osp0kv53gc6ph050qapmvpvd3gh2gk0a', '::1', '1547606563', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373630363536333B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437353137393537223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6572726F727C4E3B);
INSERT INTO `sma_sessions` VALUES ('pp1ld1tfqvis8gphrdeu1t4vko15ra6c', '::1', '1547532914', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373533323634363B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('pt1jmtebop74culukmcnnu9fmr48t7hp', '::1', '1547533802', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373533333537383B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('rismavt7mrpimfgiqpbfckheu8oi9hn4', '::1', '1547541329', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373534313332393B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('ruric7sm0nqq70dia50leem943qamdta', '::1', '1547536142', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373533363134313B7265717565737465645F706167657C733A34393A2261646D696E2F7265706F7274732F73616C65735F7265706F72745F666F725F656163685F637573746F6D65722F32303230223B);
INSERT INTO `sma_sessions` VALUES ('sck3nl5q91gr4beq0c31juhb5r1avo4o', '::1', '1547539136', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373533383935363B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437343439353239223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B6C6173745F61637469766974797C693A313534373532303539323B72656769737465725F69647C733A313A2231223B636173685F696E5F68616E647C733A363A22302E30303030223B72656769737465725F6F70656E5F74696D657C733A31393A22323031392D30312D31352030323A34303A3238223B);
INSERT INTO `sma_sessions` VALUES ('vq43eug461sjmmpe7h7hd1f5fbqm8lnh', '::1', '1547604098', 0x5F5F63695F6C6173745F726567656E65726174657C693A313534373630333933383B7265717565737465645F706167657C733A353A2261646D696E223B6964656E746974797C733A353A226F776E6572223B757365726E616D657C733A353A226F776E6572223B656D61696C7C733A31383A226F776E65724074656364696172792E636F6D223B757365725F69647C733A313A2231223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231353437353137393537223B6C6173745F69707C733A333A223A3A31223B6176617461727C4E3B67656E6465727C733A343A226D616C65223B67726F75705F69647C733A313A2231223B77617265686F7573655F69647C4E3B766965775F72696768747C733A313A2230223B656469745F72696768747C733A313A2230223B616C6C6F775F646973636F756E747C733A313A2230223B62696C6C65725F69647C4E3B636F6D70616E795F69647C4E3B73686F775F636F73747C733A313A2230223B73686F775F70726963657C733A313A2230223B);

-- ----------------------------
-- Table structure for sma_settings
-- ----------------------------
DROP TABLE IF EXISTS `sma_settings`;
CREATE TABLE `sma_settings` (
  `setting_id` int(1) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `logo2` varchar(255) NOT NULL,
  `site_name` varchar(55) NOT NULL,
  `language` varchar(20) NOT NULL,
  `default_warehouse` int(2) NOT NULL,
  `accounting_method` tinyint(4) NOT NULL DEFAULT '0',
  `default_currency` varchar(3) NOT NULL,
  `default_tax_rate` int(2) NOT NULL,
  `rows_per_page` int(2) NOT NULL,
  `version` varchar(10) NOT NULL DEFAULT '1.0',
  `default_tax_rate2` int(11) NOT NULL DEFAULT '0',
  `dateformat` int(11) NOT NULL,
  `sales_prefix` varchar(20) DEFAULT NULL,
  `quote_prefix` varchar(20) DEFAULT NULL,
  `purchase_prefix` varchar(20) DEFAULT NULL,
  `transfer_prefix` varchar(20) DEFAULT NULL,
  `delivery_prefix` varchar(20) DEFAULT NULL,
  `payment_prefix` varchar(20) DEFAULT NULL,
  `return_prefix` varchar(20) DEFAULT NULL,
  `returnp_prefix` varchar(20) DEFAULT NULL,
  `expense_prefix` varchar(20) DEFAULT NULL,
  `item_addition` tinyint(1) NOT NULL DEFAULT '0',
  `theme` varchar(20) NOT NULL,
  `product_serial` tinyint(4) NOT NULL,
  `default_discount` int(11) NOT NULL,
  `product_discount` tinyint(1) NOT NULL DEFAULT '0',
  `discount_method` tinyint(4) NOT NULL,
  `tax1` tinyint(4) NOT NULL,
  `tax2` tinyint(4) NOT NULL,
  `overselling` tinyint(1) NOT NULL DEFAULT '0',
  `restrict_user` tinyint(4) NOT NULL DEFAULT '0',
  `restrict_calendar` tinyint(4) NOT NULL DEFAULT '0',
  `timezone` varchar(100) DEFAULT NULL,
  `iwidth` int(11) NOT NULL DEFAULT '0',
  `iheight` int(11) NOT NULL,
  `twidth` int(11) NOT NULL,
  `theight` int(11) NOT NULL,
  `watermark` tinyint(1) DEFAULT NULL,
  `reg_ver` tinyint(1) DEFAULT NULL,
  `allow_reg` tinyint(1) DEFAULT NULL,
  `reg_notification` tinyint(1) DEFAULT NULL,
  `auto_reg` tinyint(1) DEFAULT NULL,
  `protocol` varchar(20) NOT NULL DEFAULT 'mail',
  `mailpath` varchar(55) DEFAULT '/usr/sbin/sendmail',
  `smtp_host` varchar(100) DEFAULT NULL,
  `smtp_user` varchar(100) DEFAULT NULL,
  `smtp_pass` varchar(255) DEFAULT NULL,
  `smtp_port` varchar(10) DEFAULT '25',
  `smtp_crypto` varchar(10) DEFAULT NULL,
  `corn` datetime DEFAULT NULL,
  `customer_group` int(11) NOT NULL,
  `default_email` varchar(100) NOT NULL,
  `mmode` tinyint(1) NOT NULL,
  `bc_fix` tinyint(4) NOT NULL DEFAULT '0',
  `auto_detect_barcode` tinyint(1) NOT NULL DEFAULT '0',
  `captcha` tinyint(1) NOT NULL DEFAULT '1',
  `reference_format` tinyint(1) NOT NULL DEFAULT '1',
  `racks` tinyint(1) DEFAULT '0',
  `attributes` tinyint(1) NOT NULL DEFAULT '0',
  `product_expiry` tinyint(1) NOT NULL DEFAULT '0',
  `decimals` tinyint(2) NOT NULL DEFAULT '2',
  `qty_decimals` tinyint(2) NOT NULL DEFAULT '2',
  `decimals_sep` varchar(2) NOT NULL DEFAULT '.',
  `thousands_sep` varchar(2) NOT NULL DEFAULT ',',
  `invoice_view` tinyint(1) DEFAULT '0',
  `default_biller` int(11) DEFAULT NULL,
  `envato_username` varchar(50) DEFAULT NULL,
  `purchase_code` varchar(100) DEFAULT NULL,
  `rtl` tinyint(1) DEFAULT '0',
  `each_spent` decimal(15,4) DEFAULT NULL,
  `ca_point` tinyint(4) DEFAULT NULL,
  `each_sale` decimal(15,4) DEFAULT NULL,
  `sa_point` tinyint(4) DEFAULT NULL,
  `update` tinyint(1) DEFAULT '0',
  `sac` tinyint(1) DEFAULT '0',
  `display_all_products` tinyint(1) DEFAULT '0',
  `display_symbol` tinyint(1) DEFAULT NULL,
  `symbol` varchar(50) DEFAULT NULL,
  `remove_expired` tinyint(1) DEFAULT '0',
  `barcode_separator` varchar(2) NOT NULL DEFAULT '-',
  `set_focus` tinyint(1) NOT NULL DEFAULT '0',
  `price_group` int(11) DEFAULT NULL,
  `barcode_img` tinyint(1) NOT NULL DEFAULT '1',
  `ppayment_prefix` varchar(20) DEFAULT 'POP',
  `disable_editing` smallint(6) DEFAULT '90',
  `qa_prefix` varchar(55) DEFAULT NULL,
  `update_cost` tinyint(1) DEFAULT NULL,
  `apis` tinyint(1) NOT NULL DEFAULT '0',
  `state` varchar(100) DEFAULT NULL,
  `pdf_lib` varchar(20) DEFAULT 'dompdf',
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_settings
-- ----------------------------
INSERT INTO `sma_settings` VALUES ('1', 'logo2.png', 'logo3.png', 'Stock Manager Advance', 'english', '1', '2', 'USD', '1', '100', '3.4.11', '1', '5', 'SALE', 'QUOTE', 'PO', 'TR', 'DO', 'IPAY', 'SR', 'PR', '', '0', 'default', '1', '1', '1', '1', '1', '1', '1', '1', '0', 'Africa/Abidjan', '800', '800', '150', '150', '0', '0', '0', '0', null, 'mail', '/usr/sbin/sendmail', 'pop.gmail.com', 'contact@sma.tecdiary.org', 'jEFTM4T63AiQ9dsidxhPKt9CIg4HQjCN58n/RW9vmdC/UDXCzRLR469ziZ0jjpFlbOg43LyoSmpJLBkcAHh0Yw==', '25', null, null, '1', 'contact@tecdiary.com', '0', '4', '1', '0', '2', '1', '1', '0', '2', '2', '.', ',', '0', '3', 'yasutori7', '35c48be6-0c89-4a02-a1d1-af610e05002c', '0', null, null, null, null, '0', '0', '0', '0', '', '0', '-', '0', '1', '1', 'POP', '90', '', '0', '0', 'AN', 'dompdf');

-- ----------------------------
-- Table structure for sma_shop_settings
-- ----------------------------
DROP TABLE IF EXISTS `sma_shop_settings`;
CREATE TABLE `sma_shop_settings` (
  `shop_id` int(11) NOT NULL,
  `shop_name` varchar(55) NOT NULL,
  `description` varchar(160) NOT NULL,
  `warehouse` int(11) NOT NULL,
  `biller` int(11) NOT NULL,
  `about_link` varchar(55) NOT NULL,
  `terms_link` varchar(55) NOT NULL,
  `privacy_link` varchar(55) NOT NULL,
  `contact_link` varchar(55) NOT NULL,
  `payment_text` varchar(100) NOT NULL,
  `follow_text` varchar(100) NOT NULL,
  `facebook` varchar(55) NOT NULL,
  `twitter` varchar(55) DEFAULT NULL,
  `google_plus` varchar(55) DEFAULT NULL,
  `instagram` varchar(55) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `cookie_message` varchar(180) DEFAULT NULL,
  `cookie_link` varchar(55) DEFAULT NULL,
  `slider` text,
  `shipping` int(11) DEFAULT NULL,
  `purchase_code` varchar(100) DEFAULT 'purchase_code',
  `envato_username` varchar(50) DEFAULT 'envato_username',
  `version` varchar(10) DEFAULT '3.4.10',
  `logo` varchar(55) DEFAULT NULL,
  `bank_details` varchar(255) DEFAULT NULL,
  `products_page` tinyint(1) DEFAULT NULL,
  `hide0` tinyint(1) DEFAULT '0',
  `products_description` varchar(255) DEFAULT NULL,
  `private` tinyint(1) DEFAULT '0',
  `hide_price` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`shop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_shop_settings
-- ----------------------------
INSERT INTO `sma_shop_settings` VALUES ('1', 'SMA Shop', 'Stock Manager Advance - SMA Shop - Demo Ecommerce Shop that would help you to sell your products from your site.', '1', '3', 'about', 'terms-conditions', 'privacy-policy', 'contact', 'We accept PayPal or you can pay with your credit/debit cards.', 'Please click the link below to follow us on social media.', 'http://facebook.com/tecdiary', 'http://twitter.com/tecdiary', '', '', '010 1234 567', 'info@tecdiary.com', 'We use cookies to improve your experience on our website. By browsing this website, you agree to our use of cookies.', '', '[{\"image\":\"s1.jpg\",\"link\":\"http:\\/\\/ci.dev\\/sma\\/shop\\/products\",\"caption\":\"\"},{\"image\":\"s2.jpg\",\"link\":\"\",\"caption\":\"\"},{\"image\":\"s3.jpg\",\"link\":\"\",\"caption\":\"\"},{\"link\":\"\",\"caption\":\"\"},{\"link\":\"\",\"caption\":\"\"}]', '0', '6a359ab7-a846-47fa-a4fb-d6b22bc4edc7', 'yasutori7', '3.4.10', 'sma-shop.png', null, null, '0', null, '0', '0');

-- ----------------------------
-- Table structure for sma_skrill
-- ----------------------------
DROP TABLE IF EXISTS `sma_skrill`;
CREATE TABLE `sma_skrill` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `account_email` varchar(255) NOT NULL DEFAULT 'testaccount2@moneybookers.com',
  `secret_word` varchar(20) NOT NULL DEFAULT 'mbtest',
  `skrill_currency` varchar(3) NOT NULL DEFAULT 'USD',
  `fixed_charges` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `extra_charges_my` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `extra_charges_other` decimal(25,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_skrill
-- ----------------------------
INSERT INTO `sma_skrill` VALUES ('1', '1', 'testaccount2@moneybookers.com', 'mbtest', 'USD', '0.0000', '0.0000', '0.0000');

-- ----------------------------
-- Table structure for sma_sms_settings
-- ----------------------------
DROP TABLE IF EXISTS `sma_sms_settings`;
CREATE TABLE `sma_sms_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auto_send` tinyint(1) DEFAULT NULL,
  `config` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_sms_settings
-- ----------------------------
INSERT INTO `sma_sms_settings` VALUES ('1', null, '{\"gateway\":\"Log\",\"Log\":{}');

-- ----------------------------
-- Table structure for sma_stock_counts
-- ----------------------------
DROP TABLE IF EXISTS `sma_stock_counts`;
CREATE TABLE `sma_stock_counts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `type` varchar(10) NOT NULL,
  `initial_file` varchar(50) NOT NULL,
  `final_file` varchar(50) DEFAULT NULL,
  `brands` varchar(50) DEFAULT NULL,
  `brand_names` varchar(100) DEFAULT NULL,
  `categories` varchar(50) DEFAULT NULL,
  `category_names` varchar(100) DEFAULT NULL,
  `note` text,
  `products` int(11) DEFAULT NULL,
  `rows` int(11) DEFAULT NULL,
  `differences` int(11) DEFAULT NULL,
  `matches` int(11) DEFAULT NULL,
  `missing` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `finalized` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `warehouse_id` (`warehouse_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_stock_counts
-- ----------------------------

-- ----------------------------
-- Table structure for sma_stock_count_items
-- ----------------------------
DROP TABLE IF EXISTS `sma_stock_count_items`;
CREATE TABLE `sma_stock_count_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_count_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_variant` varchar(55) DEFAULT NULL,
  `product_variant_id` int(11) DEFAULT NULL,
  `expected` decimal(15,4) NOT NULL,
  `counted` decimal(15,4) NOT NULL,
  `cost` decimal(25,4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stock_count_id` (`stock_count_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_stock_count_items
-- ----------------------------

-- ----------------------------
-- Table structure for sma_suspended_bills
-- ----------------------------
DROP TABLE IF EXISTS `sma_suspended_bills`;
CREATE TABLE `sma_suspended_bills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) DEFAULT NULL,
  `count` int(11) NOT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `biller_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `suspend_note` varchar(255) DEFAULT NULL,
  `shipping` decimal(15,4) DEFAULT '0.0000',
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_suspended_bills
-- ----------------------------

-- ----------------------------
-- Table structure for sma_suspended_items
-- ----------------------------
DROP TABLE IF EXISTS `sma_suspended_items`;
CREATE TABLE `sma_suspended_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `suspend_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `gst` varchar(20) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_suspended_items
-- ----------------------------

-- ----------------------------
-- Table structure for sma_tax_rates
-- ----------------------------
DROP TABLE IF EXISTS `sma_tax_rates`;
CREATE TABLE `sma_tax_rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) NOT NULL,
  `code` varchar(10) DEFAULT NULL,
  `rate` decimal(12,4) NOT NULL,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_tax_rates
-- ----------------------------
INSERT INTO `sma_tax_rates` VALUES ('1', 'No Tax', 'NT', '0.0000', '2');
INSERT INTO `sma_tax_rates` VALUES ('2', 'VAT @10%', 'VAT10', '10.0000', '1');
INSERT INTO `sma_tax_rates` VALUES ('3', 'GST @6%', 'GST', '6.0000', '1');
INSERT INTO `sma_tax_rates` VALUES ('4', 'VAT @20%', 'VT20', '20.0000', '1');

-- ----------------------------
-- Table structure for sma_transfers
-- ----------------------------
DROP TABLE IF EXISTS `sma_transfers`;
CREATE TABLE `sma_transfers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transfer_no` varchar(55) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `from_warehouse_id` int(11) NOT NULL,
  `from_warehouse_code` varchar(55) NOT NULL,
  `from_warehouse_name` varchar(55) NOT NULL,
  `to_warehouse_id` int(11) NOT NULL,
  `to_warehouse_code` varchar(55) NOT NULL,
  `to_warehouse_name` varchar(55) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT NULL,
  `grand_total` decimal(25,4) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `status` varchar(55) NOT NULL DEFAULT 'pending',
  `shipping` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `attachment` varchar(55) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_transfers
-- ----------------------------

-- ----------------------------
-- Table structure for sma_transfer_items
-- ----------------------------
DROP TABLE IF EXISTS `sma_transfer_items`;
CREATE TABLE `sma_transfer_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transfer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) DEFAULT NULL,
  `quantity_balance` decimal(15,4) NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `gst` varchar(20) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transfer_id` (`transfer_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_transfer_items
-- ----------------------------

-- ----------------------------
-- Table structure for sma_units
-- ----------------------------
DROP TABLE IF EXISTS `sma_units`;
CREATE TABLE `sma_units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `name` varchar(55) NOT NULL,
  `base_unit` int(11) DEFAULT NULL,
  `operator` varchar(1) DEFAULT NULL,
  `unit_value` varchar(55) DEFAULT NULL,
  `operation_value` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `base_unit` (`base_unit`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_units
-- ----------------------------
INSERT INTO `sma_units` VALUES ('1', '001', 'Unit', null, null, null, null);

-- ----------------------------
-- Table structure for sma_users
-- ----------------------------
DROP TABLE IF EXISTS `sma_users`;
CREATE TABLE `sma_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `last_ip_address` varbinary(45) DEFAULT NULL,
  `ip_address` varbinary(45) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `avatar` varchar(55) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `warehouse_id` int(10) unsigned DEFAULT NULL,
  `biller_id` int(10) unsigned DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `show_cost` tinyint(1) DEFAULT '0',
  `show_price` tinyint(1) DEFAULT '0',
  `award_points` int(11) DEFAULT '0',
  `view_right` tinyint(1) NOT NULL DEFAULT '0',
  `edit_right` tinyint(1) NOT NULL DEFAULT '0',
  `allow_discount` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`,`warehouse_id`,`biller_id`),
  KEY `group_id_2` (`group_id`,`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_users
-- ----------------------------
INSERT INTO `sma_users` VALUES ('1', 0x31302E3230312E302E31, 0x0000, 'owner', '2c8ab736b2ccab4f50e72d5fd7d21020cbb77ae7', null, 'owner@tecdiary.com', null, null, null, null, '1351661704', '1547604909', '1', 'Owner', 'Owner', 'Stock Manager', '012345678', null, 'male', '1', null, null, null, '0', '0', '0', '0', '0', '0');
INSERT INTO `sma_users` VALUES ('2', null, 0x31302E3230312E3232342E3735, 'daiki', 'a3baf7d7f98d3f36d32a5ffdf1b6ca54c3ba0001', null, 'daiki@camboinfo.com', null, null, null, null, '1547191172', '1547191172', '1', 'daiki', 'terashima', 'CIJD', '0962570422', null, 'male', '5', null, null, null, '0', '0', '0', '1', '0', '0');
INSERT INTO `sma_users` VALUES ('3', null, 0x31302E3230312E3232342E3735, 'kota', 'b83b1e92a004351ad0d619277bbd2e9ad6c48b56', null, 'kota@camboinfo.com', null, null, null, null, '1547191280', '1547191280', '1', 'kota', 'kusumi', 'CIJD', '0969993388', null, 'male', '1', '0', '0', null, '0', '0', '0', '1', '0', '0');
INSERT INTO `sma_users` VALUES ('5', null, 0x3A3A31, 'chean', 'f0c56968a077a6efcade852caf3a9bedb5013005', null, 'suncheanfc@gmail.com', null, null, null, null, '1547192372', '1547192372', '1', 'sun', 'chean', 'test', '012424242', null, 'male', '5', '0', '0', null, '0', '0', '0', '1', '0', '0');
INSERT INTO `sma_users` VALUES ('6', null, 0x31302E3230312E3232342E3735, 'nanae', '9500e00b3e259d0fe35d19c436ede7b6e648d40c', null, 'nanae@gmail.com', null, null, null, null, '1547192723', '1547192723', '1', 'nanae', 'shinohara', 'Tokyo', '0968886688', null, 'female', '1', '0', '0', null, '0', '0', '0', '1', '0', '0');
INSERT INTO `sma_users` VALUES ('7', 0x31302E3230312E3232342E3735, 0x31302E3230312E3232342E3735, 'taiyo', 'a462f4ed9a714f7082619138192bf7fac4a0dc7a', null, 'taiyo@gmail.com', null, null, null, null, '1547192793', '1547203457', '1', 'Taiyo', 'Oriuchi', 'Osaka ', '0969696969', null, 'male', '1', '0', '0', null, '0', '0', '0', '1', '0', '0');

-- ----------------------------
-- Table structure for sma_user_logins
-- ----------------------------
DROP TABLE IF EXISTS `sma_user_logins`;
CREATE TABLE `sma_user_logins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_user_logins
-- ----------------------------
INSERT INTO `sma_user_logins` VALUES ('1', '1', null, 0x3A3A31, 'owner', '2019-01-06 11:01:56');
INSERT INTO `sma_user_logins` VALUES ('2', '1', null, 0x3A3A31, 'owner', '2019-01-10 09:15:55');
INSERT INTO `sma_user_logins` VALUES ('3', '1', null, 0x3A3A31, 'owner', '2019-01-10 12:59:16');
INSERT INTO `sma_user_logins` VALUES ('4', '1', null, 0x3A3A31, 'owner', '2019-01-10 13:13:35');
INSERT INTO `sma_user_logins` VALUES ('5', '1', null, 0x3A3A31, 'owner', '2019-01-10 13:49:25');
INSERT INTO `sma_user_logins` VALUES ('6', '1', null, 0x3A3A31, 'owner', '2019-01-10 14:02:12');
INSERT INTO `sma_user_logins` VALUES ('7', '1', null, 0x31302E3230312E3232342E3739, 'owner', '2019-01-10 15:16:22');
INSERT INTO `sma_user_logins` VALUES ('8', '1', null, 0x31302E3230312E3232342E3834, 'owner', '2019-01-10 15:22:19');
INSERT INTO `sma_user_logins` VALUES ('9', '1', null, 0x3A3A31, 'owner', '2019-01-11 09:20:01');
INSERT INTO `sma_user_logins` VALUES ('10', '1', null, 0x3A3A31, 'owner', '2019-01-11 11:17:04');
INSERT INTO `sma_user_logins` VALUES ('11', '1', null, 0x3A3A31, 'owner', '2019-01-11 14:09:29');
INSERT INTO `sma_user_logins` VALUES ('12', '1', null, 0x31302E3230312E3232342E3735, 'owner', '2019-01-11 14:14:07');
INSERT INTO `sma_user_logins` VALUES ('13', '7', null, 0x31302E3230312E3232342E3735, 'taiyo', '2019-01-11 17:44:17');
INSERT INTO `sma_user_logins` VALUES ('14', '1', null, 0x3A3A31, 'owner', '2019-01-14 09:18:18');
INSERT INTO `sma_user_logins` VALUES ('15', '1', null, 0x3A3A31, 'owner', '2019-01-14 14:05:29');
INSERT INTO `sma_user_logins` VALUES ('16', '1', null, 0x3A3A31, 'owner', '2019-01-15 09:05:57');
INSERT INTO `sma_user_logins` VALUES ('17', '1', null, 0x3A3A31, 'owner', '2019-01-16 09:00:56');
INSERT INTO `sma_user_logins` VALUES ('18', '1', null, 0x31302E3230312E302E31, 'owner', '2019-01-16 09:15:09');

-- ----------------------------
-- Table structure for sma_variants
-- ----------------------------
DROP TABLE IF EXISTS `sma_variants`;
CREATE TABLE `sma_variants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_variants
-- ----------------------------

-- ----------------------------
-- Table structure for sma_warehouses
-- ----------------------------
DROP TABLE IF EXISTS `sma_warehouses`;
CREATE TABLE `sma_warehouses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `map` varchar(255) DEFAULT NULL,
  `phone` varchar(55) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `price_group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_warehouses
-- ----------------------------
INSERT INTO `sma_warehouses` VALUES ('1', 'WHI', 'Warehouse 1', '<p>Address, City</p>', null, '012345678', 'whi@tecdiary.com', null);
INSERT INTO `sma_warehouses` VALUES ('2', 'WHII', 'Warehouse 2', '<p>Warehouse 2, Jalan Sultan Ismail, 54000, Kuala Lumpur</p>', null, '0105292122', 'whii@tecdiary.com', null);

-- ----------------------------
-- Table structure for sma_warehouses_products
-- ----------------------------
DROP TABLE IF EXISTS `sma_warehouses_products`;
CREATE TABLE `sma_warehouses_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `rack` varchar(55) DEFAULT NULL,
  `avg_cost` decimal(25,4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `warehouse_id` (`warehouse_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_warehouses_products
-- ----------------------------
INSERT INTO `sma_warehouses_products` VALUES ('1', '1', '1', '-51.0000', null, '0.0000');
INSERT INTO `sma_warehouses_products` VALUES ('2', '1', '2', '0.0000', null, '0.0000');
INSERT INTO `sma_warehouses_products` VALUES ('3', '2', '1', '-10.0000', null, '10.0000');
INSERT INTO `sma_warehouses_products` VALUES ('4', '2', '2', '0.0000', null, '10.0000');
INSERT INTO `sma_warehouses_products` VALUES ('5', '3', '1', '0.0000', null, '5.0000');
INSERT INTO `sma_warehouses_products` VALUES ('6', '3', '2', '0.0000', null, '5.0000');
INSERT INTO `sma_warehouses_products` VALUES ('7', '4', '1', '0.0000', null, '50.0000');
INSERT INTO `sma_warehouses_products` VALUES ('8', '4', '2', '0.0000', null, '50.0000');
INSERT INTO `sma_warehouses_products` VALUES ('9', '5', '1', '-11.0000', null, '10.0000');
INSERT INTO `sma_warehouses_products` VALUES ('10', '5', '2', '0.0000', null, '10.0000');
INSERT INTO `sma_warehouses_products` VALUES ('11', '6', '1', '-11.0000', null, '10.0000');
INSERT INTO `sma_warehouses_products` VALUES ('12', '6', '2', '0.0000', null, '10.0000');
INSERT INTO `sma_warehouses_products` VALUES ('13', '7', '1', '-21.0000', null, '10.0000');
INSERT INTO `sma_warehouses_products` VALUES ('14', '7', '2', '0.0000', null, '10.0000');

-- ----------------------------
-- Table structure for sma_warehouses_products_variants
-- ----------------------------
DROP TABLE IF EXISTS `sma_warehouses_products_variants`;
CREATE TABLE `sma_warehouses_products_variants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `rack` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `option_id` (`option_id`),
  KEY `product_id` (`product_id`),
  KEY `warehouse_id` (`warehouse_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_warehouses_products_variants
-- ----------------------------

-- ----------------------------
-- Table structure for sma_wishlist
-- ----------------------------
DROP TABLE IF EXISTS `sma_wishlist`;
CREATE TABLE `sma_wishlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sma_wishlist
-- ----------------------------
