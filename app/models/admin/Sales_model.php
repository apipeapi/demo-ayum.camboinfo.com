<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getProductNames($term, $warehouse_id, $limit = 20)
    {
        $wp = "( SELECT product_id, warehouse_id, quantity as quantity from {$this->db->dbprefix('warehouses_products')} ) FWP";

        $this->db->select('products.*, FWP.quantity as quantity, categories.id as category_id, categories.name as category_name', FALSE)
            ->join($wp, 'FWP.product_id=products.id', 'left')
            // ->join('warehouses_products FWP', 'FWP.product_id=products.id', 'left')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->group_by('products.id')->get();

        if ($this->Settings->overselling) {
            $this->db->where("({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
        } else {
            $this->db->where("(products.track_quantity = 0 OR FWP.quantity > 0) AND FWP.warehouse_id = '" . $warehouse_id . "' AND "
                . "({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
        }
        // $this->db->order_by('products.name ASC');
        $this->db->limit($limit);
        $q = $this->db->get('products');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getProductComboItems($pid, $warehouse_id = NULL)
    {
        $this->db->select('products.id as id, combo_items.item_code as code, combo_items.quantity as qty, products.name as name,products.type as type, warehouses_products.quantity as quantity')
            ->join('products', 'products.code=combo_items.item_code', 'left')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->group_by('combo_items.id')->get();
        if($warehouse_id) {
            $this->db->where('warehouses_products.warehouse_id', $warehouse_id);
        }
        $q = $this->db->get_where('combo_items', array('combo_items.product_id' => $pid));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function getProductByCode($code)
    {
        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function syncQuantity($sale_id)
    {
        if ($sale_items = $this->getAllInvoiceItems($sale_id)) {
            foreach ($sale_items as $item) {
                $this->site->syncProductQty($item->product_id, $item->warehouse_id);
                if (isset($item->option_id) && !empty($item->option_id)) {
                    $this->site->syncVariantQty($item->option_id, $item->warehouse_id);
                }
            }
        }
    }

    public function getProductQuantity($product_id, $warehouse)
    {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row_array(); //$q->row();
        }
        return FALSE;
    }

    public function getProductOptions($product_id, $warehouse_id, $all = NULL)
    {
        $wpv = "( SELECT option_id, warehouse_id, quantity from {$this->db->dbprefix('warehouses_products_variants')} WHERE product_id = {$product_id}) FWPV";
        $this->db->select('product_variants.id as id, product_variants.name as name, product_variants.price as price, product_variants.quantity as total_quantity, FWPV.quantity as quantity', FALSE)
            ->join($wpv, 'FWPV.option_id=product_variants.id', 'left')
            //->join('warehouses', 'warehouses.id=product_variants.warehouse_id', 'left')
            ->where('product_variants.product_id', $product_id)
            ->group_by('product_variants.id')->get();

        if (! $this->Settings->overselling && ! $all) {
            $this->db->where('FWPV.warehouse_id', $warehouse_id);
            $this->db->where('FWPV.quantity >', 0);
        }
        $q = $this->db->get('product_variants');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductVariants($product_id)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getItemByID($id)
    {

        $q = $this->db->get_where('sale_items', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getAllInvoiceItems($sale_id, $return_id = NULL)
    {
        $this->db->select('sale_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, products.image, products.details as details, product_variants.name as variant, products.hsn_code as hsn_code, products.second_name as second_name')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left')
            ->group_by('sale_items.id')
            ->order_by('id', 'asc')->get();

        // if ($sale_id && !$return_id) {
        if ($sale_id) {
            $this->db->where('sale_id', $sale_id);
        } elseif ($return_id) {
            $this->db->where('sale_id', $return_id);
        }
        $q = $this->db->get('sale_items');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllInvoiceItemsWithDetails($sale_id)
    {
        $this->db->select('sale_items.*, products.details, product_variants.name as variant');
        $this->db->join('products', 'products.id=sale_items.product_id', 'left')
        ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
        ->group_by('sale_items.id')->get();
        $this->db->order_by('id', 'asc');
        $q = $this->db->get_where('sale_items', array('sale_id' => $sale_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getInvoiceByID($id)
    {
        $q = $this->db->get_where('sales', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getReturnByID($id)
    {
        $q = $this->db->get_where('sales', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getReturnBySID($sale_id)
    {
        $q = $this->db->get_where('sales', array('sale_id' => $sale_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductOptionByID($id)
    {
        $q = $this->db->get_where('product_variants', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateOptionQuantity($option_id, $quantity)
    {
        if ($option = $this->getProductOptionByID($option_id)) {
            $nq = $option->quantity - $quantity;
            if ($this->db->update('product_variants', array('quantity' => $nq), array('id' => $option_id))) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function addOptionQuantity($option_id, $quantity)
    {
        if ($option = $this->getProductOptionByID($option_id)) {
            $nq = $option->quantity + $quantity;
            if ($this->db->update('product_variants', array('quantity' => $nq), array('id' => $option_id))) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function getProductWarehouseOptionQty($option_id, $warehouse_id)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateProductOptionQuantity($option_id, $warehouse_id, $quantity, $product_id)
    {
        if ($option = $this->getProductWarehouseOptionQty($option_id, $warehouse_id)) {
            $nq = $option->quantity - $quantity;
            if ($this->db->update('warehouses_products_variants', array('quantity' => $nq), array('option_id' => $option_id, 'warehouse_id' => $warehouse_id))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        } else {
            $nq = 0 - $quantity;
            if ($this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $nq))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        }
        return FALSE;
    }

    public function addSale($data = array(), $items = array(), $payment = array(), $si_return = array())
    {
        $mode = $this->input->post('mode');
        if (empty($si_return)) {
            $cost = $this->site->costing($items);
            // $this->sma->print_arrays($cost);
        }
        $is_vat = isset($data['is_vat']) ? $data['is_vat'] : false;
        unset($data['is_vat']);
        if ($mode == 'sample') {
            $data['reference_no'] = '';
            $data['total'] = 0;
            $data['grand_total'] = 0;
            $data['sale_status'] = 'completed';
        }

        if ($this->db->insert('sales', $data)) {
          
            $sale_id = $this->db->insert_id();        


            foreach ($items as $item) {
                if ($mode == 'sample') {
                    $item['net_unit_price'] = 0;
                    $item['unit_price'] = 0;
                    $item['real_unit_price'] = 0;
                    $item['subtotal'] = 0;
                }

                $item['sale_id'] = $sale_id;
                $this->db->insert('sale_items', $item);
                $sale_item_id = $this->db->insert_id();
                if ($data['sale_status'] == 'completed' && empty($si_return)) {

                    $item_costs = $this->site->item_costing($item);
                    foreach ($item_costs as $item_cost) {
                        if (isset($item_cost['date']) || isset($item_cost['pi_overselling'])) {
                            $item_cost['sale_item_id'] = $sale_item_id;
                            $item_cost['sale_id'] = $sale_id;
                            $item_cost['date'] = date('Y-m-d', strtotime($data['date']));
                            if(! isset($item_cost['pi_overselling'])) {
                                $this->db->insert('costing', $item_cost);
                            }
                        } else {
                            foreach ($item_cost as $ic) {
                                $ic['sale_item_id'] = $sale_item_id;
                                $ic['sale_id'] = $sale_id;
                                $ic['date'] = date('Y-m-d', strtotime($data['date']));
                                if(! isset($ic['pi_overselling'])) {
                                    $this->db->insert('costing', $ic);
                                }
                            }
                        }
                    }

                }
            }

            if ($data['sale_status'] == 'completed') {
                //$this->site->syncPurchaseItems($cost);
            }

            if (!empty($si_return)) {
                foreach ($si_return as $return_item) {
                    $product = $this->site->getProductByID($return_item['product_id']);
                    if ($product->type == 'combo') {
                        $combo_items = $this->site->getProductComboItems($return_item['product_id'], $return_item['warehouse_id']);
                        foreach ($combo_items as $combo_item) {
                            $this->UpdateCostingAndPurchaseItem($return_item, $combo_item->id, ($return_item['quantity']*$combo_item->qty));
                        }
                    } else {
                        $this->UpdateCostingAndPurchaseItem($return_item, $return_item['product_id'], $return_item['quantity']);
                    }
                }
                $this->db->update('sales', array('return_sale_ref' => $data['return_sale_ref'], 'surcharge' => $data['surcharge'],'return_sale_total' => $data['grand_total'], 'return_id' => $sale_id), array('id' => $data['sale_id']));
            }

            if ($data['payment_status'] == 'partial' || $data['payment_status'] == 'paid' || $data['payment_status'] == 'AR' && !empty($payment)) {
                if (empty($payment['reference_no'])) {
                    $payment['reference_no'] = $this->site->getReference('pay');
                }
                $payment['sale_id'] = $sale_id;
                if ($payment['paid_by'] == 'gift_card') {
                    $this->db->update('gift_cards', array('balance' => $payment['gc_balance']), array('card_no' => $payment['cc_no']));
                    unset($payment['gc_balance']);
                    $this->db->insert('payments', $payment);
                } else {
                    if ($payment['paid_by'] == 'deposit') {
                        $customer = $this->site->getCompanyByID($data['customer_id']);
                        $this->db->update('companies', array('deposit_amount' => ($customer->deposit_amount-$payment['amount'])), array('id' => $customer->id));
                    }
                    $this->db->insert('payments', $payment);
                }
                if ($this->site->getReference('pay') == $payment['reference_no']) {
                    $this->site->updateReference('pay');
                }
                //$this->site->syncSalePayments($sale_id);
            }

            //$this->site->syncQuantity($sale_id);
            $this->sma->update_award_points($data['grand_total'], $data['customer_id'], $data['created_by']);

            if ($mode == 'sample') {
                $config_data_3 = array(
                    'sale_id' => $sale_id,
                );
                $this->api_reduce_stock_from_sale($config_data_3);
            }

            $config_data = array(
                'id' => $sale_id,
            );            
            $this->site->api_calculate_sale_supplier($config_data);   
                        
            $config_data = array(
                'type' => 'sale',
                'date' => $data['date'],
                'customer_id' => $data['customer_id'],
                'update' => 1,
            );
            $temp = $this->site->api_calculate_reference_no($config_data);

            $temp = array(
                'reference_no' => $temp['reference_no'],
                'sale_status' => 'preparing',
            );
            $this->db->update('sma_sales', $temp,"id = ".$sale_id);            


            return $sale_id;
        }

        return false;
    }

    public function updateSale($id, $data, $items = array())
    {
        $mode = $this->input->post('mode');        
        //$this->resetSaleActions($id, FALSE, TRUE);

        $config_data = array(
            'table_name' => 'sma_sales',
            'select_table' => 'sma_sales',
            'select_condition' => "id = ".$id,
        );
        $select_data = $this->site->api_select_data_v2($config_data);

        $temp_created_by = array();
        if ($select_data[0]['created_by'] > 0) {
            $temp_created_by = $this->site->api_select_some_fields_with_where("
                *
                "
                ,"sma_users"
                ,"id = ".$select_data[0]['created_by']
                ,"arr"
            );
        }        
        if ($select_data[0]['sale_status'] == 'delivering' || $select_data[0]['sale_status'] == 'completed') {
            //$this->Settings->overselling = true;
            //$cost = $this->site->costing($items, true);
            if ($select_data[0]['cs_reference_no'] == '') {
                $config_data_2 = array(
                    'id' => $id,
                    'warehouse_id' => $select_data[0]['warehouse_id'],
                    'type' => 'addition',
                );
                $this->api_update_stock_sale($config_data_2);

                if ($select_data[0]['sample'] != 1 && $temp_created_by[0]['group_id'] == 3)
                    $this->api_reverse_consignment_purchase($id);
            }
        }

        $is_vat = isset($data['is_vat']) ? $data['is_vat'] : false;
        unset($data['is_vat']);

        if ($select_data[0]['sample'] == 1) {
            $data['reference_no'] = '';
            $data['total'] = 0;
            $data['grand_total'] = 0;
        }        
        if ($this->db->update('sales', $data, array('id' => $id)) &&
            $this->db->delete('sale_items', array('sale_id' => $id)) &&
            $this->db->delete('costing', array('sale_id' => $id))) {

            $sale_id = $this->db->insert_id();

            foreach ($items as $item) {
                if ($select_data[0]['sample'] == 1) {
                    $item['net_unit_price'] = 0;
                    $item['unit_price'] = 0;
                    $item['real_unit_price'] = 0;
                    $item['subtotal'] = 0;
                }
                $item['sale_id'] = $id;
                $this->db->insert('sale_items', $item);
                $sale_item_id = $this->db->insert_id();
                if ($data['sale_status'] == 'completed' && $this->site->getProductByID($item['product_id'])) {
                    $item_costs = $this->site->item_costing($item);
                    foreach ($item_costs as $item_cost) {
                        if (isset($item_cost['date']) || isset($item_cost['pi_overselling'])) {
                            $item_cost['sale_item_id'] = $sale_item_id;
                            $item_cost['sale_id'] = $id;
                            $item_cost['date'] = date('Y-m-d', strtotime($data['date']));
                            if(! isset($item_cost['pi_overselling'])) {
                                $this->db->insert('costing', $item_cost);
                            }
                        } else {
                            foreach ($item_cost as $ic) {
                                $ic['sale_item_id'] = $sale_item_id;
                                $ic['sale_id'] = $id;
                                $item_cost['date'] = date('Y-m-d', strtotime($data['date']));
                                if(! isset($ic['pi_overselling'])) {
                                    $this->db->insert('costing', $ic);
                                }
                            }
                        }
                    }
                }

            }

            //$this->site->syncSalePayments($id);
            //$this->site->syncQuantity($id);
            $this->sma->update_award_points($data['grand_total'], $data['customer_id'], $select_data[0]['created_by']);
            $return = true;
        }
        else
            $return = false;

        if ($select_data[0]['sale_status'] == 'delivering' || $select_data[0]['sale_status'] == 'completed') {
            if ($select_data[0]['cs_reference_no'] == '') {
                $config_data_2 = array(
                    'id' => $id,
                    'warehouse_id' => $data['warehouse_id'],
                    'type' => 'subtraction',
                );
                $this->api_update_stock_sale($config_data_2);
            
                if ($select_data[0]['sample'] != 1 && $temp_created_by[0]['group_id'] == 3)
                    $this->api_calculate_consignment_purchase_wrapper($id);
            }
        }

        $config_data = array(
            'id' => $id,
        );            
        $this->site->api_calculate_sale_supplier($config_data); 

        return $return;
    }

    public function api_update_stock_sale($config_data) {
        if ($config_data['warehouse_id'] <= 0) $config_data['warehouse_id'] = $this->Settings->default_warehouse;
        $temp = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_sale_items"
            ,"sale_id = ".$config_data['id']." and warehouse_id = ".$config_data['warehouse_id']
            ,"arr"
        );
        if (is_array($temp)) if (count($temp) > 0)
        for ($i=0;$i<count($temp);$i++) {

            if ($temp[$i]['option_id'] > 0)
                $condition = " and option_id = ".$temp[$i]['option_id'];
            else
                $condition = '';
            $temp2 = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_consignment_sale_items"
                ,"sale_id = ".$config_data['id']." and product_id = ".$temp[$i]['product_id']." ".$condition
                ,"arr"
            );
            $temp3 = 0;
            for ($i2=0;$i2<count($temp2);$i2++) {
                $temp3 = $temp3 + $temp2[$i2]['quantity'];
            }
            $temp[$i]['quantity'] = $temp[$i]['quantity'] - $temp3;

            $config_data_2 = array(
                'product_id' => $temp[$i]['product_id'],
                'type' => $config_data['type'],
                'quantity' => $temp[$i]['quantity'],
                'warehouse_id' => $config_data['warehouse_id'],
                'option_id' => $temp[$i]['option_id'],
            );
            $this->site->api_update_quantity_stock($config_data_2);
        }
    }

    public function updateStatus($id, $status, $note)
    {

        $sale = $this->getInvoiceByID($id);
        $items = $this->getAllInvoiceItems($id);
        $cost = array();
        if ($status == 'completed' && $sale->sale_status != 'completed') {
            foreach ($items as $item) {
                $items_array[] = (array) $item;
            }
            $cost = $this->site->costing($items_array);
        }
        if ($status != 'completed' && $sale->sale_status == 'completed') {
            //$this->resetSaleActions($id);
        }

        if ($this->db->update('sales', array('sale_status' => $status, 'note' => $note), array('id' => $id)) && $this->db->delete('costing', array('sale_id' => $id))) {
            if ($status == 'completed' && $sale->sale_status != 'completed') {
                foreach ($items as $item) {
                    $item = (array) $item;
                    if ($this->site->getProductByID($item['product_id'])) {
                        $item_costs = $this->site->item_costing($item);
                        foreach ($item_costs as $item_cost) {
                            $item_cost['sale_item_id'] = $item['id'];
                            $item_cost['sale_id'] = $id;
                            $item_cost['date'] = date('Y-m-d', strtotime($sale->date));
                            if(! isset($item_cost['pi_overselling'])) {
                                $this->db->insert('costing', $item_cost);
                            }
                        }
                    }
                }
            }

            if (!empty($cost)) { $this->site->syncPurchaseItems($cost); }
            $this->site->syncQuantity($id);
            return true;
        }
        return false;
    }
    public function api_delete($id) {
        $return = array();

        $config_data = array(
            'table_name' => 'sma_sales',
            'select_table' => 'sma_sales',
            'select_condition' => "id = ".$id,
        );
        $select_data = $this->site->api_select_data_v2($config_data);

        if (is_int(strpos($select_data[0]['reference_no'],"SL"))) {
            $return['error'] = lang('Cannot_delete_any_sale_with_SL_Reference_No.');
            return $return;            
        }

        $temp_return = $this->site->api_select_some_fields_with_where("id
            "
            ,"sma_returns"
            ,"add_ons like '%:sale_id:{".$id."}:%'"
            ,"arr"
        );
        if (is_array($temp_return)) if (count($temp_return) > 0) {
            $return['error'] = lang('Can_not_delete_any_sale_with_a_return_record.');
            return $return;
        }

        if ($select_data[0]['reference_no'] != '') {
            $config_data = array(
                'table_name' => 'sma_sales',
                'select_table' => 'sma_sales',
                'select_condition' => "add_ons like '%:do_sl_reference_no:{".$select_data[0]['reference_no']."}:%'",
            );
            $temp = $this->site->api_select_data_v2($config_data);
            if (is_array($temp)) if (count($temp) > 0) {
                $return['error'] = lang('Can_not_delete_any_sale_with_a_DO_sale_record').' ('.$id.')';
                return $return;
            }
        }

        if ($select_data[0]['sale_status'] == 'delivering' || $select_data[0]['sale_status'] == 'completed') {
            $config_data_2 = array(
                'id' => $id,
                'warehouse_id' => $select_data[0]['warehouse_id'],
                'type' => 'addition',
            );
            $this->api_update_stock_sale($config_data_2);

            $temp_created_by = array();
            if ($select_data[0]['created_by'] > 0) {
                $temp_created_by = $this->site->api_select_some_fields_with_where("
                    *
                    "
                    ,"sma_users"
                    ,"id = ".$select_data[0]['created_by']
                    ,"arr"
                );
            }
            if ($select_data[0]['sample'] != 1 && $temp_created_by[0]['group_id'] == 3)
                $this->api_reverse_consignment_purchase($id);
        }
            
        $this->db->delete('sale_items', array('sale_id' => $id));
        $this->db->delete('sales', array('id' => $id));
        $this->db->delete('payments', array('sale_id' => $id));
        $this->db->delete('costing', array('sale_id' => $id));
        $this->db->delete('consignment_purchase_items', array('sale_id' => $id));

        $config_data4 = array(
            'date' => $select_data[0]['date']
        );
        $this->site->api_calculate_sl_reference_no_by_date($config_data4);

        $temp = $this->site->api_get_consignment_sale_items($id);
        if (count($temp) > 0) {

            $temp = $this->site->api_select_some_fields_with_where("
                *
                "
                ,"sma_consignment_sale_items"
                ,"sale_id = ".$id
                ,"arr"
            );
            for ($i=0;$i<count($temp);$i++) {
                $temp5 = $this->site->api_select_some_fields_with_where("
                    *
                    "
                    ,"sma_consignment"
                    ,"id = ".$temp[$i]['consignment_id']
                    ,"arr"
                );
                $temp6 = array(
                    'sale_status' => 'pending'
                );
                $this->db->update('sma_consignment', $temp6,'id = '.$temp5[0]['id']);                  
            }
            $this->db->delete('consignment_sale_items', array('sale_id' => $id));
        }

        return $return;
    }


    public function resetSaleActions($id, $return_id = NULL, $check_return = NULL)
    {
        if ($sale = $this->getInvoiceByID($id)) {
            if ($check_return && $sale->sale_status == 'returned') {
                $this->session->set_flashdata('warning', lang('sale_x_action'));
                redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
            }

            if ($sale->sale_status == 'completed') {
                if ($costings = $this->getSaleCosting($id)) {
                    foreach ($costings as $costing) {
                        if ($pi = $this->getPurchaseItemByID($costing->purchase_item_id)) {
                            $this->site->setPurchaseItem(['id' => $pi->id, 'product_id' => $pi->product_id, 'option_id' => $pi->option_id], $costing->quantity);
                        } else {
                            // $sale_item = $this->getSaleItemByID($costing->sale_item_id);
                            $pi = $this->site->getPurchasedItem(['product_id' => $costing->product_id, 'option_id' => $costing->option_id ? $costing->option_id : NULL, 'purchase_id' => NULL, 'transfer_id' => NULL, 'warehouse_id' => $sale->warehouse_id]);
                            $this->site->setPurchaseItem(['id' => $pi->id, 'product_id' => $pi->product_id, 'option_id' => $pi->option_id], $costing->quantity);
                        }
                    }
                }
                $items = $this->getAllInvoiceItems($id);
                $this->site->syncQuantity(NULL, NULL, $items);
                $this->sma->update_award_points($sale->grand_total, $sale->customer_id, $sale->created_by, TRUE);
                return $items;
            }
        }
    }

    public function getPurchaseItemByID($id)
    {
        $q = $this->db->get_where('purchase_items', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCostingLines($sale_item_id, $product_id, $sale_id = NULL)
    {
        if ($sale_id) { $this->db->where('sale_id', $sale_id); }
        $orderby = ($this->Settings->accounting_method == 1) ? 'asc' : 'desc';
        $this->db->order_by('id', $orderby);
        $q = $this->db->get_where('costing', array('sale_item_id' => $sale_item_id, 'product_id' => $product_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSaleItemByID($id)
    {
        $q = $this->db->get_where('sale_items', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductByName($name)
    {
        $q = $this->db->get_where('products', array('name' => $name), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addDelivery($data = array())
    {
        if ($this->db->insert('deliveries', $data)) {
            if ($this->site->getReference('do') == $data['do_reference_no']) {
                $this->site->updateReference('do');
            }
            return true;
        }
        return false;
    }

    public function updateDelivery($id, $data = array())
    {
        if ($this->db->update('deliveries', $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function getDeliveryByID($id)
    {
        $q = $this->db->get_where('deliveries', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getDeliveryBySaleID($sale_id)
    {
        $q = $this->db->get_where('deliveries', array('sale_id' => $sale_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteDelivery($id)
    {
        if ($this->db->delete('deliveries', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getInvoicePayments($sale_id)
    {
        $this->db->order_by('id', 'asc');
        $q = $this->db->get_where('payments', array('sale_id' => $sale_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }
    public function getInvoicePayments_v2($id)
    {
        if ($id != '') {        
            $temp_field = $this->site->api_get_condition_add_ons_field("add_ons","sma_add_ons","table_name = 'sma_payments'");
            $temp = $this->site->api_select_some_fields_with_where("*
                ".$temp_field
                ,"sma_payments"
                ,"sale_id = ".$id." and paid_by != 'AR' order by id asc"
                ,"arr"
            );
            return $temp;            
        }
        else
            return FALSE;

    }

    public function getPaymentByID($id)
    {
        $q = $this->db->get_where('payments', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    public function getPaymentByID_v2($id)
    {
        if ($id != '') {        
            $temp_field = $this->site->api_get_condition_add_ons_field("add_ons","sma_add_ons","table_name = 'sma_payments'");
            $temp = $this->site->api_select_some_fields_with_where("*
                ".$temp_field
                ,"sma_payments"
                ,"id = ".$id
                ,"arr"
            );
            $q = $this->db->get_where('sma_payments', array('id' => $id), 1);
            $temp2 = $q->row();
            foreach ($temp[0] as $key => $value) {
                $temp2->{$key} = $value;
            }        
            return $temp2;            
        }
        else
            return FALSE;
    }

    public function getPaymentsForSale($sale_id)
    {
        $this->db->select('payments.date, payments.paid_by, payments.amount, payments.cc_no, payments.cheque_no, payments.reference_no, users.first_name, users.last_name, type')
            ->join('users', 'users.id=payments.created_by', 'left');
        $q = $this->db->get_where('payments', array('sale_id' => $sale_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function addPayment($data = array(), $customer_id = null)
    {
        $config_data = array(
            'type' => 'payment',
            'date' => $data['date'],
            'update' => 0,
        );
        $temp = $this->site->api_calculate_reference_no($config_data);
        $data['reference_no'] = $temp['reference_no'];

        if ($this->db->insert('payments', $data)) {

            //$this->site->syncSalePayments($data['sale_id']);
            if ($data['paid_by'] == 'gift_card') {
                $gc = $this->site->getGiftCardByNO($data['cc_no']);
                $this->db->update('gift_cards', array('balance' => ($gc->balance - $data['amount'])), array('card_no' => $data['cc_no']));
            } elseif ($customer_id && $data['paid_by'] == 'deposit') {
                $customer = $this->site->getCompanyByID($customer_id);
                $this->db->update('companies', array('deposit_amount' => ($customer->deposit_amount-$data['amount'])), array('id' => $customer_id));
            } 

            $config_data = array(
                'sale_id' => $data['sale_id'],
            );
            $this->site->api_calculate_sale_payment($config_data);            

            $config_data = array(
                'type' => 'payment',
                'date' => $data['date'],
                'update' => 1,
            );
            $temp = $this->site->api_calculate_reference_no($config_data);

            return true;
        }
        return false;
    }

    public function updatePayment($id, $data = array(), $customer_id = null)
    {
        
        $temp = $this->site->api_select_some_fields_with_where("
            sale_id
            "
            ,"sma_payments"
            ,"id = ".$id
            ,"arr"
        );       

        $opay = $this->getPaymentByID($id);
        if ($this->db->update('payments', $data, array('id' => $id))) {
            //$this->site->syncSalePayments($data['sale_id']);
            if ($opay->paid_by == 'gift_card') {
                $gc = $this->site->getGiftCardByNO($opay->cc_no);
                $this->db->update('gift_cards', array('balance' => ($gc->balance+$opay->amount)), array('card_no' => $opay->cc_no));
            } elseif ($opay->paid_by == 'deposit') {
                if (!$customer_id) {
                    $sale = $this->getInvoiceByID($opay->sale_id);
                    $customer_id = $sale->customer_id;
                }
                $customer = $this->site->getCompanyByID($customer_id);
                $this->db->update('companies', array('deposit_amount' => ($customer->deposit_amount+$opay->amount)), array('id' => $customer->id));
            }
            if ($data['paid_by'] == 'gift_card') {
                $gc = $this->site->getGiftCardByNO($data['cc_no']);
                $this->db->update('gift_cards', array('balance' => ($gc->balance - $data['amount'])), array('card_no' => $data['cc_no']));
            } elseif ($customer_id && $data['paid_by'] == 'deposit') {
                $customer = $this->site->getCompanyByID($customer_id);
                $this->db->update('companies', array('deposit_amount' => ($customer->deposit_amount-$data['amount'])), array('id' => $customer_id));
            }
            
            $config_data = array(
                'sale_id' => $temp[0]['sale_id'],
            );
            $this->site->api_calculate_sale_payment($config_data);

            return true;
        }
        return false;
    }

    public function deletePayment($id)
    {
        $temp = $this->site->api_select_some_fields_with_where("
            sale_id
            "
            ,"sma_payments"
            ,"id = ".$id
            ,"arr"
        );            

        $opay = $this->getPaymentByID($id);
        if ($this->db->delete('payments', array('id' => $id))) {
            //$this->site->syncSalePayments($opay->sale_id);
            if ($opay->paid_by == 'gift_card') {
                $gc = $this->site->getGiftCardByNO($opay->cc_no);
                $this->db->update('gift_cards', array('balance' => ($gc->balance+$opay->amount)), array('card_no' => $opay->cc_no));
            } elseif ($opay->paid_by == 'deposit') {
                $sale = $this->getInvoiceByID($opay->sale_id);
                $customer = $this->site->getCompanyByID($sale->customer_id);
                $this->db->update('companies', array('deposit_amount' => ($customer->deposit_amount+$opay->amount)), array('id' => $customer->id));
            }                                    

            $config_data = array(
                'sale_id' => $temp[0]['sale_id'],
            );
            $this->site->api_calculate_sale_payment($config_data);
            $temp_2 = $this->site->api_select_some_fields_with_where("
                id
                "
                ,"sma_payments"
                ,"sale_id = ".$temp[0]['sale_id']
                ,"arr"
            );
            if (count($temp_2) <= 0) {
                $config_data = array(
                    'sale_id' => $temp[0]['sale_id'],
                    'paid_by' => 'AR',
                    'amount' => 0,
                    'created_by' => $this->session->userdata('user_id'),
                );
                $this->db->insert('sma_payments', $config_data);                
            }


            return true;
        }
        return FALSE;
    }

    public function getWarehouseProductQuantity($warehouse_id, $product_id)
    {
        $q = $this->db->get_where('warehouses_products', array('warehouse_id' => $warehouse_id, 'product_id' => $product_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    /* ----------------- Gift Cards --------------------- */

    public function addGiftCard($data = array(), $ca_data = array(), $sa_data = array())
    {
        if ($this->db->insert('gift_cards', $data)) {
            if (!empty($ca_data)) {
                $this->db->update('companies', array('award_points' => $ca_data['points']), array('id' => $ca_data['customer']));
            } elseif (!empty($sa_data)) {
                $this->db->update('users', array('award_points' => $sa_data['points']), array('id' => $sa_data['user']));
            }
            return true;
        }
        return false;
    }

    public function updateGiftCard($id, $data = array())
    {
        $this->db->where('id', $id);
        if ($this->db->update('gift_cards', $data)) {
            return true;
        }
        return false;
    }

    public function deleteGiftCard($id)
    {
        if ($this->db->delete('gift_cards', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getPaypalSettings()
    {
        $q = $this->db->get_where('paypal', array('id' => 1));
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSkrillSettings()
    {
        $q = $this->db->get_where('skrill', array('id' => 1));
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getQuoteByID($id)
    {
        $q = $this->db->get_where('quotes', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllQuoteItems($quote_id)
    {
        $q = $this->db->get_where('quote_items', array('quote_id' => $quote_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getStaff()
    {
        if (!$this->Owner) {
            $this->db->where('group_id !=', 1);
        }
        $this->db->where('group_id !=', 3)->where('group_id !=', 4);
        $q = $this->db->get('users');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductVariantByName($name, $product_id)
    {
        $q = $this->db->get_where('product_variants', array('name' => $name, 'product_id' => $product_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTaxRateByName($name)
    {
        $q = $this->db->get_where('tax_rates', array('name' => $name), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function topupGiftCard($data = array(), $card_data = NULL)
    {
        if ($this->db->insert('gift_card_topups', $data)) {
            $this->db->update('gift_cards', $card_data, array('id' => $data['card_id']));
            return true;
        }
        return false;
    }

    public function getAllGCTopups($card_id)
    {
        $this->db->select("{$this->db->dbprefix('gift_card_topups')}.*, {$this->db->dbprefix('users')}.first_name, {$this->db->dbprefix('users')}.last_name, {$this->db->dbprefix('users')}.email")
        ->join('users', 'users.id=gift_card_topups.created_by', 'left')
        ->order_by('id', 'desc')->limit(10);
        $q = $this->db->get_where('gift_card_topups', array('card_id' => $card_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getItemRack($product_id, $warehouse_id)
    {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            $wh = $q->row();
            return $wh->rack;
        }
        return FALSE;
    }

    public function getSaleCosting($sale_id)
    {
        $q = $this->db->get_where('costing', array('sale_id' => $sale_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function UpdateCostingAndPurchaseItem($return_item, $product_id, $quantity)
    {
        $bln_quantity = $quantity;
        if ($costings = $this->getCostingLines($return_item['id'], $product_id)) {
            foreach ($costings as $costing) {
                if ($costing->quantity > $bln_quantity && $bln_quantity != 0) {
                    $qty = $costing->quantity - $bln_quantity;
                    $bln = $costing->quantity_balance && $costing->quantity_balance >= $bln_quantity ? $costing->quantity_balance - $bln_quantity : 0;
                    $this->db->update('costing', array('quantity' => $qty, 'quantity_balance' => $bln), array('id' => $costing->id));
                    $bln_quantity = 0;
                    break;
                } elseif ($costing->quantity <= $bln_quantity && $bln_quantity != 0) {
                    $this->db->delete('costing', array('id' => $costing->id));
                    $bln_quantity = ($bln_quantity - $costing->quantity);
                }
            }
        }
        $clause = ['product_id' => $product_id, 'warehouse_id' => $return_item['warehouse_id'], 'purchase_id' => null, 'transfer_id' => null, 'option_id' => $return_item['option_id']];
        $this->site->setPurchaseItem($clause, $quantity);
        $this->site->syncQuantity(null, null, null, $product_id);
    }
	
	/*** Bulk Actions On Sale ***/
	public function getInvoiceByIDPayCate($id)
    {
        $this->db->select('sales.*, companies.payment_category');
        $this->db->join('companies', 'companies.id=sales.customer_id', 'left');
        $q = $this->db->get_where('sales', array('sales.id' => $id));
     
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;

    }
    public function api_update_completed_payment_status($config_data) {
        $return['updated'] = 0;
        
        $temp = $this->getInvoiceByID_v2($config_data['id']);
        if ($temp->sale_status == 'delivering' || $temp->sale_status == 'completed') {
            $temp_2 = array(
                'sale_status' => 'completed',
                'payment_status'=> $config_data['payment_status'],
            );
            if ($config_data['payment_status'] == 'due') {
                $temp_2['paid'] = 0;
                $this->db->delete('sma_payments', "sale_id = ".$config_data['id']." and paid_by = 'cash'");
            }
            if ($config_data['payment_status'] == 'paid') {
                $temp_2['paid'] = $temp->grand_total;
            }

            $this->db->update('sma_sales', $temp_2,"id = ".$config_data['id']);

            $temp_riel = $this->site->api_select_some_fields_with_where("rate
                "
                ,"sma_currencies"
                ,"code = 'KHR' order by date desc limit 1"
                ,"arr"
            );
            if (count($temp_riel) > 0) {
                $config_data_2 = array(
                    'table_name' => 'sma_sales',
                    'id_name' => 'id',
                    'field_add_ons_name' => 'add_ons',
                    'selected_id' => $config_data['id'],
                    'add_ons_title' => 'kh_currency_rate',
                    'add_ons_value' => $temp_riel[0]['rate'],
                );
                $this->site->api_update_add_ons_field($config_data_2);
            }

            $return['updated'] = 1;
        }
        $return['grand_total'] = $temp->grand_total;
        return $return;
    }

    public function api_generate_consignment_purchase($config_data) {

        $temp_purchase = $this->site->api_select_some_fields_with_where("
            id     
            "
            ,"sma_purchases"
            ,"getTranslate(add_ons,'sale_id','".f_separate."','".v_separate."') = '".$config_data['sale_id']."'"
            ,"arr"
        );        

        $config_data_2 = array(
            'table_name' => 'sma_sales',
            'select_table' => 'sma_sales',
            'translate' => '',
            'select_condition' => "id = ".$config_data['sale_id'],
        );
        $temp_sale = $this->site->api_select_data_v2($config_data_2);

        if ($temp_sale[0]['id'] > 0) {
            $temp_3 = $this->site->api_select_some_fields_with_where("
                COLUMN_NAME
                "
                ,"INFORMATION_SCHEMA.COLUMNS"
                ,"table_name = 'sma_purchases'"
                ,"arr"
            );
            for ($i=0;$i<count($temp_sale);$i++) {
                $data = array();
                foreach(array_keys($temp_sale[$i]) as $key) {
                    for ($j=0;$j<count($temp_3);$j++) {
                        if ($temp_3[$j]['COLUMN_NAME'] == $key) {
                            $data[$key] = $temp_sale[$i][$key];
                            break;
                        }
                    }
                }
                unset($data['id']);
                $data['supplier_id'] = $temp_sale[0]['customer_id'];
                $data['supplier'] = $temp_sale[0]['customer'];
                $data['payment_status'] = $temp_sale[0]['payment_status'];
                $data['status'] = 'received';
                $data['due_date'] = '';                
                $data['add_ons'] = "initial_first_add_ons:{}:cs_reference_no:{".$temp_sale[0]['cs_reference_no']."}:sale_id:{".$temp_sale[0]['id']."}:company_branch:{".$temp_sale[0]['company_branch']."}:po_number:{".$temp_sale[0]['po_number']."}:";

                if ($temp_purchase[0]['id'] <= 0) {
                    $this->db->insert('sma_purchases', $data);
                    $temp_id = $this->site->api_select_some_fields_with_where("MAX(id) as max","sma_purchases","id > 0","arr");
                    $purchase_id = $temp_id[0]['max'];
                    $this->site->updateReference('po');
                }
                else {
                    $this->db->update('sma_purchases', $data, "id = ".$temp_purchase[0]['id']);
                    $purchase_id = $temp_purchase[0]['id'];
                }
            }
        }      

        $temp_items = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_sale_items"
            ,"sale_id = ".$config_data['sale_id']." order by product_id asc"
            ,"arr"
        );
        if ($temp_items[0]['id'] > 0) {
            $temp_3 = $this->site->api_select_some_fields_with_where("
                COLUMN_NAME
                "
                ,"INFORMATION_SCHEMA.COLUMNS"
                ,"table_name = 'sma_purchase_items'"
                ,"arr"
            );
            for ($i=0;$i<count($temp_items);$i++) {
                $product = array();
                foreach(array_keys($temp_items[$i]) as $key) {
                    for ($j=0;$j<count($temp_3);$j++) {
                        if ($temp_3[$j]['COLUMN_NAME'] == $key) {
                            $product[$key] = $temp_items[$i][$key];
                            break;
                        }
                    }
                }
                unset($product['id']);
                $product['purchase_id'] = $purchase_id;

                if ($temp_purchase[0]['id'] <= 0)
                    $this->db->insert('sma_purchase_items', $product);
                else
                    $this->db->update('sma_purchase_items', $product,"purchase_id = ".$purchase_id." and product_id = ".$product['product_id']);
            }
        }      
    }

    public function api_reduce_stock_from_sale($config_data) {
        $temp = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_sale_items"
            ,"sale_id = ".$config_data['sale_id']
            ,"arr"
        );
        if (is_array($temp)) if (count($temp) > 0)
        for ($i=0;$i<count($temp);$i++) {

            if ($temp[$i]['option_id'] > 0)
                $condition = " and option_id = ".$temp[$i]['option_id'];
            else
                $condition = '';
            $temp2 = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_consignment_sale_items"
                ,"sale_id = ".$config_data['sale_id']." and product_id = ".$temp[$i]['product_id']." ".$condition
                ,"arr"
            );
            $temp3 = 0;
            for ($i2=0;$i2<count($temp2);$i2++) {
                $temp3 = $temp3 + $temp2[$i2]['quantity'];
            }
            $temp[$i]['quantity'] = $temp[$i]['quantity'] - $temp3;

            $config_data_2 = array(
                'product_id' => $temp[$i]['product_id'],
                'type' => 'subtraction',
                'quantity' => $temp[$i]['quantity'],
                'warehouse_id' => $temp[$i]['warehouse_id'],
                'option_id' => $temp[$i]['option_id'],
            );
            $this->site->api_update_quantity_stock($config_data_2);
        }        
    }

	public function update_mpayment_status_to_due($id)
    {
        $pc = $this->getInvoiceByIDPayCate($id);
        $pay_cate=$pc->payment_category;
        if ($pay_cate=="weekly" || $pay_cate=="cash on delivery" || $pay_cate=="") {
			$update_payment_status=array(
				 'payment_status'=>'due',
				 'paid'=>0,
            );
            $temp = $this->getInvoiceByID($id);
            if ($temp->sale_status == 'delivering') {
                $update_payment_status['sale_status'] = 'completed';
                $temp_riel = $this->site->api_select_some_fields_with_where("rate
                    "
                    ,"sma_currencies"
                    ,"code = 'KHR'"
                    ,"arr"
                );
                $config_data = array(
                    'table_name' => 'sma_sales',
                    'id_name' => 'id',
                    'field_add_ons_name' => 'add_ons',
                    'selected_id' => $id,
                    'add_ons_title' => 'kh_currency_rate',
                    'add_ons_value' => $temp_riel[0]['rate'],
                );
                $this->site->api_update_add_ons_field($config_data);                                                
            }
            
            $this->db->where(array('id' => $id));
            $this->db->update('sales',$update_payment_status); 
        }    
    }
	
	public function update_mpayment_status_to_partial($id)
    {
        $sale = $this->getInvoiceByIDPayCate($id);
        $pay_cate=$sale->payment_category;
        if ($pay_cate=="monthly" || $pay_cate=="2 month" || $pay_cate=="") {
			$update_payment_status=array(
				'payment_status'=>'partial',
            );
            $temp = $this->getInvoiceByID($id);
            if ($temp->sale_status == 'delivering') {
                $update_payment_status['sale_status'] = 'completed';
                $temp_riel = $this->site->api_select_some_fields_with_where("rate
                    "
                    ,"sma_currencies"
                    ,"code = 'KHR'"
                    ,"arr"
                );
                $config_data = array(
                    'table_name' => 'sma_sales',
                    'id_name' => 'id',
                    'field_add_ons_name' => 'add_ons',
                    'selected_id' => $id,
                    'add_ons_title' => 'kh_currency_rate',
                    'add_ons_value' => $temp_riel[0]['rate'],
                );
                $this->site->api_update_add_ons_field($config_data);                                                
            }            
            $this->db->where(array('id' => $id));
            $this->db->update('sales',$update_payment_status); 
        }    
    }
	
	public function getSaleID($id) {
        $q = $this->db->get_where('sales', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
	
	public function update_mpayment_status_to_paid($id)
    {
        $sale = $this->getSaleID($id);
        $amount=$sale->grand_total;
        $update_payment_status=array(
			'payment_status'=>'paid',
			'paid'=>$amount,  
        );
        $temp = $this->getInvoiceByID($id);
        if ($temp->sale_status == 'delivering') {
            $update_payment_status['sale_status'] = 'completed';
            $temp_riel = $this->site->api_select_some_fields_with_where("rate
                "
                ,"sma_currencies"
                ,"code = 'KHR'"
                ,"arr"
            );
            $config_data = array(
                'table_name' => 'sma_sales',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $id,
                'add_ons_title' => 'kh_currency_rate',
                'add_ons_value' => $temp_riel[0]['rate'],
            );
            $this->site->api_update_add_ons_field($config_data);                                                
        }                    
        $this->db->where(array('id' => $id));
        $this->db->update('sales',$update_payment_status); 
    }
    
    /* custom function for pagination */
	public function get_paging_data($warehouse_id = "", $keyword = "", $config_data = "", $sort_by = "", $sort_order = "", $limit = "",$offset = "") {

        $temp_field = $this->site->api_get_condition_add_ons_field("add_ons","sma_add_ons","table_name = 'sma_sales'");        
        $sql = $this->db->select("
                    {$this->db->dbprefix('sales')}.id as id, 
                    {$this->db->dbprefix('sales')}.date as date, 
                    reference_no,                    
                    biller, 
                    {$this->db->dbprefix('sales')}.customer, 
                    {$this->db->dbprefix('sales')}.customer_id, 
                    sale_status, 
                    grand_total, 
                    paid, 
                    (grand_total-paid) as balance, 
                    payment_status, 
                    {$this->db->dbprefix('sales')}.attachment, 
                    return_id
                    ".$temp_field
                )
                //->join("companies","companies.id = sales.customer_id",'left')
                ->where('pos !=', 1); // ->where('sale_status !=', 'returned');

        if ($this->input->get('shop') == 'yes') {
            $this->db->where('shop', 1);

        } elseif ($this->input->get('shop') == 'no') {
            $this->db->where('shop !=', 1);
        }

        if ($this->input->get('delivery') == 'no') {
            $this->db->join('deliveries', 'deliveries.sale_id=sales.id', 'left')
                ->where('sales.sale_status', 'completed')
                ->where('sales.payment_status', 'paid')
                ->where("({$this->db->dbprefix('deliveries')}.status != 'delivered' OR 
                        {$this->db->dbprefix('deliveries')}.status IS NULL)", NULL);
        }

        if ($this->input->get('attachment') == 'yes') {
            $this->db->where('payment_status !=', 'paid')->where('attachment !=', NULL);
        }

        if ((!$this->Customer) && !$this->Supplier && 
                    !$this->Owner && !$this->Admin && 
                    !$this->session->userdata('view_right')) {
            $sql_query = $sql_query->where('created_by', $this->session->userdata('user_id'));
        }
        if ($this->Customer){
            $sql_query = $sql_query->where('customer_id', $this->session->userdata('user_id'));
        }

        if ($warehouse_id != '') {
            $this->db->where("{$this->db->dbprefix('sales')}.warehouse_id", $warehouse_id);
        }
        
        if ($config_data['sale_status'] != '') 
            $this->db->where("{$this->db->dbprefix('sales')}.sale_status", $config_data['sale_status']);
            
        if ($config_data['mode'] != '') 
            $this->db->where("sample =", 1);
        else
            $this->db->where("sample !=", 1);

        if ($config_data['start_date'] != '' && $config_data['end_date'] != '')
            $this->db->where("convert({$this->db->dbprefix('sales')}.date, Date) between STR_TO_DATE('".$config_data['start_date']."', '%d/%m/%Y') and STR_TO_DATE('".$config_data['end_date']."', '%d/%m/%Y')", null);

        if ($config_data['order_tax_id'] != '') {
            if ($config_data['order_tax_id'] == 2)
                $this->db->where("{$this->db->dbprefix('sales')}.order_tax >", 0);
            else
                $this->db->where("{$this->db->dbprefix('sales')}.order_tax", 0);
        }

        if ($keyword != '') {
            if (is_numeric($keyword))
                $this->db->where("id = ".$keyword);
            else 
                $this->db->where("
                    (customer LIKE '%". $keyword ."%' OR 
                    sale_status LIKE '%". $keyword ."%' OR 
                    biller LIKE '%". $keyword ."%' OR 
                    reference_no LIKE '%". $keyword ."%' OR 
                    date LIKE '%". $keyword ."%' OR 
                    payment_status LIKE '%". $keyword ."%')", 
                    NULL, 
                    FALSE
                );
        }
        if ($sort_by != '') {
            $this->db->order_by("$sort_by", "$sort_order");
        }
        else
            $this->db->order_by("id", "DESC");
        if ($limit != '') {
            $this->db->limit($limit, $offset);
        }
        // echo '<pre>';print_r($keyword);  exit();
        // $result = $this->db->get('sales');
            
        return $this->db->get('sales');
    }
    
	public function api_select_data($id)
    {
        $temp_field = $this->site->api_get_condition_add_ons_field("add_ons","sma_add_ons","table_name = 'sma_sales'");
        $select_data = $this->site->api_select_some_fields_with_where("* 
            ".$temp_field
            ,"sma_sales"
            ,"id = ".$id
            ,"arr"
        );
        return $select_data;        
    }
    public function getInvoiceByID_v2($id)
    {
        $temp_field = $this->site->api_get_condition_add_ons_field("add_ons","sma_add_ons","table_name = 'sma_sales'");
        $temp = $this->site->api_select_some_fields_with_where("id
            ".$temp_field
            ,"sma_sales"
            ,"id = ".$id
            ,"arr"
        );
        $q = $this->db->get_where('sales', array('id' => $id), 1);
        $temp2 = $q->row();
        foreach ($temp[0] as $key => $value) {
            $temp2->{$key} = $value;
        }
        return $temp2;
    }
	public function get_delivery_person()
    {
        $temp_field = $this->site->api_get_condition_add_ons_field("add_ons","sma_add_ons","table_name = 'sma_delivery_person'");
        $select_data = $this->site->api_select_some_fields_with_where("*,
            getTranslate(translate,'en','".f_separate."','".v_separate."') as title
            ".$temp_field
            ,"sma_delivery_person"
            ,"id > 0 order by title asc"
            ,"arr"
        );
        return $select_data;        
    }
	public function getSaleSummary($config_data) {
        $condition = '';
        if ($config_data['search'] != '') {
            $condition .= "
                and (reference_no LIKE '%". $config_data['search'] ."%'
                OR customer LIKE '%". $config_data['search'] ."%'
                OR payment_status LIKE '%". $config_data['search'] ."%'
            ";
            if (is_numeric($config_data['search']))
                $condition .= ' OR '.$config_data['table_id']." = ".$config_data['search'];
            $condition .= ')';
        }

        $temp = $this->input->get('customer_id');
        if ($temp > 0)
            $condition .= " and customer_id = ".$temp;        

        $order_by = '';
        if ($config_data['sort_by'] != '') {
            if ($config_data['sort_by'] == 'currency')
                $order_by .= "order by date";
            else
                $order_by .= 'order by '.$config_data['sort_by'];
                
            $order_by .= ' '.$config_data['sort_order'];                
        }
        else
            $order_by .= 'order by '.$config_data['table_id'].' Desc';

        if ($limit != '') {
            $this->db->limit($limit, $offset);
        }

        if ($config_data['limit'] != '')
            $order_by .= ' limit '.$config_data['offset_no'].', '.$config_data['limit'];

        if ($config_data['selected_id'] > 0)
            $condition = ' and '.$config_data['table_id']." = ".$config_data['selected_id'];
        elseif ($config_data['customer_id'] != '')
            $condition = " and customer_id = ".$config_data['customer_id'];

        $temp_field = $this->site->api_get_condition_add_ons_field("add_ons","sma_add_ons","table_name = '".$config_data['table_name']."'");
        $select_data = $this->site->api_select_some_fields_with_where("* 
            ".$temp_field
            ,$config_data['table_name']
            ,$config_data['table_id']." > 0 ".$condition." ".$order_by
            ,"arr"
        );  
                    
        return $select_data;
    }
    public function api_update_consignment_purchase_status($id) {
        $config_data = array(
            'table_name' => 'sma_purchase_items',
            'select_table' => 'sma_purchase_items',
            'translate' => '',
            'select_condition' => "purchase_id = ".$id,
        );
        $temp = $this->site->api_select_data_v2($config_data);
        $temp_purchase_qty = 0;
        for ($i=0;$i<count($temp);$i++) {
            $temp_purchase_qty = $temp_purchase_qty + $temp[$i]['quantity'];
        }

        $config_data = array(
            'table_name' => 'sma_consignment_purchase_items',
            'select_table' => 'sma_consignment_purchase_items',
            'translate' => '',
            'select_condition' => "purchase_id = ".$id,
        );
        $temp = $this->site->api_select_data_v2($config_data);
        $temp_sold_qty = 0;
        for ($i=0;$i<count($temp);$i++) {
            if ($temp[$i]['quantity'] > 0)
                $temp_sold_qty = $temp_sold_qty + $temp[$i]['quantity'];
            else
                $this->db->delete('sma_consignment_purchase_items', 'id = '.$temp[$i]['id']);
        }

        if ($temp_sold_qty >= $temp_purchase_qty) {
            $config_data = array(
                'table_name' => 'sma_purchases',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $id,
                'add_ons_title' => 'consignment_status',
                'add_ons_value' => 'completed',
            );
            $this->site->api_update_add_ons_field($config_data);         
        }
        else {
            $config_data = array(
                'table_name' => 'sma_purchases',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $id,
                'add_ons_title' => 'consignment_status',
                'add_ons_value' => 'pending',
            );
            $this->site->api_update_add_ons_field($config_data);
        }
    }

    public function api_calculate_consignment_purchase_wrapper($id) {
        $config_data_2 = array(
            'table_name' => 'sma_purchases',
            'select_table' => 'sma_purchases',
            'translate' => '',
            'select_condition' => "getTranslate(add_ons,'consignment_status','".f_separate."','".v_separate."') != '' and getTranslate(add_ons,'consignment_status','".f_separate."','".v_separate."') != 'completed' order by id asc",
        );
        $temp_purchase = $this->site->api_select_data_v2($config_data_2);                     
        $temp_sale_item = $this->site->api_select_some_fields_with_where("
            *
            "
            ,"sma_sale_items"
            ,"sale_id = ".$id
            ,"arr"
        );
        $temp_return = $this->site->api_select_some_fields_with_where("
            id
            "
            ,"sma_returns"
            ,"add_ons like '%:sale_id:{".$id."}:%'"
            ,"arr"
        );
        for ($i=0;$i<count($temp_sale_item);$i++) {
            if ($temp_sale_item[$i]['option_id'] > 0)
                $condition = " and option_id = ".$temp_sale_item[$i]['option_id'];
            else
                $condition = '';

            $temp_qty = $temp_sale_item[$i]['quantity'];
            
            $temp_5 = 0;
            if ($temp_return[0]['id'] > 0) {
                $temp_return_item = $this->site->api_select_some_fields_with_where("
                    quantity
                    "
                    ,"sma_return_items"
                    ,"return_id = '".$temp_return[0]['id']."' and product_id = ".$temp_sale_item[$i]['product_id']." ".$condition
                    ,"arr"
                );
                for ($i2=0;$i2<count($temp_return_item);$i2++) {
                    $temp_5 = $temp_5 + $temp_return_item[$i2]['quantity'];
                }
            }
            $temp_qty = $temp_qty - $temp_5;

            $config_data_2 = array(
                'sale_id' => $temp_sale_item[$i]['sale_id'],
                'product_id' => $temp_sale_item[$i]['product_id'],
                'option_id' =>$temp_sale_item[$i]['option_id'],
                'quantity' => $temp_qty,
            );
            $temp = $this->api_calculate_consignment_purchase($config_data_2, $temp_purchase);

            while ($temp > 0) {
                $config_data_2 = array(
                    'sale_id' => $temp_sale_item[$i]['sale_id'],
                    'product_id' => $temp_sale_item[$i]['product_id'],
                    'option_id' => $temp_sale_item[$i]['option_id'],
                    'quantity' => $temp,
                );
                $temp = $this->api_calculate_consignment_purchase($config_data_2, $temp_purchase);
            }
        }                
    }

    public function api_calculate_consignment_purchase($config_data, $temp_purchase) {
        $temp_remain_qty = 0;
        for ($i2=0;$i2<count($temp_purchase);$i2++) {
            $config_data_2 = array(
                'table_name' => 'sma_purchase_items',
                'select_table' => 'sma_purchase_items',
                'translate' => '',
                'select_condition' => "purchase_id = ".$temp_purchase[$i2]['id'],
            );
            $temp_purchase_item = $this->site->api_select_data_v2($config_data_2);
            for ($i3=0;$i3<count($temp_purchase_item);$i3++) {
                if ($config_data['option_id'] > 0)
                    $condition = " and option_id = ".$config_data['option_id'];
                else
                    $condition = '';

                if ($temp_purchase_item[$i3]['product_id'] == $config_data['product_id']) {
                    $temp_5 = $this->site->api_select_some_fields_with_where("
                        quantity
                        "
                        ,"sma_consignment_purchase_items"
                        ,"purchase_id = ".$temp_purchase[$i2]['id']." and product_id = ".$config_data['product_id']." ".$condition
                        ,"arr"
                    );
                    $temp_sold_quantity = 0;
                    for ($i5=0;$i5<count($temp_5);$i5++) {
                        $temp_sold_quantity = $temp_sold_quantity + $temp_5[$i5]['quantity'];
                    }

                    if ($temp_sold_quantity != $temp_purchase_item[$i3]['quantity']) {
                        $temp = $temp_sold_quantity + $config_data['quantity'];                        
                        if ($temp > $temp_purchase_item[$i3]['quantity']) {
                            $temp_remain_qty = $temp - $temp_purchase_item[$i3]['quantity'];
                            $temp = $temp_purchase_item[$i3]['quantity'];
                            $temp_set_quantity = $config_data['quantity'] - $temp_remain_qty;
                        }
                        else 
                            $temp_set_quantity = $config_data['quantity'];


                        $config_data_2 = array(
                            'table_name' => 'sma_consignment_purchase_items',
                            'select_table' => 'sma_consignment_purchase_items',
                            'translate' => '',
                            'select_condition' => "sale_id = ".$config_data['sale_id']." and purchase_id = ".$temp_purchase[$i2]['id']." and product_id = ".$config_data['product_id']." ".$condition,
                        );
                        $temp_consignment_purchase_items = $this->site->api_select_data_v2($config_data_2);
                        if ($temp_consignment_purchase_items[0]['id'] > 0) {
                            $temp_4 = $temp_consignment_purchase_items[0]['quantity'] + $temp_set_quantity;
                            $config_data_2 = array(
                                'quantity' => $temp_4,
                                'date' => date('Y-m-d H:i:s'),
                            );
                            $this->db->update('sma_consignment_purchase_items', $config_data_2,'id = '.$temp_consignment_purchase_items[0]['id']);
                        }
                        else {
                            $config_data_2 = array(
                                'sale_id' => $config_data['sale_id'],
                                'purchase_id' => $temp_purchase[$i2]['id'],
                                'product_id' => $config_data['product_id'],
                                'option_id' => $config_data['option_id'],
                                'quantity' => $temp_set_quantity,
                                'date' => date('Y-m-d H:i:s'),
                            );
                            $this->db->insert('sma_consignment_purchase_items', $config_data_2);
                        }

                        $this->api_update_consignment_purchase_status($temp_purchase[$i2]['id']);

                        return $temp_remain_qty;
                    }
                }
            }
        }
        return $temp_remain_qty;
    }

    public function api_reverse_consignment_purchase($id) {
        $config_data = array(
            'table_name' => 'sma_consignment_purchase_items',
            'select_table' => 'sma_consignment_purchase_items',
            'translate' => '',
            'select_condition' => "sale_id = ".$id,
        );
        $select_data = $this->site->api_select_data_v2($config_data);
        $this->db->delete('sma_consignment_purchase_items', 'sale_id = '.$id);
        for ($i=0;$i<count($select_data);$i++) {
            $this->api_update_consignment_purchase_status($select_data[$i]['purchase_id']);
        }        
    }
    
    public function api_get_rebate_options($grand_total) {
        $config_data = array(
            'table_name' => 'sma_settings',
            'select_table' => 'sma_settings',
            'translate' => '',
            'select_condition' => "setting_id = 1",
        );
        $temp = $this->site->api_select_data_v2($config_data);

        $return['value'] = 0;
        for ($i=1;$i<=6;$i++) {
            if ($i <= 5) {
                if ($grand_total >= $temp[0]['rebate_option_from_'.$i] && $grand_total <= $temp[0]['rebate_option_to_'.$i]) {
                    $return['value'] = $temp[0]['rebate_option_value_'.$i];
                    break;
                }
            }
            else {
                if ($grand_total >= $temp[0]['rebate_option_from_'.$i]) {
                    $return['value'] = $temp[0]['rebate_option_value_'.$i];
                    break;
                }                
            }
        }
        return $return;
    }

    public function api_get_sale($config_data) {
        $condition = '';
        $config_data['search'] = trim($config_data['search']);
    
    // --> search paid by
    $paid_by = $_GET['paid_by'];
    if($paid_by != '' && $paid_by != 'AR') {
            $config_data_v2 = array(
                'table_name' => 'sma_payments',
                'select_table' => 'sma_payments',
                'translate' => '',
                'select_condition' => "sale_id > 0 and paid_by = '".$paid_by."' order by id desc",
            );
            $select_payment = $this->site->api_select_data_v2($config_data_v2);
            for($k=0;$k<count($select_payment);$k++) {
                $sale_id .= '-'.$select_payment[$k]['sale_id'];
            }
        $_GET['adjustment_sale_track'] = $sale_id;
    } elseif ($paid_by == 'AR') {
        $config_data_v2 = array(
            'table_name' => 'sma_sales',
            'select_table' => 'sma_sales',
            'translate' => '',
            'select_condition' => "id > 0 and payment_status = '".$paid_by."' order by id desc",
        );
        $select_sales = $this->site->api_select_data_v2($config_data_v2);
        for($j=0;$j<count($select_sales);$j++) {
            $sale_id2 .= '-'.$select_sales[$j]['id'];
        }
        $_GET['adjustment_sale_track'] = $sale_id2;
    }
    // search paid by <--

        if ($_GET['adjustment_sale_track'] != '') {
            $temp = explode('-',$_GET['adjustment_sale_track']);
            $condition = " and (t1.id = ".$temp[1];
            for ($i=2;$i<count($temp);$i++) {
                $condition .= " or t1.id = ".$temp[$i];
            }
            $condition .= ')';
        }
        elseif ($config_data['search'] != '') {
            $condition .= "
                and (t1.reference_no LIKE '%". $config_data['search'] ."%'
                OR t1.customer LIKE '%". $config_data['search'] ."%'
                OR t1.payment_status LIKE '%". $config_data['search'] ."%'
                OR t1.biller LIKE '%". $config_data['search'] ."%'
                
                ";
                if (is_numeric($config_data['search']))
                $condition .= ' OR t1.'.$config_data['table_id']." = ".$config_data['search'];
                $condition .= ')';
            }
            // OR t2.add_ons LIKE '%:pipay_transaction_id:{". $config_data['search'] ."%}:'
            // OR t2.add_ons LIKE '%:wing_transaction_id:{ ". $config_data['search'] ."%}:'
            // OR t2.add_ons LIKE '%:aba_qr_transfer_reference_number:{ ". $config_data['search'] ."%}:'
            // OR t2.add_ons LIKE '%:transfer_reference_number:{ ". $config_data['search'] ."%}:'
            // OR t2.paid_by LIKE '%". $config_data['search'] ."%'

        $temp = $this->input->get('warehouse_id');
        if (is_numeric($temp))
            $condition .= " and t1.warehouse_id = ".$temp;

        $temp = $this->input->get('sale_status');
        if ($temp != '')
            $condition .= " and t1.sale_status = '".$temp."'";

        $temp = $this->input->get('mode');
        if ($_GET['adjustment_sale_track'] == '') {
            if ($temp == 'sample') 
                $condition .= " and t1.sample = 1";
            else
                $condition .= " and t1.sample = 0";
        }

        $temp1 = $this->input->get('start_date');
        $temp2 = $this->input->get('end_date');
        if ($temp1 != '' && $temp1 != '')
            $condition .= " and (convert(t1.date, Date) between STR_TO_DATE('".$temp1."', '%d/%m/%Y') and STR_TO_DATE('".$temp2."', '%d/%m/%Y'))";

        $temp = $this->input->get('order_tax_id');
        if ($temp > 0)
            $condition .= " and t1.order_tax_id = ".$temp;

        $order_by = '';
        $order_by = 'order by t1.'.$config_data['table_id'].' Desc';
        if ($config_data['sort_by'] != '') {
            if ($config_data['sort_by'] == 'delivery_date')
                $order_by = 'order by delivery_date';
            elseif ($config_data['sort_by'] == 'delivery_person')
                $order_by = 'order by delivery_person';
            else
                $order_by = 'order by t1.'.$config_data['sort_by'];

            $order_by .= ' '.$config_data['sort_order'];
        }
        else
            $order_by = 'order by t1.date desc, t1.id desc';

        $temp_field = $this->site->api_get_condition_add_ons_field("t1.add_ons","sma_add_ons","table_name = '".$config_data['table_name']."'");
        
        if ($config_data['limit'] != '') {
            $temp_select = "t1.*".$temp_field;
            $order_by .= ' limit '.$config_data['offset_no'].', '.$config_data['limit'];
        }
        else
            $temp_select = "t1.id";

        $select_data = $this->site->api_select_some_fields_with_where(
            $temp_select
            // $temp_select.",t2.add_ons, t2.paid_by"
            ,$config_data['table_name']." as t1"
            // ,$config_data['table_name']." as t1 inner join sma_payments as t2 on t1.id = t2.sale_id"
            ,"t1.".$config_data['table_id']." > 0 and disabled != 1 ".$condition." ".$order_by
            ,"arr"
        );
        return $select_data;
    }
    public function api_calculate_sale_summary($id) {

        $config_data = array(
            'table_name' => 'sma_sale_summary',
            'select_table' => 'sma_sale_summary',
            'select_condition' => "id = ".$id,
        );
        $select_data = $this->site->api_select_data_v2($config_data);

        $config_data = array(
            'table_name' => 'sma_sales',
            'select_table' => 'sma_sales',
            'select_condition' => "customer_id = ".$select_data[0]['customer_id']." and reference_no like '%DO%' and grand_total > 0 and payment_status != 'paid' and convert(date, Date) between STR_TO_DATE('".$select_data[0]['start_date']."', '%Y-%m-%d') and STR_TO_DATE('".$select_data[0]['end_date']."', '%Y-%m-%d') order by date asc",
        );
        $temp = $this->site->api_select_data_v2($config_data);
        $return['grand_total'] = 0;
        $return['rebate'] = 0;
        for ($i=0;$i<count($temp);$i++) {
            $return['grand_total'] = $return['grand_total'] + $temp[$i]['grand_total'];
            $return['sales'] .= '-'.$temp[$i]['id'];
        }

        if ($select_data[0]['rebate'] == '' || $select_data[0]['rebate'] == 'yes') {
            $temp5 = $this->api_get_rebate_options($return['grand_total']);
            if ($temp5['value'] > 0)
                $return['rebate'] = ($return['grand_total'] * $temp5['value']) / 100;
        }

        return $return;


    }
}




