<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Companies_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAllBillerCompanies()
    {
        $q = $this->db->get_where('companies', array('group_name' => 'biller'));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllCustomerCompanies()
    {
        $q = $this->db->get_where('companies', array('group_name' => 'customer'));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllSupplierCompanies()
    {
        $q = $this->db->get_where('companies', array('group_name' => 'supplier'));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllCustomerGroups()
    {
        $q = $this->db->get('customer_groups');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCompanyUsers($company_id)
    {
        $q = $this->db->get_where('users', array('company_id' => $company_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCompanyByID($id)
    {
        $q = $this->db->get_where('companies', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCompanyByEmail($email)
    {
        $q = $this->db->get_where('companies', array('email' => $email), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addCompany($data = array())
    {
        if ($this->db->insert('companies', $data)) {
            $cid = $this->db->insert_id();
            return $cid;
        }
        return false;
    }

    public function updateCompany($id, $data = array())
    {
        $this->db->where('id', $id);
        if ($this->db->update('companies', $data)) {
            return true;
        }
        return false;
    }

    public function addCompanies($data = array())
    {
        if ($this->db->insert_batch('companies', $data)) {
            return true;
        }
        return false;
    }

    public function deleteCustomer($id)
    {
        if ($this->getCustomerSales($id)) {
            return false;
        }
        if ($this->db->delete('companies', array('id' => $id, 'group_name' => 'customer')) && $this->db->delete('users', array('company_id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteSupplier($id)
    {
        if ($this->getSupplierPurchases($id)) {
            return false;
        }
        if ($this->db->delete('companies', array('id' => $id, 'group_name' => 'supplier')) && $this->db->delete('users', array('company_id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteBiller($id)
    {
        if ($this->getBillerSales($id)) {
            return false;
        }
        if ($this->db->delete('companies', array('id' => $id, 'group_name' => 'biller'))) {
            return true;
        }
        return FALSE;
    }

    public function getBillerSuggestions($term, $limit = 10)
    {
        $this->db->select("id, company as text");
        $this->db->where(" (id LIKE '%" . $term . "%' OR name LIKE '%" . $term . "%' OR company LIKE '%" . $term . "%') ");
        $q = $this->db->get_where('companies', array('group_name' => 'biller'), $limit);
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getCustomerSuggestions($term, $limit = 10)
    {
        $this->db->select("id, (CASE WHEN company = '-' THEN name ELSE CONCAT(company, ' (', name, ')') END) as text, (CASE WHEN company = '-' THEN name ELSE CONCAT(company, ' (', name, ')') END) as value, phone", FALSE);
        $this->db->where(" (id LIKE '%" . $term . "%' OR name LIKE '%" . $term . "%' OR company LIKE '%" . $term . "%' OR email LIKE '%" . $term . "%' OR phone LIKE '%" . $term . "%') and getTranslate(add_ons,'closed','".f_separate."','".v_separate."') != 'yes'");
        $q = $this->db->get_where('companies', array('group_name' => 'customer'), $limit);
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getSupplierSuggestions($term, $limit = 10)
    {
        $this->db->select("id, (CASE WHEN company = '-' THEN name ELSE CONCAT(company, ' (', name, ')') END) as text", FALSE);
        $this->db->where(" (id LIKE '%" . $term . "%' OR name LIKE '%" . $term . "%' OR company LIKE '%" . $term . "%' OR email LIKE '%" . $term . "%' OR phone LIKE '%" . $term . "%') ");
        $q = $this->db->get_where('companies', array('group_name' => 'supplier'), $limit);
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getCustomerSales($id)
    {
        $this->db->where('customer_id', $id)->from('sales');
        return $this->db->count_all_results();
    }

    public function getBillerSales($id)
    {
        $this->db->where('biller_id', $id)->from('sales');
        return $this->db->count_all_results();
    }

    public function getSupplierPurchases($id)
    {
        $this->db->where('supplier_id', $id)->from('purchases');
        return $this->db->count_all_results();
    }

    public function addDeposit($data, $cdata)
    {
        if ($this->db->insert('deposits', $data) &&
            $this->db->update('companies', $cdata, array('id' => $data['company_id']))) {
            return true;
        }
        return false;
    }

    public function updateDeposit($id, $data, $cdata)
    {
        if ($this->db->update('deposits', $data, array('id' => $id)) &&
            $this->db->update('companies', $cdata, array('id' => $data['company_id']))) {
            return true;
        }
        return false;
    }

    public function getDepositByID($id)
    {
        $q = $this->db->get_where('deposits', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteDeposit($id)
    {
        $deposit = $this->getDepositByID($id);
        $company = $this->getCompanyByID($deposit->company_id);
        $cdata = array(
                'deposit_amount' => ($company->deposit_amount-$deposit->amount)
            );
        if ($this->db->update('companies', $cdata, array('id' => $deposit->company_id)) &&
            $this->db->delete('deposits', array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function getAllPriceGroups()
    {
        $q = $this->db->get('price_groups');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCompanyAddresses($company_id)
    {
        $q = $this->db->get_where('addresses', array('company_id' => $company_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function addAddress($data)
    {
        if ($this->db->insert('addresses', $data)) {
            return true;
        }
        return false;
    }

    public function updateAddress($id, $data)
    {
        if ($this->db->update('addresses', $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function deleteAddress($id)
    {
        if ($this->db->delete('addresses', array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function getAddressByID($id)
    {
        $q = $this->db->get_where('addresses', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

	public function getAllSalePerson()
    {
       $q = $this->db->get_where('users','group_id != 3');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
    public function getCompanyByID_v2($id)
    {
        $temp_field = $this->site->api_get_condition_add_ons_field("add_ons","sma_add_ons","table_name = 'sma_companies'");
        $temp = $this->site->api_select_some_fields_with_where("id
            ".$temp_field
            ,"sma_companies"
            ,"id = ".$id
            ,"arr"
        );
        $q = $this->db->get_where('sma_companies', array('id' => $id), 1);
        $temp2 = $q->row();
        foreach ($temp[0] as $key => $value) {
            $temp2->{$key} = $value;
        }
        return $temp2;
    }

    public function getCompanyBranch($config_data) {
        $condition = '';
        if ($config_data['search'] != '') {
            $condition .= "
                and (CONVERT(getTranslate(t1.translate,'".$config_data['lg']."','".f_separate."','".v_separate."') USING utf8)) LIKE '%". $config_data['search'] ."%'
                OR getTranslate(t1.add_ons,'po_number','".f_separate."','".v_separate."') LIKE '%". $config_data['search'] ."%'
            ";
        }
        
        $order_by = '';
        if ($config_data['sort_by'] != '') {
            if ($config_data['sort_by'] == 'title')
                $order_by .= "order by getTranslate(t1.translate,'en','".f_separate."','".v_separate."')";
            elseif ($config_data['sort_by'] == 'branch_number')
                $order_by .= "order by getTranslate(t1.add_ons,'branch_number','".f_separate."','".v_separate."')";
            else
                $order_by .= 'order by '.$config_data['sort_by'];
                
            $order_by .= ' '.$config_data['sort_order'];
                
        }
        else
            $order_by .= 'order by t1.'.$config_data['table_id'].' Desc';

        if ($limit != '') {
            $this->db->limit($limit, $offset);
        }

        if ($config_data['limit'] != '')
            $order_by .= ' limit '.$config_data['offset_no'].', '.$config_data['limit'];

        if ($config_data['selected_id'] > 0)
            $condition = ' and t1.'.$config_data['table_id']." = ".$config_data['selected_id'];
        elseif ($config_data['company'] != '')
            $condition = " and t1.parent_id = ".$config_data['company'];

        $temp_field = $this->site->api_get_condition_add_ons_field("t1.add_ons","sma_add_ons","table_name = '".$config_data['table_name']."'");
        $select_data = $this->site->api_select_some_fields_with_where("t1.*,
            getTranslate(t1.translate,'en','".f_separate."','".v_separate."') as title,
            t2.company as company 
            ".$temp_field
            ,"sma_company_branch as t1 inner join sma_companies as t2 on t1.parent_id = t2.id"
            ,"t1.".$config_data['table_id']." > 0 ".$condition." ".$order_by
            ,"arr"
        );  
                    
        return $select_data;
    }

    public function addCompanyBranch($data = array())
    {
        if ($this->db->insert('company_branch', $data)) {
            return true;
        }
        return false;
    }   

    public function api_get_data($config_data) {
        $condition = '';
        $config_data['search'] = trim($config_data['search']);

        if ($_GET['parent_id'] != '') {
            $condition .= " and (t1.parent_id = ".$_GET['parent_id']." or t1.id = ".$_GET['parent_id'].")";
        }
        elseif ($config_data['search'] != '') {
            $condition .= "
                and (t1.company LIKE '%". $config_data['search'] ."%'
                OR t1.name LIKE '%". $config_data['search'] ."%'
                OR t1.email LIKE '%". $config_data['search'] ."%'
                OR t1.phone LIKE '%". $config_data['search'] ."%'
                OR t1.vat_no LIKE '%". $config_data['search'] ."%'
                
            ";
            if (is_numeric($config_data['search']))
                $condition .= ' OR t1.'.$config_data['table_id']." = ".$config_data['search'];

            $condition .= ')';
        }
        if ($_GET['products_name'] != '') {
            $condition .= " and getTranslate(t1.add_ons,'products_list','".f_separate."','".v_separate."') LIKE '%-". $_GET['products_name']. "-%' ";
        }

        $order_by = '';
        if ($config_data['sort_by'] != '') {
            $order_by = 'order by '.$config_data['sort_by'];
            /*
            if ($config_data['sort_by'] == '')
                $order_by = 'order by delivery_date';
            */

            $order_by .= ' '.$config_data['sort_order'];
        }
        else
            $order_by = "order by t1.id Desc";

        $temp_field = $this->site->api_get_condition_add_ons_field("t1.add_ons","sma_add_ons","table_name = '".$config_data['table_name']."'");

        if ($config_data['limit'] != '') {
            $temp_select = "t1.*".$temp_field;
            $order_by .= ' limit '.$config_data['offset_no'].', '.$config_data['limit'];
        }
        else
            $temp_select = "t1.id";

        $select_data = $this->site->api_select_some_fields_with_where(
            $temp_select
            ,$config_data['table_name']." as t1"
            ,"t1.".$config_data['table_id']." > 0 and group_id = 3 ".$condition." ".$order_by
            ,"arr"
        );


        if ($config_data['limit'] != '') {            
            for ($i=0;$i<count($select_data);$i++) {
                if ($select_data[$i]['parent_id'] > 0) {
                    $temp_3 = $this->site->api_select_some_fields_with_where("
                        company
                        "
                        ,"sma_companies"
                        ,"id = ".$select_data[$i]['parent_id']
                        ,"arr"
                    );
                    $select_data[$i]['parent_name'] = $temp_3[0]['company'];
                }


                $temp = $this->site->api_select_some_fields_with_where("
                    *
                    "
                    ,"sma_companies"
                    ,"parent_id = ".$select_data[$i]['id']." order by company asc"
                    ,"arr"
                );
                if (count($temp) > 0) {
                    $temp_2 = '<ul style="list-style:decimal; padding-left:15px;">';
                    for ($i2=0;$i2<count($temp);$i2++) {
                        $temp_2 .= '
                            <li>
                                '.$temp[$i2]['company'].'
                            </li>
                        ';
                    }
                    $temp_2 .= '</ul>';
                    $select_data[$i]['branch_list'] = $temp_2;
                }
            }
        }


        return $select_data;
    }

	
}
