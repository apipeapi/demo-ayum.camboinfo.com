<?php

class Api_select_data {

public function __construct()
{

}
public function __get($var)
{
    return get_instance()->$var;
}

public function get_blog_category_id($category_slug) {   
    $temp = $this->site->api_select_some_fields_with_where("
        id
        "
        ,"sma_blog_category"
        ,"slug = '".$category_slug."'"
        ,"arr"
    );
    return $temp[0]['id'];
}

public function data_pagination_blog($config_data) {    
    $l = $this->Settings->api_lang_key;

    $get_search = $this->input->get('search', true);
    $get_sort_by = $this->input->get('sort_by', true);

    $condition = '';
    if ($get_search != '') {

        $condition .= "
            and (getTranslate(t1.translate,'".$l."','".f_separate."','".v_separate."') LIKE '%".$get_search."%' or getTranslate(t1.translate_2,'".$l."','".f_separate."','".v_separate."') LIKE '%".$get_search."%'
            or getTranslate(t1.description,'".$l."','".f_separate."','".v_separate."') LIKE '%".$get_search."%'
        ";
        if (is_numeric($get_search))
            $condition .= " or t1.".$config_data['table_id']." = ".$get_search;
        $condition .= ')';
    }

    $category_slug = $this->uri->segment(2); 
    if ($category_slug != '') {
        $c_id = $this->get_blog_category_id($category_slug);
        $condition .= " and t1.c_id = ".$c_id;
    }

    if($_GET['tag_id'] != ''){ 
        $condition .=" and t1.tag_id LIKE '%-".$_GET['tag_id']."-%'";
    }

    $order_by = '';
    if ($get_sort_by != '') {
        $order_by .= 'order by '.$get_sort_by;
        $order_by .= ' '.$_GET['sort_order']; 
    }
    else
        $order_by .= 'order by t1.created_date Desc';

    if ($config_data['limit'] != '')
        $order_by .= ' limit '.$config_data['offset_no'].', '.$config_data['limit'];

    if ($config_data['translate'] == 'yes') {
        $language_array = unserialize(multi_language);
        for ($i=0;$i<count($language_array);$i++) {                    
            $temp_translate .= ", getTranslate(t1.translate,'".$language_array[$i][0]."','".f_separate."','".v_separate."') as title_".$language_array[$i][0];     
            $temp_translate .= ", getTranslate(t1.translate_2,'".$language_array[$i][0]."','".f_separate."','".v_separate."') as title_2_".$language_array[$i][0];             
        }
    }
    if ($config_data['description'] == 'yes') {
        $language_array = unserialize(multi_language);
        for ($i=0;$i<count($language_array);$i++) {                    
            $temp_translate .= ", getTranslate(t1.description,'".$language_array[$i][0]."','".f_separate."','".v_separate."') as description_".$language_array[$i][0];                
        }
    }    

    if ($config_data['add_ons'] == 'yes') {
        $temp_field = $this->site->api_get_condition_add_ons_field("t1.add_ons","sma_add_ons","table_name = '".$config_data['table_name']."'");
    }

    $select_data = $this->site->api_select_some_fields_with_where("* 
        ".$temp_field." ".$temp_translate
        ,$config_data['table_name']." as t1"
        ,"t1.".$config_data['table_id']." > 0 ".$condition." ".$order_by
        ,"arr"
    );  
                
    return $select_data;
}

public function get_post($id) {
    $config_data = array(
        'table_name' => 'sma_post',
        'select_table' => 'sma_post',
        'translate' => 'yes',
        'description' => 'yes',
        'select_condition' => "id = ".$id,
    );
    $return = $this->api_helper->api_select_data_v2($config_data);
    return $return;
}
public function blog_category_slug($id) {
    $config_data = array(
        'table_name' => 'sma_blog_category',
        'select_table' => 'sma_blog_category',
        'translate' => '',
        'description' => '',
        'select_condition' => "id = ".$id,
    );
    $temp = $this->api_helper->api_select_data_v2($config_data);
    return $temp[0]['slug'];
}

}
