<?php

class Api_select_field {

public function __construct()
{

}
public function __get($var)
{
    return get_instance()->$var;
}

public function blog_category($config_data_function) {   
    $config_data = array(
        'table_name' => 'sma_blog_category',
        'select_table' => 'sma_blog_category',
        'translate' => 'yes',
        'description' => '',
        'select_condition' => "id > 0 ".$config_data_function['condition'],
    );
    $select_data = $this->api_helper->api_select_data_v2($config_data);
    $tr[''] = lang("All_Categories");
    for ($i=0;$i<count($select_data);$i++) {
        $tr[$select_data[$i]['id']] = $select_data[$i]['title_'.$config_data_function['l']];
    }
    $return['display'] .= form_dropdown($config_data_function['field_name'], $tr, $config_data_function['field_value'], 'class="form-control" ');

    return $return;
}

public function checkbox_list_blog_tag($config_data_function) {
    $l = $config_data_function['l'];
    $config_data = array(
        'table_name' => 'sma_blog_tags',
        'select_table' => 'sma_blog_tags',
        'translate' => 'yes',
        'description' => '',
        'select_condition' => "id > 0 ".$config_data_function['condition'],
    );
    $select_data = $this->api_helper->api_select_data_v2($config_data);

    $return['display'] .= '
        <input type="hidden" name="'.$config_data_function['field_name'].'" id="'.$config_data_function['field_name'].'" class="api_checkbox" value="'.$config_data_function['field_value'].'"/>
    ';
    $temp = explode('-',$config_data_function['field_value']);
    $temp_2 = array();
    $k = 0;
    for ($i=0;$i<count($temp);$i++) {
        if ($temp[$i] != '') {
            $temp_2[$k] = $temp[$i];
            $k++;
        }
    }

    for ($i=0;$i<count($select_data);$i++) {
        $temp_checked = '';
        for ($i2=0;$i2<count($temp_2);$i2++) {
            if ($temp_2[$i2] == $select_data[$i]['id']) {
                $temp_checked = 'checked';
                break;
            }
        }        
        $return['display'] .= '
            <div class="api_float_left api_padding_right_15">
                <table border="0">
                <tr>
                <td valign="middle" width="10">
                    <input class="api_checkbox" id="temp_'.$config_data_function['field_name'].'_'.$select_data[$i]['id'].'" type="checkbox" value="'.$select_data[$i]['id'].'" '.$temp_checked.' onclick="
                    var postData = {
                        \'field_name\' : \''.$config_data_function['field_name'].'\',
                        \'checkbox_id\' : \'temp_'.$config_data_function['field_name'].'_'.$select_data[$i]['id'].'\',
                    }
                    api_checkbox_list_click(postData);
                    "/>            
                </td>
                <td valign="middle">
                    <label class="api_padding_5">'.$select_data[$i]['title_'.$l].'</label>
                </td>
                </tr>
                </table>
            </div>
        ';
    }
    $return['display'] .= '
        <div class="api_clear_both"></div>
    ';
    return $return;
}


}
