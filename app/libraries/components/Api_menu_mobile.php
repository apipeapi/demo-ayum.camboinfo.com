<?php 

class api_menu_mobile extends CI_Model{
	function __construct()
	{
		parent::__construct();
	}
    

public function view($l,$api_web,$api_template,$api_page,$config_data){     
    $api_web = $this->site->api_select_some_fields_with_where("
        facebook     
        "
        ,"sma_shop_settings"
        ,"shop_id = 1"
        ,"arr"
    );
            
    $return = '';
    $return .= '
<div id="api_menu_mobile" class="api_menu_mobile">
<nav class="mm-menu mm-horizontal mm-offcanvas mm-hasfooter mm-hassearch mm-current mm-opened">
    <div class="mm-search">
        <input placeholder="Search" type="text" autocomplete="off" >
    </div>
    <div class="mm-footer">
        <ul class="follow-us api_margin_left_0 api_padding_left_0">
            <li><a target="_blank" href="'.$api_web[0]['facebook'].'"><i class="fa fa-facebook"></i></a></li>
        </ul>
    </div>
        
    <ul class="mm-list mm-panel mm-opened mm-current" id="mm-0">
        <div class="mm-search-2 api_margin_bottom_10">
                <input id="af_header_menu_yamaha_search" placeholder="Search" type="text" onkeydown="if (event.keyCode == 13) {$(\'#api_menu_mobile_blocker\').click(); window.location = \''.base_url().'\Blog?search=\' + this.value;}">
        </div>
    ';
    

    $return .= '        
        <li id="api_menu_mobile_0" class="api_display_none">
            <a onclick="$(\'#api_menu_mobile_blocker\').click();" class="api_pointer" href="#">
                <span class="fa fa-bars fa-lg"></span>
            </a>            
        </li>        
    ';
    
    $config_data = array(
        'table_name' => 'sma_menu',
        'select_table' => 'sma_menu',
        'translate' => 'yes',
        'translate_2' => 'yes',
        'description' => '',
        'select_condition' => "id > 0 order by ordering asc",
    );
    $api_header_menu = $this->api_helper->api_select_data_v2($config_data); 
    for ($i=0;$i<count($api_header_menu);$i++) {
        $return .= '                    
            <li id="api_menu_mobile_'.($i + 1).'">                    
                <a href="'.$api_header_menu[$i]['link'].'">        
                    <span>
                    <div onclick="window.location = \''.$api_header_menu[$i]['link'].'\'" class="api_display_inline_block">
                        <span class="api_big">'.$api_header_menu[$i]['title_'.$l].'</span><br>
                        <span class="api_normal">'.$api_header_menu[$i]['title_2_'.$l].'</span>
                    </div>
                    </span>
                </a>
            </li>
        ';           
    }

    $return .= '              
    </ul>
</nav>
</div>
<div id="api_menu_mobile_blocker" class="mm-slideout"></div>
<script>            
    $(document).ready(function(){
        $("#api_menu_mobile_button").click(function(){
            $("#api_menu_mobile").toggle("fast");
            $("#api_menu_mobile_blocker").toggle();
        });
        $("#api_menu_mobile_blocker").click(function(){
            $("#api_menu_mobile").toggle("fast");
            $("#api_menu_mobile_blocker").toggle();
        });
        '.$temp_script.'        
    });    
</script>
    ';
    return $return;    	
}


}
?>