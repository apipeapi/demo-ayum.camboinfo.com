<?php

class Api_initial_data {

public function __construct()
{

}
public function __get($var)
{
    return get_instance()->$var;
}

public function blog($config_data_function) {
    $return = array();    
    $select_data = $config_data_function['select_data'];
    $l = $config_data_function['l'];
    for ($i=0;$i<count($select_data);$i++) {
        $initial_data[$i]['id'] = $select_data[$i]['id'];
        $initial_data[$i]['title'] = $select_data[$i]['title_'.$l];
        $initial_data[$i]['image'] = $select_data[$i]['image'];
        $initial_data[$i]['description'] = $this->api_helper->decode_html($select_data[$i]['description_'.$l]);
    }
    $return = $initial_data;
    return $return;
} 

public function blog_step($config_data_function) {
    $return = array();    
    $select_data = $config_data_function['select_data'];
    $l = $config_data_function['l'];
    $initial_data = $this->blog($config_data_function);
    for ($i=0;$i<count($select_data);$i++) {
        $initial_data[$i]['title_2'] = $select_data[$i]['title_2_'.$l];
    }
    $return = $initial_data;
    return $return;
} 

public function large_icon($config_data_function) {
    $return = array();    
    $select_data = $config_data_function['select_data'];
    $l = $config_data_function['l'];
    for ($i=0;$i<count($select_data);$i++) {
        $initial_data[$i]['id'] = $select_data[$i]['id'];
        $initial_data[$i]['title'] = $select_data[$i]['title_'.$l];
        $initial_data[$i]['image'] = $select_data[$i]['image'];
        $initial_data[$i]['description'] = $this->api_helper->decode_html($select_data[$i]['description_'.$l]);
    }
    $return = $initial_data;
    return $return;
} 
public function large_icon_v2($config_data_function) {
    $return = array();    
    $initial_data = $this->large_icon($config_data_function);
    $l = $config_data_function['l'];
    for ($i=0;$i<count($select_data);$i++) {
        $initial_data[$i]['image'] = $select_data[$i]['image'];
    }
    $return = $initial_data;
    return $return;
} 


}
