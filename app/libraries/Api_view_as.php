<?php

class Api_view_as {

public function __construct()
{

}
public function __get($var)
{
    return get_instance()->$var;
}

public function blog_step($config_data_function) {
    $return = array();    
    $l = $config_data_function['l'];
    $select_data = $config_data_function['select_data'];
    for ($i=0;$i<count($select_data);$i++) {
        $temp_display_1 .= '
        <div class="api_admin api_admin_wrapper_blog_step_'.$select_data[$i]['id'].'">
            <table border="0">
            <tr>
            <td class="api_temp_td_1" width="50%" valign="top">
                <div class="api_big api_temp_title_1">
                    '.$select_data[$i]['title_2'].'
                </div>
                <div class="api_height_5"></div>
                <div class="api_big api_temp_title_2">
                    '.$select_data[$i]['title'].'
                </div>             
                <div class="api_height_10"></div>
                <div class="api_temp_title_3">
                    '.$select_data[$i]['description'].'
                </div>                           
            </td>
            <td class="api_temp_td_2" width="50%" valign="bottom" style="background-image:url('.$config_data_function['image_path'].'/'.$select_data[$i]['image'].');">
                <div class="api_temp_number">
                    '.str_pad('0',2,$i + 1).'
                </div>
            </td>
            </tr>
            </table>
        ';
        
        $field_data = array();
        $field_data_add = array();
        $field_data[0] = array(
            'type' => 'translate',
            'col_class' => 'col-md-6',
            'field_class' => '',
            'field_name' => 'translate',
            'translate' => 'yes',
            'field_label_name' => 'Title Top',                      
        );   
        $field_data[1] = array(
            'type' => 'translate',
            'col_class' => 'col-md-6',
            'field_class' => '',
            'field_name' => 'translate_2',
            'translate_2' => 'yes',
            'field_label_name' => 'Title Bottom',                      
        );   
        $field_data[2] = array(
            'type' => 'translate_textarea',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'description',
            'description' => 'yes',
            'field_label_name' => 'Description',                      
        );          
        $field_data[3] = array(
            'type' => 'file',
            'col_class' => 'col-md-4',
            'field_class' => '',
            'field_name' => 'image',    
            'field_label_name' => 'Image',
            'recommended_width' => 700,
            'recommended_height' => 450,
        );             
        $field_data[4] = array(
            'type' => 'text',
            'col_class' => 'col-md-4',
            'field_class' => 'api_numberic_input',
            'field_name' => 'ordering',
            'field_label_name' => 'Ordering',                      
        );
        $field_data_add[0] = array(
            'type' => 'hidden',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'c_id',
            'translate' => '',
            'field_label_name' => 'Category',   
            'field_value' => $config_data_function['category_id'],                   
        );   
        $config_data = array(
            'l' => $l,
            'wrapper_class' => 'api_admin_wrapper_blog_step_'.$select_data[$i]['id'],
            'modal_class' => 'modal-lg',
            'title' => 'Edit Feature', 
            'field_data' => $field_data,
            'selected_id' => $select_data[$i]['id'],
            'table_name' => 'sma_blog_step',
            'table_id' => 'id',
            'upload_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/blog_step',
            'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
            'field_data_add' => $field_data_add,
            'add' => 'yes',
            'title_add' => 'Add Feature',
            'delete' => 'yes',
            'title_delete' => '<div>Your will remove</div>'.$select_data[$i]['title'],
            'condition_ordering' => "and c_id = ".$config_data_function['category_id'],        
        );
        $temp_admin = $this->api_admin->front_end_edit($config_data);
        $temp_display_1 .= $temp_admin['display'];

        $temp_display_1 .= '
        </div>
        ';
        $temp_display_1 .= '
            <script>
                '.$temp_admin['script'].'
            </script>
        ';

        if ($i < count($select_data) - 1)
            $temp_display_1 .= $config_data_function['seperator'];
        
        $temp_display_2 .= '
            <table border="0">
            <tr>            
            <td class="api_temp_td_2" valign="bottom" style="background-image:url('.$config_data_function['image_path'].'/'.$select_data[$i]['image'].');">
                <div class="api_temp_number">
                    '.str_pad('0',2,$i + 1).'
                </div>
            </td>
            </tr>
            <tr>
            <td class="api_temp_td_1" width="50%" valign="top">
                <div class="api_big api_temp_title_1">
                    '.$select_data[$i]['title_2'].'
                </div>
                <div class="api_height_5"></div>
                <div class="api_big api_temp_title_2">
                    '.$select_data[$i]['title'].'
                </div>             
                <div class="api_height_10"></div>
                <div class="api_temp_title_3">
                    '.$select_data[$i]['description'].'
                </div>                           
            </td>         
            </tr>
            </table>   
        ';

        if ($i < count($select_data) - 1)
            $temp_display_2 .= $config_data_function['seperator'];

    }

    $return['display'] = '
        <div class="col-md-12 api_padding_0 api_view_as_blog_step">
            <div class="col-md-12 hidden-sm hidden-xs">
                '.$temp_display_1.'
            </div>
            <div class="col-md-12 visible-sm visible-xs api_kh_padding">
                '.$temp_display_2.'
            </div>
        </div>
    ';
    return $return;
} 

public function blog_step_v2($config_data_function) {
    $return = array();    
    $l = $config_data_function['l'];
    $select_data = $config_data_function['select_data'];
    for ($i=0;$i<count($select_data);$i++) {      
        $data_view = array (
            'wrapper_class' => '',
            'file_path' => $config_data_function['image_path'].'/'.$select_data[$i]['image'],
            'max_width' => 0,
            'max_height' => 0,
            'image_class' => $config_data_function['image_class'],
            'image_id' => '',   
            'resize_type' => 'fixed',       
        );
        $temp_image = $this->api_helper->api_get_image($data_view);

        $temp_display_1 .= '      
        <div class="api_admin api_admin_wrapper_blog_step_'.$select_data[$i]['id'].'">              
            <table border="0">
            <tr>
            <td class="api_temp_td_2" width="45%" valign="top">
                '.$temp_image['image_table'].'
            </td>            
            <td class="api_temp_td_1" width="55%" valign="top"  >
                <div class="api_big api_temp_title_1">
                    <span class="'.$config_data_function['step_class'].'">Step </span>'.str_pad('0',2,$i + 1).'
                </div>
                <div class="api_height_5"></div>
                <div class="api_big api_temp_title_2">
                    '.$select_data[$i]['title'].'
                </div>                             <div class="api_height_10"></div>
                <div class="api_temp_title_3">
                    '.$select_data[$i]['description'].'
                </div>                           
            </td>
            </tr>
            </table>   
        ';

        $field_data = array();
        $field_data_add = array();
        $field_data[0] = array(
            'type' => 'translate',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'translate',
            'translate' => 'yes',
            'field_label_name' => 'Title Top',                      
        );    
        $field_data[1] = array(
            'type' => 'translate_textarea',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'description',
            'description' => 'yes',
            'field_label_name' => 'Description',                      
        );          
        $field_data[2] = array(
            'type' => 'file',
            'col_class' => 'col-md-4',
            'field_class' => '',
            'field_name' => 'image',    
            'field_label_name' => 'Image',
            'recommended_width' => 700,
            'recommended_height' => 450,
        );             
        $field_data[3] = array(
            'type' => 'text',
            'col_class' => 'col-md-4',
            'field_class' => 'api_numberic_input',
            'field_name' => 'ordering',
            'field_label_name' => 'Ordering',                      
        );
        $field_data_add[0] = array(
            'type' => 'hidden',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'c_id',
            'translate' => '',
            'field_label_name' => 'Category',   
            'field_value' => $config_data_function['category_id'],                   
        );   
        $config_data = array(
            'l' => $l,
            'wrapper_class' => 'api_admin_wrapper_blog_step_'.$select_data[$i]['id'],
            'modal_class' => 'modal-lg',
            'title' => 'Edit Feature', 
            'field_data' => $field_data,
            'selected_id' => $select_data[$i]['id'],
            'table_name' => 'sma_blog_step',
            'table_id' => 'id',
            'upload_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/blog_step',
            'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
            'field_data_add' => $field_data_add,
            'add' => 'yes',
            'title_add' => 'Add Feature',
            'delete' => 'yes',
            'title_delete' => '<div>Your will remove</div>'.$select_data[$i]['title'],
            'condition_ordering' => "and c_id = ".$config_data_function['category_id'],        
        );
        $temp_admin = $this->api_admin->front_end_edit($config_data);
        $temp_display_1 .= $temp_admin['display'];
        $temp_display_1 .= '
            </div>
        ';
        $temp_display_1 .= '
            <script>
                '.$temp_admin['script'].'
            </script>
        ';
        
        if ($i < count($select_data) - 1)
            $temp_display_1 .= $config_data_function['seperator'];

        $temp_display_2 .= '                    
            <table border="0">
            <tr>
            <td class="api_temp_td_2" width="100%" valign="bottom">
                <img class="img-responsive" src="'.$config_data_function['image_path'].'/'.$select_data[$i]['image'].'" />            
            </td>       
            </tr>
            </table>   
            <table border="0">
            <tr>                            
            <td class="api_temp_td_1" width="100%" valign="top">
                <div class="api_big api_temp_title_1">
                    Step '.str_pad('0',2,$i + 1).'
                </div>
                <div class="api_height_5"></div>
                <div class="api_big api_temp_title_2">
                    '.$select_data[$i]['title'].'
                </div>             
                <div class="api_height_10"></div>
                <div class="api_temp_title_3">
                    '.$select_data[$i]['description'].'
                </div>                           
            </td>
            </tr>
            </table>               
        ';

        if ($i < count($select_data) - 1)
            $temp_display_2 .= $config_data_function['seperator'];        
    }

    $return['display'] = '
        <div class="col-md-12 api_padding_0 api_view_as_blog_step_v2">
            <div class="col-md-12 hidden-xs">
                '.$temp_display_1.'
            </div>
            <div class="col-md-12 visible-xs api_kh_padding">
                '.$temp_display_2.'
            </div>        
        </div>
    ';

    return $return;
} 

public function blog_step_v3($config_data_function) {
    $return = array();    
    $l = $config_data_function['l'];
    $select_data = $config_data_function['select_data'];
    for ($i=0;$i<count($select_data);$i++) {
        $temp_display_1 .= '
            <div class="api_admin api_admin_wrapper_blog_step_'.$select_data[$i]['id'].'">        
                <table border="0">
                <tr>
                <td class="api_temp_td_1" valign="top">
                    <div class="api_big api_temp_title_1">
                        '.$select_data[$i]['title'].'
                    </div>
                    <div class="api_big api_temp_title_1_underline">
                    </div>                
                    <div class="api_height_5"></div>         
                    <div class="api_height_10"></div>
                    <div class="api_temp_title_3">
                        '.$select_data[$i]['description'].'
                    </div>                           
                </td>
                <td class="api_temp_td_3">&nbsp;</td>
                <td class="api_temp_td_2" align="center" valign="bottom" style="background-image:url('.$config_data_function['image_path'].'/'.$select_data[$i]['image'].');">
                    <div class="api_temp_title_2_wrapper">
                        <div class="api_temp_title_2">
                            '.$select_data[$i]['title_2'].'
                        </div>
                    </div>
                </td>
                </tr>
                </table>
        ';
        
        $field_data = array();
        $field_data_add = array();
        $field_data[0] = array(
            'type' => 'translate',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'translate',
            'translate' => 'yes',
            'field_label_name' => 'Title',                      
        );    
        $field_data[1] = array(
            'type' => 'translate',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'translate_2',
            'translate_2' => 'yes',
            'field_label_name' => 'Title Over Image',                      
        );    
        $field_data[2] = array(
            'type' => 'translate_textarea',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'description',
            'description' => 'yes',
            'field_label_name' => 'Description',                      
        );          
        $field_data[3] = array(
            'type' => 'file',
            'col_class' => 'col-md-4',
            'field_class' => '',
            'field_name' => 'image',    
            'field_label_name' => 'Image',
            'recommended_width' => 700,
            'recommended_height' => 450,
        );             
        $field_data[4] = array(
            'type' => 'text',
            'col_class' => 'col-md-4',
            'field_class' => 'api_numberic_input',
            'field_name' => 'ordering',
            'field_label_name' => 'Ordering',                      
        );
        $field_data_add[0] = array(
            'type' => 'hidden',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'c_id',
            'translate' => '',
            'field_label_name' => 'Category',   
            'field_value' => $config_data_function['category_id'],                   
        );   
        $config_data = array(
            'l' => $l,
            'wrapper_class' => 'api_admin_wrapper_blog_step_'.$select_data[$i]['id'],
            'modal_class' => 'modal-lg',
            'title' => 'Edit Feature', 
            'field_data' => $field_data,
            'selected_id' => $select_data[$i]['id'],
            'table_name' => 'sma_blog_step',
            'table_id' => 'id',
            'upload_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/blog_step',
            'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
            'field_data_add' => $field_data_add,
            'add' => 'yes',
            'title_add' => 'Add',
            'delete' => 'yes',
            'title_delete' => '<div>Your will remove</div>'.$select_data[$i]['title'],
            'condition_ordering' => "and c_id = ".$config_data_function['category_id'],        
        );
        $temp_admin = $this->api_admin->front_end_edit($config_data);
        $temp_display_1 .= $temp_admin['display'];
        $temp_display_1 .= '
            </div>
        ';
        $temp_display_1 .= '
            <script>
                '.$temp_admin['script'].'
            </script>
        ';

          
        if ($i < count($select_data) - 1)
            $temp_display_1 .= $config_data_function['seperator'];
        
        $temp_display_2 .= '
            <table border="0">
            <tr>
            <td class="api_temp_td_2" align="center" valign="bottom" style="background-image:url('.$config_data_function['image_path'].'/'.$select_data[$i]['image'].');">
                <div class="api_temp_title_2_wrapper">
                    <div class="api_temp_title_2">
                        '.$select_data[$i]['title_2'].'
                    </div>
                </div>
            </td>
            </tr>
            <tr>
            <td class="api_temp_td_1" width="50%" valign="top">
                <div class="api_big api_temp_title_1">
                    '.$select_data[$i]['title'].'
                </div>
                <div class="api_big api_temp_title_1_underline">
                </div>                
                <div class="api_height_5"></div>         
                <div class="api_height_10"></div>
                <div class="api_temp_title_3">
                    '.$select_data[$i]['description'].'
                </div>                           
            </td>
            </tr>
            </table>
        ';

        if ($i < count($select_data) - 1)
            $temp_display_2 .= $config_data_function['seperator'];

    }

    $return['display'] = '
        <div class="col-md-12 api_padding_0 api_view_as_blog_step_v3">
            <div class="col-md-12 hidden-xs">
                '.$temp_display_1.'
            </div>
            <div class="col-md-12 visible-xs api_kh_padding">
                '.$temp_display_2.'
            </div>
        </div>
    ';

    return $return;
} 

public function large_icon_v2($config_data_function) {
    $return = array();    
    $l = $config_data_function['l'];
    $select_data = $config_data_function['select_data'];
    for ($i=0;$i<count($select_data);$i++) {
        $data_view = array (
            'wrapper_class' => 'hidden-sm hidden-xs',
            'file_path' => $config_data_function['image_path'].'/'.$select_data[$i]['image'],
            'max_width' => 600,
            'max_height' => 350,
            'product_link' => '',
            'image_class' => '',
            'image_id' => '',   
            'resize_type' => 'full',       
        );
        $temp_image = $this->api_helper->api_get_image($data_view);
        $data_view = array (
            'wrapper_class' => 'visible-sm visible-xs',
            'file_path' => $config_data_function['image_path'].'/'.$select_data[$i]['image'],
            'max_width' => 700,
            'max_height' => 300,
            'product_link' => '',
            'image_class' => 'img-responsive',
            'image_id' => '',   
            'resize_type' => 'full',       
        );
        $temp_image_mobile = $this->api_helper->api_get_image($data_view);
    
        $temp_display .= '
            <div class="col-md-6">
                <div class="api_big api_temp_title">
                    '.$select_data[$i]['title'].'
                </div>
                <div class="api_temp_image">
                    '.$temp_image['image_full_src'].'
                    '.$temp_image_mobile['image_table'].'
                </div>                
                <div class="api_temp_description" id="api_temp_id_'.$i.'">
                    '.$select_data[$i]['description'].'
                </div>
            </div>
        ';
    }

    $return['display'] = '
        <div class="col-md-12 api_padding_0 api_view_as_large_icon_v2">
            <div class="col-md-12 api_temp_wrapper_1">
                '.$temp_display.'
            </div>
        </div>
    ';

    return $return;
}


}
