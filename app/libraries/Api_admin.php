<?php

class Api_admin {

public function __construct()
{

}
public function __get($var)
{
    return get_instance()->$var;
}

public function field_textarea($field_data,$config_data_function) {
    $return = array();
    if ($field_data['required'] == 'yes') {
        $temp_required = '<span class="api_color_red_2">※</span>';
        $return['script'] .= '
            if (document.api_form_admin.api_field_text_'.$field_data['field_name'].'.value == "") {
                b = 0;
                $("#api_form_admin .'.$field_data['field_name'].'_wrapper").addClass("has-error");
                $("#api_form_admin .'.$field_data['field_name'].'_error").show();    
                $("#api_modal_admin").animate({ scrollTop: 0 }, "fast");
            }
            else {
                $("#api_form_admin .'.$field_data['field_name'].'_wrapper").removeClass("has-error");
                $("#api_form_admin .'.$field_data['field_name'].'_error").hide();    
            }
        ';
    }    
    $return['display'] .= '
        <div class="'.$field_data['col_class'].' '.$field_data['field_name'].'_wrapper">
            <div class="form-group all">
                <label class="control-label" for="'.$field_data['field_label_name'].'">'.$field_data['field_label_name'].' '.$temp_required.'</label>
                <div class="api_height_5"></div> 
                <textarea id="api_field_text_'.$field_data['field_name'].'" name="api_field_text_'.$field_data['field_name'].'" style="width:100%; height:250px;"></textarea>

                <div id="api_field_text_'.$field_data['field_name'].'_div" class="api_display_none">
                    '.$field_data['field_value'].'
                </div>                
                <small style="display:none;" class="help-block '.$field_data['field_name'].'_error" data-bv-validator="notEmpty" data-bv-for="translate_en" data-bv-result="INVALID">Please enter/select a value</small>                
                <div class="api_height_5"></div>       
            </div>
        </div>
        <script>
            var temp_id = "api_field_text_'.$field_data['field_name'].'";
            var doc = new DOMParser().parseFromString($("#" + temp_id + "_div").html(), "text/html");
            var temp = doc.documentElement.textContent;
            $("#" + temp_id).val(temp);                  
        </script>
    ';               
    return $return;
}
public function field_translate_textarea($field_data,$config_data_function) {
    $return = array();
    $language_array = unserialize(multi_language);

    $temp_tab_header .= '
    <ul class="nav nav-tabs">
    ';
    for ($i=0;$i<count($language_array);$i++) {
        if ($language_array[$i][0] == $config_data_function['l'])
            $temp = 'active';
        else
            $temp = '';
        $temp_tab_header .= '
            <li class="'.$temp.'">
                <a data-toggle="tab" href="#tab_'.$field_data['field_name'].'_'.$language_array[$i][0].'">
                    '.$field_data['field_label_name'].' '.$language_array[$i][1].'
                </a>
            </li>
        ';
    }
    $temp_tab_header .= '
    </ul>
    ';

    for ($i=0;$i<count($language_array);$i++) {            
        if ($language_array[$i][0] == $config_data_function['l'])
            $temp = 'active';
        else
            $temp = '';        
        if ($field_data['required'] == 'yes' && $language_array[$i][0] == 'en') {
            $temp_required = '<span class="api_color_red_2">※</span>';
            $return['script'] .= '
                if (document.api_form_admin.api_field_'.$field_data['field_name'].'_'.$language_array[$i][0].'.value == "") {
                    b = 0;
                    $("#api_form_admin .'.$field_data['field_name'].'_'.$language_array[$i][0].'_wrapper").addClass("has-error");
                    $("#api_form_admin .'.$field_data['field_name'].'_'.$language_array[$i][0].'_error").show();    
                    $("#api_modal_admin").animate({ scrollTop: 0 }, "fast");
                }
                else {
                    $("#api_form_admin .'.$field_data['field_name'].'_'.$language_array[$i][0].'_wrapper").removeClass("has-error");
                    $("#api_form_admin .'.$field_data['field_name'].'_'.$language_array[$i][0].'_error").hide();    
                }
            ';
        }     
        $return['display'] .= '
        <div id="tab_'.$field_data['field_name'].'_'.$language_array[$i][0].'" class="tab-pane fade in '.$temp.'">
            <div class="'.$field_data['field_name'].'_'.$language_array[$i][0].'_wrapper">            
                <div class="form-group all">
                    <textarea class="form-control" id="'.$config_data_function['wrapper_class'].'_'.$field_data['field_name'].'_'.$language_array[$i][0].'" name="api_field_'.$field_data['field_name'].'_'.$language_array[$i][0].'" style="height:250px;">'.$field_data['field_value_'.$language_array[$i][0]].'</textarea>
                    <div id="'.$config_data_function['wrapper_class'].'_'.$field_data['field_name'].'_'.$language_array[$i][0].'_div" class="api_display_none">
                        '.$field_data['field_value_'.$language_array[$i][0]].'
                    </div>

                    <small style="display:none;" class="help-block '.$field_data['field_name'].'_'.$language_array[$i][0].'_error" data-bv-validator="notEmpty" data-bv-for="translate_en" data-bv-result="INVALID">Please enter/select a value</small>
                    <div class="api_height_5"></div>       
                </div>
                
            </div>
            <script>
                // var temp_id = "'.$config_data_function['wrapper_class'].'_'.$field_data['field_name'].'_'.$language_array[$i][0].'";
                // var doc = new DOMParser().parseFromString($("#" + temp_id + "_div").html(), "text/html");
                // var temp = doc.documentElement.textContent;
                // $("#" + temp_id).val(temp);
                
                ClassicEditor.create( 
                    document.querySelector( "#'.$config_data_function['wrapper_class'].'_'.$field_data['field_name'].'_'.$language_array[$i][0].'" ), {
                        extraPlugins: [ MyCustomUploadAdapterPlugin ],
                } )
                .then( editor => {
                    window.editor = editor;
                } )
                .catch( err => {
                    console.error( err.stack );
                } );              
                
                // CKEDITOR.replace("'.$config_data_function['wrapper_class'].'_'.$field_data['field_name'].'_'.$language_array[$i][0].'");

            </script>


        </div>
        ';                   
    }

    $return['display'] = '
    <div class="col-md-12">
        '.$temp_tab_header.'        
        <div class="tab-content">
            '.$return['display'].'
        </div>
    </div>
    ';

    return $return;
}
public function field_translate($field_data,$config_data_function) {
    $return = array();
    $language_array = unserialize(multi_language);

    $temp_tab_header .= '
    <ul class="nav nav-tabs">
    ';
    for ($i=0;$i<count($language_array);$i++) {
        if ($language_array[$i][0] == $config_data_function['l'])
            $temp = 'active';
        else
            $temp = '';
        $temp_tab_header .= '
            <li class="'.$temp.'">
                <a data-toggle="tab" href="#tab_'.$field_data['field_name'].'_'.$language_array[$i][0].'">
                    '.$field_data['field_label_name'].' '.$language_array[$i][1].'
                </a>
            </li>
        ';
    }
    $temp_tab_header .= '
    </ul>
    ';

    for ($i=0;$i<count($language_array);$i++) {            
        if ($language_array[$i][0] == $config_data_function['l'])
            $temp = 'active';
        else
            $temp = '';        
        if ($field_data['required'] == 'yes' && $language_array[$i][0] == 'en') {
            $temp_required = '<span class="api_color_red_2">※</span>';
            $return['script'] .= '
                if (document.api_form_admin.api_field_'.$field_data['field_name'].'_'.$language_array[$i][0].'.value == "") {
                    b = 0;
                    $("#api_form_admin .'.$field_data['field_name'].'_'.$language_array[$i][0].'_wrapper").addClass("has-error");
                    $("#api_form_admin .'.$field_data['field_name'].'_'.$language_array[$i][0].'_error").show();    
                    $("#api_modal_admin").animate({ scrollTop: 0 }, "fast");
                }
                else {
                    $("#api_form_admin .'.$field_data['field_name'].'_'.$language_array[$i][0].'_wrapper").removeClass("has-error");
                    $("#api_form_admin .'.$field_data['field_name'].'_'.$language_array[$i][0].'_error").hide();    
                }
            ';
        }     
        $return['display'] .= '
        <div id="tab_'.$field_data['field_name'].'_'.$language_array[$i][0].'" class="tab-pane fade in '.$temp.'">
            <div class="'.$field_data['field_name'].'_'.$language_array[$i][0].'_wrapper">            
                <div class="form-group all">
                    '.form_input('api_field_'.$field_data['field_name'].'_'.$language_array[$i][0], $field_data['field_value_'.$language_array[$i][0]], 'class="form-control" placeholder="'.$field_data['field_label_name'].' '.$language_array[$i][1].'"').'                    
                    <small style="display:none;" class="help-block '.$field_data['field_name'].'_'.$language_array[$i][0].'_error" data-bv-validator="notEmpty" data-bv-for="translate_en" data-bv-result="INVALID">Please enter/select a value</small>
                    <div class="api_height_5"></div>       
                </div>
            </div>
        </div>
        ';                   
    }

    $return['display'] = '
    <div class="col-md-12">
        '.$temp_tab_header.'        
        <div class="tab-content">
            '.$return['display'].'
        </div>
    </div>
    ';

    return $return;
}
public function field_textarea_code($field_data,$config_data_function) {
    $return = array();
    $l = $config_data_function['l'];
    if ($field_data['required'] == 'yes') {
        $temp_required = '<span class="api_color_red_2">※</span>';
        $return['script'] .= '
            if (document.api_form_admin.api_field_translate_'.$field_data['field_name'].'.value == "") {
                b = 0;
                $("#api_form_admin .'.$field_data['field_name'].'_wrapper").addClass("has-error");
                $("#api_form_admin .'.$field_data['field_name'].'_error").show();    
                $("#api_modal_admin").animate({ scrollTop: 0 }, "fast");
            }
            else {
                $("#api_form_admin .'.$field_data['field_name'].'_wrapper").removeClass("has-error");
                $("#api_form_admin .'.$field_data['field_name'].'_error").hide();    
            }
        ';
    }
    $return['display'] .= '
        <div class="'.$field_data['col_class'].' '.$field_data['field_name'].'_wrapper">
            <div class="form-group '.$field_data['field_name'].'_wrapper">
                <label class="control-label" for="'.$field_data['field_label_name'].'">'.$field_data['field_label_name'].' '.$temp_required.'</label>
                <div class="api_height_5"></div>
                <textarea id="api_field_translate_'.$field_data['field_name'].'" name="api_field_translate_'.$field_data['field_name'].'" style="width:100%; height:250px;">'.$field_data['field_value_'.$l].'</textarea>
                <small style="display:none;" class="help-block '.$field_data['field_name'].'_error" data-bv-validator="notEmpty" data-bv-for="translate_en" data-bv-result="INVALID">Please enter/select a value</small>
                <div class="api_height_5"></div>
            </div>                    
        </div>
    ';
    return $return;
}
public function field_text($field_data,$config_data_function) {
    $return = array();
    if ($field_data['required'] == 'yes') {
        $temp_required = '<span class="api_color_red_2">※</span>';
        $return['script'] .= '
            if (document.api_form_admin.api_field_text_'.$field_data['field_name'].'.value == "") {
                b = 0;
                $("#api_form_admin .'.$field_data['field_name'].'_wrapper").addClass("has-error");
                $("#api_form_admin .'.$field_data['field_name'].'_error").show();    
                $("#api_modal_admin").animate({ scrollTop: 0 }, "fast");
            }
            else {
                $("#api_form_admin .'.$field_data['field_name'].'_wrapper").removeClass("has-error");
                $("#api_form_admin .'.$field_data['field_name'].'_error").hide();    
            }
        ';
    }   

    //-set max ordering + 1 when adding-------------
    if ($config_data_function['add'] == 'yes' && $field_data['field_name'] == 'ordering') {
        $config_data = array(
            'table_name' => $config_data_function['table_name'],
            'select_table' => $config_data_function['table_name'],
            'translate' => '',
            'description' => '',
            'select_condition' => $config_data_function['table_id']." > 0 ".$config_data_function['condition_ordering']." order by ordering desc limit 1",
        );
        $temp = $this->api_helper->api_select_data_v2($config_data);
        $field_data['field_value'] = $temp[0]['ordering'] + 1;
    }
    //-set max ordering + 1 when adding-------------

    $return['display'] .= '
        <div class="'.$field_data['col_class'].' '.$field_data['field_name'].'_wrapper">
            <div class="form-group">
                <label class="control-label" for="'.$field_data['field_label_name'].'">'.$field_data['field_label_name'].' '.$temp_required.'</label>
                <div class="api_height_5"></div>
                '.form_input('api_field_text_'.$field_data['field_name'], $field_data['field_value'], 'class="form-control '.$field_data['field_class'].'" ').'
                <small style="display:none;" class="help-block '.$field_data['field_name'].'_error" data-bv-validator="notEmpty" data-bv-for="translate_en" data-bv-result="INVALID">Please enter/select a value</small>
                <div class="api_height_5"></div>                      
            </div>                    
        </div>
    ';              
    return $return;
}
public function field_add_ons($field_data,$config_data_function) {
    $return = array();
    if ($field_data['required'] == 'yes') {
        $temp_required = '<span class="api_color_red_2">※</span>';
        $return['script'] .= '
            if (document.api_form_admin.api_field_add_ons_'.$field_data['field_name'].'.value == "") {
                b = 0;
                $("#api_form_admin .'.$field_data['field_name'].'_wrapper").addClass("has-error");
                $("#api_form_admin .'.$field_data['field_name'].'_error").show();    
                $("#api_modal_admin").animate({ scrollTop: 0 }, "fast");
            }
            else {
                $("#api_form_admin .'.$field_data['field_name'].'_wrapper").removeClass("has-error");
                $("#api_form_admin .'.$field_data['field_name'].'_error").hide();    
            }
        ';
    }   

    //-set max ordering + 1 when adding-------------
    if ($config_data_function['add'] == 'yes' && $field_data['field_name'] == 'ordering') {
        $config_data = array(
            'table_name' => $config_data_function['table_name'],
            'select_table' => $config_data_function['table_name'],
            'translate' => '',
            'description' => '',
            'select_condition' => $config_data_function['table_id']." > 0 ".$config_data_function['condition_ordering']." order by ordering desc limit 1",
        );
        $temp = $this->api_helper->api_select_data_v2($config_data);
        $field_data['field_value'] = $temp[0]['ordering'] + 1;
    }
    //-set max ordering + 1 when adding-------------

    $return['display'] .= '
        <div class="'.$field_data['col_class'].' '.$field_data['field_name'].'_wrapper">
            <div class="form-group">
                <label class="control-label" for="'.$field_data['field_label_name'].'">'.$field_data['field_label_name'].' '.$temp_required.'</label>
                <div class="api_height_5"></div>
                '.form_input('api_field_add_ons_'.$field_data['field_name'], $field_data['field_value'], 'class="form-control '.$field_data['field_class'].'" ').'
                <small style="display:none;" class="help-block '.$field_data['field_name'].'_error" data-bv-validator="notEmpty" data-bv-for="translate_en" data-bv-result="INVALID">Please enter/select a value</small>
                <div class="api_height_5"></div>                      
            </div>                    
        </div>
    ';              
    return $return;
}

public function field_add_ons_checkbox_yes_no($field_data,$config_data_function) {
    $return = array();
    if ($field_data['required'] == 'yes') {
        $temp_required = '<span class="api_color_red_2">※</span>';
        $return['script'] .= '
            if (document.api_form_admin.api_field_add_ons_'.$field_data['field_name'].'.value == "") {
                b = 0;
                $("#api_form_admin .'.$field_data['field_name'].'_wrapper").addClass("has-error");
                $("#api_form_admin .'.$field_data['field_name'].'_error").show();    
                $("#api_modal_admin").animate({ scrollTop: 0 }, "fast");
            }
            else {
                $("#api_form_admin .'.$field_data['field_name'].'_wrapper").removeClass("has-error");
                $("#api_form_admin .'.$field_data['field_name'].'_error").hide();    
            }
        ';
    }   

    //-set max ordering + 1 when adding-------------
    if ($config_data_function['add'] == 'yes' && $field_data['field_name'] == 'ordering') {
        $config_data = array(
            'table_name' => $config_data_function['table_name'],
            'select_table' => $config_data_function['table_name'],
            'translate' => '',
            'description' => '',
            'select_condition' => $config_data_function['table_id']." > 0 ".$config_data_function['condition_ordering']." order by ordering desc limit 1",
        );
        $temp = $this->api_helper->api_select_data_v2($config_data);
        $field_data['field_value'] = $temp[0]['ordering'] + 1;
    }
    //-set max ordering + 1 when adding-------------

    if ($field_data['field_value'] == 'yes')
        $temp_checked = 'checked="checked"';
    else
        $temp_checked = '';

    $return['display'] .= '
        <div class="'.$field_data['col_class'].' '.$field_data['field_name'].'_wrapper">
            <div class="form-group">
            <div class="api_height_5"></div>
            <label class="control-label" for="'.$field_data['field_label_name'].'">&nbsp; '.$temp_required.'</label>
            <div class="api_height_5"></div>
            <input name="api_field_add_ons_'.$field_data['field_name'].'" type="checkbox" class="api_checkbox" value="yes" '.$temp_checked.'/>
            <label for="'.$field_data['field_label_name'].'" class="padding05 api_padding_left_5">
                '.$field_data['field_label_name'].'
            </label>                
            <small style="display:none;" class="help-block '.$field_data['field_name'].'_error" data-bv-validator="notEmpty" data-bv-for="translate_en" data-bv-result="INVALID">Please enter/select a value</small>
            <div class="api_height_5"></div>                      
            </div>                    
        </div>
    ';              
    return $return;
}
public function field_hidden($field_data,$config_data_function) {
    $return = array();
    $return['display'] .= '
        <div class="'.$field_data['col_class'].' api_display_none">
            <div class="form-group">
                <label class="control-label" for="'.$conffield_dataig_data_function['field_label_name'].'">'.$field_data['field_label_name'].'</label>
                <div class="api_height_5"></div>
                '.form_input('api_field_text_'.$field_data['field_name'], $field_data['field_value'], 'class="form-control" ').'
                <small style="display:none;" class="help-block '.$field_data['field_name'].'_error" data-bv-validator="notEmpty" data-bv-for="translate_en" data-bv-result="INVALID">Please enter/select a value</small>
                <div class="api_height_5"></div>                      
            </div>                    
        </div>
    ';         
    return $return;
}
public function field_date($field_data,$config_data_function) {
    $return = array();
    if ($field_data['required'] == 'yes') {
        $temp_required = '<span class="api_color_red_2">※</span>';
        $return['script'] .= '
            if (document.api_form_admin.api_field_text_'.$field_data['field_name'].'.value == "") {
                b = 0;
                $("#api_form_admin .'.$field_data['field_name'].'_wrapper").addClass("has-error");
                $("#api_form_admin .'.$field_data['field_name'].'_error").show();    
                $("#api_modal_admin").animate({ scrollTop: 0 }, "fast");
            }
            else {
                $("#api_form_admin .'.$field_data['field_name'].'_wrapper").removeClass("has-error");
                $("#api_form_admin .'.$field_data['field_name'].'_error").hide();    
            }
        ';
    } 

    if ($field_data['field_value'] == '')
        $temp = date_create(date('Y-m-d H:i:s'));
    else
        $temp = date_create($field_data['field_value']);

    $return['display'] .= '
        <div class="'.$field_data['col_class'].' '.$field_data['field_name'].'_wrapper">
            <div class="form-group">
                <label class="control-label" for="'.$conffield_dataig_data_function['field_label_name'].'">'.$field_data['field_label_name'].' '.$temp_required.'</label>
                <div class="api_height_5"></div>
                '.form_input('api_field_text_'.$field_data['field_name'], date_format($temp,'Y-m-d h:i:s'), 'id="api_field_text_'.$field_data['field_name'].'" class="form-control" autocomplete="off"').'
                <small style="display:none;" class="help-block '.$field_data['field_name'].'_error" data-bv-validator="notEmpty" data-bv-for="translate_en" data-bv-result="INVALID">Please enter/select a value</small>
                <div class="api_height_5"></div>              
            </div>
        </div>
        <script>
            $("#api_field_text_'.$field_data['field_name'].'").datetimepicker({
                format: "YYYY-MM-DD hh:mm:ss",
                viewMode: "months",
                toolbarPlacement: "top",
                allowInputToggle: true,
                icons: {
                    time: "fa fa-time",
                    date: "fa fa-calendar",
                    up: "fa fa-chevron-up",
                    down: "fa fa-chevron-down",
                    previous: "fa fa-chevron-left",
                    next: "fa fa-chevron-right",
                    today: "fa fa-screenshot",
                    clear: "fa fa-trash",
                    close: "fa fa-remove"
                }
            });
        </script>      
    ';              
    return $return;
}
public function field_select($field_data,$config_data_function) {
    $return = array();
    if ($field_data['required'] == 'yes') {
        $temp_required = '<span class="api_color_red_2">※</span>';
        $return['script'] .= '
            if (document.api_form_admin.api_field_text_'.$field_data['field_name'].'.value == "") {
                b = 0;
                $("#api_form_admin .'.$field_data['field_name'].'_wrapper").addClass("has-error");
                $("#api_form_admin .'.$field_data['field_name'].'_error").show();    
                $("#api_modal_admin").animate({ scrollTop: 0 }, "fast");
            }
            else {
                $("#api_form_admin .'.$field_data['field_name'].'_wrapper").removeClass("has-error");
                $("#api_form_admin .'.$field_data['field_name'].'_error").hide();    
            }
        ';
    }     
    $return['display'] .= '
        <div class="'.$field_data['col_class'].' '.$field_data['field_name'].'_wrapper">
            <div class="form-group">
                <label class="control-label" for="'.$field_data['field_label_name'].'">'.$field_data['field_label_name'].' '.$temp_required.'</label>
                <div class="api_height_5"></div>
    ';
                $config_data = array(
                    'l' => $config_data_function['l'],
                    'condition' => $field_data['select_field_condition'],
                    'field_name' => 'api_field_text_'.$field_data['field_name'],
                    'field_value' => $field_data['field_value'],
                );
                $temp = $this->api_select_field->blog_category($config_data);
                $return['display'] .= $temp['display'];

    $return['display'] .= '
                <small style="display:none;" class="help-block '.$field_data['field_name'].'_error" data-bv-validator="notEmpty" data-bv-for="translate_en" data-bv-result="INVALID">Please enter/select a value</small>
                <div class="api_height_5"></div>                      
            </div>                    
        </div>
    ';           
    return $return;
}

public function field_checkbox_list($field_data,$config_data_function) {
    $return = array();
    if ($field_data['required'] == 'yes') {
        $temp_required = '<span class="api_color_red_2">※</span>';
        $return['script'] .= '
            if (document.api_form_admin.api_field_text_'.$field_data['field_name'].'.value == "") {
                b = 0;
                $("#api_form_admin .'.$field_data['field_name'].'_wrapper").addClass("has-error");
                $("#api_form_admin .'.$field_data['field_name'].'_error").show();    
                $("#api_modal_admin").animate({ scrollTop: 0 }, "fast");
            }
            else {
                $("#api_form_admin .'.$field_data['field_name'].'_wrapper").removeClass("has-error");
                $("#api_form_admin .'.$field_data['field_name'].'_error").hide();    
            }
        ';
    }     
    $return['display'] .= '
        <div class="'.$field_data['col_class'].' '.$field_data['field_name'].'_wrapper">
            <div class="form-group">
                <label class="control-label" for="'.$field_data['field_label_name'].'">'.$field_data['field_label_name'].' '.$temp_required.'</label>
                <div class="api_height_5"></div>
    ';
                $config_data = array(
                    'l' => $config_data_function['l'],
                    'condition' => $field_data['select_field_condition'],
                    'field_name' => 'api_field_text_'.$field_data['field_name'],
                    'field_value' => $field_data['field_value'],
                );
                $temp = $this->api_select_field->checkbox_list_blog_tag($config_data);
                $return['display'] .= $temp['display'];
                
    $return['display'] .= '
                <small style="display:none;" class="help-block '.$field_data['field_name'].'_error" data-bv-validator="notEmpty" data-bv-for="translate_en" data-bv-result="INVALID">Please enter/select a value</small>    
                <div class="api_height_5"></div>                      
            </div>                    
        </div>
    ';         
    return $return;
}

public function field_file($field_data,$config_data_function) {
    $return = array();
    if ($field_data['required'] == 'yes') {
        $temp_required = '<span class="api_color_red_2">※</span>';
        $return['script'] .= '
            if (document.api_form_admin.api_field_file_'.$field_data['field_name'].'.value == "") {
                b = 0;
                $("#api_form_admin .'.$field_data['field_name'].'_wrapper").addClass("has-error");
                $("#api_form_admin .'.$field_data['field_name'].'_error").show();    
                $("#api_modal_admin").animate({ scrollTop: 0 }, "fast");
            }
            else {
                $("#api_form_admin .'.$field_data['field_name'].'_wrapper").removeClass("has-error");
                $("#api_form_admin .'.$field_data['field_name'].'_error").hide();    
            }
        ';
    } 

    $data_view = array (
        'wrapper_class' => 'api_display_inline_block',
        'file_path' => $config_data_function['upload_path'].'/'.$field_data['field_value'],
        'max_width' => 100,
        'max_height' => 100,
        'image_class' => 'img-responsive',
        'image_id' => '',   
        'resize_type' => 'full',       
    );
    $temp_image = $this->api_helper->api_get_image($data_view);    
    $return['display'] .= '
        <div class="'.$field_data['col_class'].' '.$field_data['field_name'].'_wrapper">
            <div class="form-group">
                <input type="hidden" name="api_field_thumb_width_'.$field_data['field_name'].'"  value="'.$field_data['recommended_width'].'" />
                <input type="hidden" name="api_field_thumb_height_'.$field_data['field_name'].'"  value="'.$field_data['recommended_height'].'" />
                <label class="control-label" for="'.$field_data['field_label_name'].'">'.$field_data['field_label_name'].' '.$temp_required.'</label>
                <div class="api_height_5"></div>
                '.$temp_image['image_table'].'
                <input type="hidden" name="api_field_file_'.$field_data['field_name'].'" id="api_field_file_'.$field_data['field_name'].'" value="" />
                <input name="'.$field_data['field_name'].'" id="'.$field_data['field_name'].'" type="file" value="" onchange="$(\'#api_field_file_'.$field_data['field_name'].'\').val(this.value);" />
                <small style="display:none;" class="help-block '.$field_data['field_name'].'_error" data-bv-validator="notEmpty" data-bv-for="translate_en" data-bv-result="INVALID">Please enter/select a value</small>                
                <div class="api_height_5"></div>                      
            </div>                    
        </div>
        <div class="'.$field_data['col_class'].'">
            <table width="100%" border="0" >
            <tr>
            <td valign="middle" align="center" height="150">
                Recommended image size<br>
                '.$field_data['recommended_width'].' x
                '.$field_data['recommended_height'].' pixels
            </td>
            </tr>
            </table>        
        </div>
    ';         
    if ($field_data['col_class'] == 'col-md-6')
        $return['display'] .= '<div class="api_clear_both"></div>';
    return $return;
}
public function front_end_edit($config_data_function){
    $return = array();
    $l = $config_data_function['l'];
    $config_data_function['condition_ordering'] = trim($config_data_function['condition_ordering']);
    if ($this->session->userdata('group_id') == 1 && $this->api_shop_setting[0]['admin_mode'] == 'yes') {
        $temp_name = $config_data_function['wrapper_class'];
        $return['display'] .= '
            <div class="api_admin_edit_wrapper api_admin_'.$temp_name.' api_width_100_per_cent" style="display:none;">
                <table width="100%" border="0">
                <tr>
                <td valign="middle" align="center" class="api_temp_td_1">
        ';

                $temp_right_add = '0px;';
                $temp_right_edit = '0px;';
                if ($config_data_function['add'] == 'yes' && $config_data_function['delete'] == 'yes') {
                    $temp_right_add = '60px;';
                    $temp_right_edit = '30px;';
                }
                if ($config_data_function['add'] == 'yes' && $config_data_function['delete'] != 'yes') {
                    $temp_right_add = '30px;';
                    $temp_right_edit = '0px;';
                }
                if ($config_data_function['add'] != 'yes' && $config_data_function['delete'] == 'yes') {
                    $temp_right_add = '60px;';
                    $temp_right_edit = '30px;';
                }


                if ($config_data_function['delete'] == 'yes') {
                    $return['display'] .= '
                        <div class="api_big btn btn-danger api_admin_button_edit api_admin_button_delete" onclick="
                        var postData = {
                            \'message\' : \''.$config_data_function['title_delete'].'\',
                            \'table_name\' : \''.$config_data_function['table_name'].'\',
                            \'table_id\' : \''.$config_data_function['table_id'].'\',
                            \'selected_id\' : \''.$config_data_function['selected_id'].'\',
                            \'redirect\' : \''.$config_data_function['redirect'].'\',                            
                            \'upload_path\' : \''.$config_data_function['upload_path'].'\',
                        }
                        api_admin_delete(\'\',\'\',\'\',\'\',postData);
                        " title="Delete">
                            <li class="fa fa-trash"></li>
                        </div>   
                    ';
                }
                $field_data = $config_data_function['field_data'];
                $return['display'] .= '
                    <div class="api_big btn btn-info api_admin_button_edit" onclick="var postData = {
    \'wrapper_class\' : \''.$config_data_function['wrapper_class'].'\',
    \'l\' : \''.$config_data_function['l'].'\',
    \'title\' : \''.$config_data_function['title'].'\',
    \'selected_id\' : \''.$config_data_function['selected_id'].'\',
    \'table_name\' : \''.$config_data_function['table_name'].'\',
    \'table_id\' : \''.$config_data_function['table_id'].'\',
    \'upload_path\' : \''.$config_data_function['upload_path'].'\',
    \'redirect\' : \''.$config_data_function['redirect'].'\',
    \'condition_ordering\' : \''.$config_data_function['condition_ordering'].'\',
    \'generate_translate\' : \''.$config_data_function['generate_translate'].'\',
    \'generate_slug\' : \''.$config_data_function['generate_slug'].'\',
    \'datepicker\' : \''.$config_data_function['datepicker'].'\',
';

for ($i=0;$i<count($field_data);$i++) {
    foreach (array_keys($field_data[$i]) as $key) {
        if ($key != 'field_value')
$return['display'] .= '\'api_field_data_'.$i.'_'.$key.'\' : \''.$field_data[$i][$key].'\',
';    
    }
}
$return['display'] .= '}
api_ajax_admin_edit(postData);                    
                    " title="Edit" style="right:'.$temp_right_edit.'">
                        <li class="fa fa-edit"></li>
                    </div>    
                ';
                if ($config_data_function['add'] == 'yes') {
                    $return['display'] .= '
                        <div class="api_big btn btn-success api_admin_button_edit api_admin_button_add" onclick="
var postData = {
    \'wrapper_class\' : \''.$config_data_function['wrapper_class'].'\',
    \'l\' : \''.$config_data_function['l'].'\',
    \'title\' : \''.$config_data_function['title_add'].'\',
    \'selected_id\' : \''.$config_data_function['selected_id'].'\',
    \'table_name\' : \''.$config_data_function['table_name'].'\',
    \'table_id\' : \''.$config_data_function['table_id'].'\',
    \'upload_path\' : \''.$config_data_function['upload_path'].'\',
    \'redirect\' : \''.$config_data_function['redirect'].'\',
    \'condition_ordering\' : \''.$config_data_function['condition_ordering'].'\',
    \'generate_translate\' : \''.$config_data_function['generate_translate'].'\',
    \'generate_slug\' : \''.$config_data_function['generate_slug'].'\',
    \'add\' : \''.$config_data_function['add'].'\',
    \'datepicker\' : \''.$config_data_function['datepicker'].'\',
';

for ($i=0;$i<count($field_data);$i++) {
    foreach (array_keys($field_data[$i]) as $key) {
        if ($key != 'field_value')
$return['display'] .= '\'api_field_data_'.$i.'_'.$key.'\' : \''.$field_data[$i][$key].'\',
';      
    }
}

$field_data_add = $config_data_function['field_data_add'];
for ($i=0;$i<count($field_data_add);$i++) {
    foreach (array_keys($field_data_add[$i]) as $key) {
$return['display'] .= '\'api_field_add_data_'.$i.'_'.$key.'\' : \''.$field_data_add[$i][$key].'\',
';      
    }
}

$return['display'] .= '
}
api_ajax_admin_edit(postData);
                        " title="Add" style="right:'.$temp_right_add.'">
                            <li class="fa fa-plus"></li>
                        </div>   
                    ';
                }
        $return['display'] .= '
                </td>
                </tr>
                </table>
            </div>
        ';  

        if ($this->api_shop_setting[0]['admin_mode_hide'] != 'yes')
            $temp = '$(".api_admin_edit_wrapper").show();';
        else
            $temp = '$(".api_admin_edit_wrapper").hide();';    
        $return['script'] .= '
            var h = $(".'.$temp_name.'").outerHeight();
            $(".api_admin_'.$temp_name.'").css("height",h);
            $(".api_admin_'.$temp_name.' .api_temp_td_1").css("height",h);
            $(".api_admin_'.$temp_name.'").mouseover
            $(".api_admin_'.$temp_name.'").mouseover(function() { 
                $(".api_admin_'.$temp_name.' .api_admin_button_edit").show();
            });
            $(".api_admin_'.$temp_name.'").mouseout(function() { 
                $(".api_admin_'.$temp_name.' .api_admin_button_edit").hide();
            });    
            '.$temp.'
        ';        
    }

    return $return;
}



}
