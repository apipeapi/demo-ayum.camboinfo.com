<?php defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('/admin/login');
        }

        if ($this->Customer || $this->Supplier) {
            $this->ion_auth->logout();
            redirect('/admin/login');
        }

        $this->load->library('form_validation');
        $this->load->admin_model('db_model');
    }

    public function index()
    {
        if ($this->Settings->version == '2.3') {
            $this->session->set_flashdata('warning', 'Please complete your update by synchronizing your database.');
            admin_redirect('sync');
        }

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['sales'] = $this->db_model->getLatestSales();
        $this->data['quotes'] = $this->db_model->getLastestQuotes();
        $this->data['purchases'] = $this->db_model->getLatestPurchases();
        $this->data['transfers'] = $this->db_model->getLatestTransfers();
        $this->data['customers'] = $this->db_model->getLatestCustomers();
        $this->data['suppliers'] = $this->db_model->getLatestSuppliers();
        $this->data['chatData'] = $this->db_model->getChartData();
        $this->data['stock'] = $this->db_model->getStockValue();
        $this->data['bs'] = $this->db_model->getBestSeller();
        $lmsdate = date('Y-m-d', strtotime('first day of last month')) . ' 00:00:00';
        $lmedate = date('Y-m-d', strtotime('last day of last month')) . ' 23:59:59';
        $this->data['lmbs'] = $this->db_model->getBestSeller($lmsdate, $lmedate);
        $bc = array(array('link' => '#', 'page' => lang('dashboard')));
        $meta = array('page_title' => lang('dashboard'), 'bc' => $bc);
        // echo json_encode($this->data);
        // var_dump($this->data);
        // die();
        $this->page_construct('dashboard', $meta, $this->data);
    }

    public function promotions()
    {
        $this->load->view($this->theme . 'promotions', $this->data);
    }

    public function image_upload()
    {
        if (DEMO) {
            $error = array('error' => $this->lang->line('disabled_in_demo'));
            $this->sma->send_json($error);
            exit;
        }
        $this->security->csrf_verify();
        if (isset($_FILES['file'])) {
            $this->load->library('upload');
            $config['upload_path'] = 'assets/uploads/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '500';
            $config['max_width'] = $this->Settings->iwidth;
            $config['max_height'] = $this->Settings->iheight;
            $config['encrypt_name'] = true;
            $config['overwrite'] = false;
            $config['max_filename'] = 25;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('file')) {
                $error = $this->upload->display_errors();
                $error = array('error' => $error);
                $this->sma->send_json($error);
                exit;
            }
            $photo = $this->upload->file_name;
            $array = array(
                'filelink' => base_url() . 'assets/uploads/images/' . $photo
            );
            echo stripslashes(json_encode($array));
            exit;
        } else {
            $error = array('error' => 'No file selected to upload!');
            $this->sma->send_json($error);
            exit;
        }
    }

    public function set_data($ud, $value)
    {
        $this->session->set_userdata($ud, $value);
        echo true;
    }

    public function hideNotification($id = null)
    {
        $this->session->set_userdata('hidden' . $id, 1);
        echo true;
    }

    public function language($lang = false)
    {
        if ($this->input->get('lang')) {
            $lang = $this->input->get('lang');
        }
        //$this->load->helper('cookie');
        $folder = 'app/language/';
        $languagefiles = scandir($folder);
        if (in_array($lang, $languagefiles)) {
            $cookie = array(
                'name' => 'language',
                'value' => $lang,
                'expire' => '31536000',
                'prefix' => 'sma_',
                'secure' => false
            );
            $this->input->set_cookie($cookie);
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function toggle_rtl()
    {
        $cookie = array(
            'name' => 'rtl_support',
            'value' => $this->Settings->user_rtl == 1 ? 0 : 1,
            'expire' => '31536000',
            'prefix' => 'sma_',
            'secure' => false
        );
        $this->input->set_cookie($cookie);
        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function download($file)
    {
        if (file_exists('./files/'.$file)) {
            $this->load->helper('download');
            force_download('./files/'.$file, null);
            exit();
        }
        $this->session->set_flashdata('error', lang('file_x_exist'));
        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function slug()
    {
        echo $this->sma->slug($this->input->get('title', true), $this->input->get('type', true));
        exit();
    }

public function api_ajax_update_display() {
    if ($this->Owner || $this->Admin) {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }
        if ($field_value > 0) {
            $config_data = array(
                'table_name' => $table_name,
                'select_table' => $table_name,
                'select_condition' => $field_id." = ".$field_value,
            );
            $select_data = $this->site->api_select_data_v2($config_data);

            if (is_int(strpos($update_field_name,"add_ons_")))
                $temp_update_field_name = str_replace('add_ons_','',$update_field_name);
            else
                $temp_update_field_name = $update_field_name;

            $temp_value = '';
            if ($display_type == 'yes_no') {
                if ($select_data[0][$temp_update_field_name] != 'yes') {
                    $temp_value = 'yes';
                    $temp_label_class = 'label label-success';
                    $temp_label = 'Yes';
                    $temp_icon = 'fa fa-check';
                }
                else {
                    $temp_value = 'no';
                    $temp_label_class = 'label label-danger';
                    $temp_label = 'No';
                    $temp_icon = 'fa fa-times';
                }
            }
            if ($display_type == '1_0') {
                if ($select_data[0][$temp_update_field_name] != 1) {
                    $temp_value = 1;
                    $temp_label_class = 'label label-success';
                    $temp_label = 'Yes';
                    $temp_icon = 'fa fa-check';
                }
                else {
                    $temp_value = 0;
                    $temp_label_class = 'label label-danger';
                    $temp_label = 'No';
                    $temp_icon = 'fa fa-times';
                }
            }
            if ($display_type == 'enabled_disabled') {
                if ($select_data[0][$temp_update_field_name] != 'enabled' && $select_data[0][$temp_update_field_name] != '') {
                    $temp_value = 'enabled';
                    $temp_label_class = 'label label-success';
                    $temp_label = 'Enabled';
                    $temp_icon = 'fa fa-check';
                }
                else {
                    $temp_value = 'disabled';
                    $temp_label_class = 'label label-danger';
                    $temp_label = 'Disabled';
                    $temp_icon = 'fa fa-times';
                }
            }

            if (is_int(strpos($update_field_name,"add_ons_"))) {
                $config_data = array(
                    'table_name' => $table_name,
                    'id_name' => $field_id,
                    'field_add_ons_name' => 'add_ons',
                    'selected_id' => $field_value,
                    'add_ons_title' => $temp_update_field_name,
                    'add_ons_value' => $temp_value,
                );
                $this->site->api_update_add_ons_field($config_data);                      
            }
            else {
                $temp_update = array(
                    $update_field_name => $temp_value
                );
                $this->db->update($table_name, $temp_update, $field_id." = ".$field_value);
            }

            $temp_display = '
                <a href="javascript:void(0);" onclick="
        var postData = {
            \'ajax_file\' : \''.$ajax_file.'\',
            \'table_name\' : \''.$table_name.'\',
            \'field_id\' : \''.$field_id.'\',
            \'field_value\' : \''.$field_value.'\',
            \'display_type\' : \''.$display_type.'\',
            \'update_field_name\' : \''.$update_field_name.'\',
            \'element_id\' : \''.$element_id.'\',
        };
        api_ajax_update_display(postData);  
                ">
                    <span class="'.$temp_label_class.'">
                    <i class="'.$temp_icon.'"></i> '.$temp_label.'</span>
                </a>
            ';

        }

        $temp3[1] = 'api-ajax-request-multiple-result-split';  
        $temp3[1] .= $temp_value;
        $temp3[2] = 'api-ajax-request-multiple-result-split';            
        $temp3[2] .= $temp_display;
        $temp3[3] = 'api-ajax-request-multiple-result-split';            
        $result = $temp3[1].$temp3[2].$temp3[3];
        echo $result;            
    }
    else
        echo '<script>window.location = "'.base_url().'admin/login";</script>';        
}

public function api_change_admin_mode() {
    if ($this->Owner || $this->Admin) {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }
        if ($field_value == 'true')
            $temp_value = '';          
        else
            $temp_value = 'yes';

        $config_data = array(
            'table_name' => 'sma_shop_settings',
            'id_name' => 'shop_id',
            'field_add_ons_name' => 'add_ons',
            'selected_id' => 1,
            'add_ons_title' => 'admin_mode_hide',
            'add_ons_value' => $temp_value,                    
        );
        $this->site->api_update_add_ons_field($config_data);            

        $temp3[1] = 'api-ajax-request-multiple-result-split';  
        $temp3[1] .= $temp_value;
        $temp3[2] = 'api-ajax-request-multiple-result-split';            
        $temp3[2] .= $temp_value;
        $temp3[3] = 'api-ajax-request-multiple-result-split';            
        $result = $temp3[1].$temp3[2].$temp3[3];
        echo $result;            
    }
    else
        echo '<script>window.location = "'.base_url().'admin/login";</script>';        
}

public function api_ajax_admin_delete() {   
    if ($this->Owner || $this->Admin) {
        foreach ($_GET as $name => $value) {
            ${$name} = $this->input->get($name);        
        }
        if ($upload_path != '') {
            $config_data = array(
                'table_name' => $table_name,
                'select_table' => $table_name,
                'translate' => '',
                'description' => '',
                'select_condition' =>  $table_id." = ".$selected_id,
            );
            $select_data = $this->api_helper->api_select_data_v2($config_data);
            $temp = $upload_path.'/'.$select_data[0]['image'];
            $this->api_helper->api_remove_file($temp);          
        }
        $this->db->delete($table_name, $table_id." = ".$selected_id);

        $temp3[1] = 'api-ajax-request-multiple-result-split';  
        $temp3[1] .= '';
        $temp3[2] = 'api-ajax-request-multiple-result-split';                       
        $result = $temp3[1].$temp3[2].$temp3[3];
        echo $result;            
    }
    else
        echo '<script>window.location = "'.base_url().'admin/login";</script>'; 
}

public function api_ajax_admin_edit() {
    if ($this->Owner || $this->Admin) {
        $field_data = array();
        $field_data_add = array();
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
            if (is_int(strpos($name,"api_field_data_"))) {
                $temp_name = str_replace('api_field_data_','',$name);
                $temp_2 = explode('_',$temp_name);
                $temp_name_2 = '';
                for ($i=1;$i<count($temp_2);$i++) {
                    if ($i < count($temp_2) - 1)
                        $temp_name_2 .= $temp_2[$i].'_';
                    else
                        $temp_name_2 .= $temp_2[$i];
                }
                $field_data[$temp_2[0]][$temp_name_2] = $value;
            }
            if (is_int(strpos($name,"api_field_add_data_"))) {
                $temp_name = str_replace('api_field_add_data_','',$name);
                $temp_2 = explode('_',$temp_name);
                $temp_name_2 = '';
                for ($i=1;$i<count($temp_2);$i++) {
                    if ($i < count($temp_2) - 1)
                        $temp_name_2 .= $temp_2[$i].'_';
                    else
                        $temp_name_2 .= $temp_2[$i];
                }
                $field_data_add[$temp_2[0]][$temp_name_2] = $value;
            }            
        }

        $temp_submit_script = '';
        for ($i=0;$i<count($field_data);$i++) {
            if ($add == 'yes')
                $field_data[$i]['field_value'] = '';
            else {                
                $config_data = array(
                    'table_name' => $table_name,
                    'select_table' => $table_name,
                    'translate' => $field_data[$i]['translate'],
                    'translate_2' => $field_data[$i]['translate_2'],
                    'translate_3' => $field_data[$i]['translate_3'],
                    'description' => $field_data[$i]['description'],
                    'select_condition' => $table_id." = ".$selected_id,
                );
                $temp = $this->api_helper->api_select_data_v2($config_data);    
                $language_array = unserialize(multi_language);
                if ($field_data[$i]['translate'] == 'yes') 
                    for ($i2=0;$i2<count($language_array);$i2++) {
                        $field_data[$i]['field_value_'.$language_array[$i2][0]] = $temp[0]['title_'.$language_array[$i2][0]];
                    }                    
                else if ($field_data[$i]['translate_2'] == 'yes') 
                    for ($i2=0;$i2<count($language_array);$i2++) {
                        $field_data[$i]['field_value_'.$language_array[$i2][0]] = $temp[0]['title_2_'.$language_array[$i2][0]];
                    }
                else if ($field_data[$i]['translate_3'] == 'yes') 
                    for ($i2=0;$i2<count($language_array);$i2++) {
                        $field_data[$i]['field_value_'.$language_array[$i2][0]] = $temp[0]['title_3_'.$language_array[$i2][0]];
                    }                          
                else if ($field_data[$i]['description'] == 'yes') 
                    for ($i2=0;$i2<count($language_array);$i2++) {
                        $field_data[$i]['field_value_'.$language_array[$i2][0]] = $temp[0]['description_'.$language_array[$i2][0]];
                    }                    
                else    
                    $field_data[$i]['field_value'] = $temp[0][$field_data[$i]['field_name']]; 

            }

            $temp = $this->api_admin->{'field_'.$field_data[$i]['type']}($field_data[$i],$_GET);
            $temp_motal_body .= $temp['display'];
            $temp_submit_script .= $temp['script'];
        }

        if ($add == 'yes') {
            $temp_motal_body = '';
            $selected_id = 0;
            $temp_array = array_merge($field_data, $field_data_add);        
            for ($i=0;$i<count($temp_array);$i++) {
                $temp = $this->api_admin->{'field_'.$temp_array[$i]['type']}($temp_array[$i],$_GET);
                $temp_motal_body .= $temp['display'];
                $temp_submit_script .= $temp['script'];
            }
            
        }

        $temp_motal_body .= $temp_test.'
            <div class="api_clear_both"></div>
        ';

        $temp_display = '';
        if ($datepicker == 'yes')
            $temp_display .= '
<link href="'.base_url().'/assets/datepicker/bootstrap-datetimepicker.css" rel="stylesheet">
<script src="'.base_url().'/assets/datepicker/moment-with-locales.js" type="text/javascript" ></script>
<script src="'.base_url().'/assets/datepicker/bootstrap-datetimepicker.js" type="text/javascript" ></script>      
            ';
        $temp_display .= '
            <form action="'.base_url().'main/api_admin_edit" name="api_form_admin" id="api_form_admin" method="post" enctype="multipart/form-data">      
                <input type="hidden" name="'.$this->security->get_csrf_token_name().'" value="'.$this->security->get_csrf_hash().'">      
                <input type="hidden" name="api_config_lang" value="'.$l.'">
                <input type="hidden" name="api_config_selected_id" value="'.$selected_id.'" />         
                <input type="hidden" name="api_config_table_name" value="'.$table_name.'" />
                <input type="hidden" name="api_config_table_id" value="'.$table_id.'" />        
                <input type="hidden" name="api_config_upload_path" value="'.$upload_path.'" />        
                <input type="hidden" name="api_config_redirect" value="'.$redirect.'">          
                <input type="hidden" name="api_config_condition_ordering" value="'.$condition_ordering.'">
                <input type="hidden" name="api_config_generate_translate" value="'.$generate_translate.'">
                <input type="hidden" name="api_config_generate_slug" value="'.$generate_slug.'">                
                '.$temp_motal_body.'
                <div class="api_clear_both"></div>
            </form>
        ';

        $temp = 'Update';
        if ($add == 'yes')
            $temp = 'Add';        
        $temp_footer = '
            <button type="button" class="btn btn-info" onclick="api_form_admin_submit();">
                '.$temp.'
            </button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">
                Cancel
            </button>      
            <script>
                function api_form_admin_submit() {
                    var b = 1;
                    var temp = "";
                    '.$temp_submit_script.'
                    if (b == 1) {
                        $("form#api_form_admin textarea").each(function(){
                            var temp = $(this).val();
                            temp = temp.replace("like","liike");
                            $(this).val(temp);
                        });                        
                        document.api_form_admin.submit();
                    }
                }
            </script>
        ';

        $temp3[1] = 'api-ajax-request-multiple-result-split';  
        $temp3[1] .= $temp_display;
        $temp3[2] = 'api-ajax-request-multiple-result-split';            
        $temp3[2] .= $temp_footer;
        $temp3[3] = 'api-ajax-request-multiple-result-split';            
        $result = $temp3[1].$temp3[2].$temp3[3];
        echo $result;            
    }
    else
        echo '<script>window.location = "'.base_url().'admin/login";</script>';        
}

}
