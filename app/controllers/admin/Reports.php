<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        $this->lang->admin_load('reports', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('reports_model');
        $this->data['pb'] = array(
            'cash' => lang('cash'),
            'CC' => lang('CC'),
            'Cheque' => lang('Cheque'),
            'paypal_pro' => lang('paypal_pro'),
            'stripe' => lang('stripe'),
            'gift_card' => lang('gift_card'),
            'deposit' => lang('deposit'),
            'authorize' => lang('authorize'),
            );

    }

    function index()
    {
        $this->sma->checkPermissions();
        $data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['monthly_sales'] = $this->reports_model->getChartData();
        $this->data['stock'] = $this->reports_model->getStockValue();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('reports')));
        $meta = array('page_title' => lang('reports'), 'bc' => $bc);
        $this->page_construct('reports/index', $meta, $this->data);

    }

    function warehouse_stock($warehouse = NULL)
    {
        $this->sma->checkPermissions('index', TRUE);
        $data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        if ($this->input->get('warehouse')) {
            $warehouse = $this->input->get('warehouse');
        }

        $this->data['stock'] = $warehouse ? $this->reports_model->getWarehouseStockValue($warehouse) : $this->reports_model->getStockValue();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['warehouse_id'] = $warehouse;
        $this->data['warehouse'] = $warehouse ? $this->site->getWarehouseByID($warehouse) : NULL;
        $this->data['totals'] = $this->reports_model->getWarehouseTotals($warehouse);
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('reports')));
        $meta = array('page_title' => lang('reports'), 'bc' => $bc);
        $this->page_construct('reports/warehouse_stock', $meta, $this->data);

    }

    function expiry_alerts($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('expiry_alerts');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        } else {
            $user = $this->site->getUser();
            $this->data['warehouses'] = NULL;
            $this->data['warehouse_id'] = $user->warehouse_id;
            $this->data['warehouse'] = $user->warehouse_id ? $this->site->getWarehouseByID($user->warehouse_id) : NULL;
        }

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('product_expiry_alerts')));
        $meta = array('page_title' => lang('product_expiry_alerts'), 'bc' => $bc);
        $this->page_construct('reports/expiry_alerts', $meta, $this->data);
    }

    function getExpiryAlerts($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('expiry_alerts', TRUE);
        $date = date('Y-m-d', strtotime('+3 months'));

        if (!$this->Owner && !$warehouse_id) {
            $user = $this->site->getUser();
            $warehouse_id = $user->warehouse_id;
        }

        $this->load->library('datatables');
        if ($warehouse_id) {
            $this->datatables
                ->select("image, product_code, product_name, quantity_balance, warehouses.name, expiry")
                ->from('purchase_items')
                ->join('products', 'products.id=purchase_items.product_id', 'left')
                ->join('warehouses', 'warehouses.id=purchase_items.warehouse_id', 'left')
                ->where('warehouse_id', $warehouse_id)
                ->where('expiry !=', NULL)->where('expiry !=', '0000-00-00')
                ->where('quantity_balance >', 0)
                ->where('expiry <', $date);
        } else {
            $this->datatables
                ->select("image, product_code, product_name, quantity_balance, warehouses.name, expiry")
                ->from('purchase_items')
                ->join('products', 'products.id=purchase_items.product_id', 'left')
                ->join('warehouses', 'warehouses.id=purchase_items.warehouse_id', 'left')
                ->where('expiry !=', NULL)->where('expiry !=', '0000-00-00')
                ->where('quantity_balance >', 0)
                ->where('expiry <', $date);
        }
        echo $this->datatables->generate();
    }

    function quantity_alerts($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('quantity_alerts');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        } else {
            $user = $this->site->getUser();
            $this->data['warehouses'] = NULL;
            $this->data['warehouse_id'] = $user->warehouse_id;
            $this->data['warehouse'] = $user->warehouse_id ? $this->site->getWarehouseByID($user->warehouse_id) : NULL;
        }

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('product_quantity_alerts')));
        $meta = array('page_title' => lang('product_quantity_alerts'), 'bc' => $bc);
        $this->page_construct('reports/quantity_alerts', $meta, $this->data);
    }

    function getQuantityAlerts($warehouse_id = NULL, $pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('quantity_alerts', TRUE);
        if (!$this->Owner && !$warehouse_id) {
            $user = $this->site->getUser();
            $warehouse_id = $user->warehouse_id;
        }

        if ($pdf || $xls) {

            if ($warehouse_id) {
                $this->db
                    ->select('products.image as image, products.code, products.name, warehouses_products.quantity, alert_quantity')
                    ->from('products')->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
                    ->where('alert_quantity > warehouses_products.quantity', NULL)
                    ->where('warehouse_id', $warehouse_id)
                    ->where('track_quantity', 1)
                    ->order_by('products.code desc');
            } else {
                $this->db
                    ->select('image, code, name, quantity, alert_quantity')
                    ->from('products')
                    ->where('alert_quantity > quantity', NULL)
                    ->where('track_quantity', 1)
                    ->order_by('code desc');
            }

            $q = $this->db->get();
            if ($q != false && $q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('product_quantity_alerts'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('product_code'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('product_name'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('quantity'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('alert_quantity'));

                $row = 2;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->code);
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->name);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->quantity);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->alert_quantity);
                    $row++;
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $filename = 'product_quantity_alerts';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $this->load->library('datatables');
            if ($warehouse_id) {
                $this->datatables
                    ->select('image, code, name, wp.quantity, alert_quantity')
                    ->from('products')
                    ->join("( SELECT * from {$this->db->dbprefix('warehouses_products')} WHERE warehouse_id = {$warehouse_id}) wp", 'products.id=wp.product_id', 'left')
                    ->where('alert_quantity > wp.quantity', NULL)
                    ->or_where('wp.quantity', NULL)
                    ->where('track_quantity', 1)
                    ->group_by('products.id');
            } else {
                $this->datatables
                    ->select('image, code, name, quantity, alert_quantity')
                    ->from('products')
                    ->where('alert_quantity > quantity', NULL)
                    ->where('track_quantity', 1);
            }

            echo $this->datatables->generate();

        }

    }

    function suggestions()
    {
        $term = $this->input->get('term', TRUE);
        if (strlen($term) < 1) {
            die();
        }

        $rows = $this->reports_model->getProductNames($term);
        if ($rows) {
            foreach ($rows as $row) {
                $pr[] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")");

            }
            $this->sma->send_json($pr);
        } else {
            echo FALSE;
        }
    }

    public function best_sellers($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('products');

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $y1 = date('Y', strtotime('-1 month'));
        $m1 = date('m', strtotime('-1 month'));
        $m1sdate = $y1.'-'.$m1.'-01 00:00:00';
        $m1edate = $y1.'-'.$m1.'-'. days_in_month($m1, $y1) . ' 23:59:59';
        $this->data['m1'] = date('M Y', strtotime($y1.'-'.$m1));
        $this->data['m1bs'] = $this->reports_model->getBestSeller($m1sdate, $m1edate, $warehouse_id);
        $y2 = date('Y', strtotime('-2 months'));
        $m2 = date('m', strtotime('-2 months'));
        $m2sdate = $y2.'-'.$m2.'-01 00:00:00';
        $m2edate = $y2.'-'.$m2.'-'. days_in_month($m2, $y2) . ' 23:59:59';
        $this->data['m2'] = date('M Y', strtotime($y2.'-'.$m2));
        $this->data['m2bs'] = $this->reports_model->getBestSeller($m2sdate, $m2edate, $warehouse_id);
        $y3 = date('Y', strtotime('-3 months'));
        $m3 = date('m', strtotime('-3 months'));
        $m3sdate = $y3.'-'.$m3.'-01 23:59:59';
        $this->data['m3'] = date('M Y', strtotime($y3.'-'.$m3)).' - '.$this->data['m1'];
        $this->data['m3bs'] = $this->reports_model->getBestSeller($m3sdate, $m1edate, $warehouse_id);
        $y4 = date('Y', strtotime('-12 months'));
        $m4 = date('m', strtotime('-12 months'));
        $m4sdate = $y4.'-'.$m4.'-01 23:59:59';
        $this->data['m4'] = date('M Y', strtotime($y4.'-'.$m4)).' - '.$this->data['m1'];
        $this->data['m4bs'] = $this->reports_model->getBestSeller($m4sdate, $m1edate, $warehouse_id);
        // $this->sma->print_arrays($this->data['m1bs'], $this->data['m2bs'], $this->data['m3bs'], $this->data['m4bs']);
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('best_sellers')));
        $meta = array('page_title' => lang('best_sellers'), 'bc' => $bc);
        $this->page_construct('reports/best_sellers', $meta, $this->data);

    }

    function products()
    {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['categories'] = $this->site->getAllCategories();
        $this->data['brands'] = $this->site->getAllBrands();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        if ($this->input->post('start_date')) {
            $dt = "From " . $this->input->post('start_date') . " to " . $this->input->post('end_date');
        } else {
            $dt = "Till " . $this->input->post('end_date');
        }
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('products_report')));
        $meta = array('page_title' => lang('products_report'), 'bc' => $bc);
        $this->page_construct('reports/products', $meta, $this->data);
    }

    function getProductsReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('products', TRUE);

        $product = $this->input->get('product') ? $this->input->get('product') : NULL;
        $category = $this->input->get('category') ? $this->input->get('category') : NULL;
        $brand = $this->input->get('brand') ? $this->input->get('brand') : NULL;
        $subcategory = $this->input->get('subcategory') ? $this->input->get('subcategory') : NULL;
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $cf1 = $this->input->get('cf1') ? $this->input->get('cf1') : NULL;
        $cf2 = $this->input->get('cf2') ? $this->input->get('cf2') : NULL;
        $cf3 = $this->input->get('cf3') ? $this->input->get('cf3') : NULL;
        $cf4 = $this->input->get('cf4') ? $this->input->get('cf4') : NULL;
        $cf5 = $this->input->get('cf5') ? $this->input->get('cf5') : NULL;
        $cf6 = $this->input->get('cf6') ? $this->input->get('cf6') : NULL;
        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;

        $pp = "( SELECT product_id, SUM(CASE WHEN pi.purchase_id IS NOT NULL THEN quantity ELSE 0 END) as purchasedQty, SUM(quantity_balance) as balacneQty, SUM( unit_cost * quantity_balance ) balacneValue, SUM( (CASE WHEN pi.purchase_id IS NOT NULL THEN (pi.subtotal) ELSE 0 END) ) totalPurchase from {$this->db->dbprefix('purchase_items')} pi LEFT JOIN {$this->db->dbprefix('purchases')} p on p.id = pi.purchase_id WHERE p.status != 'pending' AND p.status != 'ordered' ";
        $sp = "( SELECT si.product_id, SUM( si.quantity ) soldQty, SUM( si.subtotal ) totalSale from " . $this->db->dbprefix('sales') . " s JOIN " . $this->db->dbprefix('sale_items') . " si on s.id = si.sale_id ";
        if ($start_date || $warehouse) {
            $sp .= " WHERE ";
            if ($start_date) {
                $start_date = $this->sma->fld($start_date);
                $end_date = $end_date ? $this->sma->fld($end_date) : date('Y-m-d');
                $pp .= " AND p.date >= '{$start_date}' AND p.date < '{$end_date}' ";
                $sp .= " s.date >= '{$start_date}' AND s.date < '{$end_date}' ";
                if ($warehouse) {
                    $sp .= " AND ";
                }
            }
            if ($warehouse) {
                $pp .= " AND pi.warehouse_id = '{$warehouse}' ";
                $sp .= " si.warehouse_id = '{$warehouse}' ";
            }
        }
        $pp .= " GROUP BY pi.product_id ) PCosts";
        $sp .= " GROUP BY si.product_id ) PSales";
        if ($pdf || $xls) {

            $this->db
                ->select($this->db->dbprefix('products') . ".code, " . $this->db->dbprefix('products') . ".name,
                COALESCE( PCosts.purchasedQty, 0 ) as PurchasedQty,
                COALESCE( PSales.soldQty, 0 ) as SoldQty,
                COALESCE( PCosts.balacneQty, 0 ) as BalacneQty,
                COALESCE( PCosts.totalPurchase, 0 ) as TotalPurchase,
                COALESCE( PCosts.balacneValue, 0 ) as TotalBalance,
                COALESCE( PSales.totalSale, 0 ) as TotalSales,
                (COALESCE( PSales.totalSale, 0 ) - COALESCE( PCosts.totalPurchase, 0 )) as Profit", FALSE)
                ->from('products')
                ->join($sp, 'products.id = PSales.product_id', 'left')
                ->join($pp, 'products.id = PCosts.product_id', 'left')
                ->order_by('products.name');

            if ($product) {
                $this->db->where($this->db->dbprefix('products') . ".id", $product);
            }
            if ($cf1) {
                $this->db->where($this->db->dbprefix('products') . ".cf1", $cf1);
            }
            if ($cf2) {
                $this->db->where($this->db->dbprefix('products') . ".cf2", $cf2);
            }
            if ($cf3) {
                $this->db->where($this->db->dbprefix('products') . ".cf3", $cf3);
            }
            if ($cf4) {
                $this->db->where($this->db->dbprefix('products') . ".cf4", $cf4);
            }
            if ($cf5) {
                $this->db->where($this->db->dbprefix('products') . ".cf5", $cf5);
            }
            if ($cf6) {
                $this->db->where($this->db->dbprefix('products') . ".cf6", $cf6);
            }
            if ($category) {
                $this->db->where($this->db->dbprefix('products') . ".category_id", $category);
            }
            if ($subcategory) {
                $this->db->where($this->db->dbprefix('products') . ".subcategory_id", $subcategory);
            }
            if ($brand) {
                $this->db->where($this->db->dbprefix('products') . ".brand", $brand);
            }

            $q = $this->db->get();
            if ($q != false && $q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('products_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('product_code'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('product_name'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('purchased'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('sold'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('balance'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('purchased_amount'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('sold_amount'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('profit_loss'));
                $this->excel->getActiveSheet()->SetCellValue('I1', lang('stock_in_hand'));

                $row = 2;
                $sQty = 0;
                $pQty = 0;
                $sAmt = 0;
                $pAmt = 0;
                $bQty = 0;
                $bAmt = 0;
                $pl = 0;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->code);
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->name);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->PurchasedQty);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->SoldQty);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->BalacneQty);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->TotalPurchase);
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->TotalSales);
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, $data_row->Profit);
                    $this->excel->getActiveSheet()->SetCellValue('I' . $row, $data_row->TotalBalance);
                    $pQty += $data_row->PurchasedQty;
                    $sQty += $data_row->SoldQty;
                    $bQty += $data_row->BalacneQty;
                    $pAmt += $data_row->TotalPurchase;
                    $sAmt += $data_row->TotalSales;
                    $bAmt += $data_row->TotalBalance;
                    $pl += $data_row->Profit;
                    $row++;
                }
                $this->excel->getActiveSheet()->getStyle("C" . $row . ":I" . $row)->getBorders()
                    ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                $this->excel->getActiveSheet()->SetCellValue('C' . $row, $pQty);
                $this->excel->getActiveSheet()->SetCellValue('D' . $row, $sQty);
                $this->excel->getActiveSheet()->SetCellValue('E' . $row, $bQty);
                $this->excel->getActiveSheet()->SetCellValue('F' . $row, $pAmt);
                $this->excel->getActiveSheet()->SetCellValue('G' . $row, $sAmt);
                $this->excel->getActiveSheet()->SetCellValue('H' . $row, $pl);
                $this->excel->getActiveSheet()->SetCellValue('I' . $row, $bAmt);

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('C2:G' . $row)->getAlignment()->setWrapText(true);
                $filename = 'products_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $this->load->library('datatables');
            $this->datatables
                ->select($this->db->dbprefix('products') . ".code, " . $this->db->dbprefix('products') . ".name,
                CONCAT(COALESCE( PCosts.purchasedQty, 0 ), '__', COALESCE( PCosts.totalPurchase, 0 )) as purchased,
                CONCAT(COALESCE( PSales.soldQty, 0 ), '__', COALESCE( PSales.totalSale, 0 )) as sold,
                (COALESCE( PSales.totalSale, 0 ) - COALESCE( PCosts.totalPurchase, 0 )) as Profit,
                CONCAT(COALESCE( PCosts.balacneQty, 0 ), '__', COALESCE( PCosts.balacneValue, 0 )) as balance, {$this->db->dbprefix('products')}.id as id", FALSE)
                ->from('products')
                ->join($sp, 'products.id = PSales.product_id', 'left')
                ->join($pp, 'products.id = PCosts.product_id', 'left')
                ->group_by('products.code, PSales.soldQty, PSales.totalSale, PCosts.purchasedQty, PCosts.totalPurchase, PCosts.balacneQty, PCosts.balacneValue');

            if ($product) {
                $this->datatables->where($this->db->dbprefix('products') . ".id", $product);
            }
            if ($cf1) {
                $this->datatables->where($this->db->dbprefix('products') . ".cf1", $cf1);
            }
            if ($cf2) {
                $this->datatables->where($this->db->dbprefix('products') . ".cf2", $cf2);
            }
            if ($cf3) {
                $this->datatables->where($this->db->dbprefix('products') . ".cf3", $cf3);
            }
            if ($cf4) {
                $this->datatables->where($this->db->dbprefix('products') . ".cf4", $cf4);
            }
            if ($cf5) {
                $this->datatables->where($this->db->dbprefix('products') . ".cf5", $cf5);
            }
            if ($cf6) {
                $this->datatables->where($this->db->dbprefix('products') . ".cf6", $cf6);
            }
            if ($category) {
                $this->datatables->where($this->db->dbprefix('products') . ".category_id", $category);
            }
            if ($subcategory) {
                $this->datatables->where($this->db->dbprefix('products') . ".subcategory_id", $subcategory);
            }
            if ($brand) {
                $this->datatables->where($this->db->dbprefix('products') . ".brand", $brand);
            }

            echo $this->datatables->generate();

        }

    }

    function categories()
    {
        $this->sma->checkPermissions('products');
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['categories'] = $this->site->getAllCategories();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        if ($this->input->post('start_date')) {
            $dt = "From " . $this->input->post('start_date') . " to " . $this->input->post('end_date');
        } else {
            $dt = "Till " . $this->input->post('end_date');
        }
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('categories_report')));
        $meta = array('page_title' => lang('categories_report'), 'bc' => $bc);
        $this->page_construct('reports/categories', $meta, $this->data);
    }

    function getCategoriesReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('products', TRUE);
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $category = $this->input->get('category') ? $this->input->get('category') : NULL;
        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;

        $pp = "( SELECT pp.category_id as category, SUM( pi.quantity ) purchasedQty, SUM( pi.subtotal ) totalPurchase from {$this->db->dbprefix('products')} pp
                left JOIN " . $this->db->dbprefix('purchase_items') . " pi ON pp.id = pi.product_id
                left join " . $this->db->dbprefix('purchases') . " p ON p.id = pi.purchase_id ";
        $sp = "( SELECT sp.category_id as category, SUM( si.quantity ) soldQty, SUM( si.subtotal ) totalSale from {$this->db->dbprefix('products')} sp
                left JOIN " . $this->db->dbprefix('sale_items') . " si ON sp.id = si.product_id
                left join " . $this->db->dbprefix('sales') . " s ON s.id = si.sale_id ";
        if ($start_date || $warehouse) {
            $pp .= " WHERE ";
            $sp .= " WHERE ";
            if ($start_date) {
                $start_date = $this->sma->fld($start_date);
                $end_date = $end_date ? $this->sma->fld($end_date) : date('Y-m-d');
                $pp .= " p.date >= '{$start_date}' AND p.date < '{$end_date}' ";
                $sp .= " s.date >= '{$start_date}' AND s.date < '{$end_date}' ";
                if ($warehouse) {
                    $pp .= " AND ";
                    $sp .= " AND ";
                }
            }
            if ($warehouse) {
                $pp .= " pi.warehouse_id = '{$warehouse}' ";
                $sp .= " si.warehouse_id = '{$warehouse}' ";
            }
        }
        $pp .= " GROUP BY pp.category_id ) PCosts";
        $sp .= " GROUP BY sp.category_id ) PSales";

        if ($pdf || $xls) {

            $this->db
                ->select($this->db->dbprefix('categories') . ".code, " . $this->db->dbprefix('categories') . ".name,
                    SUM( COALESCE( PCosts.purchasedQty, 0 ) ) as PurchasedQty,
                    SUM( COALESCE( PSales.soldQty, 0 ) ) as SoldQty,
                    SUM( COALESCE( PCosts.totalPurchase, 0 ) ) as TotalPurchase,
                    SUM( COALESCE( PSales.totalSale, 0 ) ) as TotalSales,
                    (SUM( COALESCE( PSales.totalSale, 0 ) )- SUM( COALESCE( PCosts.totalPurchase, 0 ) ) ) as Profit", FALSE)
                ->from('categories')
                ->join($sp, 'categories.id = PSales.category', 'left')
                ->join($pp, 'categories.id = PCosts.category', 'left')
                ->group_by('categories.id, categories.code, categories.name')
                ->order_by('categories.code', 'asc');

            if ($category) {
                $this->db->where($this->db->dbprefix('categories') . ".id", $category);
            }

            $q = $this->db->get();
            if ($q != false && $q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('categories_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('category_code'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('category_name'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('purchased'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('sold'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('purchased_amount'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('sold_amount'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('profit_loss'));

                $row = 2;
                $sQty = 0;
                $pQty = 0;
                $sAmt = 0;
                $pAmt = 0;
                $pl = 0;
                foreach ($data as $data_row) {
                    $profit = $data_row->TotalSales - $data_row->TotalPurchase;
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->code);
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->name);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->PurchasedQty);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->SoldQty);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->TotalPurchase);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->TotalSales);
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $profit);
                    $pQty += $data_row->PurchasedQty;
                    $sQty += $data_row->SoldQty;
                    $pAmt += $data_row->TotalPurchase;
                    $sAmt += $data_row->TotalSales;
                    $pl += $profit;
                    $row++;
                }
                $this->excel->getActiveSheet()->getStyle("C" . $row . ":G" . $row)->getBorders()
                    ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                $this->excel->getActiveSheet()->SetCellValue('C' . $row, $pQty);
                $this->excel->getActiveSheet()->SetCellValue('D' . $row, $sQty);
                $this->excel->getActiveSheet()->SetCellValue('E' . $row, $pAmt);
                $this->excel->getActiveSheet()->SetCellValue('F' . $row, $sAmt);
                $this->excel->getActiveSheet()->SetCellValue('G' . $row, $pl);

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('C2:G' . $row)->getAlignment()->setWrapText(true);
                $filename = 'categories_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {


            $this->load->library('datatables');
            $this->datatables
                ->select($this->db->dbprefix('categories') . ".id as cid, " .$this->db->dbprefix('categories') . ".code, " . $this->db->dbprefix('categories') . ".name,
                    SUM( COALESCE( PCosts.purchasedQty, 0 ) ) as PurchasedQty,
                    SUM( COALESCE( PSales.soldQty, 0 ) ) as SoldQty,
                    SUM( COALESCE( PCosts.totalPurchase, 0 ) ) as TotalPurchase,
                    SUM( COALESCE( PSales.totalSale, 0 ) ) as TotalSales,
                    (SUM( COALESCE( PSales.totalSale, 0 ) )- SUM( COALESCE( PCosts.totalPurchase, 0 ) ) ) as Profit", FALSE)
                ->from('categories')
                ->join($sp, 'categories.id = PSales.category', 'left')
                ->join($pp, 'categories.id = PCosts.category', 'left');

            if ($category) {
                $this->datatables->where('categories.id', $category);
            }
            $this->datatables->group_by('categories.id, categories.code, categories.name, PSales.SoldQty, PSales.totalSale, PCosts.purchasedQty, PCosts.totalPurchase');
            $this->datatables->unset_column('cid');
            echo $this->datatables->generate();

        }

    }

    function brands()
    {
        $this->sma->checkPermissions('products');
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['brands'] = $this->site->getAllBrands();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        if ($this->input->post('start_date')) {
            $dt = "From " . $this->input->post('start_date') . " to " . $this->input->post('end_date');
        } else {
            $dt = "Till " . $this->input->post('end_date');
        }
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('brands_report')));
        $meta = array('page_title' => lang('brands_report'), 'bc' => $bc);
        $this->page_construct('reports/brands', $meta, $this->data);
    }

    function getBrandsReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('products', TRUE);
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $brand = $this->input->get('brand') ? $this->input->get('brand') : NULL;
        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;

        $pp = "( SELECT pp.brand as brand, SUM( pi.quantity ) purchasedQty, SUM( pi.subtotal ) totalPurchase from {$this->db->dbprefix('products')} pp
                left JOIN " . $this->db->dbprefix('purchase_items') . " pi ON pp.id = pi.product_id
                left join " . $this->db->dbprefix('purchases') . " p ON p.id = pi.purchase_id ";
        $sp = "( SELECT sp.brand as brand, SUM( si.quantity ) soldQty, SUM( si.subtotal ) totalSale from {$this->db->dbprefix('products')} sp
                left JOIN " . $this->db->dbprefix('sale_items') . " si ON sp.id = si.product_id
                left join " . $this->db->dbprefix('sales') . " s ON s.id = si.sale_id ";
        if ($start_date || $warehouse) {
            $pp .= " WHERE ";
            $sp .= " WHERE ";
            if ($start_date) {
                $start_date = $this->sma->fld($start_date);
                $end_date = $end_date ? $this->sma->fld($end_date) : date('Y-m-d');
                $pp .= " p.date >= '{$start_date}' AND p.date < '{$end_date}' ";
                $sp .= " s.date >= '{$start_date}' AND s.date < '{$end_date}' ";
                if ($warehouse) {
                    $pp .= " AND ";
                    $sp .= " AND ";
                }
            }
            if ($warehouse) {
                $pp .= " pi.warehouse_id = '{$warehouse}' ";
                $sp .= " si.warehouse_id = '{$warehouse}' ";
            }
        }
        $pp .= " GROUP BY pp.brand ) PCosts";
        $sp .= " GROUP BY sp.brand ) PSales";

        if ($pdf || $xls) {

            $this->db
                ->select($this->db->dbprefix('brands') . ".name,
                    SUM( COALESCE( PCosts.purchasedQty, 0 ) ) as PurchasedQty,
                    SUM( COALESCE( PSales.soldQty, 0 ) ) as SoldQty,
                    SUM( COALESCE( PCosts.totalPurchase, 0 ) ) as TotalPurchase,
                    SUM( COALESCE( PSales.totalSale, 0 ) ) as TotalSales,
                    (SUM( COALESCE( PSales.totalSale, 0 ) )- SUM( COALESCE( PCosts.totalPurchase, 0 ) ) ) as Profit", FALSE)
                ->from('brands')
                ->join($sp, 'brands.id = PSales.brand', 'left')
                ->join($pp, 'brands.id = PCosts.brand', 'left')
                ->group_by('brands.id, brands.name')
                ->order_by('brands.code', 'asc');

            if ($brand) {
                $this->db->where($this->db->dbprefix('brands') . ".id", $brand);
            }

            $q = $this->db->get();
            if ($q != false && $q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('brands_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('brands'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('purchased'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('sold'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('purchased_amount'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('sold_amount'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('profit_loss'));

                $row = 2; $sQty = 0; $pQty = 0; $sAmt = 0; $pAmt = 0; $pl = 0;
                foreach ($data as $data_row) {
                    $profit = $data_row->TotalSales - $data_row->TotalPurchase;
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->name);
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->PurchasedQty);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->SoldQty);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->TotalPurchase);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->TotalSales);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $profit);
                    $pQty += $data_row->PurchasedQty;
                    $sQty += $data_row->SoldQty;
                    $pAmt += $data_row->TotalPurchase;
                    $sAmt += $data_row->TotalSales;
                    $pl += $profit;
                    $row++;
                }
                $this->excel->getActiveSheet()->getStyle("B" . $row . ":F" . $row)->getBorders()
                    ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                $this->excel->getActiveSheet()->SetCellValue('B' . $row, $pQty);
                $this->excel->getActiveSheet()->SetCellValue('C' . $row, $sQty);
                $this->excel->getActiveSheet()->SetCellValue('D' . $row, $pAmt);
                $this->excel->getActiveSheet()->SetCellValue('E' . $row, $sAmt);
                $this->excel->getActiveSheet()->SetCellValue('F' . $row, $pl);

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('C2:G' . $row)->getAlignment()->setWrapText(true);
                $filename = 'brands_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {


            $this->load->library('datatables');
            $this->datatables
                ->select($this->db->dbprefix('brands') . ".id as id, " . $this->db->dbprefix('brands') . ".name,
                    SUM( COALESCE( PCosts.purchasedQty, 0 ) ) as PurchasedQty,
                    SUM( COALESCE( PSales.soldQty, 0 ) ) as SoldQty,
                    SUM( COALESCE( PCosts.totalPurchase, 0 ) ) as TotalPurchase,
                    SUM( COALESCE( PSales.totalSale, 0 ) ) as TotalSales,
                    (SUM( COALESCE( PSales.totalSale, 0 ) )- SUM( COALESCE( PCosts.totalPurchase, 0 ) ) ) as Profit", FALSE)
                ->from('brands')
                ->join($sp, 'brands.id = PSales.brand', 'left')
                ->join($pp, 'brands.id = PCosts.brand', 'left');

            if ($brand) {
                $this->datatables->where('brands.id', $brand);
            }
            $this->datatables->group_by('brands.id, brands.name, PSales.SoldQty, PSales.totalSale, PCosts.purchasedQty, PCosts.totalPurchase');
            $this->datatables->unset_column('id');
            echo $this->datatables->generate();

        }

    }

    function profit($date = NULL, $warehouse_id = NULL, $re = NULL)
    {
        if ( ! $this->Owner) {
            $this->session->set_flashdata('error', lang('access_denied'));
            $this->sma->md();
        }
        if ( ! $date) { $date = date('Y-m-d'); }
        $this->data['costing'] = $this->reports_model->getCosting($date, $warehouse_id);
        $this->data['discount'] = $this->reports_model->getOrderDiscount($date, $warehouse_id);
        $this->data['expenses'] = $this->reports_model->getExpenses($date, $warehouse_id);
        $this->data['transaction'] = $this->reports_model->getTransaction($date, $warehouse_id);
        $this->data['sale_adjustment'] = $this->reports_model->getTransaction($date, $warehouse_id);
        $this->data['returns'] = $this->reports_model->getReturns($date, $warehouse_id);
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['swh'] = $warehouse_id;
        $this->data['date'] = $date;
        
        if ($re) {
            echo $this->load->view($this->theme . 'reports/profit', $this->data, TRUE);
            exit();
        }
        $this->load->view($this->theme . 'reports/profit', $this->data);
    }
    function monthly_profit($year, $month, $warehouse_id = NULL, $re = NULL)
    {
        if ( ! $this->Owner) {
            $this->session->set_flashdata('error', lang('access_denied'));
            $this->sma->md();
        }

        $this->data['costing'] = $this->reports_model->getCosting(NULL, $warehouse_id, $year, $month);
        $this->data['discount'] = $this->reports_model->getOrderDiscount(NULL, $warehouse_id, $year, $month);
        $this->data['expenses'] = $this->reports_model->getExpenses(NULL, $warehouse_id, $year, $month);
        $this->data['returns'] = $this->reports_model->getReturns(NULL, $warehouse_id, $year, $month);
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['swh'] = $warehouse_id;
        $this->data['year'] = $year;
        $this->data['month'] = $month;
        $this->data['date'] = date('F Y', strtotime($year.'-'.$month.'-'.'01'));
        if ($re) {
            echo $this->load->view($this->theme . 'reports/monthly_profit', $this->data, TRUE);
            exit();
        }
        $this->load->view($this->theme . 'reports/monthly_profit', $this->data);
    }

    function daily_sales( $warehouse_id = NULL, $year = NULL, $month = NULL, $pdf = NULL, $user_id = NULL)
    {
        
        $this->sma->checkPermissions();
        if (!$this->Owner && !$this->Admin && $this->session->userdata('warehouse_id')) {
            $warehouse_id = $this->session->userdata('warehouse_id');
        }
        if (!$year) {
            $year = date('Y');
        }
        if (!$month) {
            $month = date('m');
        }
        if(!$day) {
            $day = date('d');
        }
    
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $config = array(
            'show_next_prev' => TRUE,
            'next_prev_url' => admin_url('reports/daily_sales/'.($warehouse_id ? $warehouse_id : 0)),
            'month_type' => 'long',
            'day_type' => 'long'
        );
        

        $config['template'] = '{table_open}<div class="table-responsive"><table border="0" cellpadding="0" cellspacing="0" class="table table-bordered dfTable">{/table_open}
        {heading_row_start}<tr>{/heading_row_start}
        {heading_previous_cell}<th><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
        {heading_title_cell}<th colspan="{colspan}" id="month_year">{heading}</th>{/heading_title_cell}
        {heading_next_cell}<th><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}
        {heading_row_end}</tr>{/heading_row_end}
        {week_row_start}<tr>{/week_row_start}
        {week_day_cell}<td class="cl_wday">{week_day}</td>{/week_day_cell}
        {week_row_end}</tr>{/week_row_end}
        {cal_row_start}<tr class="days">{/cal_row_start}
        {cal_cell_start}<td class="day">{/cal_cell_start}
        {cal_cell_content}
        <div class="day_num">{day}</div>
        <div class="content">{content}</div>
        {/cal_cell_content}
        {cal_cell_content_today}
        <div class="day_num highlight">{day}</div>
        <div class="content">{content}</div>
        {/cal_cell_content_today}
        {cal_cell_no_content}<div class="day_num">{day}</div>{/cal_cell_no_content}
        {cal_cell_no_content_today}<div class="day_num highlight">{day}</div>{/cal_cell_no_content_today}
        {cal_cell_blank}&nbsp;{/cal_cell_blank}
        {cal_cell_end}</td>{/cal_cell_end}
        {cal_row_end}</tr>{/cal_row_end}
        {table_close}</table></div>{/table_close}';

        $this->load->library('calendar', $config);
        $sales = $user_id ? $this->reports_model->getStaffDailySales($user_id, $year, $month, $warehouse_id) : $this->reports_model->getDailySales($year, $month, $warehouse_id);

        if (!empty($sales)) {
            foreach ($sales as $sale) {
                $condition =  $year.'-'.$month.'-'.str_pad($sale->date, 2, '0', STR_PAD_LEFT);
                
                $config_data = array(
                    'table_name' => 'sma_sales',
                    'select_table' => 'sma_sales',
                    'translate' => '',
                    'select_condition' => "id > 0 and date LIKE '%{$condition}%' order by id desc",
                );
                $select_data = $this->site->api_select_data_v2($config_data);
                $total_sale_transaction = 0;
                $sale_adjustment = '';
                for($i=0;$i<count($select_data);$i++) {
                    $total_sale_transaction+=1;
                    $sale_adjustment .= '-'.$select_data[$i]['id'];
                } 
                if($total_sale_transaction == 0) {
                    $display_count_sale = '0';
                } else {
                    $display_count_sale = "<a href='".admin_url()."sales?adjustment_sale_track=".$sale_adjustment."' target='_blank'>". $total_sale_transaction. "</a>";
                }

                $daily_sale[$sale->date] = "
                    <table class='table table-bordered table-hover table-striped table-condensed data' style='margin:0;'>
                        <tr>
                            <td>" . lang("discount") . "</td>
                            <td>" . $this->sma->formatMoney($sale->discount) . "</td>
                        </tr>
                        <tr>
                            <td>" . lang("shipping") . "</td>
                            <td>" . $this->sma->formatMoney($sale->shipping) . "</td>
                        </tr>
                        <tr>
                            <td>" . lang("product_tax") . "</td>
                            <td>" . $this->sma->formatMoney($sale->tax1) . "</td>
                        </tr>
                        <tr>
                            <td>" . lang("order_tax") . "</td>
                            <td>" . $this->sma->formatMoney($sale->tax2) . "</td>
                        </tr>
                        <tr>
                            <td>". lang("Sales") . "</td>
                            <td>".$display_count_sale."</td>
                        </tr>
                        
                        <tr>
                            <td>" . lang("total") . "</td>
                            <td>" . $this->sma->formatMoney($sale->total) . "</td>
                        </tr>
                    </table>";
            }
        } else {
            $daily_sale = array();
        }
        $this->data['calender'] = $this->calendar->generate($year, $month, $daily_sale);
        $this->data['year'] = $year;
        $this->data['month'] = $month;
        if ($pdf) {
            $html = $this->load->view($this->theme . 'reports/daily', $this->data, true);
            $name = lang("daily_sales") . "_" . $year . "_" . $month . ".pdf";
            $html = str_replace('<p class="introtext">' . lang("reports_calendar_text") . '</p>', '', $html);
            $this->sma->generate_pdf($html, $name, null, null, null, null, null, 'L');
        }
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['warehouse_id'] = $warehouse_id;
        $this->data['sel_warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('daily_sales_report')));
        $meta = array('page_title' => lang('daily_sales_report'), 'bc' => $bc);
        $this->page_construct('reports/daily', $meta, $this->data);

    }

    function daily_sales_v2($warehouse_id = NULL, $year = NULL, $month = NULL, $pdf = NULL, $user_id = NULL)
    {
        $this->sma->checkPermissions();
        if (!$this->Owner && !$this->Admin && $this->session->userdata('warehouse_id')) {
            $warehouse_id = $this->session->userdata('warehouse_id');
        }
        if (!$year) {
            $year = date('Y');
        }
        if (!$month) {
            $month = date('m');
        }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $config = array(
            'show_next_prev' => TRUE,
            'next_prev_url' => admin_url('reports/daily_sales_v2/'.($warehouse_id ? $warehouse_id : 0)),
            'month_type' => 'long',
            'day_type' => 'long'
        );

        $config['template'] = '{table_open}<div class="table-responsive"><table border="0" cellpadding="0" cellspacing="0" class="table table-bordered dfTable">{/table_open}
        {heading_row_start}<tr>{/heading_row_start}
        {heading_previous_cell}<th><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
        {heading_title_cell}<th colspan="{colspan}" id="month_year">{heading}</th>{/heading_title_cell}
        {heading_next_cell}<th><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}
        {heading_row_end}</tr>{/heading_row_end}
        {week_row_start}<tr>{/week_row_start}
        {week_day_cell}<td class="cl_wday">{week_day}</td>{/week_day_cell}
        {week_row_end}</tr>{/week_row_end}
        {cal_row_start}<tr class="days">{/cal_row_start}
        {cal_cell_start}<td class="day">{/cal_cell_start}
        {cal_cell_content}
        <div class="day_num">{day}</div>
        <div class="content">{content}</div>
        {/cal_cell_content}
        {cal_cell_content_today}
        <div class="day_num highlight">{day}</div>
        <div class="content">{content}</div>
        {/cal_cell_content_today}
        {cal_cell_no_content}<div class="day_num">{day}</div>{/cal_cell_no_content}
        {cal_cell_no_content_today}<div class="day_num highlight">{day}</div>{/cal_cell_no_content_today}
        {cal_cell_blank}&nbsp;{/cal_cell_blank}
        {cal_cell_end}</td>{/cal_cell_end}
        {cal_row_end}</tr>{/cal_row_end}
        {table_close}</table></div>{/table_close}';

        $this->load->library('calendar', $config);
        //$sales = $user_id ? $this->reports_model->getStaffDailySales($user_id, $year, $month, $warehouse_id) : $this->reports_model->getDailySales($year, $month, $warehouse_id);

        $select_data = $this->reports_model->getDailySales_v2($year, $month, $warehouse_id);

        if (count($select_data)) {
            for ($i=0;$i<count($select_data);$i++) {
                $daily_sale[$select_data[$i]['date']] = '
                <table class="table table-bordered table-hover table-striped table-condensed data" style="margin:0;">
                <tr>
                    <td>
                        '.lang('Without_VAT').' 
                        <span class="fa fa-question-circle" data-trigger="hover" data-html="true" data-container="body" data-toggle="popover" data-placement="right" data-content="Total without VAT amount <br> from Yesterday 16:00 to Today 16:00" data-original-title="" title=""></span>
                    </td>
                    <td>
                        '.$this->sma->formatMoney($select_data[$i]['sum_total']).'
                    </td>
                </tr>
                <tr>
                    <td>
                        '.lang('VAT').' 
                        <span class="fa fa-question-circle" data-trigger="hover" data-html="true" data-container="body" data-toggle="popover" data-placement="right" data-content="Total VAT amount <br> from Yesterday 16:00 to Today 16:00" data-original-title="" title=""></span>                        
                    </td>
                    <td>
                        '.$this->sma->formatMoney($sale->shipping).'
                    </td>
                </tr>
                <tr>
                    <td>
                        '.lang('Collect_Today').' 
                        <span class="fa fa-question-circle" data-trigger="hover" data-html="true" data-container="body" data-toggle="popover" data-placement="right" data-content="Total amount of invoices that have changed payment to Paid from partial, due or pending <br> from Yesterday 16:00 to Today 16:00" data-original-title="" title=""></span>                             
                    </td>
                    <td>
                        '.$this->sma->formatMoney($sale->tax1).'
                    </td>
                </tr>
                <tr>
                    <td>
                        '.lang('Pending_to_Other').' 
                        <span class="fa fa-question-circle" data-trigger="hover" data-html="true" data-container="body" data-toggle="popover" data-placement="right" data-content="Total amount of invoices that have changed payment to partial, due or pending from Paid <br> from January 1st to Yesterday 16:00" data-original-title="" title=""></span>                           
                    </td>
                    <td>
                        '.$this->sma->formatMoney($sale->tax2).'
                    </td>
                </tr>
                <tr>
                    <td>
                        '.lang('Partial/Due Jan-Dec').' 
                        <span class="fa fa-question-circle" data-trigger="hover" data-html="true" data-container="body" data-toggle="popover" data-placement="right" data-content="Total amount <br>\'Sale report for each customer\' with payment status partial or due <br> from January to December" data-original-title="" title=""></span>                            
                    </td>
                    <td>
                        '.$this->sma->formatMoney($sale->total).'
                    </td>
                </tr>
                </table>
            ';
            }
        } else {
            $daily_sale = array();
        }

        $this->data['calender'] = $this->calendar->generate($year, $month, $daily_sale);
        $this->data['year'] = $year;
        $this->data['month'] = $month;
        if ($pdf) {
            $html = $this->load->view($this->theme . 'reports/daily', $this->data, true);
            $name = lang("daily_sales") . "_" . $year . "_" . $month . ".pdf";
            $html = str_replace('<p class="introtext">' . lang("reports_calendar_text") . '</p>', '', $html);
            $this->sma->generate_pdf($html, $name, null, null, null, null, null, 'L');
        }
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['warehouse_id'] = $warehouse_id;
        $this->data['sel_warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('Daily_Sales_2_Report')));
        $meta = array('page_title' => lang('Daily_Sales_2_Report'), 'bc' => $bc);
        $this->page_construct('reports/daily_v2', $meta, $this->data);

    }

    function monthly_sales($warehouse_id = NULL, $year = NULL, $pdf = NULL, $user_id = NULL)
    {
        $this->sma->checkPermissions();
        if (!$this->Owner && !$this->Admin && $this->session->userdata('warehouse_id')) {
            $warehouse_id = $this->session->userdata('warehouse_id');
        }
        if (!$year) {
            $year = date('Y');
        }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->load->language('calendar');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['year'] = $year;
        $this->data['sales'] = $user_id ? $this->reports_model->getStaffMonthlySales($user_id, $year, $warehouse_id) : $this->reports_model->getMonthlySales($year, $warehouse_id);
        if ($pdf) {
            $html = $this->load->view($this->theme . 'reports/monthly', $this->data, true);
            $name = lang("monthly_sales") . "_" . $year . ".pdf";
            $html = str_replace('<p class="introtext">' . lang("reports_calendar_text") . '</p>', '', $html);
            $this->sma->generate_pdf($html, $name, null, null, null, null, null, 'L');
        }
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['warehouse_id'] = $warehouse_id;
        $this->data['sel_warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('monthly_sales_report')));
        $meta = array('page_title' => lang('monthly_sales_report'), 'bc' => $bc);
        $this->page_construct('reports/monthly', $meta, $this->data);

    }

    function sales()
    {
        $this->sma->checkPermissions('sales');

        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : '';
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : '';
        $temp = explode(' ',$start_date);
        $start_date = $temp[0];
        $temp = explode(' ',$end_date);
        $end_date = $temp[0];

        $sort_by = $this->input->get('sort_by');
        $sort_order = $this->input->get('sort_order');        
        $per_page = $this->input->get('per_page') != '' ? $this->input->get('per_page') : 100;
        $page = $this->input->get('page') != '' ? $this->input->get('page') : 1;

        $offset_no = $page != '' ? ($page - 1 ) * $per_page : $offset_no;
        
        if ($page != '')
            $offset_no = ($page - 1 ) * $per_page;
        else
            $offset_no = 0;

        if ($page != '') $temp_url .= '?page=';
        if ($per_page != '') $temp_url .= '&per_page='.$per_page;
        if ($start_date != '') $temp_url .= '&start_date='.$start_date;
        if ($end_date != '') $temp_url .= '&end_date='.$end_date;

        foreach($_GET as $name => $value) {
            if ($name != 'page' && $name != 'per_page' && $name != 'start_date' && $name != 'end_date' && $name != 'val') {
                $value = $this->input->get($name);
                if ($value != '') $temp_url .= '&'.$name.'='.$value;
            }            
        }

        $config_data = array(
            'lg' => 'en',
            'table_name' => 'sma_sales',
            'table_id' => 'id',
            'limit' => $per_page,
            'offset_no' => $offset_no,
            'start_date' => $start_date,
            'end_date' => $end_date,
        );
        $select_data = $this->reports_model->api_get_sale($config_data);

        $config_data = array(
            'lg' => 'en',
            'table_name' => 'sma_sales',
            'table_id' => 'id',
            'limit' => '',
            'offset_no' => '',
            'start_date' => $start_date,
            'end_date' => $end_date,
        );
        $temp_count = $this->reports_model->api_get_sale($config_data);
        $total_record = count($temp_count);

        /* config pagination */
        $this->data['select_data'] = $select_data;

        $this->data['page'] = $page;
        $this->data['search'] = $search;
        $this->data['sort_by'] = $sort_by;
        $this->data['sort_order'] = $sort_order;
        $this->data['per_page'] = $per_page;
        $this->data['total_rows_sale'] = $total_record;
        $this->data['index_page'] = 'admin/reports/sales';
        $this->data['show_data'] = $page;

        $config_base_url = site_url() . 'admin/reports/sales'; //your url where the content is displayed
        $config['base_url'] = $config_base_url.$temp_url; //your url where the content is displayed
        $config['query_string_segment'] = 'page';
        $config['per_page'] = $per_page; //$per_page;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['full_tag_open'] = '<ul class="pagination pagination-new-bg">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['prev_link'] = '&lt; Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next &gt;';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['total_rows'] = $total_record; //$this->data['total_rows_sale'];        
        $this->load->library('pagination');   
        $this->pagination->initialize($config);
        /** end config pagination */
        /**Showing Label Data At Table Footer */
        $showing = $total_record != '' ? 1 : 0;
        $label_show_to= ($page == '' || $per_page < $total_record) ?  $per_page  :  $total_record;
        $label_show = "Showing " . $showing . " to ".$label_show_to." of : " . number_format($total_record) . " entries";

        $show_page = $page > 1 ? ($page * $per_page) : $total_record;
        $show_page_no = $show_page > $total_record ? $total_record : $page * $per_page;
        $label_show_page = "Showing " . (($page - 1) * ($per_page) + 1) . " to " . $show_page_no . " (filtered from " . number_format($total_record) . " total entries) ";
        $this->data['show'] = $search != '' || $page > 1 ? $label_show_page : $label_show;

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $temp_warhouse = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_warehouses"
            ,"id > 0 order by id asc"
            ,"arr"
        );
        $this->data['api_warehouse'] = $temp_warhouse;

        /*
        $temp = $this->site->api_select_some_fields_with_where("
            id, name, company   
            "
            ,"sma_companies"
            ,"id > 0 order by company asc"
            ,"arr"
        );
        $this->data['api_customer'] = $temp;
        */

        $temp = $this->site->api_select_some_fields_with_where("
            id, company   
            "
            ,"sma_companies"
            ,"id > 0 and group_name = 'biller' order by company asc"
            ,"arr"
        );
        $this->data['api_biller'] = $temp;

        $temp = $this->site->api_select_some_fields_with_where("
            id, company, name
            "
            ,"sma_companies"
            ,"id > 0 and group_name = 'biller' order by company asc"
            ,"arr"
        );
        $this->data['api_biller'] = $temp;

        $temp = $this->site->api_select_some_fields_with_where("
            id, company, name
            "
            ,"sma_companies"
            ,"id > 0 and group_name = 'supplier' order by company asc"
            ,"arr"
        );
        $this->data['api_supplier'] = $temp;        

        $temp = $this->site->api_select_some_fields_with_where("
            *, concat(first_name,' ',last_name) as fullname
            "
            ,"sma_users"
            ,"id > 0 order by fullname asc"
            ,"arr"
        );
        $this->data['api_user'] = $temp;
/*
        $temp = $this->site->api_select_some_fields_with_where("
            id, concat(name,' (',code,')') as product_name   
            "
            ,"sma_products"
            ,"id > 0 order by product_name asc"
            ,"arr"
        );
        $this->data['api_product'] = $temp;
*/

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('sales_report')));
        $meta = array('page_title' => lang('sales_report'), 'bc' => $bc);
        $this->page_construct('reports/sales', $meta, $this->data);
    }


    function getSalesReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('sales', TRUE);
        $product = $this->input->get('product') ? $this->input->get('product') : NULL;
        $user = $this->input->get('user') ? $this->input->get('user') : NULL;
        $customer = $this->input->get('customer') ? $this->input->get('customer') : NULL;
        $biller = $this->input->get('biller') ? $this->input->get('biller') : NULL;
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $reference_no = $this->input->get('reference_no') ? $this->input->get('reference_no') : NULL;
        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;
        $serial = $this->input->get('serial') ? $this->input->get('serial') : NULL;

        if ($start_date) {
            $start_date = $this->sma->fld($start_date);
            $end_date = $this->sma->fld($end_date);
        }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user = $this->session->userdata('user_id');
        }

        if ($pdf || $xls) {

            $this->db
                ->select("sma_sale_items.product_id, sma_sales.id as id, date, reference_no, biller, customer, GROUP_CONCAT(CONCAT(" . $this->db->dbprefix('sale_items') . ".product_name, ' (', " . $this->db->dbprefix('sale_items') . ".quantity, ')') SEPARATOR '\n') as iname,".$this->db->dbprefix('sale_items').".product_name, ".$this->db->dbprefix('sale_items').".quantity, grand_total, paid, payment_status, total_tax", FALSE)
                ->from('sales')
                ->join('sale_items', 'sale_items.sale_id=sales.id', 'left')
                ->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left')
                ->group_by('sales.id')
                ->order_by('sales.date desc');

            if ($user) {
                $this->db->where('sales.created_by', $user);
            }
            if ($product) {
                $this->db->where('sale_items.product_id', $product);
            }
            if ($serial) {
                $this->db->like('sale_items.serial_no', $serial);
            }
            if ($biller) {
                $this->db->where('sales.biller_id', $biller);
            }
            if ($customer) {
                $this->db->where('sales.customer_id', $customer);
            }
            if ($warehouse) {
                $this->db->where('sales.warehouse_id', $warehouse);
            }
            if ($reference_no) {
                $this->db->like('sales.reference_no', $reference_no, 'both');
            }
            if ($start_date) {
                $this->db->where($this->db->dbprefix('sales').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            $q = $this->db->get();
            if ($q != false && $q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('sales_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no').'=');
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('customer'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('product'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('unit_price'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('quantity'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('subtotal'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('Vat_10%'));
                $this->excel->getActiveSheet()->SetCellValue('I1', lang('grand_total'));
                $this->excel->getActiveSheet()->SetCellValue('J1', lang('paid'));
                $this->excel->getActiveSheet()->SetCellValue('K1', lang('balance'));
                $this->excel->getActiveSheet()->SetCellValue('L1', lang('payment_status'));
                $this->excel->getActiveSheet()->getStyle('E1:K1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $this->excel->getActiveSheet()->getStyle('L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('L')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


                $row = 2;
                $row_2 = 1;
                $total = 0;
                $paid = 0;
                $balance = 0;
                $total_row = $q->num_rows();
                foreach ($data as $data_row) {
                    $temp = $this->site->api_select_some_fields_with_where("*
                        "
                        ,"sma_sale_items"
                        ,"sale_id = ".$data_row->id." order by id asc"
                        ,"arr"
                    );
                    if (count($temp) > 1) {
                        $temp_row = $row;
                        $temp_row_2 = $row + count($temp);
                        for ($i=0;$i<count($temp);$i++) {
                            if ($temp[$i]['product_name'] != '') {
                            $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                            $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                            $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->customer);
                            $this->excel->getActiveSheet()->SetCellValue('D' . $row, $temp[$i]['product_name']);

                            $this->excel->getActiveSheet()->SetCellValue('E' . $row, $temp[$i]['unit_price']);

                            $this->excel->getActiveSheet()->SetCellValue('F' . $row, $temp[$i]['quantity']);
                            $this->excel->getActiveSheet()->SetCellValue('G' . $row, $temp[$i]['subtotal']);
                            $this->excel->getActiveSheet()->SetCellValue('H' . $row, $data_row->total_tax);
                            $this->excel->getActiveSheet()->SetCellValue('I' . $row, $data_row->grand_total);
                            $this->excel->getActiveSheet()->SetCellValue('J' . $row, $data_row->paid);
                            $this->excel->getActiveSheet()->SetCellValue('K' . $row, ($data_row->grand_total - $data_row->paid));
                            $this->excel->getActiveSheet()->SetCellValue('L' . $row, lang($data_row->payment_status));
                            $this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setWrapText(true);
                            $row++;
                            }
                        }                        

                        if ($total_row >= $row_2)
                            $temp_row_22 = $temp_row_2 - 1;
                        else
                            $temp_row_22 = $temp_row_2;

                        $this->excel->getActiveSheet()->mergeCells('A'.$temp_row.':A'.$temp_row_22);
                        $this->excel->getActiveSheet()->mergeCells('B'.$temp_row.':B'.$temp_row_22);
                        $this->excel->getActiveSheet()->mergeCells('C'.$temp_row.':C'.$temp_row_22);                        
                        $this->excel->getActiveSheet()->mergeCells('H'.$temp_row.':H'.$temp_row_22);
                        $this->excel->getActiveSheet()->mergeCells('I'.$temp_row.':I'.$temp_row_22);
                        $this->excel->getActiveSheet()->mergeCells('J'.$temp_row.':J'.$temp_row_22);
                        $this->excel->getActiveSheet()->mergeCells('K'.$temp_row.':K'.$temp_row_22);
                        $this->excel->getActiveSheet()->mergeCells('L'.$temp_row.':L'.$temp_row_22);
                        $this->excel->getActiveSheet()->getStyle("A".$temp_row.":L".$temp_row)->getBorders()
                            ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
                    }
                    else {
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->customer);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $temp[0]['product_name']);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $temp[0]['unit_price']);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $temp[0]['quantity']);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $temp[0]['subtotal']);
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $data_row->total_tax);
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $data_row->grand_total);
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $data_row->paid);
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, ($data_row->grand_total - $data_row->paid));
                        $this->excel->getActiveSheet()->SetCellValue('L' . $row, lang($data_row->payment_status));
                        $this->excel->getActiveSheet()->getStyle("A".$row.":L".$row)->getBorders()
                            ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
                        $row++;
                    }
                    
                    $total += $data_row->grand_total;
                    $paid += $data_row->paid;
                    $balance += ($data_row->grand_total - $data_row->paid);
                    $row_2++;
                }
                $this->excel->getActiveSheet()->getStyle("I" . $row . ":K" . $row)->getBorders()
                    ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                $this->excel->getActiveSheet()->SetCellValue('I' . $row, $total);
                $this->excel->getActiveSheet()->SetCellValue('J' . $row, $paid);
                $this->excel->getActiveSheet()->SetCellValue('K' . $row, $balance);

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(50); 
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);

                $this->excel->getActiveSheet()->getStyle('A1:C'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('E1:L'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                $this->excel->getActiveSheet()->getStyle('D1:D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);   

                $this->excel->getActiveSheet()->getStyle('C1:D'.$row)->getAlignment()->setWrapText(true);

                $filename = 'sales_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $si = "( SELECT sale_id, product_id, serial_no, GROUP_CONCAT(CONCAT({$this->db->dbprefix('sale_items')}.product_name, '__', {$this->db->dbprefix('sale_items')}.quantity) SEPARATOR '___') as item_nane from {$this->db->dbprefix('sale_items')} ";
            if ($product || $serial) { $si .= " WHERE "; }
            if ($product) {
                $si .= " {$this->db->dbprefix('sale_items')}.product_id = {$product} ";
            }
            if ($product && $serial) { $si .= " AND "; }
            if ($serial) {
                $si .= " {$this->db->dbprefix('sale_items')}.serial_no LIKe '%{$serial}%' ";
            }
            $si .= " GROUP BY {$this->db->dbprefix('sale_items')}.sale_id ) FSI";
            $this->load->library('datatables');
            $this->datatables
                ->select("DATE_FORMAT(date, '%Y-%m-%d %T') as date, reference_no, biller, customer, FSI.item_nane as iname, grand_total, paid, (grand_total-paid) as balance, payment_status, {$this->db->dbprefix('sales')}.id as id", FALSE)
                ->from('sales')
                ->join($si, 'FSI.sale_id=sales.id', 'left')
                ->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left');
                // ->group_by('sales.id');

            if ($user) {
                $this->datatables->where('sales.created_by', $user);
            }
            if ($product) {
                $this->datatables->where('FSI.product_id', $product);
            }
            if ($serial) {
                $this->datatables->like('FSI.serial_no', $serial);
            }
            if ($biller) {
                $this->datatables->where('sales.biller_id', $biller);
            }
            if ($customer) {
                $this->datatables->where('sales.customer_id', $customer);
            }
            if ($warehouse) {
                $this->datatables->where('sales.warehouse_id', $warehouse);
            }
            if ($reference_no) {
                $this->datatables->like('sales.reference_no', $reference_no, 'both');
            }
            if ($start_date) {
                $this->datatables->where($this->db->dbprefix('sales').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            echo $this->datatables->generate();

        }

    }

    function getQuotesReport($pdf = NULL, $xls = NULL)
    {

        if ($this->input->get('product')) {
            $product = $this->input->get('product');
        } else {
            $product = NULL;
        }
        if ($this->input->get('user')) {
            $user = $this->input->get('user');
        } else {
            $user = NULL;
        }
        if ($this->input->get('customer')) {
            $customer = $this->input->get('customer');
        } else {
            $customer = NULL;
        }
        if ($this->input->get('biller')) {
            $biller = $this->input->get('biller');
        } else {
            $biller = NULL;
        }
        if ($this->input->get('warehouse')) {
            $warehouse = $this->input->get('warehouse');
        } else {
            $warehouse = NULL;
        }
        if ($this->input->get('reference_no')) {
            $reference_no = $this->input->get('reference_no');
        } else {
            $reference_no = NULL;
        }
        if ($this->input->get('start_date')) {
            $start_date = $this->input->get('start_date');
        } else {
            $start_date = NULL;
        }
        if ($this->input->get('end_date')) {
            $end_date = $this->input->get('end_date');
        } else {
            $end_date = NULL;
        }
        if ($start_date) {
            $start_date = $this->sma->fld($start_date);
            $end_date = $this->sma->fld($end_date);
        }
        if ($pdf || $xls) {

            $this->db
                ->select("date, reference_no, biller, customer, GROUP_CONCAT(CONCAT(" . $this->db->dbprefix('quote_items') . ".product_name, ' (', " . $this->db->dbprefix('quote_items') . ".quantity, ')') SEPARATOR '<br>') as iname, grand_total, status", FALSE)
                ->from('quotes')
                ->join('quote_items', 'quote_items.quote_id=quotes.id', 'left')
                ->join('warehouses', 'warehouses.id=quotes.warehouse_id', 'left')
                ->group_by('quotes.id');

            if ($user) {
                $this->db->where('quotes.created_by', $user);
            }
            if ($product) {
                $this->db->where('quote_items.product_id', $product);
            }
            if ($biller) {
                $this->db->where('quotes.biller_id', $biller);
            }
            if ($customer) {
                $this->db->where('quotes.customer_id', $customer);
            }
            if ($warehouse) {
                $this->db->where('quotes.warehouse_id', $warehouse);
            }
            if ($reference_no) {
                $this->db->like('quotes.reference_no', $reference_no, 'both');
            }
            if ($start_date) {
                $this->db->where($this->db->dbprefix('quotes').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            $q = $this->db->get();
            if ($q != false && $q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('quotes_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('biller'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('customer'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('product_qty'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('grand_total'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('status'));

                $row = 2;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->biller);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->customer);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->iname);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->grand_total);
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->status);
                    $row++;
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('E2:E' . $row)->getAlignment()->setWrapText(true);
                $filename = 'quotes_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $qi = "( SELECT quote_id, product_id, GROUP_CONCAT(CONCAT({$this->db->dbprefix('quote_items')}.product_name, '__', {$this->db->dbprefix('quote_items')}.quantity) SEPARATOR '___') as item_nane from {$this->db->dbprefix('quote_items')} ";
            if ($product) {
                $qi .= " WHERE {$this->db->dbprefix('quote_items')}.product_id = {$product} ";
            }
            $qi .= " GROUP BY {$this->db->dbprefix('quote_items')}.quote_id ) FQI";
            $this->load->library('datatables');
            $this->datatables
                ->select("date, reference_no, biller, customer, FQI.item_nane as iname, grand_total, status, {$this->db->dbprefix('quotes')}.id as id", FALSE)
                ->from('quotes')
                ->join($qi, 'FQI.quote_id=quotes.id', 'left')
                ->join('warehouses', 'warehouses.id=quotes.warehouse_id', 'left')
                ->group_by('quotes.id');

            if ($user) {
                $this->datatables->where('quotes.created_by', $user);
            }
            if ($product) {
                $this->datatables->where('FQI.product_id', $product, FALSE);
            }
            if ($biller) {
                $this->datatables->where('quotes.biller_id', $biller);
            }
            if ($customer) {
                $this->datatables->where('quotes.customer_id', $customer);
            }
            if ($warehouse) {
                $this->datatables->where('quotes.warehouse_id', $warehouse);
            }
            if ($reference_no) {
                $this->datatables->like('quotes.reference_no', $reference_no, 'both');
            }
            if ($start_date) {
                $this->datatables->where($this->db->dbprefix('quotes').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            echo $this->datatables->generate();

        }

    }

    function getTransfersReport($pdf = NULL, $xls = NULL)
    {
        if ($this->input->get('product')) {
            $product = $this->input->get('product');
        } else {
            $product = NULL;
        }

        if ($pdf || $xls) {

            $this->db
                ->select($this->db->dbprefix('transfers') . ".date, transfer_no, (CASE WHEN " . $this->db->dbprefix('transfers') . ".status = 'completed' THEN  GROUP_CONCAT(CONCAT(" . $this->db->dbprefix('purchase_items') . ".product_name, ' (', " . $this->db->dbprefix('purchase_items') . ".quantity, ')') SEPARATOR '<br>') ELSE GROUP_CONCAT(CONCAT(" . $this->db->dbprefix('transfer_items') . ".product_name, ' (', " . $this->db->dbprefix('transfer_items') . ".quantity, ')') SEPARATOR '<br>') END) as iname, from_warehouse_name as fname, from_warehouse_code as fcode, to_warehouse_name as tname,to_warehouse_code as tcode, grand_total, " . $this->db->dbprefix('transfers') . ".status")
                ->from('transfers')
                ->join('transfer_items', 'transfer_items.transfer_id=transfers.id', 'left')
                ->join('purchase_items', 'purchase_items.transfer_id=transfers.id', 'left')
                ->group_by('transfers.id')->order_by('transfers.date desc');
            if ($product) {
                $this->db->where($this->db->dbprefix('purchase_items') . ".product_id", $product);
                $this->db->or_where($this->db->dbprefix('transfer_items') . ".product_id", $product);
            }

            $q = $this->db->get();
            if ($q != false && $q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('transfers_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('transfer_no'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('product_qty'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('warehouse') . ' (' . lang('from') . ')');
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('warehouse') . ' (' . lang('to') . ')');
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('grand_total'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('status'));

                $row = 2;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->transfer_no);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->iname);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->fname . ' (' . $data_row->fcode . ')');
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->tname . ' (' . $data_row->tcode . ')');
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->grand_total);
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->status);
                    $row++;
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('C2:C' . $row)->getAlignment()->setWrapText(true);
                $filename = 'transfers_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $this->load->library('datatables');
            $this->datatables
                ->select("{$this->db->dbprefix('transfers')}.date, transfer_no, (CASE WHEN {$this->db->dbprefix('transfers')}.status = 'completed' THEN  GROUP_CONCAT(CONCAT({$this->db->dbprefix('purchase_items')}.product_name, '__', {$this->db->dbprefix('purchase_items')}.quantity) SEPARATOR '___') ELSE GROUP_CONCAT(CONCAT({$this->db->dbprefix('transfer_items')}.product_name, '__', {$this->db->dbprefix('transfer_items')}.quantity) SEPARATOR '___') END) as iname, from_warehouse_name as fname, from_warehouse_code as fcode, to_warehouse_name as tname,to_warehouse_code as tcode, grand_total, {$this->db->dbprefix('transfers')}.status, {$this->db->dbprefix('transfers')}.id as id", FALSE)
                ->from('transfers')
                ->join('transfer_items', 'transfer_items.transfer_id=transfers.id', 'left')
                ->join('purchase_items', 'purchase_items.transfer_id=transfers.id', 'left')
                ->group_by('transfers.id');
            if ($product) {
                $this->datatables->where(" (({$this->db->dbprefix('purchase_items')}.product_id = {$product}) OR ({$this->db->dbprefix('transfer_items')}.product_id = {$product})) ", NULL, FALSE);
            }
            $this->datatables->edit_column("fname", "$1 ($2)", "fname, fcode")
                ->edit_column("tname", "$1 ($2)", "tname, tcode")
                ->unset_column('fcode')
                ->unset_column('tcode');
            echo $this->datatables->generate();

        }

    }

    function purchases()
    {
        $this->sma->checkPermissions('purchases');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('purchases_report')));
        $meta = array('page_title' => lang('purchases_report'), 'bc' => $bc);
        $this->page_construct('reports/purchases', $meta, $this->data);
    }

    function getPurchasesReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('purchases', TRUE);

        $mode = $this->input->get('mode');

        $product = $this->input->get('product') ? $this->input->get('product') : NULL;
        $user = $this->input->get('user') ? $this->input->get('user') : NULL;
        $supplier = $this->input->get('supplier') ? $this->input->get('supplier') : NULL;
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $reference_no = $this->input->get('reference_no') ? $this->input->get('reference_no') : NULL;
        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;

        if ($start_date) {
            $start_date = $this->sma->fld($start_date);
            $end_date = $this->sma->fld($end_date);
        }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user = $this->session->userdata('user_id');
        }

        if ($pdf || $xls) {

            $this->db
                ->select("getTranslate(sma_purchases.add_ons,'consignment_status','".f_separate."','".v_separate."') as consignment_status, ".$this->db->dbprefix('purchases').".id, ".$this->db->dbprefix('purchases').".warehouse_id, " . $this->db->dbprefix('purchases') . ".date, reference_no, " . $this->db->dbprefix('warehouses') . ".name as wname, supplier, GROUP_CONCAT(CONCAT(" . $this->db->dbprefix('purchase_items') . ".product_name, ' (', " . $this->db->dbprefix('purchase_items') . ".quantity, ')') SEPARATOR '\n') as iname, grand_total, paid, " . $this->db->dbprefix('purchases') . ".status", FALSE)
                ->from('purchases')
                ->join('purchase_items', 'purchase_items.purchase_id=purchases.id', 'left')
                ->join('warehouses', 'warehouses.id=purchases.warehouse_id', 'left')
                ->group_by('purchases.id')
                ->order_by('purchases.date desc');

            if ($mode == 'consignment')
                $this->db->where("getTranslate(sma_purchases.add_ons,'consignment_status','".f_separate."','".v_separate."') != ''");
            else
                $this->db->where("getTranslate(sma_purchases.add_ons,'consignment_status','".f_separate."','".v_separate."') = ''");

            if ($user) {
                $this->db->where('purchases.created_by', $user);
            }
            if ($product) {
                $this->db->where('purchase_items.product_id', $product);
            }
            if ($supplier) {
                $this->db->where('purchases.supplier_id', $supplier);
            }
            if ($warehouse) {
                $this->db->where('purchases.warehouse_id', $warehouse);
            }
            if ($reference_no) {
                $this->db->like('purchases.reference_no', $reference_no, 'both');
            }
            if ($start_date) {
                $this->db->where($this->db->dbprefix('purchases').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            $q = $this->db->get();
            if ($q != false && $q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data) && $mode != 'consignment') {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('purchase_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('warehouse'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('supplier'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('product_name'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('Net_Unit_Cost'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('product_qty'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('Total'));
                $this->excel->getActiveSheet()->SetCellValue('I1', lang('grand_total'));
                $this->excel->getActiveSheet()->SetCellValue('J1', lang('paid'));
                $this->excel->getActiveSheet()->SetCellValue('K1', lang('balance'));
                $this->excel->getActiveSheet()->SetCellValue('L1', lang('status'));


                $row = 2;
                $row_2 = 1;
                $total = 0;
                $paid = 0;
                $balance = 0;
                $total_row = $q->num_rows();
                foreach ($data as $data_row) {
                    $temp_2 = $this->site->api_select_some_fields_with_where("name
                        "
                        ,"sma_warehouses"
                        ,"id = ".$data_row->warehouse_id
                        ,"arr"
                    );

                    $temp = $this->site->api_select_some_fields_with_where("*
                        "
                        ,"sma_purchase_items"
                        ,"purchase_id = ".$data_row->id." order by id asc"
                        ,"arr"
                    );
                    if (count($temp) > 1) {
                        $temp_row = $row;
                        $temp_row_2 = $row + count($temp);
                        for ($i=0;$i<count($temp);$i++) {
                            if ($temp[$i]['product_name'] != '') {

                            $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                            $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                            $this->excel->getActiveSheet()->SetCellValue('C' . $row, $temp_2[0]['name']);
                            $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->supplier);
                            $this->excel->getActiveSheet()->SetCellValue('E' . $row, $temp[$i]['product_name']);
                            $this->excel->getActiveSheet()->SetCellValue('F' . $row, $temp[$i]['net_unit_cost']);
                            $this->excel->getActiveSheet()->SetCellValue('G' . $row, $temp[$i]['quantity']);
                            $this->excel->getActiveSheet()->SetCellValue('H' . $row, $temp[$i]['subtotal']);
                            $this->excel->getActiveSheet()->SetCellValue('I' . $row, $data_row->grand_total);
                            $this->excel->getActiveSheet()->SetCellValue('J' . $row, $data_row->paid);
                            $this->excel->getActiveSheet()->SetCellValue('K' . $row, ($data_row->grand_total - $data_row->paid));
                            $this->excel->getActiveSheet()->SetCellValue('L' . $row, $data_row->status);
                            $row++;
                            }
                        }                        

                        if ($total_row >= $row_2)
                            $temp_row_22 = $temp_row_2 - 1;
                        else
                            $temp_row_22 = $temp_row_2;

                        $this->excel->getActiveSheet()->mergeCells('A'.$temp_row.':A'.$temp_row_22);
                        $this->excel->getActiveSheet()->mergeCells('B'.$temp_row.':B'.$temp_row_22);
                        $this->excel->getActiveSheet()->mergeCells('C'.$temp_row.':C'.$temp_row_22);                        
                        $this->excel->getActiveSheet()->mergeCells('D'.$temp_row.':D'.$temp_row_22);                        
                        $this->excel->getActiveSheet()->mergeCells('I'.$temp_row.':I'.$temp_row_22);
                        $this->excel->getActiveSheet()->mergeCells('J'.$temp_row.':J'.$temp_row_22);
                        $this->excel->getActiveSheet()->mergeCells('K'.$temp_row.':K'.$temp_row_22);
                        $this->excel->getActiveSheet()->mergeCells('L'.$temp_row.':L'.$temp_row_22);
                        $this->excel->getActiveSheet()->getStyle("A".$temp_row.":L".$temp_row)->getBorders()
                            ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
                    }
                    else {
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $temp_2[0]['name']);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->supplier);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $temp[0]['product_name']);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $temp[0]['net_unit_cost']);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $temp[0]['quantity']);
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $temp[0]['subtotal']);
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $data_row->grand_total);
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $data_row->paid);
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, ($data_row->grand_total - $data_row->paid));
                        $this->excel->getActiveSheet()->SetCellValue('L' . $row, $data_row->status);
                        $this->excel->getActiveSheet()->getStyle("A".$row.":L".$row)->getBorders()
                            ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
                        $row++;
                    }
                    
                    $total += $data_row->grand_total;
                    $paid += $data_row->paid;
                    $balance += ($data_row->grand_total - $data_row->paid);
                    $row_2++;
                }

                $this->excel->getActiveSheet()->getStyle("I" . $row . ":K" . $row)->getBorders()
                    ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                $this->excel->getActiveSheet()->SetCellValue('I' . $row, $total);
                $this->excel->getActiveSheet()->SetCellValue('J' . $row, $paid);
                $this->excel->getActiveSheet()->SetCellValue('K' . $row, $balance);


                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(25); 
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);

                $this->excel->getActiveSheet()->getStyle('A1:D'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('E1:L'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                $this->excel->getActiveSheet()->getStyle('E1:E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);   

                $this->excel->getActiveSheet()->getStyle('L1:L'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                $this->excel->getActiveSheet()->getStyle('C1:E'.$row)->getAlignment()->setWrapText(true);

                $this->excel->getActiveSheet()->getStyle('I1:K'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

                $filename = 'purchase_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }

            if (!empty($data) && $mode == 'consignment') {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('Consignment_Purchase_Report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('supplier'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('product_name'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('Net_Unit_Cost'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('Quantity'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('Sold_Qty'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('Remained_Qty'));
                $this->excel->getActiveSheet()->SetCellValue('I1', lang('Returned_Qty'));
                $this->excel->getActiveSheet()->SetCellValue('J1', lang('grand_total'));
                $this->excel->getActiveSheet()->SetCellValue('K1', lang('Sold_Total'));
                $this->excel->getActiveSheet()->SetCellValue('L1', lang('Remained_Total'));
                $this->excel->getActiveSheet()->SetCellValue('M1', lang('status'));


                $row = 2;
                $row_2 = 1;
                $total = 0;
                $paid = 0;
                $balance = 0;
                $total_row = $q->num_rows();
                foreach ($data as $data_row) {
                    $temp_2 = $this->site->api_select_some_fields_with_where("name
                        "
                        ,"sma_warehouses"
                        ,"id = ".$data_row->warehouse_id
                        ,"arr"
                    );

                    $temp = $this->site->api_select_some_fields_with_where("*
                        "
                        ,"sma_purchase_items"
                        ,"purchase_id = ".$data_row->id." order by id asc"
                        ,"arr"
                    );


                    if (count($temp) > 1) {
                        $temp_row = $row;
                        $temp_row_2 = $row + count($temp);
                        for ($i=0;$i<count($temp);$i++) {
                            if ($temp[$i]['product_name'] != '') {

$temp_5 = $this->site->api_select_some_fields_with_where("
    quantity
    "
    ,"sma_consignment_purchase_items"
    ,"purchase_id = ".$temp[$i]['purchase_id']." and product_id = ".$temp[$i]['product_id']
    ,"arr"
);
$temp_sold_quantity = 0;
for ($i5=0;$i5<count($temp_5);$i5++) {
    $temp_sold_quantity = $temp_sold_quantity + $temp_5[$i5]['quantity'];
}

$temp_return = $this->site->api_select_some_fields_with_where("
id
"
,"sma_returns"
,"getTranslate(add_ons,'purchase_id','".f_separate."','".v_separate."') = '".$temp[$i]['purchase_id']."'"
,"arr"
);             
$temp_returned_quantity = 0;
for ($i2=0;$i2<count($temp_return);$i2++) {
    $temp_3 = $this->site->api_select_some_fields_with_where("
        SUM(quantity) as returned_quantity  
        "
        ,"sma_return_items"
        ,"return_id = ".$temp_return[$i2]['id']." and product_id = ".$temp[$i]['product_id']
        ,"arr"
    );    
    $temp_returned_quantity = $temp_returned_quantity + $temp_3[0]['returned_quantity'];
}
$temp_remained_quantity = $temp[$i]['quantity'] - ($temp_sold_quantity + $temp_returned_quantity);            

$temp_sold_total = $temp_sold_quantity * $temp[$i]['net_unit_cost'];
$temp_remained_total = $temp_remained_quantity * $temp[$i]['net_unit_cost'];
$temp_returned_total = $temp_returned_quantity * $temp[$i]['net_unit_cost'];
$grand_sold = $grand_sold + $temp_sold_total;
$grand_remain = $grand_remain + $temp_remained_total;
$grand_return = $grand_return + $temp_returned_total;

                                $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                                $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                                $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->supplier);
                                $this->excel->getActiveSheet()->SetCellValue('D' . $row, $temp[$i]['product_name']);
                                $this->excel->getActiveSheet()->SetCellValue('E' . $row, $temp[$i]['net_unit_cost']);
                                $this->excel->getActiveSheet()->SetCellValue('F' . $row, $temp[$i]['quantity']);
                                $this->excel->getActiveSheet()->SetCellValue('G' . $row, $temp_sold_quantity);
                                $this->excel->getActiveSheet()->SetCellValue('H' . $row, $temp_remained_quantity);
                                $this->excel->getActiveSheet()->SetCellValue('I' . $row, $temp_returned_quantity);
                                $this->excel->getActiveSheet()->SetCellValue('J' . $row, $data_row->grand_total);
                                $this->excel->getActiveSheet()->SetCellValue('K' . $row, $temp_sold_total);
                                $this->excel->getActiveSheet()->SetCellValue('L' . $row, $temp_remained_total);
                                $this->excel->getActiveSheet()->SetCellValue('M' . $row, $data_row->consignment_status);
                                $row++;
                            }
                        }                        

                        if ($total_row >= $row_2)
                            $temp_row_22 = $temp_row_2 - 1;
                        else
                            $temp_row_22 = $temp_row_2;


                        $this->excel->getActiveSheet()->mergeCells('A'.$temp_row.':A'.$temp_row_22);
                        $this->excel->getActiveSheet()->mergeCells('B'.$temp_row.':B'.$temp_row_22);
                        $this->excel->getActiveSheet()->mergeCells('C'.$temp_row.':C'.$temp_row_22);                        
                        $this->excel->getActiveSheet()->mergeCells('M'.$temp_row.':M'.$temp_row_22);
                        $this->excel->getActiveSheet()->getStyle("A".$temp_row.":M".$temp_row)->getBorders()
                            ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
                    }
                    else {

$temp_5 = $this->site->api_select_some_fields_with_where("
    quantity
    "
    ,"sma_consignment_purchase_items"
    ,"purchase_id = ".$temp[0]['purchase_id']." and product_id = ".$temp[0]['product_id']
    ,"arr"
);
$temp_sold_quantity = 0;
for ($i5=0;$i5<count($temp_5);$i5++) {
    $temp_sold_quantity = $temp_sold_quantity + $temp_5[$i5]['quantity'];
}

$temp_return = $this->site->api_select_some_fields_with_where("
id
"
,"sma_returns"
,"getTranslate(add_ons,'purchase_id','".f_separate."','".v_separate."') = '".$temp[0]['purchase_id']."'"
,"arr"
);             
$temp_returned_quantity = 0;
for ($i2=0;$i2<count($temp_return);$i2++) {
    $temp_3 = $this->site->api_select_some_fields_with_where("
        SUM(quantity) as returned_quantity  
        "
        ,"sma_return_items"
        ,"return_id = ".$temp_return[$i2]['id']." and product_id = ".$temp[0]['product_id']
        ,"arr"
    );    
    $temp_returned_quantity = $temp_returned_quantity + $temp_3[0]['returned_quantity'];
}

$temp_remained_quantity = $temp[$i]['quantity'] - ($temp_sold_quantity + $temp_returned_quantity);            

$temp_sold_total = $temp_sold_quantity * $temp[0]['net_unit_cost'];
$temp_remained_total = $temp_remained_quantity * $temp[0]['net_unit_cost'];
$temp_returned_total = $temp_returned_quantity * $temp[0]['net_unit_cost'];
$grand_sold = $grand_sold + $temp_sold_total;
$grand_remain = $grand_remain + $temp_remained_total;
$grand_return = $grand_return + $temp_returned_total;

                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->supplier);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $temp[0]['product_name']);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $temp[0]['net_unit_cost']);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $temp[0]['quantity']);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $temp_sold_quantity);
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $temp_remained_quantity);
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $temp_returned_quantity);
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $data_row->grand_total);
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, $temp_sold_total);
                        $this->excel->getActiveSheet()->SetCellValue('L' . $row, $temp_remained_total);
                        $this->excel->getActiveSheet()->SetCellValue('M' . $row, $data_row->consignment_status);


                        $this->excel->getActiveSheet()->getStyle("A".$row.":P".$row)->getBorders()
                            ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
                        $row++;
                    }
                    
                    $total += $data_row->grand_total;
                    $paid += $data_row->paid;
                    $balance += ($data_row->grand_total - $data_row->paid);

                    $row_2++;
                }

                $this->excel->getActiveSheet()->SetCellValue('J' . $row, $total);
                $this->excel->getActiveSheet()->SetCellValue('K' . $row, $grand_sold);
                $this->excel->getActiveSheet()->SetCellValue('L' . $row, $grand_remain);

                $this->excel->getActiveSheet()->getStyle("J" . $row . ":L" . $row)->getBorders()
                    ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); 
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);

                $this->excel->getActiveSheet()->getStyle('D1:O'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);   

                $this->excel->getActiveSheet()->getStyle('A1:C'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('M1:M'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                $this->excel->getActiveSheet()->getStyle('M1:M'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);   
                
                $this->excel->getActiveSheet()->getStyle('J1:L'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
                $filename = 'purchase_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $pi = "( SELECT purchase_id, product_id, (GROUP_CONCAT(CONCAT({$this->db->dbprefix('purchase_items')}.product_name, '__', {$this->db->dbprefix('purchase_items')}.quantity) SEPARATOR '___')) as item_nane from {$this->db->dbprefix('purchase_items')} ";
            if ($product) {
                $pi .= " WHERE {$this->db->dbprefix('purchase_items')}.product_id = {$product} ";
            }
            $pi .= " GROUP BY {$this->db->dbprefix('purchase_items')}.purchase_id ) FPI";

            $this->load->library('datatables');
            $this->datatables
                ->select("DATE_FORMAT({$this->db->dbprefix('purchases')}.date, '%Y-%m-%d %T') as date, reference_no, {$this->db->dbprefix('warehouses')}.name as wname, supplier, (FPI.item_nane) as iname, grand_total, paid, (grand_total-paid) as balance, {$this->db->dbprefix('purchases')}.status, {$this->db->dbprefix('purchases')}.id as id", FALSE)
                ->from('purchases')
                ->join($pi, 'FPI.purchase_id=purchases.id', 'left')
                ->join('warehouses', 'warehouses.id=purchases.warehouse_id', 'left');
                // ->group_by('purchases.id');

            if ($user) {
                $this->datatables->where('purchases.created_by', $user);
            }
            if ($product) {
                $this->datatables->where('FPI.product_id', $product, FALSE);
            }
            if ($supplier) {
                $this->datatables->where('purchases.supplier_id', $supplier);
            }
            if ($warehouse) {
                $this->datatables->where('purchases.warehouse_id', $warehouse);
            }
            if ($reference_no) {
                $this->datatables->like('purchases.reference_no', $reference_no, 'both');
            }
            if ($start_date) {
                $this->datatables->where($this->db->dbprefix('purchases').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            if ($mode == 'consignment')
                $this->datatables->where("getTranslate(sma_purchases.add_ons,'consignment_status','".f_separate."','".v_separate."') != ''");
            else
                $this->datatables->where("getTranslate(sma_purchases.add_ons,'consignment_status','".f_separate."','".v_separate."') = ''");

            echo $this->datatables->generate();

        }

    }

    function payments()
    {
        $this->sma->checkPermissions('payments');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        $this->data['pos_settings'] = POS ? $this->reports_model->getPOSSetting('biller') : FALSE;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('payments_report')));
        $meta = array('page_title' => lang('payments_report'), 'bc' => $bc);
        $this->page_construct('reports/payments', $meta, $this->data);
    }

    public function report_actions()
    {
        if (!empty($_POST['check_value'])) {
            $temp_val = explode('-',$_POST['check_value']);
            if ($this->input->post('action') == 'export_excel') {
                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('sales_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('customer'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('Parent_Company'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('Biller'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('Suppliers'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('product'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('unit_price'));
                $this->excel->getActiveSheet()->SetCellValue('I1', lang('quantity'));
                $this->excel->getActiveSheet()->SetCellValue('J1', lang('subtotal'));
                $this->excel->getActiveSheet()->SetCellValue('K1', lang('Vat_10%'));
                $this->excel->getActiveSheet()->SetCellValue('L1', lang('grand_total'));
                $this->excel->getActiveSheet()->SetCellValue('M1', lang('paid'));
                $this->excel->getActiveSheet()->SetCellValue('N1', lang('balance'));
                $this->excel->getActiveSheet()->SetCellValue('O1', lang('payment_status'));
                $this->excel->getActiveSheet()->getStyle('H1:N1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $this->excel->getActiveSheet()->getStyle('O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('O')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                $row = 2;
                $row_2 = 1;
                $total = 0;
                $paid = 0;
                $balance = 0;
                $total_row = count($temp_val);
                foreach ($temp_val as $id) {
                    if ($id != '') {            
                        $config_data = array(
                            'table_name' => 'sma_sales',
                            'select_table' => 'sma_sales',
                            'translate' => '',
                            'select_condition' => "id = ".$id,
                        );
                        $temp_sale = $this->site->api_select_data_v2($config_data);
                        $config_data = array(
                            'table_name' => 'sma_sale_items',
                            'select_table' => 'sma_sale_items',
                            'translate' => '',
                            'select_condition' => "sale_id = ".$id." order by id asc",
                        );
                        $temp = $this->site->api_select_data_v2($config_data);

                        if (count($temp) > 0) {
                            $temp_row = $row;
                            $temp_row_2 = $row + count($temp);
                            for ($i=0;$i<count($temp);$i++) {
                                if ($temp[$i]['product_name'] != '') {
                                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($temp_sale[0]['date']));
                                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $temp_sale[0]['reference_no']);
                                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $temp_sale[0]['customer']);
                                    //----
                                    if ($temp_sale[0]['customer_id'] != '') {
                                        $temp3 = $this->site->api_select_some_fields_with_where("
                                            parent_id, company
                                            "
                                            ,"sma_companies"
                                            ,"id = ".$temp_sale[0]['customer_id']
                                            ,"arr"
                                        );
                                        if ($temp3[0]['parent_id'] > 0) {
                                            $temp2 = $this->site->api_select_some_fields_with_where("
                                                id, company
                                                "
                                                ,"sma_companies"
                                                ,"id = ".$temp3[0]['parent_id']
                                                ,"arr"
                                            );                    
                                            $temp_sale[0]['parent_company'] = $temp2[0]['company'];
                                        }
                                    }
                                    //----
                                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $temp_sale[0]['parent_company']);
                                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $temp_sale[0]['biller']);
                                    //----
                                    if ($temp[$i]['supplier'] != '') {
                                        $temp2 = $this->site->api_select_some_fields_with_where("
                                            company
                                            "
                                            ,"sma_companies"
                                            ,"id = ".$temp[$i]['supplier']
                                            ,"arr"
                                        );
                                        $temp[$i]['supplier_name'] = $temp2[0]['company'];
                                    }
                                    //----                                       
                                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $temp[$i]['supplier_name']);
                                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $temp[$i]['product_name']);
                                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, $temp[$i]['unit_price']);
                                    $this->excel->getActiveSheet()->SetCellValue('I' . $row, $temp[$i]['quantity']);
                                    $this->excel->getActiveSheet()->SetCellValue('J' . $row, $temp[$i]['subtotal']);
                                    $this->excel->getActiveSheet()->SetCellValue('K' . $row, $temp_sale[0]['total_tax']);
                                    $this->excel->getActiveSheet()->SetCellValue('L' . $row, $temp_sale[0]['grand_total']);
                                    $this->excel->getActiveSheet()->SetCellValue('M' . $row, $temp_sale[0]['paid']);
                                    $this->excel->getActiveSheet()->SetCellValue('N' . $row, ($temp_sale[0]['grand_total'] - $temp_sale[0]['paid']));
                                    $this->excel->getActiveSheet()->SetCellValue('O' . $row, lang($temp_sale[0]['payment_status']));
                                    $this->excel->getActiveSheet()->getStyle('I' . $row)->getAlignment()->setWrapText(true);
                                    
                                    $row++;
                                }
                            }                        

                            if ($total_row >= $row_2)
                                $temp_row_22 = $temp_row_2 - 1;
                            else
                                $temp_row_22 = $temp_row_2;

                            $this->excel->getActiveSheet()->mergeCells('A'.$temp_row.':A'.$temp_row_22);
                            $this->excel->getActiveSheet()->mergeCells('B'.$temp_row.':B'.$temp_row_22);
                            $this->excel->getActiveSheet()->mergeCells('C'.$temp_row.':C'.$temp_row_22);                        
                            $this->excel->getActiveSheet()->mergeCells('D'.$temp_row.':D'.$temp_row_22);                      
                            $this->excel->getActiveSheet()->mergeCells('E'.$temp_row.':E'.$temp_row_22);
                            $this->excel->getActiveSheet()->mergeCells('K'.$temp_row.':K'.$temp_row_22);
                            $this->excel->getActiveSheet()->mergeCells('L'.$temp_row.':L'.$temp_row_22);
                            $this->excel->getActiveSheet()->mergeCells('M'.$temp_row.':M'.$temp_row_22);
                            $this->excel->getActiveSheet()->mergeCells('N'.$temp_row.':N'.$temp_row_22);
                            $this->excel->getActiveSheet()->mergeCells('O'.$temp_row.':O'.$temp_row_22);
                            $this->excel->getActiveSheet()->getStyle("A".$temp_row.":O".$temp_row)->getBorders()
                                ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
                        }
                        else {
                            // $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($temp_sale[0]['date']));
                            // $this->excel->getActiveSheet()->SetCellValue('B' . $row, $temp_sale[0]['reference_no']);
                            // $this->excel->getActiveSheet()->SetCellValue('C' . $row, $temp_sale[0]['customer']);
                            // $this->excel->getActiveSheet()->SetCellValue('D' . $row, $temp[0]['product_name']);
                            // $this->excel->getActiveSheet()->SetCellValue('E' . $row, $temp[0]['unit_price']);
                            // $this->excel->getActiveSheet()->SetCellValue('F' . $row, $temp[0]['quantity']);
                            // $this->excel->getActiveSheet()->SetCellValue('G' . $row, $temp[0]['subtotal']);
                            // $this->excel->getActiveSheet()->SetCellValue('H' . $row, $temp_sale[0]['total_tax']);
                            // $this->excel->getActiveSheet()->SetCellValue('I' . $row, $temp_sale[0]['grand_total']);
                            // $this->excel->getActiveSheet()->SetCellValue('J' . $row, $temp_sale[0]['paid']);
                            // $this->excel->getActiveSheet()->SetCellValue('K' . $row, ($temp_sale[0]['grand_total'] - $temp_sale[0]['paid']));
                            // $this->excel->getActiveSheet()->SetCellValue('L' . $row, lang($temp_sale[0]['payment_status']));
                            // $this->excel->getActiveSheet()->getStyle("A".$row.":L".$row)->getBorders()
                            //     ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
                            $row++;
                        }
                        
                        $total += $temp_sale[0]['grand_total'];
                        $paid += $temp_sale[0]['paid'];
                        $balance += ($temp_sale[0]['grand_total'] - $temp_sale[0]['paid']);
                        $row_2++;
                    }
                }

                $this->excel->getActiveSheet()->getStyle("L" . $row . ":N" . $row)->getBorders()
                    ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                $this->excel->getActiveSheet()->SetCellValue('L' . $row, $total);
                $this->excel->getActiveSheet()->SetCellValue('M' . $row, $paid);
                $this->excel->getActiveSheet()->SetCellValue('N' . $row, $balance);

                $this->excel->getActiveSheet()->getStyle('H1:H'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
                $this->excel->getActiveSheet()->getStyle('J1:N'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(30); 
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(20);

                $this->excel->getActiveSheet()->getStyle('A1:E'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('H1:O'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                $this->excel->getActiveSheet()->getStyle('G1:G'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);   

                $this->excel->getActiveSheet()->getStyle('C1:G'.$row)->getAlignment()->setWrapText(true);

                $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : 'January '.date('Y');
                $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : 'December '.date('Y');
                $filename = 'Sales Reports in '.$start_date.' to '.$end_date;
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
        } else {
            $this->session->set_flashdata('error', lang("No sale selected. Please select at least one sale."));
            $this->session->set_userdata('error', lang('No sale selected. Please select at least one sale.'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function getPaymentsReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('payments', TRUE);

        $user = $this->input->get('user') ? $this->input->get('user') : NULL;
        $supplier = $this->input->get('supplier') ? $this->input->get('supplier') : NULL;
        $customer = $this->input->get('customer') ? $this->input->get('customer') : NULL;
        $biller = $this->input->get('biller') ? $this->input->get('biller') : NULL;
        $payment_ref = $this->input->get('payment_ref') ? $this->input->get('payment_ref') : NULL;
        $paid_by = $this->input->get('paid_by') ? $this->input->get('paid_by') : NULL;
        $sale_ref = $this->input->get('sale_ref') ? $this->input->get('sale_ref') : NULL;
        $purchase_ref = $this->input->get('purchase_ref') ? $this->input->get('purchase_ref') : NULL;
        $card = $this->input->get('card') ? $this->input->get('card') : NULL;
        $cheque = $this->input->get('cheque') ? $this->input->get('cheque') : NULL;
        $transaction_id = $this->input->get('tid') ? $this->input->get('tid') : NULL;
        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;

        if ($start_date) {
            $start_date = $this->sma->fsd($start_date);
            $end_date = $this->sma->fsd($end_date);
        }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user = $this->session->userdata('user_id');
        }
        if ($pdf || $xls) {

            $this->db
                ->select("" . $this->db->dbprefix('payments') . ".date, " . $this->db->dbprefix('payments') . ".reference_no as payment_ref, " . $this->db->dbprefix('sales') . ".reference_no as sale_ref, " . $this->db->dbprefix('purchases') . ".reference_no as purchase_ref, paid_by, amount, type")
                ->from('payments')
                ->join('sales', 'payments.sale_id=sales.id', 'left')
                ->join('purchases', 'payments.purchase_id=purchases.id', 'left')
                ->group_by('payments.id')
                ->order_by('payments.date desc');

            if ($user) {
                $this->db->where('payments.created_by', $user);
            }
            if ($card) {
                $this->db->like('payments.cc_no', $card, 'both');
            }
            if ($cheque) {
                $this->db->where('payments.cheque_no', $cheque);
            }
            if ($transaction_id) {
                $this->db->where('payments.transaction_id', $transaction_id);
            }
            if ($customer) {
                $this->db->where('sales.customer_id', $customer);
            }
            if ($supplier) {
                $this->db->where('purchases.supplier_id', $supplier);
            }
            if ($biller) {
                $this->db->where('sales.biller_id', $biller);
            }
            if ($customer) {
                $this->db->where('sales.customer_id', $customer);
            }
            if ($payment_ref) {
                $this->db->like('payments.reference_no', $payment_ref, 'both');
            }
            if ($paid_by) {
                $this->db->where('payments.paid_by', $paid_by);
            }
            if ($sale_ref) {
                $this->db->like('sales.reference_no', $sale_ref, 'both');
            }
            if ($purchase_ref) {
                $this->db->like('purchases.reference_no', $purchase_ref, 'both');
            }
            if ($start_date) {
                $this->db->where($this->db->dbprefix('payments').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            $q = $this->db->get();
            if ($q != false && $q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('payments_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('payment_reference'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('sale_reference'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('purchase_reference'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('paid_by'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('amount'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('type'));

                $row = 2;
                $total = 0;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->payment_ref);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->sale_ref);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->purchase_ref);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, lang($data_row->paid_by));
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->amount);
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->type);
                    if ($data_row->type == 'returned' || $data_row->type == 'sent') {
                        $total -= $data_row->amount;
                    } else {
                        $total += $data_row->amount;
                    }
                    $row++;
                }
                $this->excel->getActiveSheet()->getStyle("F" . $row)->getBorders()
                    ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                $this->excel->getActiveSheet()->SetCellValue('F' . $row, $total);

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $filename = 'payments_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $this->load->library('datatables');
            $this->datatables
                ->select("DATE_FORMAT({$this->db->dbprefix('payments')}.date, '%Y-%m-%d %T') as date, " . $this->db->dbprefix('payments') . ".reference_no as payment_ref, " . $this->db->dbprefix('sales') . ".reference_no as sale_ref, " . $this->db->dbprefix('purchases') . ".reference_no as purchase_ref, paid_by, amount, type, {$this->db->dbprefix('payments')}.id as id")
                ->from('payments')
                ->join('sales', 'payments.sale_id=sales.id', 'left')
                ->join('purchases', 'payments.purchase_id=purchases.id', 'left')
                ->group_by('payments.id');

            if ($user) {
                $this->datatables->where('payments.created_by', $user);
            }
            if ($card) {
                $this->datatables->like('payments.cc_no', $card, 'both');
            }
            if ($cheque) {
                $this->datatables->where('payments.cheque_no', $cheque);
            }
            if ($transaction_id) {
                $this->datatables->where('payments.transaction_id', $transaction_id);
            }
            if ($customer) {
                $this->datatables->where('sales.customer_id', $customer);
            }
            if ($supplier) {
                $this->datatables->where('purchases.supplier_id', $supplier);
            }
            if ($biller) {
                $this->datatables->where('sales.biller_id', $biller);
            }
            if ($customer) {
                $this->datatables->where('sales.customer_id', $customer);
            }
            if ($payment_ref) {
                $this->datatables->like('payments.reference_no', $payment_ref, 'both');
            }
            if ($paid_by) {
                $this->datatables->where('payments.paid_by', $paid_by);
            }
            if ($sale_ref) {
                $this->datatables->like('sales.reference_no', $sale_ref, 'both');
            }
            if ($purchase_ref) {
                $this->datatables->like('purchases.reference_no', $purchase_ref, 'both');
            }
            if ($start_date) {
                $this->datatables->where($this->db->dbprefix('payments').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            echo $this->datatables->generate();

        }

    }

    function customers()
    {
        $this->sma->checkPermissions('customers');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('customers_report')));
        $meta = array('page_title' => lang('customers_report'), 'bc' => $bc);
        $this->page_construct('reports/customers', $meta, $this->data);
    }

    function getCustomers($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('customers', TRUE);

        if ($pdf || $xls) {

            $this->db
                ->select($this->db->dbprefix('companies') . ".id as id, company, name, phone, email, count(" . $this->db->dbprefix('sales') . ".id) as total, COALESCE(sum(grand_total), 0) as total_amount, COALESCE(sum(paid), 0) as paid, ( COALESCE(sum(grand_total), 0) - COALESCE(sum(paid), 0)) as balance", FALSE)
                ->from("companies")
                ->join('sales', 'sales.customer_id=companies.id')
                ->where('companies.group_name', 'customer')
                ->order_by('companies.company asc')
                ->group_by('companies.id');

            $q = $this->db->get();
            if ($q != false && $q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('customers_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('company'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('name'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('phone'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('email'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('total_sales'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('total_amount'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('paid'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('balance'));

                $row = 2;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->company);
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->name);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->phone);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->email);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->total);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $this->sma->formatMoney($data_row->total_amount));
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $this->sma->formatMoney($data_row->paid));
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, $this->sma->formatMoney($data_row->balance));
                    $row++;
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $filename = 'customers_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $s = "( SELECT customer_id, count(" . $this->db->dbprefix('sales') . ".id) as total, COALESCE(sum(grand_total), 0) as total_amount, COALESCE(sum(paid), 0) as paid, ( COALESCE(sum(grand_total), 0) - COALESCE(sum(paid), 0)) as balance from {$this->db->dbprefix('sales')} GROUP BY {$this->db->dbprefix('sales')}.customer_id ) FS";

            $this->load->library('datatables');
            $this->datatables
                ->select($this->db->dbprefix('companies') . ".id as id, company, name, phone, email, FS.total, FS.total_amount, FS.paid, FS.balance", FALSE)
                ->from("companies")
                ->join($s, 'FS.customer_id=companies.id')
                ->where('companies.group_name', 'customer')
                ->group_by('companies.id')
                ->add_column("Actions", "<div class='text-center'><a class=\"tip\" title='" . lang("view_report") . "' href='" . admin_url('reports/customer_report/$1') . "'><span class='label label-primary'>" . lang("view_report") . "</span></a></div>", "id")
                ->unset_column('id');
            echo $this->datatables->generate();

        }

    }

    function customer_report($user_id = NULL)
    {
        $this->sma->checkPermissions('customers', TRUE);
        if (!$user_id) {
            $this->session->set_flashdata('error', lang("no_customer_selected"));
            admin_redirect('reports/customers');
        }

        $this->data['sales'] = $this->reports_model->getSalesTotals($user_id);
        $this->data['total_sales'] = $this->reports_model->getCustomerSales($user_id);
        $this->data['total_quotes'] = $this->reports_model->getCustomerQuotes($user_id);
        $this->data['total_returns'] = $this->reports_model->getCustomerReturns($user_id);
        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompanies('biller');

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $this->data['user_id'] = $user_id;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('customers_report')));
        $meta = array('page_title' => lang('customers_report'), 'bc' => $bc);
        $this->page_construct('reports/customer_report', $meta, $this->data);

    }

    function suppliers()
    {
        $this->sma->checkPermissions('suppliers');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('suppliers_report')));
        $meta = array('page_title' => lang('suppliers_report'), 'bc' => $bc);
        $this->page_construct('reports/suppliers', $meta, $this->data);
    }

    function getSuppliers($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('suppliers', TRUE);

        if ($pdf || $xls) {

            $this->db
                ->select($this->db->dbprefix('companies') . ".id as id, company, name, phone, email, count({$this->db->dbprefix('purchases')}.id) as total, COALESCE(sum(grand_total), 0) as total_amount, COALESCE(sum(paid), 0) as paid, ( COALESCE(sum(grand_total), 0) - COALESCE(sum(paid), 0)) as balance", FALSE)
                ->from("companies")
                ->join('purchases', 'purchases.supplier_id=companies.id')
                ->where('companies.group_name', 'supplier')
                ->order_by('companies.company asc')
                ->group_by('companies.id');

            $q = $this->db->get();
            if ($q != false && $q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('suppliers_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('company'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('name'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('phone'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('email'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('total_purchases'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('total_amount'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('paid'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('balance'));

                $row = 2;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->company);
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->name);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->phone);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->email);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $this->sma->formatDecimal($data_row->total));
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $this->sma->formatDecimal($data_row->total_amount));
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $this->sma->formatDecimal($data_row->paid));
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, $this->sma->formatDecimal($data_row->balance));
                    $row++;
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $filename = 'suppliers_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $p = "( SELECT supplier_id, count(" . $this->db->dbprefix('purchases') . ".id) as total, COALESCE(sum(grand_total), 0) as total_amount, COALESCE(sum(paid), 0) as paid, ( COALESCE(sum(grand_total), 0) - COALESCE(sum(paid), 0)) as balance from {$this->db->dbprefix('purchases')} GROUP BY {$this->db->dbprefix('purchases')}.supplier_id ) FP";

            $this->load->library('datatables');
            $this->datatables
                ->select($this->db->dbprefix('companies') . ".id as id, company, name, phone, email, FP.total, FP.total_amount, FP.paid, FP.balance", FALSE)
                ->from("companies")
                ->join($p, 'FP.supplier_id=companies.id')
                ->where('companies.group_name', 'supplier')
                ->group_by('companies.id')
                ->add_column("Actions", "<div class='text-center'><a class=\"tip\" title='" . lang("view_report") . "' href='" . admin_url('reports/supplier_report/$1') . "'><span class='label label-primary'>" . lang("view_report") . "</span></a></div>", "id")
                ->unset_column('id');
            echo $this->datatables->generate();

        }

    }

    function supplier_report($user_id = NULL)
    {
        $this->sma->checkPermissions('suppliers', TRUE);
        if (!$user_id) {
            $this->session->set_flashdata('error', lang("no_supplier_selected"));
            admin_redirect('reports/suppliers');
        }

        $this->data['purchases'] = $this->reports_model->getPurchasesTotals($user_id);
        $this->data['total_purchases'] = $this->reports_model->getSupplierPurchases($user_id);
        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['warehouses'] = $this->site->getAllWarehouses();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $this->data['user_id'] = $user_id;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('suppliers_report')));
        $meta = array('page_title' => lang('suppliers_report'), 'bc' => $bc);
        $this->page_construct('reports/supplier_report', $meta, $this->data);

    }

    function users()
    {
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('staff_report')));
        $meta = array('page_title' => lang('staff_report'), 'bc' => $bc);
        $this->page_construct('reports/users', $meta, $this->data);
    }

    function getUsers()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select($this->db->dbprefix('users').".id as id, first_name, last_name, email, company, ".$this->db->dbprefix('groups').".name, active")
            ->from("users")
            ->join('groups', 'users.group_id=groups.id', 'left')
            ->group_by('users.id')
            ->where('company_id', NULL);
        if (!$this->Owner) {
            $this->datatables->where('group_id !=', 1);
        }
        $this->datatables
            ->edit_column('active', '$1__$2', 'active, id')
            ->add_column("Actions", "<div class='text-center'><a class=\"tip\" title='" . lang("view_report") . "' href='" . admin_url('reports/staff_report/$1') . "'><span class='label label-primary'>" . lang("view_report") . "</span></a></div>", "id")
            ->unset_column('id');
        echo $this->datatables->generate();
    }

    function staff_report($user_id = NULL, $year = NULL, $month = NULL, $pdf = NULL, $cal = 0)
    {

        if (!$user_id) {
            $this->session->set_flashdata('error', lang("no_user_selected"));
            admin_redirect('reports/users');
        }
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $this->data['purchases'] = $this->reports_model->getStaffPurchases($user_id);
        $this->data['sales'] = $this->reports_model->getStaffSales($user_id);
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        $this->data['warehouses'] = $this->site->getAllWarehouses();

        if (!$year) {
            $year = date('Y');
        }
        if (!$month || $month == '#monthly-con') {
            $month = date('m');
        }
        if ($pdf) {
            if ($cal) {
                $this->monthly_sales($year, $pdf, $user_id);
            } else {
                $this->daily_sales($year, $month, $pdf, $user_id);
            }
        }
        $config = array(
            'show_next_prev' => TRUE,
            'next_prev_url' => admin_url('reports/staff_report/'.$user_id),
            'month_type' => 'long',
            'day_type' => 'long'
        );

        $config['template'] = '{table_open}<div class="table-responsive"><table border="0" cellpadding="0" cellspacing="0" class="table table-bordered dfTable reports-table">{/table_open}
        {heading_row_start}<tr>{/heading_row_start}
        {heading_previous_cell}<th class="text-center"><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
        {heading_title_cell}<th class="text-center" colspan="{colspan}" id="month_year">{heading}</th>{/heading_title_cell}
        {heading_next_cell}<th class="text-center"><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}
        {heading_row_end}</tr>{/heading_row_end}
        {week_row_start}<tr>{/week_row_start}
        {week_day_cell}<td class="cl_wday">{week_day}</td>{/week_day_cell}
        {week_row_end}</tr>{/week_row_end}
        {cal_row_start}<tr class="days">{/cal_row_start}
        {cal_cell_start}<td class="day">{/cal_cell_start}
        {cal_cell_content}
        <div class="day_num">{day}</div>
        <div class="content">{content}</div>
        {/cal_cell_content}
        {cal_cell_content_today}
        <div class="day_num highlight">{day}</div>
        <div class="content">{content}</div>
        {/cal_cell_content_today}
        {cal_cell_no_content}<div class="day_num">{day}</div>{/cal_cell_no_content}
        {cal_cell_no_content_today}<div class="day_num highlight">{day}</div>{/cal_cell_no_content_today}
        {cal_cell_blank}&nbsp;{/cal_cell_blank}
        {cal_cell_end}</td>{/cal_cell_end}
        {cal_row_end}</tr>{/cal_row_end}
        {table_close}</table></div>{/table_close}';

        $this->load->library('calendar', $config);
        $sales = $this->reports_model->getStaffDailySales($user_id, $year, $month);

        if (!empty($sales)) {
            foreach ($sales as $sale) {
                $daily_sale[$sale->date] = "<table class='table table-bordered table-hover table-striped table-condensed data' style='margin:0;'><tr><td>" . lang("discount") . "</td><td>" . $this->sma->formatMoney($sale->discount) . "</td></tr><tr><td>" . lang("product_tax") . "</td><td>" . $this->sma->formatMoney($sale->tax1) . "</td></tr><tr><td>" . lang("order_tax") . "</td><td>" . $this->sma->formatMoney($sale->tax2) . "</td></tr><tr><td>" . lang("total") . "</td><td>" . $this->sma->formatMoney($sale->total) . "</td></tr></table>";
            }
        } else {
            $daily_sale = array();
        }
        $this->data['calender'] = $this->calendar->generate($year, $month, $daily_sale);
        if ($this->input->get('pdf')) {

        }
        $this->data['year'] = $year;
        $this->data['month'] = $month;
        $this->data['msales'] = $this->reports_model->getStaffMonthlySales($user_id, $year);
        $this->data['user_id'] = $user_id;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('staff_report')));
        $meta = array('page_title' => lang('staff_report'), 'bc' => $bc);
        $this->page_construct('reports/staff_report', $meta, $this->data);

    }

    function getUserLogins($id = NULL, $pdf = NULL, $xls = NULL)
    {
        if ($this->input->get('start_date')) {
            $login_start_date = $this->input->get('start_date');
        } else {
            $login_start_date = NULL;
        }
        if ($this->input->get('end_date')) {
            $login_end_date = $this->input->get('end_date');
        } else {
            $login_end_date = NULL;
        }
        if ($login_start_date) {
            $login_start_date = $this->sma->fld($login_start_date);
            $login_end_date = $login_end_date ? $this->sma->fld($login_end_date) : date('Y-m-d H:i:s');
        }
        if ($pdf || $xls) {

            $this->db
                ->select("login, ip_address, time")
                ->from("user_logins")
                ->where('user_id', $id)
                ->order_by('time desc');
            if ($login_start_date) {
                $this->db->where("time BETWEEN '{$login_start_date}' and '{$login_end_date}'", NULL, FALSE);
            }

            $q = $this->db->get();
            if ($q != false && $q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('staff_login_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('email'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('ip_address'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('time'));

                $row = 2;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->login);
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->ip_address);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $this->sma->hrld($data_row->time));
                    $row++;
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('C2:C' . $row)->getAlignment()->setWrapText(true);
                $filename = 'staff_login_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $this->load->library('datatables');
            $this->datatables
                ->select("login, ip_address, DATE_FORMAT(time, '%Y-%m-%d %T') as time")
                ->from("user_logins")
                ->where('user_id', $id);
            if ($login_start_date) {
                $this->datatables->where("time BETWEEN '{$login_start_date}' and '{$login_end_date}'", NULL, FALSE);
            }
            echo $this->datatables->generate();

        }

    }

    function getCustomerLogins($id = NULL)
    {
        if ($this->input->get('login_start_date')) {
            $login_start_date = $this->input->get('login_start_date');
        } else {
            $login_start_date = NULL;
        }
        if ($this->input->get('login_end_date')) {
            $login_end_date = $this->input->get('login_end_date');
        } else {
            $login_end_date = NULL;
        }
        if ($login_start_date) {
            $login_start_date = $this->sma->fld($login_start_date);
            $login_end_date = $login_end_date ? $this->sma->fld($login_end_date) : date('Y-m-d H:i:s');
        }
        $this->load->library('datatables');
        $this->datatables
            ->select("login, ip_address, time")
            ->from("user_logins")
            ->where('customer_id', $id);
        if ($login_start_date) {
            $this->datatables->where('time BETWEEN "' . $login_start_date . '" and "' . $login_end_date . '"');
        }
        echo $this->datatables->generate();
    }

    function profit_loss($start_date = NULL, $end_date = NULL)
    {
        $this->sma->checkPermissions('profit_loss');
        if (!$start_date) {
            $start = $this->db->escape(date('Y-m') . '-1');
            $start_date = date('Y-m') . '-1';
        } else {
            $start = $this->db->escape(urldecode($start_date));
        }
        if (!$end_date) {
            $end = $this->db->escape(date('Y-m-d H:i'));
            $end_date = date('Y-m-d H:i');
        } else {
            $end = $this->db->escape(urldecode($end_date));
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $this->data['total_purchases'] = $this->reports_model->getTotalPurchases($start, $end);
        $this->data['total_sales'] = $this->reports_model->getTotalSales($start, $end);
        $this->data['total_return_sales'] = $this->reports_model->getTotalReturnSales($start, $end);
        $this->data['total_expenses'] = $this->reports_model->getTotalExpenses($start, $end);
        $this->data['total_paid'] = $this->reports_model->getTotalPaidAmount($start, $end);
        $this->data['total_received'] = $this->reports_model->getTotalReceivedAmount($start, $end);
        $this->data['total_received_cash'] = $this->reports_model->getTotalReceivedCashAmount($start, $end);
        $this->data['total_received_cc'] = $this->reports_model->getTotalReceivedCCAmount($start, $end);
        $this->data['total_received_cheque'] = $this->reports_model->getTotalReceivedChequeAmount($start, $end);
        $this->data['total_received_ppp'] = $this->reports_model->getTotalReceivedPPPAmount($start, $end);
        $this->data['total_received_stripe'] = $this->reports_model->getTotalReceivedStripeAmount($start, $end);
        $this->data['total_returned'] = $this->reports_model->getTotalReturnedAmount($start, $end);
        $this->data['start'] = urldecode($start_date);
        $this->data['end'] = urldecode($end_date);

        $warehouses = $this->site->getAllWarehouses();
        foreach ($warehouses as $warehouse) {
            $total_purchases = $this->reports_model->getTotalPurchases($start, $end, $warehouse->id);
            $total_sales = $this->reports_model->getTotalSales($start, $end, $warehouse->id);
            $total_returns = $this->reports_model->getTotalReturnSales($start, $end, $warehouse->id);
            $total_expenses = $this->reports_model->getTotalExpenses($start, $end, $warehouse->id);
            $warehouses_report[] = array(
                'warehouse' => $warehouse,
                'total_purchases' => $total_purchases,
                'total_sales' => $total_sales,
                'total_returns' => $total_returns,
                'total_expenses' => $total_expenses,
                );
        }
        $this->data['warehouses_report'] = $warehouses_report;

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('profit_loss')));
        $meta = array('page_title' => lang('profit_loss'), 'bc' => $bc);
        $this->page_construct('reports/profit_loss', $meta, $this->data);
    }

    function profit_loss_pdf($start_date = NULL, $end_date = NULL)
    {
        $this->sma->checkPermissions('profit_loss');
        if (!$start_date) {
            $start = $this->db->escape(date('Y-m') . '-1');
            $start_date = date('Y-m') . '-1';
        } else {
            $start = $this->db->escape(urldecode($start_date));
        }
        if (!$end_date) {
            $end = $this->db->escape(date('Y-m-d H:i'));
            $end_date = date('Y-m-d H:i');
        } else {
            $end = $this->db->escape(urldecode($end_date));
        }

        $this->data['total_purchases'] = $this->reports_model->getTotalPurchases($start, $end);
        $this->data['total_sales'] = $this->reports_model->getTotalSales($start, $end);
        $this->data['total_expenses'] = $this->reports_model->getTotalExpenses($start, $end);
        $this->data['total_paid'] = $this->reports_model->getTotalPaidAmount($start, $end);
        $this->data['total_received'] = $this->reports_model->getTotalReceivedAmount($start, $end);
        $this->data['total_received_cash'] = $this->reports_model->getTotalReceivedCashAmount($start, $end);
        $this->data['total_received_cc'] = $this->reports_model->getTotalReceivedCCAmount($start, $end);
        $this->data['total_received_cheque'] = $this->reports_model->getTotalReceivedChequeAmount($start, $end);
        $this->data['total_received_ppp'] = $this->reports_model->getTotalReceivedPPPAmount($start, $end);
        $this->data['total_received_stripe'] = $this->reports_model->getTotalReceivedStripeAmount($start, $end);
        $this->data['total_returned'] = $this->reports_model->getTotalReturnedAmount($start, $end);
        $this->data['start'] = urldecode($start_date);
        $this->data['end'] = urldecode($end_date);

        $warehouses = $this->site->getAllWarehouses();
        foreach ($warehouses as $warehouse) {
            $total_purchases = $this->reports_model->getTotalPurchases($start, $end, $warehouse->id);
            $total_sales = $this->reports_model->getTotalSales($start, $end, $warehouse->id);
            $warehouses_report[] = array(
                'warehouse' => $warehouse,
                'total_purchases' => $total_purchases,
                'total_sales' => $total_sales,
                );
        }
        $this->data['warehouses_report'] = $warehouses_report;

        $html = $this->load->view($this->theme . 'reports/profit_loss_pdf', $this->data, true);
        $name = lang("profit_loss") . "-" . str_replace(array('-', ' ', ':'), '_', $this->data['start']) . "-" . str_replace(array('-', ' ', ':'), '_', $this->data['end']) . ".pdf";
        $this->sma->generate_pdf($html, $name, false, false, false, false, false, 'L');
    }

    function register()
    {
        $this->sma->checkPermissions('register');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['users'] = $this->reports_model->getStaff();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('register_report')));
        $meta = array('page_title' => lang('register_report'), 'bc' => $bc);
        $this->page_construct('reports/register', $meta, $this->data);
    }

    function getRrgisterlogs($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('register', TRUE);
        if ($this->input->get('user')) {
            $user = $this->input->get('user');
        } else {
            $user = NULL;
        }
        if ($this->input->get('start_date')) {
            $start_date = $this->input->get('start_date');
        } else {
            $start_date = NULL;
        }
        if ($this->input->get('end_date')) {
            $end_date = $this->input->get('end_date');
        } else {
            $end_date = NULL;
        }
        if ($start_date) {
            $start_date = $this->sma->fld($start_date);
            $end_date = $this->sma->fld($end_date);
        }

        if ($pdf || $xls) {

            $this->db
                ->select("date, closed_at, CONCAT(" . $this->db->dbprefix('users') . ".first_name, ' ', " . $this->db->dbprefix('users') . ".last_name, ' (', users.email, ')') as user, cash_in_hand, total_cc_slips, total_cheques, total_cash, total_cc_slips_submitted, total_cheques_submitted,total_cash_submitted, note", FALSE)
                ->from("pos_register")
                ->join('users', 'users.id=pos_register.user_id', 'left')
                ->order_by('date desc');
            //->where('status', 'close');

            if ($user) {
                $this->db->where('pos_register.user_id', $user);
            }
            if ($start_date) {
                $this->db->where('date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            $q = $this->db->get();
            if ($q != false && $q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('register_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('open_time'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('close_time'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('user'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('cash_in_hand'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('cc_slips'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('cheques'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('total_cash'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('cc_slips_submitted'));
                $this->excel->getActiveSheet()->SetCellValue('I1', lang('cheques_submitted'));
                $this->excel->getActiveSheet()->SetCellValue('J1', lang('total_cash_submitted'));
                $this->excel->getActiveSheet()->SetCellValue('K1', lang('note'));

                $row = 2;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->closed_at);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->user);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->cash_in_hand);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->total_cc_slips);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->total_cheques);
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->total_cash);
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, $data_row->total_cc_slips_submitted);
                    $this->excel->getActiveSheet()->SetCellValue('I' . $row, $data_row->total_cheques_submitted);
                    $this->excel->getActiveSheet()->SetCellValue('J' . $row, $data_row->total_cash_submitted);
                    $this->excel->getActiveSheet()->SetCellValue('K' . $row, $data_row->note);
                    if($data_row->total_cash_submitted < $data_row->total_cash || $data_row->total_cheques_submitted < $data_row->total_cheques || $data_row->total_cc_slips_submitted < $data_row->total_cc_slips) {
                        $this->excel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->applyFromArray(
                                array( 'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'F2DEDE')) )
                                );
                    }
                    $row++;
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(35);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $filename = 'register_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $this->load->library('datatables');
            $this->datatables
                ->select("date, closed_at, CONCAT(" . $this->db->dbprefix('users') . ".first_name, ' ', " . $this->db->dbprefix('users') . ".last_name, '<br>', " . $this->db->dbprefix('users') . ".email) as user, cash_in_hand, CONCAT(total_cc_slips, ' (', total_cc_slips_submitted, ')'), CONCAT(total_cheques, ' (', total_cheques_submitted, ')'), CONCAT(total_cash, ' (', total_cash_submitted, ')'), note", FALSE)
                ->from("pos_register")
                ->join('users', 'users.id=pos_register.user_id', 'left');

            if ($user) {
                $this->datatables->where('pos_register.user_id', $user);
            }
            if ($start_date) {
                $this->datatables->where('date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            echo $this->datatables->generate();

        }

    }

    public function expenses($id = null)
    {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['categories'] = $this->reports_model->getExpenseCategories();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('expenses')));
        $meta = array('page_title' => lang('expenses'), 'bc' => $bc);
        $this->page_construct('reports/expenses', $meta, $this->data);
    }

    public function getExpensesReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('expenses');

        $reference_no = $this->input->get('reference_no') ? $this->input->get('reference_no') : NULL;
        $category = $this->input->get('category') ? $this->input->get('category') : NULL;
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $note = $this->input->get('note') ? $this->input->get('note') : NULL;
        $user = $this->input->get('user') ? $this->input->get('user') : NULL;
        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;

        if ($start_date) {
            $start_date = $this->sma->fld($start_date);
            $end_date = $this->sma->fld($end_date);
        }

        if ($pdf || $xls) {

            $this->db
                ->select("date, reference, {$this->db->dbprefix('expense_categories')}.name as category, amount, note, CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as user, attachment, {$this->db->dbprefix('expenses')}.id as id", false)
            ->from('expenses')
            ->join('users', 'users.id=expenses.created_by', 'left')
            ->join('expense_categories', 'expense_categories.id=expenses.category_id', 'left')
            ->group_by('expenses.id');

            if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
                $this->db->where('created_by', $this->session->userdata('user_id'));
            }

            if ($note) {
                $this->db->like('note', $note, 'both');
            }
            if ($reference_no) {
                $this->db->like('reference', $reference_no, 'both');
            }
            if ($category) {
                $this->db->where('category_id', $category);
            }
            if ($warehouse) {
                $this->db->where('expenses.warehouse_id', $warehouse);
            }
            if ($user) {
                $this->db->where('created_by', $user);
            }
            if ($start_date) {
                $this->db->where('date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            $q = $this->db->get();
            if ($q != false && $q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('expenses_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('category'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('amount'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('note'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('created_by'));

                $row = 2; $total = 0;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->category);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->amount);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->note);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->created_by);
                    $total += $data_row->amount;
                    $row++;
                }
                $this->excel->getActiveSheet()->getStyle("D" . $row)->getBorders()
                    ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                $this->excel->getActiveSheet()->SetCellValue('D' . $row, $total);

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $filename = 'expenses_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $this->load->library('datatables');
            $this->datatables
            ->select("DATE_FORMAT(date, '%Y-%m-%d %T') as date, reference, {$this->db->dbprefix('expense_categories')}.name as category, amount, note, CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as user, attachment, {$this->db->dbprefix('expenses')}.id as id", false)
            ->from('expenses')
            ->join('users', 'users.id=expenses.created_by', 'left')
            ->join('expense_categories', 'expense_categories.id=expenses.category_id', 'left')
            ->group_by('expenses.id');

            if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
                $this->datatables->where('created_by', $this->session->userdata('user_id'));
            }

            if ($note) {
                $this->datatables->like('note', $note, 'both');
            }
            if ($reference_no) {
                $this->datatables->like('reference', $reference_no, 'both');
            }
            if ($category) {
                $this->datatables->where('category_id', $category);
            }
            if ($warehouse) {
                $this->datatables->where('expenses.warehouse_id', $warehouse);
            }
            if ($user) {
                $this->datatables->where('created_by', $user);
            }
            if ($start_date) {
                $this->datatables->where('date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            echo $this->datatables->generate();
        }
    }

    function daily_purchases($warehouse_id = NULL, $year = NULL, $month = NULL, $pdf = NULL, $user_id = NULL)
    {
        $this->sma->checkPermissions();
        if (!$this->Owner && !$this->Admin && $this->session->userdata('warehouse_id')) {
            $warehouse_id = $this->session->userdata('warehouse_id');
        }
        if (!$year) {
            $year = date('Y');
        }
        if (!$month) {
            $month = date('m');
        }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $config = array(
            'show_next_prev' => TRUE,
            'next_prev_url' => admin_url('reports/daily_purchases/'.($warehouse_id ? $warehouse_id : 0)),
            'month_type' => 'long',
            'day_type' => 'long'
        );

        $config['template'] = '{table_open}<div class="table-responsive"><table border="0" cellpadding="0" cellspacing="0" class="table table-bordered dfTable">{/table_open}
        {heading_row_start}<tr>{/heading_row_start}
        {heading_previous_cell}<th><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
        {heading_title_cell}<th colspan="{colspan}" id="month_year">{heading}</th>{/heading_title_cell}
        {heading_next_cell}<th><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}
        {heading_row_end}</tr>{/heading_row_end}
        {week_row_start}<tr>{/week_row_start}
        {week_day_cell}<td class="cl_wday">{week_day}</td>{/week_day_cell}
        {week_row_end}</tr>{/week_row_end}
        {cal_row_start}<tr class="days">{/cal_row_start}
        {cal_cell_start}<td class="day">{/cal_cell_start}
        {cal_cell_content}
        <div class="day_num">{day}</div>
        <div class="content">{content}</div>
        {/cal_cell_content}
        {cal_cell_content_today}
        <div class="day_num highlight">{day}</div>
        <div class="content">{content}</div>
        {/cal_cell_content_today}
        {cal_cell_no_content}<div class="day_num">{day}</div>{/cal_cell_no_content}
        {cal_cell_no_content_today}<div class="day_num highlight">{day}</div>{/cal_cell_no_content_today}
        {cal_cell_blank}&nbsp;{/cal_cell_blank}
        {cal_cell_end}</td>{/cal_cell_end}
        {cal_row_end}</tr>{/cal_row_end}
        {table_close}</table></div>{/table_close}';

        $this->load->library('calendar', $config);
        $purchases = $user_id ? $this->reports_model->getStaffDailyPurchases($user_id, $year, $month, $warehouse_id) : $this->reports_model->getDailyPurchases($year, $month, $warehouse_id);

        if (!empty($purchases)) {
            foreach ($purchases as $purchase) {
                $daily_purchase[$purchase->date] = "<table class='table table-bordered table-hover table-striped table-condensed data' style='margin:0;'><tr><td>" . lang("discount") . "</td><td>" . $this->sma->formatMoney($purchase->discount) . "</td></tr><tr><td>" . lang("shipping") . "</td><td>" . $this->sma->formatMoney($purchase->shipping) . "</td></tr><tr><td>" . lang("product_tax") . "</td><td>" . $this->sma->formatMoney($purchase->tax1) . "</td></tr><tr><td>" . lang("order_tax") . "</td><td>" . $this->sma->formatMoney($purchase->tax2) . "</td></tr><tr><td>" . lang("total") . "</td><td>" . $this->sma->formatMoney($purchase->total) . "</td></tr></table>";
            }
        } else {
            $daily_purchase = array();
        }

        $this->data['calender'] = $this->calendar->generate($year, $month, $daily_purchase);
        $this->data['year'] = $year;
        $this->data['month'] = $month;
        if ($pdf) {
            $html = $this->load->view($this->theme . 'reports/daily', $this->data, true);
            $name = lang("daily_purchases") . "_" . $year . "_" . $month . ".pdf";
            $html = str_replace('<p class="introtext">' . lang("reports_calendar_text") . '</p>', '', $html);
            $this->sma->generate_pdf($html, $name, null, null, null, null, null, 'L');
        }
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['warehouse_id'] = $warehouse_id;
        $this->data['sel_warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('daily_purchases_report')));
        $meta = array('page_title' => lang('daily_purchases_report'), 'bc' => $bc);
        $this->page_construct('reports/daily_purchases', $meta, $this->data);

    }


    function monthly_purchases($warehouse_id = NULL, $year = NULL, $pdf = NULL, $user_id = NULL)
    {
        $this->sma->checkPermissions();
        if (!$this->Owner && !$this->Admin && $this->session->userdata('warehouse_id')) {
            $warehouse_id = $this->session->userdata('warehouse_id');
        }
        if (!$year) {
            $year = date('Y');
        }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->load->language('calendar');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['year'] = $year;
        $this->data['purchases'] = $user_id ? $this->reports_model->getStaffMonthlyPurchases($user_id, $year, $warehouse_id) : $this->reports_model->getMonthlyPurchases($year, $warehouse_id);
        if ($pdf) {
            $html = $this->load->view($this->theme . 'reports/monthly', $this->data, true);
            $name = lang("monthly_purchases") . "_" . $year . ".pdf";
            $html = str_replace('<p class="introtext">' . lang("reports_calendar_text") . '</p>', '', $html);
            $this->sma->generate_pdf($html, $name, null, null, null, null, null, 'L');
        }
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['warehouse_id'] = $warehouse_id;
        $this->data['sel_warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('monthly_purchases_report')));
        $meta = array('page_title' => lang('monthly_purchases_report'), 'bc' => $bc);
        $this->page_construct('reports/monthly_purchases', $meta, $this->data);

    }

    function adjustments($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('products');

        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['warehouses'] = $this->site->getAllWarehouses();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('adjustments_report')));
        $meta = array('page_title' => lang('adjustments_report'), 'bc' => $bc);
        $this->page_construct('reports/adjustments', $meta, $this->data);
    }

    function getAdjustmentReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('products', TRUE);

        $product = $this->input->get('product') ? $this->input->get('product') : NULL;
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $user = $this->input->get('user') ? $this->input->get('user') : NULL;
        $reference_no = $this->input->get('reference_no') ? $this->input->get('reference_no') : NULL;
        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;
        $serial = $this->input->get('serial') ? $this->input->get('serial') : NULL;

        if ($start_date) {
            $start_date = $this->sma->fld($start_date);
            $end_date = $this->sma->fld($end_date);
        }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user = $this->session->userdata('user_id');
        }

        if ($pdf || $xls) {

            $ai = "( SELECT adjustment_id, product_id, serial_no, GROUP_CONCAT(CONCAT({$this->db->dbprefix('products')}.name, ' (', (CASE WHEN {$this->db->dbprefix('adjustment_items')}.type  = 'subtraction' THEN (0-{$this->db->dbprefix('adjustment_items')}.quantity) ELSE {$this->db->dbprefix('adjustment_items')}.quantity END), ')') SEPARATOR '\n') as item_nane from {$this->db->dbprefix('adjustment_items')} LEFT JOIN {$this->db->dbprefix('products')} ON {$this->db->dbprefix('products')}.id={$this->db->dbprefix('adjustment_items')}.product_id GROUP BY {$this->db->dbprefix('adjustment_items')}.adjustment_id ) FAI";

            $this->db->select("DATE_FORMAT(date, '%Y-%m-%d %T') as date, reference_no, warehouses.name as wh_name, CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as created_by, note, FAI.item_nane as iname, {$this->db->dbprefix('adjustments')}.id as id", FALSE)
            ->from('adjustments')
            ->join($ai, 'FAI.adjustment_id=adjustments.id', 'left')
            ->join('users', 'users.id=adjustments.created_by', 'left')
            ->join('warehouses', 'warehouses.id=adjustments.warehouse_id', 'left');

            if ($user) {
                $this->db->where('adjustments.created_by', $user);
            }
            if ($product) {
                $this->db->where('FAI.product_id', $product);
            }
            if ($serial) {
                $this->db->like('FAI.serial_no', $serial);
            }
            if ($warehouse) {
                $this->db->where('adjustments.warehouse_id', $warehouse);
            }
            if ($reference_no) {
                $this->db->like('adjustments.reference_no', $reference_no, 'both');
            }
            if ($start_date) {
                $this->db->where($this->db->dbprefix('adjustments').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            $q = $this->db->get();
            if ($q != false && $q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('adjustments_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('warehouse'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('created_by'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('note'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('products'));

                $row = 2;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->wh_name);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->created_by);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $this->sma->decode_html($data_row->note));
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->iname);
                    $row++;
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('E2:E' . $row)->getAlignment()->setWrapText(true);
                $this->excel->getActiveSheet()->getStyle('F2:F' . $row)->getAlignment()->setWrapText(true);
                $filename = 'adjustments_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $ai = "( SELECT adjustment_id, product_id, serial_no, GROUP_CONCAT(CONCAT({$this->db->dbprefix('products')}.name, '__', (CASE WHEN {$this->db->dbprefix('adjustment_items')}.type  = 'subtraction' THEN (0-{$this->db->dbprefix('adjustment_items')}.quantity) ELSE {$this->db->dbprefix('adjustment_items')}.quantity END)) SEPARATOR '___') as item_nane from {$this->db->dbprefix('adjustment_items')} LEFT JOIN {$this->db->dbprefix('products')} ON {$this->db->dbprefix('products')}.id={$this->db->dbprefix('adjustment_items')}.product_id ";
            if ($product || $serial) { $ai .= " WHERE "; }
            if ($product) {
                $ai .= " {$this->db->dbprefix('adjustment_items')}.product_id = {$product} ";
            }
            if ($product && $serial) { $ai .= " AND "; }
            if ($serial) {
                $ai .= " {$this->db->dbprefix('adjustment_items')}.serial_no LIKe '%{$serial}%' ";
            }
            $ai .= " GROUP BY {$this->db->dbprefix('adjustment_items')}.adjustment_id ) FAI";
            $this->load->library('datatables');
            $this->datatables
            ->select("DATE_FORMAT(date, '%Y-%m-%d %T') as date, reference_no, warehouses.name as wh_name, CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as created_by, note, FAI.item_nane as iname, {$this->db->dbprefix('adjustments')}.id as id", FALSE)
            ->from('adjustments')
            ->join($ai, 'FAI.adjustment_id=adjustments.id', 'left')
            ->join('users', 'users.id=adjustments.created_by', 'left')
            ->join('warehouses', 'warehouses.id=adjustments.warehouse_id', 'left');

            if ($user) {
                $this->datatables->where('adjustments.created_by', $user);
            }
            if ($product) {
                $this->datatables->where('FAI.product_id', $product);
            }
            if ($serial) {
                $this->datatables->like('FAI.serial_no', $serial);
            }
            if ($warehouse) {
                $this->datatables->where('adjustments.warehouse_id', $warehouse);
            }
            if ($reference_no) {
                $this->datatables->like('adjustments.reference_no', $reference_no, 'both');
            }
            if ($start_date) {
                $this->datatables->where($this->db->dbprefix('adjustments').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            echo $this->datatables->generate();
        }

    }

    function get_deposits($company_id = NULL)
    {
        $this->sma->checkPermissions('customers', TRUE);
        $this->load->library('datatables');
        $this->datatables
            ->select("date, amount, paid_by, CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as created_by, note", FALSE)
            ->from("deposits")
            ->join('users', 'users.id=deposits.created_by', 'left')
            ->where($this->db->dbprefix('deposits').'.company_id', $company_id);
        echo $this->datatables->generate();
    }

    function tax()
    {
        $this->sma->checkPermissions();
        $start_date = $this->input->post('start_date') ? $this->input->post('start_date') : NULL;
        $end_date = $this->input->post('end_date') ? $this->input->post('end_date') : NULL;
        if ($start_date) {
            $start_date = $this->sma->fld($start_date);
            $end_date = $this->sma->fld($end_date);
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        $this->data['sale_tax'] = $this->reports_model->getSalesTax($start_date, $end_date);
        $this->data['purchase_tax'] = $this->reports_model->getPurchasesTax($start_date, $end_date);
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('tax_report')));
        $meta = array('page_title' => lang('tax_report'), 'bc' => $bc);
        $this->page_construct('reports/tax', $meta, $this->data);
    }

    function get_sale_taxes($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('tax', TRUE);
        $biller = $this->input->get('biller') ? $this->input->get('biller') : NULL;
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;
        if ($start_date) {
            $start_date = $this->sma->fld($start_date);
            $end_date = $this->sma->fld($end_date);
        }

        if ($pdf || $xls) {

            $this->db
                ->select("date, reference_no, CONCAT({$this->db->dbprefix('warehouses')}.name, ' (', {$this->db->dbprefix('warehouses')}.code, ')') as warehouse, biller, igst, cgst, sgst, product_tax, order_tax, grand_total, paid, payment_status")
                ->from('sales')
                ->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left')
                ->order_by('date desc');

            if ($biller) {
                $this->db->where('biller_id', $biller);
            }
            if ($warehouse) {
                $this->db->where('warehouse_id', $warehouse);
            }
            if ($start_date) {
                $this->db->where('date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            $q = $this->db->get();
            if ($q != false && $q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {
                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('sales_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('warehouse'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('biller'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('igst'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('cgst'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('sgst'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('product_tax'));
                $this->excel->getActiveSheet()->SetCellValue('I1', lang('order_tax'));
                $this->excel->getActiveSheet()->SetCellValue('J1', lang('grand_total'));

                $row = 2;
                $total = $order_tax = $product_tax = $igst = $cgst = $sgst = 0;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->warehouse);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->biller);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $this->sma->formatDecimal($data_row->igst));
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $this->sma->formatDecimal($data_row->cgst));
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $this->sma->formatDecimal($data_row->sgst));
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, $this->sma->formatDecimal($data_row->product_tax));
                    $this->excel->getActiveSheet()->SetCellValue('I' . $row, $this->sma->formatDecimal($data_row->order_tax));
                    $this->excel->getActiveSheet()->SetCellValue('J' . $row, $this->sma->formatDecimal($data_row->grand_total));
                    $igst += $data_row->igst;
                    $cgst += $data_row->cgst;
                    $sgst += $data_row->sgst;
                    $product_tax += $data_row->product_tax;
                    $order_tax += $data_row->order_tax;
                    $total += $data_row->grand_total;
                    $row++;
                }
                $this->excel->getActiveSheet()->getStyle("E" . $row . ":J" . $row)->getBorders()
                    ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                $this->excel->getActiveSheet()->SetCellValue('E' . $row, $this->sma->formatDecimal($igst));
                $this->excel->getActiveSheet()->SetCellValue('F' . $row, $this->sma->formatDecimal($cgst));
                $this->excel->getActiveSheet()->SetCellValue('G' . $row, $this->sma->formatDecimal($sgst));
                $this->excel->getActiveSheet()->SetCellValue('H' . $row, $this->sma->formatDecimal($product_tax));
                $this->excel->getActiveSheet()->SetCellValue('I' . $row, $this->sma->formatDecimal($order_tax));
                $this->excel->getActiveSheet()->SetCellValue('J' . $row, $this->sma->formatDecimal($total));

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('E2:E' . $row)->getAlignment()->setWrapText(true);
                $filename = 'sale_tax_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);
            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->load->library('datatables');
            $this->datatables
                ->select("DATE_FORMAT(date, '%Y-%m-%d %T') as date, reference_no, sale_status, CONCAT({$this->db->dbprefix('warehouses')}.name, ' (', {$this->db->dbprefix('warehouses')}.code, ')') as warehouse, biller, ".($this->Settings->indian_gst ? "igst, cgst, sgst," : "")." product_tax, order_tax, grand_total, {$this->db->dbprefix('sales')}.id as id", FALSE)
                ->from('sales')
                ->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left');
            if ($biller) {
                $this->datatables->where('biller_id', $biller);
            }
            if ($warehouse) {
                $this->datatables->where('warehouse_id', $warehouse);
            }
            if ($start_date) {
                $this->datatables->where('date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            echo $this->datatables->generate();
        }
    }

    function get_purchase_taxes($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('tax', TRUE);
        $supplier = $this->input->get('supplier') ? $this->input->get('supplier') : NULL;
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;
        if ($start_date) {
            $start_date = $this->sma->fld($start_date);
            $end_date = $this->sma->fld($end_date);
        }

        if ($pdf || $xls) {

            $this->db
                ->select("date, reference_no, CONCAT({$this->db->dbprefix('warehouses')}.name, ' (', {$this->db->dbprefix('warehouses')}.code, ')') as warehouse, supplier, igst, cgst, sgst, product_tax, order_tax, grand_total, paid")
                ->from('purchases')
                ->join('warehouses', 'warehouses.id=purchases.warehouse_id', 'left')
                ->order_by('purchases.date desc');

            if ($supplier) {
                $this->db->where('supplier_id', $supplier);
            }
            if ($warehouse) {
                $this->db->where('warehouse_id', $warehouse);
            }
            if ($start_date) {
                $this->db->where('date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            $q = $this->db->get();
            if ($q != false && $q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {
                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('sales_report'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('warehouse'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('supplier'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('igst'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('cgst'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('sgst'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('product_tax'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('order_tax'));
                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('grand_total'));

                    $row = 2;
                    $total = $order_tax = $product_tax = $igst = $cgst = $sgst = 0;
                    foreach ($data as $data_row) {
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->warehouse);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->supplier);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $this->sma->formatDecimal($data_row->igst));
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $this->sma->formatDecimal($data_row->cgst));
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $this->sma->formatDecimal($data_row->sgst));
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $this->sma->formatDecimal($data_row->product_tax));
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $this->sma->formatDecimal($data_row->order_tax));
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $this->sma->formatDecimal($data_row->grand_total));
                        $igst += $data_row->igst;
                        $cgst += $data_row->cgst;
                        $sgst += $data_row->sgst;
                        $product_tax += $data_row->product_tax;
                        $order_tax += $data_row->order_tax;
                        $total += $data_row->grand_total;
                        $row++;
                    }
                    $this->excel->getActiveSheet()->getStyle("E" . $row . ":J" . $row)->getBorders()
                        ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $this->sma->formatDecimal($igst));
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $this->sma->formatDecimal($cgst));
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $this->sma->formatDecimal($sgst));
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, $this->sma->formatDecimal($product_tax));
                    $this->excel->getActiveSheet()->SetCellValue('I' . $row, $this->sma->formatDecimal($order_tax));
                    $this->excel->getActiveSheet()->SetCellValue('J' . $row, $this->sma->formatDecimal($total));

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                    $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                    $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                    $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                    $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
                    $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $this->excel->getActiveSheet()->getStyle('E2:E' . $row)->getAlignment()->setWrapText(true);
                    $filename = 'purchase_tax_report';
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
                $this->session->set_flashdata('error', lang('nothing_found'));
                redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->load->library('datatables');
            $this->datatables
                ->select("DATE_FORMAT(date, '%Y-%m-%d %T') as date, reference_no, status, CONCAT({$this->db->dbprefix('warehouses')}.name, ' (', {$this->db->dbprefix('warehouses')}.code, ')') as warehouse, supplier, ".($this->Settings->indian_gst ? "igst, cgst, sgst," : "")." product_tax, order_tax, grand_total, {$this->db->dbprefix('purchases')}.id as id", FALSE)
                ->from('purchases')
                ->join('warehouses', 'warehouses.id=purchases.warehouse_id', 'left');
            if ($supplier) {
                $this->datatables->where('supplier_id', $supplier);
            }
            if ($warehouse) {
                $this->datatables->where('warehouse_id', $warehouse);
            }
            if ($start_date) {
                $this->datatables->where('date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            echo $this->datatables->generate();
        }
    }
	
    function sales_report_for_each_customer()
    {
        $this->sma->checkPermissions('sales_report_for_each_customer');

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        
        $start_date = $this->input->post('start_date') ? $this->input->post('start_date') : 'January '.date('Y');
        $end_date = $this->input->post('end_date') ? $this->input->post('end_date') : 'December '.date('Y');
        $temp = strtotime($start_date);
        $temp2 = strtotime($end_date);        
        if ($temp > $temp2) $end_date = $start_date;
        
        if ($this->input->post('customer') > 0) {
            $temp = $this->site->api_select_some_fields_with_where("
                id, parent_id
                "
                ,"sma_companies"
                ,"id = ".$this->input->post('customer')
                ,"arr"
            );
            if ($temp[0]['parent_id'] > 0)
                $condition_parent .= ' and t1.id = '.$temp[0]['parent_id'];
            else
                $condition_parent .= ' and t1.id = '.$temp[0]['id'];

            $config_data_2 = array(
                'id' => $this->input->post('customer'),
                'table_name' => 'sma_companies',
                'condition_id_name' => 'id',
            );
            $temp = $this->site->api_get_condition_category($config_data_2);   
            $condition_customer .= $temp;                
        }

        if ($this->input->post('city') != '' && $this->input->post('city') != 'All Cities' && $this->input->post('city') != '0')
            $condition .= " and t1.city = '".$this->input->post('city')."'";
            
        if ($this->input->post('sale_person') != '' && $this->input->post('sale_person') != 'All Sale Person' && $this->input->post('sale_person') != '0')
            $condition .= " and t1.sale_person_id = ".$this->input->post('sale_person');

        if ($this->input->post('payment_status') > 0) {
            if ($this->input->post('payment_status') == 1)
                $condition .= " and t2.payment_status = 'paid'";
            if ($this->input->post('payment_status') == 2)
                $condition .= " and (t2.payment_status = 'due' or t2.payment_status = 'partial')";
            if ($this->input->post('payment_status') == 3)
                $condition .= " and t2.payment_status = 'pending'";
        }
        $condition .= " and t2.reference_no != ''";

        $temp = date_create($start_date);
        $select_start_year = date_format($temp, 'Y');
        $this->data['select_start_year'] = $select_start_year;
        $temp = date_create($end_date);
        $select_end_year = date_format($temp, 'Y');
        $this->data['select_end_year'] = $select_end_year;

        if ($this->input->post('customer_group_id') > 0)
            $condition .= " and t1.customer_group_id = ".$this->input->post('customer_group_id');

        $temp = '';
        for ($i=1;$i<=12;$i++) {
            if ($i < 12 && $j < $select_end_year)
                $temp .= "
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and t2.payment_status = 'paid' THEN t2.grand_total END) as total_paid_".$i.",
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and (t2.payment_status = 'due' or t2.payment_status = 'partial') THEN t2.grand_total END) as total_due_".$i.",
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and t2.payment_status = 'pending' THEN t2.grand_total END) as total_pending_".$i.",
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and t2.payment_status = 'paid' THEN 1 ELSE 0 END) as total_paid_count_".$i.",
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and (t2.payment_status = 'due' or t2.payment_status = 'partial') THEN 1 ELSE 0 END) as total_due_count_".$i.",
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and t2.payment_status = 'pending' THEN 1 ELSE 0 END) as total_pending_count_".$i.",
                ";
            else 
                $temp .= "
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and t2.payment_status = 'paid' THEN t2.grand_total END) as total_paid_".$i.",
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and (t2.payment_status = 'due' or t2.payment_status = 'partial') THEN t2.grand_total END) as total_due_".$i.",
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and t2.payment_status = 'pending' THEN t2.grand_total END) as total_pending_".$i.",
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and t2.payment_status = 'paid' THEN 1 ELSE 0 END) as total_paid_count_".$i.",
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and (t2.payment_status = 'due' or t2.payment_status = 'partial') THEN 1 ELSE 0 END) as total_due_count_".$i.",
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and t2.payment_status = 'pending' THEN 1 ELSE 0 END) as total_pending_count_".$i."
                ";
        }
        
        $temp_array_join = array();
        for ($i=$select_start_year;$i<=$select_end_year;$i++) {
            ${'select_data_'.$i} = $this->site->api_select_some_fields_with_where("
                t1.id, t1.name, t1.company, t1.sale_person_id, t2.grand_total, t2.payment_status, t2.id as sale_id, t2.customer_id,
                ".$temp."
                "
                ,"sma_companies as t1 inner join sma_sales as t2 on t1.id = t2.customer_id"
                ,"t1.parent_id = 0 ".$condition_parent." group by t1.id order by t1.company asc"
                ,"arr"
            );
            for ($temp_i=0;$temp_i<count(${'select_data_'.$i});$temp_i++) {
                if (${'select_data_'.$i}[$temp_i]['id'] > 0) {

                    $config_data = array(
                        'table_name' => 'sma_companies',
                        'select_table' => 'sma_companies',
                        'translate' => '',
                        'select_condition' => "parent_id = ".${'select_data_'.$i}[$temp_i]['customer_id']." ".$condition_customer." order by company asc",
                    );
                    $temp_company_branch = $this->site->api_select_data_v2($config_data);

                    if (count($temp_company_branch) > 0) {

                        foreach(array_keys(${'select_data_'.$i}[$temp_i]) as $key){
                            ${'select_data_'.$i}[$temp_i]['company_branch_0_'.${'select_data_'.$i}[$temp_i]['id']][0][$key] = ${'select_data_'.$i}[$temp_i][$key];
                        }
                        ${'select_data_'.$i}[$temp_i]['company_branch_0_'.${'select_data_'.$i}[$temp_i]['id']][0]['title'] = ${'select_data_'.$i}[$temp_i]['company'];
                        $temp22 = $this->site->api_select_some_fields_with_where("first_name,last_name
                            "
                            ,"sma_users"
                            ,"id = ".${'select_data_'.$i}[$temp_i]['sale_person_id']
                            ,"arr"
                        );
                        ${'select_data_'.$i}[$temp_i]['company_branch_0_'.${'select_data_'.$i}[$temp_i]['id']][0]['sale_person_fullname'] = $temp22[0]['first_name'].' '.$temp22[0]['last_name'];

                        for ($temp_i2=0;$temp_i2<count($temp_company_branch);$temp_i2++) {
                            ${'select_data_'.$i}[$temp_i]['company_branch_'.${'select_data_'.$i}[$temp_i]['id']][$temp_i2]['title'] = $temp_company_branch[$temp_i2]['company'];
                            ${'select_data_'.$i}[$temp_i]['company_branch_'.${'select_data_'.$i}[$temp_i]['id']][$temp_i2]['id'] = $temp_company_branch[$temp_i2]['id'];
                            $temp22 = $this->site->api_select_some_fields_with_where("first_name,last_name
                                "
                                ,"sma_users"
                                ,"id = ".$temp_company_branch[$temp_i2]['sale_person_id']
                                ,"arr"
                            );
                            ${'select_data_'.$i}[$temp_i]['company_branch_'.${'select_data_'.$i}[$temp_i]['id']][$temp_i2]['sale_person_fullname'] = $temp22[0]['first_name'].' '.$temp22[0]['last_name'];
                            
                            $temp_company_branch_2 = $this->site->api_select_some_fields_with_where("
                                t1.name, t1.sale_person_id, t2.grand_total, t2.payment_status, t2.id as sale_id,
                                ".$temp."
                                "
                                ,"sma_companies as t1 inner join sma_sales as t2 on t1.id = t2.customer_id"
                                ,"t1.id = ".$temp_company_branch[$temp_i2]['id']." and convert(t2.date, Date) between STR_TO_DATE('01 ".$start_date."', '%d %M %Y') and STR_TO_DATE('31 ".$end_date."', '%d %M %Y') and YEAR(t2.date) = ".$i." ".$condition." group by t1.id order by t1.name asc"
                                ,"arr"
                            );
                            foreach(array_keys(${'select_data_'.$i}[$temp_i]) as $key){
                                ${'select_data_'.$i}[$temp_i]['company_branch_0_'.${'select_data_'.$i}[$temp_i]['id']][count($temp_company_branch_2)][$key] = ${'select_data_'.$i}[$temp_i][$key];
                            }                              
                            for ($temp_i3=0;$temp_i3<count($temp_company_branch_2);$temp_i3++) {
                                foreach(array_keys($temp_company_branch_2[$temp_i3]) as $key){
                                    ${'select_data_'.$i}[$temp_i]['company_branch_'.${'select_data_'.$i}[$temp_i]['id']][$temp_i2][$key] = $temp_company_branch_2[$temp_i3][$key];
                                    for ($j2=1;$j2<=12;$j2++) {
                                        if ($key == 'total_paid_'.$j2)
                                            ${'select_data_'.$i}[$temp_i]['total_paid_'.$j2] += $temp_company_branch_2[$temp_i3]['total_paid_'.$j2];
                                        if ($key == 'total_due_'.$j2)
                                            ${'select_data_'.$i}[$temp_i]['total_due_'.$j2] += $temp_company_branch_2[$temp_i3]['total_due_'.$j2];
                                        if ($key == 'total_pending_'.$j2)
                                            ${'select_data_'.$i}[$temp_i]['total_pending_'.$j2] += $temp_company_branch_2[$temp_i3]['total_pending_'.$j2];
                                    }
                                }
                            }
                        }
                    }
                    array_push($temp_array_join, ${'select_data_'.$i}[$temp_i]['id']);
                }
            }


            if ($i == $select_start_year) {
                $temp2 = date_create($start_date);
                $temp3 = date_format($temp2, 'd');
                $temp4 = date_format($temp2, 'm');
                $temp5 = date_format($temp2, 'Y');
                $this->data['select_start_date_'.$i] = date_format($temp2, 'F Y');
            }
            else
                $this->data['select_start_date_'.$i] = 'January '.$i;                
            
            if ($i == $select_end_year) {
                $temp2 = date_create($end_date);
                $this->data['select_end_date_'.$i] = date_format($temp2, 'F Y');;
            }
            else
                $this->data['select_end_date_'.$i] = 'December '.$i;                           
        }
        $temp_array_join_2 = array_unique($temp_array_join);
        $array_join = array();
        foreach($temp_array_join_2 as $val){            
            array_push($array_join, $val);
        }        
                
        $array_join_2 = array();
        for ($j=0;$j<count($array_join);$j++) {
            if ($array_join[$j] != '') {
                $temp22 = $this->site->api_select_some_fields_with_where("name,sale_person_id
                    "
                    ,"sma_companies"
                    ,"id = ".$array_join[$j]
                    ,"arr"
                );
                
                $array_join_2[$j]['name'] = $temp22[0]['name'];
                $array_join_2[$j]['id'] = $array_join[$j];

                if ($temp22[0]['sale_person_id'] != '') {
                    $temp2 = $this->site->api_select_some_fields_with_where("first_name,last_name
                        "
                        ,"sma_users"
                        ,"id = ".$temp22[0]['sale_person_id']
                        ,"arr"
                    );
                    $array_join_2[$j]['sale_person_fullname'] = $temp2[0]['first_name'].' '.$temp2[0]['last_name'];
                }
                else
                    $array_join_2[$j]['sale_person_fullname'] = '';

            }
        }

        for ($i=$select_start_year;$i<=$select_end_year;$i++) {
            for ($j=0;$j<count($array_join_2);$j++) {
                $b = 0;
                for ($k=0;$k<count(${'select_data_'.$i});$k++) {
                    if ($array_join_2[$j]['id'] == ${'select_data_'.$i}[$k]['id']) {                      
                        $b = 1;
                        $temp_index_k = $k;
                        break;
                    }                    
                }
                if ($b == 1) {
                    foreach(array_keys(${'select_data_'.$i}[$temp_index_k]) as $key){
                        $array_join_2[$j][$key] = ${'select_data_'.$i}[$temp_index_k][$key];
                        for ($j2=1;$j2<=12;$j2++) {
                            if ($key == 'total_paid_'.$j2 || $key == 'total_due_'.$j2 || $key == 'total_pending_'.$j2) {
                                $array_join_2[$j]['all_total_each_month_'.$i.'_'.$j2] = ${'select_data_'.$i}[$temp_index_k]['total_paid_'.$j2] + ${'select_data_'.$i}[$temp_index_k]['total_due_'.$j2] + ${'select_data_'.$i}[$temp_index_k]['total_pending_'.$j2];
                            }
                        }                        
                    }             
                }
                else {
                    for ($j2=1;$j2<=12;$j2++) {
                        $array_join_2[$j]['all_total_each_month_'.$i.'_'.$j2] = 0;
                    }                        
                }
            }
        }
                
        $sort_by = $this->input->post('sort_by');
        $order_by = $this->input->post('order_by');
        $temp_submit_form = $this->input->post('submit_report');
        if ($sort_by == '' ) {
            $sort_by = 'name';
            if ($temp_submit_form == '') {
                $d = new DateTime('01-'.date('m').'-'.date('Y'));
                $d2 = $d->modify('first day of previous month');
                $temp_m = $d2->format('m');
                $temp_m = str_replace('0','',$temp_m);
                $temp_y = $d2->format('Y');
                if (date('Y') == $select_end_year && date('m') == 1)
                    $sort_by = 'name';
                else {                
                    $sort_by = 'all_total_each_month_'.$temp_y.'_'.$temp_m;
                    $order_by = 'desc';                
                }
            }
            else {
                $temp = date_create($start_date);
                $temp_m_start = date_format($temp, 'm');
                $temp_y_start = date_format($temp, 'Y');
                $temp_m_start = str_replace('0','',$temp_m_start);

                $temp = date_create($end_date);
                $temp_m_end = date_format($temp, 'm');
                $temp_y_end = date_format($temp, 'Y');
                $temp_m_end = str_replace('0','',$temp_m_end);
                                                
                $d = new DateTime('01-'.$temp_m_end.'-'.$temp_y_end);
                $d2 = $d->modify('first day of previous month');
                $temp_m = $d2->format('m');
                $temp_m = str_replace('0','',$temp_m);
                $temp_y = $d2->format('Y');

                if ($temp_y_start == $temp_y_end && $temp_m_start == $temp_m_end)
                    $sort_by = 'name';
                else {
                    $sort_by = 'all_total_each_month_'.$temp_y.'_'.$temp_m;
                    $order_by = 'desc';
                }                
            }
        }
        
        $array_join_3 = array();
        for ($i=0;$i<count($array_join_2);$i++) {
            foreach(array_keys($array_join_2[$i]) as $key){
                if ($sort_by == $key) 
                    $array_join_3[$i][$key] = $array_join_2[$i][$key];
            }
            foreach(array_keys($array_join_2[$i]) as $key){
                if ($sort_by != $key) 
                    $array_join_3[$i][$key] = $array_join_2[$i][$key];
            }            
        }
        $array_join_2 = $array_join_3;
         
        if ($order_by == '' || $order_by == 'asc')
            sort($array_join_2);
        else
            rsort($array_join_2);

        for ($i=$select_start_year;$i<=$select_end_year;$i++) {
            $temp_array_join_2 = array();
            for ($j=0;$j<count($array_join_2);$j++) {
                $b = 0;
                for ($k=0;$k<count(${'select_data_'.$i});$k++) {
                    if ($array_join_2[$j]['id'] == ${'select_data_'.$i}[$k]['id']) {                      
                        $b = 1;
                        $temp_index_k = $k;
                        break;
                    }                    
                }
                if ($b == 1) {
                    foreach(array_keys(${'select_data_'.$i}[$temp_index_k]) as $key){
                        $temp_array_join_2[$j][$key] = ${'select_data_'.$i}[$temp_index_k][$key];
                        for ($j2=1;$j2<=12;$j2++) {
                            if ($key == 'total_paid_'.$j2 || $key == 'total_due_'.$j2 || $key == 'total_pending_'.$j2)
                                $temp_array_join_2[$j]['all_total_each_month_'.$i.'_'.$j2] = $array_join_2[$j]['all_total_each_month_'.$i.'_'.$j2];
                        }                        
                    }                                                  
                }
                else {
                    $temp_array_join_2[$j]['id'] = $array_join_2[$j]['id'];                        
                    $temp_array_join_2[$j]['name'] = $array_join_2[$j]['name'];
                }
                $temp_array_join_2[$j]['sale_person_fullname'] = $array_join_2[$j]['sale_person_fullname'];
            }
            $this->data['select_data_'.$i] = $temp_array_join_2;            
        }
        

        $temp = $this->site->api_select_some_fields_with_where("distinct city
            "
            ,"sma_companies"
            ,"id > 0 and group_id = 3 order by city asc"
            ,"obj"
        );
        $this->data['city'] = $temp;

        $temp = $this->site->api_select_some_fields_with_where("*
            "
            ,"sma_companies"
            ,"id > 0 and group_id = 3 order by name asc"
            ,"arr"
        );
        $this->data['customer'] = $temp;

        $temp = $this->site->api_select_some_fields_with_where("*
            "
            ,"sma_customer_groups"
            ,"id > 0 order by name asc"
            ,"arr"
        );
        $this->data['customer_group'] = $temp;
                
        $this->load->admin_model('companies_model');
        $this->data['sales_person'] = $this->companies_model->getAllSalePerson();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('sales_report_for_each_customer')));
        $meta = array('page_title' => lang('sales_report_for_each_customer'), 'bc' => $bc);
        $this->page_construct('reports/sales_report_for_each_customer', $meta, $this->data);
    }

	
    function getSalesCustomerReport($csv = NULL)
    {
        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : 'January '.date('Y');
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : 'December '.date('Y');
        $temp = strtotime($start_date);
        $temp2 = strtotime($end_date);        
        if ($temp > $temp2) $end_date = $start_date;
                
        $temp = date_create($start_date);
        $select_start_year = date_format($temp, 'Y');
        $this->data['select_start_year'] = $select_start_year;
        $temp = date_create($end_date);
        $select_end_year = date_format($temp, 'Y');
        $this->data['select_end_year'] = $select_end_year;

        $condition = '';

        if ($this->input->get('customer') > 0) {
            $temp = $this->site->api_select_some_fields_with_where("
                id, parent_id
                "
                ,"sma_companies"
                ,"id = ".$this->input->get('customer')
                ,"arr"
            );
            if ($temp[0]['parent_id'] > 0)
                $condition_parent .= ' and t1.id = '.$temp[0]['parent_id'];
            else
                $condition_parent .= ' and t1.id = '.$temp[0]['id'];

            $config_data_2 = array(
                'id' => $this->input->get('customer'),
                'table_name' => 'sma_companies',
                'condition_id_name' => 't1.id',
            );
            $temp = $this->site->api_get_condition_category($config_data_2);   
            $condition_customer .= $temp;   
        }


        if ($this->input->get('city') != '' && $this->input->get('city') != 'All Cities' && $this->input->get('city') != '0')
            $condition .= " and t1.city = '".$this->input->get('city')."'";

        if ($this->input->get('sale_person') != '' && $this->input->get('sale_person') != 'All Sale Person' && $this->input->get('sale_person') != '0')
            $condition .= " and t1.sale_person_id = ".$this->input->get('sale_person');

        if ($this->input->get('payment_status') > 0) {
            if ($this->input->get('payment_status') == 1)
                $condition .= " and t2.payment_status = 'paid'";
            if ($this->input->get('payment_status') == 2)
                $condition .= " and (t2.payment_status = 'due' or t2.payment_status = 'partial')";
            if ($this->input->get('payment_status') == 3)
                $condition .= " and t2.payment_status = 'pending'";
        }
            
        $temp = '';
        for ($i=1;$i<=12;$i++) {
            if ($i < 12 && $j < $select_end_year)
                $temp .= "
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and t2.payment_status = 'paid' THEN t2.grand_total END) as total_paid_".$i.",
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and (t2.payment_status = 'due' or t2.payment_status = 'partial') THEN t2.grand_total END) as total_due_".$i.",
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and t2.payment_status = 'pending' THEN t2.grand_total END) as total_pending_".$i.",
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and t2.payment_status = 'paid' THEN 1 ELSE 0 END) as total_paid_count_".$i.",
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and (t2.payment_status = 'due' or t2.payment_status = 'partial') THEN 1 ELSE 0 END) as total_due_count_".$i.",
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and t2.payment_status = 'pending' THEN 1 ELSE 0 END) as total_pending_count_".$i.",
                ";
            else 
                $temp .= "
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and t2.payment_status = 'paid' THEN t2.grand_total END) as total_paid_".$i.",
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and (t2.payment_status = 'due' or t2.payment_status = 'partial') THEN t2.grand_total END) as total_due_".$i.",
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and t2.payment_status = 'pending' THEN t2.grand_total END) as total_pending_".$i.",
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and t2.payment_status = 'paid' THEN 1 ELSE 0 END) as total_paid_count_".$i.",
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and (t2.payment_status = 'due' or t2.payment_status = 'partial') THEN 1 ELSE 0 END) as total_due_count_".$i.",
                    SUM(CASE WHEN MONTH(t2.date) = ".$i." and t2.payment_status = 'pending' THEN 1 ELSE 0 END) as total_pending_count_".$i."
                ";
        }

        $temp_array_join = array();        
        for ($i=$select_start_year;$i<=$select_end_year;$i++) {
            ${'select_data_'.$i} = $this->site->api_select_some_fields_with_where("
                t1.id, t1.name, t1.sale_person_id, t2.grand_total, t2.payment_status, t2.id as sale_id, 
                ".$temp."
                "
                ,"sma_companies as t1 inner join sma_sales as t2 on t1.id = t2.customer_id"
                ,"convert(t2.date, Date) between STR_TO_DATE('01 ".$start_date."', '%d %M %Y') and STR_TO_DATE('31 ".$end_date."', '%d %M %Y') and YEAR(t2.date) = ".$i." ".$condition." group by t1.id order by t1.name asc"
                ,"arr"
            );
            for ($temp_i=0;$temp_i<count(${'select_data_'.$i});$temp_i++) {
                if (${'select_data_'.$i}[$temp_i]['id'] > 0)
                    array_push($temp_array_join, ${'select_data_'.$i}[$temp_i]['id']);                
            }

            if ($i == $select_start_year) {
                $temp2 = date_create($start_date);
                $temp3 = date_format($temp2, 'd');
                $temp4 = date_format($temp2, 'm');
                $temp5 = date_format($temp2, 'Y');
                $this->data['select_start_date_'.$i] = date_format($temp2, 'F Y');
            }
            else
                $this->data['select_start_date_'.$i] = 'January '.$i;                
            
            if ($i == $select_end_year) {
                $temp2 = date_create($end_date);
                $this->data['select_end_date_'.$i] = date_format($temp2, 'F Y');;
            }
            else
                $this->data['select_end_date_'.$i] = 'December '.$i;               
            
            $this->data['select_data_'.$i] = ${'select_data_'.$i};
        }

        $temp_array_join_2 = array_unique($temp_array_join);
        $array_join = array();
        foreach($temp_array_join_2 as $val){            
            array_push($array_join, $val);
        }                
        
        $array_join_2 = array();
        for ($j=0;$j<count($array_join);$j++) {
            if ($array_join[$j] != '') {
                $temp22 = $this->site->api_select_some_fields_with_where("name,sale_person_id
                    "
                    ,"sma_companies"
                    ,"id = ".$array_join[$j]
                    ,"arr"
                );
                $array_join_2[$j]['name'] = $temp22[0]['name'];
                $array_join_2[$j]['id'] = $array_join[$j];
                $array_join_2[$j]['sale_person_id'] = $temp22[0]['sale_person_id'];
            }
        }
        sort($array_join_2);

        for ($i=$select_start_year;$i<=$select_end_year;$i++) {
            $temp_array_join_2 = array();
            for ($j=0;$j<count($array_join_2);$j++) {
                $b = 0;
                for ($k=0;$k<count(${'select_data_'.$i});$k++) {
                    if ($array_join_2[$j]['id'] == ${'select_data_'.$i}[$k]['id']) {                      
                        $b = 1;
                        $temp_index_k = $k;
                        break;
                    }                    
                }
                if ($b == 1) {
                    foreach(array_keys(${'select_data_'.$i}[$temp_index_k]) as $key){
                        $temp_array_join_2[$j][$key] = ${'select_data_'.$i}[$temp_index_k][$key];
                    }                                                                    
                }
                else {
                    $temp_array_join_2[$j]['id'] = $array_join_2[$j]['id'];                        
                    $temp_array_join_2[$j]['name'] = $array_join_2[$j]['name'];
                    $temp_array_join_2[$j]['sale_person_id'] = $array_join_2[$j]['sale_person_id'];
                }
            }
            ${'select_data_'.$i} = $temp_array_join_2;            
        }
        
        if ($csv) {
                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('sales_report_for_each_customer'));

                $style_th = array(
                    'font' => 
                        array('size' => 12, 'color' => array('rgb' => '000000'),'bold' => true, 
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                                'background-color' => array('rgb' => 'd0d0d0')
                        ),
                    'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)                        
                );                        
                        
                $style_blue = array(
                    'font' => array('size' => 14,'bold' => true, 'color' => array('rgb' => 'ffffff')),
                    'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)                    
                );
                $style_money = array('borders' => array('right' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),'font' => array('size' => 12, 'bold' => true, 'color' => array('rgb' => 'ffffff')));
                $style_money_big = array('font' => array('size' => 12,'bold' => true));
                $style_name = array('borders' => array('right' => array('style' => PHPExcel_Style_Border::BORDER_HAIR)));
                $BStyle = array(
                  'borders' => array(
                    'outline' => array (
                      'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                  )
                );
                
                $j = 1;
                $k = 2;
                $r = 3;
                for ($y=$select_start_year;$y<=$select_end_year;$y++) {
                    
                    if (($y%2) != 0) { 
                        $bg_header = '428bca';
                        $bg_footer = '428bca';
                    } 
                    else {
                        $bg_header = 'd9534f';
                        $bg_footer = 'd9534f';
                    }
                    
                    $this->excel->getActiveSheet()->mergeCells('A'.($r - 2).':N'.$r);                        

                    $temp = $this->data['select_start_date_'.$y].' -> '.$this->data['select_end_date_'.$y];
                    $this->excel->getActiveSheet()->SetCellValue('A'.($r - 2), $temp)->getStyle('A'.($r - 2), $temp)->applyFromArray($style_blue);
                    $this->excel->getActiveSheet()->getStyle('A'.($r - 2))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($bg_header);                    
                    $r = $r + 2;

                    $this->excel->getActiveSheet()->SetCellValue('A'.($r - 1), lang('cal_customer'))->getStyle('A'.($r - 1), lang('cal_customer'))->applyFromArray($style_th);
                    $this->excel->getActiveSheet()->SetCellValue('B'.($r - 1), lang('cal_person'))->getStyle('B'.($r - 1), lang('cal_person'))->applyFromArray($style_th);

                    $jj = 1;                        
                    foreach (range('C', 'N') as $column) {
                        $dateObj   = DateTime::createFromFormat('!m', $jj);
                        $monthName = $dateObj->format('F');
                        $this->excel->getActiveSheet()->SetCellValue($column.($r - 1), $monthName)->getStyle($column.($r - 1), $monthName)->applyFromArray($style_th);                        
                        $jj++;
                    }

                    if (count(${'select_data_'.$y}) <= 0) {
                        $this->excel->getActiveSheet()->mergeCells('A'.$r.':Z'.$r);
                        $this->excel->getActiveSheet()->SetCellValue('A'.$r, 'No sale record');
                        
                    }
                    for ($i=0;$i<count(${'select_data_'.$y});$i++) {
                        for ($jjj=1;$jjj<=12;$jjj++) {
                            ${$y.'_grand_total_paid_'.$jjj} += ${'select_data_'.$y}[$i]['total_paid_'.$jjj];
                            ${$y.'_grand_total_due_'.$jjj} += ${'select_data_'.$y}[$i]['total_due_'.$jjj];
                            ${$y.'_grand_total_pending_'.$jjj} += ${'select_data_'.$y}[$i]['total_pending_'.$jjj];
                    
                            ${$y.'_grand_paid_count_'.$jjj} += ${'select_data_'.$y}[$i]['total_paid_count_'.$jjj];
                            ${$y.'_grand_due_count_'.$jjj} += ${'select_data_'.$y}[$i]['total_due_count_'.$jjj];
                            ${$y.'_grand_pending_count_'.$jjj} += ${'select_data_'.$y}[$i]['total_pending_count_'.$jjj];
                        }
                    }

                    $r_bg = 1;
                    for ($i=0;$i<count(${'select_data_'.$y});$i++) {      
                        if (($r_bg%2) != 0) $bg_record = "eeecec"; else $bg_record = "ffffff";

                        if (${'select_data_'.$y}[$i]['sale_person_id'] != '') {
                            $temp2 = $this->site->api_select_some_fields_with_where("first_name,last_name
                                "
                                ,"sma_users"
                                ,"id = ".${'select_data_'.$y}[$i]['sale_person_id']
                                ,"arr"
                            );
                            ${'select_data_'.$y}[$i]['first_name'] = $temp2[0]['first_name'];
                            ${'select_data_'.$y}[$i]['last_name'] = $temp2[0]['last_name'];
                        }

                        $this->excel->getActiveSheet()->SetCellValue('A'.$r, ${'select_data_'.$y}[$i]['name']);
                        $this->excel->getActiveSheet()->SetCellValue('B'.$r, ${'select_data_'.$y}[$i]['first_name'].' '.${'select_data_'.$y}[$i]['last_name']);
                        $this->excel->getActiveSheet()->mergeCells('A'.$r.':A'.($r+3));                        
                        $this->excel->getActiveSheet()->mergeCells('B'.$r.':B'.($r+3));                        

                        $jj = 1;                        
                        foreach (range('C', 'N') as $column) {
                            
                            $temp4 = 0;
                            $temp = '';
                            $temp_all_total_each_month = ${'select_data_'.$y}[$i]['total_paid_'.$jj] + ${'select_data_'.$y}[$i]['total_due_'.$jj] + ${'select_data_'.$y}[$i]['total_pending_'.$jj];
                            $temp_all_total_each_month_count = ${'select_data_'.$y}[$i]['total_paid_count_'.$jj] + ${'select_data_'.$y}[$i]['total_due_count_'.$jj] + ${'select_data_'.$y}[$i]['total_pending_count_'.$jj];
                            if ($temp_all_total_each_month > 0) {
                                
                                if ($this->input->get('payment_status') == 0) {
                                    $this->excel->getActiveSheet()->SetCellValue($column.$r, $temp_all_total_each_month);                                    
                                    $this->excel->getActiveSheet()->getStyle($column.$r)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('d6d6d6');
                                    $this->excel->getActiveSheet()->getStyle($column.$r)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                                    $this->excel->getActiveSheet()->getStyle($column.$r)->applyFromArray($style_money_big);
                                }
                        
                                if ($this->input->get('payment_status') == 1 || $this->input->get('payment_status') == 0)
                                if (${'select_data_'.$y}[$i]['total_paid_'.$jj] > 0) {        
                                    $temp = ${"select_data_".$y}[$i]["total_paid_".$jj];
                                    $this->excel->getActiveSheet()->SetCellValue($column.($r + 1), $temp);
                                    $this->excel->getActiveSheet()->getStyle($column.($r + 1))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('c4fdc4');
                                    $this->excel->getActiveSheet()->getStyle($column.($r + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                                }
                                                                                    
                                $temp = '';
                                if ($this->input->get('payment_status') == 2 || $this->input->get('payment_status') == 0)
                                if (${'select_data_'.$y}[$i]['total_due_'.$jj] > 0){        
                                    $temp = ${"select_data_".$y}[$i]["total_due_".$jj];
                                    $this->excel->getActiveSheet()->SetCellValue($column.($r + 2), $temp);
                                    $this->excel->getActiveSheet()->getStyle($column.($r + 2))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('f6b2b0');                                
                                    $this->excel->getActiveSheet()->getStyle($column.($r + 2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                                }
                                
                                $temp = '';
                                if ($this->input->get('payment_status') == 3 || $this->input->get('payment_status') == 0)
                                if (${'select_data_'.$y}[$i]['total_pending_'.$jj] > 0) {                        
                                    $temp = ${'select_data_'.$y}[$i]['total_pending_'.$jj];
                                    $this->excel->getActiveSheet()->SetCellValue($column.($r + 3), $temp);
                                    $this->excel->getActiveSheet()->getStyle($column.($r + 3))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('f4cc93');                                                                
                                    $this->excel->getActiveSheet()->getStyle($column.($r + 3))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                                }
                            }
                            else {
                                $temp_col_no_record = $column.$r.':'.$column.($r + 3);
                                $this->excel->getActiveSheet()->mergeCells($temp_col_no_record);
                            }
    
    
                            $jj++;
                        }
                        if ($i < count(${'select_data_'.$y}) - 1)
                            $r = $r + 4;
                        else
                            $r = $r + 3;
                        $r_bg++;
                    }

                    $this->excel->getActiveSheet()->mergeCells('A'.($r + 1).':A'.($r+4));                        
                    $this->excel->getActiveSheet()->mergeCells('B'.($r + 1).':B'.($r+4));
                    $column = 'A';                       
                    $this->excel->getActiveSheet() ->getStyle($column.($r + 1).':'.$column.($r + 4))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($bg_footer);
                    $column = 'B';                       
                    $this->excel->getActiveSheet() ->getStyle($column.($r + 1).':'.$column.($r + 4))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($bg_footer);

                    $jj = 1;                        
                    foreach (range('C', 'N') as $column) {
                        $temp4 = ${$y.'_grand_total_paid_'.$jj} + ${$y.'_grand_total_due_'.$jj} + ${$y.'_grand_total_pending_'.$jj};
                        $temp5 = ${$y.'_grand_paid_count_'.$jj} + ${$y.'_grand_due_count_'.$jj} + ${$y.'_grand_pending_count_'.$jj};                        
                        
                        $this->excel->getActiveSheet() ->getStyle($column.($r + 1).':'.$column.($r + 4))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($bg_footer);

                        if ($temp4 > 0) {                            
                            $temp = $temp4;
                            if ($this->input->get('payment_status') == 0)
                                $this->excel->getActiveSheet()->SetCellValue($column.($r + 1), $temp);
                            $this->excel->getActiveSheet()->getStyle($column.($r + 1))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('d6d6d6');                                                                
                            $this->excel->getActiveSheet()->getStyle($column.($r + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                            $this->excel->getActiveSheet()->getStyle($column.($r + 1))->applyFromArray($style_money_big);

                            if ($this->input->get('payment_status') == 1 || $this->input->get('payment_status') == 0)
                            if (${$y.'_grand_paid_count_'.$jj} > 0) {
                                $temp = ${$y.'_grand_total_paid_'.$jj};
                                $this->excel->getActiveSheet()->SetCellValue($column.($r + 2), $temp);
                                $this->excel->getActiveSheet()->getStyle($column.($r + 2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                            }
                            $this->excel->getActiveSheet()->getStyle($column.($r + 2))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('c4fdc4');

                            if ($this->input->get('payment_status') == 2 || $this->input->get('payment_status') == 0)
                            if (${$y.'_grand_total_due_'.$jj} > 0) {
                                $temp = ${$y.'_grand_total_due_'.$jj};
                                $this->excel->getActiveSheet()->SetCellValue($column.($r + 3), $temp);
                                $this->excel->getActiveSheet()->getStyle($column.($r + 3))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                            }
                            $this->excel->getActiveSheet()->getStyle($column.($r + 3))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('f6b2b0');
                                                                                            
                            if ($this->input->get('payment_status') == 3 || $this->input->get('payment_status') == 0)
                            if (${$y.'_grand_total_pending_'.$jj} > 0) {
                                $temp = ${$y.'_grand_total_pending_'.$jj};
                                $this->excel->getActiveSheet()->SetCellValue($column.($r + 4), $temp);
                                $this->excel->getActiveSheet()->getStyle($column.($r + 4))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                            }
                            $this->excel->getActiveSheet()->getStyle($column.($r + 4))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('f4cc93');                                                                

                        }
                        else {
                            $temp_col_no_record = $column.($r + 1).':'.$column.($r + 4);
                            $this->excel->getActiveSheet()->mergeCells($temp_col_no_record);
                        }
                        
                        $jj++;                                                                                    
                    }

                    $r = $r + 12;
                }
                
                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
                foreach (range('C', 'N') as $column) {
                    $this->excel->getActiveSheet()->getColumnDimension($column)->setWidth(13);
                }
                $this->excel->getActiveSheet()->getStyle('A1:A'.$r)->getAlignment()->setWrapText(true);                        
                $this->excel->getActiveSheet()->getStyle('B1:B'.$r)->getAlignment()->setWrapText(true);
                $this->excel->getActiveSheet()->getStyle('A1:A'.$r)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('B1:B'.$r)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                  
                $temp = date_create($start_date);
                $temp2 = date_create($end_date);
                $filename = 'Sales Report For Each Customer From '.date_format($temp, 'F-Y').' to '.date_format($temp2, 'F-Y');
                
                if ($csv) {
                    ob_clean();
                    header('Content-Type: application/csv');
                    header('Content-Disposition: attachment;filename="' . $filename . '.csv"');
                    header('Cache-Control: max-age=0');
                    ob_clean();
                    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                    $objWriter->save('php://output');
                    exit();
                }

            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);
            
        }
    }
	
	function ExportCustomerReport($csv=null)
    {
		$current_month = date('m');
        $order_column =strtolower(date('M', mktime(0, 0, 0, ($current_month -1), 1)));
		$this->db  
			->select("companies.name")   
			->select("CONCAT(`first_name`, ' ', `last_name`) AS sale_person")
			->select("SUM(CASE WHEN MONTH(`date`) = '1' THEN grand_total END) AS `jan`")
			->select("SUM(CASE WHEN MONTH(`date`) = '2' THEN grand_total END) AS `feb`")
			->select("SUM(CASE WHEN MONTH(`date`) = '3' THEN grand_total END) AS `mar`")
			->select("SUM(CASE WHEN MONTH(`date`) = '4' THEN grand_total END) AS `apr`")
			->select("SUM(CASE WHEN MONTH(`date`) = '5' THEN grand_total END) AS `may`")
			->select("SUM(CASE WHEN MONTH(`date`) = '6' THEN grand_total END) AS `jun`")
			->select("SUM(CASE WHEN MONTH(`date`) = '7' THEN grand_total END) AS `jul`")
			->select("SUM(CASE WHEN MONTH(`date`) = '8' THEN grand_total END) AS `aug`")
			->select("SUM(CASE WHEN MONTH(`date`) = '9' THEN grand_total END) AS `sep`")
			->select("SUM(CASE WHEN MONTH(`date`) = '10' THEN grand_total END) AS `oct`")
			->select("SUM(CASE WHEN MONTH(`date`) = '11' THEN grand_total END) AS `nov`")
			->select("SUM(CASE WHEN MONTH(`date`) = '12' THEN grand_total END) AS `dec`")
			->select("COUNT(CASE WHEN MONTH(`date`) = '1' AND payment_status != 'paid' THEN payment_status END) AS `janp`")
			->select("COUNT(CASE WHEN MONTH(`date`) = '2' AND payment_status != 'paid' THEN payment_status END) AS `febp`")
			->select("COUNT(CASE WHEN MONTH(`date`) = '3' AND payment_status != 'paid' THEN payment_status END) AS `marp`")
			->select("COUNT(CASE WHEN MONTH(`date`) = '4' AND payment_status != 'paid' THEN payment_status END) AS `aprp`")
			->select("COUNT(CASE WHEN MONTH(`date`) = '5' AND payment_status != 'paid' THEN payment_status END) AS `mayp`")
			->select("COUNT(CASE WHEN MONTH(`date`) = '6' AND payment_status != 'paid' THEN payment_status END) AS `junp`")
			->select("COUNT(CASE WHEN MONTH(`date`) = '7' AND payment_status != 'paid' THEN payment_status END) AS `julp`")
			->select("COUNT(CASE WHEN MONTH(`date`) = '8' AND payment_status != 'paid' THEN payment_status END) AS `augp`")
			->select("COUNT(CASE WHEN MONTH(`date`) = '9' AND payment_status != 'paid' THEN payment_status END) AS `sepp`")
			->select("COUNT(CASE WHEN MONTH(`date`) = '10' AND payment_status != 'paid' THEN payment_status END) AS `octp`")
			->select("COUNT(CASE WHEN MONTH(`date`) = '11' AND payment_status != 'paid' THEN payment_status END) AS `novp`")
			->select("COUNT(CASE WHEN MONTH(`date`) = '12' AND payment_status != 'paid' THEN payment_status END) AS `decp`")
			->select("companies.city")   
			->select("companies.name as customer_group_name")       
		->from('companies')
		->join('sales', 'companies.id=sales.customer_id', 'left')
		->join('users', 'users.id=companies.sale_person_id', 'left')
		->join('customer_groups', 'customer_groups.id=companies.customer_group_id', 'left')
		->group_by('companies.id');
		if($this->input->get('year')){
			$this->db->where('YEAR(`date`)',$this->input->get('year'));
		}else{
			$this->db->where('YEAR(`date`)',date('Y'));
		}
		if($this->input->get('city')){
			$this->db->where('city',$this->input->get('city'));
		}
		if($this->input->get('start_day') && $this->input->get('end_day')){
			$this->db->where('DAY(`date`) >=',$this->input->get('start_day'));
			$this->db->where('DAY(`date`) <=',$this->input->get('end_day'));
		}    
        $q = $this->db->get();
		if ($q != false && $q->num_rows() > 0) {
			foreach (($q->result()) as $row) {
				$data[] = $row;
			}
		} else {
			$data = NULL;
		}
		
		if (!empty($data)) {
			$this->load->library('excel');
			$this->excel->setActiveSheetIndex(0);
			$this->excel->getActiveSheet()->setTitle(lang('sales_report_for_each_customer'));
			$style_total = array('font' => array('size' => 18,'bold' => true,'color' => array('rgb' => 'ffffff')));
			$style = array('font' => array('size' => 10,'bold' => true,'color' => array('rgb' => 'ff0000')));
			$style_paid = array('font' => array('size' => 10,'bold' => true,'color' => array('rgb' => '478ECD')));
			$style_bg = array('font' => array('size' => 13,'bold' => true,'background-color' => array('rgb' => 'FFFF00')));
			$style_month = array('font' => array('size' => 13,'bold' => true,'color' => array('rgb' => '000000')));
			$style_no = array('font' => array('size' => 12,'' => true,'background-color' => array('rgb' => 'FFFF00')));
			
			$this->excel->getActiveSheet()->SetCellValue('A1', lang('no_csv'))->getStyle('A1', lang('no_csv'))->applyFromArray($style_no);
			$this->excel->getActiveSheet()->SetCellValue('B1', lang('cal_customer'))->getStyle('B1', lang('cal_customer'))->applyFromArray($style_bg);
			$this->excel->getActiveSheet()->SetCellValue('C1', lang('cal_person'))->getStyle('C1', lang('cal_person'))->applyFromArray($style_bg);
			$this->excel->getActiveSheet()->SetCellValue('D1', lang('city'))->getStyle('D1', lang('city'))->applyFromArray($style_bg);
			$this->excel->getActiveSheet()->SetCellValue('E1', lang('customer_group'))->getStyle('E1', lang('customer_group'))->applyFromArray($style_bg);
			$this->excel->getActiveSheet()->SetCellValue('F1', lang('cal_january'))->getStyle('F1', lang('cal_january'))->applyFromArray($style_bg);
			$this->excel->getActiveSheet()->SetCellValue('G1', lang('cal_february'))->getStyle('G1', lang('cal_february'))->applyFromArray($style_bg);
			$this->excel->getActiveSheet()->SetCellValue('H1', lang('cal_march'))->getStyle('H1', lang('cal_march'))->applyFromArray($style_bg);
			$this->excel->getActiveSheet()->SetCellValue('I1', lang('cal_april'))->getStyle('I1', lang('cal_april'))->applyFromArray($style_bg);
			$this->excel->getActiveSheet()->SetCellValue('J1', lang('cal_may'))->getStyle('J1', lang('cal_may'))->applyFromArray($style_bg);
			$this->excel->getActiveSheet()->SetCellValue('K1', lang('cal_june'))->getStyle('K1', lang('cal_june'))->applyFromArray($style_bg);
			$this->excel->getActiveSheet()->SetCellValue('L1', lang('cal_july'))->getStyle('L1', lang('cal_july'))->applyFromArray($style_bg);
			$this->excel->getActiveSheet()->SetCellValue('M1', lang('cal_august'))->getStyle('M1', lang('cal_august'))->applyFromArray($style_bg);
			$this->excel->getActiveSheet()->SetCellValue('N1', lang('cal_september'))->getStyle('N1', lang('cal_september'))->applyFromArray($style_bg);
			$this->excel->getActiveSheet()->SetCellValue('O1', lang('cal_october'))->getStyle('O1', lang('cal_october'))->applyFromArray($style_bg);
			$this->excel->getActiveSheet()->SetCellValue('P1', lang('cal_november'))->getStyle('P1', lang('cal_november'))->applyFromArray($style_bg);
			$this->excel->getActiveSheet()->SetCellValue('Q1', lang('cal_december'))->getStyle('Q1', lang('cal_december'))->applyFromArray($style_bg);
			
			$row =2;
			$no=1;
			foreach ($data as $data_row) {
				$janf=0;
				$jan=$data_row->jan;
				if($jan !=0){
				   $janf=$this->sma->formatMoney($jan);
				}else{
					$janf=$jan;
				}
				
				$febf=0;
				$feb=$data_row->feb;
				if($feb !=0){
				   $febf= $this->sma->formatMoney($feb);
				}else{
					$febf=$feb;
				}
				
				$marf=0;
				$mar=$data_row->mar;
				if($mar !=0){
					$marf=$this->sma->formatMoney($mar);
				}else{
					$marf=$mar;
				}
				
				$aprf=0;
				$apr=$data_row->apr;
				if($apr !=0){
					$aprf=$this->sma->formatMoney($apr);
				}else{
					$aprf=$apf;
				}
				
				$mayf=0;
				$may=$data_row->may;
				if($may !=0){
					$mayf=$this->sma->formatMoney($may);
				}else{
					$mayf=$may;
				}
				
				$junf=0;
				$jun=$data_row->jun;
				if($jun !=0){
					$junf=$this->sma->formatMoney($jun);
				}else{
					$junf=$jun;
				}
				
				$julf=0;
				$jul=$data_row->jul;
				if($jul !=0){
					$julf=$this->sma->formatMoney($jul);
				}else{
					$julf=$jul;
				}
				
				$augf=0;
				$aug=$data_row->aug;
				if($aug !=0){
					$augf=$this->sma->formatMoney($aug);
				}else{
					$augf=$aug;
				}
				
				$sepf=0;
				$sep=$data_row->sep;
				if($sep !=0){
				   $sepf=$this->sma->formatMoney($sep);
				}else{
					$sepf=$sep;
				}
				
				$octf=0;
				$oct=$data_row->oct;
				if($oct !=0){
					$octf=$this->sma->formatMoney($oct);
				}else{
					$octf=$oct;
				}
				
				$novf=0;
				$nov=$data_row->nov;
				if($nov !=0){
					$novf=$this->sma->formatMoney($nov);
				}else{
					$novf=$nov;
				}
				
				$decf=0;
				$dec=$data_row->dec;
				if($dec !=0){
					$decf=$this->sma->formatMoney($dec);
				}else{
					$decf=$dec;
				}
				
				$jana=$data_row->jan;
				$feba=$data_row->feb;
				$mara=$data_row->mar;
				$apra=$data_row->apr;
				$maya=$data_row->may;
				$juna=$data_row->jun;
				$jula=$data_row->jul;
				$auga=$data_row->aug;
				$sepa=$data_row->sep;
				$octa=$data_row->oct;
				$nova=$data_row->nov;
				$deca=$data_row->dec;
				
				$total_jan +=$jana;
				$total_feb +=$feba;
				$total_mar +=$mara;
				$total_apr +=$apra;
				$total_may +=$maya;
				$total_jun +=$juna;
				$total_jul +=$jula;
				$total_aug +=$auga;
				$total_sep +=$sepa;
				$total_oct +=$octa;
				$total_nov +=$nova;
				$total_dec +=$deca;
				$grandtotal=floatval($total_jan+$total_feb+$total_mar+$total_apr+$total_may+$total_jun+$total_jul+$total_aug+$total_sep+$total_oct+$total_nov+$total_dec);
				
				$janp=$data_row->janp;
				if($janp != 0){
					$this->excel->getActiveSheet()->getStyle('F',$janf)->applyFromArray($style);
				}else{
					$this->excel->getActiveSheet()->getStyle('F',$janf)->applyFromArray($style_paid);
				}
				$febp=$data_row->febp;
				if($febp != 0){
					$this->excel->getActiveSheet()->getStyle('G',$febf)->applyFromArray($style);
				}else{
					$this->excel->getActiveSheet()->getStyle('G',$febf)->applyFromArray($style_paid);
				}
				$marp=$data_row->marp;
				if($marp != 0){
					$this->excel->getActiveSheet()->getStyle('H',$marf)->applyFromArray($style);
				}else{
					$this->excel->getActiveSheet()->getStyle('H',$marf)->applyFromArray($style_paid);
				}
				$aprp=$data_row->aprp;
				if($aprp != 0){
					$this->excel->getActiveSheet()->getStyle('I',$aprf)->applyFromArray($style);
				}else{
					$this->excel->getActiveSheet()->getStyle('I',$aprf)->applyFromArray($style_paid);
				}
				$mayp=$data_row->mayp;
				if($mayp > 0){
					$this->excel->getActiveSheet()->getStyle('J',$mayf)->applyFromArray($style);
				}else{
					$this->excel->getActiveSheet()->getStyle('J',$mayf)->applyFromArray($style_paid);
				}
				$junp=$data_row->junp;
				if($junp > 0){
					$this->excel->getActiveSheet()->getStyle('K',$junf)->applyFromArray($style);
				}else{
					$this->excel->getActiveSheet()->getStyle('K',$junf)->applyFromArray($style_paid);
				}
				$julp=$data_row->julp;
				if($julp > 0){
					$this->excel->getActiveSheet()->getStyle('L',$julf)->applyFromArray($style);
				}else{
					$this->excel->getActiveSheet()->getStyle('L',$julf)->applyFromArray($style_paid);
				}
				$augp=$data_row->augp;
				if($augp > 0){
					$this->excel->getActiveSheet()->getStyle('M',$augf)->applyFromArray($style);
				}else{
					$this->excel->getActiveSheet()->getStyle('M',$augf)->applyFromArray($style_paid);
				}
				$sepp=$data_row->sepp;
				if($sepp > 0){
					$this->excel->getActiveSheet()->getStyle('N',$sepf)->applyFromArray($style);
				}else{
					$this->excel->getActiveSheet()->getStyle('N',$sepf)->applyFromArray($style_paid);
				}
				$octp=$data_row->octp;
				if($octp > 0){
					$this->excel->getActiveSheet()->getStyle('O',$octf)->applyFromArray($style);
				}else{
					$this->excel->getActiveSheet()->getStyle('O',$octf)->applyFromArray($style_paid);
				}
				$novp=$data_row->novp;
				if($novp > 0){
					$this->excel->getActiveSheet()->getStyle('P',$novf)->applyFromArray($style);
				}else{
					$this->excel->getActiveSheet()->getStyle('P',$novf)->applyFromArray($style_paid);
				}
				$decp=$data_row->decp;
				if($decp > 0){
					$this->excel->getActiveSheet()->getStyle('Q',$decf)->applyFromArray($style);
				}else{
					$this->excel->getActiveSheet()->getStyle('Q',$decf)->applyFromArray($style_paid);
				}
				
				PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_AdvancedValueBinder() );
			   
				$this->excel->getActiveSheet()->SetCellValue('A' . $row, $no);
				$this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->name);
				$this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->sale_person);
				$this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->city);
				$this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->customer_group_name);
				$this->excel->getActiveSheet()->SetCellValue('F' . $row, $janf);
				$this->excel->getActiveSheet()->SetCellValue('G' . $row, $febf);
				$this->excel->getActiveSheet()->SetCellValue('H' . $row, $marf);
				$this->excel->getActiveSheet()->SetCellValue('I' . $row, $aprf);
				$this->excel->getActiveSheet()->SetCellValue('J' . $row, $mayf);
				$this->excel->getActiveSheet()->SetCellValue('K' . $row, $junf);
				$this->excel->getActiveSheet()->SetCellValue('L' . $row, $julf);
				$this->excel->getActiveSheet()->SetCellValue('M' . $row, $augf);
				$this->excel->getActiveSheet()->SetCellValue('N' . $row, $sepf);
				$this->excel->getActiveSheet()->SetCellValue('O' . $row, $octf);
				$this->excel->getActiveSheet()->SetCellValue('P' . $row, $novf);
				$this->excel->getActiveSheet()->SetCellValue('Q' . $row, $decf);
				
				//formart style text center on money monthly
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$this->excel->getActiveSheet()->getStyle('B'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$this->excel->getActiveSheet()->getStyle('C'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$this->excel->getActiveSheet()->getStyle('D'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$this->excel->getActiveSheet()->getStyle('E'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('F'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('F'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('G'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('G'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('H'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('H'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('I'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('I'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('J'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('J'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('K'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('K'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('L'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('L'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('M'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('M'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('N'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('N'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('O'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('O'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('P'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('P'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('Q'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle('Q'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				//end formart style text center on money monthly
				
				
				$no++;
				$row++;

				$row_data=count($data);
				$last_row=$row_data + 2; 
				$last_row_year=$row_data + 3;
				
				$this->excel->getActiveSheet()->SetCellValue('F'.$last_row, 'Total : '.$this->sma->formatMoney($total_jan))->getStyle('F'.$last_row, 'Total')->applyFromArray($style_month);
				$this->excel->getActiveSheet()->SetCellValue('G'.$last_row, 'Total : '.$this->sma->formatMoney($total_feb))->getStyle('G'.$last_row, 'Total')->applyFromArray($style_month);
				$this->excel->getActiveSheet()->SetCellValue('H'.$last_row, 'Total : '.$this->sma->formatMoney($total_mar))->getStyle('H'.$last_row, 'Total')->applyFromArray($style_month);
				$this->excel->getActiveSheet()->SetCellValue('I'.$last_row, 'Total : '.$this->sma->formatMoney($total_apr))->getStyle('I'.$last_row, 'Total')->applyFromArray($style_month);
				$this->excel->getActiveSheet()->SetCellValue('J'.$last_row, 'Total : '.$this->sma->formatMoney($total_may))->getStyle('J'.$last_row, 'Total')->applyFromArray($style_month);
				$this->excel->getActiveSheet()->SetCellValue('K'.$last_row, 'Total : '.$this->sma->formatMoney($total_jun))->getStyle('K'.$last_row, 'Total')->applyFromArray($style_month);
				$this->excel->getActiveSheet()->SetCellValue('L'.$last_row, 'Total : '.$this->sma->formatMoney($total_jul))->getStyle('L'.$last_row, 'Total')->applyFromArray($style_month);
				$this->excel->getActiveSheet()->SetCellValue('M'.$last_row, 'Total : '.$this->sma->formatMoney($total_aug))->getStyle('M'.$last_row, 'Total')->applyFromArray($style_month);
				$this->excel->getActiveSheet()->SetCellValue('N'.$last_row, 'Total : '.$this->sma->formatMoney($total_sep))->getStyle('N'.$last_row, 'Total')->applyFromArray($style_month);
				$this->excel->getActiveSheet()->SetCellValue('O'.$last_row, 'Total : '.$this->sma->formatMoney($total_oct))->getStyle('O'.$last_row, 'Total')->applyFromArray($style_month);
				$this->excel->getActiveSheet()->SetCellValue('P'.$last_row, 'Total : '.$this->sma->formatMoney($total_nov))->getStyle('P'.$last_row, 'Total')->applyFromArray($style_month);
				$this->excel->getActiveSheet()->SetCellValue('Q'.$last_row, 'Total : '.$this->sma->formatMoney($total_dec))->getStyle('Q'.$last_row, 'Total')->applyFromArray($style_month);

				$this->excel->getActiveSheet()->mergeCells('A'.$last_row_year.':Q'.$last_row_year);
				$this->excel->getActiveSheet()->SetCellValue('A'.$last_row_year, 'Total Amount For Year '.$year.' : '.$this->sma->formatMoney($grandtotal));
				$this->excel->getActiveSheet()->getStyle('A'.$last_row_year, 'Total Amount For Year '.$year.' : '.$this->sma->formatMoney($grandtotal))->applyFromArray($style_total);
				//add padding for Total Amount For year $year
				$this->excel->getActiveSheet()->getStyle('A'.$last_row_year)->getAlignment()->setIndent(3);
			}
	
			
			//start row in excel
			$this->excel->getActiveSheet() ->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('E7E6E6');
			$this->excel->getActiveSheet() ->getStyle('C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('E7E6E6');
			$this->excel->getActiveSheet() ->getStyle('E1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('E7E6E6');
			$this->excel->getActiveSheet() ->getStyle('G1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('E7E6E6');
			$this->excel->getActiveSheet() ->getStyle('I1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('E7E6E6');
			$this->excel->getActiveSheet() ->getStyle('K1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('E7E6E6');
			$this->excel->getActiveSheet() ->getStyle('M1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('E7E6E6');
			$this->excel->getActiveSheet() ->getStyle('O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('E7E6E6');
			$this->excel->getActiveSheet() ->getStyle('Q1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('E7E6E6');
			
			$this->excel->getActiveSheet() ->getStyle('B1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('BDD7EE');
			$this->excel->getActiveSheet() ->getStyle('D1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('BDD7EE');
			$this->excel->getActiveSheet() ->getStyle('F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('BDD7EE');
			$this->excel->getActiveSheet() ->getStyle('H1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('BDD7EE');
			$this->excel->getActiveSheet() ->getStyle('J1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('BDD7EE');
			$this->excel->getActiveSheet() ->getStyle('L1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('BDD7EE');
			$this->excel->getActiveSheet() ->getStyle('N1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('BDD7EE');
			$this->excel->getActiveSheet() ->getStyle('P1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('BDD7EE');

			//end first row in excel
			
			//last row total monthly
			$this->excel->getActiveSheet() ->getStyle('A'.$last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('E7E6E6');
			$this->excel->getActiveSheet() ->getStyle('C'.$last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('E7E6E6');
			$this->excel->getActiveSheet() ->getStyle('E'.$last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('E7E6E6');
			$this->excel->getActiveSheet() ->getStyle('G'.$last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('E7E6E6');
			$this->excel->getActiveSheet() ->getStyle('I'.$last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('E7E6E6');
			$this->excel->getActiveSheet() ->getStyle('K'.$last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('E7E6E6');
			$this->excel->getActiveSheet() ->getStyle('M'.$last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('E7E6E6');
			$this->excel->getActiveSheet() ->getStyle('O'.$last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('E7E6E6');
			$this->excel->getActiveSheet() ->getStyle('Q'.$last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('E7E6E6');
			
			$this->excel->getActiveSheet() ->getStyle('B'.$last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('BDD7EE');
			$this->excel->getActiveSheet() ->getStyle('D'.$last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('BDD7EE');
			$this->excel->getActiveSheet() ->getStyle('F'.$last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('BDD7EE');
			$this->excel->getActiveSheet() ->getStyle('H'.$last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('BDD7EE');
			$this->excel->getActiveSheet() ->getStyle('J'.$last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('BDD7EE');
			$this->excel->getActiveSheet() ->getStyle('L'.$last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('BDD7EE');
			$this->excel->getActiveSheet() ->getStyle('N'.$last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('BDD7EE');
			$this->excel->getActiveSheet() ->getStyle('P'.$last_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('BDD7EE');
			//end last row total monthly
			
			//last row total year
			$this->excel->getActiveSheet() ->getStyle('A'.$last_row_year)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('85BD26');
		  
			$this->excel->getActiveSheet()->getStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			
			//center text
			$this->excel->getActiveSheet()->getStyle('F'.$last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('F'.$last_row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('G'.$last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('G'.$last_row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('H'.$last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('H'.$last_row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('I'.$last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('I'.$last_row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('J'.$last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('J'.$last_row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('K'.$last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('K'.$last_row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('L'.$last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('L'.$last_row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('M'.$last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('M'.$last_row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('N'.$last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('N'.$last_row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('O'.$last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('O'.$last_row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('P'.$last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('P'.$last_row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('Q'.$last_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('Q'.$last_row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			
			$this->excel->getActiveSheet()->getStyle('A'.$last_row_year)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$this->excel->getActiveSheet()->getStyle('A'.$last_row_year)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			
			$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('E1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('F1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('G1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('H1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('I1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('I1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('J1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('J1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('K1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('K1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('L1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('M1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('M1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('N1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('N1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('O1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('P1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('P1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('Q1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('Q1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			//end center text
			$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
			$this->excel->getActiveSheet()->getRowDimension($last_row)->setRowHeight(35);
			$this->excel->getActiveSheet()->getRowDimension($last_row_year)->setRowHeight(60);
			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
			$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
			$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
			$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(50);
			$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
			$filename = 'monthly_sales_report_for_each_customer_'.$year;
			$this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			if ($csv) {
				$this->excel->getActiveSheet()->getStyle('E2:E' . $row)->getAlignment()->setWrapText(true);
				ob_clean();
				header('Content-Type: application/csv');
				header('Content-Disposition: attachment;filename="' . $filename . '.csv"');
				header('Cache-Control: max-age=0');
				ob_clean();
				$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
				$objWriter->save('php://output');
				exit();
			}
		}
		$this->session->set_flashdata('error', lang('nothing_found'));
		redirect($_SERVER["HTTP_REFERER"]);
    }
	
	
	
}
