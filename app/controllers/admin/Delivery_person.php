<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Delivery_person extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect('admin');
        }
        $this->lang->admin_load('delivery_person', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('delivery_person_model');
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif';
        $this->allowed_file_size = '1024';
		$this->load->helper('url');
    }

    function index($id = null)
    {
        $this->sma->checkPermissions();

        $sort_by = $this->input->post('sort_by');
        $sort_order = $this->input->post('sort_order');

        $per_page = $this->input->post('per_page') != '' ? $this->input->post('per_page') : 100;
        $page = $this->input->get('page') != '' ? $this->input->get('page') : 1;

        $offset_no = $page != '' ? ($page - 1 ) * $per_page : $offset_no;
        
        if ($page != '')
            $offset_no = ($page - 1 ) * $per_page;
        else
            $offset_no = 0;


        $search = $this->input->post('search');
        $search = $this->db->escape_str($search);

        $config_data = array(
            'lg' => 'en',
            'table_name' => 'sma_delivery_person',
            'table_id' => 'id',
            'search' => $search,
            'sort_by' => $sort_by,
            'sort_order' => $sort_order,
            'limit' => $per_page,            
            'offset_no' => $offset_no,
        );
        $select_data = $this->delivery_person_model->get($config_data);

        $config_data = array(
            'lg' => 'en',
            'table_name' => 'sma_delivery_person',
            'table_id' => 'id',
            'search' => $search,
            'sort_by' => '',
            'sort_order' => '',
            'limit' => '',            
            'offset_no' => '',
        );
        $temp_count = $this->delivery_person_model->get($config_data);
        $count_data_sale = count($temp_count);

        /* config pagination */
        $this->data['select_data'] = $select_data;
        $this->data['per_page'] = $per_page;
        $this->data['sort_by'] = $sort_by;
        $this->data['sort_order'] = $sort_order;
        $this->data['total_rows_sale'] = $count_data_sale;
        $this->data['index_page'] = 'admin/delivery_person';
        $this->data['show_data'] = $page;

        $config_base_url = site_url() . 'admin/delivery_person'; //your url where the content is displayed
        $config['base_url'] = $config_base_url; //your url where the content is displayed
        $config['query_string_segment'] = 'page';
        $config['per_page'] = $per_page; //$per_page;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['full_tag_open'] = '<ul class="pagination pagination-new-bg">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['prev_link'] = '&lt; Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next &gt;';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['total_rows'] = $count_data_sale; //$this->data['total_rows_sale'];        
        $this->load->library('pagination');   
        $this->pagination->initialize($config);
        /** end config pagination */
        /**Showing Label Data At Table Footer */
        $showing = $count_data_sale != '' ? 1 : 0;
        $label_show_to= ($page == '' || $per_page < $count_data_sale) ?  $per_page  :  $count_data_sale;
        $label_show = "Showing " . $showing . " to ".$label_show_to." of : " . number_format($count_data_sale) . " entries";

        $show_page = $page > 1 ? ($page * $per_page) : $count_data_sale;
        $show_page_no = $show_page > $count_data_sale ? $count_data_sale : $page * $per_page;
        $label_show_page = "Showing " . (($page - 1) * ($per_page) + 1) . " to " . $show_page_no . " (filtered from " . number_format($count_data_sale) . " total entries) ";
        $this->data['show'] = $search != '' || $page > 1 ? $label_show_page : $label_show;
        /** End Showing Label Data At Table Footer */ 

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('delivery_person')));
        $meta = array('page_title' => lang('delivery_person'), 'bc' => $bc);

        $this->page_construct('delivery_person/index', $meta, $this->data);
    }

    function edit($id = null)
    {
        $this->sma->checkPermissions(false, true);
        $select_data = array();
        if ($id > 0) {
            $config_data = array(
                'lg' => 'en',
                'table_name' => 'sma_delivery_person',
                'table_id' => 'id',
                'selected_id' => $id,
            );
            $select_data = $this->delivery_person_model->get($config_data);
        }

        $this->form_validation->set_rules('fullname', lang("fullname"), 'trim|required');
        //$this->form_validation->set_rules('add_ons_email', lang("email"), 'trim|required|valid_email');
        //$this->form_validation->set_rules('add_ons_phone', lang("phone"), 'trim|required');

        $temp2 = trim($this->input->post('fullname'));
        if ($temp2 != '') {
            $config_data = array(
                'table_name' => 'sma_delivery_person',
                'select_table' => 'sma_delivery_person',
                'translate' => 'en',
                'select_condition' => "translate like '%en:{".$temp2."}:%'",
            );
            $temp = $this->site->api_select_data_v2($config_data);
            if (count($temp) > 0) {
                $this->session->set_flashdata('error', lang("Fullname").': '.$temp2.' '.lang("is_aready_existed."));
                admin_redirect('delivery_person/index');            
            }
        }

        
        if ($this->form_validation->run() == true) {
            $data = array(
                'translate' => 'en:{'.$this->input->post('fullname').'}:',
            );
            if ($select_data[0]['id'] > 0) {
                $this->site->api_update_table('sma_delivery_person',$data,"id = ".$select_data[0]['id']);
                foreach($_POST as $name => $value) {
                    if (is_int(strpos($name,"add_ons_"))) {
                        $temp_name = str_replace('add_ons_','',$name);                        
                        $config_data = array(
                            'table_name' => 'sma_delivery_person',
                            'id_name' => 'id',
                            'field_add_ons_name' => 'add_ons',
                            'selected_id' => $select_data[0]['id'],
                            'add_ons_title' => $temp_name,
                            'add_ons_value' => $value,                    
                        );
                        $this->site->api_update_add_ons_field($config_data);                        
                    }
                }                
                $this->session->set_flashdata('message', lang("delivery_person").' '.lang("updated"));
            }
            else {                
                $add_ons = 'initial_first_add_ons:{}:';
                foreach($_POST as $name => $value) {
                    if (is_int(strpos($name,"add_ons_"))) {
                        $temp_name = str_replace('add_ons_','',$name);
                        $add_ons .= $temp_name.':{'.$value.'}:';
                    }
                }
                $data['add_ons'] = $add_ons;  

                $this->delivery_person_model->insert($data);            
                $this->session->set_flashdata('message', lang("delivery_person").' '.lang("added"));                
            }
            
            admin_redirect('delivery_person/index');
        }
        else {            
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();

            $this->data['select_data'] = $select_data;
            $this->load->view($this->theme . 'delivery_person/edit', $this->data);
        }
        
    }

    function delete($id = null)
    {
        $this->sma->checkPermissions(null, true);
        if ($id > 0)
            $this->db->delete('sma_delivery_person', array('id' => $id));
        $this->session->set_flashdata('message', lang('delivery_person').' '.lang('deleted'));
        redirect($_SERVER["HTTP_REFERER"]);
    }
    public function bulk_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {
            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {

                    $this->sma->checkPermissions('delete');
                    $j = 0;
                    foreach ($_POST['val'] as $id) {
                        $this->db->delete('sma_delivery_person', array('id' => $id));
                        $j++;
                    }
                    $this->session->set_flashdata('message', $j.' '.lang("delivery_person").' '.lang("deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);

                } 
            } else {
                $this->session->set_flashdata('error', lang("no").' '.lang("delivery_person").' '.lang("selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
	
}
